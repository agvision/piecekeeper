jQuery(document).ready(function(){

  /* START  Multiple Pages */

  $('#reject').click(function(){
      $('#reject-form').slideDown('slow');
  });

  $('#cancel-reject').click(function(){
      $('#reject-form').slideUp('slow');
  });

  /* ENDS   Multiple Pages */

  /* START  Admins Add Page */

  $('#all-countries').click(function () {
    $("#label-countries").slideToggle(!(this.checked));
  });

  /* END    Admins Add Page */

  /* START  Admins Edit Page */

  if(typeof all_selected == 'undefined' || !all_selected) {
    $("#label-countries").slideDown();
  } else {
    $("#label-countries").slideUp();
  }

  /* END    Admins Edit Page */

  /* START  Best Performers View */

  $("#handle_checkboxes").change(function(){
      if($(this).is(":checked")){
          $(".approved").prop("checked", true);
      } else {
          $(".approved").prop("checked", false);
      }
  });

  /* END    Best Performers View */

  /* START  Broadcast Index */
  $('.broadcast-index #broadcast-filter-submit').click(function(event){
    event.preventDefault();
    $(".broadcast-index #exp").val("false");
    $(".broadcast-index #form-filter").submit();
  });

  $(".broadcast-index #submit-export").click(function(event){
      event.preventDefault();

      $(".broadcast-index #exp").val("true");

      $(".broadcast-index #exp-format").val($("#select-exp-format").val());

      $(".broadcast-index #form-filter").submit();
  });

  $('.broadcast-index #search-results').click(function(){
      $('.broadcast-index #list-results').slideToggle('slow');
  });

  $('.broadcast-index #filters-btn').click(function(){
      var visible = $('.broadcast-index #filters').is(':visible');

      $(".broadcast-index #filters").slideToggle('slow');

      if (!visible) {
          $(this).html('Hide Filters');
      } else {
          $(this).html('Show Filters');
      }
  });

  $(document).on('change','.broadcast-index #country',function(){
      var id_country = this.value;

      broadcastIndexChangeCitiesByCountry(id_country);
      broadcastIndexChangeSchoolsByCountry(id_country);
  });

  $(document).on('change',".broadcast-index #city",function(){
      var city = this.value;
      broadcastIndexChangeSchoolsByCity(city);
  });


  $('.broadcast-index #filter-add-user-tag').click(function(){
      $('.broadcast-index #user-tags-options-wrapper').append(tag_options);
  });

  /* END    Broadcast Index */

  /* START  Coupon Add */

  $('.coupon-add .amount-type').hide();
  $('.coupon-add .percent-type').val("");
  $('.coupon-add .amount-type').val("");

  $('.coupon-add #discount-type').change(function(){
    var value = $(this).val();
    if (value == 'percent') {
      $('.coupon-add .amount-type').hide();
      $('.coupon-add .percent-type').show();

      $('.coupon-add .amount-type').val("");
    }
    else if (value == 'amount') {
      $('.coupon-add .percent-type').hide();
      $('.coupon-add .amount-type').show();

      $('.coupon-add .percent-type').val("");
    }

  });

  /* END  Coupon Add */

  /* START  Coupon Edit */

  $('.coupon-edit .amount-type').hide();
  $('.coupon-edit .percent-type').val("");
  $('.coupon-edit .amount-type').val("");

  $('.coupon-edit #discount-type').change(function(){
    var value = $(this).val();
    if (value == 'percent') {
      $('.coupon-edit .amount-type').hide();
      $('.coupon-edit .percent-type').show();

      $('.coupon-edit .amount-type').val("");
    }
    else if (value == 'amount') {
      $('.coupon-edit .percent-type').hide();
      $('.coupon-edit .amount-type').show();

      $('.coupon-edit .percent-type').val("");
    }
  });

  $('.coupon-edit #reject-coupon').click(function(){
      $('.coupon-edit #reject-coupon-form').slideToggle('slow');
  });

  $('.coupon-edit #cancel-reject').click(function(){
      $('.coupon-edit #reject-coupon-form').slideUp('slow');
  });

  /* END  Coupon Edit */

  /* START  Coupons Request */

  $('.coupons-request #select-all').click(function(){
      if ($(this).is(':checked')) {
          $('.coupons-request .request-checkbox').each(function(){
              $(this).prop('checked', true);
          });
      } else {
          $('.coupons-request .request-checkbox').each(function(){
              $(this).prop('checked', false);
          });
      }
  });

  /* END    Coupons Request */

  /* START Goal Add */

  $(document).on('change','.goal-add #country',function(){
      var id_country = this.value;
      changeCitiesByCountry(id_country);
  });

  /* END   Goal Add */

  /* START Goal Edit */

  $(document).on('change','.goal-edit #country',function(){
      var id_country = this.value;
      changeCitiesByCountry(id_country);
  });

  /* END   Goal Edit */

  /* START Master Requests */

  $('.master-requests-list #select-all').click(function(){
      if ($(this).is(':checked')) {
          $('.master-requests-list .request-checkbox').each(function(){
              $(this).prop('checked', true);
          });
      } else {
          $('.master-requests-list .request-checkbox').each(function(){
              $(this).prop('checked', false);
          });
      }
  });

  /* END   Master Requests */

  /* START Mission Add */

  $(document).on('change','.mission-add #country',function(){
      var id_country = this.value;
      changeCitiesByCountry(id_country);
  });

  /* END   Mission Add */

  /* START Mission Edit */

  $(document).on('change','.mission-edit #country',function(){
      var id_country = this.value;
      changeCitiesByCountry(id_country);
  });

  /* END   Mission Edit */

  /* START Mission Response */

  $('.mission-response #reject').click(function(){
      $('.mission-response #reject-wrapper').slideToggle(500);
  });

  /* END   Mission Response */

  /* START Missions List */
  if( $(".missions-list #missions-list").length > 0) {
    $(".missions-list #missions-list").tablesorter({
        headers: {
            3: { sorter:'usLongDate' },
            4: { sorter:'usLongDate' }
        }
    });
  }

  /* END   Missions List */

  /* START Payments Requests List */

  $('.payment-requests-list #select-all').click(function(){
      if ($(this).is(':checked')) {
          $('.payment-requests-list .request-checkbox').each(function(){
              $(this).prop('checked', true);
          });
      } else {
          $('.payment-requests-list .request-checkbox').each(function(){
              $(this).prop('checked', false);
          });
      }
  });

  /* END  Payments Requests List */

  /* START Payments Requests View */

  $('.payment-requests-view #reject-coupon').click(function(){
      $('.payment-requests-view #reject-coupon-form').slideToggle('slow');
  });

  $('.payment-requests-view #cancel-reject').click(function(){
      $('.payment-requests-view #reject-coupon-form').slideUp('slow');
  });

  /* END  Payments Requests View */

  /* START Slaves Requests */

  $('.slaves-requests #select-all').click(function(){
      if ($(this).is(':checked')) {
          $('.slaves-requests .request-checkbox').each(function(){
              $(this).prop('checked', true);
          });
      } else {
          $('.slaves-requests .request-checkbox').each(function(){
              $(this).prop('checked', false);
          });
      }
  });

  /* END  Slaves Requests */

  /* START Slaves Requests View */

  $('.slaves-request-view #reject-coupon').click(function(){
      $('.slaves-request-view #reject-coupon-form').slideToggle('slow');
  });

  $('.slaves-request-view #cancel-reject').click(function(){
      $('.slaves-request-view #reject-coupon-form').slideUp('slow');
  });

  /* END  Slaves Requests View */

  /* START Stats Index */
  $(document).on('change','.stats-index .country-loader',function(){
     var id_country = this.value;
     changeCitiesByCountry(id_country);
  });

  if(jQuery('.stats-index .date').length>0) {
    jQuery('.stats-index .date').fdatepicker({format: 'yyyy-mm-dd'});
  }
  $( ".stats-index #tabs" ).tabs();

  /* END Stats Index */
  /* START User List */
  $(document).on('change','.user-list #city',function(){
      var city = this.value;
      changeSchoolsByCity(city);
  });

  if(jQuery('.user-list .date').length>0) {
    jQuery('.user-list .date').fdatepicker({format: 'yyyy-mm-dd'});
  }
  $(document).on('change','.user-list #country',function(){
      var id_country = this.value;
      changeCitiesByCountry(id_country);
      changeSchoolsByCountry(id_country);
  });
  if($(".user-list #piecekeepers-list").length>0) {
      $(".user-list #piecekeepers-list").tablesorter({
        headers: {
            5: {
                sorter:'shortDate'
            }
        }
    });
  }

  $(".user-list #table-order-by-inact").off();

  $(".user-list #table-order-by-inact").click(function(event) {
    event.preventDefault();
    if( $(".user-list #table-order-by").val() == "inact") {
        if( $(".user-list #table-order-how").val() == "asc") {
            $(".user-list #table-order-how").val("desc");
        } else {
            $(".user-list #table-order-how").val("asc");
        }

    } else {
        $(".user-list #table-order-by").val("inact");
        $(".user-list #table-order-how").val("desc");
    }

    $(".user-list #filter-form").submit();
  });

  $(".user-list #table-order-by-reg").off();

  $(".user-list #table-order-by-reg").click(function(event) {

      event.preventDefault();

      if( $(".user-list #table-order-by").val() == "reg") {
          if( $(".user-list #table-order-how").val() == "asc") {
              $(".user-list #table-order-how").val("desc");
          } else {
              $(".user-list #table-order-how").val("asc");
          }

      } else {
          $(".user-list #table-order-by").val("reg");
          $(".user-list #table-order-how").val("desc");
      }

      $(".user-list #filter-form").submit();

  });

  $(".user-list #table-order-by-points").off();

  $(".user-list #table-order-by-points").click(function(event) {
      event.preventDefault();

      if( $(".user-list #table-order-by").val() == "points") {
          if( $(".user-list #table-order-how").val() == "asc") {
              $(".user-list #table-order-how").val("desc");
          } else {
              $(".user-list #table-order-how").val("asc");
          }

      } else {
          $(".user-list #table-order-by").val("points");
          $(".user-list #table-order-how").val("desc");
      }

      $(".user-list #filter-form").submit();

  });

  $(".user-list #table-order-by-followers").off();

  $(".user-list #table-order-by-followers").click(function(event) {
      event.preventDefault();

      if( $(".user-list #table-order-by").val() == "followers") {
          if( $(".user-list #table-order-how").val() == "asc") {
              $(".user-list #table-order-how").val("desc");
          } else {
              $(".user-list #table-order-how").val("asc");
          }

      } else {
          $(".user-list #table-order-by").val("followers");
          $(".user-list #table-order-how").val("desc");
      }

      $(".user-list #filter-form").submit();

  });

  $(".user-list #table-order-by-level").off();

  $(".user-list #table-order-by-level").click(function(event) {
      event.preventDefault();

      if( $(".user-list #table-order-by").val() == "level") {
          if( $(".user-list #table-order-how").val() == "asc") {
              $(".user-list #table-order-how").val("desc");
          } else {
              $(".user-list #table-order-how").val("asc");
          }

      } else {
          $(".user-list #table-order-by").val("level");
          $(".user-list #table-order-how").val("desc");
      }

      $(".user-list #filter-form").submit();

  });

  $(".user-list #table-order-by-ipad").off();
  $(".user-list #table-order-by-ipad").click(function(event) {
      event.preventDefault();

      if( $(".user-list #table-order-by").val() == "ipad") {
          if( $(".user-list #table-order-how").val() == "asc") {
              $(".user-list #table-order-how").val("desc");
          } else {
              $(".user-list #table-order-how").val("asc");
          }
      } else {
          $(".user-list #table-order-by").val("ipad");
          $(".user-list #table-order-how").val("desc");
      }

      $(".user-list #filter-form").submit();
  });

  $(".user-list #table-order-by-name").off();

  $(".user-list #table-order-by-name").click(function(event) {
      event.preventDefault();

      if( $(".user-list #table-order-by").val() == "name") {
          if( $(".user-list #table-order-how").val() == "asc") {
              $(".user-list #table-order-how").val("desc");
          } else {
              $(".user-list #table-order-how").val("asc");
          }

      } else {
          $(".user-list #table-order-by").val("name");
          $(".user-list #table-order-how").val("desc");
      }

      $(".user-list #filter-form").submit();

  });

  $(".user-list #table-order-by-email").off();

  $(".user-list #table-order-by-email").click(function(event) {
      event.preventDefault();

      if( $(".user-list #table-order-by").val() == "email") {
          if( $(".user-list #table-order-how").val() == "asc") {
              $(".user-list #table-order-how").val("desc");
          } else {
              $(".user-list #table-order-how").val("asc");
          }

      } else {
          $(".user-list #table-order-by").val("email");
          $(".user-list #table-order-how").val("desc");
      }

      $(".user-list #filter-form").submit();

  });

  if($('.user-list #piecekeepers-list tr a').length > 0) {
      $('.user-list #piecekeepers-list tr a').imgPreview({imgCSS: { width: 400 }});
  }

  $('.user-list #filter-add-user-tag').click(function(){
      $('#user-tags-options-wrapper').append(tag_options);
  });

  $(".user-list .coupon-generator .show").click(function(){
      $(".coupon-generator form").slideToggle(500);
  });

  /* END   User List */

  /* START User View */
  $('.user-view #tabs').tabs();

  $(".user-view #approve-btn").click(function(e){
      e.preventDefault();
      var href  = $(this).attr('href');
      var level = $("#pk-starting-level").val();
      window.location = href+"/"+level;
  });

  $('.user-view .fade-background').click(function(){
    $('.user-view .fade-background').hide();
    $('.user-view .edit-value-wrapper').hide();
    $('.user-view .invalid-password').hide();
  });

  $('.user-view #edit-sales-points-button').click(function(event){

    event = event || window.event // cross-browser event

    if (event.stopPropagation)
    {
      // W3C standard variant
      event.stopPropagation()
    }
    else
    {
      // IE variant
      event.cancelBubble = true
    }

    var sales_value = $('.user-view #sales-points-value').attr('data-value');
    $('.user-view #edit-sales-points-input').val(sales_value);
    $('.user-view .fade-background').show();
    $('.user-view #edit-sales-points-wrapper').show();

    // hide the other forms
    $('#edit-bonus-points-wrapper').hide();
    $('#edit-commission-wrapper').hide();
    $('.edit-value-wrapper .invalid-password').hide();
    $('#edit-earnings-wrapper').hide();
  });

  $('.user-view #edit-bonus-points-button').click(function(event){

    event = event || window.event // cross-browser event

    if (event.stopPropagation)
    {
    // W3C standard variant
    event.stopPropagation()
    }
    else
    {
    // IE variant
    event.cancelBubble = true
    }

    var bonus_value = $('.user-view #bonus-points-value').attr('data-value');
    $('.user-view #edit-bonus-points-input').val(bonus_value);
    $('.user-view .fade-background').show();
    $('.user-view #edit-bonus-points-wrapper').show();

    // hide the other forms
    $('#edit-sales-points-wrapper').hide();
    $('#edit-commission-wrapper').hide();
    $('.edit-value-wrapper .invalid-password').hide();
    $('#edit-earnings-wrapper').hide();
  });

  $('.user-view #edit-commission-button').click(function(event){

    event = event || window.event // cross-browser event

    if (event.stopPropagation)
    {
    // W3C standard variant
    event.stopPropagation()
    }
    else
    {
    // IE variant
    event.cancelBubble = true
    }

    var commission_value = $('.user-view #commission-value').attr('data-value');
    $('.user-view #edit-commission-input').val(commission_value);
    $('.user-view #edit-commission-password').val('');
    $('.user-view .fade-background').show();
    $('.user-view #edit-commission-wrapper').show();

    $('#edit-sales-points-wrapper').hide();
    $('#edit-bonus-points-wrapper').hide();
    $('.edit-value-wrapper .invalid-password').hide();
    $('#edit-earnings-wrapper').hide();
  });

  $('.user-view #edit-earnings-button').click(function(event){

    event = event || window.event // cross-browser event

    if (event.stopPropagation)
    {
    // W3C standard variant
    event.stopPropagation()
    }
    else
    {
    // IE variant
    event.cancelBubble = true
    }

    var earnings_value = $('.user-view #earnings-value').attr('data-value');
    $('.user-view #edit-earnings-input').val(earnings_value);
    $('.user-view #edit-earnings-password').val('');
    $('.user-view .fade-background').show();
    $('.user-view #edit-earnings-wrapper').show();

    $('#edit-sales-points-wrapper').hide();
    $('#edit-bonus-points-wrapper').hide();
    $('#edit-commission-wrapper').hide();
    $('.edit-value-wrapper .invalid-password').hide();
  });


  $(".user-view #link-to-master").click(function(){
      $(this).hide();
      $(".user-view #link-to-master-form").show();
  });

  $('.user-view .unlink-pk').click(function(e){
      e.preventDefault();
      var href = $(this).attr('href');
      var confirmed = confirm('Are you sure you want to unlink this ?');
      if (confirmed) {
          window.location = href;
      }
  });

  /* END   User View */

  /* START Admin */

  $( ".admin #tabs" ).tabs();

  /* END  Admin */

  /* START Blog Post Add */
  if(jQuery('.blog-post-add .date').length>0){
    jQuery('.blog-post-add .date').fdatepicker({format: 'yyyy-mm-dd'});
  }
  /* END   Blog Post Add */

  /* START Checkout */

  if($(".checkout #shipping_country").val()=="United States") {
    $(".checkout .state-wrapper").slideDown();
    $(".checkout #shipping_state").removeAttr("required");
  } else {
    $(".checkout .state-wrapper").slideUp();
    $(".checkout #shipping_state").attr("required");
    $(".checkout #shipping_state").val("");
  }

  $(".checkout #shipping_country").change(function(){
    if($(".checkout #shipping_country").val()=="United States") {
      $(".checkout .state-wrapper").slideDown();
      $(".checkout #shipping_state").removeAttr("required");
    } else {
      $(".checkout .state-wrapper").slideUp();
      $(".checkout #shipping_state").attr("required");
      $(".checkout #shipping_state").val("");
    }
  });

  /* END Checkout */

  /* START Coupons*/

  $('.coupons .show-reason').click(function(){
      var id = $(this).attr('value');
      $('.coupons #reason-'+id).slideToggle('slow');
      $('.coupons #payout-reason-'+id).slideToggle('slow');
  });

  // hide multiple discount layout
  var layout_height = $(".coupons #multiple-discount-wrapper").height();
  $(".coupons #multiple-discount-hide").css('height', layout_height+40+"px");
  $(".coupons #multiple-discount-hide").css('margin-bottom', "-"+layout_height-20+"px");
  $(".coupons #multiple-discount-hide").css('margin-left', "-20px");
  $(".coupons #multiple-discount-hide").css('padding-top', layout_height/2-35+"px");

  $(".coupons #read-more").click(function(){
      $(".coupons #read-more-content").slideToggle(500);
  });

  /* END   Coupons */

  /* START Event Add */
    if(jQuery('.event-add .date').length>0) {
      jQuery('.event-add .date').fdatepicker({format: 'yyyy-mm-dd'});
    }
  /* END   Event Add */

  /* START Ipad */
  $(document).on('change','.ipad #city',function(){
      var city = this.value;
      if (city == "other") {
          var input = "<input type='text' name='city' placeholder='city' />";
          $('.ipad #city-wrapper').html(input);
      } else {
          changeSchoolsByCity(this.value);
      }
  });

  $(document).on('change','.ipad #school',function(){
    var school = this.value;
    if (school == "other") {
      var input = "<input type='text' name='school' placeholder='school' />";
      $('.ipad #school-wrapper').html(input);
      $('.ipad #school-wrapper').show();
    }
  });

  $(document).on('change','.ipad #state',function(){
    var state = this.value;
    changeCitiesByCountry(236, state);
  });

  $('.ipad #clickable-1').click(function(){
      $('.ipad #file1').trigger('click');
  });

  $('.ipad #clickable-2').click(function(){
      $('.ipad #file2').trigger('click');
  });

  $('.ipad input[name=check_school]').change(function(){
      var value = this.value;
      if(value == "Yes") {
        $('.ipad #school-wrapper').slideDown('slow');
      } else {
        $('.ipad #school-wrapper').slideUp('slow');
      }
  });

  $('.ipad #file1').change(function(){
      getImage('file1', 'file1-image');
  });

  $('.ipad #file2').change(function(){
      getImage('file2', 'file2-image');
  });

  $(document).on('change','.ipad #country',function(){
      var split = this.value.split('_');
      var id_country = split[0];
      var name       = split[1];

      checkUSA(id_country);

      if(id_country == 236){
        var state = $(".ipad #state").val();
        changeCitiesByCountry(id_country, state);
      } else {
        changeCitiesByCountry(id_country, null);
      }

      changeSchoolsByCountry(id_country);

      changeCountryPrefix(name);
  });

  /* END   Ipad */

  /* START Main */
  if($('.main .tipsy').length>0) {
    $('.main .tipsy').tipsy({gravity: 's'});
  }

  $('.main #clear-all-notifications').click(function(){

      $(this).css('cursor', 'progress');

      clearAllNotifications();
  });

  $('.main .read-notification').click(function(e){
      e.preventDefault();

      var id_notification = $(this).attr('value');
      var href = $(this).attr('href');
      setInactiveNotification(id_notification, function(){
          window.location = href;
      });
  });

  $('.main .delete-notification').click(function(){
      var id_notification = $(this).attr('value');
      setInactiveNotification(id_notification, function(){
          window.location.reload();
      });
  });

  /* END   Main */

  /* START Notifications History */

  $('.notifications-history .read-notification').click(function(e){
      e.preventDefault();

      var id_notification = $(this).attr('value');
      var href = $(this).attr('href');
      setInactiveNotification(id_notification, function(){
          window.location = href;
      });
  });

  $('.notifications-history .read').click(function(e){
      e.preventDefault();

      var id_notification = $(this).attr('value');
      var href = $(this).attr('href');
      setInactiveNotification(id_notification, function(){
          window.location = href;
      });
  });

  /* END   Notifications History */

  /* START Payouts */

  $('.payouts .show-reason').click(function(){
      var id = $(this).attr('value');
      $('.payouts #reason-'+id).slideToggle('slow');
      $('.payouts #payout-reason-'+id).slideToggle('slow');
  });

  $('.payouts #change-paypal').click(function(){
      $('.payouts #change-paypal-form').slideToggle('slow');
  });

  $('.payouts .amount-type').hide();
  $('.payouts .percent-type').val("");
  $('.payouts .amount-type').val("");

  $('.payouts #discount-type').change(function(){
      var value = $(this).val();
      if (value == 'percent') {
          $('.payouts .payouts .amount-type').hide();
          $('.payouts .percent-type').show();

          $('.payouts .amount-type').val("");
      }
      else if (value == 'amount') {
          $('.payouts .percent-type').hide();
          $('.payouts .amount-type').show();

          $('.payouts .percent-type').val("");
      }
  });

  $(".payouts #get-paid").click(function(event){
      event.preventDefault();
      if ($(".payouts #giftcard-checkbox").is(":checked")) {
        if (confirm("Are you sure you want this payment as a Giftcard?")) {
            $(".payouts #payment-form").submit();
        }
      } else if($(".payouts #paypal-checkbox").is(":checked")) {
        if (confirm("Are you sure you want this payment to your Paypal account?")) {
            $(".payouts #payment-form").submit();
        }
      }
  });

  /* END   Payouts */

  /* START Profile */

  // $(".profile #fb-choose-profile").click(function(){
  //   $(".profile .fade-background").show();
  //   $(".profile #fb-accounts").show();
  // });
  //
  // $('.profile .fade-background').click(function(){
  //   $(this).hide();
  //   $(".profile #fb-accounts").hide();
  // });


  /* Registration facebook linking */
  $(" .choose-facebook-registration").click(function(event){
    event = event || window.event // cross-browser event

    if (event.stopPropagation)
    {
        // W3C standard variant
        event.stopPropagation()
    }
    else
    {
        // IE variant
        event.cancelBubble = true
    }
    $(" #fb-accounts").show();
  });

  $('body').click(function(){
    $(" #fb-accounts").hide();

    // hide edit poinst, commission and earnings forms
    $('.fade-background').hide();
    $('#edit-sales-points-wrapper').hide();
    $('#edit-bonus-points-wrapper').hide();
    $('#edit-commission-wrapper').hide();
    $('.edit-value-wrapper .invalid-password').hide();
    $('#edit-earnings-wrapper').hide();
  });

  $('.edit-value-wrapper').click(function(event){
      event = event || window.event // cross-browser event

      if (event.stopPropagation)
      {
          // W3C standard variant
          event.stopPropagation()
      }
      else
      {
          // IE variant
          event.cancelBubble = true
      }
  });

  /* END Profile */


  /* START Register */

  $(document).on('change','.register #city',function(){
      var city = this.value;
      if(city == "other") {
          var input = "<input type='text' name='city' placeholder='city' />";
          $('.register #city-wrapper').html(input);
      } else {
          changeSchoolsByCity(this.value);
      }
  });
  $(document).on('change','.register #school',function(){
      var school = this.value;
      if(school == "other") {
          var input = "<input type='text' name='school' placeholder='school' />";
          $('.register #school-wrapper').html(input);
          $('.register #school-wrapper').show();
      }
  });
  $(document).on('change','.register #state',function(){
      var state = this.value;
      changeCitiesByCountry(236, state);
  });
  $('.register #clickable-1').click(function() {
    $('.register #file1').trigger('click');
  });
  $('.register #clickable-2').click(function() {
      $('.register #file2').trigger('click');
  });
  $('.register #file1').change(function() {
      getImage('file1', 'file1-image');
  });
  $('.register #file2').change(function() {
      getImage('file2', 'file2-image');
  });
  $(document).on('change','.register #country',function(){
    var split = this.value.split('_');
    var id_country = split[0];
    var name = split[1];
    checkUSA(id_country);
    if(id_country == 236) {
        var state = $(".register #state").val();
        changeCitiesByCountry(id_country, state);
    } else {
        changeCitiesByCountry(id_country, null);
    }
    changeCitiesByCountry(id_country);
    changeSchoolsByCountry(id_country);
    changeCountryPrefix(name);
  });

  /* END Register */

  /* START Sales Team */

  var layout_height = $(".sales-team #sales-team-wrapper").height();
  $(".sales-team #sales-team-hide").css('height', layout_height+40+"px");
  $(".sales-team #sales-team-hide").css('margin-bottom', "-"+layout_height-20+"px");
  $(".sales-team #sales-team-hide").css('margin-left', "-20px");
  $(".sales-team #sales-team-hide").css('padding-top', layout_height/2-35+"px");

  /* END   Sales Team */

  /* START Social */

  $('.social #ask-for-more-networks').click(function(){
    $('.social #link-networks').hide();
    $('.social #ask-for-more-networks-wrapper').show();
  });

  $('.social #link-more').click(function(){
    $('.social #link-networks').show();
    $('.social #ask-for-more-networks-wrapper').hide();
  });

  /* END Social */

  /* START Stats */

  $(".stats #tabs-stats").tabs();

  /* END   Stats */

});
