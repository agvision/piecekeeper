jQuery(document).ready(function(){

  jQuery(document).foundation(); 

  var cal = jQuery('.fc-calendar-container').calendario();

  function activeCalendar() { 
    jQuery('.fc-calendar .fc-row > div').each(function() {
      if (jQuery(this).html() != '') jQuery(this).addClass('active-calendar-cell');
    });
  }
  function updateMonthYear() {
    jQuery('.month').html(cal.getMonthName());
  }

  if (jQuery('.fc-calendar').length) {
    updateMonthYear();
    activeCalendar();
  }

  jQuery('.prev-month').click(function() {
    cal.gotoPreviousMonth(updateMonthYear);
    activeCalendar();
  });

  jQuery('.next-month').click(function() {
    cal.gotoNextMonth(updateMonthYear);
    activeCalendar();
  });
	



	jQuery('.calendar-button').click(function(event){
		
		event.preventDefault();
		
		if (jQuery('.calendar-widget').is(':hidden')){
			
			jQuery('.calendar-widget').slideDown('slow');	
			
		} else {
			
			jQuery('.calendar-widget').hide();
		}
		
	});

	
	jQuery('.logo').mouseenter(function(){
		
	   jQuery(this).fadeTo('fast', 0.7);	
		
	}).mouseleave(function(){
		
	   jQuery(this).fadeTo('fast', 1);	
		
	});
	
	
});
