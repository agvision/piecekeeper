<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_log_model extends MY_Model
{

  	function __construct()
  	{
    	parent::__construct();
  	}

  	public function insert($request, $response, $source){
  		$this->db->set("request", $request);
  		$this->db->set("source", $source);
        $this->db->set("response", $response);

  		$this->db->set('created', 'NOW()', false);
  		$this->db->insert('api_logs');
  	}
}
