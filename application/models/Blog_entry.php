<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog_entry extends MY_Model
{

  function __construct()
  {
    parent::__construct();
    $this->table = 'blog_entry';
  }

  public function getRecentPosts($number = false){
    $id_user = $this->session->userdata('id');
    $user    = $this->users->get($id_user);
    $id_country = $user['id_country'];

  	$sql = "SELECT b.*, u.first_name, u.last_name FROM " . $this->table . " as b LEFT JOIN auth_user as u ON b.id_user = u.id WHERE b.aproved='1' AND b.type='0' AND (b.country=0 OR b.country={$id_country}) ORDER BY creation_date desc ";
  	if($number) {
  	 $items = $this->db->query($sql . " limit $number")->result();
  	} else {
  	  $items = $this->db->query($sql)->result();
  	}
  	return $items;
  }

  public function getById($id) {

    return $this->db->query("SELECT b.*, u.first_name, u.last_name FROM " . $this->table . " as b LEFT JOIN auth_user as u ON b.id_user = u.id ".
              "WHERE b.id={$id} AND b.aproved='1' LIMIT 1 ")->row();

  }

  public function getAll() {
    return $this->db->query("SELECT * FROM blog_entry ORDER BY type DESC, creation_date DESC")->result_array();
  }

  /**
   * @return image name after resize
   */
  public function uploadImage($files, $name){
      if($files[$name]['size'] > 0) {

        $path_info = pathinfo($files[$name]["name"]);
        $ext = $path_info['extension'];

        $config['upload_path'] = './images/blog/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '4096';
        $config['file_name'] = md5(time()) . "." . $ext;

        $this->load->library('upload', $config);

        if($this->upload->do_upload($name)) {
           $pic_data = $this->upload->data();

           $config2['image_library'] = 'gd2';
           $config2['source_image'] = $pic_data['full_path'];
           $config2['dest_image'] = './images/blog/';
           $config2['maintain_ratio'] = TRUE;
           $config2['master_dim'] = 'width';
           $config2['overwrite'] = TRUE;
           $config2['height'] = 1;
           $config2['width'] = 560;
           $this->load->library('image_lib');
           $this->image_lib->initialize($config2);
           $this->image_lib->resize();

           $config3['image_library'] = 'gd2';
           $config3['source_image'] = $pic_data['full_path'];
           $config3['new_image'] = './images/blog/big/';
           $config3['maintain_ratio'] = TRUE;
           $config3['master_dim'] = 'width';
           $config3['height'] = 1;
           $config3['width'] = 970;

           $this->image_lib->initialize($config3);

           if($this->image_lib->resize()) {
             return $pic_data['file_name'];
           }

        }

      }
      return false;
  }

}
