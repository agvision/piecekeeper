<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ordered_products extends MY_Model
{

  function __construct()
  {
    parent::__construct();

    $this->table = 'ordered_products';
  }

  //insert into database new product
  public function insertNewOrderedProduct($data)
  {
      $this->db->insert($this->table, $data);
  }

}
