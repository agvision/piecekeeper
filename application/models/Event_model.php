<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event_model extends MY_Model 
{

  function __construct()
  {
    parent::__construct();
    $this->table = 'event_entry';
  }
  
  public function changeStatus($id=false) {
      if(!$id) {
          $id = $this->id;
      }
      $this->db->where("id", $id);
      $this->db->set("aproved", "1-aproved", FALSE);
      $this->db->update($this->table);
      
      $notification = $this->db->query("SELECT * FROM notifications WHERE type=10 AND param=$id")->row();
      if(!$notification) {
          $event = $this->db->query("SELECT * FROM event_entry WHERE id=$id")->row();
          if(!$event) exit;
            $this->load->model('notification');
            $user_notification = $this->notification;
            // author notification
            $user_notification->id_user = $event->id_user;
            $user_notification->type = 10;
            $user_notification->param = $id;
            $user_notification->insert();

            // general notification for all the users about new event entry
            $user_notification->id_user = -1;
            $user_notification->type = 6;
            $user_notification->param = $id;
            $user_notification->insert();
      }
  }


  public function getRecentEvents($number = false){
    $sql = "SELECT e.*, u.first_name, u.last_name FROM " . $this->table . " as e ".
      "LEFT JOIN auth_user as u ON u.id=e.id_user ".
      " WHERE e.aproved='1' ORDER BY creation_date desc ";
    if($number) {
     $items = $this->db->query($sql . " limit $number")->result();
    } else {
      $items = $this->db->query($sql)->result();
    }
    
    return $items;
  }

  public function getById($id) {

    return $this->db->query("SELECT e.*, u.first_name, u.last_name FROM {$this->table}  as e 
                 LEFT JOIN auth_user as u ON u.id=e.id_user 
                 WHERE e.id={$id} AND aproved=1 LIMIT 1")->row();

  }

  /**
   * @return image name after resize
   */
  public function uploadImage($files, $name){
    
      if($files[$name]['size'] > 0) {

        $path_info = pathinfo($files[$name]["name"]);
        $ext = $path_info['extension']; 

        $config['upload_path'] = './images/event/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2048';
        $config['file_name'] = md5(time()) . "." . $ext;

        $this->load->library('upload', $config);

        if($this->upload->do_upload($name)) {
           $pic_data = $this->upload->data();
     
           $config2['image_library'] = 'gd2';
           $config2['source_image'] = $pic_data['full_path'];
           $config2['dest_image'] = './images/event/';
           $config2['maintain_ratio'] = TRUE;
           $config2['master_dim'] = 'width';
           $config2['overwrite'] = TRUE;
           $config2['height'] = 1;
           $config2['width'] = 560;
           $this->load->library('image_lib'); 
           $this->image_lib->initialize($config2);
           $this->image_lib->resize();

           $config3['image_library'] = 'gd2';
           $config3['source_image'] = $pic_data['full_path'];
           $config3['new_image'] = './images/event/big/';
           $config3['maintain_ratio'] = TRUE;
           $config3['master_dim'] = 'width';
           $config3['height'] = 1;
           $config3['width'] = 970;

           $this->image_lib->initialize($config3);

           if($this->image_lib->resize()) {
             return $pic_data['file_name'];
           }

        }

      }
      return false;
  }

}

