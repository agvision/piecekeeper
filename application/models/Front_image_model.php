<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front_image_model extends MY_Model 
{

  	function __construct()
  	{
    	parent::__construct();
  	}

  	public function insert($image_name){
		$data = array(
	 		'name' => $image_name
	  	);

	  	$this->db->insert('front_images',$data);
  	}

  	public function delete($id_image){
  		$this->db->where('id', $id_image);
  		$this->db->delete('front_images');
  	}

  	public function getAll(){
  		$images = $this->db->get('front_images')->result();
  		return $images;
  	}

  	public function getRandom(){
  		$images = $this->getAll();
  		if(count($images)){
  			$key = array_rand($images);
  			return $images[$key]->name;
  		}
  		return false;
  	}

  	public function extractFiles($files){
  		$imgs = array();
  		foreach ($files as $key => $data) {
  			foreach ($data as $i => $value) {
  				$imgs['image'.$i][$key] = $value;
  			}
  		}
  		return $imgs;
  	}

  	/**
  	 * @return image name after resize
  	 */
  	public function uploadImage($files, $name){
  	    if($files[$name]['size'] > 0) {

  	      $_FILES = $files;

  	      $path_info = pathinfo($files[$name]["name"]);
  	      $ext = $path_info['extension']; 

  	      $config['upload_path'] = './images/front/';
  	      $config['allowed_types'] = 'gif|jpg|png';
  	      $config['max_size'] = '8096';
  	      $config['file_name'] = md5(time()) . "." . $ext;

  	      $this->load->library('upload', $config);
  	      $data = $this->upload->do_upload($name);

  	      if($this->upload->do_upload($name)) {
  	         $pic_data = $this->upload->data();
  	   
  	         $config2['image_library'] = 'gd2';
  	         $config2['source_image'] = $pic_data['full_path'];
  	         $config2['dest_image'] = './images/front/';
  	         $config2['maintain_ratio'] = TRUE;
  	         $config2['master_dim'] = 'width';
  	         $config2['overwrite'] = TRUE;
  	         $config2['height'] = 1;
  	         $config2['width'] = 1920;
  	         $this->load->library('image_lib');

  	         $this->image_lib->initialize($config2);

  	         if($this->image_lib->resize()) {
  	           return $pic_data['file_name'];
  	         }

  	      }

  	    }
  	    return false;
  	}
}