<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mission_model extends MY_Model
{

  function __construct()
  {
    parent::__construct();

    $this->load->model('points_model', 'Points');
    $this->load->model('email_model', 'Email');
    $this->load->model('notification', 'Notification');
  }


  public function getAll($id_user, $sort='') {
    $user = $this->users->get($id_user);

    $missions = $this->db->query("SELECT * FROM missions AS m WHERE (m.country={$user['id_country']} OR m.country=0) AND (m.city='{$user['city']}' OR m.city='') AND ((NOW() BETWEEN m.from AND m.to) OR m.countdown=0) AND m.deleted=0 ORDER BY m.created DESC")->result();

    $results = array();

    $i = 0;
    foreach ($missions as $m) {
      $submitted = $this->db->query("SELECT * FROM missions_users WHERE id_mission={$m->id} AND id_user={$id_user}")->row();
      $result = false;

      // check missions types

      if ($m->fb_share || $m->tw_share || $m->pin_share) {
          $m->is_sharing_mission = true;
      } else {
          $m->is_sharing_mission = false;
      }

      if ($m->allow_text || $m->allow_link || $m->allow_image) {
          $m->is_post_mission = true;
      } else {
          $m->is_post_mission = false;
      }

      if ($m->sales_goals) {
          $m->is_sale_mission = true;
      } else {
          $m->is_sale_mission = false;
      }


      switch ($sort) {
        case 'user':
          if (count($submitted)) {
            $result = clone $m;
            $result->remaining = $this->getRemaining($m->to);
            $result->submitted = true;
            $result->status    = $submitted->status;
            $result->fb_shared = $submitted->fb_shared;
            $result->tw_shared = $submitted->tw_shared;
            $result->pin_shared = $submitted->pin_shared;
            $result->details   = $submitted->details;
          }
          break;

        case 'completed':
          if (count($submitted) && $submitted->status == 1) {
            $result = clone $m;
            $result->remaining = $this->getRemaining($m->to);
            $result->submitted = true;
            $result->status    = $submitted->status;
            $result->fb_shared = $submitted->fb_shared;
            $result->tw_shared = $submitted->tw_shared;
            $result->pin_shared = $submitted->pin_shared;
            $result->details   = $submitted->details;
          }
          break;

        case 'waiting':
          if (count($submitted) && $submitted->status == 0) {
            $result = clone $m;
            $result->remaining = $this->getRemaining($m->to);
            $result->submitted = true;
            $result->status    = $submitted->status;
            $result->fb_shared = $submitted->fb_shared;
            $result->tw_shared = $submitted->tw_shared;
            $result->pin_shared = $submitted->pin_shared;
            $result->details   = $submitted->details;
          }
          break;

          case 'uncompleted':
            if (count($submitted)) {
              if ($submitted->status == -1) {
                $result = clone $m;
                $result->remaining = $this->getRemaining($m->to);
                $result->submitted = true;
                $result->status    = $submitted->status;
                $result->fb_shared = $submitted->fb_shared;
                $result->tw_shared = $submitted->tw_shared;
                $result->pin_shared = $submitted->pin_shared;
                $result->details   = $submitted->details;
              }
            } else {
              $result = clone $m;
              $result->remaining  = $this->getRemaining($m->to);
              $result->submitted  = false;
              $result->fb_shared  = false;
              $result->tw_shared  = false;
              $result->pin_shared = false;
            }
            break;

        default:
          if (count($submitted)) {
            $result = clone $m;
            $result->remaining = $this->getRemaining($m->to);
            $result->submitted = true;
            $result->status    = $submitted->status;
            $result->fb_shared = $submitted->fb_shared;
            $result->tw_shared = $submitted->tw_shared;
            $result->pin_shared = $submitted->pin_shared;
            $result->details   = $submitted->details;
          } else {
            $result = clone $m;
            $result->remaining = $this->getRemaining($m->to);
            $result->submitted = false;
            $result->fb_shared  = false;
            $result->tw_shared  = false;
            $result->pin_shared = false;
          }
          break;
      }

      if ($result) {
        if ($result->pin_share) {
            $result->pin_image = base_url('/pinterest/check_share?s='.$result->id.'**'.$id_user.'**'.$result->pin_image);
        }
        $results[$i] = $result;
        $i++;
      }
    }

    // filter mission by pk level if not Admin
    if(intval($user['level']) < 2){
        foreach ($results as $key => $m) {
            if(strlen($m->pk_levels)){
                $levels = explode(",", $m->pk_levels);
                if(!in_array($user['level'], $levels)){
                    unset($results[$key]);
                }
            }
        }
    }

    return $results;
  }


  public function getRemaining($expire) {
    $all = round((strtotime($expire) - time()) / 60);
    $d = floor ($all / 1440);
    $h = floor (($all - $d * 1440) / 60);
    $m = $all - ($d * 1440) - ($h * 60);

    return array('days'=>$d, 'hours'=>$h, 'mins'=>$m);
  }


  public function getParticipants($id_mission) {
      $participants = $this->db->query("SELECT * FROM missions_users AS mu INNER JOIN auth_user AS u ON u.id=mu.id_user INNER JOIN country_t AS c ON c.country_id=u.id_country WHERE id_mission={$id_mission}")->result();

      return $participants;
  }


  public function getById($id_mission) {
      return $this->db->query("SELECT * FROM missions WHERE id={$id_mission}")->row();
  }


  public function getResponse($id_mission, $id_user) {
      return $this->db->query("SELECT * FROM missions_users WHERE id_mission={$id_mission} AND id_user={$id_user}")->row();
  }


  public function reject($id_mission, $id_user, $details) {
      $data = array(
          "status" => -1,
          "details" => $details
        );

      $this->db->where(array('id_mission' => $id_mission, 'id_user' => $id_user));
      $this->db->update('missions_users', $data);

      $this->Notification->id_user = $id_user;
      $this->Notification->type = 13;
      $this->Notification->insert();

      $user = $this->users->get($id_user);
      $this->Email->sendRejectedMission($user['email']);

      $this->db->where(array('id_mission' => $id_mission, 'id_user' => $id_user));
      return $this->db->get('missions_users')->row()->id;
  }

  public function approve($id_mission, $id_user) {
      $mission = $this->getById($id_mission);

      $submitted = $this->db->query("SELECT * FROM missions_users WHERE id_mission={$id_mission} AND id_user={$id_user}")->result();

      if (count($submitted)) {
        $m = $submitted[0];
        if ($m->status != 1) {
            $this->Points->insert($id_user, $mission->points, 2, date("Y-m-d H:i:s", time()), false, '1_'.$id_mission);
        }
      } else {
          $this->Points->insert($id_user, $mission->points, 2, date("Y-m-d H:i:s", time()), false, '1_'.$id_mission);
      }

      if (count($submitted)) {
          $data = array('status' => 1);
          $this->db->where(array('id_mission' => $id_mission, 'id_user' => $id_user));
          $this->db->update('missions_users', $data);
          $id_submission = $submitted[0]->id;
      } else {
          $data = array('status' => 1, 'id_user' => $id_user, 'id_mission' => $id_mission, 'submitted' => date("Y-m-d H:i:s", time()));
          $this->db->insert('missions_users', $data);
          $id_submission = $this->db->insert_id();
      }

      $this->Notification->id_user = $id_user;
      $this->Notification->type = 12;
      $this->Notification->param = $mission->points;
      $this->Notification->insert();

      $user = $this->users->get($id_user);
      $this->Email->sendApprovedMission($user['email']);

      return $id_submission;
  }

  public function approved($id_mission, $id_user) {
      $data = $this->db->query("SELECT * FROM missions_users WHERE id_user={$id_user} AND id_mission={$id_mission} AND status=1")->result();

      return count($data);
  }


  /**
   * @return image name after resize
   */
  public function uploadImage($files, $name){
      if(isset($files[$name]) && $files[$name]['size'] > 0) {

        $path_info = pathinfo($files[$name]["name"]);
        $ext = $path_info['extension'];

        $config['upload_path'] = './images/missions/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2048';
        $config['file_name'] = md5(time()) . "." . $ext;

        $this->load->library('upload', $config);

        if($this->upload->do_upload($name)) {
           $pic_data = $this->upload->data();

           $config2['image_library'] = 'gd2';
           $config2['source_image'] = $pic_data['full_path'];
           $config2['dest_image'] = './images/missions/';
           $config2['maintain_ratio'] = TRUE;
           $config2['master_dim'] = 'width';
           $config2['overwrite'] = TRUE;
           $config2['height'] = 1;
           $config2['width'] = 970;
           $this->load->library('image_lib');

           $this->image_lib->initialize($config2);

           if($this->image_lib->resize()) {
             return $pic_data['file_name'];
           }

        }

      }
      return false;
  }

  public function logSocialShare($id_mission, $id_user, $network) {
      if(!in_array($network, array("fb", "tw", "pin"))){
          return;
      }

      $mission = $this->getById($id_mission);
      $submitted = $this->db->query("SELECT * FROM missions_users WHERE id_mission={$id_mission} AND id_user={$id_user}")->row();

      if($submitted){
          $this->db->set($network."_shared", 1);
          $this->db->where("id", $submitted->id);
          $this->db->update("missions_users");
      } else {
          $this->db->set("id_user", $id_user);
          $this->db->set("id_mission", $id_mission);
          $this->db->set($network."_shared", 1);
          $this->db->set("status", -2);
          $this->db->set("submitted", date("Y-m-d H:i:s", time()));
          $this->db->insert("missions_users");
      }
  }

  public function isShareCompleted($id_mission, $id_user){
      $mission = $this->getById($id_mission);
      $submitted = $this->db->query("SELECT * FROM missions_users WHERE id_mission={$id_mission} AND id_user={$id_user}")->row();

      if($mission->fb_share && !$submitted->fb_shared){
          return false;
      }

      if($mission->tw_share && !$submitted->tw_shared){
          return false;
      }

      if($mission->pin_share && !$submitted->pin_shared){
          return false;
      }

      return true;
  }

  public function getSalesMissions() {
      return $this->db->query("SELECT * FROM missions WHERE sales_goals=1 AND deleted=0")->result();
  }

  public function isShareMission($id_mission) {
      $mission = $this->getById($id_mission);

      if ($mission->fb_share || $mission->tw_share || $mission->pin_share) {
          return true;
      }

      return false;
  }

  public function isPostMission($id_mission) {
      $mission = $this->getById($id_mission);

      if ($mission->allow_text || $mission->allow_link || $mission->allow_image) {
          return true;
      }

      return false;
  }

  public function isPostTextMission($id_mission) {
      $mission = $this->getById($id_mission);

      if ($mission->allow_text) {
          return true;
      }

      return false;
  }

  public function isPostLinkMission($id_mission) {
      $mission = $this->getById($id_mission);

      if ($mission->allow_link) {
          return true;
      }

      return false;
  }

  public function isPostImageMission($id_mission) {
      $mission = $this->getById($id_mission);

      if ($mission->allow_image) {
          return true;
      }

      return false;
  }


  public function isSaleMission($id_mission) {
      $mission = $this->getById($id_mission);

      if ($mission->sales_goals) {
          return true;
      }

      return false;
  }

}
