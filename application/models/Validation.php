<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Validation extends MY_Model
{

    private $fields_list;
    private $errors;

    function __construct()
    {
        parent::__construct();

        $this->fields_list = array(
            'phone' => array(
                'model' => 'Order',
                'field' => 'phone'
            )
        );

        $this->errors = array();

        if(!defined("AUTH_CODE")) define('AUTH_CODE', 'LpSTuC9o6S');
        if(!defined("DATE_DB")) define('DATE_DB', 'Y-m-d H:i:s');

    }

    private function validateByAPI($field, $value)
    {
        $page = 'https://www.onepiece.ro/en-ro/shop/api/validate';

        $options = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false
        );

        $parameters = array(
            'auth'  => AUTH_CODE,
            'model' => $this->fields_list[$field]['model'],
            'fields['.$field.']' => $value
        );

        $response = $this->curl->simple_post($page, $parameters, $options);
        $response = json_decode($response);

        if($response->status == 0)
        {
            $this->errors[] = $response->error;
            return false;
        }
        return true;
    }

    public function validate($field_name, $field_value = false)
    {
        $valid = true;
        if(is_array($field_name) && empty($field_value))
        {
            foreach($field_name as $field => $value)
            {
                $valid = $valid && $this->validateByAPI($field, $value);
            }

        }
        else
        {
            $valid = $valid && $this->validateByAPI($field_name, $field_value);
        }

        return $valid;

    }

    public function getErrors()
    {
        return $this->errors;
    }




}
?>