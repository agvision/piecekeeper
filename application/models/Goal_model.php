<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Goal_model extends MY_Model
{

  function __construct()
  {
      parent::__construct();

      $this->load->model('points_model', 'Points');
  }

  public function checkAge($id_user,$requiredAge,$requiredDate){

  	if($requiredAge != 0 && !empty($requiredDate)){
  		$dateOfBirth = $this->db->query("select dob from auth_user where id like '{$id_user}'" )->row()->dob;
  		$required      = new DateTime($requiredDate);
  		$birthday = new DateTime($dateOfBirth);
  		$interval = $required->diff($birthday);
  		$yearsDifference = $interval->format('%y');
  		if($yearsDifference - $requiredAge >= 0){
  			return true;
  		}
  		return false;
  	}
  	return true;
  }


  public function calculateMinDob($requiredAge , $requiredDate){
  	$required  = new DateTime($requiredDate);
  	$dateString = "P{$requiredAge}Y";
  	$minDob = $required->sub(new DateInterval($dateString));
  	return  $minDob->format('Y-m-d');
  }

  public function getAllForUser($id_user) {
      $user = $this->users->get($id_user);

      // $goals = $this->db->query("SELECT g.*, c.short_name FROM goals AS g LEFT JOIN country_t AS c ON g.country=c.country_id WHERE deadline>NOW() AND deleted=0 ORDER BY created DESC")->result();
      $goals = $this->db->query("SELECT g.*, c.short_name FROM goals AS g LEFT JOIN country_t AS c ON g.country=c.country_id WHERE deleted=0 ORDER BY created DESC")->result();

      $results = array();

      $i = 0;
      foreach ($goals as $g) {
          $result = false;

          $g->expired = false;
          if (strtotime($g->deadline) < time()) {
            $g->expired = true;
          }

          $min_dob = $this->calculateMinDob($g->required_age, $g->required_date);

          if (strlen($g->city) != 0 && $g->city == $user['city']) {
              $result = clone $g;
              $result->title = "<b>CITY GOAL:</b> ".$g->points." total points within ".date('Y-m-d', strtotime($g->deadline));
              $result->rank  = $this->users->getCityRanking($id_user);
              if (!$g->expired) {
                $result->goal_rank = $this->users->getGoalCityRanking($id_user,$min_dob);
              }
              $result->rank_label = "YOUR CITY RANK";
              $result->invited_label = $g->city;
              $result->earned_points = $this->Points->getTotalByCity($g->city, $g->created, $g->deadline);
          }

          elseif (intval($g->country) != 0 && $g->country == $user['id_country']) {
              $result = clone $g;
              $result->title = "<b>COUNTRY GOAL:</b> ".$g->points." total points within ".date('Y-m-d', strtotime($g->deadline));
              $result->rank  = $this->users->getCountryRanking($id_user);
              if (!$g->expired) {
                $result->goal_rank = $this->users->getGoalCountryRanking($id_user,$min_dob);
              }
              $result->rank_label = "YOUR COUNTRY RANK";
              $result->invited_label = $g->short_name;
              $result->earned_points = $this->Points->getTotalByCountry($g->country, $g->created, $g->deadline);
          }

          elseif (strlen($g->city) == 0 && intval($g->country) == 0) {
              $result = clone $g;
              $result->title = "<b>GLOBAL GOAL:</b> ".$g->points." total points within ".date('Y-m-d', strtotime($g->deadline));
              $result->rank  = $this->users->getGlobalRanking($id_user);
              if (!$g->expired) {
                $result->goal_rank = $this->users->getGoalGlobalRanking($id_user,$min_dob);
              }
              $result->rank_label = "YOUR GLOBAL RANK";
              $result->invited_label = "Globally";
              $result->earned_points = $this->Points->getTotalGlobally($g->created, $g->deadline);
          }

          if ($result) {
        			if(!$this->checkAge($id_user,$g->required_age,$g->required_date)){
        				$result->rank_status       = "You are to young";
        				$result->rank_status_class = "rank-bottom-danger";
        			} else {
                if (isset($result->goal_rank) && $result->goal_rank <= $g->invited) {
                    $result->rank_status       = "You are currently winner!";
                    $result->rank_status_class = "rank-bottom-success";
                } else {
                    $result->rank_status       = "Your ranking is too low";
                    $result->rank_status_class = "rank-bottom-danger";
                }
        			}

              if ($result->expired) {
                  $result->rank_status       = "EXPIRED";
                  $result->rank_status_class = "rank-bottom-danger";
              }

              $min_width = 80;
              $max_width = 300;

              $progress_percent = intval(100*$result->earned_points/$result->points);
              $progress_width = intval($progress_percent*$max_width/100);

              if ($progress_width < $min_width) {
                  $progress_width = $min_width;
              }

              if ($progress_width > $max_width) {
                  $progress_width = $max_width;
              }

              $result->progress_width = $progress_width;

              // check if goal is completed
              if(intval($result->earned_points) >= intval($result->points)){
                  $result->completed = true;
              } else {
                  $result->completed = false;
              }

              $results[$i] = $result;
              $i++;
          }
      }

      return $results;
  }


  public function getWinners($id_goal,$required_age = false , $required_date = false) {

  	$min_dob = $this->calculateMinDob($required_age, $required_date);

      $goal = $this->db->query("SELECT * FROM goals WHERE id={$id_goal}")->row();

      if (strlen($goal->city) != 0) {
          $winners = $this->db->query("SELECT SUM(value) AS points, u.id, u.first_name, u.last_name, u.email FROM points AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE u.city='{$goal->city}' AND u.dob <= '{$min_dob}' GROUP BY p.id_user ORDER BY SUM(p.value) DESC LIMIT {$goal->invited}")->result();
      }

      elseif (intval($goal->country != 0)) {
          $winners = $this->db->query("SELECT SUM(value) AS points, u.id, u.first_name, u.last_name, u.email FROM points AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE u.id_country={$goal->country} AND u.dob <= '{$min_dob}' GROUP BY p.id_user ORDER BY SUM(p.value) DESC LIMIT {$goal->invited}")->result();
      }

      elseif (strlen($goal->city) == 0 && intval($goal->country) == 0) {
          $winners = $this->db->query("SELECT SUM(value) AS points, u.id, u.first_name, u.last_name, u.email, u.dob  FROM points AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE u.dob <= '{$min_dob}'  GROUP BY p.id_user  ORDER BY SUM(p.value) DESC LIMIT {$goal->invited}")->result();
      }

      return $winners;
  }

}
