<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webshop_model extends MY_Model
{

  	function __construct()
  	{
    	parent::__construct();
    	// define('AUTH_CODE', 'LpSTuC9o6S');
    	if(!defined("AUTH_CODE")) define('AUTH_CODE', '0XgXBZ5Jyf');
    	if(!defined("DATE_DB")) define('DATE_DB', 'Y-m-d H:i:s');

    	$this->load->model("api_log_model", "ApiLog");
  	}

  	public function createOrder($order){

		if(is_array($order)){

			// $page = 'http://onepiecedev.cloudapp.net/en-ro/shop/order/create';
			$page = 'https://www.onepiece.com/en-us/shop/order/create';
			$data = 'auth='.AUTH_CODE.'&order='.json_encode($order);

    		$options = array(
    			CURLOPT_RETURNTRANSFER => 1,
    			CURLOPT_FOLLOWLOCATION => true
    		);

    		$parameters = array(
    			'auth'  => AUTH_CODE,
    			'order' => json_encode($order)
    		);

    		$response = $this->curl->simple_post($page, $parameters, $options);

    		//$data = htmlspecialchars($response);

			$this->ApiLog->insert($page."?".$data, $response, "package_create");

			return $response;
		}

		return false;
  	}

  	public function getOrderById($id_order) {
  		$data = $this->curl->simple_get("http://www.onepiece.com/en-us/shop/order/get?auth=".AUTH_CODE."&orderid=".$id_order);

  		if ($data) {
  			return json_decode($data);
  		}

  		return null;
  	}

  	public function getOrdersByDateAndCoupon($start_date, $coupon, $id_user=false){
  		$maxtime = false;
  		do {
  			if($maxtime){
  				$start_date = $maxtime;
  			}
	  		$json     = $this->curl->simple_get("http://onepiece.com/en-us/shop/order/list?auth=".PRODUCTION_AUTH_CODE."&mintime=".$start_date);
	  		$response = json_decode($json);

	  		$maxtime    = $response->maxtime;
	  		$num_orders = count($response->orders);

	  		// foreach order
	  		foreach ($response->orders as $order) {
	  		    $json     = $this->curl->simple_get("http://onepiece.com/en-us/shop/order/get?auth=".PRODUCTION_AUTH_CODE."&orderid=".$order);
	  		    $response = json_decode($json);

				// get orderlines
				foreach ($response->orderlines as $ol) {

				    // get Discount Code
				    if(preg_match("/^Discount code (.*) \(.*\)$/", $ol->name, $matches)) {

				    	// check if order contains our coupon
				    	if($matches[1] == $coupon){
				    		file_put_contents("orders_result.log", $response->order_id."\n", FILE_APPEND);
				    		// check if order is already register
				    		$result = $this->db->get_where('orders_coupons', array('order_id' => $response->order_id))->result();
				    		if(!count($result)){
					    		// get PieceKeeper
					    		if(!$id_user){
					    			$result  = $this->db->query("SELECT * FROM coupons WHERE code='{$matches[1]}' AND approved=1 AND deleted=0")->row();
					    			$id_user = $result->id_creator;
					    		}

				    		    // insert used coupon
				    		    $data2 = array(
				    		        'order_id' => $response->order_id,
				    		        'coupon' => $matches[1],
				    		        'id_user' => $id_user,
				    		        'created' => date(DATE_DB, $response->created)
				    		      );
				    		    $this->db->insert("orders_coupons", $data2);
				    		}
				    	}
				    }
				}
	  		}

	  		file_put_contents("orders.log", $start_date." -> ".date("Y-m-d H:i:s", $start_date)."\n", FILE_APPEND);
  		} while ($num_orders == 100);
  	}
}
