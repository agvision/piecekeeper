<?php

class State extends MY_Model {

  function __construct()
  {
      parent::__construct();
  }

  // Return all states
  public function getStates()
  {
     return $this->db->query("SELECT * FROM states")->result();
  }

}


?>
