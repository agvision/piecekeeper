<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Entry extends CI_Model
{
  public $table;

  function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  function get_all_entries()
  {
    $query = $this->db->get($table);
    return $query->result();
  }

  function add_new_entry($name, $body)
  {
    $data = array(
      'name' => $name,
      'body' => $body
    );
    $this->db->insert($table, $data);
  }

  function delete_entry($id)
  {
    $this->db->where('id', $id);
    $this->db->delete($table);
  }
  
  public function changeBlogEntryStatus($entry_id = false) {
      $id = ($entry_id ? $entry_id : $this->id);
      $this->db->query("UPDATE blog_entry SET aproved=1-aproved WHERE id=" . $id );
      
      $notification = $this->db->query("SELECT * FROM notifications as n WHERE n.type=8 AND n.param={$id} ")->row();
      if(!$notification) {
          $entry = $this->db->query("SELECT id_user, aproved FROM blog_entry as b WHERE b.id={$id} and b.aproved=1")->row();
          if($entry) {
              $this->load->model('notification');
               $user_notification = $this->notification;
               $user_notification->id_user = $entry->id_user;
               $user_notification->type = 8;
               $user_notification->param = $id;
               $user_notification->insert();

              $user_notification->id_user = -1;
              $user_notification->type = 5;
              $user_notification->param = $id;
              $user_notification->insert();
          }
      }
  }
  
}

