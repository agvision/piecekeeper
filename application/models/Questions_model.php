<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Questions_model extends MY_Model 
{
    
    public function __construct() {
        parent::__construct();
    }


    public function add( $question ) {

        $data = array( 'question' => $question );

        $this->db->insert('questions', $data);
    }

    public function getAll() {

        $this->db->select("*");
        $this->db->from("questions");
        $this->db->order_by("question ASC");

        return $this->db->get()->result();
    }

    public function getAllByUser($id_user) {

        $this->db->select("q.id as id , q.question as question, a.id_user as id_user, a.answer as answer");
        $this->db->from("questions as q");
        $this->db->join('answers as a', 'q.id = a.id_question', "left");

        $this->db->where("a.id_user",$id_user);

        $this->db->order_by("q.question ASC");

        return $this->db->get()->result();
    }

    public function get($id) {

        $this->db->select("*");
        $this->db->from("questions");
        $this->db->where("id = '".$id."'");

        $result = $this->db->get()->result();

        return $result[0];
    }

    public function update($id, $question) {

        $data = array('question' => $question );

        $this->db->where('id', $id);
        $this->db->update('questions', $data);

    }

    public function delete( $question_id ) {

        $this->db->where('id', $question_id);
        $this->db->delete('questions');

        $this->db->where('id_question = '.$question_id );
        $this->db->delete('answers');

    }

    public function save_answer( $user_id , $question_id , $answer )
    {

        $this->db->where('id_user = '.$user_id.' AND id_question = '.$question_id );
        $this->db->delete('answers');

        $data = array( 'id_user' => $user_id ,'id_question' => $question_id , 'answer' => $answer );

        $this->db->insert('answers', $data);
    }
}

?>