<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_keys_model extends MY_Model
{

  	function __construct()
  	{
    	parent::__construct();
  	}

    // Verify if the key exist in DB
  	public function VerifyKey($request){

          // Search in DB our key
          $this->db->select('*');
          $this->db->from('api_keys');
          $this->db->where('key', $request);
          $query = $this->db->get()->result_array();

          // Test if any data was found in DB
          if(empty($query))
          {
              return false;
          }
          else
          {
              return true;
          }
  	}
}
