<?php

class Currencies_conversion_model extends CI_Model {
 
  	function __construct() {
		parent::__construct();
    	$this->table = 'currencies_conversions';
  	}

  	public function insertValue($conversion, $value){
  		if(!$this->getValue($conversion)){
  			$data = array(
  			   'conversion' => $conversion,
  			   'value' => $value
  			);

  			$this->db->set('updated', 'NOW()', false);
  			$this->db->insert($this->table, $data); 
  		}
  	}

  	public function updateValue($conversion, $value){
  		$this->db->set('updated', 'NOW()', false);
  		$this->db->set('value', $value, false);

  		$this->db->where('conversion', $conversion);
  		$this->db->update($this->table); 
  	}

  	public function getValue($conversion){
  		$this->db->where('conversion', $conversion);
  		$result = $this->db->get($this->table)->row();

  		if(count($result) == 0){
  			return false;
  		}

  		return $result->value;
  	}

  	public function getAll(){
  		return $this->db->get($this->table)->result();
  	}
  
}


?>