<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends MY_Model
{

  function __construct()
  {
    parent::__construct();

    $this->table = 'orders';
  }

  public function get($id){
    $this->db->select("*");
    $this->db->from("orders");
    $this->db->where('id',$id);
    return $this->db->get()->row();
  }

  public function getByOrderID($order_id){
    $this->db->select("*");
    $this->db->from("orders");
    $this->db->where('order_id',$order_id);
    return $this->db->get()->row();
  }

}
