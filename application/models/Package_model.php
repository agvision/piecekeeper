<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Package_model extends MY_Model
{

  	function __construct() {
    	parent::__construct();
    	$this->load->model('webshop_model', 'Webshop');
        $this->load->model('Shipping');

    }

    public function create($id_user, $type, $package_product_size){
    	$data = array(
    			'id_user' => $id_user,
    			'type'    => $type,
                'status'  => 0,
                'confirmed' => 1,
    			'package_product_size' => $package_product_size,
    			'reason'  => ''
    		);

    	$this->db->set('created', 'NOW()', false);
    	$this->db->insert('packages', $data);
    }
    /*
     * Delete a package by its ID.
     */
    public function delete($id, $order_id = FALSE)
    {
      $this->db->where('id', $id)->delete('packages');
      // Also delete the associated ordered_product with it.
      if(!empty($order_id))
      {
        $this->db->where('id', $order_id)->delete('ordered_products');
      }
    }

    public function createLevel3Package($id_user) {
        $data = array(
                'id_user' => $id_user,
                'type'    => 2
            );

        $this->db->set('created', 'NOW()', false);
        $this->db->insert('packages', $data);
    }

    public function createLevel2Package($id_user) {
        $data = array(
                'id_user' => $id_user,
                'type'    => 1
            );

        $this->db->set('created', 'NOW()', false);
        $this->db->insert('packages', $data);
    }

    public function getByIdUserAndType($id_user, $type) {
        $this->db->where(array(
                'id_user'   => $id_user,
                'type'      => $type
            ));
        $result = $this->db->get('packages')->row();

        return $result;
    }

    public function getOrderDetails($id_package){
        $package = $this->getById($id_package);

        $this->db->where("id", $package->id_order);
        return $this->db->get("ordered_products")->row();
    }

    public function isConfirmed($id_user, $type){
        $this->db->where(array(
                'id_user'   => $id_user,
                'type'      => $type,
                'confirmed' => 1
            ));
        $results = $this->db->get('packages')->result();

        return count($results) ? true : false;
    }

    //check if the package was approved
    public function isApproved($id_user, $type)
    {
        $this->db->where(array(
            'id_user'   => $id_user,
            'type'      => $type,
            'status'    => 1
        ));

        $results = $this->db->get('packages')->result();

        return count($results) ? true : false;
    }

    public function approve($id, $order_id){
        $this->db->set('status', 1);
    	$this->db->set('order_id', $order_id);
    	$this->db->where('id', $id);
    	$this->db->update('packages');
    }

    public function getById($id){
        $this->db->where('id', $id);
        return $this->db->get('packages')->row();
    }

    public function getByOrderID($order_id){
        $this->db->where('order_id', $order_id);
        return $this->db->get('packages')->row();
    }

    public function getApproved($id_user, $type){
        $this->db->where(array(
                'id_user' => $id_user,
                'type'    => $type,
                'status'  => 1
            ));

        return $this->db->get('packages')->result();
    }

    public function getReason($id){
        $this->db->where('id', $id);
        $package = $this->db->get('packages')->row();
        if(count($package)){
            return $package->reason;
        }
        return false;
    }

    public function reject($id, $reason){
        $this->db->set('reason', $reason);
    	$this->db->set('status', '-1');
    	$this->db->where('id', $id);
    	$this->db->update('packages');
    }

    public function addFreeProduct($id_user, $product){
    	$this->db->set('free_product', $product);
    	$this->db->where(array('id_user' => $id_user, 'type' => '2'));
    	$this->db->update('packages');
    }

    public function sendOrder($id_user, $products, $details){
    	$user = $this->users->get($id_user);

    	if($user['gender'] == '0'){
    		$user['gender'] = 'f';
    	} else {
    		$user['gender'] = 'm';
    	}


        $order = array(
            'currency'            => $details->currency,
            'email'               => $details->email,
            'phone'               => $details->phone,
            'gender'              => $details->gender,
            'total'               => 0,
            'shipping_first_name' => $details->shipping_first_name,
            'shipping_last_name'  => $details->shipping_last_name,
            'shipping_address_1'  => $details->shipping_address_1,
            'shipping_address_2'  => $details->shipping_address_2,
            'shipping_house_no'   => $details->shipping_house_no,
            'shipping_house_ext'  => $details->shipping_house_ext,
            'shipping_zip'        => $details->shipping_zip,
            'shipping_city'       => $details->shipping_city,
            'shipping_state'      => $details->shipping_state, // US Only
            'billing_first_name'  => $details->billing_first_name,
            'billing_last_name'   => $details->billing_last_name,
            'billing_address_1'   => $details->billing_address_1,
            'billing_address_2'   => $details->billing_address_2,
            'billing_house_no'    => $details->billing_house_no,
            'billing_house_ext'   => $details->billing_house_ext,
            'billing_zip'         => $details->billing_zip,
            'billing_city'        => $details->billing_city,
            'billing_state'       => $details->shipping_state, // US Only
            'country'             => $details->country,
            'products'            => $products
        );

        $order['shipping_id'] = $this->Shipping->getCheapestMethod($details->country);
        if(empty($order['shipping_id']))
        {
            $order['shipping_id'] = $this->Shipping->getCheapestMethod($details->country, 'no');
        }
		$response = $this->Webshop->createOrder($order);
        $response = json_decode($response);

        if((isset($response->success) && $response->success == 1) || (isset($response->status) && $response->status == 1)){
            return $response->order_id;
        }
        else
        {
            // do NOT show the SQL errors - they are too long for the div-box.
            if(strpos($response->error, "SQL") === FALSE)
            {
                // show package error details.
                $this->session->set_flashdata('package_error', $response->error);
            }
            else
            {
                if(strpos($response->error, "stock_quantity_check") !== FALSE)
                {
                    $this->session->set_flashdata('package_error', 'One or more products are out-of-stock.');
                }
            }
            return false;
        }

    }

}
