<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scratch_coupon_model extends MY_Model 
{
    
    public function __construct() {
        parent::__construct();
    }

    public function setCreatedCoupon($coupon){
        $this->db->set("created", 1);
        $this->db->where("coupon", $coupon);
        $this->db->update("scratch_coupons");
    }

    public function setCreatedBatch($batch){
        $this->db->set("created", 1);
        $this->db->where("batch", $batch);
        $this->db->update("scratch_batches");
    }

    public function getUsedCoupons($batch){
    	$this->db->where(array(
    			'batch' => $batch,
    			'used' => 1
    		));
    	return $this->db->get('scratch_coupons')->result();
    }

    public function getPendingBatches(){
        $this->db->where(array(
                'active' => 1,
                'created' => 0
            ));
        return $this->db->get('scratch_batches')->result();
    }

    public function getAllCoupons($batch){
        $this->db->where(array(
                'batch' => $batch
            ));
        return $this->db->get('scratch_coupons')->result();
    }

    public function getPendingCoupons($batch){
        $this->db->where(array(
                'batch' => $batch,
                'created' => 0
            ));
        return $this->db->get('scratch_coupons')->result();
    }

    public function countUsedCoupons($batch){
    	$this->db->where(array(
    			'batch' => $batch,
    			'used' => 1
    		));
    	return $this->db->count_all_results('scratch_coupons');
    }

    public function countAllCoupons($batch){
    	$this->db->where(array(
    			'batch' => $batch
    		));
    	return $this->db->count_all_results('scratch_coupons');
    }

    public function getBatch($batch){
        $this->db->where('batch', $batch);
        return $this->db->get('scratch_batches')->row();
    }

    public function assignBatch($id_user, $batch){
        $this->db->where(array(
                'active' => 0,
                'batch'  => $batch
            ));
        $this->db->set('id_user', $id_user);
        $this->db->set('active', 1);
        $this->db->set('activated', 'NOW()', false);
        $this->db->update('scratch_batches');
    }
}

?>