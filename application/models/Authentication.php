<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Authentication extends CI_Model  {

  function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('user_agent');
  }

  /*
   * Check if user is logged.
   */
  public function isLogged() {

    if(MAINTENANCE == TRUE) {
        if(!$this->session->userdata('test_system'))
        {
            redirect("migrated");
        }
    }

      /* Conditions for logged in. */
      if ($this->session->userdata('id') != false && is_numeric($this->session->userdata('id')) && ($this->session->userdata('id') > 0)) {
          return true;
      }

      return false;
  }


  /*
   * Redirect to mobile app
   */
  public function mobileRedirect() {
      $redirect = false;

      $link = "https://piecekeeper.onepiece.com/mobile/#/";
      $social_link = $link."social/";

      if ($this->session->userdata("oauth_redirect_url") == "profile") {
        $social_link = $link."app/profile/";
      }

      // check if user agent is a mobile device
      // deactivate this feature for api service and logout
      if ($this->agent->is_mobile() &&
          strpos($_SERVER['REQUEST_URI'], 'api') === false &&
          strpos($_SERVER['REQUEST_URI'], 'logout') === false) {

        // create custom link for Twitter oAuth callback
        if (strpos($_SERVER['REQUEST_URI'], 'twitter_callback') !== false) {
          $link = $social_link."twitter/".$_GET["oauth_token"]."/".$_GET["oauth_verifier"];
        }

        // create custom link for Instagram oAuth callback
        if (strpos($_SERVER['REQUEST_URI'], 'instagram_callback') !== false) {
          $link = $social_link."instagram/".$_GET["code"];
        }

        // create custom link for Tumblr oAuth callback
        if (strpos($_SERVER['REQUEST_URI'], 'tumblr_callback') !== false) {
          $link = $social_link."tumblr/".$_GET["oauth_token"]."/".$_GET["oauth_verifier"];
        }

        // create custom link for Youtube oAuth callback
        if (strpos($_SERVER['REQUEST_URI'], 'youtube_callback') !== false) {
          $_GET["state"] = str_replace("/", "----", $_GET["state"]);
          $_GET["code"]  = str_replace("/", "----", $_GET["code"]);
          $link = $social_link."youtube/".$_GET["state"]."/".$_GET["code"];
        }

        // create custom link for forgot password page
        if (strpos($_SERVER['REQUEST_URI'], 'forgot_password') !== false) {
          $data = explode("forgot_password/", $_SERVER['REQUEST_URI']);
          $link = "https://piecekeeper.onepiece.com/mobile/#/reset/".$data[1];
        }

          // create custom link for invitation register page
          if (strpos($_SERVER['REQUEST_URI'], 'register?i=') !== false) {
              $data = explode("register?i=", $_SERVER['REQUEST_URI']);
              $link = "https://piecekeeper.onepiece.com/mobile/#/register/".$data[1];
          }

        if ($this->isLogged()) {
          $user = $this->users->getById($this->session->userdata('id'));

          // redirect only specific levels
          if (intval($user->level) <= 1) {
            $redirect = true;
          }
        } else {
          $redirect = true;
        }
      }

      if (strpos($_SERVER['REQUEST_URI'], 'social?hash=') !== false)
      {
          $redirect = false;
      }
      if (strpos($_SERVER['REQUEST_URI'], 'pku') !== false)
      {
          $redirect = false;
      }

      if ($redirect) {
        redirect($link);
      }
  }


  /**
   * Verify if a user is recently registered.
   *
   * @return boolean [recently registered or not]
   */
  public function isRegistered() {

      if(MAINTENANCE == TRUE) {
            redirect("maintenance");
    }

      /* Conditions for logged in. */
      if ($this->session->userdata('registered_id') != false && is_numeric($this->session->userdata('registered_id')) && ($this->session->userdata('registered_id') > 0)) {
          return true;
      }

      return false;
  }

  /*
   * Check if is maintenance.
   */
  public function isMaintenance() {
        if(MAINTENANCE == TRUE) {
            if(!$this->session->userdata('test_system'))
            {
                redirect("migrated");
            }
        }
  }
  /*
  * Save the last url for future redirect
  */
  public function saveTheLastUrl()
  {

    //save the current url
    $lastUrlData = array(
      'lastUrl' => current_url()
    );

    //set the last url into session variable
    $this->session->set_userdata($lastUrlData);
  }

}
