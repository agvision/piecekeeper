<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_comment_model extends MY_Model 
{

  	function __construct()
  	{
    	parent::__construct();
  	}

  	public function insert($id_admin, $id_user, $comment, $sent_email = 0){

  		$this->db->set("id_admin", $id_admin);
      $this->db->set("id_user", $id_user);
      $this->db->set("comment", $comment);
      $this->db->set("email_sent", $sent_email);
  		$this->db->set('created', 'NOW()', false);

  		$this->db->insert('admins_comments');
  	}

    public function getByUser($id_user){
      $id_admin = $this->session->userdata('id');

      $this->db->select("c.*, u.first_name, u.last_name");
      $this->db->from("admins_comments AS c");
      $this->db->join("auth_user AS u", "u.id=c.id_admin");
      $this->db->where(array(
          "c.id_user" => $id_user,
          "c.deleted" => 0    
        ));
      $this->db->order_by("created");

      $results = $this->db->get()->result();

      foreach ($results as $key => $value) {
        if($results[$key]->id_admin == $id_admin){
          $results[$key]->delete = true;
        } else {
          $results[$key]->delete = false;
        }
      }

      return $results;
    }

    public function getById($id){
      $this->db->where("id", $id);

      return $this->db->get("admins_comments")->row();
    }

    public function delete($id){
      $this->db->set("deleted", 1);
      $this->db->where("id", $id);
      $this->db->update("admins_comments");
    }
}