<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_tag_model extends MY_Model 
{

    function __construct()
    {
    	parent::__construct();
      $this->load->model('site_config');
    }

    public function add($id_user, $tag){
      $tag = str_replace(' ', '', $tag);
  		$data = array(
  				'id_user' => $id_user,
  				'tag' => $tag
  			);

  		$this->db->where($data);
  		$tagged = $this->db->get('user_tags')->result();

  		if(!count($tagged)){
  			$this->db->insert('user_tags', $data);
  		}
    }

    public function remove($id_user, $tag){
      $tag = str_replace(' ', '', $tag);
  		$data = array(
	  			'id_user' => $id_user,
	  			'tag' => $tag
	  		);
  		$this->db->where($data);
  		$this->db->delete('user_tags');
  	}

    public function get($id_user){
      $this->db->where('id_user', $id_user);
      $data = $this->db->get('user_tags')->result();

      $tags = array();
      foreach ($data as $key => $value) {
        $tags[] = $value->tag;
      }
      return $tags;
    }

    public function getAll(){
      $settings = $this->site_config->getGeneralSettings();
      $tags     = explode(',', $settings['user_tags']);

      return $tags;
    }

}