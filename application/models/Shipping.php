<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shipping extends MY_Model
{

    private $shipping_methods;
    private $url = "http://www.onepiece.com/en-us/shop/order/getshippingmethods";

    function __construct() {
        parent::__construct();
        if(!$this->check40X($this->url))
        {
            $result = @file_get_contents($this->url);
            $this->shipping_methods = json_decode($result, true);
        }
        else
        {
            $this->shipping_methods = array();
        }
    }

    /**
     * @param $url (a string URL)
     * @return bool (TRUE if accessing $url returns 40X HTTP response code, else FALSE)
     */
    public function check40X($url)
    {
        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

        /* Get the HTML or whatever is linked in $url. */
        $response = curl_exec($handle);

        /* Check for 404 (file not found). */
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        curl_close($handle);

        if($httpCode >= 400)
        {
            return true;
        }

        return false;
    }

    /**
     * @param string $country (2-letters country name (e.g. 'no', 'us' etc.)
     * @return int (ID of cheapest shipping method, based on getshippingmethods OnePiece API endpoint)
     */
    public function getCheapestMethod($country = "", $stock_location = 'nl')
    {

        if(empty($country))
        {
            return 1;
        }
        if(isset($this->shipping_methods) && $this->shipping_methods['success'])
        {
            if(isset($this->shipping_methods['shipping_methods'][$country]) && is_array($this->shipping_methods['shipping_methods'][$country]))
            {
                $cheapest_method_price = 1000;
                $cheapest_method_id = 0;
                foreach($this->shipping_methods['shipping_methods'][$country] as $method)
                {
                    if($method['price'] < $cheapest_method_price && $method['stock_location'] == $stock_location)
                    {
                        $cheapest_method_price = $method['price'];
                        $cheapest_method_id = $method['shipping_method_id'];

                    }
                }
                return $cheapest_method_id;
            }
            else
            {
                return 1;
            }
        }
        else
        {
            return 1;
        }


    }

    public function getAllMethodsByCountry($shortname)
    {
        $methods = $this->shipping_methods['shipping_methods'][$shortname];
        if(empty($methods))
        {
            return false;
        }
        return $methods;

    }


}