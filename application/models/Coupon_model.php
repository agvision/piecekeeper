<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Coupon_model extends MY_Model
{

  private $APIKEY = "0XgXBZ5Jyf";

  function __construct()
  {
    parent::__construct();

    $this->load->model("Site_config");
    $this->load->model("api_log_model", "ApiLog");

    $this->load->library('currency');

    $this->table = 'coupons';
    if(!defined("DATE_DB")) define('DATE_DB', 'Y-m-d H:i:s');
  }

  public function getByCode($code){
      $this->db->where('code', $code);
      return $this->db->get("coupons")->row();
  }

  public function getAllCreated($ids_countries = false) {
      if ($ids_countries) {
          $query = $this->db->query("SELECT c.*, u.first_name, u.last_name FROM coupons AS c LEFT JOIN auth_user as u ON c.id_creator=u.id WHERE c.approved=1 AND c.deleted=0 AND type IN (1,2) AND u.id_country IN ($ids_countries) ORDER BY c.created DESC");
      } else {
          $query = $this->db->query("SELECT c.*, u.first_name, u.last_name FROM coupons AS c LEFT JOIN auth_user as u ON c.id_creator=u.id WHERE c.approved=1 AND c.deleted=0 AND type IN (1,2) ORDER BY c.created DESC");
      }

      $result = $query->result();

      return $result;
  }

  public function getActiveMultiuse($id_user) {
      // get an active multiuse code
      $this->db->where(array(
          'id_creator' => $id_user,
          'approved' => 1,
          'to >=' => date("Y-m-d H:i:s", time()),
          'type' => 2
        ));
      $this->db->order_by("created", "desc");
      $this->db->limit(1);
      $result = $this->db->get("coupons")->row();

      if($result) {
          // check if this code has been used at least once
          $code   = $result->code;
          $result = $this->db->get_where("orders_coupons", array("coupon" => $code))->row();

          if($result) {
              return false;
          }
          return $code;
      }

      return false;
  }

  public function createBatchCoupon($code, $percent){
      $data = array(
          'percent' => $percent,
          'code' => $code,
          'to' => date('Y-m-d H:i', strtotime(' +1 year')),
          'description' => 'Batch Code'
        );

      $params = array(
          'description' => $data['description'],
          'totime' => $data['to'],
          'code' => $data['code'],
          'percent' => $data['percent'],
          // 'days_active' => 2,
          'usage_limit' => 1
        );
      $this->sendCreateRequest($params);
  }

  public function createAutoCoupon($id_creator, $code, $type, $percent, $days){
      $data = array(
          'id_creator' => $id_creator,
          'type' => 4,
          'percent' => $percent,
          'code' => $code,
          'to' => date('Y-m-d H:i', strtotime(' +'.$days.' days')),
          'description' => 'Bulk Code',
          'approved' => 1
        );

      $params = array(
          'description' => $data['description'],
          'totime' => $data['to'],
          'code' => $data['code'],
          // 'days_active' => 2,
          'percent' => $data['percent']
        );

      if($type == "1"){
        $data['u_limit'] = 1;
        $params['usage_limit'] = 1;
      }

      $api_result = $this->sendCreateRequest( $params );

      return $this->create($data);
  }

  public function createGiftcard($id_creator, $code, $amount, $country) {

      // set currency by country
      switch ($country) {
        case '235':
          $currency = 'GBP';
          break;

        case '165':
          $currency = 'NOK';
          break;

        case '215':
          $currency = 'SEK';
          break;

        case '61':
          $currency = 'DKK';
          break;

        case '75': case '15': case '76': case '83': case '155': case '216': case '3': case '12': case '21': case '22': case '29':
        case '35': case '55': case '59': case '85': case '86': case '106': case '109': case '121': case '127': case '128': case '145':
        case '176': case '177': case '181': case '182': case '196': case '201': case '202': case '209': case '75': case '75': case '75':
          $currency = 'EUR';
          break;

        default:
          $currency = 'USD';
          break;
      }

      $amount = $this->currency->convertFromUSD($amount, $currency);

      $data = array(
          'id_creator' => $id_creator,
          'type' => 3,
          'amount' => $amount,
          'currency' => $currency,
          'code' => $code,
          'to' => date('Y-m-d H:i', strtotime(' +1 year')),
          'country' => '',
          'description' => 'Giftcard',
          'u_limit' => 1,
          'approved' => 1
        );

      $params = array(
          'description' => $data['description'],
          'totime' => $data['to'],
          'code' => $data['code'],
          'country' => $data['country'],
          'usage_limit' => $data['u_limit'],
          // 'days_active' => 2,
          'amount' => $data['amount'],
          'currency' => $data['currency']
        );

      $api_result = $this->sendCreateRequest( $params );

      if (!$api_result->status) {
         $this->session->set_flashdata(
           'error',
           "<div class='alert-box error form-alert'>".$api_result->error."</div>"
         );
         redirect('admin/coupons/add/'.$id_creator);
      }

      return $this->create($data);
  }

  public function createIpadCode($id_creator, $code, $percent){
      $data = array(
          'id_creator' => $id_creator,
          'type' => 2,
          'percent' => $percent,
          'code' => $code,
          'description' => 'iPad Registration'
        );

       $data['approved'] = 1;
       $data['to']       = date('Y-m-d H:i', strtotime('+2 weeks'));

       $params = array(
          'description' => $data['description'],
          'totime' => $data['to'],
          'code' => $data['code'],
          'days_active' => 2,
          'percent' => $data['percent']
        );

       $api_result = $this->sendCreateRequest($params);

      return $this->create($data);
  }

  public function createMultipleUse($id_creator, $code, $percent, $amount, $currency, $country, $link, $u_limit, $p_limit, $freeshipping, $admin = false, $valability = false) {

      $settings = $this->Site_config->getGeneralSettings();

      $data = array(
          'id_creator' => $id_creator,
          'type' => 2,
          'percent' => $percent,
          'amount' => $amount,
          'currency' => $currency,
          'freeshipping' => $freeshipping,
          'code' => $code,
          'link' => $link,
          'country' => $country,
          'description' => 'Discount code for multiple use',
          'u_limit' => $u_limit,
          'p_limit' => $p_limit
        );

        if ($admin) {
           $data['approved'] = 1;
           //$data['to']       = date('Y-m-d H:i', strtotime(' +36 hours'));
           if($valability)
           {
              $hours_to = $valability*24;
              $data['to'] = date('Y-m-d H:i', strtotime('+'.$hours_to.' hours'));
           }
           else
           {
              $data['to'] = date('Y-m-d H:i', strtotime('+'.$settings['multiuse_coupon_active_time'].' hours'));
           }


           $params = array(
              'description' => $data['description'],
              'totime' => $data['to'],
              'code' => $data['code'],
              'country' => $data['country'],
              'usage_limit' => $data['u_limit'],
              'product_limit' => $data['p_limit'],
              // 'days_active' => 2,
              'freeshipping' => $data['freeshipping']
            );

            if (intval($data['amount']) != 0) {
              $params['amount'] = $data['amount'];
              $params['currency'] = $data['currency'];
            } else {
              $params['percent'] = $data['percent'];
            }

           $api_result = $this->sendCreateRequest( $params );

           if (!$api_result->status) {
              $this->session->set_flashdata(
                'error',
                "<div class='alert-box error form-alert'>".$api_result->error."</div>"
              );
              redirect('admin/coupons/add/'.$id_creator);
           }
        }

      return $this->create($data);
  }

  public function createSingleUse($id_creator, $code, $country = '', $admin = false) {

      $settings = $this->Site_config->getGeneralSettings();

      $data = array(
          'id_creator' => $id_creator,
          'type' => 1,
          'percent' => $settings['default_discount_percentage'],
          'code' => $code,
          'country' => $country,
          'description' => 'Unique '.$settings['default_discount_percentage'].'% discount code for use once',
          'u_limit' => 1
        );

       $data['approved'] = 1;
       $data['to']       = date('Y-m-d H:i', strtotime('+'.$settings['singleuse_coupon_active_time'].' hours'));

      $params = array(
         'description' => $data['description'],
         'code' => $data['code'],
         'totime' => $data['to'],
         'country' => $data['country'],
         'usage_limit' => $data['u_limit'],
         // 'days_active' => 2,
         'percent' => $data['percent'],

       );

       $api_result = $this->sendCreateRequest( $params );

       if (!$api_result->status) {
          $this->session->set_flashdata(
            'error',
            "<div class='alert-box error form-alert'>".$api_result->error."</div>"
        );
          redirect('admin/coupons/add/'.$id_creator);
       }

      return $this->create($data);
  }

  public function setIpadCode($code){
    $this->db->set("ipad", 1, false);
    $this->db->where('code', $code);
    $this->db->update("coupons");
  }

  public function getIpadCode($id_user){
    $this->db->where(array(
      "id_creator" => $id_user,
      "ipad" => 1
    ));

    $result = $this->db->get("coupons");
    if($result->row()){
      return $result->row()->code;
    }

    return NULL;
  }

  public function findAllRequests($ids_countries = false){

    if ($ids_countries) {
        $query = $this->db->query("SELECT c.*, u.first_name, u.last_name FROM coupons AS c LEFT JOIN auth_user as u ON c.id_creator=u.id WHERE c.approved=0 AND c.deleted=0 AND u.id_country IN ($ids_countries) ORDER BY c.created DESC");
    } else {
        $query = $this->db->query("SELECT c.*, u.first_name, u.last_name FROM coupons AS c LEFT JOIN auth_user as u ON c.id_creator=u.id WHERE c.approved=0 AND c.deleted=0 ORDER BY c.created DESC");
    }

    $result = $query->result();

    return $result;

  }


  public function findAllMultipleCodesRequests($ids_countries = false){

    if ($ids_countries) {
        $query = $this->db->query("SELECT c.*, u.first_name, u.last_name FROM coupons AS c LEFT JOIN auth_user as u ON c.id_creator=u.id WHERE c.approved=0 AND c.deleted=0 AND type=2 AND u.id_country IN ($ids_countries) ORDER BY c.created DESC");
    } else {
        $query = $this->db->query("SELECT c.*, u.first_name, u.last_name FROM coupons AS c LEFT JOIN auth_user as u ON c.id_creator=u.id WHERE c.approved=0 AND c.deleted=0 AND type=2 ORDER BY c.created DESC");
    }

    $result = $query->result();

    return $result;

  }


  public function findAllByIdUser($id_user) {

    // $query = $this->db->get_where($this->table, array('id_creator' => $id_user, 'deleted' => '0'));
    $query = $this->db->query("SELECT * FROM {$this->table} WHERE id_creator={$id_user} ORDER BY created DESC");
    return $query->result();
  }

  public function findAllActiveByIdUser($id_user) {

    $query = $this->db->query("SELECT * FROM coupons WHERE id_creator=".$id_user." AND approved=1 AND coupons.to>='".date("Y-m-d H:i:s", time())."' AND ( type = 1 OR TYPE = 2 ) ORDER BY created DESC");
    return $query->result();

  }

  public function getApprovedItems($id_user) {

    $query = $this->db->get_where($this->table,
      array('id_creator' => $id_user,
      'deleted' => '0', 'approved' => '1'
      ));

    return $query->result();
  }

  public function getActiveItems($id_user) {

    $date_type1 = date('Y-m-d 00:00:00', strtotime('-2 day'));
    $date_type2 = date('Y-m-d 00:00:00', strtotime('-365 day'));

    $query = $this->db->query("SELECT * FROM {$this->table} WHERE deleted='0' AND approved='1' AND id_creator='$id_user'
       AND ( (type='1' AND created>'$date_type1' ) OR (type='2' AND created > '$date_type2' ) )
      ");

    return $query->result();
  }

  public function getUserItems($id_user) {

    $date_type1 = date('Y-m-d 00:00:00', strtotime('-2 day'));
    $date_type2 = date('Y-m-d 00:00:00', strtotime('-365 day'));

    $query = $this->db->query("SELECT * FROM {$this->table} WHERE id_creator='$id_user' ORDER BY created DESC");

    return $query->result();
  }

  public function approve($id) {
      $coupon = $this->db->query("SELECT * from " . $this->table . " where id={$id}")->row();

      $data = array(
           'approved' => 1
        );

      if ($coupon->type == 1) {
          $data['to'] = date('Y-m-d H:i', strtotime('+2 weeks'));
      }
      else if($coupon->type == 2) {
          $data['to'] = date('Y-m-d H:i', strtotime('+36 hours'));
      }

      if ($coupon->type == 1) {
         $params = array(
            'description' => $coupon->description,
            'code' => $coupon->code,
            'totime' => $data['to'],
            'country' => $coupon->country,
            'usage_limit' => $coupon->u_limit,
            // 'days_active' => 2,
            'percent' => $coupon->percent,

          );
      }
      else if ($coupon->type == 2) {
          $params = array(
             'description' => $coupon->description,
             'code' => $coupon->code,
             'totime' => $data['to'],
             'country' => $coupon->country,
             'usage_limit' => $coupon->u_limit,
             'product_limit' => $coupon->p_limit,
             // 'days_active' => 2,
             'freeshipping' => $coupon->freeshipping
           );

           if (intval($coupon->amount) != 0) {
             $params['amount'] = intval($coupon->amount);
             $params['currency'] = $coupon->currency;
           } else {
             $params['percent'] = $coupon->percent;
           }
      }
      $api_result = $this->sendCreateRequest( $params );

      if (!$api_result->status) {
         $this->session->set_flashdata(
           'error',
           "<div class='alert-box error form-alert'>".$api_result->error."</div>"
         );
         redirect('admin/coupons/');
      } else {
        $this->session->set_flashdata(
           'success',
           'Coupon Approved.'
         );
      }

      $this->db->where('id', $id);
      $this->db->update($this->table, $data);

      $user = $this->users->get($coupon->id_creator);
      $this->Email->sendApprovedMultiCoupon($user['email']);

  }

  public function delete($id) {
      $coupon = $this->db->query("SELECT * from " . $this->table . " where id={$id}")->row();
      $user = $this->users->get($coupon->id_creator);
      $this->Email->sendRejectedMultiCoupon($user['email']);

      $data = array(
                     'deleted' => 1
                  );

      $this->db->where('id', $id);
      $this->db->update($this->table, $data);
  }


  public function disable($id) {
      $coupon = $this->db->query("SELECT * from " . $this->table . " where id={$id}")->row();
      $user = $this->users->get($coupon->id_creator);

      $data = array(
                     'deleted' => 1
                  );

      $this->db->where('id', $id);
      $this->db->update($this->table, $data);
  }

  public function enable($id) {
      $coupon = $this->db->query("SELECT * from " . $this->table . " where id={$id}")->row();
      $user = $this->users->get($coupon->id_creator);

      $data = array(
                     'deleted' => 0
                  );

      $this->db->where('id', $id);
      $this->db->update($this->table, $data);
  }

  public function create($data){

    $this->db->set('created', 'NOW()', FALSE);
    $this->db->insert($this->table, $data);

    return $this->db->insert_id();

  }

  public function randomString($length) {
      $alphabet = "ABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
      $pass = array(); //remember to declare $pass as an array
      $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
      for ($i = 0; $i < $length; $i++) {
          $n = rand(0, $alphaLength);
          $pass[] = $alphabet[$n];
      }
      return implode($pass); //turn the array into a string
  }

  private function sendCreateRequest($params) {

    $url = "http://onepiece.com/en-us/shop/discount/create";

    $p = array();
    $p[] = "auth=" . $this->APIKEY;

    foreach( $params as $name => $value ) {
        if($name == 'desc')
        {
            $name = 'description';
        }
        if($name == 'from')
        {
            $name = 'fromtime';
        }
        if($name == 'to')
        {
            $name = 'totime';
        }
        if($name == 'u_limit')
        {
            $name = 'usage_limit';
        }
        if($name == 'u_limit')
        {
            $name = 'usage_limit';
        }
        if($name == 'p_limit')
        {
            $name = 'product_limit';
        }
      $p[] = $name . "=" . $value;

    }

    $params_string = "?" . implode("&" , $p);
    $result = $this->curl->simple_get($url . $params_string);

    $this->ApiLog->insert($url . $params_string, $result, "create_coupon");

    return json_decode($result);

  }

  public function countOrders($coupon)
  {
    $this->db->select('*');
    $this->db->from('orders_coupons');
    $this->db->where('coupon',$coupon);

    return $this->db->count_all_results();
  }

  public function getCoupon24($id_user, $time)
  {
      return $this->db->query("SELECT COUNT(*) AS total FROM coupons WHERE type=1 AND id_creator={$id_user} AND created>='{$time}'")->row();
  }

  public function getCouponsForThisWeek($id_user, $last_week)
  {
      return $this->db->query("SELECT COUNT(*) AS total FROM coupons WHERE type=2 AND id_creator={$id_user} AND created>='{$last_week}'")->row();
  }

    public function getCouponsForCron($since = null, $limit = 250)
    {
        $half = intval($limit/2);

        if(empty($time))
        {
            $since = strtotime('-24hours');
        }

        $return = array();

        $standard_coupons = $this->db
            ->select('c.*')
            ->from('coupons AS c')
            ->join('auth_user AS u', 'c.id_creator = u.id', 'left')
            ->where('c.approved', 1)
            ->where('c.deleted !=', 1)
            ->where('c.last_check <=', date(DATE_DB, $since))
            ->where_in('c.type', array(1, 2))
            ->limit($half)
            ->order_by('c.created', 'DESC')
            ->order_by('c.last_check', 'ASC')
            //->get_compiled_select();
            ->get()->result();

        foreach($standard_coupons as $sc)
        {
            $this->db->set('last_check', 'NOW()', false);
            $this->db->where('id', $sc->id);
            $this->db->update('coupons');
        }

        $sb_limit = intval(ceil($half/100));

        $scratch_batches = $this->db->select('*')
            ->from('scratch_batches')
            ->join('auth_user AS u', 'scratch_batches.id_user = u.id', 'left')
            ->where('id_user !=', 0)
            ->where('id_user !=', null)
            ->where('u.deleted != ', 1)
            ->where('last_check <=', date(DATE_DB, $since))
            ->limit($sb_limit)
            ->order_by('activated', 'DESC')
            ->order_by('last_check', 'ASC')
            ->get()->result();
            //->get_compiled_select();

        foreach($scratch_batches as $sb)
        {
            $this->db->set('last_check', 'NOW()', false);
            $this->db->where('id', $sb->id);
            $this->db->update('scratch_batches');
        }



        $return = $standard_coupons;
        if(!empty($scratch_batches))
        {
            foreach($scratch_batches as $sb)
            {
                $scratch_coupon = $this->db->select('*')->from('scratch_coupons')->where('batch', $sb->batch)->get()->result();
                foreach($scratch_coupon as $sc)
                {
                    $sc_obj = new stdClass();
                    $sc_obj->code = $sc->coupon;
                    $sc_obj->last_check = $sb->last_check;
                    $return[] = $sc_obj;
                }
            }
        }

        return $return;
            //$this->db->query("SELECT c.*, u.first_name, u.last_name FROM coupons AS c LEFT JOIN auth_user as u ON c.id_creator=u.id WHERE c.approved=1 AND c.deleted=0 AND type IN (1,2) ORDER BY c.created DESC");
    }
}
