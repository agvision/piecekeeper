<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commission_model extends MY_Model 
{
    
    public function __construct() {
        parent::__construct();

        $this->load->model('users', 'User');
        $this->load->model('site_config', 'Config');

        $this->table = "commissions";
    }
    
    public function insert($id_user, $value, $order_id, $created, $coupon, $type) {
        $data = array(
                'id_user' => $id_user,
                'order_id' => $order_id,
                'type' => $type,
                'value' => $value,
                'coupon' => $coupon,
                'created' => $created
            );
        
        $this->db->insert($this->table, $data);
    }


    public function getValue($amount, $id_user, $slave = false){

        $user     = $this->User->get($id_user);
        $settings = $this->Config->getGeneralSettings();  

        if ($user['level'] > 0) {
            $user['level'] = 0;
        }

        // get slave commission for master 
        if ($slave) {
            $country_commission = $this->db->query("SELECT * FROM countries_commissions WHERE id_country={$user['id_country']} AND type=2")->row();
            $master             = $this->User->get($user['id_master']);

            if ($master['bonus_per_slave'] && $master['bonus_per_slave'] != '0.00') {
                $slave_commission = floatval($master['bonus_per_slave']);
            }
            else if(count($country_commission)){
                $slave_commission = floatval($country_commission->value);
            }
            else {
                $slave_commission = floatval($settings['slave_commission']);
            }

            switch ($user['level']) {
                case '-2':
                    $percent = floatval($slave_commission * $settings['level1_commission'] / 100);
                    break;
                
                case '-1':
                    $percent = floatval($slave_commission * $settings['level2_commission'] / 100);
                    break;

                case '0':
                case '1':
                default:
                    $percent = floatval($slave_commission * $settings['level3_commission'] / 100);
                    break;
            }

        // get piecekeeper commission    
        } else {
            $country_commission = $this->db->query("SELECT * FROM countries_commissions WHERE id_country={$user['id_country']} AND level={$user['level']} AND type=1")->row();

            switch ($user['level']) {
                case '-2':
                    $percent = floatval($settings['level1_commission']);
                    break;
                
                case '-1':
                    $percent = floatval($settings['level2_commission']);
                    break;

                case '0':
                case '1':
                default:
                    $percent = floatval($settings['level3_commission']);
                    break;
            }

            if (count($country_commission)) {
                $percent = $country_commission->value;
            }

            if ($user['commission'] && $user['commission'] != '0.00') {
                $percent = floatval($user['commission']);
            }
        }
        
        return number_format( ($percent / 100) * $amount, 2, '.', '' );
    }
    
}

?>