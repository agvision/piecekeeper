<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_model extends MY_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function getSalesByCoupon($code){

		$this->db->select('SUM(o.total) AS original_total');
	    $this->db->from('coupons c');
	    $this->db->join('orders_coupons oc', 'oc.coupon=c.code', 'left');
		$this->db->join('orders o', 'o.order_id=oc.order_id', 'left');
		$this->db->where('c.code', $code);
		$this->db->where('oc.checked', 1);

		$query = $this->db->get();

		return $query->result();
  }

  public function getProducts($country = 'gb')
  {
      $json = $this->curl->simple_get('https://www.onepiece.com/en-us/shop/categorytag/products:49/'.$country);
      return json_decode($json);
  }

  /**
   * Verify if the size of a product is correct.
   * @param  [string]  $size [size]
   * @return boolean       [description]
   */
  public function isValidSize($size)
  {
      if($size == 'Normal' || $size == 'Regular' || $size == 'Lightweight')
      {
          return true;
      }

      return false;
  }

}
