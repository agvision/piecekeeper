<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_config extends MY_Model
{

    public function __construct() {
        parent::__construct();
        $this->table = "payment_requests";
    }

    public function getAdminStatistics($ids_countries = false) {

        if ($ids_countries) {
            $data['pending_ambasadors'] = $this->db->query("SELECT * FROM auth_user WHERE is_active=0 AND (completed=0 or completed = 1) AND deleted=0 AND id_country IN ($ids_countries) AND (pku is null OR pku = 0)")->num_rows();
            $data['total_ambasadors'] = $this->db->query("SELECT * FROM auth_user WHERE level<2 AND is_active=1 AND completed=1 AND deleted=0  AND id_country IN ($ids_countries)")->num_rows();
            $data['total_sales'] = $this->db->query("SELECT * FROM orders_coupons AS oc INNER JOIN auth_user AS u ON u.id=oc.id_user WHERE u.id_country IN ({$ids_countries}) and u.completed = 1")->num_rows();
            $res = $this->db->query("SELECT SUM(o.transaction_amount) AS earnings FROM orders_coupons AS oc INNER JOIN orders AS o ON o.order_id=oc.order_id INNER JOIN auth_user AS u ON u.id=oc.id_user WHERE u.id_country IN ($ids_countries) and u.completed = 1")->row();
            $data['total_earnings'] = $res->earnings;
            $data['sales_points'] = $this->db->query("SELECT SUM(p.value) AS total FROM points AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE u.id_country IN ({$ids_countries}) AND p.type=1 and u.completed = 1")->row()->total;
            $data['bonus_points'] = $this->db->query("SELECT SUM(p.value) AS total FROM points AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE u.id_country IN ({$ids_countries}) AND p.type=2 and u.completed = 1")->row()->total;
            $data['total_points'] = $this->db->query("SELECT SUM(p.value) AS total FROM points AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE u.id_country IN ({$ids_countries}) and u.completed = 1")->row()->total;
            $data['commission'] = $this->db->query("SELECT SUM(c.value) AS total FROM commissions AS c INNER JOIN auth_user AS u ON u.id=c.id_user WHERE u.id_country IN ({$ids_countries}) and u.completed = 1")->row()->total;
        } else {
            $data['pending_ambasadors'] = $this->db->query("SELECT * FROM auth_user WHERE is_active=0 AND (completed=0 or completed = 1) AND deleted=0 AND (pku is null OR pku = 0)")->num_rows();
            $data['total_ambasadors'] = $this->db->query("SELECT * FROM auth_user WHERE level<2 and is_active=1 AND completed=1 AND deleted=0")->num_rows();
            $data['total_sales'] = $this->db->query("SELECT * FROM orders_coupons")->num_rows();
            $res = $this->db->query("SELECT SUM(o.transaction_amount) AS earnings FROM orders_coupons AS oc INNER JOIN orders AS o ON o.order_id=oc.order_id INNER JOIN auth_user AS u ON u.id=oc.id_user and u.completed = 1")->row();
            $data['total_earnings'] = $res->earnings;
            $data['sales_points'] = $this->db->query("SELECT SUM(p.value) AS total FROM points AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE p.type=1 and u.completed = 1")->row()->total;
            $data['bonus_points'] = $this->db->query("SELECT SUM(p.value) AS total FROM points AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE p.type=2 and u.completed = 1")->row()->total;
            $data['total_points'] = $this->db->query("SELECT SUM(p.value) AS total FROM points AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE u.completed = 1")->row()->total;
            $data['commission'] = $this->db->query("SELECT SUM(c.value) AS total FROM commissions AS c INNER JOIN auth_user AS u ON u.id=c.id_user WHERE u.completed = 1")->row()->total;
        }

        return $data;
    }

    public function getGeneralSettings() {
        $query   = $this->db->query("SELECT * FROM general_settings");
        $db_rows = $query->result();

        $settings = array();
        foreach ($db_rows as $r) {
          $settings[$r->name] = $r->value;
        }
        return $settings;
    }

}

?>
