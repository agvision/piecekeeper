<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification extends MY_Model
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('package_model', 'Package');
    $this->table = 'notifications';
    $this->param = '';
  }

  public function setLevel2($id_user){
        $this->insertData($id_user, 15, 0);
  }

  public function setLevel3($id_user){
        $this->insertData($id_user, 16, 0);
  }

  public function insertLevel2Package($id_user){
        $this->insertData($id_user, 17, 0);
  }

  public function insertLevel3Package($id_user){
        $this->insertData($id_user, 18, 0);
  }

  public function insertData($id_user, $type, $param){

    $data=array(
        'id_user' => $id_user,
        'id_admin' => intval($this->session->userdata('id')),
        'type' => $type,
        'param' => $param
    );

    $this->db->set('created', 'NOW()', false);
    $this->db->insert('notifications',$data);
  }

  public function insert() {
  	$data=array(
  	 	'id_user' => $this->id_user,
      'id_admin' => intval($this->session->userdata('id')),
      'type' => $this->type,
      'param' => $this->param
    );

    $this->db->set('created', 'NOW()', false);
    $this->db->insert('notifications',$data);
  }

  public function setInactive($id) {
     $data=array(
        'active' => 0
    );
    $this->db->update($this->table,$data, array('id' => $id));
  }

  public function getRecent($id_user, $number = false) {
    // id_user = -1 is for notifications that must appear for every user, like new blog post or new event

    $sql = "SELECT * FROM " . $this->table . " WHERE (id_user='".$id_user."' OR id_user='-1') AND active=1 ORDER BY active DESC, created DESC ";
    if($number) {
     $items = $this->db->query($sql . " limit $number")->result();
    } else {
      $items = $this->db->query($sql)->result();
    }
    $notifications = array();
    foreach($items as $db_row) {
      $n = new Notification();
      $n->id   = $db_row->id;
      $n->id_user = $db_row->id_user;
      $n->active = $db_row->active;
      $n->type = $db_row->type;
      $n->param = $db_row->param;
      $n->created    = $db_row->created;
      $notifications[] = $n;
    }
    return $notifications;
  }

  public function getPackageNotifications($id_user, $number = false) {
    // id_user = -1 is for notifications that must appear for every user, like new blog post or new event

    $sql = "SELECT * FROM " . $this->table . " WHERE (id_user='".$id_user."' OR id_user='-1') AND active=1 and type in (15, 16) ORDER BY active DESC, created DESC ";
    if($number) {
     $items = $this->db->query($sql . " limit $number")->result();
    } else {
      $items = $this->db->query($sql)->result();
    }
    $notifications = array();
    foreach($items as $db_row) {
      $n = new Notification();
      $n->id   = $db_row->id;
      $n->id_user = $db_row->id_user;
      $n->active = $db_row->active;
      $n->type = $db_row->type;
      $n->param = $db_row->param;
      $n->created    = $db_row->created;
      $notifications[] = $n;
    }
    return $notifications;
  }


  public function getHistory($id_user, $page, $limit = 20){
    $this->db->where('id_user', $id_user);
    $this->db->order_by('created', 'desc');
    $items = $this->db->get('notifications', $limit, $limit*($page-1))->result();

    $notifications = array();
    foreach($items as $db_row) {
      $n = new Notification();
      $n->id   = $db_row->id;
      $n->id_user = $db_row->id_user;
      $n->id_admin = $db_row->id_admin;
      $n->active = $db_row->active;
      $n->type = $db_row->type;
      $n->param = $db_row->param;
      $n->created    = $db_row->created;
      $notifications[] = $n;
    }
    return $notifications;
  }

  public function countAll($id_user){
    $this->db->where_in('id_user', $id_user);
    $total_items = $this->db->count_all_results('notifications');
    return $total_items;
  }

  public function countPages($id_user, $items_per_page){
    $total_items = $this->countAll($id_user);
    return ceil($total_items / $items_per_page);
  }

  public function getDetails() {
        $details = array();
        if ($this->type == 1) {
            $details['class'] = "";
            $details['label'] = "email";
            $details['text'] = "We've sent you an email.";
            $details['url'] = "";

        } elseif ($this->type == 2) {
            $details['class'] = "success";
            $details['label'] = "shipping";
            $details['text'] = "Your package has been shipped.";
            $details['url'] = "";
        } elseif ($this->type == 3) {
            $details['class'] = "success";
            $details['label'] = "sale";
            $details['url'] = base_url('/stats');

            if($this->param) {
                $data = explode(",", $this->param);
                $commission = $data[0];
                $points     = $data[1];
                $details['text'] = "You've made a sale and earned $".$commission." commission and ".$points." sales points.";
            } else {
                $details['text'] = "You've made a sale.";
            }
        } elseif ($this->type == 4) {
            $details['class'] = "success";
            $details['label'] = "referals";
            $details['text'] = "Approved UnderKeeper";
            $details['url'] = base_url('/sales_team');
        } elseif ($this->type == 5) {
            $details['class'] = "";
            $details['label'] = "news";
            $details['text'] = "New blog post";
            $details['url'] = base_url('/blog');
        } elseif ($this->type == 6) {
            $details['class'] = "";
            $details['label'] = "news";
            $details['text'] = "New Event";
            $details['url'] = base_url('/events');
        } elseif ($this->type == 7) {
            $details['class'] = "success";
            $details['label'] = "referrals";
            $details['text'] = "Approved as Master PieceKeeper";
            $details['url'] = base_url('/sales_team');
        } elseif ($this->type == 8) {
            $details['class'] = "success";
            $details['label'] = "news";
            $details['text'] = "Approved blog post";
            $details['url'] = base_url('/blog');
        } elseif ($this->type == 9) {
            $details['class'] = "success";
            $details['label'] = "payment";
            $details['text'] = "Approved Payment Request";
            $details['url'] = base_url('/payouts');
        } elseif ($this->type == 10) {
            $details['class'] = "success";
            $details['label'] = "news";
            $details['text'] = "Approved Event";
            $details['url'] = base_url('/events');
        } elseif ($this->type == 11) {
            $details['class'] = "success";
            $details['label'] = "referals";
            $details['text'] = "Approved extra UnderKeepers Request ";
            $details['url'] = base_url('/sales_team');
        } elseif ($this->type == 12) {
            $details['class'] = "success";
            $details['label'] = "mission";
            $details['text'] = "Your mission was approved. You have received ".$this->param." bonus points.";
            $details['url'] = base_url('/missions?sort=user');
        } elseif ($this->type == 13) {
            $details['class'] = "error";
            $details['label'] = "mission";
            $details['text'] = "Your mission was rejected. Check missions for details.";
            $details['url'] = base_url('/missions?sort=user');
        } elseif ($this->type == 14) {
            $details['class'] = "success";
            $details['label'] = "coupon";
            $details['text'] = "Your Discount Coupon has been approved.";
            $details['url'] = base_url('/coupons');
        } elseif ($this->type == 15) {
            $details['class'] = "success";
            $details['label'] = "gift";
            $details['text'] = "You have reached Level 2! Click here to redeem your free gift.";
            $details['url'] = base_url('/packages/gift');
        } elseif ($this->type == 16) {
            $details['class'] = "success";
            $details['label'] = "gift";
            $details['text'] = "You have reached Level 3! Click here to choose your free gift.";
            $details['url'] = base_url('/packages/free_product');
        } elseif ($this->type == 17) {
            $data = explode('_', $this->param);
            $type  = $data[0];
            $value = $data[1];

            if($type == 1){
              $points_type = "sales points";
            } elseif ($type == 2){
              $points_type = "bonus points";
            } elseif ($type == 3){
              $points_type = "commission";
              $value = "$".$value;
            } elseif ($type == 4){
              $points_type = "earnings";
              $value = "$".$value;
            }

            $details['class'] = "success";
            $details['label'] = "points";
            $details['text'] = "You have just received an extra ".$value." ".$points_type.".";
            $details['url'] = "";
        } elseif ($this->type == 18){
            $reason = $this->Package->getReason($this->param);
            $reason = wordwrap($reason, 55);
            $reason = substr($reason, 0, strpos($reason, "\n"));

            $details['class'] = "success";
            $details['label'] = "package";
            $details['text'] = "Your package has been rejected. ".'"'.$reason.' ..." '."<u>Read More</u>";
            $details['url'] = base_url('/packages/details/'.$this->param);
        }

        return $details;
    }

    public function countNew($id_user){
        $user = $this->users->get($id_user);
        $last_login = $user['last_login'];
        $data = $this->db->query("SELECT COUNT(*) AS total FROM notifications WHERE (id_user={$id_user} OR id_user='-1') AND active='1' ")->row();
        return $data->total;
    }

}
