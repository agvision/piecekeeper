<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Best_performers_model extends MY_Model
{

  	function __construct()
  	{
    	parent::__construct();
    	$this->load->model('points_model', 'Points');
   		$this->load->model('email_model', 'Email');
  	}

  	public function insert($list, $from, $to){
  		$this->db->set('list', $list);
  		$this->db->set('status', '0');
  		$this->db->set('from', $from);
  		$this->db->set('to', $to);
  		$this->db->set('created', 'NOW()', false);
  		$this->db->insert('best_performers');
  	}

  	public function reject($id, $reason=false){
		$this->db->set('status', '-1');
		$this->db->set('reason', $reason);
		$this->db->where('id', $id);
		$this->db->update('best_performers');
  	}

  	public function approve($id, $value, $approved_list){
  		$this->db->set('status', '1');
  		$this->db->where('id', $id);
  		$this->db->update('best_performers');

      // insert email report
      $id_admin = ($this->session->userdata('id') == NULL) ? FALSE : $this->session->userdata('id');
      $data = array(
              'subject' => "Best Performers",
              'body' => "Best Performers Template",
              'id_sender' => $id_admin,
              'created' => date('Y-m-d H:i:s', time())
          );
      $this->db->insert('emails_report', $data);
      $id_email = $this->db->insert_id();

  		$list = $this->db->query("SELECT * FROM best_performers WHERE id={$id}")->row();
  		$ids_users = explode(',', $list->list);

  		foreach ($ids_users as $id_user) {
          if(!in_array($id_user, $approved_list)){
              continue;
          }
          $pk = $this->users->get($id_user);
          if(!$pk){
            continue;
          }
  		    $user = $pk;
  		    $this->Points->insert($id_user, $value, 2, false, false, '3_'.$id);
  		    $this->Email->sendBestPerformers($user['email']);

          // insert receivcers into email report
          $data = array(
                  'id_email' => $id_email,
                  'id_user' => $id_user
              );
          $this->db->insert('emails_users', $data);
      }
  	}

}
