<?php

class School extends MY_Model {

  function __construct()
  {
      parent::__construct();
  }

  // Return all schools
  public function getAllSchools()
  {
     return  $this->db->query("SELECT school FROM auth_user WHERE TRIM(school) <>'' GROUP BY school ORDER BY school")->result();
  }

  // Return all schools from a state
  public function getAllSchoolsByState($id_state)
  {
      return $this->db->query("SELECT school FROM auth_user WHERE state='{$id_state}' AND TRIM(school) <>'' GROUP BY school ORDER BY school")->result();
  }

  public function getSchoolByCountry($id_country,$id_user,$user)
  {
      if (!$id_country) {
          if ($user['level'] == 2) {
              $data = $this->db->query("SELECT u.school FROM auth_user AS u INNER JOIN admins_countries AS ac ON ac.id_country=u.id_country WHERE ac.id_user={$id_user} AND TRIM(u.school) <>'' GROUP BY u.school ORDER BY u.school")->result();
          } else {
              $data = $this->db->query("SELECT school FROM auth_user WHERE TRIM(school) <>'' GROUP BY school ORDER BY school")->result();
          }
      } else {
          $data = $this->db->query("SELECT school FROM auth_user WHERE id_country={$id_country} AND TRIM(school) <>'' GROUP BY school ORDER BY school")->result();
      }

      $results = array();

      foreach ($data as $d) {
          $results[] = $d->school;
      }

      return $results;
  }

  public function getSchoolByCountryForApi($id_country)
  {

      $data = $this->db->query("SELECT school FROM auth_user WHERE id_country={$id_country} AND TRIM(school) <>'' GROUP BY school ORDER BY school")->result();

      $results = array();

      foreach ($data as $d) {
          $results[] = $d->school;
      }

      return $results;
  }

  public function getSchoolByCity($city,$id_user,$user)
  {
      if (strlen($city)) {
          $data = $this->db->query("SELECT school FROM auth_user WHERE city='{$city}' AND TRIM(school) <>'' GROUP BY school ORDER BY school")->result();
      } else {
          if ($user['level'] == 2) {
              $data = $this->db->query("SELECT u.school FROM auth_user AS u INNER JOIN admins_countries AS ac ON ac.id_country=u.id_country WHERE ac.id_user={$id_user} AND TRIM(u.school) <>'' GROUP BY u.school ORDER BY u.school")->result();
          } else {
              $data = $this->db->query("SELECT school FROM auth_user WHERE TRIM(school) <>'' GROUP BY school ORDER BY city")->result();
          }
      }
      $results = array();

      foreach ($data as $d) {
          $results[] = $d->school;
      }

      return $results;
  }

  public function getSchoolByCityForApi($city)
  {

      $data = $this->db->query("SELECT school FROM auth_user WHERE city='{$city}' AND TRIM(school) <>''GROUP BY school ORDER BY school")->result();

      $results = array();

      foreach ($data as $d) {
          $results[] = $d->school;
      }

      return $results;
  }



}


?>
