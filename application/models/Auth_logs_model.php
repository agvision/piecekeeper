<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_logs_model extends MY_Model
{
  function __construct()
  {
    parent::__construct();

    $this->load->model("Site_config");

  }

  public function blocked($ip){

    $query = $this->db->query("SELECT * FROM `auth_logs` WHERE `ip` = '".$ip."'  ORDER BY `date` DESC LIMIT 5");

    $i = 0;

    foreach ($query->result() as $row)
    {
      if($row->success == 1) return false;

      if($i == 0) $last_attempt_date = (strtotime($row->date));

      $first_attempt_date = (strtotime($row->date));

      $i++;
    }

    if( $i == 0 ) {

      return false;

    }

    if( isset($last_attempt_date) && isset($first_attempt_date)) {

      $diff = abs($last_attempt_date - $first_attempt_date);

    } else {

     return false;

    }

    $minutes = floor($diff / (60*60));

    if( ( $i == 5 ) && ( $minutes <= 60 ) && ($last_attempt_date + 60*60*24) > strtotime("now")  ) {

      return true;

    }

    return false;

  }

  public function attemptsVerification($ip){

    $one_hour_ago = strtotime('-1 hour');
    $one_hour_ago_formated = date("Y-m-d H:i:s", $one_hour_ago);
    $query = $this->db->query("SELECT * FROM `auth_logs` WHERE `ip` = '".$ip."' AND date > '".$one_hour_ago_formated."' - INTERVAL 1 HOUR ORDER BY `date` ASC");

    $i = 0;

    foreach ($query->result() as $row)
    {
        if($row->success == 1)
        {
            $i = 0;
        }

        $i++;
    }

    return $i;
  }

  public function lastLoginAttempt($ip)
  {
      $number_of_attempts = $this->attemptsVerification($ip);

      if( ( $number_of_attempts == 4 ) ) {

        return true;

      }

      return false;
  }

  public function add_attempt($ip,$success) {

    // don't add if the IP is localhost
    if( ! $this->blocked($ip) && $ip != '127.0.0.1' && $ip != 'localhost' ) {

      $this->db->query("INSERT INTO `auth_logs`(`ip`, `date`, `success`) VALUES ('".$ip."','".date("Y-m-d H:i:s")."','".$success."')");

    }

  }

}
