<?php

class Users extends CI_Model {
  function __construct()
  {
      $this->load->model('site_config', 'Settings');
      $this->load->model('email_model', 'Email');
      $this->load->model('notification', 'Notification');
      $this->load->model('package_model', 'Package');
      $this->load->model('user_tag_model', 'UserTag');
      $this->load->model('social_model', 'Social');
      $this->load->model('coupon_model', 'Coupon');
  }

  public function login($email, $password, $force_login=false)
  {
    $this->db->select('*');
    $this->db->from('auth_user');
    $this->db->where('email', $email );

    if (!$force_login) {
        $this->db->where('password', md5($password));
    }

    $query = $this->db->get()->result_array();

    if(empty($query)) {
      // not MD5 password

      $this->db->select('*');
      $this->db->from('auth_user');
      $this->db->where('email', $email );

      $query_auth = $this->db->get()->result_array();

      if(!empty($query_auth)) {

        $user = $query_auth[0];

        if ($user['deleted'] == 1) {
          return false;
        }

        if ($user['is_active'] == 0 || $user['is_active'] == -1) {
          return false;
        }

        if (!$force_login) {
          if(password_verify($password, $user['password'])) {
            return $user;
          }
        }
      }

    }
    else {
      // MD5 password

      $user = $query[0];

      if ($user['deleted'] == 1) {
        return false;
      }

      if ($user['is_active'] == 0 || $user['is_active'] == -1) {
        return false;
      }

      // change the MD5 algorithm to BCRYPT algorithm
      $this->db->set('password', password_hash($password, PASSWORD_DEFAULT));
      $this->db->where('id', $user['id']);
      $this->db->update('auth_user');

      return $user;
    }

    return false;

  }

  public function surf_login($email)
  {
    $this->db->select('*');
    $this->db->from('auth_user');
    $this->db->where('email', $email );

    $query = $this->db->get()->result_array();

    if(!empty($query))
    {
      $user = $query[0];

      if ($user['deleted'] == 1) {
        return false;
      }

      if ($user['is_active'] == 0 || $user['is_active'] == -1) {
        return false;
      }


      return $user;
    }

    return false;

  }

  public function linkToMaster($id_user, $id_master){
    $this->db->where('id', $id_user);
    $this->db->set("id_master", $id_master);
    $this->db->update("auth_user");
  }

  public function getByEmail($email){
      $this->db->where('email', $email);
      return $this->db->get('auth_user')->row();
  }

  public function getById($id_user){
      $this->db->where('id', $id_user);
      return $this->db->get('auth_user')->row();
  }

  public function getCurrency($id_user){
      $user = $this->getById($id_user);

      $this->db->where("country_id", $user->id_country);
      $country = $this->db->get("country_t")->row();

      if ($country && $country->currency) {
          return $country->currency;
      }

      return "EUR";
  }

  public function getSocialCredit($total, $format = true){
    $credit = ($total / 500) - 0.01;
    $credit = round($credit, 1);

    if($format){
      return "$".number_format($credit, 2);
    } else {
      return $credit;
    }
  }

  public function updatePackageSent($id_user, $level, $action){
    if($level == "2"){
      $field = "lvl2_pk_sent";
    } else {
      $field = "lvl3_pk_sent";
    }

    if($action == "set"){
      $this->db->set($field, "1");
    } else {
      $this->db->set($field, "0");
    }

    $this->db->where("id", $id_user);
    $this->db->update("auth_user");
  }

  public function getStatsForEmail($email){
      $user = $this->getByEmail($email);
      $id_user = $user->id;

      $stats = array();
      $stats['name']         = $user->first_name.' '.$user->last_name;
      $stats['followers']    = $this->getAllFollowers($id_user);
      $stats['total_points'] = $this->getPoints($id_user, 'total');
      $stats['commission']   = $this->getTotalCommission($id_user);
      $stats['global_rank']  = $this->getGlobalRanking($id_user);
      $stats['country_rank'] = $this->getCountryRanking($id_user);
      $stats['city_rank']    = $this->getCityRanking($id_user);

      return $stats;
  }

  public function getEarningsBetween($id_user, $from, $to){
      $from = date('Y-m-d H:i:s', strtotime($from));
      $to   = date('Y-m-d H:i:s', strtotime($to));

      $this->db->select("SUM(total) AS total");
      $this->db->from("orders AS o");
      $this->db->join("orders_coupons AS oc", "oc.order_id=o.order_id");
      $this->db->where("o.created >= '{$from}' AND o.created <= '{$to}' AND oc.id_user = {$id_user}");

      $result = $this->db->get()->result();

      if(count($result)){
        return $result[0]->total;
      }
      return 0;
  }

  public function getStartingLevel($id_user){
      $settings = $this->site_config->getGeneralSettings();
      $level_upgrade_networks = explode(",", $settings['level_upgrade_networks']);

      $upgrade_followers = 0;
      foreach ($level_upgrade_networks as $n) {
          switch ($n) {
            case 'fb':
              $upgrade_followers += (int) $this->Social->countFollowers($id_user, "facebook");
              break;

            case 'tw':
              $upgrade_followers += (int) $this->Social->countFollowers($id_user, "twitter");
              break;

            case 'in':
              $upgrade_followers += (int) $this->Social->countFollowers($id_user, "instagram");
              break;

            case 'tl':
              $upgrade_followers += (int) $this->Social->countFollowers($id_user, "tumblr");
              break;

            case 'pi':
              $upgrade_followers += (int) $this->Social->countFollowers($id_user, "pinterest");
              break;

            case 'ln':
              $upgrade_followers += (int) $this->Social->countFollowers($id_user, "linkedin");
              break;

            case 'yb':
              $upgrade_followers += (int) $this->Social->countFollowers($id_user, "youtube");
              break;

            case 'vi':
              $upgrade_followers += (int) $this->Social->countFollowers($id_user, "vine");
              break;
          }
      }

      if ($upgrade_followers > (int) $settings['level3_upgrade']) {
          return 0;
      }
      else if ($upgrade_followers > (int) $settings['level2_upgrade']) {
          return -1;
      } else {
          return -2;
      }
  }

  public function changeLevel($id_user, $level){
      $this->db->set("level", $level);
      $this->db->where("id", $id_user);
      $this->db->update("auth_user");
  }

  public function getTotalFollowers($id_user){
    return $this->Social->countFollowers($id_user);
  }

  public function getLevel3FreeProduct($id_user){
      $result = $this->db->query("SELECT * FROM packages WHERE id_user={$id_user} AND type=2 AND free_product != ''")->row();

      if(count($result)){
          return $result->free_product;
      }

      return false;
  }

  public function setLevel3FreeProduct($id_user,$sku,$size,$id_order){
      $this->db->query("UPDATE `packages` SET `free_product`='".$sku."' , `package_product_size`='".$size."' ,`id_order` = ".$id_order.", `confirmed`=1 WHERE id_user={$id_user} AND type=2 ");
  }

  public function getLevel3Package($id_user){
      $result = $this->db->query("SELECT * FROM packages WHERE id_user={$id_user} AND type=2")->row();
      return $result;
  }

  public function getLevel2Package($id_user){
      $result = $this->db->query("SELECT * FROM packages WHERE id_user={$id_user} AND type=1")->row();
      return $result;
  }

  public function setLevel2Product($id_user,$id_order){
      $this->db->query("UPDATE `packages` SET `id_order` = ".$id_order.", `confirmed`=1 WHERE id_user={$id_user} AND type=1 ");
  }

  public function getScratchBatches($id_user){
    $this->db->where('id_user', $id_user);
    return $this->db->get('scratch_batches')->result();
  }

  public function getTopUsers($limit) {
    return $this->db->query("SELECT SUM(p.value) AS total, u.*, c.iso2  FROM auth_user AS u INNER JOIN points AS p ON u.id=p.id_user INNER JOIN country_t as c ON c.short_name = u.country GROUP BY p.id_user ORDER BY total DESC LIMIT {$limit}")->result();
  }

  public function getUnderkeepersStats($id_user, $begin, $end) {
      if (!$begin) {
         $begin = strtotime("-1 year", time());
      }

      if (!$end) {
         $end = time();
      }

      $begin = date('Y-m-d G:i:s', $begin);
      $end   = date('Y-m-d G:i:s', $end);

      $stats = $this->db->query("SELECT SUM(o.total)*count(DISTINCT o.order_id)/count(*) AS sales, SUM(c.value) AS commission, u.first_name, u.last_name FROM orders_coupons AS oc INNER JOIN orders AS o ON o.order_id=oc.order_id INNER JOIN commissions AS c ON c.order_id=oc.order_id INNER JOIN auth_user AS u ON u.id=oc.id_user WHERE u.id_master={$id_user} AND oc.created>='{$begin}' AND oc.created<='{$end}' GROUP BY u.id")->result();
      $results = array();

      $i = 0;
      foreach ($stats as $s) {
          $results[$i]['sales']      = number_format($s->sales, 2, '.', '');
          $results[$i]['commission'] = number_format($s->commission, 2, '.', '');
          $results[$i]['first_name'] = $s->first_name;
          $results[$i]['last_name']  = $s->last_name;
          $i++;
      }

      return $results;
  }

  // return all orders made with their coupons
  public function getOrders($id_user) {

      $sql = "SELECT COUNT(*) AS total_sales, SUM(o.total) AS revenue, c.code, o.order_id, o.total, o.created, o.shipping_first_name, o.shipping_last_name FROM coupons AS c RIGHT JOIN orders_coupons AS oc ON oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id INNER JOIN auth_user AS u ON u.id=oc.id_user WHERE oc.id_user={$id_user}";

      $qry    = $this->db->query($sql);
      $result = $qry->result();
      return $result;
  }

  public function getSalesStats($id_user, $begin, $end) {
      if (!$begin) {
         $begin = strtotime("-1 year", time());
      }

      if (!$end) {
         $end = time();
      }

      $begin = date('Y-m-d G:i:s', $begin);
      $end   = date('Y-m-d G:i:s', $end);

      $stats = $this->db->query("SELECT o.total AS sales, c.value AS commission, oc.coupon, o.shipping_first_name, o.shipping_last_name, o.order_id, o.created FROM orders_coupons AS oc INNER JOIN orders AS o ON o.order_id=oc.order_id INNER JOIN commissions AS c ON c.order_id=oc.order_id WHERE c.id_user={$id_user} AND c.type=1 AND oc.created>='{$begin}' AND oc.created<='{$end}' ORDER BY oc.created DESC")->result();
      return $stats;
  }

  public function getTotalSales($id) {

    $sql = "SELECT SUM(total) as total FROM coupons AS c INNER JOIN orders_coupons AS oc ON oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE id_creator=".$id;
    $result = $this->db->query($sql)->result();

    return $result[0]->total;
  }

  public function getCommission($id) {

    $sql = "SELECT SUM(value) as total FROM commissions WHERE id_user=".$id;
    $result = $this->db->query($sql)->result();
    $commission = floatval($result[0]->total);

    $sql = "SELECT SUM(amount) as total FROM payment_requests WHERE id_user={$id} AND aproved != '-1'";
    $result = $this->db->query($sql)->result();
    $payment = floatval($result[0]->total);

    return $commission - $payment;
  }

  public function getTotalCommission($id) {
      $sql = "SELECT SUM(value) as total FROM commissions WHERE id_user=".$id;
      $result = $this->db->query($sql)->result();
      $commission = floatval($result[0]->total);

      return $commission;
  }

  public function getSlaves($id) {
      $slaves = $this->db->query("SELECT * FROM auth_user WHERE id_master={$id} AND level<1")->result();
      $result = array();

      foreach ($slaves as $key => $s) {
          $result[$key] = $s;
          $result[$key]->commission  = $this->getCommission($s->id);
          $result[$key]->total_sales = $this->getTotalSales($s->id);
      }

      return $result;
  }

  public function get($id)
  {
    $this->load->driver('cache');
    $cached = false;

    // get from cache
    if ($this->cache->memcached->is_supported()) {

        $user_profile_data = $this->cache->memcached->get('user_profile_data_'.$id);

        if ($user_profile_data) {
            $cached = true;
        }
    }

    if ($cached) {
        return $user_profile_data;
    }

    $this->db->select('*');
    $this->db->from('auth_user');
    $this->db->where('id', $id);
    $query = $this->db->get()->result_array();



    if(count($query)){

      // save to cache
      if ($this->cache->memcached->is_supported()) {
          $this->cache->memcached->save('user_profile_data_'.$id, $query[0], MEMCACHED_LIFETIIME);
      }

      return $query[0];
    }
  }

  public function is_admin($id)
  {
    $user = $this->get($id);

    if ($user['level'] >= 2)
      return true;
    else
      return false;
  }

  public function is_super_admin($id)
  {
    $user = $this->get($id);

    if ($user['level'] == 3)
      return true;
    else
      return false;
  }

  public function isSuperAdmin($id) {
      return $this->is_super_admin($id);
  }

  public function is_master($id) {
    $user = $this->get($id);

    if ($user['level'] >= 1)
      return true;
    else
      return false;
  }

  public function get_all($limit='', $start='')
  {
    if ($limit !== '' && $start !== '') $this->db->limit($limit, $start);
    $this->db->select('*');
    $this->db->from('auth_user');

	  if ($this->input->get('search')) { $this->db->like('first_name', $this->input->get('search')); $this->db->or_like('last_name', $this->input->get('search')); }
    $query = $this->db->get()->result_array();

    return $query;
  }

  /* return array of objects */
  public function getAll() {
      return $this->db->query("SELECT * FROM auth_user WHERE is_active=1 AND deleted=0 AND completed=1")->result();
  }

  public function count()
  {
    return $this->db->count_all('auth_user');
  }

  public function get_profile($id)
  {
    $this->db->select('*');
    $this->db->from('auth_user');
    $this->db->where('auth_user.id', $id);

    $query = $this->db->get()->result_array();

    return $query[0];
  }

  public function profile_update($id)
  {
    unset($_POST['save']);
  	unset($_POST['password-confirm']);

    $formValues = $this->input->post(NULL, TRUE);

    foreach($formValues as $key => $answer)
    {
      if( strpos($key, 'question_') !== FALSE )
      {
        $question_id = substr($key,9);

        $this->questions->save_answer( $id , $question_id , $answer );

        unset($_POST[$key]);
      }
    }

    if (isset($_POST['is_active'])) {
      if (!$_POST['is_active']) $_POST['is_active'] = 0;
    }

    if (isset($_POST['pk_level'])) {
      $_POST['level'] = intval($_POST['pk_level']);
      unset($_POST['pk_level']);
    }

    $old_pass         = $this->input->post('old-pass');
    $new_pass         = $this->input->post('new-pass');
    $new_re_pass      = $this->input->post('new-re-pass');

    unset($_POST['old-pass']);
    unset($_POST['new-pass']);
    unset($_POST['new-re-pass']);


    $data = $this->input->post();


    if( !empty($old_pass) || !empty($new_pass) || !empty($new_re_pass))
    {

      if( !$this->checkPassword( $id , $old_pass ) ) {
        $this->session->set_flashdata('changed_password', 'The old password it is not correct!');
        redirect('/profile');
      } else if ( $new_pass != $new_re_pass ) {
        $this->session->set_flashdata('changed_password', 'The new password it is not retyped correctly!');
        redirect('/profile');
      } else if ( empty($old_pass) ) {
        $this->session->set_flashdata('changed_password', 'Please enter the old password!');
        redirect('/profile');
      } else if ( empty($new_pass) ) {
        $this->session->set_flashdata('changed_password', 'Please enter the new password!');
        redirect('/profile');
      } else if ( empty($new_re_pass) ) {
        $this->session->set_flashdata('changed_password', 'Please retype the new password!');
        redirect('/profile');
      } else {
        $data['password'] =  password_hash($new_re_pass,PASSWORD_DEFAULT);
      }

    }


    $country = $this->input->post('country');
    if (strlen($country) > 0) {
      $c = explode('_', $country);
      $data['id_country'] = $c[0];
      $data['country'] = $c[1];
    }

    $data['dob'] = date('Y-m-d', strtotime($data['dob']));

    $this->db->where('id', $id);
    $this->db->update('auth_user', $data);
  }

  public function block($id, $delete_pending = false)
  {
    $this->db->where('id', $id);
    $this->db->set('deleted', 1);
    $this->db->set('is_active', 0);
    $this->db->update('auth_user');

    $user = $this->get($id);

    $this->load->model("email_model", "Email");

    if($delete_pending == true)
    {
        $this->Email->sendPiecekeeperApplicationRejected($user['email']);
    }
    else
    {
        $this->Email->blockedPiecekeeper($user);
    }
  }


  public function delete($id)
  {

    // delete commissions where id_user=
    $this->db->where('id_user', $id);
    $this->db->delete('commissions');

    // delete blog_entry where id_user=
    $this->db->where('id_user', $id);
    $this->db->delete('blog_entry');

    // delete event_entry where id_user=
    $this->db->where('id_user', $id);
    $this->db->delete('event_entry');

    // delete coupons where id_creator=
    $this->db->where('id_creator', $id);
    $this->db->delete('coupons');

    // delete missions_users where id_user=
    $this->db->where('id_user', $id);
    $this->db->delete('missions_users');

    // delete notifications where id_user=
    $this->db->where('id_user', $id);
    $this->db->delete('notifications');

    // delete payment_requests where id_user=
    $this->db->where('id_user', $id);
    $this->db->delete('payment_requests');

    // delete points where id_user=
    $this->db->where('id_user', $id);
    $this->db->delete('points');

    // delete slaves_requests where id_user=
    $this->db->where('id_user', $id);
    $this->db->delete('slaves_requests');

    // delete social_accounts where id_user=
    $this->db->where('id_user', $id);
    $this->db->delete('social_accounts');

    // delete social_followers where id_user=
    $this->db->where('id_user', $id);
    $this->db->delete('social_followers');

    // delete admins_countries where id_user=
    $this->db->where('id_user', $id);
    $this->db->delete('admins_countries');

    // delete emails_report where id_sender=
    $this->db->where('id_sender', $id);
    $this->db->delete('emails_report');

    // foreach orders_coupons where id_user=
    $orders_coupons = $this->db->query("SELECT * FROM orders_coupons WHERE id_user={$id}")->result();
    if (count($orders_coupons)) {
        foreach ($orders_coupons as $oc) {
            // delete orders where order_id = oc.order_id
            $this->db->where('order_id', $oc->order_id);
            $this->db->delete('orders');

            // delete oc
            $this->db->where('id', $oc->id);
            $this->db->delete('orders_coupons');
        }
    }

    $this->db->where('id', $id);
    $this->db->delete('auth_user');
  }

  public function unblock($id)
  {
    $this->db->where('id', $id);
    $this->db->set('deleted', 0);
    $this->db->set('is_active', 1);
    $this->db->update('auth_user');
  }

  public function approve($id)
  {
    $this->db->where('id', $id);
    $this->db->set('is_active', 1);
    $this->db->update('auth_user');

    $user = $this->users->get($id);
    $this->Email->approvedAsPiecekeeper($user['email']);
  }

  public function disapprove($id)
  {
    $this->db->where('id', $id);
    $this->db->set('is_active', -1);
    $this->db->set('completed', 0);
    $this->db->update('auth_user');
  }

  public function statistics_general()
  {

	$this->db->select('COUNT(*) AS total_ambassadors');
        $this->db->from('auth_user');
        $this->db->where('level', 0);

	$query = $this->db->get()->result_array();
	$statistics_general['total_ambassadors'] = $query[0]['total_ambassadors'];

	$this->db->flush_cache();

	$this->db->select('COUNT(*) AS total_sales, SUM(total) AS total_earnings');
        $this->db->from('sales');

	$query = $this->db->get()->result_array();
	$statistics_general['total_earnings'] = $query[0]['total_earnings'];
	$statistics_general['total_sales'] = $query[0]['total_sales'];

	$this->db->flush_cache();

	return $statistics_general;

  }

  public function register($id_master = false, $admin = false) {
      $inv = $this->input->post('invitation');
      $invitation = $this->db->query("SELECT * FROM invitations WHERE hash='{$inv}'")->row();
      if (count($invitation)) {

          $id_master = $invitation->id_user;

          $result = $this->db->query("SELECT COUNT(*) as slaves FROM auth_user WHERE id_master=".$id_master)->result();
          $slaves = $result[0]->slaves;

          $result       = $this->db->get_where('auth_user', array('id' => $id_master))->result();
          $slaves_limit = $result[0]->slaves;

          if (intval($slaves) >= intval($slaves_limit)) {
              $id_master = 0;
          } else {
              $this->db->where('id', $invitation->id);
              $this->db->set('approved', 1);
              $this->db->update('invitations');
          }
      }

      do {
          $hash = md5(time());
          $check = $this->db->query("SELECT * FROM auth_user WHERE hash='{$hash}'")->result();
      } while ( count($check) > 0);

      $phone = $this->input->post('dial_prefix').$this->input->post('phone');

      $data = array(
        'id_master' => $id_master,
        'email'=> ($this->input->post('email') == NULL) ? FALSE : $this->input->post('email'),
        'first_name'=> ($this->input->post('first_name') == NULL) ? FALSE : $this->input->post('first_name'),
        'last_name'=> ($this->input->post('last_name') == NULL) ? FALSE : $this->input->post('last_name'),
        'password' => password_hash($this->input->post('password'),PASSWORD_DEFAULT),
        'level' => -2,
        'website' => ($this->input->post('website') == NULL) ? FALSE : $this->input->post('website'),
        'address1' => ($this->input->post('address1') == NULL) ? FALSE : $this->input->post('address1'),
        'address2' => ($this->input->post('address2') == NULL) ? FALSE : $this->input->post('address2'),
        'city' => ($this->input->post('city') == NULL) ? FALSE : $this->input->post('city'),
        'postcode' => ($this->input->post('postcode') == NULL) ? FALSE : $this->input->post('postcode'),
        'phone' => $phone,
        'gender' => ($this->input->post('gender') == NULL) ? FALSE : $this->input->post('gender'),
        'school' => ($this->input->post('school') == NULL) ? FALSE : $this->input->post('school'),
        'graduation_year' => ($this->input->post('graduation_year') == NULL) ? FALSE : $this->input->post('graduation_year'),
        'celebrity' => ($this->input->post('celebrity') == NULL) ? FALSE : $this->input->post('celebrity'),
        'vacantion' => ($this->input->post('vacantion') == NULL) ? FALSE : $this->input->post('vacantion'),
        'activity' => ($this->input->post('activity') == NULL) ? FALSE : $this->input->post('activity'),
        'island' => ($this->input->post('island') == NULL) ? FALSE : $this->input->post('island'),
        'jesus' => ($this->input->post('jesus') == NULL) ? FALSE : $this->input->post('jesus'),
        'obama' => ($this->input->post('obama') == NULL) ? FALSE : $this->input->post('obama'),
        'jongun' => ($this->input->post('jongun') == NULL) ? FALSE : $this->input->post('jongun'),
        'jackson' => ($this->input->post('jackson') == NULL) ? FALSE : $this->input->post('jackson'),
        'tw_description' => ($this->input->post('tw_description') == NULL) ? FALSE : $this->input->post('tw_description'),
        'find_source' => ($this->input->post('find_source') == NULL) ? FALSE : $this->input->post('find_source'),
        'hash' => $hash,
        'terms_accepted' => 1,
        'pku' => 0
    );

    $dob = DateTime::createFromFormat('m-d-Y', $this->input->post('month').'-'.$this->input->post('day').'-'.$this->input->post('year'));
    if ($dob) {
        $dob = $dob->format('Y-m-d');
        $data['dob'] = $dob;
    }

    $country = $this->input->post('country');
    if (strlen($country) > 0) {
      $c = explode('_', $country);
      $data['id_country'] = $c[0];
      $data['country'] = $c[1];
    }

    if ($this->input->post('state')) {
        $data['state'] = $this->input->post('state');
    }

    if ($admin) {
      $data['level']     = 2;
      $data['is_active'] = 1;
    }

    $this->load->model("email_model", "Email");

    // this action is done in finish_register
    // if (!$admin) {
    //     $this->Email->newPiecekeeperEmail( $data['email'] );
    // }

    $this->db->set('date_joined', 'NOW()', false);
    $this->db->insert('auth_user', $data);

  }

  public function change_status($id) {
      $this->db->query("UPDATE auth_user SET is_active=1-is_active WHERE id=$id");

      $user = $this->get($id);

      $notification = $this->db->query("SELECT * FROM notifications WHERE param={$id} AND type=4")->row();
      if(!$notification) {
          if($user['is_active'] == 1 && $user['id_master'] != 0) {
               $this->load->model('notification');
               $user_notification = $this->notification;
               $user_notification->id_user = $user['id_master'];
               $user_notification->type = 4;
               $user_notification->param = $id;

               $user_notification->insert();
          }
      }

      $this->load->model("email_model", "Email");
      if( $user['is_active'] == 1 ) {
        $this->Email->approvedAsPiecekeeper( $user['email'] );
      } else {
          // $this->Email->disapprovedAsPiecekeeper( $user['email'] );
      }

  }

  public function getList($filters, $count_only = false, $ids_countries = false) {

      $where = array();
      $join = array();

      if(!empty($where)) {
          $wheresql = " WHERE ". implode(" AND " , $where);
      }


      $user_tags = '';
      if($filters['user-tags']){
        $tags = $filters['user-tags'];
        foreach ($tags as $tag) {
          $user_tags[] = "'".$tag."'";
        }
        $user_tags = implode(',', $user_tags);
        $user_tags = str_replace(",''", "", $user_tags);
        $user_tags = str_replace("'',", "", $user_tags);
        $user_tags = str_replace("''", "", $user_tags);
        $user_tags = str_replace(" ", "", $user_tags);
      }

      $inner_join_user_tags = '';
      $user_tags_rows = '';
      $groupby = '';
      if(strlen($user_tags)){
        $where[] = "ut.tag IN (".$user_tags.")";
        $inner_join_user_tags = "INNER JOIN user_tags AS ut ON ut.id_user=u.id";
        $user_tags_rows = ', ut.tag';
        $groupby = 'GROUP BY ut.id_user';
      }


      $where[] = "u.completed=1 AND u.is_active=1 AND u.deleted=0 ";

      if($filters['age_min']) {
          $time = strtotime("-{$filters['age_min']} year", time());
          $date = date('Y-m-d', $time);
          $where[] = "u.dob<='$date'";
      }
      if($filters['age_max']) {
          $time = strtotime("-{$filters['age_max']} year", time());
          $date = date('Y-m-d', $time);
          $where[] = "u.dob>='$date'";
      }
      if($filters['gender']) {
          $where[] = "u.gender=" . ($filters['gender']-1);
      }
      if($filters['min_sales'] || $filters['min_sales'] === '0') {

      	$and = "";
         if ($filters['min_sales_start']) {
         	  $from_date = date('Y-m-d H:i:s', strtotime($filters['min_sales_start']));
              $and = "AND o.created >= '{$from_date}' ";
         }
        if($filters['min_sales_end']) {
        	$to_date= date('Y-m-d H:i:s',strtotime('+1 day',strtotime($filters['min_sales_end'])));
            $and .= "AND o.created < '{$to_date}'";
        }
         $where[] = "(SELECT COUNT(*) FROM orders_coupons as oc INNER JOIN coupons as c ON c.code=oc.coupon INNER JOIN orders as o
             ON o.order_id=oc.order_id WHERE c.id_creator=u.id $and) = {$filters['min_sales']} ";
      }
      if($filters['activity_days']) {
          $time = strtotime("-{$filters['activity_days']} day", time());
          $date = date('Y-m-d G:i:s', $time);
          $where[] = "u.last_login>'$date'";
      }
      if($filters['no_activity_days']) {
          $time = strtotime("-{$filters['no_activity_days']} day", time());
          $date = date('Y-m-d G:i:s', $time);
          $where[] = "u.last_login<'$date'";
      }
      if($filters['min_slaves']) {
          $where[] = "(SELECT COUNT(*) FROM auth_user WHERE id_master=u.id) >= {$filters['min_slaves']}";
      }

      if($filters['level']) {
        if ($filters['level'] == -2) {
            $where[] = "u.level=-2";
        } elseif ($filters['level'] == -1) {
            $where[] = "u.level=-1";
        } elseif ($filters['level'] == 1) {
            $where[] = "u.level=1 AND u.level=2 AND u.level=3";
        }
      }

      if ($filters['level'] === "0") {
            $where[] = "u.level=0";
      }

      if ($filters['slaves']) {
          if ($filters['slaves'] == 1) {
              $where[] = "u.id_master!=0 AND u.level < 1";
          }
      }

      if ($filters['filter_country']) {
          $where[] = "u.id_country={$filters['filter_country']}";
      }

      if ($filters['filter_city']) {
          $where[] = "u.city='{$filters['filter_city']}'";
      }

      if ($filters['filter_state_arr']) {
          $where[] = "u.state IN ('".implode("','", $filters['filter_state_arr'])."')";
      }

      if ($filters['filter_school']) {
          $where[] = "u.school='{$filters['filter_school']}'";
      }

      $where[] = "u.level <= 3";

      if (count($where)) {
          $sql = "SELECT u.*$user_tags_rows FROM auth_user as u $inner_join_user_tags " . (count($where) ? "WHERE " : "") . implode(" AND ", $where).($ids_countries ? " AND id_country IN ($ids_countries)" : "");
      } else {
          $sql = "SELECT u.*$user_tags_rows FROM auth_user as u $inner_join_user_tags ". (count($where) ? "WHERE " : "") . implode(" AND ", $where).($ids_countries ? " WHERE id_country IN ($ids_countries)" : "");
      }

      $sql = $sql." ".$groupby;

      $users = $this->db->query($sql)->result();

      $selected_tags = explode(',', $user_tags);
      if(strlen($user_tags)){
        $count_selected_tags = count($selected_tags);
      } else {
        $count_selected_tags = 0;
      }

      $results = array();

      foreach ($users as $u) {
          $result = false;
          $result = clone $u;

          $count_user_tags = count($this->UserTag->get($u->id));
          if($count_selected_tags && $count_selected_tags > $count_user_tags){
            continue;
          }

          if ($filters['global_rank']) {
              $global_rank = $this->getGlobalRanking($u->id);
              if ($global_rank >= $filters['global_rank']) {
                  continue;
              }
          }

          if ($filters['country_rank']) {
              $country_rank = $this->getCountryRanking($u->id);
              if ($country_rank >= $filters['country_rank']) {
                  continue;
              }
          }

          if ($filters['city_rank']) {
              $city_rank = $this->getCityRanking($u->id);
              if ($city_rank >= $filters['city_rank']) {
                  continue;
              }
          }

          if (isset($filters['min_followers']) && $filters['min_followers']) {
              $followers = intval($this->getAllFollowers($u->id));
              if ($followers <= intval($filters['min_followers'])) {
                  continue;
              }
          }

          if (isset($filters['max_followers']) && $filters['max_followers']) {
              $followers = intval($this->getAllFollowers($u->id));
              if ($followers >= intval($filters['max_followers'])) {
                  continue;
              }
          }

          $results[] = $result;
      }

      if($count_only) return count($results);

      return $results;

  }


  public function checkAdminUserRights($id_user, $url) {
    $id_admin = $this->session->userdata('id');
    $admin    = $this->get($id_admin);
    $user     = $this->get($id_user);

    if ($admin['level'] == 2) {
        $ids_countries = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_admin}")->result();
        $rights = false;
        foreach ($ids_countries as $c) {
            if ($c->id_country == $user['id_country']) {
               $rights = true;
            }
        }

        if (!$rights) {
            redirect($url);
        }
    }
  }


  public function getPoints($id_user, $type=false, $from=false, $to=false) {
      if($from && $to){
          $sales = $this->db->query("SELECT SUM(value) AS total FROM points WHERE type=1 AND id_user={$id_user} AND created >= '{$from}' AND created <= '{$to}'")->row();
          $bonus = $this->db->query("SELECT SUM(value) AS total FROM points WHERE type=2 AND id_user={$id_user} AND created >= '{$from}' AND created <= '{$to}'")->row();
      } else {
          $sales = $this->db->query("SELECT SUM(value) AS total FROM points WHERE type=1 AND id_user={$id_user}")->row();
          $bonus = $this->db->query("SELECT SUM(value) AS total FROM points WHERE type=2 AND id_user={$id_user}")->row();
      }

      $result = array(
              'sales' => $sales->total,
              'bonus' => $bonus->total,
              'total' => $sales->total + $bonus->total
          );

      if ($type) {
          switch ($type) {
            case 'sales':
              return $result['sales'];
              break;

            case 'bonus':
              return $result['bonus'];
              break;

            case 'total':
              return $result['total'];
              break;
          }
      }

      return $result;
  }


  public function insertPoints($id_user, $value, $type, $created, $order_id = false, $bonus_type = false) {
      $data = array(
                'id_user' => $id_user,
                'value' => $value,
                'type' => $type,
                'created' => $created
            );

      if($bonus_type){
          $data['bonus_type'] = $bonus_type;
      }

        $this->db->insert('points', $data);
  }


  public function checkForLevelUpgrade($id_user) {

      // if($id_user == 557){
      //   $this->Package->create($id_user, 1);
      // } else if($id_user == 558){
      //   $this->Package->create($id_user, 2);
      // }

      $user = $this->users->get($id_user);
      $settings = $this->Settings->getGeneralSettings();

      if ($user['level'] == -2 || $user['level'] == -1) {
          $orders = $this->getOrders($id_user);
          $sales  = $orders[0]->total_sales;
          $points = $this->getPoints($id_user);
          $total_points = $points['total'];
      }

      switch ($user['level']) {
        case '-2':

          if ($sales >= $settings['sales_level_2'] && $total_points >= $settings['total_points_level_2']) {
              $this->insertPoints($id_user, $settings['bonus_points_level_2'], 2, date('Y-m-d H:i:s', time()), false, '2_2');

              $this->db->set('level', '-1', false);
              $this->db->where('id', $id_user);
              $this->db->update('auth_user');

              $this->Notification->setLevel2($id_user);
              $this->Email->sendLevel2($user['email']);
           }
          break;

        case '-1':

          if ($sales >= $settings['sales_level_3'] && $total_points >= $settings['total_points_level_3']) {
              $this->insertPoints($id_user, $settings['bonus_points_level_3'], 2, date('Y-m-d H:i:s', time()), false, '2_3');

              $this->db->set('level', '0', false);
              $this->db->where('id', $id_user);
              $this->db->update('auth_user');

              $this->Notification->setLevel3($id_user);
              $this->Email->sendLevel3($user['email']);
           }
          break;
      }
  }


  public function computeLevel($id_user) {

      $user = $this->users->get($id_user);
      $settings = $this->Settings->getGeneralSettings();

      $orders = $this->getOrders($id_user);
      $sales  = $orders[0]->total_sales;
      $points = $this->getPoints($id_user);
      $total_points = $points['total'];

      $level = "-2";

      if ($sales >= $settings['sales_level_2'] && $total_points >= $settings['total_points_level_2']) {
        $level = "-1";
      }

      if ($sales >= $settings['sales_level_3'] && $total_points >= $settings['total_points_level_3']) {
        $level = "0";
      }

      return $level;
  }

  public function updateLevel($id_user, $level){
      $this->db->set('level', $level, false);
      $this->db->where('id', $id_user);
      $this->db->update('auth_user');
  }


  public function getAllFollowers($id_user) {
      return $this->Social->countFollowers($id_user);
  }


  public function countSalesBetween($id_user, $from, $to) {
      $from = date('Y-m-d H:i:s', strtotime($from));
      $to   = date('Y-m-d H:i:s', strtotime($to));

      $data = $this->db->query("SELECT COUNT(*) AS sales FROM orders_coupons AS oc INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE id_user={$id_user} AND o.created >= '{$from}' AND o.created <= '{$to})'")->row();

      return $data->sales;
  }

  public function getGlobalClassament($start_date, $end_date, $limit=false){
      $this->db->select("u.id, u.id_country, u.first_name, u.last_name, SUM(p.value) AS total_points");
      $this->db->from("auth_user AS u");
      $this->db->join('points AS p', 'p.id_user = u.id', 'left');
      $this->db->where("p.created >= '{$start_date}' AND p.created <= '{$end_date}'");
      $this->db->group_by("u.id");
      $this->db->order_by("total_points DESC, u.date_joined ASC");
      if($limit){
        $this->db->limit($limit);
      }
      $users = $this->db->get()->result();

      return $users;
  }



  public function getGoalGlobalRanking($id_user,$min_dob){
  	$user = $this->get($id_user);

  		$points  = $this->db->query("SELECT SUM(value) AS total FROM points WHERE id_user={$id_user}")->row();
  		if (!$points->total) {
  			$points->total = 0;
  		}
  		$ranking = $this->db->query("SELECT u.id,u.dob, SUM(p.value) AS total, u.date_joined FROM points AS p RIGHT JOIN auth_user AS u ON p.id_user=u.id GROUP BY u.id HAVING total>'{$points->total}' AND u.dob <= '{$min_dob}' OR ((total='{$points->total}' OR '{$points->total}'='0') AND u.date_joined<'".$user['date_joined']."' AND u.dob <= '{$min_dob}')")->result();

  	return count($ranking)+1;
  }

  public function getGoalCountryRanking($id_user,$min_dob){
  	$user = $this->get($id_user);

  	$points  = $this->db->query("SELECT SUM(value) AS total FROM points WHERE id_user={$id_user}")->row();
          if (!$points->total) {
              $points->total = 0;
          }
          $ranking =  $this->db->query("SELECT u.id, SUM(p.value) AS total, u.* FROM points AS p RIGHT JOIN auth_user AS u ON p.id_user=u.id WHERE u.id_country={$user['id_country']} GROUP BY u.id HAVING total>'{$points->total}' AND u.dob <= '{$min_dob}' OR ((total='{$points->total}' OR '{$points->total}'='0') AND u.date_joined<'".$user['date_joined']."' AND u.dob <= '{$min_dob}')")->result();


      return count($ranking)+1;
  }


  public function getGoalCityRanking($id_user,$min_dob){
  	$user = $this->get($id_user);

  	 $points  = $this->db->query("SELECT SUM(value) AS total FROM points WHERE id_user={$id_user}")->row();
          if (!$points->total) {
              $points->total = 0;
          }
          $ranking = $this->db->query("SELECT u.id, SUM(p.value) AS total, u.* FROM points AS p RIGHT JOIN auth_user AS u ON p.id_user=u.id WHERE u.city='{$user['city']}' GROUP BY u.id HAVING total>'{$points->total}' AND u.dob <= '{$min_dob}' OR ((total='{$points->total}' OR '{$points->total}'='0') AND u.date_joined<'".$user['date_joined']."' AND u.dob <= '{$min_dob}' )")->result();

      return count($ranking)+1;
  }



  public function getGlobalRanking($id_user, $last_month = false) {
        $this->load->driver('cache');
        $user = $this->users->get($id_user);

        $cached = false;

        // get from cache
        if ($this->cache->memcached->is_supported()) {

            if ($last_month) {
                $ranking = $this->cache->memcached->get('last_month_global_ranking_'.$id_user);
            } else {
                $ranking = $this->cache->memcached->get('current_global_ranking_'.$id_user);
            }

            if ($ranking) {
                $cached = true;
            }
        }

        if ($cached) {
            return $ranking;
        }

        // construct queries
        if ($last_month) {
            $last_month = date('Y-m-d H:i:s', strtotime('-1 month'));
            $points = $this->db->query("SELECT SUM(value) AS total FROM points WHERE id_user={$id_user} AND created<='{$last_month}'")->row();
            if (!$points->total) {
                $points->total = 0;
            }
            $sql = "SELECT u.id, SUM(p.value) AS total, u.date_joined FROM points AS p RIGHT JOIN auth_user AS u ON p.id_user=u.id WHERE p.created<='{$last_month}' GROUP BY u.id HAVING total>'{$points->total}' OR ((total='{$points->total}' OR '{$points->total}'='0') AND u.date_joined<'".$user['date_joined']."')";
        } else {
            $points  = $this->db->query("SELECT SUM(value) AS total FROM points WHERE id_user={$id_user}")->row();
            if (!$points->total) {
                $points->total = 0;
            }
            $sql = "SELECT u.id, SUM(p.value) AS total, u.date_joined FROM points AS p RIGHT JOIN auth_user AS u ON p.id_user=u.id GROUP BY u.id HAVING total>'{$points->total}' OR ((total='{$points->total}' OR '{$points->total}'='0') AND u.date_joined<'".$user['date_joined']."')";
        }

        // compute if not cached
        $ranking = $this->db->query($sql)->result();

        // save to cache
        if ($this->cache->memcached->is_supported()) {
            if ($last_month) {
                $this->cache->memcached->save('last_month_global_ranking_'.$id_user, count($ranking)+1, MEMCACHED_LIFETIIME);
            } else {
                $this->cache->memcached->save('current_global_ranking_'.$id_user, count($ranking)+1, MEMCACHED_LIFETIIME);
            }
        }

        return count($ranking)+1;
  }


  public function getCountryRanking($id_user, $last_month = false) {
      $this->load->driver('cache');
      $user = $this->users->get($id_user);

      $cached = false;

      // get from cache
      if ($this->cache->memcached->is_supported()) {

          if ($last_month) {
              $ranking = $this->cache->memcached->get('last_month_country_ranking_'.$id_user);
          } else {
              $ranking = $this->cache->memcached->get('current_country_ranking_'.$id_user);
          }

          if ($ranking) {
              $cached = true;
          }
      }

      if ($cached) {
          return $ranking;
      }

      // construct queries
      if ($last_month) {
          $last_month = date('Y-m-d H:i:s', strtotime('-1 month'));
          $points = $this->db->query("SELECT SUM(value) AS total FROM points WHERE id_user={$id_user} AND created<='{$last_month}'")->row();
          if (!$points->total) {
              $points->total = 0;
          }
          $sql = "SELECT u.id, SUM(p.value) AS total, u.* FROM points AS p RIGHT JOIN auth_user AS u ON p.id_user=u.id WHERE p.created<='{$last_month}' AND u.id_country={$user['id_country']} GROUP BY u.id HAVING total>'{$points->total}' OR ((total='{$points->total}' OR '{$points->total}'='0') AND u.date_joined<'".$user['date_joined']."')";
      } else {
          $points  = $this->db->query("SELECT SUM(value) AS total FROM points WHERE id_user={$id_user}")->row();
          if (!$points->total) {
              $points->total = 0;
          }
          $sql = "SELECT u.id, SUM(p.value) AS total, u.* FROM points AS p RIGHT JOIN auth_user AS u ON p.id_user=u.id WHERE u.id_country={$user['id_country']} GROUP BY u.id HAVING total>'{$points->total}' OR ((total='{$points->total}' OR '{$points->total}'='0') AND u.date_joined<'".$user['date_joined']."')";
      }

      // compute if not cached
      $ranking = $this->db->query($sql)->result();

      // save to cache
      if ($this->cache->memcached->is_supported()) {
          if ($last_month) {
              $this->cache->memcached->save('last_month_country_ranking_'.$id_user, count($ranking)+1, MEMCACHED_LIFETIIME);
          } else {
              $this->cache->memcached->save('current_country_ranking_'.$id_user, count($ranking)+1, MEMCACHED_LIFETIIME);
          }
      }

      return count($ranking)+1;
  }


  public function getCityRanking($id_user, $last_month = false) {
      $this->load->driver('cache');
      $user = $this->users->get($id_user);

      $cached = false;

      // get from cache
      if ($this->cache->memcached->is_supported()) {

          if ($last_month) {
              $ranking = $this->cache->memcached->get('last_month_city_ranking_'.$id_user);
          } else {
              $ranking = $this->cache->memcached->get('current_city_ranking_'.$id_user);
          }

          if ($ranking) {
              $cached = true;
          }
      }

      if ($cached) {
          return $ranking;
      }

      // construct queries
      if ($last_month) {
          $last_month = date('Y-m-d H:i:s', strtotime('-1 month'));
          $points = $this->db->query("SELECT SUM(value) AS total FROM points WHERE id_user={$id_user} AND created<='{$last_month}'")->row();
          if (!$points->total) {
              $points->total = 0;
          }
          $sql = "SELECT u.id, SUM(p.value) AS total, u.* FROM points AS p RIGHT JOIN auth_user AS u ON p.id_user=u.id WHERE p.created<='{$last_month}' AND u.city='{$user['city']}' GROUP BY u.id HAVING total>'{$points->total}' OR ((total='{$points->total}' OR '{$points->total}'='0') AND u.date_joined<'".$user['date_joined']."')";
      } else {
          $points  = $this->db->query("SELECT SUM(value) AS total FROM points WHERE id_user={$id_user}")->row();
          if (!$points->total) {
              $points->total = 0;
          }
          $sql = "SELECT u.id, SUM(p.value) AS total, u.* FROM points AS p RIGHT JOIN auth_user AS u ON p.id_user=u.id WHERE u.city='{$user['city']}' GROUP BY u.id HAVING total>'{$points->total}' OR ((total='{$points->total}' OR '{$points->total}'='0') AND u.date_joined<'".$user['date_joined']."')";
      }

      // compute if not cached
      $ranking = $this->db->query($sql)->result();

      // save to cache
      if ($this->cache->memcached->is_supported()) {
          if ($last_month) {
              $this->cache->memcached->save('last_month_city_ranking_'.$id_user, count($ranking)+1, MEMCACHED_LIFETIIME);
          } else {
              $this->cache->memcached->save('current_city_ranking_'.$id_user, count($ranking)+1, MEMCACHED_LIFETIIME);
          }
      }

      return count($ranking)+1;
  }


  public function getGroupGlobalRanking($ids_users = null, $last_month = false){
    $this->load->driver('cache');

    // construct query
      $this->db->select("u.id, u.id_country, u.first_name, u.last_name, SUM(p.value) AS total_points");
      $this->db->from("auth_user AS u");
      $this->db->join('points AS p', 'p.id_user = u.id', 'left');
      $this->db->group_by("u.id");
      $this->db->order_by("total_points DESC, u.date_joined ASC");

      if ($last_month) {
        $last_month = date("Y-m-d H:i:s", strtotime("-1 month"));
        $this->db->where("p.created <= '{$last_month}' OR p.created IS NULL");
      }

      // get users list ordered by ranking
      $result = $this->db->get()->result();

      $rankings = array();
      $place = 1;

    // compute place
      foreach ($result as $r) {
          if(!$ids_users || isset($ids_users['user_'.$r->id])){
              $r->place = $place;
              $rankings['user_'.$r->id] = $r;

              if ($this->cache->memcached->is_supported()) {
                if ($last_month) {
                    $this->cache->memcached->save('last_month_global_ranking_'.$r->id, $place, MEMCACHED_LIFETIIME);
                } else {
                    $this->cache->memcached->save('current_global_ranking_'.$r->id, $place, MEMCACHED_LIFETIIME);
                }
              }

          }
          $place++;
      }

      return $rankings;
  }


  public function getGroupCountryRanking($ids_users = null, $last_month = false){
      $this->load->driver('cache');

      //construct query
      $this->db->select("u.id, u.id_country, u.first_name, u.last_name, SUM(p.value) AS total_points");
      $this->db->from("auth_user AS u");
      $this->db->join('points AS p', 'p.id_user = u.id', 'left');
      $this->db->group_by("u.id");
      $this->db->order_by("u.id_country ASC, total_points DESC, u.date_joined ASC");

      if ($last_month) {
        $last_month = date("Y-m-d H:i:s", strtotime("-1 month"));
        $this->db->where("p.created <= '{$last_month}' OR p.created IS NULL");
      }

      // get users list ordered by ranking
      $result = $this->db->get()->result();

      $users = array();
      $place = 1;

      // compute place
      foreach ($result as $key => $r) {
          if($key > 0 && $result[$key]->id_country != $result[$key-1]->id_country){
              $place = 1;
          }
          if(!$ids_users || isset($ids_users['user_'.$r->id])){
              $r->place = $place;
              $users['user_'.$r->id] = $r;

              if ($this->cache->memcached->is_supported()) {
                if ($last_month) {
                    $this->cache->memcached->save('last_month_country_ranking_'.$r->id, $place, MEMCACHED_LIFETIIME);
                } else {
                    $this->cache->memcached->save('current_country_ranking_'.$r->id, $place, MEMCACHED_LIFETIIME);
                }
              }

          }
          $place++;
      }

      return $users;
  }

  public function getGroupCityRanking($ids_users = null, $last_month = false){
      $this->load->driver('cache');

      //construct query
      $this->db->select("u.id, u.city, u.first_name, u.last_name, SUM(p.value) AS total_points");
      $this->db->from("auth_user AS u");
      $this->db->join('points AS p', 'p.id_user = u.id', 'left');
      $this->db->group_by("u.id");
      $this->db->order_by("u.city ASC, total_points DESC, u.date_joined ASC");

      if ($last_month) {
        $last_month = date("Y-m-d H:i:s", strtotime("-1 month"));
        $this->db->where("p.created <= '{$last_month}' OR p.created IS NULL");
      }

      // get users list ordered by ranking
      $result = $this->db->get()->result();

      $users = array();
      $place = 1;

      foreach ($result as $key => $r) {
          if($key > 0 && str_replace(' ', '', $result[$key]->city) != str_replace(' ', '', $result[$key-1]->city)){
              $place = 1;
          }
          if(!$ids_users || isset($ids_users['user_'.$r->id])){
              $r->place = $place;
              $users['user_'.$r->id] = $r;

              if ($this->cache->memcached->is_supported()) {
                if ($last_month) {
                    $this->cache->memcached->save('last_month_city_ranking_'.$r->id, $place, MEMCACHED_LIFETIIME);
                } else {
                    $this->cache->memcached->save('current_city_ranking_'.$r->id, $place, MEMCACHED_LIFETIIME);
                }
              }

          }
          $place++;
      }

      return $users;
  }


  public function getCommissionPercentage($id_user) {
      $user = $this->getById($id_user);

      $settings = $this->site_config->getGeneralSettings();

      if ($user->level == -2) {
          return $settings['level1_commission'];
      }
      else if ($user->level == -1) {
          return $settings['level2_commission'];
      }
      else {
          return $settings['level3_commission'];
      }
  }


  /**
   * @return image name after resize
   */
  public function uploadImage($files, $name, $filename = null){
      if(isset($files[$name]) && $files[$name]['size'] > 0) {

        $path_info = pathinfo($files[$name]["name"]);
        $ext = $path_info['extension'];

        $config['upload_path'] = './images/profiles/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '2048';
        if($filename)
        {
            $config['file_name'] = $filename;
        }
        else
        {
            $config['file_name'] = md5(time()."asd".rand(10, 999999)) . "." . $ext;
        }


        $this->load->library('upload', $config);

        if($this->upload->do_upload($name)) {

           $pic_data = $this->upload->data();

           $config2['image_library'] = 'gd2';
           $config2['source_image'] = $pic_data['full_path'];
           $config2['new_image'] = './images/profiles/big/';
           $config2['maintain_ratio'] = TRUE;
           $config2['master_dim'] = 'height';
           $config2['overwrite'] = TRUE;
           $config2['height'] = 700;
           $config2['width'] = 1;
           $this->load->library('image_lib');
           $this->image_lib->initialize($config2);
           $this->image_lib->resize();

           $config3['image_library'] = 'gd2';
           $config3['source_image'] = $pic_data['full_path'];
           $config3['new_image'] = './images/profiles/';
           $config3['maintain_ratio'] = TRUE;
           $config3['master_dim'] = 'height';
           $config3['height'] = 120;
           $config3['width'] = 1;

           $this->image_lib->initialize($config3);

           if($this->image_lib->resize()) {
             return $pic_data['file_name'];
           }

        }

      }
      return false;
  }

    /**
     * Uploads a URL image.
     *
     * @param $url
     * @param $filename
     * @return $filename (same as $filename from parameters)
     */
    public function uploadURLImage($url, $filename)
  {
      file_put_contents('./images/profiles/'.$filename, file_get_contents($url));

      $config2['image_library'] = 'gd2';
      $config2['source_image'] = './images/profiles/'.$filename;
      $config2['new_image'] = './images/profiles/big/';
      $config2['maintain_ratio'] = TRUE;
      $config2['master_dim'] = 'height';
      $config2['overwrite'] = TRUE;
      $config2['height'] = 700;
      $config2['width'] = 1;
      $this->load->library('image_lib');
      $this->image_lib->initialize($config2);
      $this->image_lib->resize();

      $config3['image_library'] = 'gd2';
      $config3['source_image'] = './images/profiles/'.$filename;
      $config3['new_image'] = './images/profiles/';
      $config3['maintain_ratio'] = TRUE;
      $config3['master_dim'] = 'height';
      $config3['height'] = 120;
      $config3['width'] = 1;

      $this->image_lib->initialize($config3);

      if($this->image_lib->resize()) {
          return $filename;
      }
      return false;

  }


  /**
   * @return image name after resize
   */
  public function uploadResume($files, $name){
      if(isset($files[$name]) && $files[$name]['size'] > 0) {

        $path_info = pathinfo($files[$name]["name"]);
        $ext = $path_info['extension'];

        $config['upload_path'] = './files/resumes/';
        $config['allowed_types'] = '*';
        // $config['max_size'] = '2048';
        $config['file_name'] = md5(time()."asd".rand(10, 999999)) . "." . $ext;

        $this->load->library('upload', $config);

        if($this->upload->do_upload($name)) {

           $pic_data = $this->upload->data();

           return $pic_data['file_name'];
        }

      }
      return false;
  }


  public function getDateFormat($id_user, $long = false){

      $user = $this->users->get($id_user);

      $format = "d.m.Y";

      if ($user["id_country"] == '236') {
        $format = "m.d.Y";
      }

      if( $long ) {
        $format .= " H:i:s";
      }

      return $format;
  }

  public function formatDate($id_user, $timestamp, $long = false){
      $user = $this->users->get($id_user);

      if ($user['id_country'] == '236') {
          if ($long) {
              return date("M d Y H:i", $timestamp);
          } else {
              return date("M d Y", $timestamp);
          }
      } else {
          if ($long) {
              return date("d M Y H:i", $timestamp);
          } else {
              return date("d M Y", $timestamp);
          }
      }
  }

  public function getDatepickerFormat($id_user){
      $user = $this->users->get($id_user);

      if ($user['id_country'] == '236') {
          return 'M d yyyy';
      } else {
          return 'd M yyyy';
      }
  }

  public function checkPassword($id_user, $password){

      $this->db->where(array(
          'id' => $id_user,
          'password' => md5($password)
        ));
      $data = $this->db->get('auth_user')->result();

      if(count($data)){
        // MD5 password
        return true;
      }
      else {
        // not MD5 password
        $this->db->where(array(
          'id' => $id_user
        ));

        $query = $this->db->get('auth_user')->result_array();
        $user = $query[0];

        if(password_verify($password, $user['password'])) {
          return true;
        }
      }

      return false;
  }

  public function applyMaster($id_user){
      $this->db->set("master_request", "1");
      $this->db->set("master_request_time", "NOW()", false);
      $this->db->where("id", $id_user);
      $this->db->update("auth_user");
  }

  public function isCountry($country){
    $this->db->where("short_name", $country);
    $results = $this->db->get("country_t")->result();
    if(count($results)){
      return true;
    }
    return false;
  }

  public function isState($state){
    $this->db->where("state", $state);
    $results = $this->db->get("states")->result();
    if(count($results)){
      return true;
    }
    return false;
  }

  public function getCountryId($country){
    $this->db->where("short_name", $country);
    $country = $this->db->get("country_t")->row();
    if($country){
      return $country->country_id;
    }
  }

  /**
   * Update completed = 1 from auth_user for normal registration
   *
   * @param string $hash
   */
  public function completeRegistration($hash = false)
  {
      if(!$hash)
      {
        redirect("/");
      }

      $user = $this->db->query("SELECT * FROM auth_user WHERE hash='{$hash}'")->row();

      if(!$user->completed)
      {
          $this->db->set('completed', 1, false);
          $this->db->where('hash', $hash);
          $this->db->update('auth_user');

          $this->Email->newPiecekeeperEmail($user->email);
      }
  }

  /**
   *  Update completed = 1 from auth_user for ipad registration
   *
   * @param string $hash
   */
  public function completeRegistrationIpad($hash = false)
  {
      if(!$hash)
      {
        redirect("/");
      }

      $user = $this->db->query("SELECT * FROM auth_user WHERE hash='{$hash}'")->row();

      if(!$user->completed)
      {
          $this->db->set('completed', 1, false);
          $this->db->set('is_active', 1, false);
          $this->db->set('ipad', 1, false);
          $this->db->where('hash', $hash);
          $this->db->update('auth_user');

          $settings = $this->site_config->getGeneralSettings();
          $code     = $this->Coupon->randomString(6);
          $percent  = $settings['default_discount_percentage'];
          $this->Coupon->createIpadCode($user->id, $code, $percent);
          $this->Coupon->setIpadCode($code);

          $this->Email->sendIpadRegistration($user->email, $code);
      }
  }

  /**
   *  Update completed = 1 from auth_user for pku registration
   *
   * @param string $hash
   */
  public function completeRegistrationPKU($hash = false)
  {
      if(!$hash)
      {
        redirect("/");
      }

      $user = $this->db->query("SELECT * FROM auth_user WHERE hash='{$hash}'")->row();

      if(!$user->completed)
      {
          $this->db->set('completed', 1, false);
          $this->db->set('pku', 1, false);
          $this->db->where('hash', $hash);
          $this->db->update('auth_user');

          $this->Email->sendPKUApplication($user->email);
      }
  }

  public function getCitiesByCountries($id_country, $state, $id_user, $user)
  {
      if ($id_country == 0) {
          if ($user && $user['level'] == 2) {
              $data = $this->db->query("SELECT u.city FROM auth_user AS u INNER JOIN admins_countries AS ac ON ac.id_country=u.id_country WHERE ac.id_user={$id_user} AND TRIM(u.city) <> '' GROUP BY u.city ORDER BY u.city")->result();
          } else {
              $data = $this->db->query("SELECT city FROM auth_user WHERE TRIM(city) <>'' GROUP BY city ORDER BY city")->result();
          }
      } else {
          if($id_country == 236){
              $data = $this->db->query("SELECT city FROM auth_user WHERE id_country={$id_country} AND state='{$state}' AND TRIM(city) <> '' GROUP BY city ORDER BY city")->result();
          } else {
              $data = $this->db->query("SELECT city FROM auth_user WHERE id_country={$id_country} AND TRIM(city) <> '' GROUP BY city ORDER BY city")->result();
          }
      }

      return $data;
  }

  /*
  * Get user data by email
  */
  public function getUserDataByEmail($email)
  {
    $user = $this->db->query("SELECT * FROM auth_user WHERE email='{$email}'")->row();
    return $user;
  }

  /**
   * Delete all the entries from auth_logs if the user resets his password
   */
  public function resetLoginAttempts()
  {
      $client_ip = $_SERVER['REMOTE_ADDR'];

      $conditions = array(
          'ip' => $client_ip,
          'success' => 0,
          'date >=' =>   date("Y-m-d H:i:s",strtotime("-1 day"))
      );

      $this->db->select('*');
      $this->db->from('auth_logs');
      $this->db->where($conditions);
      $this->db->order_by("id", "desc");
      $this->db->limit(5);

      $results = $this->db->get()->result_array();

      foreach ($results as $result)
      {
          $this->deleteFailedLoginAttempts($result['id']);
      }

  }

  /**
   * Delete failed login attempts
   * @param  [int] $id [row id]
   * @return [type]     [description]
   */
  public function deleteFailedLoginAttempts($id)
  {
      $this->db->where('id', $id);
      $this->db->delete('auth_logs');
  }

  /**
   * Set completed status
   * @param  [int] $id_user [id of user]
   */
  public function setCompleted($id_user)
  {
        $this->db->where('id', $id_user);
        $this->db->set('completed', 1);
        $this->db->update('auth_user');
  }

  /**
   * Get completed status
   * @param  [int]  $id_user  [id of user]
   * @param  [int]            [return completed status]
   */
  public function getCompletedStatus($id_user)
  {
        $this->db->select('completed');
        $this->db->from('auth_user');
        $this->db->where('id', $id_user);
        $result = $this->db->get()->row()->completed;

        return $result;
  }

    /**
     * Verify if a user is a pending user.
     *
     * @param  [string]  $email [email address]
     * @return [bool]           [is pending user or not]
     */
    public function isPendingUser($email)
    {
        $user = $this->getByEmail($email);

        if(!empty($user))
        {
            if($user->is_active == 0 && $user->deleted == 0)
            {
                return true;
            }
        }

        return false;
    }
}
