<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_notification extends MY_Model
{

  function __construct()
  {
    parent::__construct();
    $this->table = 'notifications';
  }

  public function logHandling($id_admin, $id_notification, $table, $id_admin_field = false, $handled_field = false){
      $this->db->where('id', $id_notification);
      if($id_admin_field){
          $this->db->set($id_admin_field, $id_admin);
      } else {
          $this->db->set('id_admin', $id_admin);
      }
      if($handled_field){
          $this->db->set($handled_field, 'NOW()', false);
      } else {
          $this->db->set('handled', 'NOW()', false);
      }
      $this->db->update($table);
  }

  public function getAll($ids_countries, $history = false, $size = false, $page = false , $filters = false) {
      $id_admin = $this->session->userdata('id');

      $notifications = array();


      $results = $this->getPendingPiecekeepers($ids_countries, $history, $size*$page , $filters);
      foreach ($results as $r) {
          array_push($notifications, $r);
      }

      $results = $this->getPendingBlogPosts($ids_countries, $history, $size*$page , $filters);
      foreach ($results as $r) {
          array_push($notifications, $r);
      }

      $results = $this->getPendingEvents($ids_countries, $history, $size*$page ,$filters);
      foreach ($results as $r) {
          array_push($notifications, $r);
      }

      $results = $this->getPendingPayments($ids_countries, $history, $size*$page ,$filters);
      foreach ($results as $r) {
          array_push($notifications, $r);
      }

      $results = $this->getPendingSlavesRequests($ids_countries, $history, $size*$page ,$filters);
      foreach ($results as $r) {
          array_push($notifications, $r);
      }

      $results = $this->getPendingCoupons($ids_countries, $history, $size*$page ,$filters);
      foreach ($results as $r) {
          array_push($notifications, $r);
      }

      $results = $this->getPendingMasters($ids_countries, $history, $size*$page ,$filters);
      foreach ($results as $r) {
          array_push($notifications, $r);
      }

      $results = $this->getPendingMissions($ids_countries, $history, $size*$page ,$filters);
      foreach ($results as $r) {
          array_push($notifications, $r);
      }

      $results = $this->getPendingPackages($ids_countries, $history, $size*$page ,$filters);
      foreach ($results as $r) {
          array_push($notifications, $r);
      }

      $results = $this->getFollowersWarning($ids_countries, $history, $size*$page, $filters);
      foreach ($results as $r) {
          array_push($notifications, $r);
      }

      if($this->users->is_super_admin($id_admin)){
          $results = $this->getBestPerformers($history, $size*$page ,$filters);
          foreach ($results as $r) {
              array_push($notifications, $r);
          }
      }

      if($history){
          usort($notifications, array($this, 'history_sorter'));
          $result = array();
          foreach ($notifications as $key => $n) {
              if($key >= ($page-1)*$size && $key < $page*$size){
                  $result[] = $n;
              }
          }

          return $result;
      }

      usort($notifications, array($this, 'sorter'));
      return $notifications;
  }

  private function sorter($a, $b) {
      if ($a['created'] <= $b['created']) {
          return true;
      }
      return false;
  }

  private function history_sorter($a, $b) {
      if ($a['handled'] <= $b['handled']) {
          return true;
      }
      return false;
  }

  public function getBestPerformers($history = false, $limit = false , $filters = false){
  	$where = "";

  	if($filters){
  		foreach($filters as $filter => $value){
  			if($value){
  				$where .= " AND {$filter} = {$value}";
  			}
  		}
  	}

      if($history){
          $results = $this->db->query("SELECT * FROM best_performers WHERE id_admin!=0 {$where}")->result();
      } else {
          $results = $this->db->query("SELECT * FROM best_performers WHERE status=0")->result();
      }

      $notifications = array();
      foreach ($results as $r) {
          $notifications[] = array(
              'message'  => "A new <b>Best Performers</b> list was created.",
              'link'     => base_url('admin/best_performers/view/'.$r->id),
              'created'  => $r->created,
              'id_admin' => $r->id_admin,
              'handled'  => $r->handled
            );
      }
      return $notifications;
  }

  public function getPendingPackages($ids_countries, $history = false, $limit = false , $filters = false){
  	$where = "";

  	if($filters){
  		foreach($filters as $filter => $value){
  			if($value){
  				$where .= " AND p.{$filter} = {$value}";
  			}
  		}
  	}

      if($history){
          $results = $this->db->query("SELECT p.*, u.first_name, u.last_name FROM packages AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE p.id_admin!=0 AND u.id_country IN ({$ids_countries}) {$where}")->result();
      } else {
          $results = $this->db->query("SELECT p.*, u.first_name, u.last_name FROM packages AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE p.status=0 AND p.confirmed=1 AND u.id_country IN ({$ids_countries})")->result();
      }

      $notifications = array();
      foreach ($results as $r) {
          $notifications[] = array(
              'message' => "A new Package for <b>".$r->first_name." ".$r->last_name."</b> is waiting for approval",
              'link'    => base_url('admin/packages/view/'.$r->id),
              'created' => $r->created,
              'id_admin' => $r->id_admin,
              'handled'  => $r->handled
            );
      }
      return $notifications;
  }

  public function getPendingPiecekeepers($ids_countries, $history = false, $limit = false ,$filters = false) {

  	$where = "";

  	if($filters){
  		foreach($filters as $filter => $value){
  			if($value){
  				$where .= " AND {$filter} = {$value}";
  			}
  		}
  	}

    // Remove PKU piecekeepers from the notification list
    $where .= "AND (pku is null OR pku = 0)";

  	if($history){
          $users = $this->db->query("SELECT * FROM auth_user WHERE id_admin!=0 AND id_country IN ({$ids_countries}) {$where} ORDER BY date_joined DESC LIMIT {$limit}")->result();
      } else {
          $users = $this->db->query("SELECT * FROM auth_user WHERE is_active=0 AND deleted=0 AND (completed = 1 or completed = 0) AND id_country AND (pku is null OR pku = 0) IN ({$ids_countries})")->result();
      }

      $notifications = array();
      foreach ($users as $u) {
          $notifications[] = array(
              'message' => "A new PieceKeeper is waiting for approval: <b>".$u->first_name." ".$u->last_name."</b>",
              'link'    => base_url('admin/user/view/'.$u->id),
              'created' => $u->date_joined,
              'id_admin' => $u->id_admin,
              'handled'  => $u->handled
            );
      }

      return $notifications;
  }

  public function getPendingBlogPosts($ids_countries, $history = false, $limit = false ,$filters = false) {
	  	$where = "";

	  	if($filters){
	  		foreach($filters as $filter => $value){
	  			if($value){
	  				$where .= " AND b.{$filter} = {$value}";
	  			}
	  		}
	  	}


  	  if($history){
          $results = $this->db->query("SELECT b.*, u.first_name, u.last_name FROM blog_entry as b INNER JOIN auth_user AS u ON u.id=b.id_user WHERE b.id_admin!=0 AND u.id_country IN ({$ids_countries}) {$where}")->result();
      } else {
          $results = $this->db->query("SELECT b.*, u.first_name, u.last_name FROM blog_entry as b INNER JOIN auth_user AS u ON u.id=b.id_user WHERE b.aproved=0 AND u.id_country IN ({$ids_countries})")->result();
      }

      $notifications = array();
      foreach ($results as $r) {
          $notifications[] = array(
              'message' => "A new Blog Post is waiting for approval: <b>".$r->name."</b> added by <b>".$r->first_name." ".$r->last_name."</b>",
              'link'    => base_url('admin/blogPosts/post/'.$r->id),
              'created' => $r->creation_date,
              'id_admin' => $r->id_admin,
              'handled'  => $r->handled
            );
      }
      return $notifications;
  }

  public function getPendingEvents($ids_countries, $history = false, $limit = false ,$filters = false) {
	  	$where = "";

	  	if($filters){
	  		foreach($filters as $filter => $value){
	  			if($value){
	  				$where .= " AND e.{$filter} = {$value}";
	  			}
	  		}
	  	}

  	  if($history){
          $results = $this->db->query("SELECT e.*, u.first_name, u.last_name FROM event_entry as e INNER JOIN auth_user AS u ON u.id=e.id_user WHERE e.id_admin!=0 AND u.id_country IN ({$ids_countries}) {$where}")->result();
      } else {
          $results = $this->db->query("SELECT e.*, u.first_name, u.last_name FROM event_entry as e INNER JOIN auth_user AS u ON u.id=e.id_user WHERE e.aproved=0 AND u.id_country IN ({$ids_countries})")->result();
      }

      $notifications = array();
      foreach ($results as $r) {
          $notifications[] = array(
              'message' => "A new Event is waiting for approval: <b>".$r->name."</b> added by <b>".$r->first_name." ".$r->last_name."</b>",
              'link'    => base_url('admin/event/edit/'.$r->id),
              'created' => $r->creation_date,
              'id_admin' => $r->id_admin,
              'handled'  => $r->handled
            );
      }
      return $notifications;
  }

  public function getPendingPayments($ids_countries, $history = false, $limit = false , $filters = false) {
		$where = "";

		if($filters){
  			foreach($filters as $filter => $value){
  				if($value){
  					$where .= " AND p.{$filter} = {$value}";
  				}
  			}
  		}

  	  if($history){
          $results = $this->db->query("SELECT p.*, u.first_name, u.last_name FROM payment_requests as p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE p.id_admin!=0 AND u.id_country IN ({$ids_countries}) {$where}")->result();
      } else {
          $results = $this->db->query("SELECT p.*, u.first_name, u.last_name FROM payment_requests as p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE p.aproved=0 AND u.id_country IN ({$ids_countries})")->result();
      }

      $notifications = array();
      foreach ($results as $r) {
          $notifications[] = array(
              'message' => "<b>".$r->first_name." ".$r->last_name."</b> requested a <b>".$r->amount."$ payment</b>",
              'link'    => base_url('admin/payments/view/'.$r->id),
              'created' => $r->created,
              'id_admin' => $r->id_admin,
              'handled'  => $r->handled
            );
      }
      return $notifications;
  }

  public function getPendingSlavesRequests($ids_countries, $history = false, $limit = false ,$filters = false) {

  	$where = "";

  	if($filters){
  		foreach($filters as $filter => $value){
  			if($value){
  				$where .= " AND s.{$filter} = {$value}";
  			}
  		}
  	}


  	if($history){
          $results = $this->db->query("SELECT s.*, u.first_name, u.last_name FROM slaves_requests as s INNER JOIN auth_user AS u ON u.id=s.id_user WHERE s.id_admin!=0 AND u.id_country IN ({$ids_countries}) {$where}")->result();
      } else {
          $results = $this->db->query("SELECT s.*, u.first_name, u.last_name FROM slaves_requests as s INNER JOIN auth_user AS u ON u.id=s.id_user WHERE s.aproved=0 AND u.id_country IN ({$ids_countries})")->result();
      }

      $notifications = array();
      foreach ($results as $r) {
          $notifications[] = array(
              'message' => "<b>".$r->first_name." ".$r->last_name."</b> requested for <b>".$r->slaves." more UnderKeepers</b>",
              'link'    => base_url('/admin/slaves_requests/view/'.$r->id),
              'created' => $r->created,
              'id_admin' => $r->id_admin,
              'handled'  => $r->handled
            );
      }
      return $notifications;
  }

  public function getPendingCoupons($ids_countries, $history = false, $limit = false , $filters = false) {
  	$where = "";

  	if($filters){
  		foreach($filters as $filter => $value){
  			if($value){
  				$where .= " AND c.{$filter} = {$value}";
  			}
  		}
  	}



  	if($history){
          $results = $this->db->query("SELECT c.*, u.first_name, u.last_name FROM coupons AS c INNER JOIN auth_user AS u ON u.id=c.id_creator WHERE c.id_admin!=0 AND u.id_country IN ({$ids_countries}) {$where}")->result();
      } else {
          $results = $this->db->query("SELECT c.*, u.first_name, u.last_name FROM coupons AS c INNER JOIN auth_user AS u ON u.id=c.id_creator WHERE c.approved=0 AND c.deleted=0 AND type=2 AND u.id_country IN ({$ids_countries})")->result();
      }

      $notifications = array();
      foreach ($results as $r) {
          $notifications[] = array(
              'message' => "<b>".$r->first_name." ".$r->last_name."</b> requested a new coupon: <b>".$r->code."</b>",
              'link'    => base_url('/admin/coupons/'),
              'created' => $r->created,
              'id_admin' => $r->id_admin,
              'handled'  => $r->handled
            );
      }
      return $notifications;
  }

  public function getPendingMasters($ids_countries, $history = false, $limit = false ,$filters = false) {
  	$where = "";

  	if($filters){
  		foreach($filters as $filter => $value){
  			if($filter == "id_admin"){
  				if($value){
  					$where .= " AND u.id_admin_master = {$value}";
  				}
  			}
  		}
  	}
      if($history){
          $results = $this->db->query("SELECT u.first_name, u.master_request_time, u.last_name, u.date_joined, u.id_admin_master, u.handled_master FROM auth_user AS u WHERE u.id_admin_master!=0 AND u.id_country IN ({$ids_countries}) {$where}")->result();
      } else {
          $results = $this->db->query("SELECT u.first_name, u.master_request_time ,u.last_name, u.date_joined, u.id_admin_master, u.handled_master FROM auth_user AS u WHERE u.master_request=1 AND u.id_country IN ({$ids_countries})")->result();
      }

      $notifications = array();
      foreach ($results as $r) {
          $notifications[] = array(
              'message' => "<b>".$r->first_name." ".$r->last_name."</b> applied as Master PieceKeeper",
              'link'    => base_url('/admin/master_requests/'),
              'created' => $r->master_request_time,
              'id_admin' => $r->id_admin_master,
              'handled'  => $r->handled_master
            );
      }
      return $notifications;
  }

  public function getPendingMissions($ids_countries, $history = false, $limit = false ,$filters = false) {
  	$where = "";

  	if($filters){
  		foreach($filters as $filter => $value){
  			if($value){
  				$where .= " AND mu.{$filter} = {$value}";
  			}
  		}
  	}
      if($history){
          $results = $this->db->query("SELECT m.name, mu.*, u.first_name, u.last_name FROM missions_users as mu INNER JOIN auth_user AS u ON u.id=mu.id_user INNER JOIN missions AS m ON m.id=mu.id_mission WHERE mu.id_admin!=0 AND u.id_country IN ({$ids_countries}) {$where}")->result();
      } else {
          $results = $this->db->query("SELECT m.name, mu.*, u.first_name, u.last_name FROM missions_users as mu INNER JOIN auth_user AS u ON u.id=mu.id_user INNER JOIN missions AS m ON m.id=mu.id_mission WHERE mu.status=0 AND u.id_country IN ({$ids_countries})")->result();
      }

      $notifications = array();
      foreach ($results as $r) {
          $notifications[] = array(
              'message' => "<b>".$r->first_name." ".$r->last_name."</b> submitted a mission: <b>".$r->name."</b>",
              'link'    => base_url('admin/missions/view/'.$r->id_mission.'/'.$r->id_user),
              'created' => $r->submitted,
              'id_admin' => $r->id_admin,
              'handled'  => $r->handled
            );
      }
      return $notifications;
  }

    /**
     * @param array $ids_countries
     * @param bool $history
     * @param bool $limit
     * @param bool $filters
     * @return social followers decrease notifications
     */
  public function getFollowersWarning($ids_countries, $history = false, $limit = false , $filters = false)
  {

      $ids_countries = explode(", ", $ids_countries);
      $this->db->select('notifications.id_user, notifications.param, auth_user.first_name, auth_user.last_name, notifications.created, notifications.id_admin');
      $this->db->from("notifications");
      $this->db->join("auth_user", "notifications.id_user = auth_user.id", "INNER");
      $this->db->where_in("auth_user.id_country", $ids_countries);
      $this->db->where("type = 19");
      $this->db->where("notifications.created >= DATE_SUB(NOW(), INTERVAL 3 DAY) AND notifications.created <= NOW()");
      $results = $this->db->get()->result();

      $notifications = array();
      $networks = array(
          1 => "Facebook",
          2 => "Twitter",
          3 => "Instagram",
          4 => "Tumblr",
          5 => "Pinterest",
          6 => "Linkedin",
          7 => "YouTube",
          8 => "Vine"
      );
      foreach ($results as $r)
      {
          //param[0] = social network id param[1] sored followers param[2] actual followers
          $param = unserialize($r->param);
    
          $notifications[] = array(
              'message' => "Follower decrease for <b>".$r->first_name." ".$r->last_name."</b> on <b>".$networks[$param['network']]."</b>"." from <b>".number_format(floatval($param['storedFollowers']),0, ',', '.')."</b> followers to <b>".number_format(floatval($param['followers']),0, ',', '.')."</b> followers",
              'link'    => base_url('admin/user/view/'.$r->id_user),
              'handled' => $r->created,
              'id_admin' => $r->id_admin,
              'created' => $r->created
          );
      }
      return $notifications;
  }


  // This function is Deprecated and you should not use it!

  public function getDetails() {
    $details = array();
    if($this->type == 1) {
      $user = $this->db->query("SELECT * FROM auth_user WHERE id={$this->param}")->result();
      if($user) {
        $details['text'] = "A new ambasador is waiting for approval: <b>" . ($user[0]->first_name) . " ". ($user[0]->last_name) ."</b>";
        $details['url'] = $this->config->base_url(). "admin/user/view/". $user[0]->id ;
      }
    }
    elseif($this->type == 2) {
      $blog_entry = $this->db->query("SELECT b.id, b.name, u.first_name, u.last_name FROM " .
                            "blog_entry as b INNER JOIN auth_user as u ON b.id_user=u.id ".
                            "WHERE b.id={$this->param}")->result();
      if($blog_entry) {
        $details['text'] = "A new blog entry is waiting for approval: <b>{$blog_entry[0]->name}</b> added by <b>{$blog_entry[0]->first_name} {$blog_entry[0]->last_name}</b>";
        $details['url'] = $this->config->base_url(). "admin/blogPosts/post/". $blog_entry[0]->id;
      }
    }
    elseif($this->type==3) {
      $event = $this->db->query("SELECT e.id, e.name, u.first_name, u.last_name FROM " .
                            "event_entry as e INNER JOIN auth_user as u ON e.id_user=u.id ".
                            "WHERE e.id={$this->param}")->result();
      if($event) {
        $details['text'] = "A new event is waiting for approval: <b>{$event[0]->name}</b> added by <b>{$event[0]->first_name} {$event[0]->last_name}</b>";
        $details['url'] = $this->config->base_url(). "admin/event/edit/". $event[0]->id;
      }
    } elseif($this->type==4) {
      $p_request = $this->db->query("SELECT p.id, p.amount, u.first_name, u.last_name FROM " .
                            "payment_requests as p INNER JOIN auth_user as u ON p.id_user=u.id ".
                            "WHERE p.id={$this->param}")->result();
      if($p_request) {
        $details['text'] = "<b>{$p_request[0]->first_name} {$p_request[0]->last_name}</b> requested for a <b>{$p_request[0]->amount}&euro;</b> payment";
        $details['url'] = $this->config->base_url(). "admin/payments/view/". $p_request[0]->id;
      }
    }elseif($this->type==5) {
        $s_request = $this->db->query("SELECT s.id, s.slaves, u.first_name, u.last_name FROM " .
                            "slaves_requests as s INNER JOIN auth_user as u ON s.id_user=u.id ".
                            "WHERE s.id={$this->param}")->result();
      if($s_request) {
        $details['text'] = "<b>{$s_request[0]->first_name} {$s_request[0]->last_name}</b> requested for <b>{$s_request[0]->slaves}</b> more slaves";
        $details['url'] = $this->config->base_url(). "admin/slaves_requests/view/". $s_request[0]->id;
      }
    } elseif($this->type==6) {
        $c_request = $this->db->query("SELECT coupons.*, u.first_name, u.last_name FROM " .
                            "coupons LEFT JOIN auth_user as u ON coupons.id_creator=u.id ".
                            "WHERE coupons.id={$this->param}")->result();
      if($c_request) {
        $details['text'] = "<b>{$c_request[0]->first_name} {$c_request[0]->last_name}</b> requested a new coupon";
        $details['url'] = $this->config->base_url(). "admin/coupons/index";
      }
    } elseif($this->type==7) {
        $m_request = $this->db->query("SELECT first_name, last_name FROM " .
                            "auth_user ".
                            "WHERE id={$this->param}")->result();
      if($m_request) {
        $details['text'] = "<b>{$m_request[0]->first_name} {$m_request[0]->last_name}</b> applied as Master";
        $details['url'] = $this->config->base_url(). "admin/master_requests/index";
      }
    } elseif($this->type==8) {
        // param = id_mission.','.id_user
        $data = explode(',', $this->param);
        $id_user    = $data[1];
        $id_mission = $data[0];

        $user = $this->db->query("SELECT first_name, last_name FROM " .
                            "auth_user ".
                            "WHERE id={$id_user}")->row();
      if(count($user)) {
        $details['text'] = "<b>{$user->first_name} {$user->last_name}</b> submitted a mission";
        $details['url'] = $this->config->base_url(). "admin/missions/view/".$id_mission.'/'.$id_user;
      }
    }

    $details['correct'] = true;
    if(!isset($details['text'])) {
      $details['text'] = "";
      $details['correct'] = false;
    }
    if(!isset($details['url'])) {
      $details['url'] = "";
      $details['correct'] = false;
    } else {
       $details['url'] .= "/" . $this->id;
    }

    return $details;
  }

  function insert() {
     $data=array(
        'type' => $this->type,
        'param' => $this->param,
        'id_user' => 0
    );
    // $this->db->set('created', 'NOW()', FALSE);
    // $this->db->insert($this->table,$data);
  }

  function setInactive($id) {
     $data=array(
        'active' => 0
    );
    $this->db->update($this->table,$data, array('id' => $id));
  }

  function getRecent($number = false, $ids_countries = false) {
    if ($ids_countries) {
        $sql = "SELECT * FROM notifications AS n INNER JOIN auth_user AS u ON u.id=n.param WHERE n.active='1' AND n.id_user=0 AND u.id_country IN ($ids_countries) ORDER BY created desc ";
    } else {
        $sql = "SELECT * FROM " . $this->table . " WHERE active='1' AND id_user=0 ORDER BY created desc ";
    }

    if($number) {
     $items = $this->db->query($sql . " limit $number")->result();
    } else {
      $items = $this->db->query($sql)->result();
    }
    $notifications = array();
    foreach($items as $db_row) {
      $n = new Admin_notification();
      $n->id   = $db_row->id;
      $n->type = $db_row->type;
      $n->param = $db_row->param;
      $n->created    = $db_row->created;
      $notifications[] = $n;
    }
    return $notifications;
  }

  function getItem($id) {
     $query = $this->db->query("SELECT * FROM notifications WHERE active=1 AND id={$id}");

      $db_row       = $query->row();
      $n            = new Admin_notification();
      $n->id        = $id;
      $n->type      = $db_row->type;
      $n->param     = $db_row->param;
      $n->created   = $db_row->created;

      return $n;
  }

}
