<?php

class Email_model extends CI_Model {

  function __construct()
  {
  	$this->load->library('email');
	$this->email->initialize();
  }

  public function replaceContent($content, $stats){
      $content = str_replace('{NAME}', $stats['name'], $content);
      $content = str_replace('{FOLLOWERS}', $stats['followers'], $content);
      $content = str_replace('{TOTAL_POINTS}', $stats['total_points'], $content);
      $content = str_replace('{COMMISSION}', $stats['commission'], $content);
      $content = str_replace('{GLOBAL_RANK}', $stats['global_rank'], $content);
      $content = str_replace('{COUNTRY_RANK}', $stats['country_rank'], $content);
      $content = str_replace('{CITY_RANK}', $stats['city_rank'], $content);
      return $content;
  }

  public function replaceRecoveryLink($content, $level)
  {
      switch($level)
      {
          case 2:
              $content = str_replace('{REQUEST_PACKAGE}', base_url('packages/gift'), $content);
              break;
          case 3:
              $content = str_replace('{REQUEST_PACKAGE}', base_url('packages/free_product'), $content);
              break;
      }

      return $content;
  }

  public function sendRecoveryLink($email, $link) {

      $message = "Click the link bellow to recover your password: <br><br> <a href='".$link."'>".$link."</a>";
      $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
      $this->email->to($email);

      $this->email->subject('Recovery Link');
      $this->email->message($message);
      $this->email->send();

  }

  public function sendBlockedAttempt($ip, $email) {
    $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
    $this->email->to("alin@agvision.ro");

    $this->email->subject('Login Blocked Attempt');
    $this->email->message("<p>IP : <b>".$ip."</b></p><p>Email : <b>".$email."</b></p>");

    $this->email->send();
  }

    // public function sendLevel3Package($email)
    // {
    //     $user = $this->User->getByEmail($email);
    //     $email_id = $this->verifyWhatEmailToSend(18, $user);
    //
    //     $template = $this->db->query("SELECT * FROM email_templates WHERE id = {$email_id}")->row();
    //
    //     if (count($template))
    //     {
    //         $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
    //         $this->email->to($email);
    //         $this->email->subject($template->name);
    //         $this->email->message($template->content);
    //         $this->email->send();
    //     }
    // }

    public function sendLevel3Package($email)
    {
      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(61, $user);

      $template = $this->db->query("SELECT * FROM email_templates WHERE id = {$email_id}")->row();

      if (count($template))
      {
          $content = $this->replaceRecoveryLink($template->content, 3);

          $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
          $this->email->to($email);
          $this->email->subject($template->name);
          $this->email->message($content);
          $this->email->send();
      }
    }

    public function sendLevel2Package($email)
    {
      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(60, $user);

      $template = $this->db->query("SELECT * FROM email_templates WHERE id = {$email_id}")->row();

      if (count($template))
      {
          $content = $this->replaceRecoveryLink($template->content, 2);

          $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
          $this->email->to($email);
          $this->email->subject($template->name);
          $this->email->message($content);
          $this->email->send();
      }

    }

  public function sendComment($email,$comment) {

    $user = $this->users->getByEmail($email);
    $email_id = $this->verifyWhatEmailToSend(50, $user);

    $template = $this->db->query("SELECT * FROM email_templates WHERE id = {$email_id} ")->row();

    $stats = $this->users->getStatsForEmail($email);
    $template->content = $this->replaceContent($template->content, $stats);
    $template->content = str_replace("{COMMENT}", $comment, $template->content);

    if (count($template)) {
      $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
      $this->email->to($email);
      $this->email->subject($template->name);
      $this->email->message($template->content);
      $this->email->send();
    }
  }

  public function sendSuccessSale($email) {

      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(11, $user);

  	  $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

      $stats = $this->users->getStatsForEmail($email);
      $template->content = $this->replaceContent($template->content, $stats);

  		if (count($template)) {
  			$this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
			  $this->email->to($email);
  			$this->email->subject($template->name);
  			$this->email->message($template->content);
  			$this->email->send();
  		}
  }

  public function sendUpgradeLevel2($email) {

      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(48, $user);

      $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

      $stats = $this->users->getStatsForEmail($email);
      $template->content = $this->replaceContent($template->content, $stats);

      if (count($template)) {
        $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
        $this->email->to($email);
        $this->email->subject($template->name);
        $this->email->message($template->content);
        $this->email->send();
      }
  }

  public function sendUpgradeLevel3($email) {
      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(49, $user);

      $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

      $stats = $this->users->getStatsForEmail($email);
      $template->content = $this->replaceContent($template->content, $stats);

      if (count($template)) {
        $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
        $this->email->to($email);
        $this->email->subject($template->name);
        $this->email->message($template->content);
        $this->email->send();
      }
  }

  public function sendIpadRegistration($email, $code) {

      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(46, $user);

      $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

      $stats = $this->users->getStatsForEmail($email);
      $template->content = $this->replaceContent($template->content, $stats);
      $template->content = str_replace("{IPAD_COUPON}", $code, $template->content);

      if (count($template)) {
        $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
        $this->email->to($email);
        $this->email->subject($template->name);
        $this->email->message($template->content);
        $this->email->send();
      }
  }

  public function sendBestPerformers($email) {

      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(23, $user);

      $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

      $stats = $this->users->getStatsForEmail($email);
      $template->content = $this->replaceContent($template->content, $stats);

      if (count($template)) {
        $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
        $this->email->to($email);
        $this->email->subject($template->name);
        $this->email->message($template->content);
        $this->email->send();
      }
  }

  public function sendMasterApplicationRejected($email) {

      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(21, $user);

      $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

      $stats = $this->users->getStatsForEmail($email);
      $template->content = $this->replaceContent($template->content, $stats);

      if (count($template)) {
        $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
        $this->email->to($email);
        $this->email->subject($template->name);
        $this->email->message($template->content);
        $this->email->send();
      }
  }

  public function sendApprovedMultiCoupon($email) {
      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(17, $user);

      $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

      $stats = $this->users->getStatsForEmail($email);
      $template->content = $this->replaceContent($template->content, $stats);

      if (count($template)) {
        $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
        $this->email->to($email);
        $this->email->subject($template->name);
        $this->email->message($template->content);
        $this->email->send();
      }
  }

  public function sendRejectedMultiCoupon($email) {
      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(18, $user);

      $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

      $stats = $this->users->getStatsForEmail($email);
      $template->content = $this->replaceContent($template->content, $stats);

      if (count($template)) {
        $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
        $this->email->to($email);
        $this->email->subject($template->name);
        $this->email->message($template->content);
        $this->email->send();
      }

  }

  public function sendApprovedMission($email) {
      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(8, $user);

      $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

      $stats = $this->users->getStatsForEmail($email);
      $template->content = $this->replaceContent($template->content, $stats);

      if (count($template)) {
        $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
        $this->email->to($email);
        $this->email->subject($template->name);
        $this->email->message($template->content);

          $id_user = $this->session->userdata('id');
          $data = array(
              'subject'     => $template->name,
              'to_email'    => $email,
              'body'        => $template->content,
              'id_sender'   => (empty($id_user) ? 0 : $id_user),
              'created' => date('Y-m-d H:i:s', time())
          );
          $this->db->insert('emails_report', $data);

        $this->email->send();
      }
  }

  public function sendRejectedMission($email) {
      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(9, $user);

  	  $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

      $stats = $this->users->getStatsForEmail($email);
      $template->content = $this->replaceContent($template->content, $stats);

  		if (count($template)) {
  			$this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
  			$this->email->to($email);
  			$this->email->subject($template->name);
  			$this->email->message($template->content);

            $id_user = $this->session->userdata('id');
            $data = array(
                'subject'     => $template->name,
                'to_email'    => $email,
                'body'        => $template->content,
                'id_sender'   => (empty($id_user) ? 0 : $id_user),
                'created' => date('Y-m-d H:i:s', time())
            );
            $this->db->insert('emails_report', $data);

  			$this->email->send();
  		}
  }

  public function sendLevel2($email) {
      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(6, $user);

      $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

      $stats = $this->users->getStatsForEmail($email);
      $template->content = $this->replaceContent($template->content, $stats);

      if (count($template)) {
        $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
        $this->email->to($email);
        $this->email->subject($template->name);
        $this->email->message($template->content);
        $this->email->send();
      }
  }

  public function sendLevel3($email) {
      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(7, $user);

      $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

      $stats = $this->users->getStatsForEmail($email);
      $template->content = $this->replaceContent($template->content, $stats);

      if (count($template)) {
        $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
        $this->email->to($email);
        $this->email->subject($template->name);
        $this->email->message($template->content);
        $this->email->send();
      }
  }

  public function sendSlaveInvitation($email, $user, $hash) {
    $email_id = $this->verifyWhatEmailToSend(22, $user);

    $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

    if(count($template)){
        $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
        $this->email->to($email);

        $piecekeeper_name = $user['first_name'].' '.$user['last_name'];
        $link             = base_url('/auth/register?i='.$hash);

        $template->content = str_replace("{PIECEKEEPER_NAME}", $piecekeeper_name, $template->content);
        $template->content = str_replace("{LINK}", $link, $template->content);
        $this->email->subject($template->name);
        $this->email->message($template->content);
        $this->email->send();
    }

  }

  public function giftcardCreated( $code, $user ) {
  	$this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
  	$this->email->to($user['email']);

  	$this->email->subject('Your giftcard was approved');
  	$this->email->message("Hi {$user['first_name']} {$user['last_name']}, <br /><br /> Your giftcard code is: <b>{$code}</b> ");

  	$this->email->send();
  }

  public function inactive45Days( $user ) {

    $email_id = $this->verifyWhatEmailToSend(24, $user);

  	$template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();
    $email    = $user["email"];

    $stats = $this->users->getStatsForEmail($email);
    $template->content = $this->replaceContent($template->content, $stats);

    if (count($template)) {
        $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
        $this->email->to($email);
        $this->email->subject($template->name);
        $this->email->message($template->content);
        $this->email->send();
    }
  }

  public function newPiecekeeperEmail( $email ) {
      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(16, $user);

      $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

      // $stats = $this->users->getStatsForEmail($email);
      // $template->content = $this->replaceContent($template->content, $stats);

      if (count($template)) {
          $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
          $this->email->to($email);
          $this->email->subject($template->name);
          $this->email->message($template->content);
          $this->email->send();
      }
  }

  public function sendPKUApplication( $email ) {

      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(53, $user);

      $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

      $stats = $this->users->getStatsForEmail($email);
      $template->content = $this->replaceContent($template->content, $stats);

      if (count($template)) {
          $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
          $this->email->to($email);
          $this->email->subject($template->name);
          $this->email->message($template->content);
          $this->email->send();
      }
  }

  public function sendPKUEmailPage( $email ) {

      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(54, $user);

      $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

      if (count($template)) {
          $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
          $this->email->to($email);
          $this->email->subject($template->name);
          $this->email->message($template->content);
          $id_user = $this->session->userdata('id');
          $data = array(
              'subject'     => $template->name,
              'to_email'    => $email,
              'body'        => $template->content,
              'id_sender'   => (empty($id_user) ? 0 : $id_user),
              'created' => date('Y-m-d H:i:s', time())
          );
          $this->db->insert('emails_report', $data);
          $this->email->send();
      }
  }

  public function approvedAsPiecekeeper( $email ) {

     $user = $this->users->getByEmail($email);
     $email_id = $this->verifyWhatEmailToSend(15, $user);

  	 $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

     $stats = $this->users->getStatsForEmail($email);
     $template->content = $this->replaceContent($template->content, $stats);

     if (count($template)) {
         $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
         $this->email->to($email);
         $this->email->subject($template->name);
         $this->email->message($template->content);
         $this->email->send();
     }
  }

  public function missionWaiting( $email ) {

     $user = $this->users->getByEmail($email);
     $email_id = $this->verifyWhatEmailToSend(10, $user);

     $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

     $stats = $this->users->getStatsForEmail($email);
     $template->content = $this->replaceContent($template->content, $stats);

     if (count($template)) {
         $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
         $this->email->to($email);
         $this->email->subject($template->name);
         $this->email->message($template->content);
         $id_user = $this->session->userdata('id');
         $data = array(
             'subject'     => $template->name,
             'to_email'    => $email,
             'body'        => $template->content,
             'id_sender'   => (empty($id_user) ? 0 : $id_user),
             'created' => date('Y-m-d H:i:s', time())
         );
         $this->db->insert('emails_report', $data);
         
         $this->email->send();
     }
  }

  public function sendPiecekeeperApplicationRejected($email)
  {
    $template_id = 57;

    $user = $this->users->getByEmail($email);

    // send other mail if the user didn't link any social account
    if($user->completed == 0 && $user->deleted == 1 && $user->is_active == 0)
    {
        $template_id = 58;
    }

    $email_id = $this->verifyWhatEmailToSend($template_id, $user);

    $template = $this->db->query("SELECT * FROM email_templates WHERE id = {$email_id}")->row();

    $stats = $this->users->getStatsForEmail($email);
    $template->content = $this->replaceContent($template->content, $stats);

    if (count($template))
    {
        $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
        $this->email->to($email);
        $this->email->subject($template->name);
        $this->email->message($template->content);
        $this->email->send();
    }

  }

  public function blockedPiecekeeper($user)
  {
      $first_name = ucwords($user['first_name']);
      $last_name  = ucwords($user['last_name']);

      $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
  	  $this->email->to($user['email']);
      $this->email->subject('You have been blocked as Piecekeeper');
      $this->email->message("Hi {$first_name} {$last_name}, <br /><br /> Your account is no longer active on OnePiece, so you won't be able to login, unless you are approved again.<br />");
      $this->email->send();
  }

  public function paymentApproved( $email ) {

    $user = $this->users->getByEmail($email);
    $email_id = $this->verifyWhatEmailToSend(19, $user);

  	$template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

    $stats = $this->users->getStatsForEmail($email);
    $template->content = $this->replaceContent($template->content, $stats);

    if (count($template)) {
        $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
        $this->email->to($email);
        $this->email->subject($template->name);
        $this->email->message($template->content);
        $this->email->send();
    }
  }

  public function appliedMaster( $email ) {

      $user = $this->users->getByEmail($email);
      $email_id = $this->verifyWhatEmailToSend(14, $user);

      $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

      $stats = $this->users->getStatsForEmail($email);
      $template->content = $this->replaceContent($template->content, $stats);

      if (count($template)) {
          $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
          $this->email->to($email);
          $this->email->subject($template->name);
          $this->email->message($template->content);
          $this->email->send();
      }
  }

  public function approvedMaster( $email ) {

    $user = $this->users->getByEmail($email);
    $email_id = $this->verifyWhatEmailToSend(13, $user);

    $template = $this->db->query("SELECT * FROM email_templates WHERE id={$email_id}")->row();

    $stats = $this->users->getStatsForEmail($email);
    $template->content = $this->replaceContent($template->content, $stats);

    if (count($template)) {
        $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
        $this->email->to($email);
        $this->email->subject($template->name);
        $this->email->message($template->content);
        $this->email->send();
    }
  }

  public function sendBroadcast($email, $subject, $content, $stats = false) {

    if($stats){
      $content = $this->replaceContent($content, $stats);
    }

    $this->email->to($email);
    $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');

    $this->email->subject($subject);
    $this->email->message($content);

      $id_user = $this->session->userdata('id');
      $data = array(
          'subject'     => $subject,
          'to_email'    => $email,
          'body'        => $content,
          'id_sender'   => (empty($id_user) ? 0 : $id_user),
          'created' => date('Y-m-d H:i:s', time())
      );
      $this->db->insert('emails_report', $data);

    $this->email->send();
  }

  public function instagramFollowersInfo($userId, $profile) {

      $this->email->from('piecekeepers@onepiece.com', 'OnePiece PieceKeepers');
      $this->email->to(array('razvan@agvision.ro','alin@agvision.ro'));
      $this->email->subject("Instagram followers error");
      $this->email->message('User id:'.$userId.' Profile:'.$profile);
      $this->email->send();
  }

  /**
   * Get parent emails.
   *
   * @return [array] [emails]
   */
  public function getParentEmails()
  {
      $this->db->select('*');
      $this->db->from('email_templates');
      $this->db->where('parent_id IS NULL', null, false);
      $this->db->order_by('name');

      return $this->db->get()->result();
  }

  /**
   * Get the countries for email.
   *
   * @param  [int] $email_id [email id]
   * @return [array]         [countries ids]
   */
  public function getCountriesForEmail($email_id)
  {
      $this->db->select('country_id');
      $this->db->from('email_country');
      $this->db->where('email_id', $email_id);

      $countries = $this->db->get()->result();

      $result = array();

      foreach ($countries as $country)
      {
          $result[] = $country->country_id;
      }

      return $result;
  }


  /**
   * Verify if the email is a parent email.
   *
   * @param  [int] $email_id [email id]
   * @return [bool]          [parent email or not]
   */
  public function verifyIfParentEmail($email_id)
  {
      $this->db->select('parent_id');
      $this->db->from('email_templates');
      $this->db->where('id', $email_id);

      $email = $this->db->get()->row();

      if(!empty($email))
      {
          if($email->parent_id == null)
          {
              return true;
          }
      }

      return false;
  }


  /**
   * Get childs emails for a parent email.
   *
   * @param  [int] $parent_id [parent email id]
   * @return [array]          [childs emails]
   */
  public function getChildsEmails($parent_id)
  {
      $this->db->select('*');
      $this->db->from('email_templates');
      $this->db->where('parent_id', $parent_id);

      return $this->db->get()->result();
  }

    /**
     * Get number of childs emails for a parent email.
     *
     * @param  [int] $parent_id [parent email id]
     * @return [int]            [number of childs emails]
     */
    public function getNumberOfChildsEmails($parent_id)
    {
      $this->db->select('*');
      $this->db->from('email_templates');
      $this->db->where('parent_id', $parent_id);

      return $this->db->count_all_results();;
    }

    /**
     * Verify what evrsion of  email will be sent.
     *
     * @param  [int] $email_id  [email id]
     * @param  [User] $user     [user]
     * @return [int]            [email id]
     */
    public function verifyWhatEmailToSend($email_id, $user)
    {
        // Verify if the email is a parent email
        if($this->verifyIfParentEmail($email_id))
        {
            $childs = $this->getNumberOfChildsEmails($email_id);

            // Verify if it has childs emails
            if(!empty($childs))
            {
                 $child_emails = $this->getChildsEmails($email_id);

                 // For each child verify if the user's country is in the selected countries
                 foreach($child_emails as $child)
                 {
                     $countries = $this->getCountriesForEmail($child->id);

                     if(!empty($user))
                     {
                         if(in_array($user->id_country, $countries))
                         {
                             $email_id = $child->id;
                         }
                     }
                 }
            }
        }

        return $email_id;
    }


}

?>
