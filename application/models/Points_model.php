<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Points_model extends MY_Model
{

    public function __construct() {
        parent::__construct();

        $this->load->model('users', 'User');
        $this->load->model('site_config', 'Config');

        $this->table = 'points';
    }

    public function getAll($ids_countries, $type = false){

        switch ($type) {
            case 'sales':
                $type = 1;
                break;

            case 'bonus':
                $type = 2;
                break;

            default:
                $type = false;
                break;
        }

        $this->db->select('p.*, u.first_name, u.last_name');
        $this->db->from('points as p');
        $this->db->join('auth_user as u', 'u.id = p.id_user');
        $this->db->where("u.id_country IN ({$ids_countries})");
        if($type){
            $this->db->where('type', $type);
        }
        $this->db->order_by('created', 'desc');
        return $this->db->get()->result();
    }

    public function getFromUser($id_user) {
        $sales = $this->db->query("SELECT SUM(value) AS total FROM points WHERE type=1 AND id_user={$id_user}")->row();
        $bonus = $this->db->query("SELECT SUM(value) AS total FROM points WHERE type=2 AND id_user={$id_user}")->row();

        $result = array(
                'sales' => $sales->total,
                'bonus' => $bonus->total,
                'total' => $sales->total + $bonus->total
            );

        return $result;
    }

    public function insert($id_user, $value, $type, $created = false, $order_id = false, $bonus_type = false) {
        $data = array(
                'id_user' => $id_user,
                'value' => $value,
                'type' => $type
            );

        if($order_id){
            $data['order_id'] = $order_id;
        }

        if($bonus_type){
            $data['bonus_type'] = $bonus_type;
        }

        if($created){
            $data['created'] = $created;
        } else {
            $this->db->set('created', 'NOW()', false);
        }
        $this->db->insert($this->table, $data);
    }

    public function getValue($amount, $id_user) {
        $user     = $this->User->get($id_user);
        $settings = $this->Config->getGeneralSettings();

        switch ($user['level']) {
            case '-2':
                $percent = floatval($settings['sales_points_level1']);
                break;

            case '-1':
                $percent = floatval($settings['sales_points_level2']);
                break;

            case '0':
            case '1':
            default:
                $percent = floatval($settings['sales_points_level3']);
                break;
        }

        return intval( ($percent / 100) * $amount);
    }

    public function getTotalGlobally($starting_date = false, $ending_date = false) {

        $this->load->driver('cache');
        $cached = false;

        if ($starting_date && $ending_date) {
            $sql = "SELECT SUM(value) AS total FROM points WHERE created >= '{$starting_date}' AND created <= '{$ending_date}'";
        }
        else if ($starting_date) {
            $sql = "SELECT SUM(value) AS total FROM points WHERE created >= '{$starting_date}'";
        }
        else if ($ending_date) {
            $sql = "SELECT SUM(value) AS total FROM points WHERE created <= '{$ending_date}'";
        }
        else {
            $sql = "SELECT SUM(value) AS total FROM points";
        }

        // get from cache
        if ($this->cache->memcached->is_supported()) {

            $ranking = $this->cache->memcached->get('total_points_globally_'.md5($sql));

            if ($ranking) {
                $cached = true;
            }
        }

        if ($cached) {
            return $ranking;
        }

        // Execute query
        $data = $this->db->query($sql)->row();

        // save to cache
        if ($this->cache->memcached->is_supported()) {
            $this->cache->memcached->save('total_points_globally_'.md5($sql), $data->total, MEMCACHED_LIFETIIME);
        }

        return $data->total;
    }

    public function getTotalByCountry($id_country, $starting_date = false, $ending_date = false) {

        $this->load->driver('cache');
        $cached = false;

        if ($starting_date && $ending_date) {
            $sql = "SELECT SUM(p.value) AS total FROM points AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE u.id_country={$id_country} AND created >= '{$starting_date}' AND created <= '{$ending_date}'";
        }
        else if ($starting_date) {
            $sql = "SELECT SUM(p.value) AS total FROM points AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE u.id_country={$id_country} AND created >= '{$starting_date}'";
        }
        else if ($ending_date) {
            $sql = "SELECT SUM(p.value) AS total FROM points AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE u.id_country={$id_country} AND created <= '{$ending_date}'";
        }
        else {
            $sql = "SELECT SUM(p.value) AS total FROM points AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE u.id_country={$id_country}";
        }

        // get from cache
        if ($this->cache->memcached->is_supported()) {

            $ranking = $this->cache->memcached->get('total_points_country_'.md5($sql));

            if ($ranking) {
                $cached = true;
            }
        }

        if ($cached) {
            return $ranking;
        }

        // Execute query
        $data = $this->db->query($sql)->row();

        // save to cache
        if ($this->cache->memcached->is_supported()) {
            $this->cache->memcached->save('total_points_country_'.md5($sql), $data->total, MEMCACHED_LIFETIIME);
        }


        return $data->total;
    }

    public function getTotalByCity($city, $starting_date = false, $ending_date = false) {

        $this->load->driver('cache');
        $cached = false;

        if ($starting_date && $ending_date) {
            $sql = "SELECT SUM(p.value) AS total FROM points AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE u.city='{$city}' AND created >= '{$starting_date}' AND created <= '{$ending_date}'";
        }
        else if ($starting_date) {
            $sql = "SELECT SUM(p.value) AS total FROM points AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE u.city='{$city}' AND created >= '{$starting_date}'";
        }
        else if ($ending_date) {
            $sql = "SELECT SUM(p.value) AS total FROM points AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE u.city='{$city}' AND created <= '{$ending_date}'";
        }
        else {
            $sql = "SELECT SUM(p.value) AS total FROM points AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE u.city='{$city}'";
        }

        // get from cache
        if ($this->cache->memcached->is_supported()) {

            $ranking = $this->cache->memcached->get('total_points_city_'.md5($sql));

            if ($ranking) {
                $cached = true;
            }
        }

        if ($cached) {
            return $ranking;
        }

        // Execute query
        $data = $this->db->query($sql)->row();

        // save to cache
        if ($this->cache->memcached->is_supported()) {
            $this->cache->memcached->save('total_points_city_'.md5($sql), $data->total, MEMCACHED_LIFETIIME);
        }

        return $data->total;
    }
}

?>
