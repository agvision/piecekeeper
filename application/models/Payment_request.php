<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_request extends MY_Model 
{
    
    function __construct() {
        parent::__construct();

        $this->load->model('users', 'User');

        $this->table = "payment_requests";
    }
    
    public function insert() {
        $data = array(
            'amount' => $this->amount,
            'id_user' => $this->id_user,
        );
        $this->db->set('created', 'NOW()', FALSE);
        $this->db->insert($this->table,$data);
    }

    
    public function getItem($id) {
        $query = $this->db->query("SELECT * FROM payment_requests WHERE id={$id}");

        $db_row       = $query->row();
        $n            = new Payment_request();
        $n->id        = $id;
        $n->id_user   = $db_row->id_user;
        $n->amount    = $db_row->amount;
        $n->created   = $db_row->created;
        $n->aproved   = $db_row->aproved;

        return $n;              
    }
    
    public function aprove($id = false) {
        if(!$id) {
            $id = $this->id;
            $payment = $this;
        } else {
            $payment = $this->getItem($id);
        }
        if($payment->aproved!=0) {
            return;
        }
        $this->db->set("aproved", '1');
        $this->db->where("id", $id);
        $this->db->update($this->table);

        $this->load->model("notification");

        $user_notification = $this->notification;
        $user_notification->id_user = $payment->id_user;
        $user_notification->type = 9;
        $user_notification->param = $payment->id;
        $user_notification->insert();

        $this->load->model("users");
        $user = $this->User->get($payment->id_user);

        $this->load->model("email_model", "Email");
        $this->Email->paymentApproved( $user['email'] );

    }
    
    public function getItems($nr = false) {
        $query = $this->db->query("SELECT * FROM payment_requests ORDER BY created DESC " . ($nr ? "LIMIT $nr" : ""));
        $db_rows       = $query->result();
        $items = array();
        foreach($db_rows as $db_row) {
            $n            = new Payment_request();
            $n->id        = $id;
            $n->id_user   = $db_row->id_user;
            $n->amount    = $db_row->amount;
            $n->created   = $db_row->created;
            $n->aproved   = $db_row->aproved;
            $items[] = $n;
        }
        return $items;   
    }

    public function getApprovedItems($id_user, $nr = false){
        $query   = $this->db->query("SELECT * FROM payment_requests WHERE aproved='1' and id_user='".$id_user."' ORDER BY created desc" . ($nr ? "LIMIT $nr" : ""));
        $db_rows = $query->result();

        return $db_rows;
    }


    public function getUserItems($id_user, $nr = false){
        $query   = $this->db->query("SELECT * FROM payment_requests WHERE id_user='".$id_user."' ORDER BY created desc" . ($nr ? "LIMIT $nr" : ""));
        $db_rows = $query->result();

        return $db_rows;
    }

    public function delete($id){
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }
    
}
?>