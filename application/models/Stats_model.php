<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stats_model extends MY_Model
{

  function __construct()
  {
    parent::__construct();
    $this->load->driver('cache');
    $this->load->model('Scratch_coupon_model', 'Scratch');
    $this->max_results = 31;
  }


  public function getPKStatsByTotalSales($country, $city, $begin, $end, $ids_countries){
    $id_user = $this->session->userdata('id');
    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }
    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT COUNT(*) as total_sum, u.first_name, u.last_name, oc.id_user FROM orders AS o ".
            "INNER JOIN orders_coupons AS oc ON oc.order_id=o.order_id ".
            "INNER JOIN auth_user AS u ON u.id=oc.id_user ".
            "WHERE o.created>='{$begin}' AND o.created<='{$end}' ".
            "AND u.is_active=1 AND u.deleted=0 AND u.completed=1 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }
    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }
    $sql .= "GROUP BY oc.id_user ORDER BY total_sum DESC LIMIT ".$this->max_results;

    $result  = $this->db->query($sql);
    $results = $result->result_array();

    $pk_stats = array();
    $total = 0;

    foreach ($results as $result) {
       $pk_stats['keys'][] = "'".$result['first_name'].' '.$result['last_name']."'";
       $pk_stats['values'][] = $result['total_sum'];
       $total += $result['total_sum'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $pk_stats['total'] = $total;
    $pk_stats['graph_title'] = "PieceKeepers Total Sales from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $pk_stats['y_axis_label'] = "Total Sales";

    return $pk_stats;
  }


  public function getPKStatsByRevenue($country, $city, $begin, $end, $ids_countries){
    $id_user = $this->session->userdata('id');
    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }
    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT SUM(o.total) as total_sum, u.first_name, u.last_name, oc.id_user FROM orders AS o ".
            "INNER JOIN orders_coupons AS oc ON oc.order_id=o.order_id ".
            "INNER JOIN auth_user AS u ON u.id=oc.id_user ".
            "WHERE o.created>='{$begin}' AND o.created<='{$end}' ".
            "AND u.is_active=1 AND u.deleted=0 AND u.completed=1 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }
    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }
    $sql .= "GROUP BY oc.id_user ORDER BY total_sum DESC LIMIT ".$this->max_results;

    $result  = $this->db->query($sql);
    $results = $result->result_array();

    $pk_stats = array();
    $total = 0;

    foreach ($results as $result) {
       $pk_stats['keys'][] = "'".$result['first_name'].' '.$result['last_name']."'";
       $pk_stats['values'][] = $result['total_sum'];
       $total += $result['total_sum'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $pk_stats['total'] = $total;
    $pk_stats['graph_title'] = "PieceKeepers Generated Revenue from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $pk_stats['y_axis_label'] = "Generated Revenue";

    return $pk_stats;
  }


  public function getPKStatsByBonusPoints($country, $city, $begin, $end, $ids_countries){
    $id_user = $this->session->userdata('id');
    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }
    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT SUM(p.value) as total_sum, u.first_name, u.last_name, p.id_user FROM points AS p ".
            "INNER JOIN auth_user AS u ON u.id=p.id_user ".
            "WHERE p.created>='{$begin}' AND p.created<='{$end}' ".
            "AND p.type=2 AND u.is_active=1 AND u.deleted=0 AND u.completed=1 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }
    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }
    $sql .= "GROUP BY p.id_user ORDER BY total_sum DESC LIMIT ".$this->max_results;

    $result  = $this->db->query($sql);
    $results = $result->result_array();

    $pk_stats = array();
    $total = 0;

    foreach ($results as $result) {
       $pk_stats['keys'][] = "'".$result['first_name'].' '.$result['last_name']."'";
       $pk_stats['values'][] = $result['total_sum'];
       $total += $result['total_sum'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $pk_stats['total'] = $total;
    $pk_stats['graph_title'] = "PieceKeepers Bonus Points from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $pk_stats['y_axis_label'] = "Bonus Points";

    return $pk_stats;
  }


  public function getPKStatsBySalesPoints($country, $city, $begin, $end, $ids_countries){
    $id_user = $this->session->userdata('id');
    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }
    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT SUM(p.value) as total_sum, u.first_name, u.last_name, p.id_user FROM points AS p ".
            "INNER JOIN auth_user AS u ON u.id=p.id_user ".
            "WHERE p.created>='{$begin}' AND p.created<='{$end}' ".
            "AND p.type=1 AND u.is_active=1 AND u.deleted=0 AND u.completed=1 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }
    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }
    $sql .= "GROUP BY p.id_user ORDER BY total_sum DESC LIMIT ".$this->max_results;

    $result  = $this->db->query($sql);
    $results = $result->result_array();

    $pk_stats = array();
    $total = 0;

    foreach ($results as $result) {
       $pk_stats['keys'][] = "'".$result['first_name'].' '.$result['last_name']."'";
       $pk_stats['values'][] = $result['total_sum'];
       $total += $result['total_sum'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $pk_stats['total'] = $total;
    $pk_stats['graph_title'] = "PieceKeepers Bonus Points from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $pk_stats['y_axis_label'] = "Bonus Points";

    return $pk_stats;
  }


  public function getPKStatsByCommission($country, $city, $begin, $end, $ids_countries){
    $id_user = $this->session->userdata('id');
    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }
    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT SUM(c.value) as total_sum, u.first_name, u.last_name, c.id_user FROM commissions AS c ".
            "INNER JOIN auth_user AS u ON u.id=c.id_user ".
            "WHERE c.created>='{$begin}' AND c.created<='{$end}' ".
            "AND u.is_active=1 AND u.deleted=0 AND u.completed=1 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }
    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }
    $sql .= "GROUP BY c.id_user ORDER BY total_sum DESC LIMIT ".$this->max_results;

    $result  = $this->db->query($sql);
    $results = $result->result_array();

    $pk_stats = array();
    $total = 0;

    foreach ($results as $result) {
       $pk_stats['keys'][] = "'".$result['first_name'].' '.$result['last_name']."'";
       $pk_stats['values'][] = $result['total_sum'];
       $total += $result['total_sum'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $pk_stats['total'] = $total;
    $pk_stats['graph_title'] = "PieceKeepers Commission from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $pk_stats['y_axis_label'] = "Commission";

    return $pk_stats;
  }


  public function getPKStatsByTotalPoints($country, $city, $begin, $end, $ids_countries){
    $id_user = $this->session->userdata('id');
    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }
    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT SUM(p.value) as total_sum, u.first_name, u.last_name, p.id_user FROM points AS p ".
            "INNER JOIN auth_user AS u ON u.id=p.id_user ".
            "WHERE p.created>='{$begin}' AND p.created<='{$end}' ".
            "AND u.is_active=1 AND u.deleted=0 AND u.completed=1 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }
    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }
    $sql .= "GROUP BY p.id_user ORDER BY total_sum DESC LIMIT ".$this->max_results;

    $result  = $this->db->query($sql);
    $results = $result->result_array();

    $pk_stats = array();
    $total = 0;

    foreach ($results as $result) {
       $pk_stats['keys'][] = "'".$result['first_name'].' '.$result['last_name']."'";
       $pk_stats['values'][] = $result['total_sum'];
       $total += $result['total_sum'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $pk_stats['total'] = $total;
    $pk_stats['graph_title'] = "PieceKeepers Bonus Points from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $pk_stats['y_axis_label'] = "Bonus Points";

    return $pk_stats;
  }


  public function getBonusPointsStats($country, $city, $begin, $end, $ids_countries, $frequency = false){
    $id_user = $this->session->userdata('id');
    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }
    if (!$end) {
       $end = time();
    }

    $begin_timestamp = $begin;
    $end_timestamp   = $end;

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    if($frequency == "day"){
        $frequency_sql = "CONCAT(DAY(p.created), ' ', MONTHNAME(p.created), ' ', YEAR(p.created))";
    }
    else if($frequency == "week"){
        $frequency_sql = "CONCAT(WEEK(p.created), ' ', YEAR(p.created))";
    } else {
        $frequency_sql = "MONTH(p.created)";
    }

    $sql =  "SELECT SUM(p.value) as total_sum, ".$frequency_sql." as frequency, p.created FROM points AS p ".
            "INNER JOIN auth_user AS u ON u.id=p.id_user ".
            "WHERE p.created>='{$begin}' AND p.created<='{$end}' AND p.type=2 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }
    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }
    $sql .= "GROUP BY frequency ORDER BY CREATED ASC LIMIT ".$this->max_results;

    $result  = $this->db->query($sql);
    $results = $result->result_array();

    $sales_points_stats = array();
    $total = 0;

    foreach ($results as $result) {
        $sales_points_stats['values'][] = $result['total_sum'];

        if($frequency == "day"){
            $sales_points_stats['keys'][] = "'".date('d M Y', strtotime($result['created']))."'";
        }
        else if($frequency == "week"){
            $data = explode(' ', $result['frequency']);
            $week = $data[0];
            $year = $data[1];

            $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
            $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
            $timestamp_for_sunday = $timestamp_for_monday + 3600 * 24 * 6;

            if($begin_timestamp >= $timestamp_for_monday && $begin_timestamp <= $timestamp_for_sunday){
                $timestamp_for_monday = $begin_timestamp;
            }
            if($end_timestamp <= $timestamp_for_sunday && $end_timestamp >= $timestamp_for_monday){
                $timestamp_for_sunday = $end_timestamp;
            }
            if(time() >= $timestamp_for_monday && time() <= $timestamp_for_sunday){
                $timestamp_for_sunday = time();
            }

            $date_for_monday = date( 'd M Y', $timestamp_for_monday );
            $label_for_monday = date( 'd M', $timestamp_for_monday );
            $date_for_sunday = date( 'd M Y', $timestamp_for_sunday );
            $label_for_sunday = date( 'd M', $timestamp_for_sunday );

            $sales_points_stats['keys'][] = "'".$label_for_monday."-".$label_for_sunday."'";
        } else {
            $sales_points_stats['keys'][] = "'".date('M Y', strtotime($result['created']))."'";
        }

        $total += $result['total_sum'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_points_stats['total'] = $total;
    $sales_points_stats['graph_title'] = "Bonus Points from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_points_stats['y_axis_label'] = "Bonus Points";

    return $sales_points_stats;
  }


  public function getBonusPointsStatsByCountry($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT u.country, SUM(p.value) as total FROM points AS p ".
            "INNER JOIN auth_user AS u ON u.id=p.id_user ".
            "WHERE p.created>='{$begin}' AND p.created<='{$end}' AND p.type=2 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.country ORDER BY total DESC";

    $query = $this->db->query($sql);

    $results = $query->result_array();

    $stats_no = count($results);
    $sales_points_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
      if (strlen($result['country']) == 0) {
        $sales_points_stats['keys'][] = "'Others'";
        $sales_points_stats['values'][] = $result['total'];
      } else {
        $sales_points_stats['keys'][] = "'".$result['country']."'";
        $sales_points_stats['values'][] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_points_stats['total'] = $total;
    $sales_points_stats['graph_title'] = "Bonus Points by Country from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_points_stats['y_axis_label'] = "Bonus Points";

    return $sales_points_stats;
  }


  public function getBonusPointsStatsByTag($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT ut.tag, SUM(p.value) as total FROM points AS p ".
            "INNER JOIN auth_user AS u ON u.id=p.id_user ".
            "INNER JOIN user_tags AS ut ON ut.id_user=u.id ".
            "WHERE p.created>='{$begin}' AND p.created<='{$end}' AND p.type=2 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY ut.tag ORDER BY total DESC";

    $query = $this->db->query($sql);

    $results = $query->result_array();

    $stats_no = count($results);
    $sales_points_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
        $sales_points_stats['keys'][] = "'".$result['tag']."'";
        $sales_points_stats['values'][] = $result['total'];

        $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_points_stats['total'] = $total;
    $sales_points_stats['graph_title'] = "Bonus Points by Tag from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_points_stats['y_axis_label'] = "Bonus Points";

    return $sales_points_stats;
  }


  public function getBonusPointsStatsByCity($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT u.city, SUM(p.value) as total FROM points AS p ".
            "INNER JOIN auth_user AS u ON u.id=p.id_user ".
            "WHERE p.created>='{$begin}' AND p.created<='{$end}' AND p.type=2 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.city ORDER BY total DESC";

    $query = $this->db->query($sql);

    $results = $query->result_array();

    $stats_no = count($results);
    $sales_points_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
      if (strlen($result['city']) == 0) {
        $sales_points_stats['keys'][] = "'Others'";
        $sales_points_stats['values'][] = $result['total'];
      } else {
        $sales_points_stats['keys'][] = "'".$result['city']."'";
        $sales_points_stats['values'][] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_points_stats['total'] = $total;
    $sales_points_stats['graph_title'] = "Bonus Points by City from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_points_stats['y_axis_label'] = "Bonus Points";

    return $sales_points_stats;
  }


  public function getBonusPointsStatsBySchool($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT u.school, SUM(p.value) as total FROM points AS p ".
            "INNER JOIN auth_user AS u ON u.id=p.id_user ".
            "WHERE p.created>='{$begin}' AND p.created<='{$end}' AND p.type=2 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.school ORDER BY total DESC";

    $query = $this->db->query($sql);

    $results = $query->result_array();

    $stats_no = count($results);
    $sales_points_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
      if (strlen($result['school']) == 0) {
        $sales_points_stats['keys'][] = "'Others'";
        $sales_points_stats['values'][] = $result['total'];
      } else {
        $sales_points_stats['keys'][] = "'".$result['school']."'";
        $sales_points_stats['values'][] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_points_stats['total'] = $total;
    $sales_points_stats['graph_title'] = "Bonus Points by School from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_points_stats['y_axis_label'] = "Bonus Points";

    return $sales_points_stats;
  }


  public function getBonusPointsStatsByGender($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);


    $sql =  "SELECT u.gender, SUM(p.value) as total FROM points AS p ".
            "INNER JOIN auth_user AS u ON u.id=p.id_user ".
            "WHERE u.gender IN (0, 1) AND p.created>='{$begin}' AND p.created<='{$end}' AND p.type=2 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.gender ORDER BY u.gender";

    $query = $this->db->query($sql);

    $results = $query->result_array();

    $stats_no = count($results);
    $sales_stats = array();
    $total = 0;

    $sales_stats['keys'][0] = "'Women'";
    $sales_stats['keys'][1] = "'Men'";

    $sales_stats['values'][0] = "0";
    $sales_stats['values'][1] = "0";

    foreach ($results as $result) {
      if ($result['gender'] == 0) {
        $sales_stats['values'][0] = $result['total'];
      } else {
        $sales_stats['values'][1] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_stats['total'] = $total;
    $sales_stats['graph_title'] = "Bonus Points by Gender from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_stats['y_axis_label'] = "Bonus Points";

    return $sales_stats;
  }


  public function getSalesPointsStats($country, $city, $begin, $end, $ids_countries, $frequency = false){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }
    if (!$end) {
       $end = time();
    }

    $begin_timestamp = $begin;
    $end_timestamp   = $end;

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    if($frequency == "day"){
        $frequency_sql = "CONCAT(DAY(p.created), ' ', MONTHNAME(p.created), ' ', YEAR(p.created))";
    }
    else if($frequency == "week"){
        $frequency_sql = "CONCAT(WEEK(p.created), ' ', YEAR(p.created))";
    } else {
        $frequency_sql = "MONTH(p.created)";
    }

    $sql =  "SELECT SUM(p.value) as total_sum, ".$frequency_sql." as frequency, p.created FROM points AS p ".
            "INNER JOIN auth_user AS u ON u.id=p.id_user ".
            "WHERE p.created>='{$begin}' AND p.created<='{$end}' AND p.type=1 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY frequency ORDER BY CREATED ASC LIMIT ".$this->max_results;

    $result  = $this->db->query($sql);
    $results = $result->result_array();

    $sales_points_stats = array();
    $total = 0;

    foreach ($results as $result) {
       $sales_points_stats['values'][] = $result['total_sum'];

       if($frequency == "day"){
           $sales_points_stats['keys'][] = "'".date('d M Y', strtotime($result['created']))."'";
       }
       else if($frequency == "week"){
           $data = explode(' ', $result['frequency']);
           $week = $data[0];
           $year = $data[1];

           $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
           $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
           $timestamp_for_sunday = $timestamp_for_monday + 3600 * 24 * 6;

           if($begin_timestamp >= $timestamp_for_monday && $begin_timestamp <= $timestamp_for_sunday){
               $timestamp_for_monday = $begin_timestamp;
           }
           if($end_timestamp <= $timestamp_for_sunday && $end_timestamp >= $timestamp_for_monday){
               $timestamp_for_sunday = $end_timestamp;
           }
           if(time() >= $timestamp_for_monday && time() <= $timestamp_for_sunday){
               $timestamp_for_sunday = time();
           }

           $date_for_monday = date( 'd M Y', $timestamp_for_monday );
           $label_for_monday = date( 'd M', $timestamp_for_monday );
           $date_for_sunday = date( 'd M Y', $timestamp_for_sunday );
           $label_for_sunday = date( 'd M', $timestamp_for_sunday );

           $sales_points_stats['keys'][] = "'".$label_for_monday."-".$label_for_sunday."'";
       } else {
            $sales_points_stats['keys'][] = "'".date('M Y', strtotime($result['created']))."'";
       }

       $total += $result['total_sum'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_points_stats['total'] = $total;
    $sales_points_stats['graph_title'] = "Sales Points from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_points_stats['y_axis_label'] = "Sales Points";

    return $sales_points_stats;
  }


  public function getSalesPointsStatsByTag($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT SUM(p.value) as total, ut.tag AS tag, p.created FROM points AS p ".
            "INNER JOIN auth_user AS u ON u.id=p.id_user ".
            "INNER JOIN user_tags AS ut ON ut.id_user=u.id ".
            "WHERE p.created>='{$begin}' AND p.created<='{$end}' AND p.type=1 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY ut.tag ORDER BY total DESC";

    $query = $this->db->query($sql);
    $results = $query->result_array();

    $stats_no = count($results);
    $sales_points_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
        $sales_points_stats['keys'][] = "'".$result['tag']."'";
        $sales_points_stats['values'][] = $result['total'];

        $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_points_stats['total'] = $total;
    $sales_points_stats['graph_title'] = "Sales Points by Tag from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_points_stats['y_axis_label'] = "Sales Points";

    return $sales_points_stats;
  }


  public function getSalesPointsStatsByCoupon($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT SUM(p.value)*count(DISTINCT c.id)/count(*) as total, c.coupon AS coupon, p.created FROM points AS p ".
            "INNER JOIN auth_user AS u ON u.id=p.id_user ".
            "INNER JOIN orders_coupons AS c ON c.id_user=u.id ".
            "WHERE p.created>='{$begin}' AND p.created<='{$end}' AND p.type=1 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY c.coupon ORDER BY total DESC";

    $query = $this->db->query($sql);
    $results = $query->result_array();

    $stats_no = count($results);
    $sales_points_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
      if (strlen($result['coupon']) == 0) {
        $sales_points_stats['keys'][] = "'Others'";
        $sales_points_stats['values'][] = $result['total'];
      } else {
        $sales_points_stats['keys'][] = "'".$result['coupon']."'";
        $sales_points_stats['values'][] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_points_stats['total'] = $total;
    $sales_points_stats['graph_title'] = "Sales Points by Coupon from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_points_stats['y_axis_label'] = "Sales Points";

    return $sales_points_stats;
  }


  public function getSalesPointsStatsByCountry($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT u.country, SUM(p.value) as total FROM points AS p ".
            "INNER JOIN auth_user AS u ON u.id=p.id_user ".
            "WHERE p.created>='{$begin}' AND p.created<='{$end}' AND p.type=1 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.country ORDER BY total DESC";

    $query = $this->db->query($sql);

    $results = $query->result_array();

    $stats_no = count($results);
    $sales_points_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
      if (strlen($result['country']) == 0) {
        $sales_points_stats['keys'][] = "'Others'";
        $sales_points_stats['values'][] = $result['total'];
      } else {
        $sales_points_stats['keys'][] = "'".$result['country']."'";
        $sales_points_stats['values'][] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_points_stats['total'] = $total;
    $sales_points_stats['graph_title'] = "Sales Points by Country from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_points_stats['y_axis_label'] = "Sales Points";

    return $sales_points_stats;
  }


  public function getSalesPointsStatsByCity($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT u.city, SUM(p.value) as total FROM points AS p ".
            "INNER JOIN auth_user AS u ON u.id=p.id_user ".
            "WHERE p.created>='{$begin}' AND p.created<='{$end}' AND p.type=1 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.city ORDER BY total DESC";

    $query = $this->db->query($sql);

    $results = $query->result_array();

    $stats_no = count($results);
    $sales_points_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
      if (strlen($result['city']) == 0) {
        $sales_points_stats['keys'][] = "'Others'";
        $sales_points_stats['values'][] = $result['total'];
      } else {
        $sales_points_stats['keys'][] = "'".$result['city']."'";
        $sales_points_stats['values'][] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_points_stats['total'] = $total;
    $sales_points_stats['graph_title'] = "Sales Points by City from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_points_stats['y_axis_label'] = "Sales Points";

    return $sales_points_stats;
  }


  public function getSalesPointsStatsBySchool($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT u.school, SUM(p.value) as total FROM points AS p ".
            "INNER JOIN auth_user AS u ON u.id=p.id_user ".
            "WHERE p.created>='{$begin}' AND p.created<='{$end}' AND p.type=1 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.school ORDER BY total DESC";

    $query = $this->db->query($sql);

    $results = $query->result_array();

    $stats_no = count($results);
    $sales_points_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
      if (strlen($result['school']) == 0) {
        $sales_points_stats['keys'][] = "'Others'";
        $sales_points_stats['values'][] = $result['total'];
      } else {
        $sales_points_stats['keys'][] = "'".$result['school']."'";
        $sales_points_stats['values'][] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_points_stats['total'] = $total;
    $sales_points_stats['graph_title'] = "Sales Points by School from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_points_stats['y_axis_label'] = "Sales Points";

    return $sales_points_stats;
  }


  public function getSalesPointsStatsByGender($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);


    $sql =  "SELECT u.gender, SUM(p.value) as total FROM points AS p ".
            "INNER JOIN auth_user AS u ON u.id=p.id_user ".
            "WHERE u.gender IN (0, 1) AND p.created>='{$begin}' AND p.created<='{$end}' AND p.type=1 ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.gender ORDER BY u.gender";

    $query = $this->db->query($sql);

    $results = $query->result_array();

    $stats_no = count($results);
    $sales_stats = array();
    $total = 0;

    $sales_stats['keys'][0] = "'Women'";
    $sales_stats['keys'][1] = "'Men'";

    $sales_stats['values'][0] = "0";
    $sales_stats['values'][1] = "0";

    foreach ($results as $result) {
      if ($result['gender'] == 0) {
        $sales_stats['values'][0] = $result['total'];
      } else {
        $sales_stats['values'][1] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_stats['total'] = $total;
    $sales_stats['graph_title'] = "Sales Points by Gender from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_stats['y_axis_label'] = "Sales Points";

    return $sales_stats;
  }


  public function getCommissionsStats($country, $city, $begin, $end, $ids_countries, $frequency = false){

    $id_user = $this->session->userdata('id');

    $months = array("","Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin_timestamp = $begin;
    $end_timestamp   = $end;

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    if($frequency == "day"){
        $frequency_sql = "CONCAT(DAY(c.created), ' ', MONTHNAME(c.created), ' ', YEAR(c.created))";
    }
    else if($frequency == "week"){
        $frequency_sql = "CONCAT(WEEK(c.created), ' ', YEAR(c.created))";
    } else {
        $frequency_sql = "MONTH(c.created)";
    }

    $sql =  "SELECT SUM(c.value) as total_sum, ".$frequency_sql." as frequency, c.created FROM commissions AS c ".
            "INNER JOIN auth_user AS u ON u.id=c.id_user ".
            "WHERE c.created>='{$begin}' AND c.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY frequency ORDER BY CREATED ASC LIMIT ".$this->max_results;

    $result  = $this->db->query($sql);
    $results = $result->result_array();

    $commissions_stats = array();
    $total = 0;

    foreach ($results as $result) {
        $commissions_stats['values'][] = $result['total_sum'];

        if($frequency == "day"){
            $commissions_stats['keys'][] = "'".date('d M Y', strtotime($result['created']))."'";
        }
        else if($frequency == "week"){
            $data = explode(' ', $result['frequency']);
            $week = $data[0];
            $year = $data[1];

            $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
            $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
            $timestamp_for_sunday = $timestamp_for_monday + 3600 * 24 * 6;

            if($begin_timestamp >= $timestamp_for_monday && $begin_timestamp <= $timestamp_for_sunday){
                $timestamp_for_monday = $begin_timestamp;
            }
            if($end_timestamp <= $timestamp_for_sunday && $end_timestamp >= $timestamp_for_monday){
                $timestamp_for_sunday = $end_timestamp;
            }
            if(time() >= $timestamp_for_monday && time() <= $timestamp_for_sunday){
                $timestamp_for_sunday = time();
            }

            $date_for_monday = date( 'd M Y', $timestamp_for_monday );
            $label_for_monday = date( 'd M', $timestamp_for_monday );
            $date_for_sunday = date( 'd M Y', $timestamp_for_sunday );
            $label_for_sunday = date( 'd M', $timestamp_for_sunday );

            $commissions_stats['keys'][] = "'".$label_for_monday."-".$label_for_sunday."'";
        } else {
            $commissions_stats['keys'][] = "'".date('M Y', strtotime($result['created']))."'";
        }

        $total += $result['total_sum'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $commissions_stats['total'] = $total;
    $commissions_stats['graph_title'] = "Commissions from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $commissions_stats['y_axis_label'] = "Commission ($)";

    return $commissions_stats;
  }


  public function getCommissionsStatsByCoupon($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT com.coupon AS coupon, SUM(com.value) as total FROM commissions AS com ".
            "INNER JOIN auth_user AS u ON u.id=com.id_user ".
            "WHERE com.created>='{$begin}' AND com.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY com.coupon ORDER BY total DESC";

    $query = $this->db->query($sql);
    $results = $query->result_array();

    $stats_no = count($results);
    $sales_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
      if (strlen($result['coupon']) == 0) {
        $sales_stats['keys'][] = "'Others'";
        $sales_stats['values'][] = $result['total'];
      } else {
        $sales_stats['keys'][] = "'".$result['coupon']."'";
        $sales_stats['values'][] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_stats['total'] = $total;
    $sales_stats['graph_title'] = "Commission by Coupon from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_stats['y_axis_label'] = "Commission ($)";

    return $sales_stats;
  }


  public function getCommissionsStatsByTag($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT ut.tag AS tag, SUM(com.value) as total FROM commissions AS com ".
            "INNER JOIN auth_user AS u ON u.id=com.id_user ".
            "INNER JOIN user_tags AS ut ON ut.id_user=u.id ".
            "WHERE com.created>='{$begin}' AND com.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY ut.tag ORDER BY total DESC";

    $query = $this->db->query($sql);
    $results = $query->result_array();

    $stats_no = count($results);
    $sales_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
        $sales_stats['keys'][] = "'".$result['tag']."'";
        $sales_stats['values'][] = $result['total'];

        $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_stats['total'] = $total;
    $sales_stats['graph_title'] = "Commission by Tag from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_stats['y_axis_label'] = "Commission ($)";

    return $sales_stats;
  }


  public function getCommissionsStatsByGender($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);


    $sql =  "SELECT u.gender, SUM(com.value) as total FROM commissions AS com ".
            "INNER JOIN auth_user AS u ON u.id=com.id_user ".
            "WHERE u.gender IN (0, 1) AND com.created>='{$begin}' AND com.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.gender ORDER BY u.gender DESC";

    $query = $this->db->query($sql);

    $results = $query->result_array();

    $stats_no = count($results);
    $sales_stats = array();
    $total = 0;

    $sales_stats['keys'][0] = "'Women'";
    $sales_stats['keys'][1] = "'Men'";

    $sales_stats['values'][0] = "0";
    $sales_stats['values'][1] = "0";

    foreach ($results as $result) {
      if ($result['gender'] == 0) {
        $sales_stats['values'][0] = $result['total'];
      } else {
        $sales_stats['values'][1] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_stats['total'] = $total;
    $sales_stats['graph_title'] = "Commission by Gender from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_stats['y_axis_label'] = "Commission ($)";

    return $sales_stats;
  }


  public function getCommissionsStatsByCountry($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT u.country, SUM(com.value) as total FROM commissions AS com ".
            "INNER JOIN auth_user AS u ON u.id=com.id_user ".
            "WHERE u.gender IN (0, 1) AND com.created>='{$begin}' AND com.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.country ORDER BY total DESC";

    $query = $this->db->query($sql);

    // $query = $this->db->query("SELECT u.country, SUM(sales.total) as total FROM sales LEFT JOIN auth_user AS u ON sales.id_user = u.id GROUP BY u.country ORDER BY u.country");

    $results = $query->result_array();

    $stats_no = count($results);
    $sales_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
      if (strlen($result['country']) == 0) {
        $sales_stats['keys'][] = "'Others'";
        $sales_stats['values'][] = $result['total'];
      } else {
        $sales_stats['keys'][] = "'".$result['country']."'";
        $sales_stats['values'][] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_stats['total'] = $total;
    $sales_stats['graph_title'] = "Commission by Country from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_stats['y_axis_label'] = "Commission ($)";

    return $sales_stats;
  }


  public function getCommissionsStatsByCity($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT u.city, SUM(com.value) as total FROM commissions AS com ".
            "INNER JOIN auth_user AS u ON u.id=com.id_user ".
            "WHERE u.gender IN (0, 1) AND com.created>='{$begin}' AND com.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.city ORDER BY total DESC";

    $query = $this->db->query($sql);

    // $query = $this->db->query("SELECT u.country, SUM(sales.total) as total FROM sales LEFT JOIN auth_user AS u ON sales.id_user = u.id GROUP BY u.country ORDER BY u.country");

    $results = $query->result_array();

    $stats_no = count($results);
    $sales_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
      if (strlen($result['city']) == 0) {
        $sales_stats['keys'][] = "'Others'";
        $sales_stats['values'][] = $result['total'];
      } else {
        $sales_stats['keys'][] = "'".$result['city']."'";
        $sales_stats['values'][] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_stats['total'] = $total;
    $sales_stats['graph_title'] = "Commission by City from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_stats['y_axis_label'] = "Commission ($)";

    return $sales_stats;
  }


  public function getCommissionsStatsBySchool($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT u.school, SUM(com.value) as total FROM commissions AS com ".
            "INNER JOIN auth_user AS u ON u.id=com.id_user ".
            "WHERE u.gender IN (0, 1) AND com.created>='{$begin}' AND com.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.school ORDER BY total DESC";

    $query = $this->db->query($sql);

    // $query = $this->db->query("SELECT u.country, SUM(sales.total) as total FROM sales LEFT JOIN auth_user AS u ON sales.id_user = u.id GROUP BY u.country ORDER BY u.country");

    $results = $query->result_array();

    $stats_no = count($results);
    $sales_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
      if (strlen($result['school']) == 0) {
        $sales_stats['keys'][] = "'Others'";
        $sales_stats['values'][] = $result['total'];
      } else {
        $sales_stats['keys'][] = "'".$result['school']."'";
        $sales_stats['values'][] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_stats['total'] = $total;
    $sales_stats['graph_title'] = "Commission by School from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_stats['y_axis_label'] = "Commission ($)";

    return $sales_stats;
  }


  public function getCommissionsStatsByUser($id_user, $begin, $end){

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql =  "SELECT SUM(c.value) as total_sum, MONTH(c.created) as month, c.created FROM commissions AS c ".
            "INNER JOIN auth_user AS u ON u.id=c.id_user ".
            "WHERE c.created>='{$begin}' AND c.created<='{$end}' AND c.type=1 ".
            "AND u.id={$id_user} ".
            "GROUP BY month ORDER BY c.created ASC";

    $result  = $this->db->query($sql);
    $results = $result->result_array();

    $commissions_stats = array();
    $total = 0;

    foreach ($results as $result) {
       $commissions_stats['keys'][] = "'".date('M Y', strtotime($result['created']))."'";
       $commissions_stats['values'][] = $result['total_sum'];
       $total += $result['total_sum'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $commissions_stats['total'] = $total;
    $commissions_stats['graph_title'] = "Commissions from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $commissions_stats['y_axis_label'] = "Commission ($)";

    return $commissions_stats;
  }


  public function getROIStats($country, $city, $begin, $end, $ids_countries, $frequency = false){

    $id_user = $this->session->userdata('id');

    $months = array("","Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin_timestamp = $begin;
    $end_timestamp   = $end;

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    if($frequency == "day"){
        $frequency_sql = "CONCAT(DAY(c.created), ' ', MONTHNAME(c.created), ' ', YEAR(c.created))";
    }
    else if($frequency == "week"){
        $frequency_sql = "CONCAT(WEEK(c.created), ' ', YEAR(c.created))";
    } else {
        $frequency_sql = "MONTH(c.created)";
    }

    // get commission
    $sql = "SELECT SUM(c.value) as total_sum, ".$frequency_sql." as frequency, c.created FROM commissions AS c ".
            "INNER JOIN auth_user AS u ON u.id=c.id_user ".
            "WHERE c.created>='{$begin}' AND c.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY frequency ORDER BY CREATED ASC LIMIT ".$this->max_results;

    $result  = $this->db->query($sql);
    $results = $result->result_array();

    $roi_stats = array();

    $total = 0;
    $roi_stats['commission_values'] = array();

    foreach ($results as $result) {
       $roi_stats['commission_values'][] = $result['total_sum'];

       if($frequency == "day"){
           $roi_stats['keys'][] = "'".date('d M Y', strtotime($result['created']))."'";
       }
       else if($frequency == "week"){
           $data = explode(' ', $result['frequency']);
           $week = $data[0];
           $year = $data[1];

           $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
           $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
           $timestamp_for_sunday = $timestamp_for_monday + 3600 * 24 * 6;

           if($begin_timestamp >= $timestamp_for_monday && $begin_timestamp <= $timestamp_for_sunday){
               $timestamp_for_monday = $begin_timestamp;
           }
           if($end_timestamp <= $timestamp_for_sunday && $end_timestamp >= $timestamp_for_monday){
               $timestamp_for_sunday = $end_timestamp;
           }
           if(time() >= $timestamp_for_monday && time() <= $timestamp_for_sunday){
               $timestamp_for_sunday = time();
           }

           $date_for_monday = date( 'd M Y', $timestamp_for_monday );
           $label_for_monday = date( 'd M', $timestamp_for_monday );
           $date_for_sunday = date( 'd M Y', $timestamp_for_sunday );
           $label_for_sunday = date( 'd M', $timestamp_for_sunday );

           $roi_stats['keys'][] = "'".$label_for_monday."-".$label_for_sunday."'";
       } else {
            $roi_stats['keys'][] = "'".date('M Y', strtotime($result['created']))."'";
       }

       $total += $result['total_sum'];
    }

    $roi_stats['commission_total'] = $total;

    if($frequency == "day"){
        $frequency_sql = "CONCAT(DAY(o.created), ' ', MONTHNAME(o.created), ' ', YEAR(o.created))";
    }
    else if($frequency == "week"){
        $frequency_sql = "CONCAT(WEEK(o.created), ' ', YEAR(o.created))";
    } else {
        $frequency_sql = "MONTH(o.created)";
    }

    // get sales
    $sql = "SELECT SUM(o.total) as total_sum, ".$frequency_sql." as frequency, o.created ".
       "FROM orders_coupons AS oc ".
       "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
       "INNER JOIN coupons AS c ON c.code=oc.coupon ".
       "INNER JOIN auth_user AS u ON u.id=c.id_creator ".
       "WHERE o.created>='{$begin}' AND o.created<='{$end}' ".
       "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY frequency ORDER BY o.created ASC LIMIT ".$this->max_results;

    $query = $this->db->query($sql);
    $results = $query->result_array();

    $total = 0;
    $roi_stats['sales_values'] = array();

    foreach ($results as $result) {
        $roi_stats['sales_values'][] = $result['total_sum'];
        $total += $result['total_sum'];
    }

    $roi_stats['sales_total'] = $total;


    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $roi_stats['graph_title'] = "Values from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $roi_stats['y_axis_label'] = "Values ($)";

    return $roi_stats;
  }


  public function getROIStatsByCoupon($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);


    $sql =  "SELECT SUM(c.value) AS commission, SUM(o.total)*count(DISTINCT o.order_id)/count(*) AS sales, oc.coupon FROM orders_coupons AS oc ".
            "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
            "INNER JOIN commissions AS c ON c.order_id=oc.order_id ".
            "INNER JOIN auth_user AS u ON u.id=oc.id_user ".
            "WHERE oc.created>='{$begin}' AND oc.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY oc.coupon ORDER BY SUM(o.total)*count(DISTINCT o.order_id)/count(*) DESC";

    $result  = $this->db->query($sql);
    $results = $result->result_array();

    $roi_stats = array();

    $total_sales      = 0;
    $total_commission = 0;
    $roi_stats['commission_values'] = array();

    foreach ($results as $result) {
       $roi_stats['keys'][] = "'".$result['coupon']."'";
       $roi_stats['commission_values'][] = number_format($result['commission'], 2, '.', '');
       $roi_stats['sales_values'][] = number_format($result['sales'], 2, '.', '');
       $total_sales += number_format($result['sales'], 2, '.', '');
       $total_commission += number_format($result['commission'], 2, '.', '');
    }

    $roi_stats['commission_total'] = $total_commission;
    $roi_stats['sales_total'] = $total_sales;


    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $roi_stats['graph_title'] = "Values from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $roi_stats['y_axis_label'] = "Values ($)";


    return $roi_stats;
  }


  public function getROIStatsByTag($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);


    $sql =  "SELECT SUM(c.value) AS commission, SUM(o.total)*count(DISTINCT o.order_id)/count(*) AS sales, ut.tag FROM orders_coupons AS oc ".
            "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
            "INNER JOIN commissions AS c ON c.order_id=oc.order_id ".
            "INNER JOIN auth_user AS u ON u.id=oc.id_user ".
            "INNER JOIN user_tags AS ut ON ut.id_user=u.id ".
            "WHERE oc.created>='{$begin}' AND oc.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY ut.tag ORDER BY SUM(o.total)*count(DISTINCT o.order_id)/count(*) DESC";

    $result  = $this->db->query($sql);
    $results = $result->result_array();

    $roi_stats = array();

    $total_sales      = 0;
    $total_commission = 0;
    $roi_stats['commission_values'] = array();

    foreach ($results as $result) {
       $roi_stats['keys'][] = "'".$result['tag']."'";
       $roi_stats['commission_values'][] = number_format($result['commission'], 2, '.', '');
       $roi_stats['sales_values'][] = number_format($result['sales'], 2, '.', '');
       $total_sales += number_format($result['sales'], 2, '.', '');
       $total_commission += number_format($result['commission'], 2, '.', '');
    }

    $roi_stats['commission_total'] = $total_commission;
    $roi_stats['sales_total'] = $total_sales;


    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $roi_stats['graph_title'] = "Values from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $roi_stats['y_axis_label'] = "Values ($)";


    return $roi_stats;
  }


  public function getROIStatsByGender($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);


    $sql =  "SELECT SUM(c.value) AS commission, SUM(o.total)*count(DISTINCT o.order_id)/count(*) AS sales, u.gender FROM orders_coupons AS oc ".
            "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
            "INNER JOIN commissions AS c ON c.order_id=oc.order_id ".
            "INNER JOIN auth_user AS u ON u.id=oc.id_user ".
            "WHERE u.gender IN (0, 1) AND oc.created>='{$begin}' AND oc.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.gender ORDER BY u.gender";

    $result  = $this->db->query($sql);
    $results = $result->result_array();

    $total_sales = 0;
    $total_commission = 0;
    $roi_stats['sales_values'] = array();
    $roi_stats['commission_values'] = array();

    $roi_stats['sales_values'][0] = "0";
    $roi_stats['sales_values'][1] = "0";
    $roi_stats['commission_values'][0] = "0";
    $roi_stats['commission_values'][1] = "0";

    $roi_stats['keys'][0] = "'Women'";
    $roi_stats['keys'][1] = "'Men'";

    foreach ($results as $result) {
      if ($result['gender'] == 0) {
          $roi_stats['sales_values'][0] = number_format($result['sales'], 2, '.', '');
          $roi_stats['commission_values'][0] = number_format($result['commission'], 2, '.', '');
      } else {
          $roi_stats['sales_values'][1] = number_format($result['sales'], 2, '.', '');
          $roi_stats['commission_values'][1] = number_format($result['commission'], 2, '.', '');
      }

        $total_sales += number_format($result['sales'], 2, '.', '');
        $total_commission += number_format($result['commission'], 2, '.', '');
    }

    $roi_stats['sales_total'] = $total_sales;
    $roi_stats['commission_total'] = $total_commission;


    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $roi_stats['graph_title'] = "Values from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $roi_stats['y_axis_label'] = "Values ($)";

    return $roi_stats;
  }


  public function getROIStatsByCountry($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);


    $sql =  "SELECT SUM(c.value) AS commission, SUM(o.total)*count(DISTINCT o.order_id)/count(*) AS sales, cou.short_name AS country FROM orders_coupons AS oc ".
            "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
            "INNER JOIN commissions AS c ON c.order_id=oc.order_id ".
            "INNER JOIN auth_user AS u ON u.id=oc.id_user ".
            "INNER JOIN country_t AS cou ON cou.country_id=u.id_country ".
            "WHERE u.gender IN (0, 1) AND oc.created>='{$begin}' AND oc.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.id_country ORDER BY cou.short_name ASC";

    $result  = $this->db->query($sql);
    $results = $result->result_array();

    $roi_stats = array();

    $total_sales = 0;
    $total_commission = 0;
    $roi_stats['commission_values'] = array();
    $roi_stats['sales_values'] = array();

    foreach ($results as $result) {
       $roi_stats['keys'][] = "'".$result['country']."'";
       $roi_stats['commission_values'][] = number_format($result['commission'], 2, '.', '');
       $roi_stats['sales_values'][] = number_format($result['sales'], 2, '.', '');
       $total_commission += number_format($result['commission'], 2, '.', '');
       $total_sales += number_format($result['sales'], 2, '.', '');
    }


    $roi_stats['commission_total'] = $total_commission;
    $roi_stats['sales_total'] = $total_sales;


    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $roi_stats['graph_title'] = "Values from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $roi_stats['y_axis_label'] = "Values ($)";

    return $roi_stats;
  }


  public function getROIStatsByCity($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);


    $sql =  "SELECT SUM(c.value) AS commission, SUM(o.total)*count(DISTINCT o.order_id)/count(*) AS sales, u.city FROM orders_coupons AS oc ".
            "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
            "INNER JOIN commissions AS c ON c.order_id=oc.order_id ".
            "INNER JOIN auth_user AS u ON u.id=oc.id_user ".
            "WHERE u.gender IN (0, 1) AND oc.created>='{$begin}' AND oc.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.city ORDER BY u.city ASC";

    $result  = $this->db->query($sql);
    $results = $result->result_array();

    $roi_stats = array();

    $total_sales = 0;
    $total_commission = 0;
    $roi_stats['commission_values'] = array();
    $roi_stats['sales_values'] = array();

    foreach ($results as $result) {
       $roi_stats['keys'][] = "'".$result['city']."'";
       $roi_stats['commission_values'][] = number_format($result['commission'], 2, '.', '');
       $roi_stats['sales_values'][] = number_format($result['sales'], 2, '.', '');
       $total_commission += number_format($result['commission'], 2, '.', '');
       $total_sales += number_format($result['sales'], 2, '.', '');
    }


    $roi_stats['commission_total'] = $total_commission;
    $roi_stats['sales_total'] = $total_sales;


    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $roi_stats['graph_title'] = "Values from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $roi_stats['y_axis_label'] = "Values ($)";

    return $roi_stats;
  }


  public function getROIStatsBySchool($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

      if (!$begin) {
         $begin = strtotime("-1 year", time());
      }

      if (!$end) {
         $end = time();
      }

      $begin = date('Y-m-d G:i:s', $begin);
      $end   = date('Y-m-d G:i:s', $end+86399);


      $sql =  "SELECT SUM(c.value) AS commission, SUM(o.total)*count(DISTINCT o.order_id)/count(*) AS sales, u.school FROM orders_coupons AS oc ".
              "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
              "INNER JOIN commissions AS c ON c.order_id=oc.order_id ".
              "INNER JOIN auth_user AS u ON u.id=oc.id_user ".
              "WHERE u.gender IN (0, 1) AND oc.created>='{$begin}' AND oc.created<='{$end}' ".
              "AND u.id_country IN ($ids_countries) ";

      if ($country) {
          $sql .= "AND u.id_country={$country} ";
      }

      if ($city) {
          $sql .= "AND u.city='{$city}' ";
      }

      $sql .= "GROUP BY u.school ORDER BY u.school ASC";

      $result  = $this->db->query($sql);
      $results = $result->result_array();

      $roi_stats = array();

      $total_sales = 0;
      $total_commission = 0;
      $roi_stats['commission_values'] = array();
      $roi_stats['sales_values'] = array();

      foreach ($results as $result) {
         $roi_stats['keys'][] = "'".$result['school']."'";
         $roi_stats['commission_values'][] = number_format($result['commission'], 2, '.', '');
         $roi_stats['sales_values'][] = number_format($result['sales'], 2, '.', '');
         $total_commission += number_format($result['commission'], 2, '.', '');
         $total_sales += number_format($result['sales'], 2, '.', '');
      }


      $roi_stats['commission_total'] = $total_commission;
      $roi_stats['sales_total'] = $total_sales;


      $begin = date('Y-m-d', strtotime($begin));
      $end   = date('Y-m-d', strtotime($end));

      $roi_stats['graph_title'] = "Values from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
      $roi_stats['y_axis_label'] = "Values ($)";

      return $roi_stats;
  }


  public function getSPCStats($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql = "SELECT u.first_name, u.last_name, u.id AS id_user, c.total_coupons AS total_coupons, COUNT(*) AS total_orders, (COUNT(*) / c.total_coupons) AS spc ".
            "FROM (SELECT COUNT(*) AS total_coupons, code, id_creator FROM coupons GROUP BY id_creator) AS c ".
            "INNER JOIN orders_coupons AS oc ON oc.id_user=c.id_creator ".
            "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
            "INNER JOIN auth_user AS u ON u.id=c.id_creator ".
            "WHERE o.created>='{$begin}' AND o.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY c.id_creator ORDER BY spc DESC LIMIT ".$this->max_results;

    $results = $this->db->query($sql)->result_array();


    $spc_stats = array();

    $spc_stats['keys'] = array();
    $spc_stats['values'] = array();

    foreach ($results as $result) {
       $spc_stats['keys'][]    = "'".$result['first_name'].' '.$result['last_name']."'";
       $spc_stats['values'][]  = number_format($result['spc'] * 100, 2, '.', '');
       $spc_stats['sales'][]   = $result['total_orders'];
       $spc_stats['coupons'][] = $result['total_coupons'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $spc_stats['graph_title'] = "SPC from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $spc_stats['y_axis_label'] = "SPC (%)";

    return $spc_stats;

  }


  public function getSPCStatsByTag($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql = "SELECT ut.tag, u.first_name, u.last_name, u.id AS id_user, c.total_coupons AS total_coupons, COUNT(*) AS total_orders, (COUNT(*) / c.total_coupons) AS spc ".
            "FROM (SELECT COUNT(*) AS total_coupons, code, id_creator FROM coupons GROUP BY id_creator) AS c ".
            "INNER JOIN orders_coupons AS oc ON oc.id_user=c.id_creator ".
            "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
            "INNER JOIN auth_user AS u ON u.id=c.id_creator ".
            "INNER JOIN user_tags AS ut ON ut.id_user=u.id ".
            "WHERE o.created>='{$begin}' AND o.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY ut.tag ORDER BY spc DESC LIMIT ".$this->max_results;

    $results = $this->db->query($sql)->result_array();


    $spc_stats = array();

    $spc_stats['keys'] = array();
    $spc_stats['values'] = array();

    foreach ($results as $result) {
       $spc_stats['keys'][]    = "'".$result['tag']."'";
       $spc_stats['values'][]  = number_format($result['spc'] * 100, 2, '.', '');
       $spc_stats['sales'][]   = $result['total_orders'];
       $spc_stats['coupons'][] = $result['total_coupons'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $spc_stats['graph_title'] = "SPC by Tag from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $spc_stats['y_axis_label'] = "SPC (%)";

    return $spc_stats;

  }


  public function getSPCStatsByGender($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql = "SELECT u.gender, u.first_name, u.last_name, u.id AS id_user, c.total_coupons AS total_coupons, COUNT(*) AS total_orders, (COUNT(*) / c.total_coupons) AS spc ".
            "FROM (SELECT COUNT(*) AS total_coupons, code, id_creator FROM coupons GROUP BY id_creator) AS c ".
            "INNER JOIN orders_coupons AS oc ON oc.id_user=c.id_creator ".
            "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
            "INNER JOIN auth_user AS u ON u.id=c.id_creator ".
            "WHERE o.created>='{$begin}' AND o.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY c.id_creator ORDER BY spc DESC";

    $users = $this->db->query($sql)->result_array();

    // krumo::dump($users); die();

    // build data by gender
    $results = array();

    foreach ($users as $u) {
        $found = 0;
        foreach ($results as $key => $r) {
            if ($r['gender'] == $u['gender']) {
                $results[$key]['sales'] += $u['total_orders'];
                $results[$key]['coupons'] += $u['total_coupons'];
                $found = 1;
            }
        }

        if (!$found) {
            $index = count($results);
            $results[$index]['gender'] = $u['gender'];
            $results[$index]['sales'] = $u['total_orders'];
            $results[$index]['coupons'] = $u['total_coupons'];
        }
    }

    $spc_stats = array();

    $spc_stats['keys'] = array();
    $spc_stats['values'] = array();

    $spc_stats['keys'][0] = "'Women'";
    $spc_stats['keys'][1] = "'Men'";

    // $spc_stats['values'][0] = "0";
    // $spc_stats['values'][1] = "0";
    // $spc_stats['sales'][0] = "0";
    // $spc_stats['sales'][1] = "0";
    // $spc_stats['coupons'][0] = "0";
    // $spc_stats['coupons'][1] = "0";

    foreach ($results as $result) {
        if (intval($result['gender']) == 0) {
            $spc_stats['values'][0]  = number_format(($result['sales'] / $result['coupons']) * 100, 2, '.', '');
            $spc_stats['sales'][0]   = $result['sales'];
            $spc_stats['coupons'][0] = $result['coupons'];
        } else {
            $spc_stats['values'][1]  = number_format(($result['sales'] / $result['coupons']) * 100, 2, '.', '');
            $spc_stats['sales'][1]   = $result['sales'];
            $spc_stats['coupons'][1] = $result['coupons'];
        }
    }

    ksort($spc_stats['values']);
    ksort($spc_stats['sales']);
    ksort($spc_stats['coupons']);

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $spc_stats['graph_title'] = "SPC from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $spc_stats['y_axis_label'] = "SPC (%)";

    return $spc_stats;

  }


  public function getSPCStatsByCountry($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql = "SELECT u.country, u.first_name, u.last_name, u.id AS id_user, c.total_coupons AS total_coupons, COUNT(*) AS total_orders, (COUNT(*) / c.total_coupons) AS spc ".
            "FROM (SELECT COUNT(*) AS total_coupons, code, id_creator FROM coupons GROUP BY id_creator) AS c ".
            "INNER JOIN orders_coupons AS oc ON oc.id_user=c.id_creator ".
            "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
            "INNER JOIN auth_user AS u ON u.id=c.id_creator ".
            "WHERE o.created>='{$begin}' AND o.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY c.id_creator ORDER BY spc DESC";

    $users = $this->db->query($sql)->result_array();

    // build data by gender
    $results = array();

    foreach ($users as $u) {
        $found = 0;
        foreach ($results as $key => $r) {
            if ($r['country'] == $u['country']) {
                $results[$key]['sales'] += $u['total_orders'];
                $results[$key]['coupons'] += $u['total_coupons'];
                $found = 1;
            }
        }

        if (!$found) {
            $index = count($results);
            $results[$index]['country'] = $u['country'];
            $results[$index]['sales'] = $u['total_orders'];
            $results[$index]['coupons'] = $u['total_coupons'];
        }
    }

    $spc_stats = array();

    $spc_stats['keys'] = array();
    $spc_stats['values'] = array();

    foreach ($results as $result) {
       $spc_stats['keys'][]    = "'".$result['country']."'";
       $spc_stats['values'][]  = number_format(($result['sales'] / $result['coupons']) * 100, 2, '.', '');
       $spc_stats['sales'][]   = $result['sales'];
       $spc_stats['coupons'][] = $result['coupons'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $spc_stats['graph_title'] = "SPC from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $spc_stats['y_axis_label'] = "SPC (%)";

    return $spc_stats;

  }


  public function getSPCStatsByCity($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql = "SELECT u.city, u.first_name, u.last_name, u.id AS id_user, c.total_coupons AS total_coupons, COUNT(*) AS total_orders, (COUNT(*) / c.total_coupons) AS spc ".
            "FROM (SELECT COUNT(*) AS total_coupons, code, id_creator FROM coupons GROUP BY id_creator) AS c ".
            "INNER JOIN orders_coupons AS oc ON oc.id_user=c.id_creator ".
            "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
            "INNER JOIN auth_user AS u ON u.id=c.id_creator ".
            "WHERE o.created>='{$begin}' AND o.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY c.id_creator ORDER BY spc DESC";

    $users = $this->db->query($sql)->result_array();

    // build data by gender
    $results = array();

    foreach ($users as $u) {
        $found = 0;
        foreach ($results as $key => $r) {
            if ($r['city'] == $u['city']) {
                $results[$key]['sales'] += $u['total_orders'];
                $results[$key]['coupons'] += $u['total_coupons'];
                $found = 1;
            }
        }

        if (!$found) {
            $index = count($results);
            $results[$index]['city'] = $u['city'];
            $results[$index]['sales'] = $u['total_orders'];
            $results[$index]['coupons'] = $u['total_coupons'];
        }
    }

    $spc_stats = array();

    $spc_stats['keys'] = array();
    $spc_stats['values'] = array();

    foreach ($results as $result) {
       $spc_stats['keys'][]    = "'".$result['city']."'";
       $spc_stats['values'][]  = number_format(($result['sales'] / $result['coupons']) * 100, 2, '.', '');
       $spc_stats['sales'][]   = $result['sales'];
       $spc_stats['coupons'][] = $result['coupons'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $spc_stats['graph_title'] = "SPC from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $spc_stats['y_axis_label'] = "SPC (%)";

    return $spc_stats;

  }


  public function getSPCStatsBySchool($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql = "SELECT u.school, u.first_name, u.last_name, u.id AS id_user, c.total_coupons AS total_coupons, COUNT(*) AS total_orders, (COUNT(*) / c.total_coupons) AS spc ".
            "FROM (SELECT COUNT(*) AS total_coupons, code, id_creator FROM coupons GROUP BY id_creator) AS c ".
            "INNER JOIN orders_coupons AS oc ON oc.id_user=c.id_creator ".
            "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
            "INNER JOIN auth_user AS u ON u.id=c.id_creator ".
            "WHERE o.created>='{$begin}' AND o.created<='{$end}' ".
            "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY c.id_creator ORDER BY spc DESC";

    $users = $this->db->query($sql)->result_array();

    // build data by gender
    $results = array();

    foreach ($users as $u) {
        $found = 0;
        foreach ($results as $key => $r) {
            if ($r['school'] == $u['school']) {
                $results[$key]['sales'] += $u['total_orders'];
                $results[$key]['coupons'] += $u['total_coupons'];
                $found = 1;
            }
        }

        if (!$found) {
            $index = count($results);
            $results[$index]['school'] = $u['school'];
            $results[$index]['sales'] = $u['total_orders'];
            $results[$index]['coupons'] = $u['total_coupons'];
        }
    }

    $spc_stats = array();

    $spc_stats['keys'] = array();
    $spc_stats['values'] = array();

    foreach ($results as $result) {
       $spc_stats['keys'][]    = "'".$result['school']."'";
       $spc_stats['values'][]  = number_format(($result['sales'] / $result['coupons']) * 100, 2, '.', '');
       $spc_stats['sales'][]   = $result['sales'];
       $spc_stats['coupons'][] = $result['coupons'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $spc_stats['graph_title'] = "SPC from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $spc_stats['y_axis_label'] = "SPC (%)";

    return $spc_stats;

  }


  public function getSalesStats($country, $city, $begin, $end, $ids_countries, $frequency = false){
    $id_user = $this->session->userdata('id');
    $months_days = array("Jan" => 31, "Feb" => 28, "Mar" => 31, "Apr" => 30, "May" => 31, "Jun" => 30, "Jul" => 31, "Aug" => 31, "Sep" => 30, "Oct" => 31, "Nov" => 30, "Dec" => 31);

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin_timestamp = $begin;
    $end_timestamp   = $end;

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $interval = array();
    $interval['start_month'] = date('M', strtotime($begin));
    $interval['start_day']   = date('d', strtotime($begin));
    $interval['start_year']  = date('Y', strtotime($begin));
    $interval['end_month']   = date('M', strtotime($end));
    $interval['end_day']     = date('d', strtotime($end));
    $interval['end_year']    = date('Y', strtotime($end));

    if($frequency == "day"){
        $frequency_sql = "CONCAT(DAY(o.created), ' ', MONTHNAME(o.created), ' ', YEAR(o.created))";
    }
    else if($frequency == "week"){
        $frequency_sql = "CONCAT(WEEK(o.created), ' ', YEAR(o.created))";
    } else {
        $frequency_sql = "MONTH(o.created)";
    }

    $sql = "SELECT SUM(o.total) as total_sum, ".$frequency_sql." as frequency, o.created ".
      "FROM orders_coupons AS oc ".
      "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
      "INNER JOIN coupons AS c ON c.code=oc.coupon ".
      "INNER JOIN auth_user AS u ON u.id=c.id_creator ".
      "WHERE o.created>='{$begin}' AND o.created<='{$end}'".
      "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY frequency ORDER BY o.created ASC LIMIT ".$this->max_results;

    $query   = $this->db->query($sql);
    $results = $query->result_array();

    $sales_stats = array();

    $total = 0;
    $sales_stats['keys'] = array();
    $sales_stats['values'] = array();

    foreach ($results as $result) {
        $sales_stats['values'][] = $result['total_sum'];

        if($frequency == "day"){
            $sales_stats['keys'][] = "'".date('d M Y', strtotime($result['created']))."'";
            $sales_stats['table_values'][] = "'".date('d M Y', strtotime($result['created']))." - ".date('d M Y', strtotime($result['created']))."'";
        }
        else if($frequency == "week"){
            $data = explode(' ', $result['frequency']);
            $week = $data[0];
            $year = $data[1];

            $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
            $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
            $timestamp_for_sunday = $timestamp_for_monday + 3600 * 24 * 6;

            if($begin_timestamp >= $timestamp_for_monday && $begin_timestamp <= $timestamp_for_sunday){
                $timestamp_for_monday = $begin_timestamp;
            }
            if($end_timestamp <= $timestamp_for_sunday && $end_timestamp >= $timestamp_for_monday){
                $timestamp_for_sunday = $end_timestamp;
            }
            if(time() >= $timestamp_for_monday && time() <= $timestamp_for_sunday){
                $timestamp_for_sunday = time();
            }

            $date_for_monday = date( 'd M Y', $timestamp_for_monday );
            $label_for_monday = date( 'd M', $timestamp_for_monday );
            $date_for_sunday = date( 'd M Y', $timestamp_for_sunday );
            $label_for_sunday = date( 'd M', $timestamp_for_sunday );

            $sales_stats['keys'][] = "'".$label_for_monday."-".$label_for_sunday."'";
            $sales_stats['table_values'][] = "'".$date_for_monday." - ".$date_for_sunday."'";
        } else {
            $sales_stats['keys'][] = "'".date('M Y', strtotime($result['created']))."'";
            if (date('M', strtotime($result['created'])) == $interval['start_month'] || date('M', strtotime($result['created'])) == $interval['end_month']) {
                if ($interval['start_month'] == $interval['end_month'] && $interval['start_year'] == $interval['end_year']) {
                    $sales_stats['table_values'][] = "'".$interval['start_day']." ".$interval['start_month']." ".date('Y', strtotime($result['created'])).' - '.$interval['end_day']." ".$interval['end_month']." ".date('Y', strtotime($result['created']))."'";
                } else {
                    if (date('M', strtotime($result['created'])) == $interval['start_month'] && date('Y', strtotime($result['created'])) == $interval['start_year']) {
                        $sales_stats['table_values'][] = "'".$interval['start_day']." ".$interval['start_month']." ".date('Y', strtotime($result['created'])).' - '.$months_days[$interval['start_month']]." ".$interval['start_month']." ".date('Y', strtotime($result['created']))."'";
                    }
                    else if (date('M', strtotime($result['created'])) == $interval['end_month'] && date('Y', strtotime($result['created'])) == $interval['end_year']) {
                        $sales_stats['table_values'][] = "'1 ".$interval['end_month']." ".date('Y', strtotime($result['created'])).' - '.$interval['end_day']." ".$interval['end_month']." ".date('Y', strtotime($result['created']))."'";
                    }
                    else {
                         $sales_stats['table_values'][] = "'".date('M Y', strtotime($result['created']))."'";
                    }
                }

            } else {
                $sales_stats['table_values'][] = "'".date('M Y', strtotime($result['created']))."'";
            }
        }

        $total += $result['total_sum'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_stats['total'] = $total;
    $sales_stats['graph_title'] = "Total Sales from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_stats['y_axis_label'] = "Sales ($)";

    return $sales_stats;
  }


  public function getSalesStatsByGender($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);


    $sql = "SELECT u.gender, SUM(o.total) as total ".
      "FROM orders_coupons AS oc ".
      "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
      "INNER JOIN coupons AS c ON c.code=oc.coupon " .
      "INNER JOIN auth_user as u ON c.id_creator=u.id " .
      "WHERE u.gender IN (0, 1) AND o.created>='{$begin}' AND o.created<='{$end}'".
      "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.gender ORDER BY u.gender";

    $query = $this->db->query($sql);

    $results = $query->result_array();

    $stats_no = count($results);
    $sales_stats = array();
    $total = 0;

    $sales_stats['keys'][0] = "'Women'";
    $sales_stats['keys'][1] = "'Men'";

    $sales_stats['values'][0] = "0";
    $sales_stats['values'][1] = "0";

    foreach ($results as $result) {
      if ($result['gender'] == 0) {
        $sales_stats['values'][0] = $result['total'];
      } else {
        $sales_stats['values'][1] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_stats['total'] = $total;
    $sales_stats['graph_title'] = "Sales by Gender from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_stats['y_axis_label'] = "Sales ($)";

    return $sales_stats;
  }


  public function getSalesStatsByCountry($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql = "SELECT u.country, SUM(o.total) as total ".
      "FROM orders_coupons AS oc ".
      "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
      "INNER JOIN coupons AS c ON c.code=oc.coupon " .
      "INNER JOIN auth_user as u ON c.id_creator=u.id " .
      "WHERE o.created>='{$begin}' AND o.created<='{$end}'".
      "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.country ORDER BY total DESC";

    $query = $this->db->query($sql);

    // $query = $this->db->query("SELECT u.country, SUM(sales.total) as total FROM sales LEFT JOIN auth_user AS u ON sales.id_user = u.id GROUP BY u.country ORDER BY u.country");

    $results = $query->result_array();

    $stats_no = count($results);
    $sales_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
      if (strlen($result['country']) == 0) {
        $sales_stats['keys'][] = "'Others'";
        $sales_stats['values'][] = $result['total'];
      } else {
        $sales_stats['keys'][] = "'".$result['country']."'";
        $sales_stats['values'][] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_stats['total'] = $total;
    $sales_stats['graph_title'] = "Sales by Country from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_stats['y_axis_label'] = "Sales ($)";

    return $sales_stats;
  }


  public function getSalesStatsByCity($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql = "SELECT u.city, SUM(o.total) as total ".
      "FROM orders_coupons AS oc ".
      "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
      "INNER JOIN coupons AS c ON c.code=oc.coupon " .
      "INNER JOIN auth_user as u ON c.id_creator=u.id " .
      "WHERE o.created>='{$begin}' AND o.created<='{$end}'".
      "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.city ORDER BY total DESC";

    $query = $this->db->query($sql);

    // $query = $this->db->query("SELECT u.city, SUM(sales.total) as total FROM sales LEFT JOIN auth_user AS u ON sales.id_user = u.id GROUP BY u.city ORDER BY u.city");

    $results = $query->result_array();

    $stats_no = count($results);
    $sales_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
      if (strlen($result['city']) == 0) {
        $sales_stats['keys'][] = "'Others'";
        $sales_stats['values'][] = $result['total'];
      } else {
        $sales_stats['keys'][] = "'".$result['city']."'";
        $sales_stats['values'][] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_stats['total'] = $total;
    $sales_stats['graph_title'] = "Sales by City from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_stats['y_axis_label'] = "Sales ($)";

    return $sales_stats;
  }


  public function getSalesStatsBySchool($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql = "SELECT u.school, SUM(o.total) as total ".
      "FROM orders_coupons AS oc ".
      "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
      "INNER JOIN coupons AS c ON c.code=oc.coupon " .
      "INNER JOIN auth_user as u ON c.id_creator=u.id " .
      "WHERE o.created>='{$begin}' AND o.created<='{$end}'".
      "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY u.school ORDER BY total DESC";

    $query = $this->db->query($sql);

    // $query = $this->db->query("SELECT u.school, SUM(sales.total) as total FROM sales LEFT JOIN auth_user AS u ON sales.id_user = u.id GROUP BY u.school ORDER BY u.school");

    $results = $query->result_array();

    $stats_no = count($results);
    $sales_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
      if (strlen($result['school']) == 0) {
        $sales_stats['keys'][] = "'Others'";
        $sales_stats['values'][] = $result['total'];
      } else {
        $sales_stats['keys'][] = "'".$result['school']."'";
        $sales_stats['values'][] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_stats['total'] = $total;
    $sales_stats['graph_title'] = "Sales by School from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_stats['y_axis_label'] = "Sales ($)";

    return $sales_stats;
  }

  public function getSalesStatsByTag($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql = "SELECT ut.tag, SUM(o.total) as total ".
      "FROM orders_coupons AS oc ".
      "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
      "INNER JOIN user_tags AS ut ON ut.id_user=oc.id_user " .
      "INNER JOIN auth_user as u ON ut.id_user=u.id ".
      "WHERE o.created>='{$begin}' AND o.created<='{$end}'".
      "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY ut.tag ORDER BY total DESC";

    $query = $this->db->query($sql);
    $results = $query->result_array();

    $stats_no = count($results);
    $sales_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
        $sales_stats['keys'][] = "'".$result['tag']."'";
        $sales_stats['values'][] = $result['total'];

        $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_stats['total'] = $total;
    $sales_stats['graph_title'] = "Sales by Tags from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_stats['y_axis_label'] = "Sales ($)";

    return $sales_stats;
  }

  public function getSalesStatsByCoupon($country, $city, $begin, $end, $ids_countries){

    $id_user = $this->session->userdata('id');

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql = "SELECT oc.coupon, SUM(o.total) as total ".
      "FROM orders_coupons AS oc ".
      "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
      "INNER JOIN coupons AS c ON c.code=oc.coupon " .
      "INNER JOIN auth_user as u ON c.id_creator=u.id ".
      "WHERE o.created>='{$begin}' AND o.created<='{$end}'".
      "AND u.id_country IN ($ids_countries) ";

    if ($country) {
        $sql .= "AND u.id_country={$country} ";
    }

    if ($city) {
        $sql .= "AND u.city='{$city}' ";
    }

    $sql .= "GROUP BY oc.coupon ORDER BY total DESC";

    $query = $this->db->query($sql);
    $results = $query->result_array();

    $stats_no = count($results);
    $sales_stats = array();
    $total = 0;

    foreach ($results as $key => $result) {
      if (strlen($result['coupon']) == 0) {
        $sales_stats['keys'][] = "'Others'";
        $sales_stats['values'][] = $result['total'];
      } else {
        $sales_stats['keys'][] = "'".$result['coupon']."'";
        $sales_stats['values'][] = $result['total'];
      }

      $total += $result['total'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_stats['total'] = $total;
    $sales_stats['graph_title'] = "Sales by Coupon from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_stats['y_axis_label'] = "Sales ($)";

    return $sales_stats;
  }


  public function getSalesStatsByUser($id_user, $begin, $end){

    if (!$begin) {
       $begin = strtotime("-1 year", time());
    }

    if (!$end) {
       $end = time();
    }

    $begin = date('Y-m-d G:i:s', $begin);
    $end   = date('Y-m-d G:i:s', $end+86399);

    $sql = "SELECT SUM(o.total) as total_sum, MONTH(o.created) as month, o.created ".
      "FROM orders_coupons AS oc ".
      "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
      "INNER JOIN coupons AS c ON c.code=oc.coupon ".
      "INNER JOIN auth_user AS u ON u.id=c.id_creator ".
      "WHERE o.created>='{$begin}' AND o.created<='{$end}'".
      "AND u.id={$id_user} ".
      "GROUP BY month ORDER BY o.created ASC";

    $query   = $this->db->query($sql);
    $results = $query->result_array();

    $sales_stats = array();

    $total = 0;
    $sales_stats['keys'] = array();
    $sales_stats['values'] = array();

    foreach ($results as $result) {
       $sales_stats['keys'][] = "'".date('M Y', strtotime($result['created']))."'";
       $sales_stats['values'][] = $result['total_sum'];
       $total += $result['total_sum'];
    }

    $begin = date('Y-m-d', strtotime($begin));
    $end   = date('Y-m-d', strtotime($end));

    $sales_stats['total'] = $total;
    $sales_stats['graph_title'] = "Total Sales from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
    $sales_stats['y_axis_label'] = "Sales ($)";

    return $sales_stats;
  }


  public function getUnderkeepersSalesStats($id_user, $begin, $end){
        if (!$begin) {
           $begin = strtotime("-1 year", time());
        }

        if (!$end) {
           $end = time();
        }

        $begin = date('Y-m-d G:i:s', $begin);
        $end   = date('Y-m-d G:i:s', $end+86399);

        $sql = "SELECT SUM(o.total) as total_sum, u.id, u.first_name, u.last_name, o.created ".
          "FROM orders_coupons AS oc ".
          "INNER JOIN orders AS o ON o.order_id=oc.order_id ".
          "INNER JOIN coupons AS c ON c.code=oc.coupon ".
          "INNER JOIN auth_user AS u ON u.id=c.id_creator ".
          "WHERE o.created>='{$begin}' AND o.created<='{$end}'".
          "AND u.id_master={$id_user} ".
          "GROUP BY u.id ORDER BY total_sum DESC LIMIT ".$this->max_results;

        $query   = $this->db->query($sql);
        $results = $query->result_array();

        $sales_stats = array();

        $total = 0;
        $sales_stats['keys'] = array();
        $sales_stats['values'] = array();

        foreach ($results as $result) {
           $sales_stats['keys'][] = "'".$result['first_name'].' '.$result['last_name']."'";
           $sales_stats['values'][] = $result['total_sum'];
           $total += $result['total_sum'];
        }

        $begin = date('Y-m-d', strtotime($begin));
        $end   = date('Y-m-d', strtotime($end));

        $sales_stats['total'] = $total;
        $sales_stats['graph_title'] = "Total Sales from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
        $sales_stats['y_axis_label'] = "Sales ($)";

        return $sales_stats;
  }


  public function getUnderkeepersCommissionStats($id_user, $begin, $end){
        if (!$begin) {
           $begin = strtotime("-1 year", time());
        }

        if (!$end) {
           $end = time();
        }

        $begin = date('Y-m-d G:i:s', $begin);
        $end   = date('Y-m-d G:i:s', $end+86399);

        $sql = "SELECT SUM(c.value) as total_sum, u.id, u.first_name, u.last_name, c.created ".
          "FROM commissions AS c ".
          "INNER JOIN orders_coupons AS oc ON oc.order_id=c.order_id ".
          "INNER JOIN auth_user AS u ON u.id=oc.id_user ".
          "WHERE c.created>='{$begin}' AND c.created<='{$end}' AND c.type=2 AND c.id_user={$id_user} ".
          "GROUP BY u.id ORDER BY total_sum DESC";

        $query   = $this->db->query($sql);
        $results = $query->result_array();

        $sales_stats = array();

        $total = 0;
        $sales_stats['keys'] = array();
        $sales_stats['values'] = array();

        foreach ($results as $result) {
           $sales_stats['keys'][] = "'".$result['first_name'].' '.$result['last_name']."'";
           $sales_stats['values'][] = $result['total_sum'];
           $total += $result['total_sum'];
        }

        $begin = date('Y-m-d', strtotime($begin));
        $end   = date('Y-m-d', strtotime($end));

        $sales_stats['total'] = $total;
        $sales_stats['graph_title'] = "Total Comission from ".$this->users->formatDate($id_user, strtotime($begin))." to ".$this->users->formatDate($id_user, strtotime($end));
        $sales_stats['y_axis_label'] = "Comission ($)";

        return $sales_stats;
  }
  public function getScratchStats($country, $city, $begin, $end, $ids_countries, $action, $total_sales = 0)
  {
      $total = 0;
      if (!$begin) {
        $begin = strtotime("-1 year", time());
      }

      if (!$end) {
        $end = time();
      }

      $begin = date('Y-m-d G:i:s', $begin);
      $end   = date('Y-m-d G:i:s', $end+86399);

      switch($action)
      {

          case 'scratch_sales_percentage':
            $this->db
                   ->select("sb.batch")
                   ->select_sum("o.total", "total")
                   ->from("scratch_batches as sb")
                   ->join("scratch_coupons AS sc", "sb.batch = sc.batch")
                   ->join("orders_coupons AS oc", "sc.coupon = oc.coupon")
                   ->join("orders AS o", "oc.order_id = o.order_id")
                   ->where(array(
                      "sb.active" => 1,
                      "sb.created" => 1,
                      "sb.activated >=" => $begin,
                      "sb.activated <=" => $end))
                   ->group_by("sb.batch");
          if(!empty($country))
          {
            $this->db
              ->join("auth_user AS u", "u.id = oc.id_user")
              ->where("u.id_country", $country);
          }
          if(!empty($city))
          {
            $this->db
              ->join("auth_user AS u", "u.id = oc.id_user")
              ->where("u.city", $city);
          }
          $query = $this->db->get();

          $results = $query->result_array();
          $ret = array();
          $total = 0;
          for($i=0; $i<count($results); $i++)
          {
            // FORMULA: 100 * current_batch_value / total_sales
            $ret[$results[$i]['batch']] = 100 * $results[$i]['total'] / $total_sales;
            $total += $ret[$results[$i]['batch']];
          }

          return array($ret, $total);

          break;
          case "scratch_revenue":
          $this->db
                 ->select("sb.batch")
                 ->select_sum("o.total", "total")
                 ->from("scratch_batches as sb")
                 ->join("scratch_coupons AS sc", "sb.batch = sc.batch")
                 ->join("orders_coupons AS oc", "sc.coupon = oc.coupon")
                 ->join("orders AS o", "oc.order_id = o.order_id")
                 ->where(array(
                    "sb.active" => 1,
                    "sb.created" => 1,
                    "sb.activated >=" => $begin,
                    "sb.activated <=" => $end))
                 ->group_by("sb.batch");
          if(!empty($country))
          {
            $this->db
            ->join("auth_user AS u", "u.id = oc.id_user")
            ->where("u.id_country", $country);
          }
          if(!empty($city))
          {
            $this->db
            ->join("auth_user AS u", "u.id = oc.id_user")
            ->where("u.city", $city);
          }
          $query = $this->db->get();

          $results = $query->result_array();
          $ret = array();
          $total = 0;
          for($i=0; $i<count($results); $i++)
          {
            $ret[$results[$i]['batch']] = $results[$i]['total'];
            $total += $results[$i]['total'];
          }
          return array($ret, $total);
          break;

          case "scratch_piecekeepers":

          $this->db
                 ->select(array("u.first_name", "u.last_name"))
                 ->select_sum("o.total", "total")
                 ->from("scratch_batches as sb")
                 ->join("scratch_coupons AS sc", "sb.batch = sc.batch")
                 ->join("orders_coupons AS oc", "sc.coupon = oc.coupon")
                 ->join("orders AS o", "oc.order_id = o.order_id")
                 ->join("auth_user AS u", "u.id = oc.id_user")
                 ->where(array(
                    "sb.active" => 1,
                    "sb.created" => 1,
                    "sb.activated >=" => $begin,
                    "sb.activated <=" => $end))
                 ->group_by("u.id");
          if(!empty($country))
          {
            $this->db->where("u.id_country", $country);
          }
          if(!empty($city))
          {
            $this->db->where("u.city", $city);
          }
          $query = $this->db->get();
          $results = $query->result_array();
          $ret = array();
          $total = 0;
          for($i=0; $i<count($results); $i++)
          {
              $ret[$results[$i]['first_name']." ".$results[$i]['last_name']] = $results[$i]['total'];
              $total += $results[$i]['total'];
          }

          return array($ret, $total);
          break;
      }


      return "";




  }

}
