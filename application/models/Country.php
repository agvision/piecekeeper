<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Country extends MY_Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function getAllCountries()
	{
		$query = $this->db->query("SELECT * FROM country_t");

		return $query->result();
	}


	/**
	 * Get information abaout a country.
	 *
	 * @param  [int] $id [country id]
	 * @return [row]     [country]
	 */
	public function getCountry($id)
	{
		$this->db->select('*');
		$this->db->from('country_t');
		$this->db->where('country_id', $id);

		return $this->db->get()->row();
	}
	public function getById($id)
	{
		$country_information = $this->db->select("*")
										->from("country_t")
										->where("country_id", $id)
										->get()
										->row();
		return $country_information;
	}
	public function getByShortName($short_name)
	{
		$country_information = $this->db->select("*")
										->from("country_t")
										->where("short_name", $short_name)
										->get()
										->row();
		return $country_information;
	}

}
