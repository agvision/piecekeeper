<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Twilio_sms_model extends MY_Model 
{

  private $from    = null;
  private $to      = null;
  private $message = null;

  function __construct()
  {
    parent::__construct();

    define('SID', 'AC704b71cb98cad832cb1acbf476c4e98a');
    define('TOKEN', '2c1576fab880273157e1f23cd31fefef');

    include_once(APPPATH."libraries/Twilio/Twilio.php");
  }
  
  public function from($value) {
    $this->from = $value;
  }

  public function to($value) {
    $this->to = $value;
  }

  public function message($value) {
    $this->message = $value;
  }

  public function send() {
    $sid = SID; 
    $token = TOKEN; 
    $client = new Services_Twilio($sid, $token);
     
    $message = $client->account->sms_messages->create($this->from, $this->to, $this->message, array());
  }
  
}

