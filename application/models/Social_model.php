<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Social_model extends MY_Model
{

    public function __construct() {
        parent::__construct();
        $CI = & get_instance();

        /* Twitter Stuff */
        $CI->config->load("twitter", TRUE);
        $config = $CI->config->item('twitter');
        $this->load->library('twconnect', $config);

        /* Tumblr Stuff */
        $this->load->library('Tumblr');

        /* YouTube Stuff */
        $this->load->library('youtube_lib');

        /* Vine Stuff */
        $this->load->library('vine_api');

        $this->load->model('Email_model','emailModel');

    }

    public function deleteUnexistingAccounts(){
        $data = $this->db->query("SELECT * FROM (SELECT sa.id_user, u.id FROM (`social_accounts` AS sa) LEFT JOIN `auth_user` AS u ON `u`.`id`=`sa`.`id_user` ORDER BY `sa`.`id_user`) AS T1 WHERE T1.id IS NULL")->result();
        foreach ($data as $d) {
            $this->db->delete('social_accounts', array('id_user' => $d->id_user));
            $this->db->delete('social_followers', array('id_user' => $d->id_user));
        }
    }

    public function deleteAccount($id_user, $name) {
        $network = $this->getNetworkID($name);

        $this->db->delete('social_accounts', array(
            'id_user' => $id_user,
            'network' => $network
        ));
        $this->db->delete('social_followers', array(
            'id_user' => $id_user,
            'network' => $network
        ));
    }

    public function check40X($url, $options_par = true){

        $options = array(
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => false
        );

        /* Get the HTML or whatever is linked in $url. */
        if($options_par)
        {
            $handle = $this->curl->simple_get($url, $options);
        }
        else
        {
             $handle = $this->curl->simple_get($url);
        }

        $data = htmlspecialchars($handle);

        /* Verify if there is any response. */
        if(empty($data))
        {
            return true;
        }

        return false;
    }

    /**
     * It will return the number of likes for facebook pages.
     * For a personal account it will return 0.
     *
     * @param  [int] $id_account [account id]
     * @return [int]             [number of likes]
     */
    public function getAPIFacebookFollowers($id_account)
    {
        $this->db->where('id', $id_account);
        $account  = $this->db->get('social_accounts')->row();

        if($account->network == '1')
        {
            $profile  = $account->profile;
            $username = str_replace("https://www.facebook.com/", "", $profile);
            $id_user  = $account->id_user;

            try
            {
                $fb_page    = $this->facebook->api('/'.$username);
                $fb_profile = $fb_page['link'];
                $fb_likes   = isset($fb_page['likes']) ? $fb_page['likes'] : 0;
            }
            catch (Exception $e)
            {
                $fb_likes = 0;
            }

            return $fb_likes;
        }
    }

    public function getAPITwitterFollowers($id_account){
        $this->db->where('id', $id_account);
        $account  = $this->db->get('social_accounts')->row();
        if($account->network == '2'){
            $profile = $account->profile;
            $username = str_replace("https://twitter.com/", "", $profile);

            $data = $this->twconnect->app_tw_get("users/show", array("screen_name" => $username));

            if(isset($data->followers_count))
            {
                return $data->followers_count;
            }
        }
        return false;
    }

    /**
    * Verify if social account is valid
    *
    * @param  [int] 	$id_user [id of user]
    * @param  [string] 	$profile [social account]
    * @param  [int] 	$network_id [Netword ID]
    * @return [bool]  	Checks if the provided social account
	* 				  	is either valid or not.
    */
    public function verifyTwitterAccount($profile, $network_id)
    {
        //check if profile is empty
        if(empty($profile))
        {
            return FALSE;
        }
        else
        {
            if(!($this->check40X($profile)))
            {
				// This is a valid account.
                return TRUE;
            }
            else
            {
				// This account is not valid.
				//$this->archiveSocialAccount($id_user, $profile, $network_id);
                return FALSE;
            }
        }
    }

    public function linkedFacebook()
    {
      
    }

    public function getAPIPinterestFollowers($id_account){
        $this->db->where('id', $id_account);
        $account  = $this->db->get('social_accounts')->row();
        if($account->network == '5'){
            $profile = $account->profile;
            if(!($this->check40X($profile))) {
              $metas = @get_meta_tags($profile);
            }

            if($metas && isset($metas['pinterestapp:followers'])){
                return $metas['pinterestapp:followers'];
            }
        }
        return false;
    }

    public function getAPIYoutubeFollowers($id_account){
        $this->db->where('id', $id_account);
        $account  = $this->db->get('social_accounts')->row();
        if($account->network == '7'){
            $profile = $account->profile;
            $username = str_replace("https://www.youtube.com/user/", "", $profile);

            return $this->youtube_lib->get_subs($username);
        }
        return false;
    }

    public function getAPIVineFollowers($id_account){
        $this->db->where('id', $id_account);
        $account  = $this->db->get('social_accounts')->row();
        if($account->network == '8'){
            $profile = $account->profile;
            $username = str_replace("https://vine.co/u/", "", $profile);
            $vine_user = $this->vine_api->getUser($username);

            if($vine_user && isset($vine_user->followerCount)){
                return $vine_user->followerCount;
            }

        }
        return false;
    }
    /*
     * Function that get the instagram followers for every account
    */
    public function getInstagramFollowers($id_account) {
        $this->db->where('id', $id_account);
        $account  = $this->db->get('social_accounts')->row();
        if($account->network == '3'){

            // get the instagram profile
            $profile = $account->profile;

            if(!$this->Social->check40X($profile))
            {
                // get the instagram profile content
                $handle = $this->curl->simple_get($profile);

                // get the html special chars
                $data = htmlspecialchars($handle);

                if(!empty($data))
                {
                    // get the followed_by position in the entire file source
                    $followedByWord= 'followed_by';
                    $positionOfFollowedByWord = strpos($data, $followedByWord);

                    // get the position of country_block word in the entire file source
                    $countryBlockWord = 'country_block';
                    $positionOfCountryBlockWord = strpos($data, $countryBlockWord );

                    // get the length of text that I want to search in
                    $length =   $positionOfCountryBlockWord - $positionOfFollowedByWord;

                    // the text that contain the number of followers
                    $auxiliaryText = substr($data, $positionOfFollowedByWord-4 , $length+4);

                    // first component of auxiliary text
                    $firstComponent = explode(':',$auxiliaryText);

                    if(!empty($firstComponent))
                    {

                      // third component of auxiliar text
                      $secondComponent = explode ('}',$firstComponent[2]);

                      $numberOfFollowers = $secondComponent[0];

                      return $numberOfFollowers;
                    }
                    else
                    {
                        $this->emailModel->instagramFollowersInfo($id_account,$profile);

                        return false;
                    }

                }
            }

            return false;


        }
        return false;
    }
    /**
     * Social_model::archiveSocialAccount()
     *
     * @param int 		$id_user
     * @param string 	$profile
     * @param int 		$network_id
     * @return void; its deleting (and archiving in a separate table) a
     * giving social account and it's number of social followers.
     */
    private function archiveSocialAccount($id_user, $profile, $network_id)
    {
		// Will not affect "0'ed" profiles. (do it manually in this case).
        if(empty($profile))
			return;
		// Get network name(as 'Facebook' etc.) and number of followers
		$network_name = $this->getNetworkName($network_id);
        $followers = $this->countFollowers($id_user, $network_name);

        // If social account doesn't exist, archive it and delete it's social
		// followers.
		  $this->db->delete("social_accounts", array(
							'id_user' => $id_user,
							'network' => $network_id
							));
		  $this->deleteNetworkFollowers($network_name, $id_user);

		  // Keep log of the archived action.
		  $data = array(
			'id_user' => $id_user,
			'profile' => $profile,
			'network' => $network_id,
			'followers' => $followers
		  );
		  $this->db->set('created', 'NOW()', FALSE);
		  $this->db->insert("social_accounts_archive", $data);
    }

    /**
     * @param $network
     * @param $id_user
     * @param $followers
     * @return void
     */
    public function createFollowersDecreaseNotification($network, $id_user, $followers, $followersDecreasePercentage)
    {

         $storedFollowers = $this->countFollowers($id_user, $this->getNetworkName($network));

        if(($storedFollowers > $followers) && $followers !== false && !empty($followers))
        {
            $percentage = (  $storedFollowers - $followers )  / $storedFollowers * 100;
            if($percentage >= $followersDecreasePercentage && $storedFollowers >= 2000)
            {
                $paramValue = array(
                    'network' => $network,
                    'storedFollowers' => $storedFollowers,
                    'followers' => $followers
                );
                $data = [];
                $data['id_user'] = $id_user;
                $data['param'] =  serialize($paramValue);
                $data['id_admin'] = 1061;
                $data['type'] = 19;
                $data['active'] = 1;
                $this->db->set("created", "NOW()", FALSE);
                $this->db->insert("notifications", $data);
            }
        }
    }
    //
    // /**
    //  * Test if instagram account exist if the account dosen't exist move it to social_accounts_archive table
    //  *
    //  * @param int 		$user_id        id of user
    //  * @param string 	$profile        social profile
    //  * @param int 		$network_id     netwrk id (like 1 for facebook 2 for twitter ..)
    //  *
    //  */
    // public function testInstagramAccount($user_id, $network, $profile)
    // {
    //     if($this->Social->check40X($profile))
    //     {
    //         $this->archiveSocialAccount($user_id, $profile, $network);
    //     }
    //     else
    //     {
    //         return;
    //     }
    // }
    //
    // /**
    //  * Test if Pinterest account exist if the account dosen't exist move it to social_accounts_archive table
    //  *
    //  * @param int 		$user_id        id of user
    //  * @param string 	$profile        social profile
    //  * @param int 		$network_id     netwrk id (like 1 for facebook 2 for twitter ..)
    //  *
    //  */
    // public function testPinterestAccount($user_id, $network, $profile)
    // {
    //     if($this->Social->check40X($profile))
    //     {
    //         $this->archiveSocialAccount($user_id, $profile, $network);
    //     }
    //     else
    //     {
    //         return;
    //     }
    // }
    //
    // /**
    //  * Test if Youtube account exist if the account dosen't exist move it to social_accounts_archive table
    //  *
    //  * @param int 		$user_id        id of user
    //  * @param string 	$profile        social profile
    //  * @param int 		$network_id     netwrk id (like 1 for facebook 2 for twitter ..)
    //  *
    //  */
    // public function testYoutubeAccount($user_id, $network, $profile)
    // {
    //     if($this->Social->check40X($profile))
    //     {
    //         $this->archiveSocialAccount($user_id, $profile, $network);
    //     }
    //     else
    //     {
    //         return;
    //     }
    // }
    //
    // /**
    //  * Test if Vine account exist if the account dosen't exist move it to social_accounts_archive table
    //  *
    //  * @param int 		$user_id        id of user
    //  * @param string 	$profile        social profile
    //  * @param int 		$network_id     netwrk id (like 1 for facebook 2 for twitter ..)
    //  *
    //  */
    // public function testVineAccount($user_id, $network, $profile)
    // {
    //     $username = str_replace("https://vine.co/u/", "", $profile);
    //
    //     //test if the str_replace has executed else stop the script
    //     if(empty($username))
    //     {
    //         return;
    //     }
    //     else
    //     {
    //         $vine_user = $this->vine_api->getUser($username);
    //
    //         if(empty($vine_user))
    //         {
    //             $this->archiveSocialAccount($user_id, $profile, $network);
    //         }
    //         else
    //         {
    //             return;
    //         }
    //     }
    // }
    //
    // /**
    //  * Test if Twitter account exist if the account dosen't exist move it to social_accounts_archive table
    //  *
    //  * @param int 		$user_id        id of user
    //  * @param string 	$profile        social profile
    //  * @param int 		$network_id     netwrk id (like 1 for facebook 2 for twitter ..)
    //  *
    //  */
    // public function testTwitterAccount($user_id, $network, $profile)
    // {
    //     $username = str_replace("https://twitter.com/", "", $profile);
    //
    //     //test if the str_replace has executed else stop the script
    //     if(empty($username))
    //     {
    //         return;
    //     }
    //     else
    //     {
    //         $data = $this->twconnect->app_tw_get("users/show", array("screen_name" => $username));
    //
    //         if(!empty($data->errors))
    //         {
    //             $this->archiveSocialAccount($user_id, $profile, $network);
    //         }
    //         else
    //         {
    //             return;
    //         }
    //     }
    // }

    public function getAllAccounts(){
        return $this->db->get("social_accounts")->result();
    }

    public function getNotUpdatedAccounts(){
        $this->db->where("updated", "0");
        return $this->db->get("social_accounts")->result();
    }

    public function countAllFollowers()
    {
        $conditions = array(
            'u.is_active' => 1,
            'u.completed' => 1,
            'u.deleted'   => 0
        );

        $this->db->select_sum('s.value');
        $this->db->from('social_followers as s');
        $this->db->join('auth_user as u', 'u.id = s.id_user', 'RIGHT');
        $this->db->where($conditions);

        $result = $this->db->get()->row();

        return $result->value;
    }

    public function setUpdated($id_account){
        $this->db->where('id', $id_account);
        $this->db->set('updated', '1');
        $this->db->update('social_accounts');
    }

    public function countLast24h()
    {
        $conditions = array(
            'u.is_active' => 1,
            'u.completed' => 1,
            'u.deleted'   => 0
        );

        $date = date('Y-m-d H:i:s', strtotime('-1 day'));
        $this->db->select_sum('value');
        $this->db->from('social_followers as s');
        $this->db->join('auth_user as u', 'u.id = s.id_user', 'RIGHT');
        $this->db->where("created >= '".$date."'");
        $this->db->where($conditions);
        $result = $this->db->get()->row();

        return (int)$result->value;
    }

    public function countNetworks($id_user) {
        $linked = $this->db->query("SELECT COUNT(*) AS total FROM social_accounts WHERE id_user={$id_user}")->row();
        return $linked->total;
    }

    public function deleteNetworkFollowers($name, $id_user){
        $network = $this->getNetworkID($name);
        $this->db->where(array(
                'network' => $network,
                'id_user' => $id_user
            ));
        $this->db->delete('social_followers');
    }

    public function updateNetwork($name, $id_user, $value, $profile = false){
        $value = intval($value);
        $network = $this->getNetworkID($name);
        $linked = $this->isLinked($name, $id_user);

        if($profile && !$this->uniqueAccount($id_user, $profile)){
            $this->session->set_flashdata('social_not_unique', true);
            return;
        }

        if ($linked) {
            $old_profile = $this->getNetworkProfile($name, $id_user);
            if($profile && $profile != $old_profile){
                $this->db->where(array(
                        'network' => $network,
                        'id_user' => $id_user
                    ));
                $this->db->set('profile', $profile);
                $this->db->update('social_accounts');
                $this->deleteNetworkFollowers($name, $id_user);
            }

            $followers = $this->countFollowers($id_user, $name);

            // Test if the value is empty then update the followers to 0
            // Else the fallowers will be updated to the curent value
            if(!empty($value) && $value != 0 && $value != $followers)
            {
                $value = $value - $followers;
                // Insert new Social Followers
                $this->db->set('id_user', $id_user);
                $this->db->set('network', $network);
                $this->db->set('value', $value);
                $this->db->set('created', 'NOW()', false);
                $this->db->insert('social_followers');
            }
        } else {
            // Insert Social Account
            $this->db->set('id_user', $id_user);
            $this->db->set('network', $network);
            $this->db->set('profile', $profile);
            $this->db->insert('social_accounts');

            // Insert Social Followers
            $this->db->set('id_user', $id_user);
            $this->db->set('network', $network);
            $this->db->set('value', $value);
            $this->db->set('created', 'NOW()', false);
            $this->db->insert('social_followers');
        }

    }

    public function uniqueAccount($id_user, $profile){
        $this->db->where('profile', $profile);
        $results = $this->db->get('social_accounts')->result();
        if(count($results) > 1){
            return false;
        }
        else if(count($results) == 1){
            $result = $results[0];
            if ($result->id_user == $id_user) {
                return true;
            }

            return false;
        }
        else {
            return true;
        }
    }

    public function countFollowers($id_user, $name = false){
        $network = $this->getNetworkID($name);
        $this->db->select_sum('value');
        if($name){
            $this->db->where(array(
                    'id_user' => $id_user,
                    'network' => $network
                ));
        } else {
            $this->db->where('id_user', $id_user);
        }
        $result = $this->db->get('social_followers')->row();
        return (int)$result->value;
    }

    public function checkNetwork($name, $id_user) {
        $network = $this->getNetworkID($name);
        $linked = $this->isLinked($name, $id_user);

        if ($linked) {
            return $this->countFollowers($id_user, $name);
        }
        return false;
    }

    public function isLinked($name, $id_user){
        $network = $this->getNetworkID($name);
        $linked = $this->db->query("SELECT * FROM social_accounts WHERE id_user={$id_user} AND network={$network}")->row();

        if (count($linked)) {
            return true;
        }
        return false;
    }

    public function getNetworkProfile($name, $id_user) {
        $network = $this->getNetworkID($name);
        $linked = $this->db->query("SELECT * FROM social_accounts WHERE id_user={$id_user} AND network={$network}")->row();

        if (count($linked)) {
            return $linked->profile;
        }
        return false;
    }

    public function getNetworkID($name){
        switch ($name) {
            case 'facebook':
                return 1;

            case 'twitter':
                return 2;

            case 'instagram':
                return 3;

            case 'tumblr':
                return 4;

            case 'pinterest':
                return 5;

            case 'linkedin':
                return 6;

            case 'youtube':
                return 7;

            case 'vine':
                return 8;
        }
    }
    public function getNetworkName($id){
        switch ($id) {
            case 1:
                return 'facebook';

            case 2:
                return 'twitter';

            case 3:
                return 'instagram';

            case 4:
                return 'tumblr';

            case 5:
                return 'pinterest';

            case 6:
                return 'linkedin';

            case 7:
                return 'youtube';

            case 8:
                return 'vine';
        }
    }


}

?>
