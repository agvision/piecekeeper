<?php

// Include two files from google-php-client library in controller
include_once APPPATH . "libraries/youtube/src/Google/autoload.php";
include_once APPPATH . "libraries/youtube/src/Google/Client.php";
include_once APPPATH . "libraries/youtube/src/Google/Service/YouTube.php";

class Youtube_api
{
    public $client_id;
    public $client_secret;
    public $redirect_uri;
    public $simple_api_key;
    public $client;
    public $youtube;


    /**
     * Redirect back to the app.
     *
     * @param  [string] $hash               [hash]
     * @param  [string/bool] $page_redirect ['profile' or false]
     * @return [type]                [description]
     */
	public function youtubeRedirect($hash = '', $page_redirect = false)
	{
        if(!$this->isConnected())
        {
            $auth_url = $this->getAuthUrl();

            redirect($auth_url);
        }
        else
        {
            if(!empty($hash) && $page_redirect == false)
            {
                redirect('/social/youtube/' . $hash);
            }
            else
            {
                redirect('profile');
            }
        }
	}

    /**
     * Connect with OAuth.
     *
     * @return [type] [description]
     */
    public function youtubeConnect()
    {
        $ci =& get_instance();
		$ci->config->load('youtube', true);
		$config = $ci->config->item('youtube');

        // Store values in variables from project created in Google Developer Console
        $this->client_id = $config['client_id'];
        $this->client_secret = $config['client_secret'];
        $redirect_uri = base_url() . 'profile/youtube_callback';

        // $simple_api_key = $config['simple_api_key'];

        // Create Client Request to access Google API
        $this->client = new Google_Client();
        $this->client->setApplicationName("PieceKeeper");
        $this->client->setClientId($this->client_id);
        $this->client->setClientSecret($this->client_secret);
        $this->client->setRedirectUri($redirect_uri);
        // $this->client->setDeveloperKey($simple_api_key);
        $this->client->setAccessType('offline');
        $this->client->setScopes('https://www.googleapis.com/auth/youtube');

        // Define an object that will be used to make all API requests.
        $this->youtube = new Google_Service_YouTube($this->client);


        if (isset($_GET['code']))
        {
            if(!empty($_GET['state']))
            {
                if (strval($ci->session->userdata('state')) !== strval($_GET['state']))
                {
                    die('The session state did not match.');
                }
            }

            $this->client->authenticate($_GET['code']);
            $ci->session->set_userdata('token', $this->client->getAccessToken());
        }

        if (!empty($ci->session->userdata('token')))
        {
            $this->client->setAccessToken($ci->session->userdata('token'));
            // echo '<code>' . $ci->session->userdata('token') . '</code>';
        }

        $state = mt_rand();
        $this->client->setState($state);
        $ci->session->set_userdata('state', $state);
    }


    /**
     * Verify if the user is connected with Youtube.
     *
     * @return [bool] [description]
     */
    public function isConnected()
    {
        $ci =& get_instance();

        if ($this->client->getAccessToken())
        {
            // die('true');
            $ci->session->set_userdata('token', $this->client->getAccessToken());
            return true;
        }
        else
        {
            return false;
        }
    }


    /**
     * Get Auth URL.
     *
     * @return [string] [auth url]
     */
    public function getAuthUrl()
    {
        $ci =& get_instance();
		$ci->config->load('youtube', true);
		$config = $ci->config->item('youtube');

        $this->client->setState($ci->session->userdata('state'));
        $authUrl = $this->client->createAuthUrl();

        return $authUrl;
    }


    public function getChannelID() {
        if(!$this->isConnected())
        {
            return null;
        }

        try
        {
            $statisticsResponse = $this->youtube->channels->listChannels('statistics', array(
                'mine' => 'true',
            ));

            $id = $statisticsResponse['items'][0]['id'];

            return $id;
        }
        catch (Google_ServiceException $e)
        {
            //die(' server error');
            return null;
            // echo sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
        catch (Google_Exception $e)
        {
            //die(' client error');
            return null;
            // echo sprintf('<p>An client error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
    }


    public function getFollowers() {
        if(!$this->isConnected())
        {
            return null;
        }

        try
        {
            $statisticsResponse = $this->youtube->channels->listChannels('statistics', array(
                'mine' => 'true',
            ));

            $subscribers = $statisticsResponse['items'][0]['statistics']['subscriberCount'];

            return $subscribers;
        }
        catch (Google_ServiceException $e)
        {
            //die(' server error');
            return null;
            // echo sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
        catch (Google_Exception $e)
        {
            //die(' client error');
            return null;
            // echo sprintf('<p>An client error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
    }


    /**
     * Get the number of views.
     *
     * @return [int] [number of views]
     */
    public function getViews()
    {
        if(!$this->isConnected())
        {
            return;
        }

        try
        {
            $statisticsResponse = $this->youtube->channels->listChannels('statistics', array(
                'mine' => 'true',
            ));

            // Get the number of views.
            $youtube_views =  $statisticsResponse['items'][0]['statistics']['viewCount'];

            return $youtube_views;
        }
        catch (Google_ServiceException $e)
        {
            //die(' server error');
            echo sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
        catch (Google_Exception $e)
        {
            //die(' client error');
            echo sprintf('<p>An client error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
    }

    public function getUserDetails()
    {
        if(!$this->isConnected())
        {
            return;
        }

        try
        {
            $statisticsResponse = $this->youtube->channels->listChannels('contentOwnerDetails', array(
                'mine' => 'true',
            ));

            // Get the number of views.
            $youtube_views =  $statisticsResponse['items'][0]['statistics']['viewCount'];

            return $youtube_views;
        }
        catch (Google_ServiceException $e)
        {
            //die(' server error');
            echo sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
        catch (Google_Exception $e)
        {
            //die(' client error');
            echo sprintf('<p>An client error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
    }

    /**
     * Get user's channels.
     *
     * @return [type] [description]
     */
    public function getChannels()
    {
        if(!$this->isConnected())
        {
            return;
        }

        try
        {
            // Call the channels.list method to retrieve information about the
            // currently authenticated user's channel.
            $channelsResponse = $this->youtube->channels->listChannels('contentDetails', array(
                'mine' => 'true',
            ));

            return $channelsResponse['items'];
        }
        catch (Google_ServiceException $e)
        {
            //die(' server error');
            echo sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
        catch (Google_Exception $e)
        {
            //die(' client error');
            echo sprintf('<p>An client error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
    }

    /**
     * Get uploaded videos from a channel.
     * Specify what videos to get (uploads, favourites or likes).
     *
     * @param  [type] $channel [channel]
     * @param  [string] $type  [type: uploads, favourites or likes]
     * @return [array]         [uploaded videos from $channel]
     */
    public function getVideosFromChannel($channel, $type = 'uploads')
    {
        // Extract the unique playlist ID that identifies the list of videos
        // uploaded/favourites/likes to the channel, and then call the playlistItems.list method
        // to retrieve that list.

        if(!$this->isConnected())
        {
            return;
        }

        if(empty($channel) || ($type != 'uploads' && $type != 'favourites' && $type !='likes'))
        {
            return false;
        }

        try
        {
            $videosListId  = $channel['contentDetails']['relatedPlaylists'][$type];
            $videos        = $this->getVideosFromList($videosListId);

            return $videos;
        }
        catch (Google_ServiceException $e)
        {
            //die(' server error');
            echo sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
        catch (Google_Exception $e)
        {
            //die(' client error');
            echo sprintf('<p>An client error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }

    }

    /**
     * Get playlists from the current user.
     *
     * @return [array] [playlists]
     */
    public function getPlaylists()
    {
        if(!$this->isConnected())
        {
            return;
        }

        try
        {
            // Get the playlists.
            $playlistResponse = $this->youtube->playlists->listPlaylists('snippet', array(
                'mine' => 'true'
            ));

            $playlistItems = $playlistResponse['items'];

            return $playlistItems;
        }
        catch (Google_ServiceException $e)
        {
            //die(' server error');
            echo sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
        catch (Google_Exception $e)
        {
            //die(' client error');
            echo sprintf('<p>An client error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
    }


    /**
     * Get videos from playlist.
     *
     * @param  [int] $playlist_id [playlist id]
     * @return [array]            [videos]
     */
    public function getVideosFromPlaylist($playlist_id)
    {
        if(empty($playlist_id))
        {
            return;
        }

        try
        {
            // Get the videos
            $videos = $this->getVideosFromList($playlist_id);

            return $videos;
        }
        catch (Google_ServiceException $e)
        {
            //die(' server error');
            echo sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
        catch (Google_Exception $e)
        {
            //die(' client error');
            echo sprintf('<p>An client error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
    }


    /**
     * Get videos from specific list.
     *
     * @param  [int]  $list_id          [video list id]
     * @param  [int] $results_per_page  [results per page]
     * @return [array]                  [videos(id, title, category, descrition)]
     */
    public function getVideosFromList($list_id, $results_per_page = 2)
    {
        if(empty($list_id))
        {
            return;
        }

        try
        {
            $nextPageToken = '';
            $videos        = array();

            while($nextPageToken !== null)
            {
                $videosItemsResponse = $this->youtube->playlistItems->listPlaylistItems('snippet', array(
                    'playlistId' => $list_id,
                    'maxResults' => $results_per_page,
                    'pageToken'  => $nextPageToken
                ));

                if(empty($videosItemsResponse['items']))
                {
                    return false;
                }
                else
                {
                    foreach ($videosItemsResponse['items'] as $playlistItem)
                    {
                        $video             = array();
                        $video_title       = $playlistItem['snippet']['title'];
                        $video_id          = $playlistItem['snippet']['resourceId']['videoId'];
                        $video_description = $playlistItem['snippet']['description'];

                        // Take video category id.
                        $video_category_id = $this->getVideoCategoryID($video_id);

                        // Take video category name.
                        $video_category_name = $this->getVideoCategoryName($video_category_id);

                        $video['id']          = $video_id;
                        $video['title']       = $video_title;
                        $video['category']    = $video_category_name;
                        $video['description'] = $video_description;

                        $videos[] = $video;
                    }
                }

                // Add  next page token
                $nextPageToken = $videosItemsResponse['nextPageToken'];
            }

            return $videos;
        }
        catch (Google_ServiceException $e)
        {
            //die(' server error');
            echo sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
        catch (Google_Exception $e)
        {
            //die(' client error');
            echo sprintf('<p>An client error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
    }


    /**
     * Get video category id.
     *
     * @param  [int] $video_id [video id]
     * @return [int]           [category id]
     */
    public function getVideoCategoryID($video_id)
    {
        try
        {
            // Take video category id.
            $videoResponse = $this->youtube->videos->listVideos("snippet", array(
                'id' => $video_id
            ));

            $video             = $videoResponse[0];
            $video_category_id = $video['snippet']['categoryId'];

            return $video_category_id;
        }
        catch (Google_ServiceException $e)
        {
            //die(' server error');
            echo sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
        catch (Google_Exception $e)
        {
            //die(' client error');
            echo sprintf('<p>An client error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
    }

    /**
     * Get category name.
     *
     * @param  [int] $video_category_id [category id]
     * @return [string]                 [category name]
     */
    public function getVideoCategoryName($video_category_id)
    {
        try
        {
            // Take video category name.
            $videoCatgeoryResponse = $this->youtube->videoCategories->listVideoCategories('snippet', array(
                'id' => $video_category_id
            ));

            $video_category_name = $videoCatgeoryResponse[0]['snippet']['title'];

            return $video_category_name;
        }
        catch (Google_ServiceException $e)
        {
            //die(' server error');
            echo sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
        catch (Google_Exception $e)
        {
            //die(' client error');
            echo sprintf('<p>An client error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        }
    }

    /**
     * Unset the session and the Google Objects.
     */
    public function unsetSession()
    {
        $ci =& get_instance();

        unset($this->client);
        unset($this->youtube);

        $ci->session->unset_userdata('state');
        $ci->session->unset_userdata('token');
        $ci->session->unset_userdata('yt_hash');
        $ci->session->unset_userdata('yt_redirect');

    }
}

?>
