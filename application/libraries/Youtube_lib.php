<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Youtube_lib
{

		public function get_subs($username)
		{
				$public_key 			 = 'AIzaSyD14Ng6Z4itzcR_BcCspXB9kbts2ujhpKI';
				$account_details   = 'https://www.googleapis.com/youtube/v3/channels?part=snippet%2CcontentDetails&forUsername='.$username.'&key='.$public_key;

				if(!$this->check40X($account_details))
				{
						$account_details   = @file_get_contents($account_details);
						$account_details   = json_decode($account_details);

						$youtube_account   = 'https://www.youtube.com/user/'.$username;
						if(!empty($account_details) && !property_exists($account_details, 'error') && !$this->check40X($youtube_account))
						{
								$account_id        = $account_details->items[0]->id;
								$statistic_details = 'https://www.googleapis.com/youtube/v3/channels?part=statistics&id='.$account_id.'&key='.$public_key;

								if(!$this->check40X($statistic_details))
								{
										$statistic_details = @file_get_contents($statistic_details);
										$statistic_details = json_decode($statistic_details);

										if(!empty($statistic_details))
										{
												return $statistic_details->items[0]->statistics->subscriberCount;
										}
										else
										{
												return -1;
										}
								}
								else
								{
										return -1;
								}

							}
							else
							{
									return -1;
							}
				}
				else
				{
						return -1;
				}

		}

		public function check40X($url)
		{
      	$handle = curl_init($url);
      	curl_setopt($handle, CURLOPT_FOLLOWLOCATION, true);
      	curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

	      /* Get the HTML or whatever is linked in $url. */
	      $response = curl_exec($handle);

	      /* Check for 404 (file not found). */
	      $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
	      curl_close($handle);

	      if($httpCode >= 400)
				{
	          return true;
	      }

	      return false;
    }


}

?>
