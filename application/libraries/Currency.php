<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Currency
{

	function __construct() {
		$this->CI =& get_instance();
		$this->CI->load->model('currencies_conversion_model', 'Conversion');
  	}

	public function convertFromUSD($value, $currency){
		return $this->convert($value, 'USD', $currency);
	}

	public function convertToUSD($value, $currency){
		return $this->convert($value, $currency, 'USD');
	}

	public function convertToEUR($value, $currency){
		return $this->convert($value, $currency, 'EUR');
	}

	public function convert($value, $from, $to){
		$rate = $this->CI->Conversion->getValue($from.$to);

		if(!$rate){
			$request_url = "http://download.finance.yahoo.com/d/quotes.csv?s=".$from.$to."=X&f=l1";
			$rate        = file_get_contents($request_url);
			$this->CI->Conversion->insertValue($from.$to, $rate);
		}

		$conversion  = $value * $rate;
		return number_format($conversion, 2, '.', '');
	}

	// public function convert($value, $from, $to){
	// 	$request_url = "http://download.finance.yahoo.com/d/quotes.csv?s=".$from.$to."=X&f=l1";
	// 	$rate        = file_get_contents($request_url);

	// 	$conversion  = $value * $rate;
		
	// 	return number_format($conversion, 2, '.', '');
	// }

}

 ?>