<?php

/*
|--------------------------------------------------------------------------
| Instagram
|--------------------------------------------------------------------------
|
| Instagram client details
|
*/

$config['instagram_client_name']	= 'OnePieceApp';
$config['instagram_client_id']		= 'da394bbfa9314a138056501a6e394692';
$config['instagram_client_secret']	= 'd304c4a900df459bb78a57e67aba9457';
$config['instagram_callback_url']	= 'http://piecekeeper.onepiece.com/profile/instagram_callback';
$config['instagram_website']		= 'http://piecekeeper.onepiece.com/onepiece';
$config['instagram_description']	= 'OnePiece Shop Affiliate Program';

// There was issues with some servers not being able to retrieve the data through https
// If you have this problem set the following to FALSE 
// See https://github.com/ianckc/CodeIgniter-Instagram-Library/issues/5 for a discussion on this
$config['instagram_ssl_verify']		= TRUE;

?>