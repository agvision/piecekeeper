<div class="package-view content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <?php if ($package->status==1): ?>
        <div class="alert-box success no-image">This package has been approved</div>
        <?php elseif($package->status==0): ?>
        <div class="alert-box warning no-image">This package has not been approved yet</div>
        <?php else: ?>
        <div class="alert-box error no-image">This package has been rejected </div>
        <?php endif ?>
        <div class="header-stats">
            <?=$title?>
                <?php if($package->status==0) { ?>
                <a href="<?=$this->config->base_url()?>admin/packages/approve/<?=$package->id?>" class="approve-package">
                    <button class="save-profile-black admin-form-button btn-success">Approve</button>
                </a>
                <!-- <a href="<?=$this->config->base_url()?>admin/packages/reject/<?=$package->id?>"><button class="save-profile-white admin-form-button btn-danger">Reject</button></a> --><a class="btn-danger save-profile-white admin-form-button" href="#" id='reject'>Reject</a>
                <form id='reject-form' method='post' class="package-view-reject" action="<?php echo base_url('admin/packages/reject/'.$package->id) ?>">
                    <textarea placeholder="Reason" name='reason' class="package-view-reject-reason"></textarea>
                    <input type='submit' value='Update' class='btn-success' /> <a href='#' id='cancel-reject' class="package-view-reject-cancel"> Cancel</a> </form>
                <?php } ?> </div>

        <?php if ($this->session->flashdata('response_error')): ?>
            <div class="alert-box error no-image">There was an order error and the package has not been approved.<br><br><b>Package ID: <?php echo $this->session->flashdata('response_error') ?></b><?php if ($this->session->flashdata('package_error')): ?><br/><b>Details about the error</b>: <?php echo $this->session->flashdata('package_error'); ?><?php endif; ?></div>
        <?php endif ?>

        <table border="0" class="package-view-piecekeeper">
            <tr>
                <td class="package-view-piecekeeper-name">PieceKeeper Name: </td>
                <td><b><?=$package->first_name . " " . $package->last_name?></b> </td>
            </tr>
            <tr>
                <td>Type: </td>
                <td><b>
                        <?php if ($package->type == 1): ?>
                            Package for Level 2
                        <?php elseif ($package->type == 2): ?>
                            Package for Level 3
                        <?php endif ?>
                    </b> </td>
            </tr>
            <?php if ($package->type == 2): ?>
                <tr>
                    <td>Product SKU: </td>
                    <td> <b><?=$package->free_product?></b> </td>
                </tr>
                <tr>
                    <td>Product Size: </td>
                    <td> <b><?=$package->package_product_size?></b> </td>
                </tr>

                <?php if (is_object($package_order) &&
                      property_exists($package_order,'shipping_address_1') &&
                      property_exists($package_order,'shipping_address_2') &&
                      property_exists($package_order,'shipping_city') &&
                      property_exists($package_order,'shipping_country') &&
                      property_exists($package_order,'shipping_state') ): ?>

                    <tr>
                        <td>Shipping Address: </td>
                        <td> <b><?=$package_order->shipping_address_1.", ".$package_order->shipping_address_2.", ".$package_order->shipping_city.", ".$package_order->shipping_country.", ".$package_order->shipping_state?></b> </td>
                    </tr>
                <?php endif ?>
            <?php else: ?>
                <tr>
                    <td>Shipping Address: </td>
                    <td> <b><?=$package->address1.", ".$package->address2.", ".$package->city.", ".$package->country.", ".$package->state?></b> </td>
                </tr>
            <?php endif ?>

            <tr>
                <td colspan="2"><a href="<?=$this->config->base_url()?>admin/user/view/<?=$package->id_user?>">View user profile</a> </td>
            </tr>
        </table>
        <?php if ($package->status== -1): ?>
        <a href="<?php echo base_url('admin/packages/delete/'.$package->id) ?>" class="btn-danger save-profile-black admin-form-button delete-item user-view-admin-actions-btn" style="opacity: 1;">Remove</a>
        <?php endif ?>
    </div>
</div>
