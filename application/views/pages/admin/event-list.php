    </div>
    <div class="content-bottom">
      <?php $this->load->view('pages/admin/menu') ?>
        <div class="column-right">
          <div class="header-stats clearfix">
              Event Administration
              <a href="<?=base_url('admin/event/edit')?>" class="btn-success save-profile-black admin-form-button">Add</a>
          </div>
          <table class="admin-table">
            <thead>
              <th>Event Name</th>
              <th>Edit</th>
            </thead>
            <tbody>
              <?php
                foreach( $entries as $entry )
                {
                  echo '<tr>';
                  echo '<td>' . $entry['name'] . '</td>';
                  echo '<td>' . '<a href="' . base_url('/admin/event/edit/' . $entry['id']) . '">' .
                    '<img src="' . base_url('images/edit.png') . '" /></a>' . '</td>';
                  echo '</tr>';
                }
              ?>
            </tbody>
          </table>
        </div>
      </div>
