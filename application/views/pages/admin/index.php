
    <div class="content-bottom">
      <?php $this->load->view('pages/admin/menu') ?>
        <div class="column-right">
          <div class="top-points-board">
          	  <a href="#" class="top-cell">
              	  <div class="cell-text"><?php echo number_format($total_social); ?></div>
                  <div class="cell-sub-text">TOTAL FOLLOWERS</div>
              </a>
              <a href="<?php echo base_url('/admin/user') ?>" class="top-cell">
                  <div class="cell-text"><?php echo number_format(intval($statistics['total_ambasadors'])); ?></div>
                  <div class="cell-sub-text">PIECEKEEPERS</div>
              </a>
              <a href='#pending-piecekeepers' class="top-cell">
              	<div class="cell-text"><?=number_format(intval($statistics['pending_ambasadors']));?></div>
                  <div class="cell-sub-text">PENDING PIECEKEEPERS</div>
              </a>
              <a href="<?php echo base_url('/admin/sales/view/all') ?>" class="top-cell">
              	<div class="cell-text"><?php echo number_format(intval($statistics['total_sales'])); ?></div>
                  <div class="cell-sub-text">TOTAL SALES</div>
              </a>
              <a href="<?php echo base_url('/admin/sales/history') ?>" class="top-green-cell">
              	<div class="cell-text"><?php echo $this->currencies->format($statistics['total_earnings']); ?></div>
                  <div class="cell-sub-text">TOTAL EARNINGS</div>
              </a>
              <div class="clear"></div>
          </div>

          <div class="top-points-board">
              <a href="#" class="top-cell">
                <div class="cell-text"><?php echo number_format($total_social_last24h); ?></div>
                  <div class="cell-sub-text">FOLLOWERS <br> LAST 24H</div>
              </a>
              <a href="<?php echo base_url('/admin/sales/history') ?>" class="top-cell">
                <div class="cell-text"><?php echo number_format(intval($statistics['sales_points'])); ?></div>
                  <div class="cell-sub-text">SALES POINTS</div>
              </a>
              <a href='<?php echo base_url('/admin/points/view/2')?>' class="top-cell">
                <div class="cell-text"><?=number_format(intval($statistics['bonus_points']));?></div>
                  <div class="cell-sub-text">BONUS POINTS</div>
              </a>
              <a href="<?php echo base_url('/admin/points/view')?>" class="top-cell">
                <div class="cell-text"><?php echo number_format(intval($statistics['total_points'])); ?></div>
                  <div class="cell-sub-text">TOTAL POINTS</div>
              </a>
              <a href="<?php echo base_url('/admin/sales/history') ?>" class="top-green-cell">
                <div class="cell-text"><?php echo $this->currencies->format($statistics['commission']); ?></div>
                  <div class="cell-sub-text">TOTAL COMMISSION</div>
              </a>
              <div class="clear"></div>
          </div>

          <br>

          <div id="admin-requests-menu" style='margin-top: 10px; border: 0;'>
              <a href="<?php echo base_url('admin/coupons') ?>" class='btn-success'>Discount Codes</a>
              <a href="<?php echo base_url('admin/payments') ?>" class='btn-success'>Payment Requests</a>
              <a href="<?php echo base_url('admin/master_requests') ?>" class='btn-success'>Master Requests</a>
              <a href="<?php echo base_url('admin/slaves_requests') ?>" class='btn-success'>UnderKeeper Requests</a>
          </div>

          <div style='margin-bottom: 20px; margin-top: 40px;'>
              <a href="<?php echo base_url('/admin/notifications_history/') ?>">Notifications History</a>
          </div>

          <?php if (empty($notifications)): ?>
              <p>You have no new notifications.</p>
          <?php else: ?>
              <?php foreach ($notifications as $n): ?>
                  <div class='alert-box'>
                    <div class='admin-notification-message'>
                      <a href="<?php echo $n['link'] ?>"><?php echo $n['message'] ?></a>
                    </div>
                    <div class='admin-notification-date'>
                      <?php echo date('d M Y', strtotime($n['created'])) ?> <br> <?php echo date('H:i:s', strtotime($n['created'])) ?>
                    </div>
                  </div>
              <?php endforeach ?>
          <?php endif ?>

            <div id='pending-piecekeepers' class="header-stats">
                Pending PieceKeepers
            </div>

           <?php if(empty($pending_ambasadors)) { ?>
            <div class="alert-box warning">
                There are no pending PieceKeepers
            </div>
           <?php } else { ?>
          <table style='width:100%' id='pending-table' class='tablesorter'>
            <thead>
                <tr style='text-align: left'><th>Name</th><th>Email address</th><td>Actions</td></tr>
            </thead>
            <?php foreach ($pending_ambasadors as $user) { ?>
              <tr>
                <td><a href="<?php echo base_url('admin/user/view/'.$user->id) ?>"><?=$user->first_name . " " . $user->last_name?></a></td>
                <td><?=$user->email?></td>
                <td>
                  <a href='<?=$this->config->base_url()?>admin/user/change_status/<?=$user->id?>'>Approve</a> |
                  <a href='<?=$this->config->base_url()?>admin/user/view/<?=$user->id?>'>View</a> |
                  <a href='<?=$this->config->base_url()?>admin/user/delete_pending/<?=$user->id?>' class="delete-item">Delete</a>
                </td>
              </tr>
            <?php } ?>
            <tr>
               <td></td><td></td><td></td>
            </tr>
          </table>
           <?php } ?>



      </div>
  </div>
