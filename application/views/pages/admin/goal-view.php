<div class="content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats clearfix">
            <?=$title?>
        </div>
        <?php if (count($winners)): ?>
        <table class="full-width">
            <thead>
                <tr class="text-align-left">
                    <th>Name</th>
                    <th>Email</th>
                    <th>Points</th>
                </tr>
            </thead>
            <?php foreach ($winners as $w) { ?>
            <tr>
                <td>
                    <a href="<?php echo base_url('/admin/user/view/'.$w->id) ?>">
                        <?php echo $w->first_name.' '.$w->last_name ?></a>
                </td>
                <td>
                    <?php echo $w->email ?></td>
                <td>
                    <?php echo number_format($w->points) ?></td>
            </tr>
            <?php } ?> </table>
        <?php else: ?>
        <div class='form-label no-mt'>There are no winners.</div>
        <?php endif ?> </div>
</div>
