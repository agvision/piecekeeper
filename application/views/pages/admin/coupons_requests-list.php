<div class="coupons-request content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats"> Coupon Requests </div>
        <br>
        <div id="admin-requests-menu" class="coupons-request-admin-menu"> <a href="<?php echo base_url('admin/coupons') ?>" class='btn-success'>Discount Codes</a> <a href="<?php echo base_url('admin/payments') ?>" class='btn-success'>Payment Requests</a> <a href="<?php echo base_url('admin/master_requests') ?>" class='btn-success'>Master Requests</a> <a href="<?php echo base_url('admin/slaves_requests') ?>" class='btn-success'>UnderKeeper Requests</a> </div>
        <br>
        <?php $error_flashdata=$this->session->flashdata('error'); if ($this->session->flashdata('error') != ''): ?>
        <div class='form-alert-wrapper'>
            <?php echo $error_flashdata; ?> </div>
        <?php endif ?>
        <?php if ($this->session->flashdata('success') != ''): ?>
        <div class='form-alert-wrapper'>
            <div class='alert-box success form-alert'>
                <?php echo $this->session->flashdata('success'); ?> </div>
        </div>
        <?php endif ?>
        <?php if (count($coupons)): ?>
        <table class="admin-table">
            <thead>
                <th width='120'>Code</th>
                <th width='110'>Discount</th>
                <th width='130'>PieceKeeper</th>
                <th width='100'>Type</th>
                <th width='150'>Created</th>
            </thead>
            <tbody>
                <?php foreach ($coupons as $c): ?>
                <tr>
                    <td>
                        <?php echo $c->code ?></td>
                    <td>
                        <?php if ($c->amount && $c->amount != '0.00'): ?>
                        <?php echo $c->amount." ".$c->currency ?>
                        <?php else: ?>
                        <?php echo $c->percent." %" ?>
                        <?php endif ?> </td>
                    <td>
                        <a href="<?php echo base_url('admin/user/view/'.$c->id_creator) ?>">
                            <?php echo $c->first_name.' '.$c->last_name ?></a>
                    </td>
                    <td>
                        <?php if ($c->type == '1'): ?> Singleuse
                        <?php elseif ($c->type == '2'): ?> Multiuse
                        <?php endif ?> </td>
                    <td>
                        <?php echo date( 'Y-M-d H:i:s', strtotime($c->created)) ?></td>
                </tr>
                <?php endforeach ?> </tbody>
        </table>
        <?php else: ?>
        <div class='form-label no-mt'>There are no Coupons.</div>
        <?php endif ?> </div>
</div>
