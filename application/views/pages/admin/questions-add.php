<div class="content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats full-width"> Add Question </div>
        <?php if (validation_errors()): ?>
        <div class='form-alert-wrapper'>
            <?php echo validation_errors( "<div class='alert-box error form-alert'>", "</div>"); ?> </div>
        <?php endif ?>
        <form method='post' action="<?php echo base_url('admin/questions/add/') ?>">
            <div>
                <input type='text' name='question' placeholder='Your Question' /> </div>
            <input type='hidden' name='trigger' value='add' />
            <div class="dotted-line questions-dotted-line"></div>
            <button class="btn-black" type="submit">Add</button>
        </form>
    </div>
</div>
