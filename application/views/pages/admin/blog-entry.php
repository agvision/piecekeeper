</div>
<div class="content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <?php if(isset($user)) { ?>
        <?php if($entry[ 'aproved']==0) { ?>
        <div class="alert-box warning"><b>This post is added by <?=$user['first_name'] . " " . $user['last_name']?></b> and it is not active.</div>
        <?php } else { ?>
        <div class="alert-box success"><b>This post is added by <?=$user['first_name'] . " " . $user['last_name']?></b> and it is currently active.</div>
        <?php } ?>
        <br>
        <a href="<?=$this->config->base_url()?>admin/blogPosts/change_status/<?=$entry['id']?>">
            <button class="btn-black">
                <?=($entry[ 'aproved']==1 ? "Reject" : "Approve")?>
            </button>
        </a>
        <br>
        <br>
        <?php } ?>
        <form action="<?php echo base_url('admin/blogPosts/save') ?>" method='post' enctype='multipart/form-data'>
            <input type="hidden" name="id" value="<?=$entry['id']?>" />
            <div class="header-stats first clearfix">
                <?=$title?>
                    <?php if ($entry[ 'id'] !='' ) echo '<a class="btn-danger save-profile-white admin-form-button delete-item"
                    href="' . base_url( 'admin/blogPosts/delete/' . $entry[ 'id']) . '">Delete</a>'; ?>
                    <?php if ($entry[ 'id'] !='' ): ?>
                    <input type="submit" class="btn-success save-profile-black admin-form-button" value="Update">
                    <?php else: ?>
                    <input type="submit" class="btn-success save-profile-black admin-form-button" value="Add">
                    <?php endif ?> </div>
            <br />
            <?php if ( strlen($this->session->userdata("blog_errors")) > 0 ): ?>
            <div class='form-alert-wrapper'>
                <div class='alert-box error form-alert'>
                    <?php echo $this->session->userdata("blog_errors") ?> </div>
            </div>
            <?php endif ?>
            <div class="large-8">
                <input type="text" name="name" placeholder="Entry name" value="<?=$entry['name']?>" /> </div>
            <div class="large-12">
                <input type="text" name="short_description" placeholder="Entry short description" value="<?=$entry['short_description']?>" /> </div>
            <div class="large-12">
                <input type="text" name="youtube_embed" placeholder="Youtube Link" value="<?=$entry['youtube_embed']?>" /> </div>
            <div class="large-4">
                <?php if($entry[ 'image']) { ?> <a class="float-right" href='<?=base_url() . "images/blog/" . $entry['image']?>'>
                <img class="admin-blog-entry-image" src="<?=base_url() . "images/blog/" . $entry['image']?>" />
              </a>
                <?php } ?>
                <input type="file" name="image" /> </div>
            <div class='large-4'>
                <select name='country'>
                    <option value=''>All Countries</option>
                    <?php foreach ($countries as $c): ?>
                    <option value="<?php echo $c->country_id ?>" <?php if($entry[ 'country']==$c->country_id) echo "selected='selected'" ?>>
                        <?php echo $c->short_name ?></option>
                    <?php endforeach ?> </select>
            </div>
            <div class='large-4'>
                <select name="display">
                    <option value='0'>Don't display on Main Page</option>
                    <option value='1' <?php if($entry[ 'display']==1 ) echo "selected='selected'"; ?>>Main Page Top</option>
                    <option value='2' <?php if($entry[ 'display']==2 ) echo "selected='selected'"; ?>>Main Page Bottom Left</option>
                    <option value='3' <?php if($entry[ 'display']==3 ) echo "selected='selected'"; ?>>Main Page Bottom Right</option>
                </select>
            </div>
            <div class='large-4'>
                <input type='checkbox' name='level_1' <?php if($entry[ 'level_1']) echo "checked='checked'" ?> /> Visible Level 1 Main Page </div>
            <div class='large-4'>
                <input type='checkbox' name='level_2' <?php if($entry[ 'level_2']) echo "checked='checked'" ?> /> Visible Level 2 Main Page </div>
            <div class='large-4'>
                <input type='checkbox' name='level_3' <?php if($entry[ 'level_3']) echo "checked='checked'" ?> /> Visible Level 3 Main Page </div>
            <textarea name="body" id="entry_content">
                <?=$entry[ 'body']?>
            </textarea>
        </form>
    </div>
    <script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
    <script>
        window.onload = function() {
            CKEDITOR.replace('entry_content', {
                contentsCss: "<?php echo base_url('/css/style.css') ?>"
            });
            CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
            CKEDITOR.config.allowedContent = true;
        }
    </script>
