<script>
    jQuery(document).ready(function() {
        $('#approve').click(function() {
            var value = $('#points-value').val();
            var approved = [];
            $(".approved").each(function() {
                if($(this).is(":checked")) {
                    approved.push($(this).val());
                }
            });
            var approved_list = approved.join("_");
            window.location = "<?php echo base_url('/admin/best_performers/approve/'.$list->id) ?>/" + value + "/" + approved_list;
        });
    });
</script>
<div class="content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <?php if ($list->status==1): ?>
        <div class="alert-box success no-image">This list has been approved</div>
        <?php elseif($list->status <0): ?>
            <div class="alert-box error no-image">This list has been rejected
                <br>
                <br>"
                <?php echo $list->reason ?>" </div>
            <?php endif ?>
            <div class="header-stats">
                <?=$title?>
                    <?php if($list->status==0) { ?>
                    <button class="save-profile-black admin-form-button btn-success" id="approve">Approve</button>
                    <!-- <a href="<?=$this->config->base_url()?>admin/best_performers/reject/<?=$list->id?>"><button class="save-profile-white admin-form-button btn-danger">Reject</button></a> --><a class="btn-danger save-profile-white admin-form-button" href="#" id='reject'>Reject</a>
                    <input id="points-value" type='text' class="best-points-value" value="<?php echo $settings['best_performers_default_points'] ?>" />
                    <div class="best-points-bonus-label">BONUS POINTS:</div>
                    <form id='reject-form' class="best-form-bonus-points" method='post' action="<?php echo base_url('admin/best_performers/reject/'.$list->id) ?>">
                        <textarea placeholder="Reason" name='reason' class="best-form-bonus-points__reason"></textarea>
                        <input type='submit' value='Save' class='btn-black' /> <a href='#' id='cancel-reject' class="best-form-bonus-points__cancel">Cancel</a> </form>
                    <?php } ?> </div>
            <table border="0" class="best-form-table">
                <thead>
                    <tr>
                        <td width="25">
                            <input type="checkbox" id="handle_checkboxes" checked="checked" class="no-margin" /> </td>
                        <th width="200" align="left">Name</th>
                        <th>Sales</th>
                        <th>Revenue</th>
                        <th>Bonus Points</th>
                        <th>Total Points</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $u): ?>
                    <tr>
                        <td>
                            <input type="checkbox" class="approved no-margin" value="<?php echo $u['id'] ?>" checked="checked" /> </td>
                        <td>
                            <a href="<?php echo base_url('/admin/user/view/'.$u['id']) ?>">
                                <?php echo $u[ 'first_name']. ' '.$u[ 'last_name'] ?>
                            </a>
                        </td>
                        <td>
                            <?php echo intval($u[ 'total_sales']) ?>
                        </td>
                        <td>$
                            <?php echo number_format(floatval($u[ 'earnings']), 2) ?>
                        </td>
                        <td>
                            <?php echo intval($u[ 'bonus_points']) ?>
                        </td>
                        <td>
                            <?php echo intval($u[ 'total_points']) ?>
                        </td>
                    </tr>
                    <?php endforeach ?> </tbody>
            </table>
    </div>
</div>
