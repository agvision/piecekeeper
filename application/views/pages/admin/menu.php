      <div class="column-left">
          <ul class="vermenu">

            <li class="social-network-link <?php if($this->router->fetch_class() == 'index') echo 'current' ?>">
                <a href="<?=base_url('/admin')?>">Home</a>
            </li>

            <?php if ($admin['level'] == 3): ?>
                <li class="social-network-link <?php if($this->router->fetch_class() == 'admins') echo 'current' ?>">
                    <a href="<?=base_url('/admin/admins/')?>">Admins</a>
                </li>
            <?php endif ?>

            <li class="social-network-link <?php if($this->router->fetch_class() == 'user') echo 'current' ?>">
                <a href="<?=base_url('/admin/user/')?>">PieceKeepers</a>
            </li>

            <li class="social-network-link <?php if($this->router->fetch_class() == 'broadcast') echo 'current' ?>">
                <a href="<?=base_url('/admin/broadcast/')?>">Broadcast</a>
            </li>

            <li class="social-network-link <?php if($this->router->fetch_class() == 'stats') echo 'current' ?>">
                <a href="<?=base_url('/admin/stats')?>">Statistics</a>
            </li>

            <li class="social-network-link <?php if($this->router->fetch_class() == 'missions') echo 'current' ?>">
                <a href="<?=base_url('/admin/missions')?>">Missions</a>
            </li>

            <li class="social-network-link <?php if($this->router->fetch_class() == 'goals') echo 'current' ?>">
                <a href="<?=base_url('/admin/goals')?>">Goals</a>
            </li>

            <li class="social-network-link <?php if($this->router->fetch_class() == 'blogPosts') echo 'current' ?>">
                <a href="<?=base_url('/admin/blogPosts')?>">Blog</a>
            </li>

            <li class="social-network-link <?php if($this->router->fetch_class() == 'content') echo 'current' ?>">
                <a href="<?=base_url('/admin/content')?>">Content</a>
            </li>

            <?php if ($admin['level'] == 3): ?>
                <li class="social-network-link <?php if($this->router->fetch_class() == 'settings') echo 'current' ?>">
                    <a href="<?=base_url('/admin/settings/')?>">Settings</a>
                </li>
            <?php endif ?>

            <li class="social-network-link">
                <a href="<?=base_url('/auth/logout')?>">Logout</a>
            </li>

          </ul>

</div>
