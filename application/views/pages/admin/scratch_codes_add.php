
  <div class="content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
      <div class="header-stats">
          <?php echo $title ?>
      </div>

      <br>

      <?php if (count($errors)): ?>
          <div class='form-alert-wrapper'>
            <?php foreach ($errors as $error): ?>
                <div class='alert-box error form-alert'><?php echo $error ?></div>
            <?php endforeach ?>
          </div>
      <?php endif ?>

      <form action="<?=base_url('admin/scratch_codes/add/'.$user['id'])?>" method="post" accept-charset="utf-8">
        <div class="row collapse">
          <div class="large-3">
            <input type='text' name='batch' placeholder='Batch'/>
            <input type="submit" class="btn-black" value="Add" style='width: 110px;' />
            <input type='hidden' name='trigger' value='add' />
          </div>
          <div class="large-3 column"></div>
        </div>
      </form>

     

    </div>
  </div>
