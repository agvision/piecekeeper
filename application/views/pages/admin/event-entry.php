    </div>
    <div class="content-bottom">
      <?php $this->load->view('pages/admin/menu') ?>
      <div class="column-right">
        <?php if(isset($user)) { ?>
          <?php if($entry['aproved']==0) { ?>
            <div class="alert-box warning"><b>This event is added by <?=$user['first_name'] . " " . $user['last_name']?></b> and it is not active.</div>
          <?php } else { ?>
            <div class="alert-box success"><b>This event is added by <?=$user['first_name'] . " " . $user['last_name']?></b> and it is currently active.</div>
          <?php } ?>
            <br>
             <a href="<?=$this->config->base_url()?>admin/event/change_status/<?=$entry['id']?>"><button class="btn-black">
                      <?=($entry['aproved']==1 ? "Reject" : "Approve")?>
                  </button></a>
        <?php } ?>
        <br><br>
        <form action="<?php echo base_url('admin/event/save') ?>" method='post' enctype='multipart/form-data'>
          <input type="hidden" name="id" value="<?=$entry['id']?>"  />
          <div class="header-stats first clearfix">
             Add / Edit Event 
            <?php
              if ($entry['id'] != '')
                echo '<a class="btn-danger save-profile-white admin-form-button delete-item" 
                    href="' . base_url('admin/event/delete/' . $entry['id']) . '">Delete</a>';
            ?>
            <?php if ($entry['id'] != ''): ?>
                <input type="submit" class="btn-success save-profile-black admin-form-button" value="Update">
            <?php else: ?>
                <input type="submit" class="btn-success save-profile-black admin-form-button" value="Add">
            <?php endif ?>
          </div>
          <br />
          <div class="large-4">
            <input type="text" name="name" placeholder="Entry name" value="<?=$entry['name']?>" />
          </div>
          <div class="large-4">
            <?php if ($entry['id'] != ''): ?>
                <input type="text" name="date" class="date" placeholder="Choose a date" value="<?=$this->users->formatDate($admin['id'], strtotime($entry['date'])) ?>" />
            <?php else: ?>
                <input type="text" name="date" class="date" placeholder="Choose a date" value="" />
            <?php endif ?>
            
          </div>
           <div class="large-4">
            <input type="text" name="short_description" placeholder="Entry short description" value="<?=$entry['short_description']?>" />
          </div>
          <div class="large-4">
            <?php if($entry['image']) { ?>
              <a style='float: right;' href='<?=base_url() . "images/event/" . $entry['image']?>'>
                <img style='width: 250px; margin-bottom: 10px; margin-top: 15px;' src="<?=base_url() . "images/event/" . $entry['image']?>" />
              </a>
            <?php } ?>
            <input type="file" name="image" />
          </div>
          <textarea name="body" id="entry_content" >
            <?=$entry['body']?>
          </textarea>
        </form> 
        </div>
      </div>
<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script>
  window.onload = function() {
    CKEDITOR.replace('entry_content');
  }
  jQuery('.date').fdatepicker({format: '<?php echo $this->users->getDatepickerFormat($admin["id"]) ?>', weekStart: 1});
</script>
