    </div>
    <div class="content-bottom">
      <?php $this->load->view('pages/admin/menu') ?>
        <div class="column-right">
          <div class="header-stats">
              Coupon Orders
          </div>

          <div class='profile-picture-right'>
              <div class='form-label'>
                  <p><b>CODE:</b> <?php echo $coupon->code ?></p>
                    
                  <?php if ($coupon->amount && $coupon->amount != '0.00'): ?>
                      <p><b>AMOUNT:</b> <?php echo $coupon->amount.' '.$coupon->currency ?></p>
                  <?php else: ?>
                      <p><b>PERCENT:</b> <?php echo $coupon->percent ?></p>
                  <?php endif ?>

                  <p><b>DETAILS:</b> <?php echo $coupon->description ?></p>
                  <?php if ($coupon->type == 2): ?>
                      <p><b>LINK:</b> <?php echo $coupon->link ?></p>
                  <?php endif ?>
                  <p><b>STATUS:</b> 
                      <?php if ($coupon->approved == 1): ?>
                          <?php if (time() > strtotime($coupon->to)): ?>
                              Expired
                          <?php else: ?>
                              Active
                          <?php endif ?>
                      <?php else: ?>
                          Pending
                      <?php endif ?>
                  </p>
                  <p><b>EXPIRES:</b> <?php echo date('d M Y', strtotime($coupon->to)) ?></p>
              </div>
          </div>

          <table class="admin-table">
            <thead>
              <th>Order ID</th>
              <th>Total</th>
              <th>Ambassador</th>
              <th>Created</th>
            </thead>
            <tbody>
                <?php foreach ($orders as $order): ?>
                    <tr>
                      <td><?php echo $order->order_id ?></td>
                      <td><?php echo $order->total ?></td>
                      <td><?php echo $order->first_name." ".$order->last_name ?></td>
                      <td><?php echo $order->created ?></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
