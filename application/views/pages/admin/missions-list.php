<div class="missions-list content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats clearfix">
            <?=$title?> <a class="btn-success save-profile-black admin-form-button" href="<?php echo base_url('admin/missions/add') ?>">Add</a> </div>
        <?php if (count($missions)): ?>
        <table id="missions-list full-width">
            <thead>
                <tr class="text-align-left">
                    <th>Name</th>
                    <th>Country</th>
                    <th>City</th>
                    <th>From</th>
                    <th>To</th>
                    <td></td>
                </tr>
            </thead>
            <?php foreach ($missions as $m) { ?>
            <tr>
                <td>
                    <?php echo $m->name ?></td>
                <td>
                    <?php if (intval($m->country)): ?>
                    <?php echo $m->short_name ?>
                    <?php else: ?>
                    <?php echo "All" ?>
                    <?php endif ?> </td>
                <td>
                    <?php if (strlen($m->city)): ?>
                    <?php echo $m->city ?>
                    <?php else: ?>
                    <?php echo "All" ?>
                    <?php endif ?> </td>
                <td>
                    <?php echo $this->users->formatDate($admin['id'], strtotime($m->from)) ?></td>
                <td>
                    <?php if ($m->countdown): ?>
                    <?php echo $this->users->formatDate($admin['id'], strtotime($m->to)) ?>
                    <?php else: ?> -----
                    <?php endif ?> </td>
                <td>
                    <?php if ($admin[ 'level']==2 && !$m->country): ?> <a href="<?php echo base_url('admin/missions/view/').'/'.$m->id ?>">View</a>
                    <?php else: ?> <a href="<?php echo base_url('admin/missions/view/').'/'.$m->id ?>">View</a> | <a href="<?php echo base_url('admin/missions/edit/').'/'.$m->id ?>">Edit</a> | <a href="<?php echo base_url('admin/missions/delete/').'/'.$m->id ?>" class='delete-item'>Delete</a>
                    <?php endif ?> </td>
            </tr>
            <?php } ?> </table>
        <?php else: ?>
        <div class='form-label no-mt'>There are no Missions created.</div>
        <?php endif ?> </div>
</div>
