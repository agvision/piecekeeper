<script type="text/javascript">
	$(document).ready(function(){
		$('#sales-page-items').change(function(){
			location.href = "<?php echo base_url('/admin/sales/history/') ?>"+"/"+$(this).val();
		});
	});
</script>

<div class="content-bottom">        
  <?php $this->load->view('pages/admin/menu') ?>

    <div class="column-right">

        <div class="header-stats">
            Sales
        </div>

        <div id='sales-items-per-page'>
        	<p style='display: inline; margin-right: 5px;'>Items per page:</p>
        	<select name='sales-page-tems' id='sales-page-items' style='width: 55px;'>
        		<option value='1'>All</option>
        		<option <?php if($items_per_page == 100) echo "selected='selected'" ?> value='100'>100</option>
        		<option <?php if($items_per_page == 200) echo "selected='selected'" ?> value='200'>200</option>
        		<option <?php if($items_per_page == 500) echo "selected='selected'" ?> value='500'>500</option>
        	</select>
        </div>

        <div class='pagination-wrapper' style='margin-top: 20px;'>
          <div class='pagination-prev-page'>
            <?php if ($current_page > 1): ?>
              <a href="<?php echo base_url('/admin/sales/history/'.$items_per_page.'/'.($current_page-1)) ?>"><< Prev</a>
            <?php endif ?>
          </div>
          <div class='pagination-next-page'>
            <?php if ($current_page < $total_pages): ?>
              <a href="<?php echo base_url('/admin/sales/history/'.$items_per_page.'/'.($current_page+1)) ?>">Next >></a>
            <?php endif ?>
          </div>
        </div>

        <table>
            <thead>
                <th>ID</th>
                <th>PieceKeeper</th>
                <th>Total</th>
                <th>Commission</th>
                <th>Sales Points</th>
                <th>Created</th>
            </thead>
            <tbody>
                <?php foreach ($sales as $s): ?>
                <tr>
                    <td><?php echo $s->order_id ?></td>
                    <td>
                        <a href="<?php echo base_url('/admin/user/view/'.$s->id_user) ?>"><?php echo $s->first_name.' '.$s->last_name ?></a>
                    </td>
                    <td>$<?php echo $s->total ?></td>
                    <td>$<?php echo $s->commission ?></td>
                    <td><?php echo $s->points ?></td>
                    <td><?php echo $this->users->formatDate($admin['id'], strtotime($s->created)) ?></td>
                </tr>
                <?php endforeach ?>
            </tbody>
        </table>

        <div class='pagination-wrapper' style='margin-top: 20px;'>
          <div class='pagination-prev-page'>
            <?php if ($current_page > 1): ?>
              <a href="<?php echo base_url('/admin/sales/history/'.$items_per_page.'/'.($current_page-1)) ?>"><< Prev</a>
            <?php endif ?>
          </div>
          <div class='pagination-next-page'>
            <?php if ($current_page < $total_pages): ?>
              <a href="<?php echo base_url('/admin/sales/history/'.$items_per_page.'/'.($current_page+1)) ?>">Next >></a>
            <?php endif ?>
          </div>
        </div>



        <br><br><br>            

    </div>

</div>