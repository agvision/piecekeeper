<script type="text/javascript">
    jQuery(document).ready(function() {
        $('#add-country').click(function() {
            var countries = ""; <?php foreach($countries as $c): ?> countries += "<option value='" + "<?php echo $c->country_id ?>" + "'>" + "<?php echo $c->short_name ?>" + "</option>"; <?php endforeach ?>
                var select = "<div class='admin-edit-input-wrapper'><select name='countries[]'>" + "<option value=''>Select Country</option>" + countries + "</select></div>";
            $('#countries-wrapper').append(select);
        });
        var all_selected = <?php
        if($all_countries_already_selected) echo "true";
        else echo "false"; ?> ;
    });
</script>
<div class="content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats"> Edit Admin </div>
        <?php if (validation_errors()): ?>
        <div class='form-alert-wrapper'>
            <?php echo validation_errors( "<div class='alert-box error form-alert'>", "</div>"); ?> </div>
        <?php endif ?>
        <div class="form-label">Admin Details:</div>
        <form method='post' action="<?php echo base_url('admin/admins/edit/'.$admin_user->id) ?>">
            <div class='admin-edit-input-wrapper'>
                <input type='text' name='first_name' placeholder='first name' value="<?php echo $admin_user->first_name ?>" /> </div>
            <div class='admin-edit-input-wrapper'>
                <input type='text' name='last_name' placeholder='last name' value="<?php echo $admin_user->last_name ?>" /> </div>
            <div class='admin-edit-input-wrapper'>
                <input type='email' name='email' placeholder='email' value="<?php echo $admin_user->email ?>" /> </div>
            <?php if ($admin[ 'id']==4 ): ?>
            <input type='checkbox' name='super_admin' <?php if($admin_user->level == 3) echo "checked='checked'" ?>/> Super Admin
            <input type='hidden' name='edit_super_admin' value='on'>
            <?php endif ?>
            <div class="form-label">Assign Countries:</div>
            <input type="checkbox" class="all-countries-ckb" name="all-countries" id="all-countries" value="all" <?php if($all_countries_already_selected) echo "checked"; ?> />All countries
            <div id="label-countries"> <a id='clear-countries' href="<?php echo base_url('admin/admins/remove_countries/'.$admin_user->id) ?>" class='btn-white admin-add-btn-clear-countries'>Clear countries</a>
                <table>
                    <?php foreach ($admin_countries as $c): ?>
                    <tr>
                        <td>
                            <?php echo $c->short_name ?></td>
                        <td><a href="<?php echo base_url('admin/admins/remove_country/'.$admin_user->id.'/'.$c->country_id) ?>">Remove</a>
                        </td>
                    </tr>
                    <?php endforeach ?> </table>
                <div id='countries-wrapper'> </div>
                <div id='add-country' class='btn-white admin-edit-btn-add-country'>Add Country</div>
            </div>
            <input type='hidden' name='trigger' value='edit' />
            <div class="dotted-line admin-edit-dotted-line"></div>
            <button class="btn-success" type="submit">Update</button>
        </form>
    </div>
</div>
