<script type="text/javascript">
    function changeCitiesByCountry(id_country) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('auth/get_cities_by_country') ?>",
            data: 'id_country=' + id_country,
            dataType: 'json',
            success: function(data) {
                var input = "<select name='city'>\n";
                input += "<option value=''>All Cities</option>\n";
                $.each(data, function(index, value) {
                    input += "<option value='" + value + "'>" + value + "</option>\n";
                })
                input += "</select>";
                $('.city-loader').html(input);
            }
        });
    }
</script>
<?php if (isset($commissions_stats[ 'keys']) && count($commissions_stats[ 'keys'])): ?>
<?php if ($sort=='gender' ): ?>
<?php $this->load->view('pages/admin/stats/pie-gender-commissions') ?>
<?php else: ?>
<?php $this->load->view('pages/admin/stats/column-commissions') ?>
<?php endif ?>
<?php endif ?>
<?php if (isset($chart_sales_stats[ 'keys']) && count($chart_sales_stats[ 'keys'])): ?>
<?php if ($sort=='gender' ): ?>
<?php $this->load->view('pages/admin/stats/pie-gender-sales') ?>
<?php else: ?>
<?php $this->load->view('pages/admin/stats/column-sales') ?>
<?php endif ?>
<?php endif ?>
<?php if (isset($roi_stats[ 'keys']) && count($roi_stats[ 'keys'])): ?>
<?php $this->load->view('pages/admin/stats/columns-roi') ?>
<?php endif ?>
<?php if (isset($spc_stats[ 'keys']) && count($spc_stats[ 'keys'])): ?>
<?php $this->load->view('pages/admin/stats/column-spc') ?>
<?php endif ?>
<?php if (isset($chart_sales_points_stats[ 'keys']) && count($chart_sales_points_stats[ 'keys'])): ?>
<?php if ($sort=='gender' ): ?>
<?php $this->load->view('pages/admin/stats/pie-gender-sales-points') ?>
<?php else: ?>
<?php $this->load->view('pages/admin/stats/column-sales-points') ?>
<?php endif ?>
<?php endif ?>
<?php if (isset($chart_bonus_points_stats[ 'keys']) && count($chart_bonus_points_stats[ 'keys'])): ?>
<?php if ($sort=='gender' ): ?>
<?php $this->load->view('pages/admin/stats/pie-gender-bonus-points') ?>
<?php else: ?>
<?php $this->load->view('pages/admin/stats/column-bonus-points') ?>
<?php endif ?>
<?php endif ?>
<?php if (isset($chart_pk_stats[ 'keys']) && count($chart_pk_stats[ 'keys'])): ?>
<?php $this->load->view('pages/admin/stats/column-pk') ?>
<?php endif ?>
<?php $this->load->view('pages/admin/stats/scratch-coupons') ?>
<div class="stats-index content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats">
            <?=$title?>
        </div>
        <div id="tabs" class="stats-index-tabs">
            <ul>
                <li><a href="#tabs-1">Sales</a>
                </li>
                <li><a href="#tabs-2">Commissions</a>
                </li>
                <li><a href="#tabs-3">ROI</a>
                </li>
                <li><a href="#tabs-4">SPC</a>
                </li>
                <li><a href="#tabs-5">Sales Points</a>
                </li>
                <li><a href="#tabs-6">Bonus Points</a>
                </li>
                <li><a href="#tabs-7">PieceKeepers</a>
                </li>
                <li><a href="#tabs-8">Scratch Coupons</a>
                </li>
            </ul>
            <div id="tabs-1">
                <form method='get' action='#tabs-1' class='stats-sort'>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='sort'>
                            <option selected='selected' value='total'>Filter By</option>
                            <option value='total' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='total' ) ? "selected='selected'" : "" ?> >Total Sales</option>
                            <option value='coupon' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='coupon' ) ? "selected='selected'" : "" ?>>Coupon</option>
                            <option value='gender' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='gender' ) ? "selected='selected'" : "" ?>>Gender</option>
                            <option value='country' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='country' ) ? "selected='selected'" : "" ?>>Country</option>
                            <option value='city' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='city' ) ? "selected='selected'" : "" ?>>City</option>
                            <option value='school' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='school' ) ? "selected='selected'" : "" ?>>School</option>
                            <option value='tags' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='tags' ) ? "selected='selected'" : "" ?>>Tags</option>
                        </select>
                    </div>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='country' class='country-loader'>
                            <option value=''>All Countries</option>
                            <?php foreach ($filter_countries as $c): ?>
                            <option value="<?php echo $c->country_id ?>" <?php echo (isset($_GET[ 'country']) && $_GET[ 'country']==$c->country_id) ? "selected='selected'" : "" ?> >
                                <?php echo $c->short_name ?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <div class='large-2 left city-loader stats-index-select-wrapper'>
                        <select name='city'>
                            <option value=''>All Cities</option>
                            <?php foreach ($filter_cities as $c): ?>
                            <option value="<?php echo $c->name ?>" <?php echo (isset($_GET[ 'city']) && $_GET[ 'city']==$c->name) ? "selected='selected'" : "" ?> >
                                <?php echo $c->name ?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <input type='text' class='date stats-index-input-date' name='begin' placeholder='Begin Date' value="<?php echo $this->input->get('begin') ?>" />
                    <input type='text' class='date stats-index-input-date' name='end' placeholder='End Date' value="<?php echo $this->input->get('end') ?>" />
                    <button type='submit' class='small-btn'>GO</button>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='frequency'>
                            <option value='month' <?php if($frequency=="month" ) echo "selected='selected'"; ?> >Month</option>
                            <option value='week' <?php if($frequency=="week" ) echo "selected='selected'"; ?> >Week</option>
                            <option value='day' <?php if($frequency=="day" ) echo "selected='selected'"; ?>>Day</option>
                        </select>
                    </div>
                </form>
                <div id="sales-stats" class="stats-index-stats-wrapper">
                    <?php if (!isset($total_sales_stats[ 'keys']) || !count($total_sales_stats[ 'keys'])): ?>
                    <p class="stats-index-no-results">There are no results to display.</p>
                    <?php endif ?> </div>
                <br>
                <br>
                <table>
                    <thead>
                        <tr>
                            <th style="width:300px;">Name</th>
                            <?php if (isset($total_sales_stats[ 'table_values'])): ?>
                            <td style="width:100px;"></td>
                            <?php endif ?>
                            <th style="width:300px;">Value ($)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($total_sales_stats[ 'keys'])): ?>
                        <?php foreach ($total_sales_stats[ 'keys'] as $key=> $value): ?>
                        <tr>
                            <?php if (isset($total_sales_stats[ 'table_values'])): ?>
                            <?php $interval=str_replace( "'", "", $total_sales_stats[ 'table_values'][$key]); ?>
                            <td>
                                <?php echo $interval; ?>
                            </td>
                            <td><a href="<?php echo base_url('admin/sales/view/'.str_replace(" ", "_ ", $interval)) ?>">Show Sales</a>
                            </td>
                            <?php else: ?>
                            <td>
                                <?php echo str_replace( "'", "", $total_sales_stats[ 'keys'][$key]) ?>
                            </td>
                            <?php endif ?>
                            <td>
                                <?php echo number_format($total_sales_stats[ 'values'][$key]) ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                        <?php endif ?>
                        <tr>
                            <td><b>Total</b>
                            </td>
                            <td><b><?php echo number_format($total_sales_stats['total']) ?></b>
                            </td>
                            <?php if (isset($total_sales_stats[ 'table_values'])): ?>
                            <td></td>
                            <?php endif ?> </tr>
                    </tbody>
                </table>
            </div>
            <div id="tabs-2">
                <form method='get' action='#tabs-2' class='stats-sort'>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='sort'>
                            <option selected='selected' value='total'>Filter By</option>
                            <option value='total' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='total' ) ? "selected='selected'" : "" ?> >Total Sales</option>
                            <option value='coupon' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='coupon' ) ? "selected='selected'" : "" ?>>Coupon</option>
                            <option value='gender' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='gender' ) ? "selected='selected'" : "" ?>>Gender</option>
                            <option value='country' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='country' ) ? "selected='selected'" : "" ?>>Country</option>
                            <option value='city' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='city' ) ? "selected='selected'" : "" ?>>City</option>
                            <option value='school' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='school' ) ? "selected='selected'" : "" ?>>School</option>
                            <option value='tags' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='tags' ) ? "selected='selected'" : "" ?>>Tags</option>
                        </select>
                    </div>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='country' class='country-loader'>
                            <option value=''>All Countries</option>
                            <?php foreach ($filter_countries as $c): ?>
                            <option value="<?php echo $c->country_id ?>" <?php echo (isset($_GET[ 'country']) && $_GET[ 'country']==$c->country_id) ? "selected='selected'" : "" ?> >
                                <?php echo $c->short_name ?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <div class='large-2 left city-loader stats-index-select-wrapper'>
                        <select name='city'>
                            <option value=''>All Cities</option>
                            <?php foreach ($filter_cities as $c): ?>
                            <option value="<?php echo $c->name ?>" <?php echo (isset($_GET[ 'city']) && $_GET[ 'city']==$c->name) ? "selected='selected'" : "" ?> >
                                <?php echo $c->name ?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <input type='text' class='date stats-index-input-date' name='begin' placeholder='Begin Date' value="<?php echo $this->input->get('begin') ?>" />
                    <input type='text' class='date stats-index-input-date' name='end' placeholder='End Date' value="<?php echo $this->input->get('end') ?>" />
                    <button type='submit' class='small-btn'>GO</button>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='frequency'>
                            <option value='month' <?php if($frequency=="month" ) echo "selected='selected'"; ?> >Month</option>
                            <option value='week' <?php if($frequency=="week" ) echo "selected='selected'"; ?> >Week</option>
                            <option value='day' <?php if($frequency=="day" ) echo "selected='selected'"; ?>>Day</option>
                        </select>
                    </div>
                </form>
                <div id="commissions-stats" class="stats-index-stats-wrapper">
                    <?php if (!isset($commissions_stats[ 'keys'])): ?>
                    <p class="stats-index-no-results">There are no results to display.</p>
                    <?php endif ?> </div>
                <br>
                <br>
                <table>
                    <thead>
                        <tr>
                            <th class="stats-index-column">Name</th>
                            <th class="stats-index-column">Value ($)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($commissions_stats[ 'keys'])): ?>
                        <?php foreach ($commissions_stats[ 'keys'] as $key=> $value): ?>
                        <tr>
                            <td>
                                <?php echo str_replace( "'", "", $commissions_stats[ 'keys'][$key]) ?>
                            </td>
                            <td>
                                <?php echo number_format($commissions_stats[ 'values'][$key]) ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                        <?php endif ?>
                        <tr>
                            <td><b>Total</b>
                            </td>
                            <td><b><?php echo number_format($commissions_stats['total']) ?></b>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="tabs-3">
                <form method='get' action='#tabs-3' class='stats-sort'>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='sort'>
                            <option selected='selected' value='total'>Filter By</option>
                            <option value='total' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='total' ) ? "selected='selected'" : "" ?> >Total Sales</option>
                            <option value='coupon' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='coupon' ) ? "selected='selected'" : "" ?>>Coupon</option>
                            <option value='gender' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='gender' ) ? "selected='selected'" : "" ?>>Gender</option>
                            <option value='country' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='country' ) ? "selected='selected'" : "" ?>>Country</option>
                            <option value='city' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='city' ) ? "selected='selected'" : "" ?>>City</option>
                            <option value='school' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='school' ) ? "selected='selected'" : "" ?>>School</option>
                            <option value='tags' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='tags' ) ? "selected='selected'" : "" ?>>Tags</option>
                        </select>
                    </div>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='country' class='country-loader'>
                            <option value=''>All Countries</option>
                            <?php foreach ($filter_countries as $c): ?>
                            <option value="<?php echo $c->country_id ?>" <?php echo (isset($_GET[ 'country']) && $_GET[ 'country']==$c->country_id) ? "selected='selected'" : "" ?> >
                                <?php echo $c->short_name ?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <div class='large-2 left city-loader stats-index-select-wrapper'>
                        <select name='city'>
                            <option value=''>All Cities</option>
                            <?php foreach ($filter_cities as $c): ?>
                            <option value="<?php echo $c->name ?>" <?php echo (isset($_GET[ 'city']) && $_GET[ 'city']==$c->name) ? "selected='selected'" : "" ?> >
                                <?php echo $c->name ?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <input type='text' class='date stats-index-input-date' name='begin' placeholder='Begin Date' value="<?php echo $this->input->get('begin') ?>" />
                    <input type='text' class='date stats-index-input-date' name='end' placeholder='End Date' value="<?php echo $this->input->get('end') ?>" />
                    <button type='submit' class='small-btn'>GO</button>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='frequency'>
                            <option value='month' <?php if($frequency=="month" ) echo "selected='selected'"; ?> >Month</option>
                            <option value='week' <?php if($frequency=="week" ) echo "selected='selected'"; ?> >Week</option>
                            <option value='day' <?php if($frequency=="day" ) echo "selected='selected'"; ?>>Day</option>
                        </select>
                    </div>
                </form>
                <div id="roi-stats" class="stats-index-roi">
                    <?php if (!isset($roi_stats[ 'keys'])): ?>
                    <p class="stats-index-no-results">There are no results to display.</p>
                    <?php endif ?> </div>
                <br>
                <br>
                <table>
                    <thead>
                        <tr>
                            <th class="stats-index-column">Name</th>
                            <th class="stats-index-column">Sales ($)</th>
                            <th class="stats-index-column">Commission ($)</th>
                            <th class="stats-index-column">ROI</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($roi_stats[ 'keys'])): ?>
                        <?php foreach ($roi_stats[ 'keys'] as $key=> $value): ?>
                        <tr>
                            <td>
                                <?php echo str_replace( "'", "", $roi_stats[ 'keys'][$key]) ?>
                            </td>
                            <td>
                                <?php if (isset($roi_stats[ 'sales_values'][$key])): ?>
                                <?php echo number_format($roi_stats[ 'sales_values'][$key]) ?>
                                <?php endif ?> </td>
                            <td>
                                <?php if (isset($roi_stats[ 'commission_values'][$key])): ?>
                                <?php echo number_format($roi_stats[ 'commission_values'][$key]) ?>
                                <?php endif ?> </td>
                            <td>
                                <?php if (isset($roi_stats[ 'sales_values'][$key]) && isset($roi_stats[ 'commission_values'][$key])): ?>
                                <?php if (intval($roi_stats[ 'commission_values'][$key]) !=0 ): ?>
                                <?php echo number_format($roi_stats[ 'sales_values'][$key] / $roi_stats[ 'commission_values'][$key], 2) ?>
                                <?php else: ?> 0.00
                                <?php endif ?>
                                <?php endif ?> </td>
                        </tr>
                        <?php endforeach ?>
                        <?php endif ?>
                        <tr>
                            <td><b>Total</b>
                            </td>
                            <td><b><?php echo number_format($roi_stats['sales_total']) ?></b>
                            </td>
                            <td><b><?php echo number_format($roi_stats['commission_total']) ?></b>
                            </td>
                            <?php if (intval($roi_stats[ 'commission_total']) !=0 ): ?>
                            <td><b><?php echo number_format($roi_stats['sales_total'] / $roi_stats['commission_total'], 2) ?></b>
                            </td>
                            <?php else: ?>
                            <td><b>0</b>
                            </td>
                            <?php endif ?> </tr>
                    </tbody>
                </table>
            </div>
            <div id="tabs-4">
                <form method='get' action='#tabs-4' class='stats-sort'>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='sort'>
                            <option selected='selected' value='total'>Filter By</option>
                            <option value='total' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='total' ) ? "selected='selected'" : "" ?> >Total Sales</option>
                            <option value='gender' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='gender' ) ? "selected='selected'" : "" ?>>Gender</option>
                            <option value='country' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='country' ) ? "selected='selected'" : "" ?>>Country</option>
                            <option value='city' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='city' ) ? "selected='selected'" : "" ?>>City</option>
                            <option value='school' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='school' ) ? "selected='selected'" : "" ?>>School</option>
                            <option value='tags' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='tags' ) ? "selected='selected'" : "" ?>>Tags</option>
                        </select>
                    </div>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='country' class='country-loader'>
                            <option value=''>All Countries</option>
                            <?php foreach ($filter_countries as $c): ?>
                            <option value="<?php echo $c->country_id ?>" <?php echo (isset($_GET[ 'country']) && $_GET[ 'country']==$c->country_id) ? "selected='selected'" : "" ?> >
                                <?php echo $c->short_name ?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <div class='large-2 left city-loader stats-index-select-wrapper'>
                        <select name='city'>
                            <option value=''>All Cities</option>
                            <?php foreach ($filter_cities as $c): ?>
                            <option value="<?php echo $c->name ?>" <?php echo (isset($_GET[ 'city']) && $_GET[ 'city']==$c->name) ? "selected='selected'" : "" ?> >
                                <?php echo $c->name ?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <input type='text' class='date stats-index-input-date' name='begin' placeholder='Begin Date' value="<?php echo $this->input->get('begin') ?>" />
                    <input type='text' class='date stats-index-input-date' name='end' placeholder='End Date' value="<?php echo $this->input->get('end') ?>" />
                    <button type='submit' class='small-btn'>GO</button>
                </form>
                <div id="spc-stats" class="stats-index-stats-wrapper">
                    <?php if (!isset($spc_stats[ 'keys']) || !count($spc_stats[ 'keys'])): ?>
                    <p class="stats-index-no-results">There are no results to display.</p>
                    <?php endif ?> </div>
                <br>
                <br>
                <table>
                    <thead>
                        <tr>
                            <th class="stats-index-column">Name</th>
                            <th class="stats-index-column">SPC</th>
                            <th class="stats-index-column">Sales</th>
                            <th class="stats-index-column">Coupons</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($spc_stats[ 'keys'])): ?>
                        <?php foreach ($spc_stats[ 'keys'] as $key=> $value): ?>
                        <tr>
                            <td>
                                <?php echo str_replace( "'", "", $spc_stats[ 'keys'][$key]) ?>
                            </td>
                            <td>
                                <?php echo number_format($spc_stats[ 'values'][$key]) ?> %</td>
                            <td>
                                <?php echo number_format($spc_stats[ 'sales'][$key]) ?>
                            </td>
                            <td>
                                <?php echo number_format($spc_stats[ 'coupons'][$key]) ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                        <?php endif ?> </tbody>
                </table>
            </div>
            <div id="tabs-5">
                <form method='get' action='#tabs-5' class='stats-sort'>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='sort'>
                            <option selected='selected' value='total'>Filter By</option>
                            <option value='total' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='total' ) ? "selected='selected'" : "" ?> >Total Points</option>
                            <option value='coupon' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='coupon' ) ? "selected='selected'" : "" ?>>Coupon</option>
                            <option value='gender' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='gender' ) ? "selected='selected'" : "" ?>>Gender</option>
                            <option value='country' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='country' ) ? "selected='selected'" : "" ?>>Country</option>
                            <option value='city' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='city' ) ? "selected='selected'" : "" ?>>City</option>
                            <option value='school' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='school' ) ? "selected='selected'" : "" ?>>School</option>
                            <option value='tags' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='tags' ) ? "selected='selected'" : "" ?>>Tags</option>
                        </select>
                    </div>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='country' class='country-loader'>
                            <option value=''>All Countries</option>
                            <?php foreach ($filter_countries as $c): ?>
                            <option value="<?php echo $c->country_id ?>" <?php echo (isset($_GET[ 'country']) && $_GET[ 'country']==$c->country_id) ? "selected='selected'" : "" ?> >
                                <?php echo $c->short_name ?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <div class='large-2 left city-loader stats-index-select-wrapper'>
                        <select name='city'>
                            <option value=''>All Cities</option>
                            <?php foreach ($filter_cities as $c): ?>
                            <option value="<?php echo $c->name ?>" <?php echo (isset($_GET[ 'city']) && $_GET[ 'city']==$c->name) ? "selected='selected'" : "" ?> >
                                <?php echo $c->name ?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <input type='text' class='date stats-index-input-date' name='begin' placeholder='Begin Date' value="<?php echo $this->input->get('begin') ?>" />
                    <input type='text' class='date stats-index-input-date' name='end' placeholder='End Date' value="<?php echo $this->input->get('end') ?>" />
                    <button type='submit' class='small-btn'>GO</button>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='frequency'>
                            <option value='month' <?php if($frequency=="month" ) echo "selected='selected'"; ?> >Month</option>
                            <option value='week' <?php if($frequency=="week" ) echo "selected='selected'"; ?> >Week</option>
                            <option value='day' <?php if($frequency=="day" ) echo "selected='selected'"; ?>>Day</option>
                        </select>
                    </div>
                </form>
                <div id="sales-points-stats" class="stats-index-stats-wrapper">
                    <?php if (!isset($total_sales_points_stats[ 'keys']) || !count($total_sales_points_stats[ 'keys'])): ?>
                    <p class="stats-index-no-results">There are no results to display.</p>
                    <?php endif ?> </div>
                <br>
                <br>
                <table>
                    <thead>
                        <tr>
                            <th class="stats-index-column">Name</th>
                            <th class="stats-index-column">Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($total_sales_points_stats[ 'keys'])): ?>
                        <?php foreach ($total_sales_points_stats[ 'keys'] as $key=> $value): ?>
                        <tr>
                            <td>
                                <?php echo str_replace( "'", "", $total_sales_points_stats[ 'keys'][$key]) ?>
                            </td>
                            <td>
                                <?php echo number_format($total_sales_points_stats[ 'values'][$key]) ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                        <?php endif ?>
                        <tr>
                            <td><b>Total</b>
                            </td>
                            <td><b><?php echo number_format($total_sales_points_stats['total']) ?></b>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="tabs-6">
                <form method='get' action='#tabs-6' class='stats-sort'>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='sort'>
                            <option selected='selected' value='total'>Filter By</option>
                            <option value='total' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='total' ) ? "selected='selected'" : "" ?> >Total Points</option>
                            <option value='gender' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='gender' ) ? "selected='selected'" : "" ?>>Gender</option>
                            <option value='country' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='country' ) ? "selected='selected'" : "" ?>>Country</option>
                            <option value='city' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='city' ) ? "selected='selected'" : "" ?>>City</option>
                            <option value='school' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='school' ) ? "selected='selected'" : "" ?>>School</option>
                            <option value='tags' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='tags' ) ? "selected='selected'" : "" ?>>Tags</option>
                        </select>
                    </div>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='country' class='country-loader'>
                            <option value=''>All Countries</option>
                            <?php foreach ($filter_countries as $c): ?>
                            <option value="<?php echo $c->country_id ?>" <?php echo (isset($_GET[ 'country']) && $_GET[ 'country']==$c->country_id) ? "selected='selected'" : "" ?> >
                                <?php echo $c->short_name ?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <div class='large-2 left city-loader stats-index-select-wrapper'>
                        <select name='city'>
                            <option value=''>All Cities</option>
                            <?php foreach ($filter_cities as $c): ?>
                            <option value="<?php echo $c->name ?>" <?php echo (isset($_GET[ 'city']) && $_GET[ 'city']==$c->name) ? "selected='selected'" : "" ?> >
                                <?php echo $c->name ?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <input type='text' class='date stats-index-input-date' name='begin' placeholder='Begin Date' value="<?php echo $this->input->get('begin') ?>" />
                    <input type='text' class='date stats-index-input-date' name='end' placeholder='End Date' value="<?php echo $this->input->get('end') ?>" />
                    <button type='submit' class='small-btn'>GO</button>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='frequency'>
                            <option value='month' <?php if($frequency=="month" ) echo "selected='selected'"; ?> >Month</option>
                            <option value='week' <?php if($frequency=="week" ) echo "selected='selected'"; ?> >Week</option>
                            <option value='day' <?php if($frequency=="day" ) echo "selected='selected'"; ?>>Day</option>
                        </select>
                    </div>
                </form>
                <div id="bonus-points-stats" class="stats-index-stats-wrapper">
                    <?php if (!isset($total_bonus_points_stats[ 'keys']) || !count($total_bonus_points_stats[ 'keys'])): ?>
                    <p class="stats-index-no-results">There are no results to display.</p>
                    <?php endif ?> </div>
                <br>
                <br>
                <table>
                    <thead>
                        <tr>
                            <th class="stats-index-column">Name</th>
                            <th class="stats-index-column">Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($total_bonus_points_stats[ 'keys'])): ?>
                        <?php foreach ($total_bonus_points_stats[ 'keys'] as $key=> $value): ?>
                        <tr>
                            <td>
                                <?php echo str_replace( "'", "", $total_bonus_points_stats[ 'keys'][$key]) ?>
                            </td>
                            <td>
                                <?php echo number_format($total_bonus_points_stats[ 'values'][$key]) ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                        <?php endif ?>
                        <tr>
                            <td><b>Total</b>
                            </td>
                            <td><b><?php echo number_format($total_bonus_points_stats['total']) ?></b>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id='tabs-7'>
                <form method='get' action='#tabs-7' class='stats-sort'>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='sort'>
                            <option selected='selected' value='pk_revenue'>Filter By</option>
                            <option value='pk_total_sales' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='pk_total_sales' ) ? "selected='selected'" : "" ?> >Total Sales</option>
                            <option value='pk_revenue' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='pk_revenue' ) ? "selected='selected'" : "" ?>>Revenue</option>
                            <option value='pk_bonus_points' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='pk_bonus_points' ) ? "selected='selected'" : "" ?>>Bonus Points</option>
                            <option value='pk_sales_points' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='pk_sales_points' ) ? "selected='selected'" : "" ?>>Sales Points</option>
                            <option value='pk_total_points' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='pk_total_points' ) ? "selected='selected'" : "" ?>>Total Points</option>
                            <option value='pk_commission' <?php echo (isset($_GET[ 'sort']) && $_GET[ 'sort']=='pk_commission' ) ? "selected='selected'" : "" ?>>Commission</option>
                        </select>
                    </div>
                    <div class='large-2 left stats-index-select-wrapper'>
                        <select name='country' class='country-loader'>
                            <option value=''>All Countries</option>
                            <?php foreach ($filter_countries as $c): ?>
                            <option value="<?php echo $c->country_id ?>" <?php echo (isset($_GET[ 'country']) && $_GET[ 'country']==$c->country_id) ? "selected='selected'" : "" ?> >
                                <?php echo $c->short_name ?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <div class='large-2 left city-loader stats-index-select-wrapper'>
                        <select name='city'>
                            <option value=''>All Cities</option>
                            <?php foreach ($filter_cities as $c): ?>
                            <option value="<?php echo $c->name ?>" <?php echo (isset($_GET[ 'city']) && $_GET[ 'city']==$c->name) ? "selected='selected'" : "" ?> >
                                <?php echo $c->name ?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <input type='text' class='date stats-index-input-date' name='begin' placeholder='Begin Date' value="<?php echo $this->input->get('begin') ?>" />
                    <input type='text' class='date stats-index-input-date' name='end' placeholder='End Date' value="<?php echo $this->input->get('end') ?>" />
                    <button type='submit' class='small-btn'>GO</button>
                </form>
                <div id="pk-stats" class="stats-index-stats-wrapper">
                    <?php if (!isset($pk_stats[ 'keys']) || !count($pk_stats[ 'keys'])): ?>
                    <p class="stats-index-no-results">There are no results to display.</p>
                    <?php endif ?> </div>
                <br>
                <br>
                <table>
                    <thead>
                        <tr>
                            <th class="stats-index-column">Name</th>
                            <th class="stats-index-column">Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($pk_stats[ 'keys'])): ?>
                        <?php foreach ($pk_stats[ 'keys'] as $key=> $value): ?>
                        <tr>
                            <td>
                                <?php echo str_replace( "'", "", $pk_stats[ 'keys'][$key]) ?>
                            </td>
                            <td>
                                <?php if ($sort=='pk_revenue' || $sort=='pk_commission' ): ?> $
                                <?php echo number_format($pk_stats[ 'values'][$key]) ?>
                                <?php else: ?>
                                <?php echo number_format($pk_stats[ 'values'][$key]) ?>
                                <?php endif ?> </td>
                        </tr>
                        <?php endforeach ?>
                        <?php endif ?>
                        <!-- <tr>
                            <td><b>Total</b></td>
                            <td><b><?php echo $pk_stats['total'] ?></b></td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
            <div id="tabs-8">
              <form method='get' action='#tabs-8' class='stats-sort'>
                  <div class='large-2 left stats-index-select-wrapper'>
                      <select name='scratch_action'>
                          <option selected='selected' value='scratch'>Stats for</option>
                          <option value='scratch_sales_percentage' <?php echo (isset($_GET[ 'scratch_action']) && $_GET[ 'scratch_action']=='scratch_sales_percentage' ) ? "selected='selected'" : "" ?> >Sales Percentage</option>
                          <option value='scratch_revenue' <?php echo (isset($_GET[ 'scratch_action']) && $_GET[ 'scratch_action']=='scratch_revenue' ) ? "selected='selected'" : "" ?>>Revenue</option>
                          <option value='scratch_piecekeepers' <?php echo (isset($_GET[ 'scratch_action']) && $_GET[ 'scratch_action']=='scratch_piecekeepers' ) ? "selected='selected'" : "" ?>>Best Piecekeepers</option>
                      </select>
                  </div>
                  <div class='large-2 left stats-index-select-wrapper'>
                      <select name='country' class='country-loader'>
                          <option value=''>All Countries</option>
                          <?php foreach ($filter_countries as $c): ?>
                          <option value="<?php echo $c->country_id ?>" <?php echo (isset($_GET[ 'country']) && $_GET[ 'country']==$c->country_id) ? "selected='selected'" : "" ?> >
                              <?php echo $c->short_name ?></option>
                          <?php endforeach ?> </select>
                  </div>
                  <div class='large-2 left city-loader stats-index-select-wrapper'>
                      <select name='city'>
                          <option value=''>All Cities</option>
                          <?php foreach ($filter_cities as $c): ?>
                          <option value="<?php echo $c->name ?>" <?php echo (isset($_GET[ 'city']) && $_GET[ 'city']==$c->name) ? "selected='selected'" : "" ?> >
                              <?php echo $c->name ?></option>
                          <?php endforeach ?> </select>
                  </div>
                  <input type='text' class='date stats-index-input-date' name='begin' placeholder='Begin Date' value="<?php echo $this->input->get('begin') ?>" />
                  <input type='text' class='date stats-index-input-date' name='end' placeholder='End Date' value="<?php echo $this->input->get('end') ?>" />
                  <button type='submit' class='small-btn'>GO</button>
              </form>

                  <?php if (empty($scratch)): ?>
                  <p class="stats-index-no-results">There are no results to display.</p>
                  <br>
                  <br>
                <?php endif ?>
                <div id="scratch-stats" class="stats-index-stats-wrapper">

                 </div>

                <?php if(isset($_GET[ 'scratch_action']) && $_GET[ 'scratch_action']=='scratch_sales_percentage' ): ?>

                  <table>
                      <thead>
                          <tr>
                              <th class="stats-index-column">Name</th>
                              <th class="stats-index-column">Total Sales Percentage (%)</th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php foreach($scratch as $k => $v): ?>
                          <tr>
                              <td>
                                  <?php echo  $k; ?>
                              </td>
                              <td>
                                  <?php echo  number_format($v, 4); ?>
                          </tr>
                          <?php endforeach ?>
                          <tr>
                              <td><b>Total</b>
                              </td>
                              <td><b><?php echo number_format($scratch_total, 4) ?></b>
                              </td>
                          </tr>
                      </tbody>
                  </table>

                <?php endif ?>
                <?php if(isset($_GET[ 'scratch_action']) && $_GET[ 'scratch_action']=='scratch_piecekeepers' ): ?>
                  <table>
                      <thead>
                          <tr>
                              <th class="stats-index-column">Name</th>
                              <th class="stats-index-column">Revenue ($)</th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php foreach($scratch as $user => $revenue ): ?>
                          <tr>
                              <td>
                                  <?php echo  $user; ?>
                              </td>
                              <td>
                                  <?php echo  number_format($revenue); ?>
                          </tr>
                          <?php endforeach ?>
                          <tr>
                              <td><b>Total</b>
                              </td>
                              <td><b><?php echo number_format($scratch_total) ?></b>
                              </td>
                          </tr>
                      </tbody>
                  </table>

                <?php endif ?>
                <?php if(isset($_GET[ 'scratch_action']) && $_GET[ 'scratch_action']=='scratch_revenue' ): ?>
                <table>
                    <thead>
                        <tr>
                            <th class="stats-index-column">Name</th>
                            <th class="stats-index-column">Value ($)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($scratch as $scr => $rev): ?>
                        <tr>
                            <td>
                                <?php echo  $scr; ?>
                            </td>
                            <td>
                                <?php echo  number_format($rev); ?>
                        </tr>
                        <?php endforeach ?>

                        <tr>
                            <td><b>Total</b>
                            </td>
                            <td><b><?php echo number_format($scratch_total) ?></b>
                            </td>
                        </tr>
                    </tbody>
                </table>

              <?php endif ?>


            </div>
        </div>
    </div>
</div>
