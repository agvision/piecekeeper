<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script>
    $(document).ready(function() {
        CKEDITOR.replace('goal-description');
        jQuery('.date').fdatepicker({
            format: '<?php echo $this->users->getDatepickerFormat($admin["id"]) ?>',
            weekStart: 1
        });
    });

    function changeCitiesByCountry(id_country) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('auth/get_cities_by_country') ?>",
            data: 'id_country=' + id_country,
            dataType: 'json',
            success: function(data) {
                var input = "<select id='city' name='city' >\n";
                input += "<option value=''>All Cities</option>\n";
                $.each(data, function(index, value) {
                    input += "<option value='" + value + "'>" + value + "</option>\n";
                })
                input += "</select>";
                $('#city-wrapper').html(input);
            }
        });
    }
</script>
<div class="goal-edit content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <form action="<?php echo base_url('admin/goals/edit/'.$goal->id) ?>" method='post'>
            <div class="header-stats"> Edit Goal </div>
            <br />
            <div class="large-4">
                <input type="text" name="title" placeholder="Goal title" value="<?php echo $goal->title ?>" /> </div>
            <div class="large-4">
                <input type="text" name="deadline" class="date" placeholder="Deadline" value="<?php echo $this->users->formatDate($admin['id'], strtotime($goal->deadline)) ?>" /> </div>
            <div class="large-4">
                <input type="text" name="invited" placeholder="Who will be invited" value="<?php echo $goal->invited ?>" /> </div>
            <div class='large-4'>
                <input type='text' name='points' placeholder='Total Points required' value="<?php echo $goal->points ?>" /> </div>
            <br>
            <div class='large-4'>
                <select name='country' id='country'>
                    <option value=''>All Countries</option>
                    <?php foreach ($countries as $c): ?>
                    <option value="<?php echo $c->country_id ?>" <?php if($c->country_id == $goal->country) echo "selected='selected'" ?>>
                        <?php echo $c->short_name ?></option>
                    <?php endforeach ?> </select>
            </div>
            <div class='large-4' id='city-wrapper'>
                <select name='city'>
                    <option value=''>All Cities</option>
                    <?php foreach ($cities as $c): ?>
                    <option value="<?php echo $c->name ?>" <?php if($c->name == $goal->city) echo "selected='selected'" ?>>
                        <?php echo $c->name ?></option>
                    <?php endforeach ?> </select>
            </div>
            <br>
            <div class="large-4">
                <input type="text" name="required_age" placeholder="Required age" value="<?php  if($goal->required_age != 0){echo $goal->required_age;} ?>" /> </div>
            <div class="large-4">
                <input type="text" name="required_date" id="date_for_goals" placeholder="Date for required age" value="<?php echo $goal->required_date ?>" /> </div>
            <textarea name="description" id="goal-description">
                <?php echo $goal->description ?> </textarea>
            <input type='hidden' name="trigger" value='edit' />
            <br>
            <br>
            <button type='submit' class='btn-success'>Update</button>
        </form>
    </div>
</div>
