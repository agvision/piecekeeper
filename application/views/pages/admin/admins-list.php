    </div>
    <div class="content-bottom">
      <?php $this->load->view('pages/admin/menu') ?>
        <div class="column-right">
          <div class="header-stats">
              Admins <a class="btn-success save-profile-black admin-form-button" href="<?php echo base_url('admin/admins/add/') ?>">Add admin</a>

          </div>

          <table>
          	<thead>
              <tr>
            		<th>Name</th>
            		<th>Email</th>
            		<th>Level</th>
            		<th>Status</th>
            		<td></td>
              </tr>
          	</thead>

          	<?php foreach ($admins as $a): ?>
          		<tr>
          			<td><?php echo $a->first_name.' '.$a->last_name ?></td>
          			<td><?php echo $a->email ?></td>
          			<td>
          				<?php if (intval($a->level) == 2): ?>
          					Admin
          				<?php elseif(intval($a->level) == 3): ?>
          					Super Admin
          				<?php endif ?>
          			</td>
          			<td>
          				<?php if (intval($a->is_active) == 1): ?>
          					Active
          				<?php else: ?>
          					Blocked
          				<?php endif ?>
          			</td>
          			<td>
          				<a href="<?php echo base_url('admin/admins/edit/'.$a->id) ?>">Edit</a> |
          				<?php if (intval($a->is_active) == 1): ?>
          					<a href="<?php echo base_url('admin/admins/block/'.$a->id) ?>">Block</a> |
          				<?php else: ?>
          					<a href="<?php echo base_url('admin/admins/approve/'.$a->id) ?>">Activate</a> |
          				<?php endif ?>
                  <a href="<?php echo base_url('admin/admins/remove_rights/'.$a->id) ?>" class="remove-admin-rights">Remove</a>
          			</td>
          		</tr>
          	<?php endforeach ?>

          </table>


        </div>
