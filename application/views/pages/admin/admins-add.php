<script>
    jQuery(document).ready(function() {
        $('#add-country').click(function() {
            var countries = "";
            <?php foreach($countries as $c): ?>
              countries += "<option value='" + "<?php echo $c->country_id ?>" + "'>" + "<?php echo $c->short_name ?>" + "</option>";
            <?php endforeach ?>
            var select = "<div class='admin-edit-input-wrapper'><select name='countries[]'>" + "<option value=''>Select Country</option>" + countries + "</select></div>";
            $('#countries-wrapper').append(select);
        });
    });
</script>
<div class="content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats"> Add Admin </div>
        <?php if (validation_errors()): ?>
        <div class='form-alert-wrapper'>
            <?php echo validation_errors( "<div class='alert-box error form-alert'>", "</div>"); ?> </div>
        <?php endif ?>
        <div class="form-label">Admin Details:</div>
        <form method='post' action="<?php echo base_url('admin/admins/add/') ?>">
            <div class='admin-add-input-wrapper'>
                <input type='text' name='first_name' placeholder='first name' /> </div>
            <div class='admin-add-input-wrapper'>
                <input type='text' name='last_name' placeholder='last name' /> </div>
            <div class='admin-add-input-wrapper'>
                <input type='email' name='email' placeholder='email' /> </div>
            <div class='admin-add-input-wrapper'>
                <input type='password' name='password' placeholder='password' /> </div>
            <?php if ($admin[ 'id']==4 ): ?>
            <input type='checkbox' name='super_admin' /> Super Admin
            <?php endif ?>
            <div class="form-label">Assign Countries:</div>
            <input type="checkbox" class="all-countries-ckb" name="all-countries" id="all-countries" value="all" />All countries
            <div id="label-countries">
                <div id='countries-wrapper'> </div>
                <div id='add-country' class='btn-white admin-add-btn-add-country'>Add Country</div>
            </div>
            <input type='hidden' name='trigger' value='add' />
            <div class="dotted-line admin-add-dotted-line"></div>
            <button class="btn-black" type="submit">Add</button>
        </form>
    </div>
</div>
