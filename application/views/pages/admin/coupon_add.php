<div class="coupon-add content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats"> Add Coupon for
            <?php echo $user[ 'first_name']. " ".$user[ 'last_name'] ?> </div>
        <br>
        <br>
        <?php if ($this->session->flashdata('error') != ''): ?>
        <div class='form-alert-wrapper'>
            <?php echo $this->session->flashdata('error'); ?> </div>
        <br>
        <?php endif ?>
        <?php if ($this->session->flashdata('success') != ''): ?>
        <div class='form-alert-wrapper'>
            <div class='alert-box success form-alert'>
                <?php echo $this->session->flashdata('success'); ?> </div>
        </div>
        <br>
        <?php endif ?>
        <p class="no-mb">
            Create single use discount code:
        </p>
        <br>
        <form action="<?=base_url('admin/coupons/add/'.$user['id'])?>" method="post" accept-charset="utf-8">
            <div class="row collapse">
                <div class="large-3">
                    <input type='hidden' name='trigger' value='discount_20' />
                    <input type="text" name="code_single" placeholder="code" />
                    <select name="country" class="hide">
                        <option value="">Country</option>
                        <?php foreach ($countries as $country): ?>
                        <option value="<?=$country->code?>">
                            <?=$country->name?></option>
                        <?php endforeach ?> </select>
                    <input type="submit" class="btn-black coupon-add-create-submit" value="Add" /> </div>
                <div class="large-3 column"></div>
            </div>
        </form>
        <div class="divider"></div>
        <p>Create multi use discount code:</p>
        <form action="<?=base_url('admin/coupons/add/'.$user['id'])?>" method="post" accept-charset="utf-8">
            <div class="row collapse">
                <div class="large-3">
                    <input type="text" name="code" placeholder="code" />
                    <select id='discount-type'>
                        <option value='percent' selected='selected'>Percent Discount</option>
                        <option value='amount'>Amount Discount</option>
                    </select>
                    <input class='percent-type' type='text' name='percent' placeholder='percent' />
                    <input class='amount-type' type='text' name='amount' placeholder='amount' />
                    <select class='amount-type' name='currency'>
                        <?php foreach ($currencies as $currency): ?>
                        <option value="<?=$currency->code?>">
                            <?=$currency->code?></option>
                        <?php endforeach ?> </select>
                    <input type='text' name='valability' placeholder='valability in days' />
                    <!-- <input type='text' name='link' placeholder='link' /> -->
                    <!-- <input type='text' name='u_limit' placeholder='usage limit' />
            <input type='text' name='p_limit' placeholder='products limit' /> -->
                    <select name='country' class="hide">
                        <option value="">Country</option>
                        <?php foreach ($countries as $country): ?>
                        <option value="<?=$country->code?>">
                            <?=$country->name?></option>
                        <?php endforeach ?> </select>
                    <input type='hidden' name='trigger' value='48_hours' />
                    <input type="submit" class="btn-black coupon-add-add-submit" value="Add" /> </div>
                <div class="large-3 column"></div>
            </div>
        </form>
    </div>
</div>
