<div class="slaves-request-view content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <?php if ($request->aproved==1): ?>
        <div class="alert-box success no-image">This request was approved</div>
        <?php elseif($request->aproved==0): ?>
        <div class="alert-box warning no-image">This request was not approved yet</div>
        <?php else: ?>
        <div class="alert-box error no-image">This request was rejected
            <br>
            <br>"
            <?php echo $request->reason ?>" </div>
        <?php endif ?>
        <div class="header-stats">
            <?=$title?>
                <?php if($request->aproved==0) { ?>
                <a href="<?=$this->config->base_url()?>admin/slaves_requests/approve/<?=$request->id?>">
                    <button class="save-profile-black admin-form-button btn-success">Approve</button>
                </a> <a class="btn-danger save-profile-white admin-form-button" href="#" id='reject-coupon'>Reject</a>
                <form id='reject-coupon-form' class="slaves-view-reject-coupon" method='post' action="<?php echo base_url('admin/slaves_requests/reject/'.$request->id) ?>">
                    <textarea placeholder="Reason" name='reason' class="slaves-view-reject-coupon-reason"></textarea>
                    <input type='submit' value='Update' class='btn-success' /> <a href='#' id='cancel-reject' class="slaves-view-reject-coupon-cancel"> Cancel</a> </form>
                <?php } ?> </div>
        <h3>Details</h3>
        <table border="0" class="slaves-requests">
            <tr>
                <td class="slaves-requests-column">Name: </td>
                <td><b><?=$request->first_name . " " . $request->last_name?></b>
                </td>
            </tr>
            <tr>
                <td class="slaves-requests-column">Email: </td>
                <td><b><?=$request->email?></b>
                </td>
            </tr>
            <tr>
                <td>Request date:</td>
                <td><b><?=$this->users->formatDate($admin['id'], strtotime($request->created))?></b>
                </td>
            </tr>
            <tr>
                <td>Extra slots request number: </td>
                <td><b><?=$request->slaves?></b>
                </td>
            </tr>
            <tr>
                <td>Current slaves slots: </td>
                <td><b><?=$request->user_slaves?></b>
                </td>
            </tr>
            <tr>
                <td>Approved slaves slots: </td>
                <td><b><?=$slaves?></b>
                </td>
            </tr>
            <tr>
                <td>Used invitations: </td>
                <td><b><?=$request->slave_invitations_sent?></b>
                </td>
            </tr>
            <tr>
                <td colspan="2"><a href="<?=$this->config->base_url()?>admin/user/view/<?=$request->user_id?>">View user profile</a>
                </td>
            </tr>
        </table>
    </div>
</div>
