<script type="text/javascript">
    $(document).ready(function() {
        window.tag_options = "<div class='tag-options'>" + "<select name='user-tags[]'>" + "<option value=''>&nbsp;</option>" +<?php foreach($all_user_tags as $tag): ?>"<option value='<?php echo $tag ?>'> <?php echo $tag ?></option>" + <?php endforeach ?> "</select></div>";
    });

    function changeCitiesByCountry(id_country) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('auth/get_cities_by_country') ?>",
            data: 'id_country=' + id_country,
            dataType: 'json',
            success: function(data) {
                var input = "<select id='city' name='city' >\n";
                input += "<option value=''>All Cities</option>\n";
                $.each(data, function(index, value) {
                    input += "<option value='" + value + "'>" + value + "</option>\n";
                })
                input += "</select>";
                $('#city-wrapper').html(input);
            }
        });
    }

    function changeSchoolsByCountry(id_country) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('auth/get_schools_by_country') ?>",
            data: 'id_country=' + id_country,
            dataType: 'json',
            success: function(data) {
                var input = "<select id='school' name='school'>\n";
                input += "<option value=''>All Schools</option>\n";
                $.each(data, function(index, value) {
                    input += "<option value='" + value + "'>" + value + "</option>\n";
                })
                input += "</select>";
                $('#school-wrapper').html(input);
            }
        });
    }

    function changeSchoolsByCity(city) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('auth/get_schools_by_city') ?>",
            data: 'city=' + city,
            dataType: 'json',
            success: function(data) {
                var input = "<select id='school' name='school' class='medium-input'>\n";
                input += "<option value=''>All Schools</option>\n";
                $.each(data, function(index, value) {
                    input += "<option value='" + value + "'>" + value + "</option>\n";
                })
                input += "</select>";
                $('#school-wrapper').html(input);
            }
        });
    }
</script>
<div class="user-list content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats">
            <?=$title?>
        </div>
        <div class='sort-items-header'>
            <form method="GET" action="#" class="user-list-filter-form" name="filter-form" id="filter-form">
                <div>
                    <input type='text' value="<?=$name?>" name='name' placeholder="Name or Coupon" class="user-list-name-or-coupon" />
                    <input type='text' class="user-list-days-inactive" name='days-inactive' value="<?php echo $days_inactive ?>" placeholder="Days inactive" />
                    <select class="width-auto float-left" name='type'>
                        <option value=''>All Types</option>
                        <option value='1' <?=$type=="1" ? "selected='selected'" : ""?>>UnderKeepers</option>
                        <option value='2' <?=$user_level=="1" ? "selected='selected'" : ""?>>Masters</option>
                    </select>
                    <select class="width-auto float-left" name='level'>
                        <option value=''>All Levels</option>
                        <option value='-2' <?=$user_level=="-2" ? "selected='selected'" : ""?>>PieceKeeper Level 1</option>
                        <option value='-1' <?=$user_level=="-1" ? "selected='selected'" : ""?>>PieceKeeper Level 2</option>
                        <option value='0' <?=$user_level==="0" ? "selected='selected'" : ""?>>PieceKeeper Level 3</option>
                    </select>
                    <select class="width-auto float-left" name='only'>
                        <option value="">Show all</option>
                        <option value='1' <?=$only=="1" ? "selected='selected'" : ""?>>Approved</option>
                        <option value='2' <?=$only==="2" ? "selected='selected'" : ""?>>Disapproved</option>
                        <option value='0' <?=$only==="0" ? "selected='selected'" : ""?>>Pending</option>
                        <option value='3' <?=$only==="3" ? "selected='selected'" : ""?>>Blocked</option>
                    </select>
                    <select class="user-list-select" name='gender'>
                        <option value="">All Genders</option>
                        <option value='0' <?=$filter_gender==="0" ? "selected='selected'" : ""?>>Women</option>
                        <option value='1' <?=$filter_gender=="1" ? "selected='selected'" : ""?>>Men</option>
                    </select>
                    <select id='country' class="user-list-select" name='country'>
                        <option value="">All Countries</option>
                        <?php foreach ($filter_countries as $c): ?>
                        <option value="<?php echo $c->country_id ?>" <?=$filter_country==$c->country_id ? "selected='selected'" : ""?> >
                            <?php echo $c->short_name ?></option>
                        <?php endforeach ?> </select>
                    <select class="user-list-select" name='state' id='state'>
                        <option value="">All States</option>
                        <?php foreach ($filter_states as $s) { if( $s->name != "") { ?>
                        <option value="<?php echo $s->name ?>" <?=$filter_state==$s->name ? "selected='selected'" : ""?> >
                            <?php echo $s->name ?></option>
                        <?php } } ?> </select>
                    <script>
                        $(document).ready(function() {
                            if($("#country").val() != "236") {
                                $("#state").hide();
                            }
                            $("#country").change(function() {
                                if($("#country").val() != "236") {
                                    $("#state").hide();
                                } else {
                                    $("#state").show();
                                }
                            });
                        });
                    </script>
                    <div id='city-wrapper'>
                        <select class="user-list-select" name='city' id='city'>
                            <option value="">All Cities</option>
                            <?php foreach ($filter_cities as $c): ?>
                            <option value="<?php echo $c->name ?>" <?=$filter_city===$c->name ? "selected='selected'" : ""?> >
                                <?php echo $c->name ?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <div id="school-wrapper">
                        <select class="user-list-select" name='school'>
                            <option value="">All Schools</option>
                            <?php foreach ($filter_schools as $s): ?>
                            <option value="<?php echo $s->name ?>" <?=$filter_school===$s->name ? "selected='selected'" : ""?> >
                                <?php echo $s->name ?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <select class="user-list-select" name="signup">
                        <option value="">All Signup Types</option>
                        <option value="ipad" <?=$filter_signup === "ipad" ? "selected='selected'" : ""?>>iPad</option>
                        <option value="pku" <?=$filter_signup === "pku" ? "selected='selected'" : ""?>>PKU</option>
                    </select>
                    <div class="display-inline-block">Age from
                        <input type='text' class="user-list-age" name='low-age' value="<?php echo $low_age ?>" /> to
                        <input type='text' class="user-list-age" name='high-age' value="<?php echo $high_age ?>" /> years</div>
                    <div class="limit-by">
                        Limit by:
                        <input type='text' class="user-list-age" name='limit-rows' value="<?php echo $limit_rows ?>" />
                    </div>
                    <div class='user-tags-filter-wrapper'>
                        <div class='filter-label'>PieceKeeper Tags:</div>
                        <div id='user-tags-options-wrapper'>
                            <?php if ($count_selected_tags): ?>
                            <?php foreach ($selected_tags as $selected_tag): ?>
                            <div class='tag-options'>
                                <select name='user-tags[]'>
                                    <option value=''>&nbsp;</option>
                                    <?php foreach ($all_user_tags as $tag): ?>
                                    <option value='<?php echo $tag ?>' <?php if(str_replace( ' ', '', $tag)==str_replace( "'", "", $selected_tag)) echo "selected='selected'" ?>>
                                        <?php echo $tag ?>
                                    </option>
                                    <?php endforeach ?> </select>
                            </div>
                            <?php endforeach ?>
                            <?php endif ?> </div>
                        <div id='filter-add-user-tag'>+ Add</div>
                    </div>
                    <input id="table-order-by" name="table-order-by" type="hidden" value="<?=$order_by;?>" />
                    <input id="table-order-how" name="table-order-how" type="hidden" value="<?=$order_how;?>" />
                    <input class='small-btn small-btn-success' type='submit' value="APPLY FILTERS" /> </div>
            </form>
            <?php if ($this->session->flashdata("coupons_response")): ?>
            <?php $response=$this->session->flashdata("coupons_response") ?>
            <?php if ($response[ 'status']=="success" ): ?>
            <div class='form-alert-wrapper user-list-alert-wrapper'>
                <div class='alert-box no-image success form-alert user-list-form-alert'> You have created
                    <?php echo $response[ 'count'] ?> coupons with the following details:
                    <br>
                    <br>
                    <?php if ($response[ 'type']=="1" ): ?> Type: Single Use
                    <br>
                    <?php else: ?> Type: Multiple Use
                    <br>
                    <?php endif ?> Availability:
                    <?php echo $response[ 'days'] ?> Days
                    <br> Discount:
                    <?php echo $response[ 'percentage'] ?>%
            </div>
            <?php elseif ($response[ 'status']=="error" ): ?>
            <div class='form-alert-wrapper user-list-alert-wrapper'>
                <div class='alert-box no-image error form-alert user-list-form-alert'> You have the following errors:
                    <br>
                    <br>
                    <?php foreach ($response[ 'errors'] as $error): ?> -
                    <?php echo $error ?>
                    <br>
                    <?php endforeach ?> </div>
            </div>
            <?php endif ?>
            <?php endif ?>
            <div class="coupon-generator">
                <div class="show coupon-generator-title">Create Discount Code</div>
                <form method="post" action="<?php echo base_url('/admin/user/create_discount_codes') ?>">
                    <select name="type">
                        <option value="" disabled="disabled" selected="selected">Select Type</option>
                        <option value="1">Single Use</option>
                        <option value="2">Multiple Use</option>
                    </select>
                    <select name="days">
                        <option value="" disabled="disabled" selected="selected">Availability in Days</option>
                        <?php for($i=1 ; $i <=10; $i++){ echo "<option value='".$i. "'>".$i. "</option>"; } ?> </select>
                    <input type="text" name="percentage" placeholder="Percentage" />
                    <input type="hidden" name="users" value="<?php echo $all_ids_users ?>" />
                    <input type="hidden" name="filters_string" value="<?php echo $filters_string ?>" />
                    <button type="submit">Create</button>
                </form>
            </div>
            <form method='post' action='#' class="user-list-export-form">
                <select name='format' class="user-list-export-select">
                    <option value='csv'>CSV</option>
                    <!--
                    <option value='xls'>XLS</option>
                    <option value='xlsx'>XLSX</option>
                    -->
                </select>
                <input type='hidden' name='trigger' value='export-xls' />
                <input type='submit' value="Export" class='btn-white user-list-export-btn' /> </form>
        </div>
        <table class="admin-table tablesorter table-layout-fixed" id='piecekeepers-list'>
            <thead>
                <tr>
                    <th id="table-order-by-name" style="width:130px;">Name</th>
                    <th id="table-order-by-email" style="width:160px;">Email</th>
                    <th id="table-order-by-level" style="width:60px;">Level</th>
                    <th id="table-order-by-ipad" style="width:50px;">iPad</th>
                    <th id="table-order-by-followers" style="width:80px;">Followers</th>
                    <th id="table-order-by-points" style="width:60px;">Total Points</th>
                    <th id="table-order-by-reg" style="width:80px;">Joined</th>
                    <th id="table-order-by-inact" style="width:80px;" class="empty-top">Inactive for days</th>
                    <td style="width:70px;">&nbsp;</td>
                </tr>
            </thead>
            <tbody>
                <?php $level[ '-2']='Level 1' ; $level[ '-1']='Level 2' ; $level[ '0']='Level 3' ; $level[ '1']='Master' ; $level[ '2']='Admin' ; $level[ '3']='Super Admin' ; $currentTime=new DateTime(date( 'Y-m-d')); ?>


                <?php foreach ($users as $user): ?>

                    <?php $last_login = new DateTime(substr($user->last_login,0,10)); $diff = $currentTime->diff($last_login); if($diff->days > 700000){ $loggedInDays = ""; } else { $loggedInDays = $diff->days; } ?>

                    <tr>
                        <td>
                            <a href="<?php echo base_url('/admin/user/view/' . $user->id) ?>" id="<?php echo base_url('/images/profiles/big/' . $user->profile_pic) ?>"><?php echo $user->first_name." ".$user->last_name ?></a>
                        </td>
                        <?php if (strlen($user->email) > 25): ?>
                            <td><?php echo str_replace("@", "<br>@", $user->email) ?></td>
                        <?php else: ?>
                            <td><?php echo $user->email ?></td>
                        <?php endif ?>
                        <td><?php echo $level[$user->level] ?></td>
                        <td>
                            <?php if ($user->ipad): ?>
                                <img src="<?php echo base_url('images/ipad-icon.png') ?>" />
                            <?php endif ?>
                        </td>
                        <td><?php echo number_format($user->followers) ?></td>
                        <td><?php echo number_format($user->total_points) ?></td>
                        <td><?php echo date('d M y', strtotime($user->date_joined)) ?></td>
                        <td><?php echo $loggedInDays ?></td>
                        <td>
                            <a href="<?php echo base_url('/admin/user/edit/' . $user->id) ?>">
                                <img src="<?php echo base_url('images/edit.png') ?>" alt="edit icon" >
                            </a>

                            <?php if (($user->is_active == 0 || $user->is_active == -1) && $user->deleted == 0): ?>
                                <a href="<?php echo base_url('/admin/user/approve/' . $user->id) ?>">Approve</a>
                            <?php endif ?>
                        </td>
                    </tr>
                <?php endforeach ?>

                <?php if (!sizeof($users)): ?>
                    <tr>
                        <td colspan="9">There are no PieceKeepers matching this criteria!</td>
                    </tr>
                <?php endif ?>

            </tbody>
        </table>
        <div class="text-align-center">
            <?php echo $this->pagination->create_links(); ?> </div>
    </div>
</div>
