<div class="content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats clearfix">
            <?=$title?>
                <div class="sub-title">
                    <?=count($participants)?> submissions </div>
        </div>
        <div>
            <form method='post' action='#' class="text-align-right">
                <select name='format' class="mission-select">
                    <option value='xls'>XLS</option>
                    <option value='xlsx'>XLSX</option>
                </select>
                <input type='hidden' name='trigger' value='export-xls' />
                <input type='submit' value="Export" class='btn-white mission-export-submit' /> </form>
        </div>
        <?php if (count($participants)): ?>
        <table class="full-width">
            <thead>
                <tr class="text-align-left">
                    <th>Name</th>
                    <th>Country</th>
                    <th>City</th>
                    <th>Submitted</th>
                    <th>Status</th>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($participants as $p): ?>
                <tr>
                    <td>
                        <a href="<?php echo base_url('admin/user/view/'.$p->id_user) ?>">
                            <?php echo $p->first_name.' '.$p->last_name ?></a>
                    </td>
                    <td>
                        <?php echo $p->short_name ?> </td>
                    <td>
                        <?php echo $p->city ?> </td>
                    <td>
                        <?php echo $p->submitted ?></td>
                    <td>
                        <?php if ($p->status == -1): ?> <i>Rejected</i>
                        <?php elseif($p->status == 0): ?> <b>Waiting</b>
                        <?php else: ?> Completed
                        <?php endif ?> </td>
                    <td> <a href="<?php echo base_url('admin/missions/view/').'/'.$mission->id.'/'.$p->id_user ?>">View</a> </td>
                </tr>
                <?php endforeach ?> </tbody>
        </table>
        <?php else: ?>
        <div class='form-label no-mt'>There are no participants.</div>
        <?php endif ?> </div>
</div>
