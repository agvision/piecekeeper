
    <div class="content-bottom">        
      <?php $this->load->view('pages/admin/menu') ?>
        <div class="column-right">

            <div class="header-stats"><?=$title?></div>
            
            <?php if (count($notifications)): ?>
                <div id='not-wrapper'>
                    <?php foreach ($notifications as $notification): ?>
                        <?php $details = $notification->getDetails(); ?>
                        <?php if (isset($details['label']) && isset($details['url']) && isset($details['text'])): ?>
                            <div class="alert-box no-image <?php if($notification->active) echo 'success'; ?>">
                                <div class='admin-notification-message'>
                                    <div class='wrapper-label'>
                                        <div class='notification-label'><?=$details['label']?></div>
                                    </div>
                                    <a href="#" class='read-notification' value="<?php echo $notification->id ?>"><?=$details['text']?></a>
                                </div>
                                <?php if ($notification->id_admin): ?>
                                    <?php if ($notification->active): ?>
                                        <div class='admin-notification-date' style='color: inherit; margin-top: 0;'>
                                          <b>Admin:</b> <?php echo $notification->admin_name ?> 
                                        </div>
                                    <?php else: ?>
                                        <div class='admin-notification-date' style='margin-top: 0;'>
                                          <b>Admin:</b> <?php echo $notification->admin_name ?> 
                                        </div>
                                    <?php endif ?>
                                <?php endif ?>
                            </div>
                        <?php endif ?>
                    <?php endforeach ?>
                </div>

                <div class='pagination-wrapper'>
                  <div class='pagination-prev-page'>
                    <?php if ($current_page > 1): ?>
                      <a href="<?php echo base_url('/admin/user/notifications/'.$user->id.'/'.($current_page-1)) ?>"><< Prev</a>
                    <?php endif ?>
                  </div>
                  <div class='pagination-next-page'>
                    <?php if ($current_page < $total_pages): ?>
                      <a href="<?php echo base_url('/admin/user/notifications/'.$user->id.'/'.($current_page+1)) ?>">Next >></a>
                    <?php endif ?>
                  </div>
                </div>
            <?php else: ?>
                <br>
            <?php endif ?>

        </div>
    </div>