<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script type="text/javascript">
		$(document).ready(function(){
			CKEDITOR.replace('body', {
	            contentsCss: "<?php echo base_url('/css/style.css') ?>"
	        });
	        CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
	        CKEDITOR.config.allowedContent = true;
		});
</script>

<div class="content-bottom">        
      <?php $this->load->view('pages/admin/menu') ?>
        <div class="column-right">

        	<div class="header-stats">Add Content</div>

			<form action="<?php echo base_url('admin/content/add') ?>" method='post'>
				<input type='text' name='name' placeholder='name' />
				<select name='type'>
					<option value='0'>Style</option>
					<option value='1'>Text Only</option>
				</select>	
				<textarea id='body' name='body'></textarea>
				<input type='hidden' name='trigger' value='add' />
				<br>
				<button class='btn-black' type='submit'>Add</button>
			</form>

		</div>
</div>