<div class="mission-response content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats clearfix">
            <?=$title?>
                <?php if ($response->status == 0): ?>
                <div id='reject' class="save-profile-black admin-form-button btn-danger">Reject</div>
                <a href="<?php echo base_url('admin/missions/approve/'.$mission->id.'/'.$participant['id']) ?>">
                    <div class="save-profile-black admin-form-button btn-success">Approve</div>
                </a>
                <?php endif ?>
                <div id='reject-wrapper'>
                    <form action="<?php echo base_url('admin/missions/reject') ?>" method='post'>
                        <textarea name='details' placeholder='Rejection Details'></textarea>
                        <input type='submit' class='btn-black' value='Send' />
                        <input type='hidden' name='mission' value="<?php echo $mission->id ?>" />
                        <input type='hidden' name='user' value="<?php echo $participant['id'] ?>" />
                        <input type='hidden' name='trigger' value="reject" /> </form>
                </div>
        </div>
        <table border="0" class="mission-response-participants">
            <tr>
                <td class="mission-response-column">Name: </td>
                <td><b><?=$participant['first_name'] . " " . $participant['last_name']?></b>
                </td>
            </tr>
            <tr>
                <td class="mission-response-column">Email: </td>
                <td><b><?=$participant['email']?></b>
                </td>
            </tr>
            <tr>
                <td>Submitted:</td>
                <td><b><?=$response->submitted?></b>
                </td>
            </tr>
            <?php if ($mission->fb_share): ?>
            <tr>
                <td>Facebook Share:</td>
                <td>
                    <?php if ($response->fb_shared): ?> Shared
                    <?php else: ?> <b>Not shared</b>
                    <?php endif ?> </td>
            </tr>
            <?php endif ?>
            <?php if ($mission->tw_share): ?>
            <tr>
                <td>Twitter Share:</td>
                <td>
                    <?php if ($response->tw_shared): ?> Shared
                    <?php else: ?> <b>Not shared</b>
                    <?php endif ?> </td>
            </tr>
            <?php endif ?>
            <?php if ($mission->pin_share): ?>
            <tr>
                <td>Pinterest Share:</td>
                <td>
                    <?php if ($response->pin_shared): ?> Shared
                    <?php else: ?> <b>Not shared</b>
                    <?php endif ?> </td>
            </tr>
            <?php endif ?>
            <?php if ($response->text): ?>
            <tr>
                <td>Response Message</td>
                <td>
                    <?php echo $response->text ?></td>
            </tr>
            <?php endif ?>
            <?php if ($response->link): ?>
            <tr>
                <td>Response Link</td>
                <td>
                    <a href="<?=$response->link?>" target="_blank"><?php echo $response->link ?></a></td>
            </tr>
            <?php endif ?>
            <?php if ($response->image): ?>
            <tr>
                <td>Response Image</td>
                <td> <a href="<?php echo base_url('images/missions/'.$response->image) ?>" target='_blank'>
                                <img src="<?php echo base_url('images/missions/'.$response->image) ?>" width="320">
                            </a> </td>
            </tr>
            <?php endif ?>
            <tr>
                <td colspan="2"><a href="<?=$this->config->base_url()?>admin/user/view/<?=$participant['id']?>">View user profile</a>
                </td>
            </tr>
        </table>
    </div>
</div>
