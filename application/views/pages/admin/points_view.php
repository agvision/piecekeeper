<script type="text/javascript">
		$(document).ready(function() {
				$('#points-type').change(function() {
						location.href = "<?php echo base_url('/admin/points/view') ?>" + "/" + $(this).val();
				});
		});
</script>
<div class="content-bottom">
		<?php $this->load->view('pages/admin/menu') ?>
		<div class="column-right">
				<div class="header-stats">
						<?php echo $title ?> </div>
				<div id='points-type-wrapper'>
						<p class="points-view-type-label">Points Type:</p>
						<select name='points-type' id='points-type' class="points-view-type-select">
								<option value='0'>All</option>
								<option <?php if($type==1 ) echo "selected='selected'" ?> value='1'>Sales</option>
								<option <?php if($type==2 ) echo "selected='selected'" ?> value='2'>Bonus</option>
						</select>
				</div>
				<table>
						<thead>
								<th width="150">PieceKeeper</th>
								<th width="150">Points</th>
								<th width="150">Type</th>
								<th width="150">Details</th>
								<th width="150">Created</th>
						</thead>
						<tbody>
								<?php foreach ($points as $p): ?>
								<tr>
										<td>
												<a href="<?php echo base_url('/admin/user/view/'.$p->id_user) ?>">
														<?php echo $p->first_name.' '.$p->last_name ?></a>
										</td>
										<td>
												<?php echo $p->value ?></td>
										<td>
												<?php if ($p->type == 1): ?> Sales
												<?php elseif ($p->type == 2): ?> Bonus
												<?php endif ?> </td>
										<td>
												<?php if ($p->type == 1): ?>
												<?php echo $p->label ?>
												<?php elseif ($p->type == 2): ?>
												<?php if ($p->link): ?>
												<a href="<?php echo $p->link ?>">
														<?php echo $p->label ?></a>
												<?php else: ?>
												<?php echo $p->label ?>
												<?php endif ?>
												<?php endif ?> </td>
										<td>
												<?php echo $this->users->formatDate($admin['id'], strtotime($p->created)) ?></td>
								</tr>
								<?php endforeach ?> </tbody>
				</table>
				<br>
				<br>
				<br> </div>
</div>
