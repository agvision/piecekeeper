    <div class="content-bottom">        
      <?php $this->load->view('pages/admin/menu') ?>
        <div class="column-right">
            
            <div class="header-stats">
                <?php echo $title ?>
            </div>

            <br>

            <form class='form-horizontal' id="" action="<?=base_url('admin/settings/add_commission')?>" method="post" accept-charset="utf-8">
              <div class='row collapse'>


                <div class='large-4'> 
                    <select name='country'>
                        <option value=''>Choose Country</option>
                        <?php foreach ($countries as $c): ?>
                            <option value="<?php echo $c->country_id ?>"><?php echo $c->short_name ?></option>
                        <?php endforeach ?>
                    </select> 
                </div>

                <div class='large-4'>
                    <select name='level'>
                        <option value='-2'>PieceKeeper Level 1</option>
                        <option value='-1'>PieceKeeper Level 2</option>
                        <option value='0'>PieceKeeper Level 3</option>
                    </select>
                </div>

                <div class='large-4'>
                    <input type='text' name='value' placeholder='value' />
                </div>

                <input type='hidden' name='trigger' value='add' />

                <div class="row collapse clear">
                  <input type="submit" class="save-profile-black" value="Add" style='width: 70px; padding-top: 8px;' />
                </div>

              </div>
            </form>
       
        </div>
    </div>