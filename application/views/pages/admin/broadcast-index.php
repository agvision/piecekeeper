<script type="text/javascript">
    $(document).ready(function() {
        window.tag_options = "<div class='tag-options'>" + "<select name='user-tags[]'>" + "<option value=''>&nbsp;</option>" + <?php foreach($all_user_tags as $tag): ?> "<option value='<?php echo $tag ?>'><?php echo $tag ?></option>" + <?php endforeach ?> "</select></div>";
    });

    function changeCitiesByCountry(id_country) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('auth/get_cities_by_country') ?>",
            data: 'id_country=' + id_country,
            dataType: 'json',
            success: function(data) {
                var input = "<select id='city' name='filter_city'>\n";
                input += "<option value=''>All</option>\n";
                $.each(data, function(index, value) {
                    input += "<option value='" + value + "'>" + value + "</option>\n";
                })
                input += "</select>";
                $('#city-wrapper').html(input);
            }
        });
    }

    function changeSchoolsByCountry(id_country) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('auth/get_schools_by_country') ?>",
            data: 'id_country=' + id_country,
            dataType: 'json',
            success: function(data) {
                var input = "<select id='school' name='filter_school'>\n";
                input += "<option value=''>All</option>\n";
                $.each(data, function(index, value) {
                    input += "<option value='" + value + "'>" + value + "</option>\n";
                })
                input += "</select>";
                $('#school-wrapper').html(input);
            }
        });
    }

    function changeSchoolsByCity(city) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('auth/get_schools_by_city') ?>",
            data: 'city=' + city,
            dataType: 'json',
            success: function(data) {
                var input = "<select id='school' name='filter_school'>\n";
                input += "<option value=''>All</option>\n";
                $.each(data, function(index, value) {
                    input += "<option value='" + value + "'>" + value + "</option>\n";
                })
                input += "</select>";
                $('#school-wrapper').html(input);
            }
        });
    }
</script>
<div class="broadcast-index content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="float-right">
            <div id='search-results' class="cursor-pointer"> Based on selected filters found <b><?=$results_no?></b> users </div>
            <div class="admin-broadcast-export-wrapper">
                <form method='post' action='#' class="float-right">
                    <select id="select-exp-format" name='format' class="admin-broadcast-export-format">
                        <option value='csv'>CSV</option>
                        <!--
                        <option value='xls'>XLS</option>
                        <option value='xlsx'>XLSX</option>
                        -->
                    </select>
                    <input type='hidden' name='trigger' value='export-xls' />
                    <input id="submit-export" type='submit' value="Export" class='btn-white admin-broadcast-export-submit' /> </form>
            </div>
        </div>
        <button id='filters-btn' class='btn-success'>Show Filters</button> <a href="<?php echo base_url('/admin/emails_report') ?>" class='btn-black'>Broadcast Reports</a>
        <br>
        <br>
        <div id='list-results' class="display-none">
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Country</th>
                        <th>City</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($results as $r): ?>
                    <tr>
                        <td>
                            <a href="<?php echo base_url('/admin/user/view/'.$r->id) ?>">
                                <?php echo $r->first_name.' '.$r->last_name ?></a>
                        </td>
                        <td>
                            <?php echo $r->email ?></td>
                        <td>
                            <?php echo $r->country ?></td>
                        <td>
                            <?php echo $r->city ?></td>
                    </tr>
                    <?php endforeach ?> </tbody>
            </table>
        </div>
        <div id="filters">
            <div class='dotted-line admin-broadcast-dotted-line'></div>
            <form action="#" method="post" id="form-filter">
                <input type="hidden" id="exp-format" name="exp-format" value="xls">
                <input type="hidden" id="exp" name="exp" value="false">
                <ul id='broadcast-wrapper'>
                    <li>
                        <div class='user-tags-filter-wrapper'>
                            <div class='filter-label'>PieceKeeper Tags:</div>
                            <div id='user-tags-options-wrapper'>
                                <?php if ($count_selected_tags): ?>
                                <?php foreach ($selected_tags as $selected_tag): ?>
                                <div class='tag-options'>
                                    <select name='user-tags[]'>
                                        <option value=''>&nbsp;</option>
                                        <?php foreach ($all_user_tags as $tag): ?>
                                        <option value='<?php echo $tag ?>' <?php if(str_replace( ' ', '', $tag)==str_replace( "'", "", $selected_tag)) echo "selected='selected'" ?>>
                                            <?php echo $tag ?>
                                        </option>
                                        <?php endforeach ?> </select>
                                </div>
                                <?php endforeach ?>
                                <?php endif ?> </div>
                            <div id='filter-add-user-tag'>+ Add</div>
                        </div>
                    </li>
                    <li>Age between
                        <input value="<?=$age_min?>" type="number" name="age_min" class="small" /> and
                        <input type="number" value="<?=$age_max?>" name="age_max" class="small" />
                    </li>
                    <li>Gender:
                        <select name="gender" class="small">
                            <option value="">All</option>
                            <option <?=($gender==1 ? "selected='selected'" : "" )?> value="1">Female</option>
                            <option <?=($gender==2 ? "selected='selected'" : "" )?> value="2">Male</option>
                        </select>
                    </li>
                    <li> From country
                        <select id='country' name="filter_country" class='small-3'>
                            <option value=''>All</option>
                            <?php foreach ($filter_countries as $c): ?>
                            <option value="<?php echo $c->country_id ?>" <?=($filter_country==$c->country_id ? "selected='selected'" : "" )?> >
                                <?php echo $c->short_name ?></option>
                            <?php endforeach ?> </select>
                    </li>
                    <li> From state
                        <select multiple id='state' name="filter_state_arr[]" class='small-3'>
                            <option value=''>All</option>
                            <?php foreach ($filter_states as $s): ?>
                            <option value="<?php echo $s->name ?>" <?=( ( is_array($filter_state_arr) && in_array($s->name, $filter_state_arr) ) ? "selected='selected'" : "" )?> >
                                <?php echo $s->name ?></option>
                            <?php endforeach ?> </select>
                    </li>
                    <li> From city
                        <div class='small-3' id='city-wrapper'>
                            <select name="filter_city" id='city'>
                                <option value=''>All</option>
                                <?php foreach ($filter_cities as $c): ?>
                                <option value="<?php echo $c->name ?>" <?=($filter_city===$c->name ? "selected='selected'" : "" )?> >
                                    <?php echo $c->name ?></option>
                                <?php endforeach ?> </select>
                        </div>
                    </li>
                    <li> From school
                        <div class='small-3' id='school-wrapper'>
                            <select name="filter_school" id='school'>
                                <option value=''>All</option>
                                <?php foreach ($filter_schools as $s): ?>
                                <option value="<?php echo $s->name ?>" <?=($filter_school===$s->name ? "selected='selected'" : "" )?> >
                                    <?php echo $s->name ?></option>
                                <?php endforeach ?> </select>
                        </div>
                    </li>
                    <li>
                        <input value="<?=$min_sales?>" name="min_sales" type="number" class="small" /> sales in the period
                        <input value="<?=$min_sales_start?>" name="min_sales_start" id="date_start" class="admin-broadcast-input display-inline" placeholder="Start" type="text"> to
                        <input value="<?=$min_sales_end?>" name="min_sales_end" id="date_end" class="admin-broadcast-input display-inline" placeholder="End" type="text"> </li>
                    <li>Anyone with activity the last
                        <input value="<?=$activity_days?>" type="number" class="small" name="activity_days" /> days</li>
                    <li>Anyone with no activity since the last
                        <input value="<?=$no_activity_days?>" type="number" class="small" name="no_activity_days" /> days</li>
                    <li> Anyone with more than
                        <input value="<?=$min_slaves?>" type="number" class="small" name="min_slaves" /> UnderKeepers </li>
                    <li> Global Rank better than
                        <input value="<?=$global_rank?>" name='global_rank' type='number' class="admin-broadcast-input" />
                        <li>
                            <li> Country Rank better than
                                <input value="<?=$country_rank?>" name='country_rank' type='number' class="admin-broadcast-input" />
                                <li>
                                    <li> City Rank better than
                                        <input value="<?=$city_rank?>" name='city_rank' type='number' class="admin-broadcast-input" />
                                        <li>
                                            <li> Social Media following between
                                                <input value="<?=$min_followers?>" name='min_followers' type='number' class="admin-broadcast-input" /> and
                                                <input value="<?=$max_followers?>" name='max_followers' type='number' class="admin-broadcast-input" />
                                                <li> </li>
                                                <li>Level
                                                    <select name="level" class="small-3">
                                                        <option value="">All</option>
                                                        <option <?=($level=="-2" ? "selected='selected'" : "" )?> value="-2">PieceKeeper Level 1</option>
                                                        <option <?=($level=="-1" ? "selected='selected'" : "" )?> value="-1">PieceKeeper Level 2</option>
                                                        <option <?=($level==="0" ? "selected='selected'" : "" )?> value="0">PieceKeeper Level 3</option>
                                                        <option <?=($level==="1" ? "selected='selected'" : "" )?> value="1">Master PieceKeeper</option>
                                                    </select>
                                                </li>
                                                <li>Only UnderKeepers
                                                    <select name='slaves' class='small'>
                                                        <option value=''>No</option>
                                                        <option value='1' <?=($slaves==="1" ? "selected='selected'" : "" )?> >Yes</option>
                                                    </select>
                                                </li>
                                                <li>
                                                    <input class='small-btn' type="submit" value="Filter" id="broadcast-filter-submit" />
                                                </li>
                </ul>
            </form>
        </div>
        <br>
        <div class="header-stats">Broadcast</div>
        <div class="float-right"><a href="<?=base_url('admin/email_templates')?>">Manage templates</a>
        </div> Template:
        <select class="admin-broadcast-template-select" onchange="changeTemplate(this.value);">
            <option value="">No template</option>
            <?php foreach ($templates as $template): ?>
            <option value="<?=$template->id?>">
                <?=$template->name?></option>
            <?php endforeach ?> </select>
        <form method="post" action="<?php echo base_url('admin/broadcast/send_email') ?>">
            <?php foreach ($filters as $key=> $value): ?>
            <?php if (is_array($value)): ?>
            <?php foreach ($value as $v): ?>
            <input value="<?=$v?>" name="<?=$key?>[]" type="hidden" />
            <?php endforeach ?>
            <?php else: ?>
            <input value="<?=$value?>" name="<?=$key?>" type="hidden" />
            <?php endif ?>
            <?php endforeach ?>
            <input type="text" value="" name="subject" placeholder="Email Subject" />
            <textarea name="mail_body" id="mail_body"></textarea>
            <br>
            <button class='btn-black' type="submit">Send</button>
        </form>
    </div>
</div>
<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script>
    window.onload = function() {
            CKEDITOR.replace('mail_body');
        }
        //jQuery('.date').fdatepicker({format: '<?php echo $this->users->getDatepickerFormat($admin["id"]) ?>', weekStart: 1});
    function changeTemplate(template_id) {
        if(template_id == "") return;
        $.getJSON("<?=base_url('admin/broadcast/get_template')?>/" + template_id, function(data) {
            CKEDITOR.instances.mail_body.setData(data.content);
        });
    }
</script>
