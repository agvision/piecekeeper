<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script>
		$(document).ready(function() {
				CKEDITOR.replace('mission-description');
				jQuery('.date').fdatepicker({
						format: '<?php echo $this->users->getDatepickerFormat($admin["id"]) ?>',
						weekStart: 1
				});
		});

		function changeCitiesByCountry(id_country) {
				$.ajax({
						type: 'POST',
						url: "<?php echo base_url('auth/get_cities_by_country') ?>",
						data: 'id_country=' + id_country,
						dataType: 'json',
						success: function(data) {
								var input = "<select id='city' name='city' >\n";
								input += "<option value=''>All Cities</option>\n";
								$.each(data, function(index, value) {
										input += "<option value='" + value + "'>" + value + "</option>\n";
								})
								input += "</select>";
								$('#city-wrapper').html(input);
						}
				});
		}

		function addSocialButton(network, elem)
		{
			if(elem.checked)
			{
				CKEDITOR.instances['mission-description'].setData(CKEDITOR.instances['mission-description'].getData() + "\n{"+network+"_button}");
				//$('#mission-description').val(');
			}


		}
</script>
<div class="mission-add content-bottom">
		<?php $this->load->view('pages/admin/menu') ?>
		<div class="column-right">
				<form action="<?php echo base_url('admin/missions/add') ?>" method='post' enctype='multipart/form-data'>
						<div class="header-stats"> Add Mission </div>
						<?php if (validation_errors()): ?>
						<div class='form-alert-wrapper'>
								<?php echo validation_errors( "<div class='alert-box error form-alert'>", "</div>"); ?> </div>
						<?php endif ?>
						<br />
						<div class="large-4">
								<input type="text" name="name" placeholder="Mission name" value="" /> </div>
						<div class="large-4">
								<input type="text" name="from" class="date" placeholder="Begin Date" value="" />
								<input type="text" name="to" class="date" placeholder="End Date" value="" />
								<input type="checkbox" name="countdown" /> Show Countdown </div>
						<div class='large-4'>
								<input type='text' name='points' placeholder='Bonus Points' /> </div>
						<br>
						<br>
						<div class='large-4'>
								<input type='file' name='image' /> </div>
						<div class='large-4'>
								<textarea name='video' placeholder='Embed Video Code'></textarea>
						</div>
						<br>
						<?php if ($admin[ 'level']==3 ): ?>
						<div class='large-4'>
								<select name='country' id='country'>
										<option value=''>All Countries</option>
										<?php foreach ($countries as $c): ?>
										<option value="<?php echo $c->country_id ?>">
												<?php echo $c->short_name ?></option>
										<?php endforeach ?> </select>
						</div>
						<?php else: ?>
						<div class='large-4'>
								<select name='country' id='country'>
										<option value=''>Select Country</option>
										<?php foreach ($countries as $c): ?>
										<option value="<?php echo $c->country_id ?>">
												<?php echo $c->short_name ?></option>
										<?php endforeach ?> </select>
						</div>
						<?php endif ?>
						<?php if ($admin[ 'level']==3 ): ?>
						<div id='city-wrapper' class='large-4'>
								<select name='city'>
										<option value=''>All Cities</option>
										<?php foreach ($cities as $c): ?>
										<option value="<?php echo $c->name ?>">
												<?php echo $c->name ?></option>
										<?php endforeach ?> </select>
						</div>
						<?php else: ?>
						<div id='city-wrapper' class='large-4'>
								<select name='city'>
										<option value=''>Select City</option>
										<?php foreach ($cities as $c): ?>
										<option value="<?php echo $c->name ?>">
												<?php echo $c->name ?></option>
										<?php endforeach ?> </select>
						</div>
						<?php endif ?>
						<br> Target PieceKeepers by Level:
						<div class='admin-label'> * If no level is selected this mission will be available for all PieceKeepers </div>
						<div class='large-4'>
								<input type='checkbox' name='pk_level_1' /> Level 1 </div>
						<div class='large-4'>
								<input type='checkbox' name='pk_level_2' /> Level 2 </div>
						<div class='large-4'>
								<input type='checkbox' name='pk_level_3' /> Level 3 </div>
						<div class='large-4'>
								<input type='checkbox' name='pk_level_m' /> Master PieceKeeper </div>
						<br>
						<div class='large-6'>
								<input type='checkbox' name='allow_text' /> Allow Text Response (use {text_box})</div>
						<div class='large-4'>
								<input type='text' name='text_details' placeholder='Details about text response' /> </div>
						<div class='large-6'>
								<input type='checkbox' name='allow_link' /> Allow Link Response (use {link_box})</div>
						<div class='large-4'>
								<input type='text' name='link_details' placeholder='Details about link' /> </div>
						<div class='large-4'>
								<input type='checkbox' name='allow_image' /> Allow Image Response </div>
						<div class='large-4'>
								<input type='checkbox' name='sales_goals' /> Sales Goals </div>
						<div class='large-4'>
								<input type='text' name='sales' placeholder='Number of sales' /> </div>
						<div class='large-4'>
								<input type='text' name='days_number' placeholder='Number of days' /> </div>
						<div class='large-4'>
								<input type='text' class='date' name='sales_from' placeholder='Sales from' /> </div>
						<div class='large-4'>
								<input type='text' class='date' name='sales_to' placeholder='Sales to' /> </div>
						<div class='large-6'>
								<input type='checkbox' name='fb_share' onclick="addSocialButton('facebook', this);" /> Facebook Share (use {facebook_button}) </div>
						<div class='large-4'>
								<input type='text' name='fb_link' placeholder='Link to Share' /> </div>
						<div class='large-4'>
								<textarea name='fb_text' placeholder='Text to Share'></textarea>
						</div>
						<div class='large-6'>
								<input type='checkbox' name='tw_share' onclick="addSocialButton('twitter', this);" /> Twitter Share  (use {twitter_button})</div>
						<div class='large-4'>
								<input type='text' name='tw_link' placeholder='Link to Share' /> </div>
						<div class='large-4'>
								<textarea name='tw_text' placeholder='Text to Share'></textarea>
						</div>
						<div class='large-6'>
								<input type='checkbox' name='pin_share' onclick="addSocialButton('pinterest', this);" /> Pinterest Share (use {pinterest_button}) </div>
						<div class='large-4'>
								<input type='text' name='pin_link' placeholder='Link to Share' /> </div>
						<div class='large-4'>
								<input type='text' name='pin_image' placeholder='Link to Image' /> </div>
						<div class='large-4'>
								<textarea name='pin_text' placeholder='Text to Share'></textarea>
						</div>
						<div class='large-4'>
							<input type='text' name='min_followers' placeholder='At least X followers' />
						</div>
						<div class='large-4'>
							<input type='text' name='max_followers' placeholder='At most X followers' />
						</div>

					<textarea name="description" id="mission-description"> </textarea>
						<input type='hidden' name="trigger" value='add' />
						<br>
						<br>
						<button type='submit' class='btn-black'>Add</button>
				</form>
		</div>
</div>
