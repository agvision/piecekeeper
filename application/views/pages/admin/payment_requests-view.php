<div class="payment-requests-view content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <?php if ($payment->aproved==1): ?>
        <div class="alert-box success no-image">This payment request was approved</div>
        <?php elseif($payment->aproved==0): ?>
        <div class="alert-box warning no-image">This payment request was not approved yet</div>
        <?php else: ?>
        <div class="alert-box error no-image">This payment request was rejected
            <br>
            <br>"
            <?php echo $payment->reason ?>" </div>
        <?php endif ?>
        <div class="header-stats">
            <?=$title?>
                <?php if($payment->aproved==0) { ?>
                <a href="<?=$this->config->base_url()?>admin/payments/approve/<?=$payment->id?>">
                    <button class="save-profile-black admin-form-button btn-success">Approve</button>
                </a> <a class="btn-danger save-profile-white admin-form-button" href="#" id='reject-coupon'>Reject</a>
                <form id='reject-coupon-form' method='post' action="<?php echo base_url('admin/payments/reject/'.$payment->id) ?>" class="payment-request-view-reject">
                    <textarea placeholder="Reason" name='reason' class="payment-request-view-reject-reason"></textarea>
                    <input type='submit' value='Update' class='btn-success' /> <a href='#' id='cancel-reject' class="payment-request-view-reject-cancel"> Cancel</a> </form>
                <?php } ?> </div>
        <table border="0" class="payment-request-view-payments">
            <tr>
                <td class="payment-request-view-column">Name: </td>
                <td><b><?=$payment->first_name . " " . $payment->last_name?></b>
                </td>
            </tr>
            <tr>
                <td class="payment-request-view-column">Email: </td>
                <td><b><?=$payment->payment_number ?></b>
                </td>
            </tr>
            <tr>
                <td>Request date:</td>
                <td><b><?=$this->users->formatDate($admin['id'], strtotime($payment->created))?></b>
                </td>
            </tr>
            <tr>
                <td>Amount: </td>
                <td><b>$<?=$payment->amount?></b>
                    <?php if ($payment->giftcard): ?> (Giftcard $
                    <?php echo $payment->gift_amount ?>)
                    <?php endif ?> </td>
            </tr>
            <?php if ($payment->giftcard): ?>
            <tr>
                <td>Giftcard Code: </td>
                <td><b><?=$payment->code?></b>
                </td>
            </tr>
            <?php endif ?>
            <?php if ($payment->paypal): ?>
            <tr>
                <td>Paypal Account: </td>
                <td><b><?=$payment->payment_number?></b> (
                    <?php echo $payment->payment_name ?>)</td>
            </tr>
            <?php endif ?>
            <tr>
                <td colspan="2"><a href="<?=$this->config->base_url()?>admin/user/view/<?=$payment->user_id?>">View user profile</a>
                </td>
            </tr>
        </table>
    </div>
</div>
