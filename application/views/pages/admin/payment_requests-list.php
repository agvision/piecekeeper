<div class="payment-requests-list content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats"> Payment Requests
            <?php if(!isset($only) || $only==0) { ?>
            <a href="<?=$this->config->base_url()?>admin/payments?only=1">
                <button class="btn-black admin-form-button">Approved</button>
            </a>
            <?php } if(!isset($only) || $only==1) { ?>
            <a href="<?=$this->config->base_url()?>admin/payments?only=0">
                <button class="btn-black admin-form-button">Waiting</button>
            </a>
            <?php } if(isset($only)) { ?>
            <a href="<?=$this->config->base_url()?>admin/payments">
                <button class="btn-black admin-form-button">All</button>
            </a>
            <?php } ?> </div>
        <br>
        <div id="admin-requests-menu" class="payment-request-admin-menu"> <a href="<?php echo base_url('admin/coupons') ?>" class='btn-success'>Discount Codes</a> <a href="<?php echo base_url('admin/payments') ?>" class='btn-success'>Payment Requests</a> <a href="<?php echo base_url('admin/master_requests') ?>" class='btn-success'>Master Requests</a> <a href="<?php echo base_url('admin/slaves_requests') ?>" class='btn-success'>UnderKeeper Requests</a> </div>
        <br>
        <?php if (count($requests)): ?>
        <form method='post' action="<?php echo base_url('admin/payments/multipleApprove') ?>">
            <table width="100%">
                <thead>
                    <tr class="text-align-left">
                        <th>PieceKeeper Name</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <td>
                            <div class="float-right"> Select All
                                <input type='checkbox' id='select-all' class="no-margin" /> </div>
                        </td>
                    </tr>
                </thead>
                <?php foreach ($requests as $request) { ?>
                <tr>
                    <td>
                        <a href="<?php echo base_url('admin/user/view/'.$request->user_id) ?>">
                            <?=$request->first_name . " " . $request->last_name?></a>
                    </td>
                    <td>$
                        <?=$request->amount ?>
                            <?php if ($request->giftcard): ?> (Giftcard $
                            <?php echo $request->gift_amount ?>)
                            <?php endif ?> </td>
                    <td>
                        <?php if ($request->aproved == 1): ?> Approved
                        <?php elseif($request->aproved == 0): ?> Pending
                        <?php else: ?> Rejected
                        <?php endif ?> </td>
                    <td> <a href="<?=$this->config->base_url()?>admin/payments/view/<?=$request->id?>">View</a>
                        <?php if($request->aproved==0) { ?> | <a href="<?=$this->config->base_url()?>admin/payments/approve/<?=$request->id?>">Approve</a>
                        <input type='checkbox' value="<?php echo $request->id ?>" class='request-checkbox no-margin float-right' name='requests[]' />
                        <?php } ?> </td>
                </tr>
                <?php } ?> </table>
            <input type='submit' value='Approve' class='btn-black float-right payment-request-approve' /> </form>
        <?php else: ?>
        <div class='form-label no-mt'>There are no Payment Requests.</div>
        <?php endif ?> </div>
</div>
