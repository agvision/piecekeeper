<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		CKEDITOR.replace('content');

		/**
		 * Hide countries dropdown if the email os a parent email.
		 */
		if($("#parent_select").val() == "null")
		{
			$("#countries").hide();
		}

		/**
		 * Display countries dropdown if the email is not a Parent email (english version)
		 */
		$("#parent_select").change(function()
		{
			if($("#parent_select").val() == "null")
			{
				$("#countries").hide();
				$("#countries option:selected").removeAttr("selected");
			}
			else
			{
				$("#countries").show();
			}
		});

	});
</script>

<div class="content-bottom">
      <?php $this->load->view('pages/admin/menu') ?>

        <div class="column-right">

        	<div class="header-stats">Edit Template</div>

			<form action="<?php echo base_url('admin/email_templates/edit').'/'.$template->id ?>" method='post'>

				<label for="title">Email subject:</label>
				<input id="title" type='text' name='name' placeholder='name' value="<?php echo $template->name; ?>" />

				<label for="parent_select">Parent email template:</label>
				<select id="parent_select" name="parent_id">
					<option value="null" <?php if($template->parent_id == null) echo 'selected'; ?>></option>
					<?php
						foreach ($parent_emails as $email)
						{
							?>
								<option value="<?= $email->id ?>" <?php if($template->parent_id == $email->id) echo 'selected'; ?>> <?= $email->name ?> </option>
							<?php
						}
					?>
				</select>

				<div id="countries" >
					<label for="countries_select">Countries attributed to the email:</label>
					<select id="countries_select" name="countries[]" multiple  size="10">

						<?php
							foreach($countries as $country)
							{
								?>
									<option value="<?= $country->country_id ?>" <?php if(in_array($country->country_id, $email_countries)) echo 'selected'; ?> > <?= $country->short_name ?> </option>
								<?php
							}
						?>
					</select>
				</div>

				<textarea id='content' name='content'><?php echo $template->content ?></textarea>
				<input type='hidden' name='trigger' value='edit' />
				<br>
				<button class='btn-success' type='submit'>Update</button>
			</form>

		</div>
</div>
