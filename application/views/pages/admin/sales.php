    <div class="content-bottom">        
      <?php $this->load->view('pages/admin/menu') ?>

        <div class="column-right">

            <div class="header-stats">
                Sales
            </div>

            <table style='table-layout: fixed;'>
                <thead>
                    <th width="80">ID</th>
                    <th width="180">Customer</th>
                    <th width="180">PieceKeeper</th>
                    <th width="145">Code</th>
                    <th width="80">Total</th>
                    <th width="">Created</th>
                </thead>
                <tbody>
                    <?php foreach ($sales as $s): ?>
                    <tr>
                        <td><?php echo $s->order_id ?></td>
                        <td>
                            <a href="mailto:<?php echo $s->email ?>"><?php echo $s->shipping_first_name.' '.$s->shipping_last_name ?></a> 
                        </td>
                        <td>
                            <a href="<?php echo base_url('/admin/user/view/'.$s->id_user) ?>"><?php echo $s->first_name.' '.$s->last_name ?></a>
                        </td>
                        <td><?php echo $s->coupon ?></td>
                        <td>$<?php echo $s->total ?></td>
                        <td><?php echo $this->users->formatDate($admin['id'], strtotime($s->created)) ?></td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>



            <br><br><br>            

        </div>

    </div>