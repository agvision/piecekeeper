<div class="content-bottom">
  <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
      <div class="header-stats clearfix">
          <?php echo $title ?>
      </div>
      <table class="admin-table">
        <thead>
          <th>Email</th>
          <th>Sender</th>
          <th>Receivers</th>
          <th>Sent</th>
        </thead>
        <tbody>
          <?php foreach ($emails as $e): ?>
              <tr>
                  <td><a href="<?php echo base_url('admin/emails_report/view/'.$e->id) ?>"><?php echo $e->subject ?></a></td>
                  <td>
                    <?php if ($admin['level'] == 3): ?>
                      <a href="<?php echo base_url('admin/admins') ?>"><?php echo $e->sender ?></a>
                    <?php else: ?>
                      <?php echo $e->sender ?>
                    <?php endif ?>
                  </td>
                  <td><?php echo $e->receivers ?></td>
                  <td><?php echo $this->users->formatDate($admin['id'], strtotime($e->created), true) ?></td>
              </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
  </div>