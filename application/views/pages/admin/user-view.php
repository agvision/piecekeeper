<script src="<?php echo base_url('js/lightbox-2.6.min.js') ?>"></script>
<script>
    $(document).ready(function() {
        $('.user-tag').click(function() {
            var tag = $(this).attr('title');
            var id_user = <?php echo $user['id'] ?> ;
            var status;
            if($(this).hasClass('user-tag-selected')) {
                status = "selected";
            } else {
                status = "default";
            }
            if(status == "selected") {
                removeTag(id_user, tag);
                $(this).removeClass('user-tag-selected');
                $(this).addClass('user-tag-default');
            } else {
                addTag(id_user, tag);
                $(this).removeClass('user-tag-default');
                $(this).addClass('user-tag-selected');
            }
        });
        $('#apply-sales-points').click(function() {
            var id_user = <?php echo $user['id'] ?> ;
            var value = $('#edit-sales-points-input').val();
            var old_value = $('#sales-points-value').attr('data-old-value');
            editSalesPoints(value, old_value, id_user);
        });
        $('#apply-bonus-points').click(function() {
            var id_user = <?php echo $user['id'] ?> ;
            var value = $('#edit-bonus-points-input').val();
            var old_value = $('#bonus-points-value').attr('data-old-value');
            editBonusPoints(value, old_value, id_user);
        });
        $('#apply-commission').click(function() {
            var id_user = <?php echo $user['id'] ?> ;
            var value = $('#edit-commission-input').val();
            var old_value = $('#commission-value').attr('data-old-value');
            var pass = $('#edit-commission-password').val();
            editCommission(value, old_value, id_user, pass);
        });
        $('#apply-earnings').click(function() {
            var id_user = <?php echo $user['id'] ?> ;
            var value = $('#edit-earnings-input').val();
            var old_value = $('#earnings-value').attr('data-old-value');
            var pass = $('#edit-earnings-password').val();
            editEarnings(value, old_value, id_user, pass);
        });

        // On focus on register link input, select all the text
        $('#register-link').on('focus', function (e) {
            $(this).one('mouseup', function () {
                $(this).select();
                return false;
            }).select();
        });


        // toggle comments list
        var comments = false;
        $("#show-comments").click(function() {
            if(comments) {
                $(".pk-comments .expand").slideUp(500, function() {
                    comments = false;
                    $("#show-comments").html("Show Comments (<?php echo count($comments) ?>)");
                });
            } else {
                $(".pk-comments .expand").slideDown(500, function() {
                    comments = true;
                    $("#show-comments").html("Hide Comments");
                });
            }
        });
    })

    function editSalesPoints(value, old_value, id_user) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('/admin/user/edit_sales_points') ?>",
            data: 'value=' + value + '&old_value=' + old_value + '&id_user=' + id_user,
            success: function(data) {
                if(data.trim() == 'success') {
                    $('#sales-points-value').html(value);
                    $('#sales-points-value').attr('data-value', value);
                    $('#sales-points-value').attr('data-old-value', value);
                }
                $('.fade-background').hide();
                $('#edit-sales-points-wrapper').hide();
            }
        });
    }

    function editBonusPoints(value, old_value, id_user) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('/admin/user/edit_bonus_points') ?>",
            data: 'value=' + value + '&old_value=' + old_value + '&id_user=' + id_user,
            success: function(data) {
                if(data.trim() == 'success') {
                    $('#bonus-points-value').html(value);
                    $('#bonus-points-value').attr('data-value', value);
                    $('#bonus-points-value').attr('data-old-value', value);
                }
                $('.fade-background').hide();
                $('#edit-bonus-points-wrapper').hide();
            }
        });
    }

    function editCommission(value, old_value, id_user, password) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('/admin/user/edit_commission') ?>",
            data: 'value=' + value + '&old_value=' + old_value + '&id_user=' + id_user + '&password=' + password,
            success: function(data) {
                if(data.trim() == 'success') {
                    $('#commission-value').html(value);
                    $('#commission-value').attr('data-value', value);
                    $('#commission-value').attr('data-old-value', value);
                    $('.fade-background').hide();
                    $('#edit-commission-wrapper').hide();
                    $('.invalid-password').hide();
                } else if(data == 'invalid value') {
                    $('.fade-background').hide();
                    $('#edit-commission-wrapper').hide();
                    $('.invalid-password').hide();
                } else if(data == 'invalid password') {
                    $('.invalid-password').show();
                    $('#edit-commission-password').val('')
                }
            }
        });
    }

    function editEarnings(value, old_value, id_user, password) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('/admin/user/edit_earnings') ?>",
            data: 'value=' + value + '&old_value=' + old_value + '&id_user=' + id_user + '&password=' + password,
            success: function(data) {
                if(data.trim() == 'success') {
                    $('#earnings-value').html(value);
                    $('#earnings-value').attr('data-value', value);
                    $('#earnings-value').attr('data-old-value', value);
                    $('.fade-background').hide();
                    $('#edit-earnings-wrapper').hide();
                    $('.invalid-password').hide();
                } else if(data == 'invalid value') {
                    $('.fade-background').hide();
                    $('#edit-earnings-wrapper').hide();
                    $('.invalid-password').hide();
                } else if(data == 'invalid password') {
                    $('.invalid-password').show();
                    $('#edit-earnings-password').val('')
                }
            }
        });
    }

    function addTag(id_user, tag) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('/admin/user/add_tag') ?>",
            data: 'id_user=' + id_user + '&tag=' + tag,
            success: function(data) {}
        });
    }

    function removeTag(id_user, tag) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('/admin/user/remove_tag') ?>",
            data: 'id_user=' + id_user + '&tag=' + tag,
            success: function(data) {}
        });
    }
</script>
<div class="user-view content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <?php if($user[ 'deleted']==1) { ?>
        <br>
        <div class="alert-box error"><b><?=$user['first_name'] . " " . $user['last_name']?></b> is blocked.</div>
        <br>
        <br>
        <?php } ?>
        <?php if (isset($master) && $user[ 'level'] < 1): ?>
        <div class='form-label user-view-master'> This user is connected to
            <a href="<?php echo base_url('admin/user/view/'.$master['id']) ?>">
                <?php echo $master[ 'first_name']. ' '.$master[ 'last_name'] ?>
            </a>. (<a class="unlink-pk" href="<?php echo base_url('admin/user/unlink_pk/'.$user['id'].'/'.$master['id']) ?>">unlink</a>) </div>
        <?php endif ?>
        <div class="user-view-admin-actions">
            <?php if ($user[ 'is_active']==0 || $user[ 'is_active']==-1 ): ?>
            <a id="approve-btn" href="<?=$this->config->base_url()?>admin/user/approve/<?=$user['id']?>">
                <button class="btn-black">Approve</button>
            </a>
            <?php endif ?>
            <?php if ($user[ 'is_active']==0 ): ?>
            <a href="<?=$this->config->base_url()?>admin/user/disapprove/<?=$user['id']?>">
                <button class="btn-black">Disapprove</button>
            </a>
            <?php endif ?>
            <?php if($user[ 'level'] < 2 ) { ?>
            <a href="<?=$this->config->base_url()?>admin/user/change_level/<?=$user['id']?>">
                <button class="btn-white">
                    <?=($user[ 'level']==1 ? "Downgrade to PieceKeeper" : "Upgrade to Master PieceKeeper")?>
                </button>
            </a>
            <?php } ?>
            <?php if ($admin[ 'level']>= 3): ?> <a href="<?php echo base_url('admin/user/delete/'.$user['id']) ?>" class='btn-danger save-profile-black admin-form-button delete-item user-view-admin-actions-btn'>Delete</a>
            <?php endif ?>
            <?php if ($user[ 'deleted']==1 ): ?> <a href="<?php echo base_url('admin/user/unblock/'.$user['id']) ?>" class='btn-success save-profile-black admin-form-button user-view-admin-actions-btn'>Unblock</a>
            <?php else: ?> <a href="<?php echo base_url('admin/user/block/'.$user['id']) ?>" class='btn-danger save-profile-black admin-form-button user-view-admin-actions-btn'>Block</a>
            <?php endif ?> </div>
        <?php if ($user[ 'is_active']==0 ): ?>
        <select id="pk-starting-level" class="user-view-pk-starting-level">
            <option value="-2" <?php if ($starting_level===-2) echo "selected='selected'" ?>>PieceKeeper Level 1</option>
            <option value="-1" <?php if ($starting_level===-1) echo "selected='selected'" ?>>PieceKeeper Level 2</option>
            <option value="0" <?php if ($starting_level===0 ) echo "selected='selected'" ?>>PieceKeeper Level 3</option>
        </select>
        <?php endif ?>
        <?php if ($user[ 'level'] < 1 && !isset($master) ): ?>
        <div id="link-to-master">Link to Master PieceKeeper </div>
        <form class="display-none" id="link-to-master-form" action="<?php echo base_url('admin/user/link_to_master/'.$user['id']) ?>" method="post">
            <input name="email" type="email" placeholder="Master PieceKeeper Email" />
            <button type="submit">Link</button>
        </form>
        <?php endif ?>
        <div class='ranking-wrapper user-view-ranking-wrapper'>
            <div class='ranking-item'>
                <div class='ranking-top'>
                    <div class='ranking-value'>
                        <?php echo $global_ranking ?>
                    </div>
                    <div class='ranking-label'>GLOBAL RANK</div>
                </div>
                <div class="ranking-bottom <?php echo $global_ranking_progress>=0 ? 'ranking-success' : 'ranking-danger' ?>">
                    <div class='ranking-progress'>
                        <?php if ($global_ranking_progress> 0): ?> Up
                        <?php echo $global_ranking_progress ?> last month
                        <?php elseif($global_ranking_progress==0 ): ?> Same as last month
                        <?php else: ?> Down
                        <?php echo (-1)*intval($global_ranking_progress) ?> last month
                        <?php endif ?> </div>
                </div>
            </div>
            <div class='ranking-item'>
                <div class='ranking-top'>
                    <div class='ranking-value'>
                        <?php echo $country_ranking ?>
                    </div>
                    <div class='ranking-label'>COUNTRY RANK</div>
                </div>
                <div class="ranking-bottom <?php echo $country_ranking_progress>=0 ? 'ranking-success' : 'ranking-danger' ?>">
                    <div class='ranking-progress'>
                        <?php if ($country_ranking_progress> 0): ?> Up
                        <?php echo $country_ranking_progress ?> last month
                        <?php elseif($country_ranking_progress==0 ): ?> Same as last month
                        <?php else: ?> Down
                        <?php echo (-1)*intval($country_ranking_progress) ?> last month
                        <?php endif ?> </div>
                </div>
            </div>
            <div class='ranking-item'>
                <div class='ranking-top'>
                    <div class='ranking-value'>
                        <?php echo $city_ranking ?>
                    </div>
                    <div class='ranking-label'>CITY RANK</div>
                </div>
                <div class="ranking-bottom <?php echo $city_ranking_progress>=0 ? 'ranking-success' : 'ranking-danger' ?>">
                    <div class='ranking-progress'>
                        <?php if ($city_ranking_progress> 0): ?> Up
                        <?php echo $city_ranking_progress ?> last month
                        <?php elseif($city_ranking_progress==0 ): ?> Same as last month
                        <?php else: ?> Down
                        <?php echo (-1)*intval($city_ranking_progress) ?> last month
                        <?php endif ?> </div>
                </div>
            </div>
        </div>
        <div class="user-view-total-stats">Total Stats</div>
        <div class="top-points-board">
            <div class="top-cell">
                <div class='edit-value-button' id='edit-sales-points-button'>+</div>
                <div class='edit-value-wrapper' id='edit-sales-points-wrapper'>
                    <div class='edit-value-label'>Edit Sales Points</div>
                    <input type='text' id='edit-sales-points-input' />
                    <div class='front-btn' id='apply-sales-points'>APPLY</div>
                </div>
                <div class="cell-text" id='sales-points-value' data-old-value="<?php echo intval($total_stats['total_points']) ?>" data-value="<?php echo intval($total_stats['total_points']) ?>">
                    <?=intval($total_stats[ 'total_points'])?>
                </div>
                <div class="cell-sub-text">SALE POINTS</div>
            </div>
            <div class="top-cell">
                <div class='edit-value-button' id='edit-bonus-points-button'>+</div>
                <div class='edit-value-wrapper' id='edit-bonus-points-wrapper'>
                    <div class='edit-value-label'>Edit Bonus Points</div>
                    <input type='text' id='edit-bonus-points-input' />
                    <div class='front-btn' id='apply-bonus-points'>APPLY</div>
                </div>
                <div class="cell-text" id='bonus-points-value' data-old-value="<?php echo intval($total_stats['bonus_points']) ?>" data-value="<?php echo intval($total_stats['bonus_points']) ?>">
                    <?=intval($total_stats[ 'bonus_points'])?>
                </div>
                <div class="cell-sub-text">BONUS POINTS</div>
            </div>
            <div class="top-cell">
                <!-- <div class='edit-value-button' id='edit-sales-button'>+</div> -->
                <div class="cell-text" id='sales-value' data-value="<?php echo intval($total_stats['sales']) ?>">
                    <?=intval($total_stats[ 'sales'])?>
                </div>
                <div class="cell-sub-text">TOTAL SALES</div>
            </div>
            <div class="top-green-cell">
                <div class='edit-value-button' id='edit-earnings-button'>+</div>
                <div class='edit-value-wrapper' id='edit-earnings-wrapper'>
                    <div class='edit-value-label'>Edit Earnings</div>
                    <div class='invalid-password'>Invalid Password</div>
                    <input type='text' id='edit-earnings-input' />
                    <input type='password' placeholder='Password' id='edit-earnings-password' />
                    <div class='front-btn' id='apply-earnings'>APPLY</div>
                </div>
                <div class="cell-text" id='earnings-value' data-old-value="<?php echo $total_stats['earnings'] ?>" data-value="<?php echo $total_stats['earnings'] ?>">
                    <?=$this->currencies->format($total_stats['earnings'])?></div>
                <div class="cell-sub-text">TOTAL EARNINGS</div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="user-view-stats-wrapper">PieceKeeper Stats</div>
        <div class='top-points-board user-stats-board'>
            <div class='top-cell'>
                <div class='cell-text'>
                    <?php echo intval($user_stats->total_sales) ?></div>
                <div class='cell-sub-text'>Sales</div>
            </div>
            <div class='top-cell'>
                <div class='cell-text'>$
                    <?php echo number_format($user_stats->total_earnings, 2) ?></div>
                <div class='cell-sub-text'>Revenue</div>
            </div>
            <div class='top-cell'>
                <div class='cell-text'>
                    <?php echo count($coupons) ?>
                </div>
                <div class='cell-sub-text'>Coupons</div>
            </div>
            <div class='top-cell'>
                <?php if (count($coupons)): ?>
                <div class='cell-text'>
                    <?php echo number_format(intval($user_stats->total_sales) / count($coupons), 2) ?></div>
                <?php else: ?>
                <div class='cell-text'>0</div>
                <?php endif ?>
                <div class='cell-sub-text'>SPC</div>
            </div>
            <div class='top-cell'>
                <?php if (floatval($user_stats->total_earnings) && floatval($commission)): ?>
                <div class='cell-text'>
                    <?php echo number_format(floatval($user_stats->total_earnings) / floatval($commission), 2) ?></div>
                <?php else: ?>
                <div class='cell-text'>0</div>
                <?php endif ?>
                <div class='cell-sub-text'>ROI</div>
            </div>
            <div class='top-green-cell'>
                <div class='edit-value-button' id='edit-commission-button'>+</div>
                <div class='edit-value-wrapper' id='edit-commission-wrapper'>
                    <div class='edit-value-label'>Edit Commission</div>
                    <div class='invalid-password'>Invalid Password</div>
                    <input type='text' id='edit-commission-input' />
                    <input type='password' placeholder='Password' id='edit-commission-password' />
                    <div class='front-btn' id='apply-commission'>APPLY</div>
                </div>
                <div class='cell-text' id='commission-value' data-old-value="<?php echo number_format($commission, 2) ?>" data-value="<?php echo number_format($commission, 2) ?>">$
                    <?php echo number_format($commission, 2) ?>
                </div>
                <div class='cell-sub-text'>Commission</div>
            </div>
            <div class="clear"></div>
        </div>
        <?php if ($user[ 'level']>= 1): ?>
        <div class="user-view-stats-wrapper">UnderKeepers Stats</div>
        <div class='top-points-board user-stats-board'>
            <div class='top-cell'>
                <div class='cell-text'>
                    <?php echo $s_sales ?>
                </div>
                <div class='cell-sub-text'>Sales</div>
            </div>
            <div class='top-cell'>
                <div class='cell-text'>$
                    <?php echo $s_revenue ?>
                </div>
                <div class='cell-sub-text'>Revenue</div>
            </div>
            <div class='top-cell'>
                <div class='cell-text'>
                    <?php echo $s_coupons ?>
                </div>
                <div class='cell-sub-text'>Coupons</div>
            </div>
            <div class='top-cell'>
                <?php if ($s_coupons): ?>
                <div class='cell-text'>
                    <?php echo number_format($s_sales / $s_coupons, 2) ?>
                </div>
                <?php else: ?>
                <div class='cell-text'>0</div>
                <?php endif ?>
                <div class='cell-sub-text'>SPC</div>
            </div>
            <div class='top-cell'>
                <?php if (floatval($s_revenue)): ?>
                <div class='cell-text'>
                    <?php echo number_format(floatval($s_revenue) / floatval($s_commission), 2) ?>
                </div>
                <?php else: ?>
                <div class='cell-text'>0</div>
                <?php endif ?>
                <div class='cell-sub-text'>ROI</div>
            </div>
            <div class='top-green-cell'>
                <div class='cell-text'>$
                    <?php echo $s_commission ?>
                </div>
                <div class='cell-sub-text'>Commission</div>
            </div>
        </div>
        <?php endif ?>

        <?php if ($user['ipad']): ?>
            <div class="user-view-social-following-wrapper">Social Credit:
                <?php echo $social_credit ?>
            </div>
            <br>
        <?php endif ?>

        <div class="user-view-social-following-wrapper">Social Following:
            <?php echo number_format($total_followers) ?>
        </div>
        <div class="followers">
            <div class="followers-box">
                <?php if (isset($fb_friends)): ?> <a href="<?php echo $fb_profile ?>" target='_blank'><img src="<?=base_url('images/facebook_followers.jpg')?>" alt="facebook followers"  /></a>
                <div class='social-friends'>
                    <?php echo $fb_friends ?>
                </div>
                <?php else: ?> <a href="#"><img src="<?=base_url('images/facebook_followers.jpg')?>"  alt="facebook followers" /></a>
                <div class='social-friends'></div>
                <?php endif ?> </div>
            <div class="followers-box">
                <?php if (isset($tw_followers)): ?> <a href="<?php echo $tw_profile ?>" target='_blank'><img src="<?=base_url('images/twitter_followers.jpg')?>"  alt="twitter followers"/></a>
                <div class='social-friends'>
                    <?php echo $tw_followers ?>
                </div>
                <?php else: ?> <a href="#"><img src="<?=base_url('images/twitter_followers.jpg')?>" alt="twitter followers" /></a>
                <div class='social-friends'></div>
                <?php endif ?> </div>
            <div class="followers-box">
                <?php if (isset($inst_followers)): ?> <a href="<?php echo $inst_profile ?>" target='_blank'><img src="<?=base_url('images/instagram_followers.jpg')?>" alt="instagram followers" /></a>
                <div class='social-friends'>
                    <?php echo $inst_followers ?>
                </div>
                <?php else: ?> <a href="#"><img src="<?=base_url('images/instagram_followers.jpg')?>" alt="instagram followers" /></a>
                <div class='social-friends'></div>
                <?php endif ?> </div>
            <div class="followers-box">
                <?php if (isset($tub_followers)): ?> <a href="<?php echo $tub_profile ?>" target='_blank'><img src="<?=base_url('images/tumblr_followers.jpg')?>" alt="tumblr followers" /></a>
                <div class='social-friends'>
                    <?php echo $tub_followers ?>
                </div>
                <?php else: ?> <a href="#"><img src="<?=base_url('images/tumblr_followers.jpg')?>" alt="tumblr followers" /></a>
                <div class='social-friends'></div>
                <?php endif ?> </div>
            <div class="followers-box">
                <?php if (isset($pin_followers)): ?> <a href="<?php echo $pin_profile ?>" target='_blank'><img src="<?=base_url('images/pinterest_followers.jpg')?>" alt="printerest followers"/></a>
                <div class='social-friends'>
                    <?php echo $pin_followers ?>
                </div>
                <?php else: ?> <a href="#"><img src="<?=base_url('images/pinterest_followers.jpg')?>" alt="printerest followers" /></a>
                <div class='social-friends'></div>
                <?php endif ?> </div>
            <div class="followers-box">
                <?php if (isset($ln_followers)): ?> <a href="<?php echo $ln_profile ?>" target='_blank'><img src="<?=base_url('images/in_followers.jpg')?>" alt="in followers" /></a>
                <div class='social-friends'>
                    <?php echo $ln_followers ?>
                </div>
                <?php else: ?> <a href="#"><img src="<?=base_url('images/in_followers.jpg')?>" alt="in followers" /></a>
                <div class='social-friends'></div>
                <?php endif ?> </div>
            <div class="followers-box">
                <?php if (isset($yb_followers)): ?> <a href="<?php echo $yb_profile ?>" target='_blank'><img src="<?=base_url('images/yb_followers.jpg')?>" alt="yb followers" /></a>
                <div class='social-friends'>
                    <?php echo $yb_followers ?>
                </div>
                <?php else: ?> <a href="#"><img src="<?=base_url('images/yb_followers.jpg')?>" alt="yb followers" /></a>
                <div class='social-friends'></div>
                <?php endif ?> </div>
            <div class="followers-box">
                <?php if (isset($vn_followers)): ?> <a href="<?php echo $vn_profile ?>" target='_blank'><img src="<?=base_url('images/vn_followers.jpg')?>" alt="vn followers" /></a>
                <div class='social-friends'>
                    <?php echo $vn_followers ?>
                </div>
                <?php else: ?> <a href="#"><img src="<?=base_url('images/vn_followers.jpg')?>" alt="vn followers" /></a>
                <div class='social-friends'></div>
                <?php endif ?> </div>
            <div class="clear"></div>
            <?php if ($admin[ 'level']==3 ): ?>
            <div class="delete-box">
                <div class="delete-network">
                    <?php if (isset($fb_friends)): ?> <a href="<?php echo base_url('admin/user/delete_social_account/'.$user['id'].'/facebook') ?>">Delete</a>
                    <?php endif ?> </div>
                <div class="delete-network">
                    <?php if (isset($tw_followers)): ?> <a href="<?php echo base_url('admin/user/delete_social_account/'.$user['id'].'/twitter') ?>">Delete</a>
                    <?php endif ?> </div>
                <div class="delete-network">
                    <?php if (isset($inst_followers)): ?> <a href="<?php echo base_url('admin/user/delete_social_account/'.$user['id'].'/instagram') ?>">Delete</a>
                    <?php endif ?> </div>
                <div class="delete-network">
                    <?php if (isset($tub_followers)): ?> <a href="<?php echo base_url('admin/user/delete_social_account/'.$user['id'].'/tumblr') ?>">Delete</a>
                    <?php endif ?> </div>
                <div class="delete-network">
                    <?php if (isset($pin_followers)): ?> <a href="<?php echo base_url('admin/user/delete_social_account/'.$user['id'].'/pinterest') ?>">Delete</a>
                    <?php endif ?> </div>
                <div class="delete-network">
                    <?php if (isset($ln_followers)): ?> <a href="<?php echo base_url('admin/user/delete_social_account/'.$user['id'].'/linkedin') ?>">Delete</a>
                    <?php endif ?> </div>
                <div class="delete-network">
                    <?php if (isset($yb_followers)): ?> <a href="<?php echo base_url('admin/user/delete_social_account/'.$user['id'].'/youtube') ?>">Delete</a>
                    <?php endif ?> </div>
                <div class="delete-network">
                    <?php if (isset($vn_followers)): ?> <a href="<?php echo base_url('admin/user/delete_social_account/'.$user['id'].'/vine') ?>">Delete</a>
                    <?php endif ?> </div>
            </div>
            <?php endif ?>
        </div>

        <?php
            if($user['is_active'] == 0)
            {
                ?>
                <div class='user-view-register-link'>
                    Social Registration URL
                </div>
                <div>
                    <input id="register-link" type="text" value="<?php echo base_url() . 'social?hash=' . $user['hash'] ?>" readonly="readonly" />
                </div>
                <?php
            }
        ?>

        <div class="user-view-tags">PieceKeeper Tags</div>
        <div class='user-tags-wrapper'>
            <?php foreach ($all_user_tags as $tag): ?>
            <?php if (in_array(str_replace( ' ' , '', $tag), $user_tags)): ?>
            <div class='user-tag user-tag-selected' title="<?php echo $tag ?>">
                <?php echo $tag ?>
            </div>
            <?php else: ?>
            <div class='user-tag user-tag-default' title="<?php echo $tag ?>">
                <?php echo $tag ?>
            </div>
            <?php endif ?>
            <?php endforeach ?> </div>

        <div class="pk-comments">
            <div class="show" id="show-comments">Show Comments (
                <?php echo count($comments) ?>)</div>
            <div class="expand">
                <div class="list">
                    <?php foreach ($comments as $c): ?>
                    <div class="item">
                        <div class="header">
                            <div class="admin">
                                <a href="<?php echo base_url('admin/user/view/'.$c->id_admin) ?>">
                                    <?php echo $c->first_name." ".$c->last_name ?></a>
                                <?php if ($c->email_sent): ?> (email sent)
                                <?php endif ?> </div>
                            <div class="created">
                                <?php echo date( "d M Y H:i:s", strtotime($c->created)) ?></div>
                        </div>
                        <?php if ($c->delete): ?>
                        <div class="delete"> <a href="<?php echo base_url('admin/user/delete_comment/'.$c->id) ?>" class="delete-item">Delete</a> </div>
                        <?php endif ?>
                        <div class="value">
                            <?php echo $c->comment ?></div>
                    </div>
                    <?php endforeach ?> </div>
                <form action="<?php echo base_url('admin/user/add_comment/'.$user['id']) ?>" method="post">
                    <textarea name="comment"></textarea>
                    <button type="submit">Add</button>
                    <div class="comment-send-mail">
                        <label>
                            <input type="checkbox" name="comment-send-mail" value="send-mail" />Send mail</label>
                    </div>
                </form>
            </div>
        </div>
        <div id="tabs" class="user-view-tabs">
            <ul>
                <li><a href="#tabs-1">Profile</a>
                </li>
                <li><a href="#tabs-2">Payment Req</a>
                </li>
                <li><a href="#tabs-3">Coupons</a>
                </li>
                <li><a href="#tabs-5">Orders</a>
                </li>
                <?php if($user[ 'level']>=1) { ?>
                <li><a href="#tabs-4">UnderKeeper Req</a>
                </li>
                <li><a href="#tabs-6">UnderKeepers</a>
                </li>
                <?php } ?> </ul>
            <div id="tabs-1">
              <?php if ($this->session->flashdata('picture_upload_false')): ?>
                <div class="form-alert-wrapper no-mb">
                  <div class="alert-box error form-alert">Please upload a valid image (size < 2MB and format: .gif, .jpg, .jpeg, .png).</div>
                </div>
              <?php endif ?>

                <div class="header-stats">
                    <?=$title?> <a href="<?=base_url('admin/user/edit/'.$user['id'])?>" class="btn-black admin-form-button color-white">Edit</a> <a href="<?=base_url('auth/surf/'.$user['hash'])?>" class="btn-black admin-form-button color-white">Surf Profile</a> <a href="<?=base_url('admin/user/notifications/'.$user['id'])?>" class="btn-black admin-form-button color-white">Notifications</a> </div>
                <div class="profile-picture user-view-profile-picture">
                    <div>
                        <?php if ($user[ 'profile_pic']): ?> <a class="max-width-none" href="<?=base_url('images/profiles/big/'.$user['profile_pic'])?>" data-lightbox="image-1">
                      <img  src="<?=base_url('images/profiles/'.$user['profile_pic'])?>"  alt="profile picture"/>
                    </a>
                        <?php else: ?> <img src="<?=base_url('images/profile_picture2.jpg')?>" alt="profile picture" />
                        <?php endif ?> </div>
                    <div class="user-view-profile-picture-2">
                        <?php if ($user[ 'profile_pic_2']): ?> <a href="<?=base_url('images/profiles/big/'.$user['profile_pic_2'])?>" data-lightbox="image-2">
                      <img  src="<?=base_url('images/profiles/'.$user['profile_pic_2'])?>"  alt="profile picture" />
                    </a>
                        <?php else: ?> <img src="<?=base_url('images/profile_picture2.jpg')?>" alt="profile picture" />
                        <?php endif ?> </div>
                </div>
                <div class='profile-picture-right'>
                    <div class='form-label'> <b><?php echo $user['first_name'].' '.$user['last_name'] ?></b> </div>
                    <div class='form-label'>
                        <?php if ($user[ 'gender']=='0' ): ?> Female,
                        <?php else: ?> Male,
                        <?php endif ?>
                        <?php echo $age ?> years </div>
                    <div class='form-label'>
                        <?php echo $user[ 'country']. ', '; if($user[ 'state']){echo $user[ 'state']. ', ';}echo $user[ 'city']; ?> </div>
                    <div class="row collapse">
                        <div class="large-4 columns">
                            <?php if ($user[ 'level']>= 1): ?>
                            <div class='form-label'>BONUS PER SLAVE:
                                <?php if (intval($user[ 'bonus_per_slave'])==0 ): ?> Default
                                <?php else: ?>
                                <?php echo $user[ 'bonus_per_slave'] ?>
                                <?php endif ?> </div>
                            <?php else: ?>
                            <div class='form-label'>PieceKeeper LEVEL:
                                <?php echo $user[ 'level'] + 3 ?>
                            </div>
                            <?php endif ?>
                            <div class='form-label'>COMMISSION:
                                <?php if (intval($user[ 'commission'])==0 ): ?> Default
                                <?php else: ?>
                                <?php echo intval($user[ 'commission']) ?>%
                                <?php endif ?> </div>
                            <div class="form-label">DATE JOINED</div>
                            <div class="form-label view-user-details">
                                <?=$this->users->formatDate($admin['id'], strtotime($user['date_joined']), true)?></div>
                            <div class="form-label">LAST LOGIN</div>
                            <div class="form-label view-user-details">
                                <?php $date=$this->users->formatDate($admin['id'], strtotime($user['last_login']), true); if( strpos( $date, "00:00:00" ) ) { echo "Never"; } else { echo $date; } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="pk-sent">
                    <?php if ($package_lvl2): ?>
                        <?php if ($package_lvl2->status == -1): ?>
                            <a class="btn-danger" href="<?php echo base_url('/admin/packages/view/'.$package_lvl2->id) ?>">LEVEL 2 PACKAGE REJECTED</a>
                        <?php elseif ($package_lvl2->status == 0): ?>
                            <a class="btn-success" href="<?php echo base_url('/admin/packages/view/'.$package_lvl2->id) ?>">SEND LEVEL 2 PACKAGE</a>
                        <?php elseif ($package_lvl2->status == 1): ?>
                            <div class="btn-default">LEVEL 2 PACKAGE SENT</div>
                            <?php if ($package_lvl2->order_id): ?>
                                <a class="show-order-details" href="<?php echo base_url('/order/view/'.$package_lvl2->order_id) ?>">Show order details</a>
                            <?php endif ?>
                        <?php endif ?>
                    <?php else: ?>
                        <div class="btn-default">LEVEL 2 PACKAGE NOT SENT</div>
                        <?php if($user['level'] == -1): ?>
                          <a class="btn-success reminder" href="<?= base_url('packages/send_package_reminder/' . $user['id'] . '/2') ?>">SEND REMINDER</a>
                        <?php endif ?>
                    <?php endif ?>
                </div>
                <br>
                <div class="pk-sent">
                    <?php if ($package_lvl3): ?>
                        <?php if ($package_lvl3->status == -1): ?>
                            <a class="btn-danger" href="<?php echo base_url('/admin/packages/view/'.$package_lvl3->id) ?>">LEVEL 3 PACKAGE REJECTED</a>
                        <?php elseif ($package_lvl3->status == 0): ?>
                            <a class="btn-success" href="<?php echo base_url('/admin/packages/view/'.$package_lvl3->id) ?>">SEND LEVEL 3 PACKAGE</a>
                        <?php elseif ($package_lvl3->status == 1): ?>
                            <div class="btn-default">LEVEL 3 PACKAGE SENT</div>
                            <?php if ($package_lvl3->order_id): ?>
                                <a class="show-order-details" href="<?php echo base_url('/order/view/'.$package_lvl3->order_id) ?>">Show order details</a>
                            <?php endif ?>
                        <?php endif ?>
                    <?php else: ?>
                        <div class="btn-default">LEVEL 3 PACKAGE NOT SENT</div>
                        <?php if($user['level'] >= 0): ?>
                          <a class="btn-success reminder" href="<?= base_url('packages/send_package_reminder/' . $user['id'] . '/3') ?>">SEND REMINDER</a>
                        <?php endif ?>
                    <?php endif ?>
                </div>
                <div class="header-stats"> Personal details </div>
                <br />
                <div class='form-label no-mt'> WEBSITE </div>
                <div class="form-label view-user-details">
                    <?=$user[ 'website']?>
                </div>
                <div class="form-label"> ADDRESS </div>
                <div class="form-label view-user-details">
                    <?=$user[ 'address1']?>
                </div>
                <div class="form-label view-user-details">
                    <?=$user[ 'address2']?>
                </div>
                <div class="form-label view-user-details">
                    <?=$user[ 'postcode']?>
                </div>
                <div class="form-label view-user-details">
                    <?=$user[ 'city']?>
                </div>
                <div class="form-label view-user-details">
                    <?=$user[ 'country']?>
                </div>
                <div class="dotted-line"></div>
                <div class="form-label"> DATE OF BIRTH </div>
                <div class="form-label view-user-details">
                    <?=$user[ 'dob']?>
                </div>
                <div class="dotted-line"></div>
                <div class="form-label"> DID YOU GO TO SCHOOL </div>
                <div class="form-label view-user-details">
                    <?=$user[ 'school']?>
                </div>
                <div class="form-label view-user-details">
                    <?=$user[ 'graduation_year']?>
                </div>
                <div class="clear"></div>
                <div class="dotted-line"></div>
                <div class="form-label"> CONTACT INFORMATION </div>
                <div class="form-label view-user-details">
                    <?=$user[ 'email']?>
                </div>
                <div class="form-label view-user-details">(+
                    <?=$user[ 'prefix'] ?>)
                        <?=$user[ 'phone']?>
                </div>
                <div class="dotted-line"></div>
                <div class="clear"></div>
                <div class="header-stats"> Personality questions </div>
                <?php if($user[ 'celebrity'] !="0" && !empty($user[ 'celebrity'])) { ?>
                <div class="form-label"> FAVORITE ARTIST / CELEBRITY </div>
                <div class="form-label view-user-details">
                    <?=$user[ 'celebrity']?>
                </div>
                <div class="clear"></div>
                <div class="dotted-line"></div>
                <?php $personality_q=true; } ?>
                <?php if($user[ 'food'] !="0" && !empty($user[ 'food']) ){ ?>
                <div class="form-label"> FAVORITE FOOD </div>
                <div class="form-label view-user-details">
                    <?=$user[ 'food']?>
                </div>
                <div class="clear"></div>
                <div class="dotted-line"></div>
                <?php $personality_q=true; } ?>
                <?php if($user[ 'activity'] !="0" && !empty($user[ 'activity'])) { ?>
                <div class="form-label"> FAVORITE ACTIVITY </div>
                <div class="form-label view-user-details">
                    <?=$user[ 'activity']?>
                </div>
                <div class="clear"></div>
                <div class="dotted-line"></div>
                <?php $personality_q=true; } ?>
                <?php if($user[ 'island'] !="0" && !empty($user[ 'island'])) { ?>
                <div class="form-label"> ITEM TO BRING TO A DESERTED ISLAND (YOU ARE ALREADY WEARING A ONEPIECE JUMPSUIT) </div>
                <div class="form-label view-user-details">
                    <?=$user[ 'island']?>
                </div>
                <div class="clear"></div>
                <div class="dotted-line"></div>
                <?php $personality_q=true; } ?>
                <?php if($user[ 'tw_description'] !="0" && !empty($user[ 'tw_description'])) { ?>
                <div class="form-label"> ME IN A TWEET </div>
                <div class="form-label view-user-details">
                    <?=$user[ 'tw_description']?>
                </div>
                <div class="dotted-line"></div>
                <div class="clear"></div>
                <?php $personality_q=true; } ?>
                <?php if($user[ 'find_source'] !="0" && !empty($user[ 'find_source'])) { ?>
                <div class="form-label"> WHERE DID YOU HEAR ABOUT THE PIECEKEEPERS? </div>
                <div class="form-label view-user-details">
                    <?=$user[ 'find_source']?>
                </div>
                <div class="dotted-line"></div>
                <div class="clear"></div>
                <?php $personality_q=true; } ?>
                <?php if(!isset( $personality_q ) ) { ?>
                <div class="form-label view-user-details">The user didn't answered any personality question.</div>
                <div class="dotted-line"></div>
                <div class="clear"></div>
                <?php } ?>
                <div class="clear"></div>
                <div class="header-stats"> Questions </div>
                <?php $nr_q=0 ; foreach ($questions as $q) { if($q->answer == "") continue; ?>
                <div class="form-label">
                    <?=$q->question?> </div>
                <div class="form-label view-user-details">
                    <?=$q->answer?></div>
                <div class="dotted-line"></div>
                <div class="clear"></div>
                <?php $nr_q++; } if( $nr_q==0 ) { ?>
                <div class="form-label view-user-details">The user hasn't answered any questions.</div>
                <div class="dotted-line"></div>
                <div class="clear"></div>
                <?php } ?>

                <?php if ($user['pku']): ?>
                    <div class="header-stats"> PKU </div>
                    <div class="form-label view-user-details">
                        <div><?php echo $user['pku_campus_involvement'] ?></div>
                    </div>
                    <div class="dotted-line"></div>
                    <div class="clear"></div>
                <?php endif ?>

                <div class="clear"></div>
            </div>
            <div id="tabs-2">
                <div class="header-stats"> Payment requests from
                    <?=$user[ 'first_name']?>
                        <?=$user[ 'last_name']?> - $
                            <?php echo number_format($unpaid_commission, 2) ?> left </div>
                <table style="width:100%;">
                    <thead>
                        <tr class="text-align-left">
                            <th>Ambasador Name</th>
                            <th>Amount</th>
                            <th>Code</th>
                            <th>Status</th>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($payment_requests as $request) { ?>
                        <tr>
                            <td>
                                <?=$request->first_name . " " . $request->last_name?></td>
                            <td>
                                <?=$request->amount ?>$
                                    <?php if ($request->giftcard): ?> (Giftcard
                                    <?php echo $request->gift_amount ?>$)
                                    <?php endif ?> </td>
                            <td>
                                <?=$request->code?></td>
                            <td>
                                <?=($request->aproved==1 ? "Approved" : "Waiting")?></td>
                            <td><a href="<?=$this->config->base_url()?>admin/payments/view/<?=$request->id?>" class="user-view-link-payments-view">View</a>
                                <?php if($request->aproved==0) { ?> | <a href="<?=$this->config->base_url()?>admin/payments/approve/<?=$request->id?>" class="user-view-link-payments-approve">Approve</a>
                            </td>
                            <?php } ?> </td>
                        </tr>
                        <?php } ?> </tbody>
                </table>
            </div>
            <div id='tabs-3'>
                <div class="header-stats"> Coupons from
                    <?=$user[ 'first_name']?>
                        <?=$user[ 'last_name']?> <a href="<?php echo base_url('admin/coupons/add').'/'.$user['id'] ?>" class='save-profile-black admin-form-button btn-success'>ADD</a> </div>
                <table id="coupons-table">
                    <thead>
                        <tr>
                            <th style="width:29%;">Code</th>
                            <th style="width:13%;">Discount</th>
                            <th style="width:9%;">Status</th>
                            <th style="width:9%;">Used</th>
                            <th style="width:14%;">Created</th>
                            <th style="width:14%;">Expire</th>
                            <td style="width:10%;"></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($coupons as $c): if( $c->type == 3 ) { continue; } ?>
                        <tr>
                            <td>
                                <a href="<?php echo base_url('admin/coupons/view/').'/'.$c->id ?>" class="user-view-link-coupons-view">
                                    <?php echo $c->code ?></a>
                            </td>
                            <td>
                                <?php if ($c->amount && $c->amount != '0.00'): ?>
                                <?php echo $c->amount." ".$c->currency ?>
                                <?php else: ?>
                                <?php echo $c->percent." %" ?>
                                <?php endif ?> </td>
                            <td>
                                <?php if ($c->approved == 1): ?>
                                <?php if (time()> strtotime($c->to)): ?>
                                <div class="user-view-link-coupons-expired">Expired</div>
                                <?php else: ?> <b>Active</b>
                                <?php endif ?>
                                <?php else: ?> Pending
                                <?php endif ?> </td>
                            <td>
                                <?=$c->countOrders?> </td>
                            <td>
                                <?=$c->created?> </td>
                            <td>
                                <?=$c->to?> </td>
                            <td>
                                <?php if ($c->approved == 0) { ?> <a href="<?php echo base_url('admin/coupons/approve/').'/'.$c->id.'/'.$c->id_creator; ?>" class="user-view-link-coupons-approve">Approve</a>
                                <?php } else { ?>
                                <?php if ($c->deleted == 0){ ?> <a href="<?php echo base_url('admin/coupons/disable/').'/'.$c->id.'/'.$c->id_creator; ?>" class="disable-item user-view-link-coupons-disable">Disable</a>
                                <?php } else { ?> <a href="<?php echo base_url('admin/coupons/enable/').'/'.$c->id.'/'.$c->id_creator; ?>" class="enable-item user-view-link-coupons-enable">Enable</a>
                                <?php } } ?> </td>
                        </tr>
                        <?php endforeach ?> </tbody>
                </table>
                <div class="user-view-scratch-card-batches"> Scratch Card Batches <a href="<?php echo base_url('admin/scratch_codes/add').'/'.$user['id'] ?>" class='save-profile-black admin-form-button btn-success'>ADD</a> </div>
                <table>
                    <thead>
                        <tr>
                            <th style="width:200px;">Serial</th>
                            <th style="width:150px;">Discount</th>
                            <th style="width:90px;">Used</th>
                            <td style="width:160px;">Activation Date</td>
                            <td style="width:160px;"></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($scratch_batches as $sb): ?>
                        <tr>
                            <td>
                                <a class="user-view-link-scratch-code-view" href="<?php echo base_url('/admin/scratch_codes/view/'.$sb->batch) ?>">
                                    <?php echo $sb->batch ?></a>
                            </td>
                            <td>
                                <?php echo $scratch_code_discount ?>%</td>
                            <td>
                                <?php echo $sb->used_coupons."/".$sb->all_coupons ?></td>
                            <td>
                                <?php echo date($user_date_format, strtotime($sb->activated)) ?></td>
                            <td>
                                <?php if( $sb->used_coupons == 0) { ?> <a href="<?php echo base_url('admin/coupons/reset_batch/').'/'.$sb->batch.'/'.$user_id; ?>" class="delete-item user-view-link-coupons-reset-batch">Delete</a> </td>
                            <?php } ?> </tr>
                        <?php endforeach ?> </tbody>
                </table>
            </div>
            <div id='tabs-5'>
                <div class="header-stats"> Orders from
                    <?=$user[ 'first_name']?>
                        <?=$user[ 'last_name']?>
                </div>
                <table class="admin-table">
                    <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Total</th>
                            <th>Coupon</th>
                            <th>Shipping Name</th>
                            <th>Created</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($orders as $order): ?>
                        <tr>
                            <td>
                                <?php echo $order->order_id ?></td>
                            <td>
                                <?php echo $order->commission ?></td>
                            <td>
                                <?php echo $order->coupon ?></td>
                            <td>
                                <?php echo $order->shipping_first_name." ".$order->shipping_last_name ?></td>
                            <td>
                                <?php echo $order->created ?></td>
                        </tr>
                        <?php endforeach ?> </tbody>
                </table>
            </div>
            <?php if($user[ 'level']>=1) { ?>
            <div id="tabs-4">
                <div class='header-stats'> UnderKeeper Requests from
                    <?=$user[ 'first_name']?>
                        <?=$user[ 'last_name']?>
                </div>
                <?php if (count($slaves_requests)): ?>
                <table width="100%">
                    <thead>
                        <tr class="text-align-left">
                            <th>Ambasador Name</th>
                            <th>UnderKeepers No</th>
                            <th>Status</th>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($slaves_requests as $request) { ?>
                        <tr>
                            <td>
                                <?=$request->first_name . " " . $request->last_name?></td>
                            <td>
                                <?=$request->slaves ?></td>
                            <td>
                                <?=($request->aproved==1 ? "Approved" : "Waiting")?></td>
                            <td><a href="<?=$this->config->base_url()?>admin/slaves_requests/view/<?=$request->id?>">View</a>
                                <?php if($request->aproved==0) { ?> | <a href="<?=$this->config->base_url()?>admin/slaves_requests/approve/<?=$request->id?>">Approve</a>
                            </td>
                            <?php } ?> </tr>
                        <?php } ?> </tbody>
                </table>
                <?php else: ?> No extra UnderKeepers requests.
                <?php endif ?>
                <?php if (count($slaves)): ?>
                <div class="dotted-line user-view-dotted-line"></div>
                <p>UnderKeepers</p>
                <table>
                    <thead>
                        <th width='150'>Name</th>
                        <th width='250'>Email</th>
                        <th width='200'>Status</th>
                    </thead>
                    <tbody>
                        <?php foreach ($slaves as $slave): ?>
                        <tr>
                            <td>
                                <a class="user-view-admin-user-view" href="<?php echo base_url('admin/user/view/'.$slave->id) ?>">
                                    <?php echo $slave->first_name.' '.$slave->last_name ?></a>
                            </td>
                            <td>
                                <?php echo $slave->email ?></td>
                            <td>
                                <?php if ($slave->is_active): ?> Accepted
                                <?php elseif(!$slave->is_active && $slave->deleted): ?> Rejected
                                <?php else: ?> Pending
                                <?php endif ?> </td>
                        </tr>
                        <?php endforeach ?> </tbody>
                </table>
                <?php endif ?> </div>
            <div id="tabs-6">
                <div class='header-stats'>
                    <?=$user[ 'first_name']?>
                        <?=$user[ 'last_name']?>'s UnderKeepers </div>
                <table>
                    <thead>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Sales($)</th>
                        <th>Commission($)</th>
                    </thead>
                    <tbody>
                        <?php foreach ($slaves as $s): ?>
                        <tr>
                            <td>
                                <a class="user-view-admin-user-view" href="<?php echo base_url('admin/user/view/'.$s->id) ?>">
                                    <?php echo $s->first_name.' '.$s->last_name ?></a>
                            </td>
                            <td>
                                <?php echo $s->email ?></td>
                            <td>
                                <?php echo floatval($s->total_sales) ?></td>
                            <td>
                                <?php echo floatval($s->commission) ?></td>
                        </tr>
                        <?php endforeach ?> </tbody>
                </table>
            </div>
            <?php } ?> </div>
    </div>
    <div class="clear"></div>
</div>
