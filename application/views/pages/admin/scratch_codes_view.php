<div class="content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats">
            <?php echo $title ?> </div>
        <div class="scratch-codes-view-codes">Scratch Codes</div>
        <?php if (count($coupons)): ?>
        <table>
            <thead>
                <th width='200'>Code</th>
                <th width='150'>Status</th>
            </thead>
            <tbody>
                <?php foreach ($coupons as $c): ?>
                <tr>
                    <td>
                        <?php echo $c->coupon ?></td>
                    <td>
                        <?php if ($c->used): ?> Used
                        <?php else: ?> Active
                        <?php endif ?> </td>
                </tr>
                <?php endforeach ?> </tbody>
        </table>
        <?php endif ?>
        <br>
        <br>
        <br> </div>
</div>
