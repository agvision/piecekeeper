<div class="content-bottom">
      <?php $this->load->model('Country'); ?>
      <?php $this->load->view('pages/admin/menu') ?>
        <div class="column-right">

        	<div class="header-stats">Email Templates
        		<a class="btn-black admin-form-button" href="<?php echo base_url('admin/email_templates/add') ?>">ADD</a>
        	</div>

			<table>
				<thead>
					<tr>
						<th style="width:300px">Name</th>
						<th></th>
						<th></th>
					</tr>
				</thead>
			<?php foreach ($templates as $t): ?>
				<tr>
					<td><?php echo $t['name'] ?></td>
					<td>
						<?php if ($t['automatic']): ?>
							<a href="<?php echo base_url('admin/email_templates/edit/').'/'.$t['id']; ?>">Edit</a>
						<?php else: ?>
							<a href="<?php echo base_url('admin/email_templates/edit/').'/'.$t['id']; ?>">Edit</a> |
							<a href="<?php echo base_url('admin/email_templates/delete/').'/'.$t['id']; ?>" class="delete-item">Delete</a>
						<?php endif ?>
					</td>
					<td>
						<?php if ($t['automatic']): ?>
							Automatic
						<?php endif ?>
					</td>
				</tr>
			<?php endforeach ?>
			</table>

		</div>
</div>
