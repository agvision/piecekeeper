<div class="coupon-edit content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats">
            <?php echo $title ?> <a class="btn-danger save-profile-white admin-form-button full-opacity" href="#" id='reject-coupon'>Reject</a>
            <form id='reject-coupon-form' method='post' action="<?php echo base_url('admin/coupons/delete/'.$coupon->id) ?>" class="coupon-edit__reject">
                <textarea placeholder="Reason" name='reason' class="coupon-edit__reject-reason"></textarea>
                <input type='submit' value='Update' class='btn-success' /> <a href='#' id='cancel-reject' class="coupon-edit__reject-reason-cancel"> Cancel</a> </form>
        </div>
        <br>
        <br>
        <?php if ($this->session->flashdata('error') != ''): ?>
        <div class='form-alert-wrapper'>
            <?php echo $this->session->flashdata('error'); ?> </div>
        <br>
        <?php endif ?>
        <?php if ($this->session->flashdata('success') != ''): ?>
        <div class='form-alert-wrapper'>
            <div class='alert-box success form-alert'>
                <?php echo $this->session->flashdata('success'); ?> </div>
        </div>
        <br>
        <?php endif ?>
        <form action="<?=base_url('admin/coupons/edit/'.$coupon->id)?>" method="post" accept-charset="utf-8">
            <div class="row collapse">
                <div class="large-10">
                    <?php if ($coupon->type == 2): ?>
                    <div class="large-5 column">Date</div>
                    <div class="large-5 left">
                        <?php echo form_input( 'date', $this->users->formatDate($admin['id'], strtotime($coupon->created)), 'disabled="disabled"'); ?> </div>
                    <div class="large-5 column">Link</div>
                    <div class="large-5 left coupon-edit-link">
                        <a href="<?php echo $coupon->link ?>">
                            <?php echo $coupon->link ?></a>
                    </div>
                    <div class="large-5 column">Availability (days)</div>
                    <div class="large-5 left">
                        <input type='text' name='valability' placeholder='valability in days' value="<?php echo $coupon->valability ?>" /> </div>
                    <?php endif ?>
                    <div class="large-5 column">Code</div>
                    <div class="large-5 left">
                        <input type="text" name="code" placeholder="code" value="<?php echo $coupon->code ?>" /> </div>
                    <div class="large-5 column">&nbsp</div>
                    <div class="large-5 left">
                        <select id='discount-type'>
                            <option value='percent' selected='selected'>Percent Discount</option>
                            <option value='amount'>Amount Discount</option>
                        </select>
                    </div>
                    <div class="large-5 column">&nbsp</div>
                    <div class="large-5 left">
                        <input class='percent-type' type='text' name='percent' placeholder='percent' />
                        <input class='amount-type' type='text' name='amount' placeholder='amount' />
                        <select class='amount-type' name='currency'>
                            <?php foreach ($currencies as $currency): ?>
                            <option value="<?=$currency->code?>">
                                <?=$currency->code?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <div class="large-5 column">Country</div>
                    <div class="large-5 left">
                        <select name='country'>
                            <option value=''>All Countries</option>
                            <?php foreach ($countries as $c): ?>
                            <option value="<?php echo $c->iso2 ?>" <?php if($c->iso2 == $coupon->country) echo "selected='selected'" ?>>
                                <?php echo $c->short_name ?></option>
                            <?php endforeach ?> </select>
                    </div>
                    <input type='hidden' name='trigger' value='save' />
                    <input type="submit" class="btn-success coupon-edit-submit" value="Update" /> </div>
                <div class="large-3 column"></div>
            </div>
        </form>
    </div>
</div>
