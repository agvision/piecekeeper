<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		CKEDITOR.replace('content');

		/**
		 * Display countries dropdown if the email is not a Parent email (english version)
		 */
		$("#parent_select").change(function()
		{
			if($("#parent_select").val() == "null")
			{
				$("#countries").hide();
			}
			else
			{
				$("#countries").show();
			}
		});
	});
</script>

<div class="content-bottom">
      <?php $this->load->view('pages/admin/menu') ?>
        <div class="column-right">

        	<div class="header-stats">Add Templates</div>

			<form action="<?php echo base_url('admin/email_templates/add') ?>" method='post'>

				<label for="title">Email subject:</label>
				<input id="title" type='text' name='name' placeholder='name' />

				<label for="parent_select">Parent email template:</label>
				<select id="parent_select" name="parent_id">
					<option value="null"></option>
					<?php
						foreach ($parent_emails as $email)
						{
							?>
								<option value="<?= $email->id ?>"> <?= $email->name ?> </option>
							<?php
						}
					?>
				</select>

				<div id="countries" class="hidden">
					<label for="countries_select">Countries attributed to the email:</label>
					<select id="countries_select" name="countries[]" multiple size="10">
						<?php
							foreach ($countries as $country)
							{
								?>
									<option value="<?= $country->country_id ?>"> <?= $country->short_name ?> </option>
								<?php
							}
						?>
					</select>
				</div>

				<textarea id='content' name='content'></textarea>
				<input type='hidden' name='trigger' value='add' />
				<br>
				<button class='btn-black' type='submit'>Add</button>
			</form>

		</div>
</div>
