<div class="master-requests-list content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats">
            <?=$title?>
        </div>
        <br>
        <div id="admin-requests-menu" class="master-req-admin-menu"> <a href="<?php echo base_url('admin/coupons') ?>" class='btn-success'>Discount Codes</a> <a href="<?php echo base_url('admin/payments') ?>" class='btn-success'>Payment Requests</a> <a href="<?php echo base_url('admin/master_requests') ?>" class='btn-success'>Master Requests</a> <a href="<?php echo base_url('admin/slaves_requests') ?>" class='btn-success'>UnderKeeper Requests</a> </div>
        <br>
        <?php if (count($requests)): ?>
        <form method='post' action="<?php echo base_url('admin/master_requests/multipleApprove') ?>">
            <table width="100%">
                <thead>
                    <tr class="text-align-left">
                        <th>PieceKeeper Name</th>
                        <td width='100'></td>
                        <td width='100'>
                            <div class="float-right"> Select All
                                <input type='checkbox' id='select-all' class="no-margin" /> </div>
                        </td>
                    </tr>
                </thead>
                <?php foreach ($requests as $request) { ?>
                <tr>
                    <td>
                        <a href="<?php echo base_url('admin/user/view/'.$request->id) ?>">
                            <?=$request->first_name . " " . $request->last_name?> </a>
                    </td>
                    <td><a href="<?php echo base_url('/admin/master_requests/reject/'.$request->id) ?>">Reject</a>
                    </td>
                    <td>
                        <input type='checkbox' value="<?php echo $request->id ?>" class='request-checkbox no-margin float-right' name='requests[]' /> </td>
                </tr>
                <?php } ?> </table>
            <input type='submit' value='Approve' class='btn-black master-req-approve' /> </form>
        <?php else: ?>
        <div class='form-label no-mt'>There are no Master Requests.</div>
        <?php endif ?> </div>
</div>
