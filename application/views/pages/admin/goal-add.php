<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script>
    $(document).ready(function() {
        CKEDITOR.replace('goal-description');
        jQuery('.date').fdatepicker({
            format: '<?php echo $this->users->getDatepickerFormat($admin["id"]) ?>',
            weekStart: 1
        });
    });

    function changeCitiesByCountry(id_country) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('auth/get_cities_by_country') ?>",
            data: 'id_country=' + id_country,
            dataType: 'json',
            success: function(data) {
                var input = "<select id='city' name='city' >\n";
                input += "<option value=''>All Cities</option>\n";
                $.each(data, function(index, value) {
                    input += "<option value='" + value + "'>" + value + "</option>\n";
                })
                input += "</select>";
                $('#city-wrapper').html(input);
            }
        });
    }
</script>
<div class="goal-add content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <form action="<?php echo base_url('admin/goals/add') ?>" method='post'>
            <div class="header-stats"> Add Goal </div>
            <br />
            <div class="large-4">
                <input type="text" name="title" placeholder="Goal title" value="" /> </div>
            <div class="large-4">
                <input type="text" name="deadline" class="date" placeholder="Deadline" value="" /> </div>
            <div class="large-4">
                <input type="text" name="invited" placeholder="Who will be invited" value="" /> </div>
            <div class='large-4'>
                <input type='text' name='points' placeholder='Total Points required' /> </div>
            <br>
            <?php if ($admin[ 'level']==3 ): ?>
            <div class='large-4'>
                <select name='country' id='country'>
                    <option value=''>All Countries</option>
                    <?php foreach ($countries as $c): ?>
                    <option value="<?php echo $c->country_id ?>">
                        <?php echo $c->short_name ?></option>
                    <?php endforeach ?> </select>
            </div>
            <?php else: ?>
            <div class='large-4'>
                <select name='country' id='country'>
                    <option value=''>Select Country</option>
                    <?php foreach ($countries as $c): ?>
                    <option value="<?php echo $c->country_id ?>">
                        <?php echo $c->short_name ?></option>
                    <?php endforeach ?> </select>
            </div>
            <?php endif ?>
            <?php if ($admin[ 'level']==3 ): ?>
            <div class='large-4' id='city-wrapper'>
                <select name='city'>
                    <option value=''>All Cities</option>
                    <?php foreach ($cities as $c): ?>
                    <option value="<?php echo $c->name ?>">
                        <?php echo $c->name ?></option>
                    <?php endforeach ?> </select>
            </div>
            <?php else: ?>
            <div class='large-4' id='city-wrapper'>
                <select name='city'>
                    <option value=''>Select City</option>
                    <?php foreach ($cities as $c): ?>
                    <option value="<?php echo $c->name ?>">
                        <?php echo $c->name ?></option>
                    <?php endforeach ?> </select>
            </div>
            <?php endif ?>
            <br>
            <div class="large-4">
                <input type="text" name="required_age" placeholder="Required age" value="" /> </div>
            <div class="large-4">
                <input type="text" name="required_date" id="date_for_goals" placeholder="Date for required age" value="" /> </div>
            <textarea name="description" id="goal-description"> </textarea>
            <input type='hidden' name="trigger" value='add' />
            <br>
            <br>
            <button type='submit' class='btn-black'>Add</button>
        </form>
    </div>
</div>
