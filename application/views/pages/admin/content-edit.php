<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		CKEDITOR.replace('body', {
            contentsCss: "<?php echo base_url('/css/style.css') ?>"
        });
        CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
        CKEDITOR.config.allowedContent = true;
	});
</script>

<div class="content-bottom">
      <?php $this->load->view('pages/admin/menu') ?>

        <div class="column-right">

        	<div class="header-stats">Edit Content</div>

			<form action="<?php echo base_url('admin/content/edit').'/'.$content->id ?>" method='post'>
				<input type='text' name='name' placeholder='name' value="<?php echo $content->name; ?>" />
				<textarea id='body' name='body'><?php echo $content->body ?></textarea>
				<input type='hidden' name='trigger' value='edit' />
				<br>
				<button class='btn-success' type='submit'>Update</button>
			</form>

		</div>
</div>
