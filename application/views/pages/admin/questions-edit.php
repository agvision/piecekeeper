<div class="content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats full-width"> Edit Question </div>
        <?php if (validation_errors()): ?>
        <div class='form-alert-wrapper'>
            <?php echo validation_errors( "<div class='alert-box error form-alert'>", "</div>"); ?> </div>
        <?php endif ?>
        <form method='post' action="<?php echo base_url('admin/questions/edit/'.$question_id) ?>">
            <div>
                <input type='text' name='question' placeholder='Your Question' value="<?=$question?>" /> </div>
            <input type='hidden' name='trigger' value='add' />
            <div class="dotted-line questions-dotted-line"></div>
            <button class="btn-success" type="submit">Update</button>
        </form>
    </div>
</div>
