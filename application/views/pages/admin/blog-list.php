    </div>
    <div class="content-bottom">
      <?php $this->load->view('pages/admin/menu') ?>
        <div class="column-right">
          <div class="header-stats clearfix">
              Blog Administration
              <a href="<?=base_url('admin/blogPosts/post')?>" class="btn-success save-profile-black admin-form-button">Add</a>
          </div>
          <table class="admin-table">
            <thead>
              <tr>
                <th width="500">Post Name</th>
                <th>Type</th>
                <th>Edit</th>
              </tr>
            </thead>
            <tbody>
              <?php
                foreach( $entries as $entry )
                {
                  echo '<tr>';
                  echo '<td>' . $entry['name'] . '</td>';

                  echo '<td>';
                  if ($entry['type'] == 1) {
                      echo "Custom";
                  }
                  echo '</td>';

                  echo '<td>' . '<a href="' . base_url('/admin/blogPosts/post/' . $entry['id']) . '">' .
                    '<img src="' . base_url('images/edit.png') . '" alt="edit icon" /></a> </td>';
                  echo '</tr>';
                }
              ?>
            </tbody>
          </table>
        </div>
