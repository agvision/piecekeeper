<div class="content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <?php if($user[ 'is_active']==0) { ?>
        <br>
        <div class="alert-box warning"><b><?=$user['first_name'] . " " . $user['last_name']?></b> is not active. You can make him active by pressing the button bellow.</div>
        <br>
        <br>
        <?php } else { ?>
        <br>
        <div class="alert-box success"><b><?=$user['first_name'] . " " . $user['last_name']?></b> is currently active.</div>
        <br>
        <br>
        <?php } ?>
        <?php if ($user[ 'is_active']==0 ): ?>
        <a href="<?=$this->config->base_url()?>admin/user/change_status/<?=$user['id']?>">
            <button class="btn-black">Approve</button>
        </a>
        <?php endif ?>
        <?php if($user[ 'level']<2) { ?>
        <a href="<?=$this->config->base_url()?>admin/user/change_level/<?=$user['id']?>">
            <button class="btn-white">
                <?=($user[ 'level']==1 ? "Downgrade to PieceKeeper" : "Upgrade to Master PieceKeeper")?>
            </button>
        </a>
        <?php } ?>
        <br>
        <br>
        <div class="header-stats">
            <?=$title?>
        </div>
        <?=form_open( '/profile/save/' . $user[ 'id'], array( 'class'=> 'custom'))?>
            <div class="row collapse">
                <div class="large-4 columns">
                    <?php if ($user[ 'level']>= 1): ?>
                    <div class='form-label'>Bonus per UnderKeeper
                        <br><span class="user-edit-note">* Percentage from UnderKeeper Commission</span>
                    </div>
                    <?php if (intval($user[ 'bonus_per_slave'])==0 ): ?>
                    <input type='text' class='large-3' name='bonus_per_slave' value='' placeholder='Default' />
                    <?php else: ?>
                    <input type='text' class='large-3' name='bonus_per_slave' value='<?php echo $user[' bonus_per_slave '] ?>' />
                    <?php endif ?>
                    <?php else: ?>
                    <div class='form-label'>PieceKeeper Level:</div>
                    <select name='pk_level'>
                        <option value='-2' <?php echo (intval($user[ 'level'])==- 2) ? "selected='selected'" : "" ?>>Level 1</option>
                        <option value='-1' <?php echo (intval($user[ 'level'])==- 1) ? "selected='selected'" : "" ?>>Level 2</option>
                        <option value='0' <?php echo (intval($user[ 'level'])==0 ) ? "selected='selected'" : "" ?>>Level 3</option>
                    </select>
                    <?php endif ?>
                    <div class='form-label'>Commission:</div>
                    <?php if (intval($user[ 'commission'])==0 ): ?>
                    <input name='commission' type='text' placeholder='Default' />
                    <?php else: ?>
                    <input name='commission' type='text' value="<?php echo intval($user['commission']) ?>" />
                    <?php endif ?>
                    <div class="form-label">DATE JOINED</div>
                    <input class="name-input" name="date_joined" type="text" disabled="disabled" value="<?=date('m-d-Y H:i:s',strtotime($user['date_joined']))?>" />
                    <div class="form-label">LAST LOGIN</div>
                    <input class="name-input" name="last_login" type="text" disabled="disabled" value="<?=date('m-d-Y H:i:s',strtotime($user['last_login']))?>" /> </div>
            </div>
            <div class="header-stats"> Personal details </div>
            <br />
            <div class="row collapse">
                <div class="small-2 large-3 column">
                    <input class="name-input" name="first_name" type="text" placeholder="First name" value="<?=$user['first_name']?>" /> </div>
                <div class="small-2 large-3 next-input column">
                    <input class="name-input" name="last_name" type="text" placeholder="Last name" value="<?=$user['last_name']?>" /> </div>
                <div class="small-1 large-1 column"></div>
            </div>
            <div class="small-3">
                <input class="name-input" name="website" type="text" placeholder="Blog / Personal Website" value="<?=$user['website']?>" /> </div>
            <div class="small-1 large-1 column"></div>
            <div class="clear"></div>
            <br>
            <div class="dotted-line"></div>
            <div class="form-label"> YOUR ADDRESS </div>
            <div class="row collapse">
                <div class="large-8 column">
                    <input name="address1" type="text" placeholder="Address line 1" value="<?=$user['address1']?>" /> </div>
            </div>
            <div class="row collapse">
                <div class="large-8 column">
                    <input name="address2" type="text" placeholder="Address line 2" value="<?=$user['address2']?>" /> </div>
            </div>
            <div class="row collapse">
                <div class="large-4 column">
                    <input name="postcode" type="text" placeholder="Postcode" value="<?=$user['postcode']?>" /> </div>
            </div>
            <div class="row collapse">
                <div class="large-2 column">
                    <input name="city" type="text" placeholder="City" value="<?=$user['city']?>" /> </div>
                <div class="large-2 column next-input">
                    <select name='country'>
                        <?php foreach ($countries as $c): ?>
                        <option value="<?php echo $c->country_id.'_'.$c->short_name ?>" <?php echo ($user[ 'country']==$c->short_name) ? "selected='selected'" : "" ?>>
                            <?php echo $c->short_name ?></option>
                        <?php endforeach ?> </select>
                </div>
                <div class="large-1 column"></div>
            </div>
            <div class="dotted-line"></div>
            <div class="form-label"> DATE OF BIRTH </div>
            <div class="row collapse">
                <div class="large-2 column">
                    <input name="dob" type="text" placeholder="Birthdate" value="<?=$user['dob']?>" /> </div>
                <div class="large-2 column next-input">
                    <select name="gender">
                        <option value="-1" disabled>Gender</option>
                        <option value="0" <?=($user[ 'gender']==0 ? 'SELECTED' : '')?> >Female</option>
                        <option value="1" <?=($user[ 'gender']==1 ? 'SELECTED' : '')?> >Male</option>
                    </select>
                </div>
                <div class="large-1 column"></div>
            </div>
            <div class="dotted-line"></div>
            <div class="form-label"> DID YOU GO TO SCHOOL </div>
            <div class="row collapse">
                <div class="large-3 column">
                    <input name="school" type="text" placeholder="Choose school" value="<?=$user['school']?>" /> </div>
                <div class="large-3 column next-input">
                    <input name="graduation_year" type="text" placeholder="Year of graduation" value="<?=$user['graduation_year']?>" /> </div>
                <div class="large-3 column"></div>
            </div>
            <div class="clear"></div>
            <div class="dotted-line"></div>
            <div class="form-label"> CONTACT INFORMATION </div>
            <div class="row collapse">
                <div class="large-3 column">
                    <input name="email" type="text" placeholder="Email" value="<?=$user['email']?>" /> </div>
                <div class="large-3 column next-input">
                    <input name="phone" type="text" placeholder="Phone number" value="<?=$user['phone']?>" /> </div>
                <div class="large-3 column"></div>
            </div>
            <div class="dotted-line"></div>
            <div class="clear"></div>
            <div class="header-stats"> 5 Personality questions </div>
            <div class="form-label"> FAVORITE ARTIST / CELEBRITY </div>
            <div class="large-3">
                <input name="celebrity" type="text" placeholder="e.g.John Doe" value="<?=$user['celebrity']?>" /> </div>
            <div class="clear"></div>
            <div class="dotted-line"></div>
            <div class="form-label"> FAVORITE FOOD </div>
            <div class="large-3">
                <input name="food" type="text" placeholder="e.g.Seafood" value="<?=$user['food']?>" /> </div>
            <div class="clear"></div>
            <div class="dotted-line"></div>
            <div class="form-label"> FAVORITE ACTIVITY </div>
            <div class="large-3">
                <input name="activity" type="text" placeholder="e.g.Couch potato" value="<?=$user['activity']?>" /> </div>
            <div class="clear"></div>
            <div class="dotted-line"></div>
            <div class="form-label"> ITEM TO BRING TO A DESERTED ISLAND (YOU ARE ALREADY WEARING A ONEPIECE JUMPSUIT) </div>
            <div class="large-3">
                <input name="island" type="text" placeholder="e.g.Old Radio" value="<?=$user['island']?>" /> </div>
            <div class="clear"></div>
            <div class="dotted-line"></div>
            <div class="form-label"> IF YOU HAD THE POSIBILLITY TO SAY ONE SENTENCE TO THE FOLLOWING </div>
            <div class="row collapse">
                <div class="small-3 large-2 columns"> <span class="prefix">
                              Jesus
                            </span> </div>
                <div class="small-9 large-10 columns">
                    <input name="jesus" type="text" value="<?=$user['jesus']?>" /> </div>
            </div>
            <div class="row collapse">
                <div class="small-3 large-2 columns"> <span class="prefix">
                              Obama
                            </span> </div>
                <div class="small-9 large-10 columns">
                    <input name="obama" type="text" value="<?=$user['obama']?>" /> </div>
            </div>
            <div class="row collapse">
                <div class="small-3 large-2 columns"> <span class="prefix">
                              Kim Jong Un
                            </span> </div>
                <div class="small-9 large-10 columns">
                    <input name="jongun" type="text" value="<?=$user['jongun']?>" /> </div>
            </div>
            <div class="row collapse">
                <div class="small-3 large-2 columns"> <span class="prefix">
                              Michael Jackson
                            </span> </div>
                <div class="small-9 large-10 columns">
                    <input name="jackson" type="text" value="<?=$user['jackson']?>" /> </div>
            </div>
            <br>
            <input type="submit" name="save" class="btn-success" value="UPDATE PROFILE" />
            <!--<a href="<?php echo base_url('admin/user_edit/'.$user['id'].'?action=starter-pack'); ?>"><input name="starter_pack" class="save-profile-black" value="SEND STARTER PACK" /></a>-->
            <div class="clear"></div>
            </form>
    </div>
</div>
