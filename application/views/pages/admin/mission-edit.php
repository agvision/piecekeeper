<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script>
		$(document).ready(function() {
				CKEDITOR.replace('mission-description');
				jQuery('.date').fdatepicker({
						format: '<?php echo $this->users->getDatepickerFormat($admin["id"]) ?>',
						weekStart: 1
				});
		});

		function changeCitiesByCountry(id_country) {
				$.ajax({
						type: 'POST',
						url: "<?php echo base_url('auth/get_cities_by_country') ?>",
						data: 'id_country=' + id_country,
						dataType: 'json',
						success: function(data) {
								var input = "<select id='city' name='city' >\n";
								input += "<option value=''>All Cities</option>\n";
								$.each(data, function(index, value) {
										input += "<option value='" + value + "'>" + value + "</option>\n";
								})
								input += "</select>";
								$('#city-wrapper').html(input);
						}
				});
		}

		function addSocialButton(network, elem)
		{
			if(elem.checked)
			{
				CKEDITOR.instances['mission-description'].setData(CKEDITOR.instances['mission-description'].getData() + "\n{"+network+"_button}");
				//$('#mission-description').val(');
			}


		}
</script>
<div class="mission-edit content-bottom">
		<?php $this->load->view('pages/admin/menu') ?>
		<div class="column-right">
				<form action="<?php echo base_url('admin/missions/edit/'.$mission->id) ?>" method='post' enctype='multipart/form-data'>
						<div class="header-stats"> Edit Mission </div>
						<?php if (validation_errors()): ?>
						<div class='form-alert-wrapper'>
								<?php echo validation_errors( "<div class='alert-box error form-alert'>", "</div>"); ?> </div>
						<?php endif ?>
						<br />
						<div class="large-4">
								<input type="text" name="name" placeholder="Mission name" value="<?php echo $mission->name ?>" /> </div>
						<div class="large-4">
								<input type="text" name="from" class="date" placeholder="Begin Date" value="<?php echo $this->users->formatDate($admin['id'], strtotime($mission->from)) ?>" />
								<input type="text" name="to" class="date" placeholder="End Date" value="<?php echo $this->users->formatDate($admin['id'], strtotime($mission->to)) ?>" />
								<?php if ($mission->countdown): ?>
								<input type="checkbox" name="countdown" checked="checked" /> Show Countdown
								<?php else: ?>
								<input type="checkbox" name="countdown" /> Show Countdown
								<?php endif ?> </div>
						<div class='large-4'>
								<input type='text' name='points' placeholder='Bonus Points' value='<?php echo $mission->points ?>' /> </div>
						<br>
						<?php if ($mission->image): ?>
						<br> <a href="<?php echo base_url('/images/missions/'.$mission->image) ?>" target='_black'>
							<img src="<?php echo base_url('/images/missions/'.$mission->image) ?>" width="320">
					</a>
						<br>
						<input type="checkbox" name="remove_image" class="mission-edit-remove-image" /> Remove Image
						<?php endif ?>
						<br>
						<div class='large-4'>
								<input type='file' name='image' /> </div>
						<div class='large-4'>
								<textarea name='video' placeholder='Embed Video Code'>
										<?php echo $mission->video ?></textarea>
						</div>
						<br>
						<div class='large-4'>
								<select name='country' id='country'>
										<option value=''>All Countries</option>
										<?php foreach ($countries as $c): ?>
										<option value="<?php echo $c->country_id ?>" <?php if($c->country_id == $mission->country) echo "selected='selected'" ?>>
												<?php echo $c->short_name ?></option>
										<?php endforeach ?> </select>
						</div>
						<div class='large-4' id='city-wrapper'>
								<select name='city'>
										<option value=''>All Cities</option>
										<?php foreach ($cities as $c): ?>
										<option value="<?php echo $c->name ?>" <?php if($c->name == $mission->city) echo "selected='selected'" ?>>
												<?php echo $c->name ?></option>
										<?php endforeach ?> </select>
						</div>
						<br> Target PieceKeepers by Level:
						<div class='admin-label'> * If no level is selected this mission will be available for all PieceKeepers </div>
						<div class='large-4'>
								<input type='checkbox' name='pk_level_1' <?php if($mission->pk_level_1) echo "checked='checked'" ?> /> Level 1 </div>
						<div class='large-4'>
								<input type='checkbox' name='pk_level_2' <?php if($mission->pk_level_2) echo "checked='checked'" ?> /> Level 2 </div>
						<div class='large-4'>
								<input type='checkbox' name='pk_level_3' <?php if($mission->pk_level_3) echo "checked='checked'" ?> /> Level 3 </div>
						<div class='large-4'>
								<input type='checkbox' name='pk_level_m' <?php if($mission->pk_level_m) echo "checked='checked'" ?> /> Master PieceKeeper </div>
						<br>
						<div class='large-6'>
								<input type='checkbox' name='allow_text' <?php if($mission->allow_text) echo "checked='checked'" ?> /> Allow Text Response (use {text_box})</div>
						<div class='large-4'>
								<input type='text' name='text_details' value="<?php echo $mission->text_details ?>" /> </div>
						<div class='large-6'>
								<input type='checkbox' name='allow_link' <?php if($mission->allow_link) echo "checked='checked'" ?> /> Allow Link Response (use {link_box})</div>
						<div class='large-4'>
								<input type='text' name='link_details' value="<?php echo $mission->link_details ?>" /> </div>
						<div class='large-4'>
								<input type='checkbox' name='allow_image' <?php if($mission->allow_image) echo "checked='checked'" ?> /> Allow Image Response </div>
						<div class='large-4'>
								<input type='checkbox' name='sales_goals' <?php if($mission->sales_goals) echo "checked='checked'" ?> /> Sales Goals </div>
						<div class='large-4'>
								<input type='text' name='sales' placeholder='Number of sales' value="<?php echo $mission->sales ?>" /> </div>
						<div class='large-4'>
								<input type='text' name='sales_from' class='date' placeholder='Sales from' value="<?php echo $this->users->formatDate($admin['id'], strtotime($mission->sales_from)) ?>" /> </div>
						<div class='large-4'>
								<input type='text' name='sales_to' class='date' placeholder='Sales to' value="<?php echo $this->users->formatDate($admin['id'], strtotime($mission->sales_to)) ?>" /> </div>
						<div class='large-6'>
								<input type='checkbox' name='fb_share' <?php if($mission->fb_share) echo "checked='checked'" ?> onclick="addSocialButton('facebook', this);" /> Facebook Share (use {facebook_button})</div>
						<div class='large-4'>
								<input type='text' name='fb_link' placeholder='Link to Share' value="<?php echo $mission->fb_link ?>" /> </div>
						<div class='large-4'>
								<textarea name='fb_text' placeholder='Text to Share'>
										<?php echo $mission->fb_text ?></textarea>
						</div>
						<div class='large-6'>
								<input type='checkbox' name='tw_share' <?php if($mission->tw_share) echo "checked='checked'" ?> onclick="addSocialButton('twitter', this);" /> Twitter Share (use {twitter_button}) </div>
						<div class='large-4'>
								<input type='text' name='tw_link' placeholder='Link to Share' value="<?php echo $mission->tw_link ?>" /> </div>
						<div class='large-4'>
								<textarea name='tw_text' placeholder='Text to Share'>
										<?php echo $mission->tw_text ?></textarea>
						</div>
						<div class='large-6'>
								<input type='checkbox' name='pin_share' <?php if($mission->pin_share) echo "checked='checked'" ?> onclick="addSocialButton('pinterest', this);" /> Pinterest Share (use {pinterest_button})</div>
						<div class='large-4'>
								<input type='text' name='pin_link' placeholder='Link to Share' value="<?php echo $mission->pin_link ?>" /> </div>
						<div class='large-4'>
								<input type='text' name='pin_image' placeholder='Link to Image' value="<?php echo $mission->pin_image ?>" /> </div>
						<div class='large-4'>
								<textarea name='pin_text' placeholder='Text to Share'>
										<?php echo $mission->pin_text ?></textarea>
						</div>
						<div class='large-4'>
							<input type='text' name='min_followers' placeholder='At least X followers' value="<?=$mission->min_followers?>" />
						</div>
						<div class='large-4'>
							<input type='text' name='max_followers' placeholder='At most X followers' value="<?=$mission->max_followers?>" />
						</div>
						<textarea name="description" id="mission-description">
								<?php echo $mission->description ?> </textarea>
						<input type='hidden' name="trigger" value='edit' />
						<br>
						<br>
						<button type='submit' class='btn-success'>Update</button>
				</form>
		</div>
</div>
