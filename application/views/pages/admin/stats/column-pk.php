<script type="text/javascript">
$(function () {
    var chart;
    var sorter = '<?php echo $sort ?>';

    $(document).ready(function(){
        $('#pk-stats').highcharts({
            chart: {
                // type: 'column',
                margin: [ 50, 0, 130, 60]
            },
            title: {
                text: "<?=$chart_pk_stats['graph_title']?>"
            },
            xAxis: {
                categories: [
                    <?php echo implode(", ", $chart_pk_stats['keys']); ?>
                ],
                labels: {
                    rotation: -45,
                    align: 'right',
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: '<?php echo $chart_pk_stats['y_axis_label']; ?>'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                formatter: function() {
                    if(sorter == "pk_revenue" || sorter == "pk_commission"){
                        return '<b>'+ this.x +'</b><br/>'+ '<span style="color:'+this.series.color+'">Total Sales</span>: '+'$'+ Highcharts.numberFormat(this.y, 0);
                    } else {
                        return '<b>'+ this.x +'</b><br/>'+ '<span style="color:'+this.series.color+'">Total Sales</span>: '+Highcharts.numberFormat(this.y, 0);
                    }
                }
            },
            series: [{
                name: 'Sales',
                data: [<?php echo implode(", ", $chart_pk_stats['values']); ?>],
                color: '#50B432'
            }]
        });
    });

});

</script>
