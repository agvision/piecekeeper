
<script type="text/javascript">

	$(function () {
        $('#roi-stats').highcharts({
            chart: {
                // type: 'column'
                margin: [ 50, 0, 170, 50]
            },
            title: {
                text: "<?php echo $chart_roi_stats['graph_title'] ?>"
            },
            xAxis: {
                categories: [ <?php echo implode(", ", $chart_roi_stats['keys']); ?> ],
                labels: {
                    rotation: -45,
                    align: 'right',
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: "<?php echo $chart_roi_stats['y_axis_label'] ?>"
                }
            },
            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: {point.y} ({point.percentage:.0f}%)<br/>',
                shared: true
            },
            plotOptions: {
                column: {
                    stacking: 'normal'
                }
            },
            series: [{
                name: 'Commission',
                data: [ <?php echo implode(", ", $chart_roi_stats['commission_values']); ?> ],
                color: '#50B432'

            }, {
                name: 'Sales',
                data: [ <?php echo implode(", ", $chart_roi_stats['sales_values']); ?> ]
            }]
        });
    });

</script>
