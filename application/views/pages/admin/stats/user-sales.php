<script type="text/javascript">
$(function () {
    var chart;

    $(document).ready(function(){
        $('#sales-stats').highcharts({
            chart: {
                // type: 'column',
                mmargin: [ 50, 0, 130, 60]
            },
            title: {
                text: "<?=$chart_sales_stats['graph_title']?>"
            },
            xAxis: {
                categories: [
                    <?php echo implode(", ", $chart_sales_stats['keys']); ?>
                ],
                labels: {
                    rotation: -45,
                    align: 'right',
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: '<?php echo $chart_sales_stats['y_axis_label']; ?>'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        '<span style="color:'+this.series.color+'">Sales</span>: '+ Highcharts.numberFormat(this.y, 1) +
                        ' $';
                }
            },
            series: [{
                name: 'Sales',
                data: [<?php echo implode(", ", $chart_sales_stats['values']); ?>],
                color: "#50B432"
            }]
        });
    });

});

</script>
