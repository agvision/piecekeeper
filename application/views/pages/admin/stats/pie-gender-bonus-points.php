<script type="text/javascript">

$(function () {
    var chart;
    
    $(document).ready(function () {
        
        // Build the chart
        $('#bonus-points-stats').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: '<?php echo $chart_bonus_points_stats["graph_title"] ?>'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                type: 'pie',
                name: '<?php echo $chart_bonus_points_stats["y_axis_label"] ?>',
                data: [
                    
                    {
                        name: 'Males',
                        y: <?php echo $chart_bonus_points_stats['values'][1] ?>,
                        color: '#2f7ed8'
                    },

                    {
                        name: 'Females',
                        y: <?php echo $chart_bonus_points_stats['values'][0] ?>,
                        color: '#E86489'
                    }
                ]
            }]
        });
    });
    
});

</script>