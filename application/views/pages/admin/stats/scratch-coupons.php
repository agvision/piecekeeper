<script type="text/javascript">
$(function () {
    var chart;

    $(document).ready(function(){
        $('#scratch-stats').highcharts({
            chart: {
                // type: 'column',
                mmargin: [ 50, 0, 130, 60]
            },
            title: {
                text: "<?=$chart_scratch_stats['graph_title']?>"
            },
            xAxis: {
                categories: [
                    <?php echo $chart_scratch_stats['x_data']; ?>
                ],
                labels: {
                    rotation: -45,
                    align: 'right',
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: '<?php echo $chart_scratch_stats['y_axis_label']; ?>'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        '<span style="color:'+this.series.color+'"><?php echo $chart_scratch_stats['title']; ?></span>: '+ Highcharts.numberFormat(this.y,  <?php if(isset( $chart_scratch_stats['decimals'])) echo $chart_scratch_stats['decimals']; else echo 1; ?>) +
                        ' <?php echo $chart_scratch_stats['sign']; ?>';
                }
            },
            series: [{
                name: '<?php echo $chart_scratch_stats['title']; ?>',
                data: [<?php echo $chart_scratch_stats['y_data']; ?>],
                color: '#50B432'
            }]
        });
    });

});

</script>
