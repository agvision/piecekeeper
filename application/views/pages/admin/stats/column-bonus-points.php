<script type="text/javascript">
$(function () {
    var chart;

    $(document).ready(function(){
        $('#bonus-points-stats').highcharts({
            chart: {
                // type: 'column',
                margin: [ 50, 0, 130, 60]
            },
            title: {
                text: "<?=$chart_bonus_points_stats['graph_title']?>"
            },
            xAxis: {
                categories: [
                    <?php echo implode(", ", $chart_bonus_points_stats['keys']); ?>
                ],
                labels: {
                    rotation: -45,
                    align: 'right',
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: '<?php echo $chart_bonus_points_stats['y_axis_label']; ?>'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        '<span style="color: '+this.series.color+'">Sales</span>: '+ Highcharts.numberFormat(this.y, 1) +
                        ' $';
                }
            },
            series: [{
                name: 'Sales',
                data: [<?php echo implode(", ", $chart_bonus_points_stats['values']); ?>],
                color: "#50B432"
            }]
        });
    });

});

</script>
