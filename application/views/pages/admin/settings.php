<div class="content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats"> Settings </div>
        <br>
        <form class='form-horizontal compact' id="frm1" action="<?=base_url('admin/settings/save')?>" method="post" accept-charset="utf-8" enctype='multipart/form-data'>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Contact Email:</div>
                <div class='large-4 left'>
                    <input type="email" name="contact_email" value="<?php echo $settings['contact_email'] ?>" /> </div>
            </div>
            <div class='dotted-line'></div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Front Images:</div>
                <div class='large-6 left'>
                    <?php foreach ($front_images as $img): ?>
                    <div class="front-img">
                        <a href="<?=base_url('images/front/'.$img->name)?>" target="_blank" class="link">
                            <?php echo $img->name ?></a> <a href="<?=base_url('admin/settings/delete_front_image/'.$img->id)?>" class="delete-item">Delete</a> </div>
                    <?php endforeach ?>
                    <input type="file" name="front_images[]" multiple/> </div>
            </div>
            <div class='dotted-line'></div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Level 2 Upgrade:</div>
                <div class='large-4 left'>
                    <input type="text" name="level2_upgrade" value="<?php echo $settings['level2_upgrade'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Level 3 Upgrade:</div>
                <div class='large-4 left'>
                    <input type="text" name="level3_upgrade" value="<?php echo $settings['level3_upgrade'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Level Upgrade Networks:</div>
                <div class='large-4 left'>
                    <input type="checkbox" name="fb" <?php if(in_array( "fb", $level_upgrade_networks)) echo "checked='checked'" ?> /> Facebook
                    <br>
                    <input type="checkbox" name="tw" <?php if(in_array( "tw", $level_upgrade_networks)) echo "checked='checked'" ?> /> Twitter
                    <br>
                    <input type="checkbox" name="in" <?php if(in_array( "in", $level_upgrade_networks)) echo "checked='checked'" ?> /> Instagram
                    <br>
                    <input type="checkbox" name="tl" <?php if(in_array( "tl", $level_upgrade_networks)) echo "checked='checked'" ?> /> Tumblr
                    <br>
                    <input type="checkbox" name="pi" <?php if(in_array( "pi", $level_upgrade_networks)) echo "checked='checked'" ?> /> Pinterest
                    <br>
                    <input type="checkbox" name="ln" <?php if(in_array( "ln", $level_upgrade_networks)) echo "checked='checked'" ?> /> LinkedIn
                    <br>
                    <input type="checkbox" name="yb" <?php if(in_array( "yb", $level_upgrade_networks)) echo "checked='checked'" ?> /> YouTube
                    <br>
                    <input type="checkbox" name="vi" <?php if(in_array( "vi", $level_upgrade_networks)) echo "checked='checked'" ?> /> Vine
                    <br> </div>
            </div>
            <div class='dotted-line'></div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Best Performers Days Interval:</div>
                <div class='large-4 left'>
                    <input type="text" name="best_performers_days_interval" value="<?php echo $settings['best_performers_days_interval'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Best Performers List Size:</div>
                <div class='large-4 left'>
                    <input type="text" name="best_performers_list_size" value="<?php echo $settings['best_performers_list_size'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Best Performers Default Points:</div>
                <div class='large-4 left'>
                    <input type="text" name="best_performers_default_points" value="<?php echo $settings['best_performers_default_points'] ?>" /> </div>
            </div>
            <div class='dotted-line'></div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Scratch Code Discount (%):</div>
                <div class='large-4 left'>
                    <input type="text" name="scratch_code_discount" value="<?php echo $settings['scratch_code_discount'] ?>" /> </div>
            </div>
            <div class='dotted-line'></div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">PieceKeeper Tags:</div>
                <div class='large-1 left'>
                    <textarea class="settings-tags" name="user_tags">
                        <?php echo $settings[ 'user_tags'] ?>
                    </textarea>
                </div>
            </div>
            <div class='dotted-line'></div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Level 2 Package Product ID:</div>
                <div class='large-4 left'>
                    <input type="text" name="level2_package" value="<?php echo $settings['level2_package'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Choose Size:</div>
                <div class='large-4 left'>
                    <input type="text" name="level2_package_sizes" value="<?php echo $settings['level2_package_sizes'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Level 3 Package Product ID:</div>
                <div class='large-4 left'>
                    <input type="text" name="level3_package" value="<?php echo $settings['level3_package'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Choose Size:</div>
                <div class='large-4 left'>
                    <input type="text" name="level3_package_sizes" value="<?php echo $settings['level3_package_sizes'] ?>" /> </div>
            </div>
            <div class='dotted-line'></div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column settings-level-3-products-label"> PieceKeepers Level 3 Free Products IDs:
                    <br>
                    <br> (IDs must be comma separated.) </div>
                <div class='large-1 left'>
                    <textarea class="settings-level-3-products-textarea" name="level3_free_products">
                        <?php echo $settings[ 'level3_free_products'] ?>
                    </textarea>
                </div>
            </div>
            <div class='dotted-line'></div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">PieceKeepers Comission Level 1 (%):</div>
                <div class='large-1 left'>
                    <input type="text" name="level1_commission" value="<?php echo $settings['level1_commission'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">PieceKeepers Comission Level 2 (%):</div>
                <div class='large-1 left'>
                    <input type="text" name="level2_commission" value="<?php echo $settings['level2_commission'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">PieceKeepers Comission Level 3 (%):</div>
                <div class='large-1 left'>
                    <input type="text" name="level3_commission" value="<?php echo $settings['level3_commission'] ?>" /> </div>
            </div>
            <div class='dotted-line'></div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column"> Bonus per UnderKeeper (%):
                    <br>
                    <br> (Percentage from PieceKeeper Commission) </div>
                <div class='large-1 left'>
                    <input type="text" name="slave_commission" value="<?php echo $settings['slave_commission'] ?>" /> </div>
            </div>
            <div class='dotted-line'></div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Sales Points Percentage Level 1 (%):</div>
                <div class='large-1 left'>
                    <input type="text" name="sales_points_level1" value="<?php echo $settings['sales_points_level1'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Sales Points Percentage Level 2 (%):</div>
                <div class='large-1 left'>
                    <input type="text" name="sales_points_level2" value="<?php echo $settings['sales_points_level2'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Sales Points Percentage Level 3 (%):</div>
                <div class='large-1 left'>
                    <input type="text" name="sales_points_level3" value="<?php echo $settings['sales_points_level3'] ?>" /> </div>
            </div>
            <div class='dotted-line'></div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Bonus Points at Level 2:</div>
                <div class='large-1 left'>
                    <input type="text" name="bonus_points_level_2" value="<?php echo $settings['bonus_points_level_2'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Bonus Points at Level 3:</div>
                <div class='large-1 left'>
                    <input type="text" name="bonus_points_level_3" value="<?php echo $settings['bonus_points_level_3'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Total Points for Level 2:</div>
                <div class='large-1 left'>
                    <input type="text" name="total_points_level_2" value="<?php echo $settings['total_points_level_2'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Total Points for Level 3:</div>
                <div class='large-1 left'>
                    <input type="text" name="total_points_level_3" value="<?php echo $settings['total_points_level_3'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Sales for Level 2:</div>
                <div class='large-1 left'>
                    <input type="text" name="sales_level_2" value="<?php echo $settings['sales_level_2'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class="large-5 column">Sales for Level 3:</div>
                <div class='large-1 left'>
                    <input type="text" name="sales_level_3" value="<?php echo $settings['sales_level_3'] ?>" /> </div>
            </div>
            <div class='dotted-line'></div>
            <div class='row collapse pb15 clearfix'>
                <div class='large-5 column'>Singleuse Codes per Day:</div>
                <div class='large-1 column left'>
                    <input type="text" name="24h_discounts_per_user" value="<?php echo $settings['24h_discounts_per_user'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class='large-5 column'>Multiuse Codes per Week:</div>
                <div class='large-1 column left'>
                    <input type="text" name="multiuse_codes_per_week" value="<?php echo $settings['multiuse_codes_per_week'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class='large-5 column'>Level for Multiple Use Discount Code:</div>
                <div class='large-3 column left'>
                    <select name='multiple_discount_level'>
                        <option value='-2' <?php echo ($settings[ 'multiple_discount_level']=='-2' ) ? "selected='selected'" : "" ?>>PieceKeeper Level 1</option>
                        <option value='-1' <?php echo ($settings[ 'multiple_discount_level']=='-1' ) ? "selected='selected'" : "" ?>>PieceKeeper Level 2</option>
                        <option value='0' <?php echo ($settings[ 'multiple_discount_level']=='0' ) ? "selected='selected'" : "" ?>>PieceKeeper Level 3</option>
                        <option value='1' <?php echo ($settings[ 'multiple_discount_level']=='1' ) ? "selected='selected'" : "" ?>>Master PieceKeeper</option>
                    </select>
                </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class='large-5 column'>Minimum Commission for Payout ($):</div>
                <div class='large-1 column left'>
                    <input type="text" name="minimum_commission_payout" value="<?php echo $settings['minimum_commission_payout'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class='large-5 column'>Extra Commission for Giftcard Payout (%):</div>
                <div class='large-1 column left'>
                    <input type="text" name="giftcard_extra_commission" value="<?php echo $settings['giftcard_extra_commission'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class='large-5 column'>Payout Requests per Month:</div>
                <div class='large-1 column left'>
                    <input type="text" name="payout_requests_per_month" value="<?php echo $settings['payout_requests_per_month'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class='large-5 column'>Orders Pending Days:</div>
                <div class='large-1 column left'>
                    <input type="text" name="orders_pending_days" value="<?php echo $settings['orders_pending_days'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class='large-5 column'>Single use Coupon Active Time (hours):</div>
                <div class='large-1 column left'>
                    <input type="text" name="singleuse_coupon_active_time" value="<?php echo $settings['singleuse_coupon_active_time'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class='large-5 column'>Multiuse Coupon Active Time (hours):</div>
                <div class='large-1 column left'>
                    <input type="text" name="multiuse_coupon_active_time" value="<?php echo $settings['multiuse_coupon_active_time'] ?>" /> </div>
            </div>
            <div class='row collapse pb15 clearfix'>
                <div class='large-5 column'>Default Discount Percentage (%):</div>
                <div class='large-1 column left'>
                    <input type="text" name="default_discount_percentage" value="<?php echo $settings['default_discount_percentage'] ?>" /> </div>
            </div>
            <div class='dotted-line settings-dotted-line'></div>
            <div class='row collapse pb15 clearfix'>
                <div class='large-5 column'>Followers Decrease Warning (%):</div>
                <div class='large-1 column left'>
                    <input type="text" name="followers_decrease_warning" value="<?php echo $settings['followers_decrease_warning'] ?>" /> </div>
            </div>

            <div class="row collapse pb15 clearfix">
                <input type="submit" class="btn-success save-profile-black settings-save" value="Update" /> </div>
        </form>
        <div class='dotted-line settings-dotted-line'></div>
        <div class='large-5 clear settings-commission-by-country-label'>PieceKeeper Commission by Country:</div>
        <br>
        <table>
            <?php foreach ($countries_commissions as $c): ?>
            <?php if ($c->type == 1): ?>
            <tr>
                <td>
                    <?php echo $c->short_name ?></td>
                <td>PieceKeeper Level
                    <?php echo $c->level+3 ?></td>
                <td>
                    <?php echo $c->value ?> %</td>
                <td><a href="<?php echo base_url('admin/settings/remove_country/1/'.$c->id_country.'/'.$c->level) ?>" class="delete-item">Remove</a>
                </td>
            </tr>
            <?php endif ?>
            <?php endforeach ?> </table> <a href="<?php echo base_url('admin/settings/add_commission') ?>" class='small-btn'>Add Country</a>
        <div class='dotted-line settings-dotted-line'></div>
        <div id="questions" class='large-5 clear settings-questions'>
            <?php if( count($questions)==0 ) { echo "There are no questions."; } else { echo "Questions:"; } ?> </div>
        <br>
        <table>
            <?php foreach ($questions as $q): ?>
            <tr>
                <td class="settings-question">
                    <?php echo $q->question ?></td>
                <td> <a href="<?php echo base_url('admin/questions/edit').'/'.$q->id; ?>">Edit</a> <a href="<?php echo base_url('admin/questions/remove/'.$q->id) ?>" class="delete-item">Remove</a> </td>
            </tr>
            <?php endforeach ?> </table> <a href="<?php echo base_url('admin/questions/add') ?>" class='small-btn'>Add Question</a>
        <div class='dotted-line settings-dotted-line'></div>
        <div class='large-5 clear settings-underkeeper'>Master Bonus per UnderKeeper by Country:
            <br> <span class="settings-underkeeper-note">* Percentage from UnderKeeper Commission</span>
        </div>
        <br>
        <table>
            <?php foreach ($countries_commissions as $c): ?>
            <?php if ($c->type == 2): ?>
            <tr>
                <td>
                    <?php echo $c->short_name ?></td>
                <td>PieceKeeper Level
                    <?php echo $c->level+3 ?></td>
                <td>
                    <?php echo $c->value ?> %</td>
                <td><a href="<?php echo base_url('admin/settings/remove_country/2/'.$c->id_country.'/'.$c->level) ?>" class="delete-item">Remove</a>
                </td>
            </tr>
            <?php endif ?>
            <?php endforeach ?> </table> <a href="<?php echo base_url('admin/settings/add_bonus') ?>" class='small-btn'>Add Country</a>
        <br>
        <br>
        <br> </div>
</div>
