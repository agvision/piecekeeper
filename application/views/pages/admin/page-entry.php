    </div>
    <div class="content-bottom">
      <?php $this->load->view('pages/admin/menu') ?>
      <div class="column-right">
        <?=form_open('admin/page/save')?>
          <input type="hidden" name="id" value="<?=$entry['id']?>" />
          <div class="header-stats">
             Add / Edit Blog Entry
            <input type="submit" class="btn-success save-profile-black admin-form-button" value="Update">
          </div>
          <br />
          <textarea name="body" id="entry_content" >
            <?=$entry['body']?>
          </textarea>
        </form>
        </div>
      </div>
<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script>
  window.onload = function() {
    CKEDITOR.replace('entry_content');
  }
</script>
