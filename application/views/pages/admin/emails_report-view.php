<div class="content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats clearfix">
            <?php echo $title ?> </div>
        <table border="0" class="email-report-view">
            <tr>
                <td class="email-report-view-column"><b>Sender</b> </td>
                <td>
                    <?php if ($admin[ 'level']==3 ): ?>
                    <a href="base_url('admin/admins')">
                        <?=$email->first_name . " " . $email->last_name?></a>
                    <?php else: ?>
                    <?=$email->first_name . " " . $email->last_name?>
                        <?php endif ?> </td>
            </tr>
            <tr>
                <td class="email-report-view-column"><b>Subject</b> </td>
                <td>
                    <?=$email->subject?></td>
            </tr>
            <tr>
                <td><b>Body</b>
                </td>
                <td>
                    <?=$email->body?></td>
            </tr>
        </table>
        <p>Receivers:</p>
        <table class="admin-table">
            <thead>
                <th>Name</th>
                <th>Email</th>
                <th>Country</th>
                <th>City</th>
            </thead>
            <tbody>
                <?php foreach ($receivers as $r): ?>
                <tr>
                    <td>
                        <a href="<?php echo base_url('admin/user/view/'.$r->id) ?>">
                            <?php echo $r->first_name." ".$r->last_name ?></a>
                    </td>
                    <td>
                        <?php echo $r->email ?></td>
                    <td>
                        <?php echo $r->country ?></td>
                    <td>
                        <?php echo $r->city ?></td>
                </tr>
                <?php endforeach ?> </tbody>
        </table>
    </div>
</div>
