<div class="content-bottom">
    <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
        <div class="header-stats clearfix">
            <?=$title?> <a class="btn-success save-profile-black admin-form-button" href="<?php echo base_url('admin/goals/add') ?>">Add</a> </div>
        <?php if (count($goals)): ?>
        <table class="full-width">
            <thead>
                <tr class="text-align-left">
                    <th>Name</th>
                    <th>Invited</th>
                    <th>Points</th>
                    <th>Deadline</th>
                    <th>Country</th>
                    <th>City</th>
                    <td class="goals-list-column"></td>
                </tr>
            </thead>
            <?php foreach ($goals as $g) { ?>
            <tr>
                <td>
                    <?php echo $g->title ?></td>
                <td>
                    <?php echo $g->invited ?></td>
                <td>
                    <?php echo number_format($g->points) ?></td>
                <td>
                    <?php echo $this->users->formatDate($admin['id'], strtotime($g->deadline)) ?></td>
                <td>
                    <?php if (intval($g->country)): ?>
                    <?php echo $g->short_name ?>
                    <?php else: ?>
                    <?php echo "All" ?>
                    <?php endif ?> </td>
                <td>
                    <?php if (strlen($g->city)): ?>
                    <?php echo $g->city ?>
                    <?php else: ?>
                    <?php echo "All" ?>
                    <?php endif ?> </td>
                <td>
                    <?php if ($admin[ 'level']==2 && !$g->country): ?> <a href="<?php echo base_url('admin/goals/view/').'/'.$g->id ?>">View</a>
                    <?php else: ?> <a href="<?php echo base_url('admin/goals/view/').'/'.$g->id ?>">View</a> | <a href="<?php echo base_url('admin/goals/edit/').'/'.$g->id ?>">Edit</a> | <a href="<?php echo base_url('admin/goals/delete/').'/'.$g->id ?>" class='delete-item'>Delete</a>
                    <?php endif ?> </td>
            </tr>
            <?php } ?> </table>
        <?php else: ?>
        <div class='form-label no-mt'>There are no Goals created.</div>
        <?php endif ?> </div>
</div>
