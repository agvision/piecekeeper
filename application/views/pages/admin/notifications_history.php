
<div class="content-bottom">
  <?php $this->load->view('pages/admin/menu') ?>
    <div class="column-right">
      <div class="header-stats">
          Notifications History
      </div>
	        <?php if($show_filters){ ?>
          	 <form action=<?php echo base_url('/admin/notifications_history/page/1')?> method="post">
          			<div >
          			Admin : 
          			<select name="filter_by_admin" id='city' style = "width :200px">

                                <option value=''>All</option>

                                <?php foreach ($admins as $admin): ?>

                                    <option value="<?php echo $admin->id ?>" <?=($filter_by_admin===$admin->id ? "selected='selected'" : "" )?> ><?php echo $admin->first_name.' '.$admin->last_name ?></option>
                                
                                <?php endforeach ?>
                    </select>
                    <input class='small-btn' type="submit" value="Filter" />
                    </div>
          	</form>
          	<?php 	} ?>
      <?php if (empty($notifications)): ?>
          <p>There are no notifications.</p>
      <?php else: ?>
          <?php foreach ($notifications as $n): ?>
              <div class='alert-box'>
                <div class='admin-notification-message'>
                  <a href="<?php echo $n['link'] ?>"><?php echo $n['message'] ?></a>
                </div>
                <div class='admin-notification-date'>
                  <b>Admin:</b> <?php echo $n['admin_name'] ?> <br> <b>Date:</b> <?php echo date('d M Y', strtotime($n['handled'])) ?>
                </div>
              </div>
          <?php endforeach ?>
      <?php endif ?>

      <div class='pagination-wrapper'>
        <div class='pagination-prev-page'>
          <?php if ($current_page > 1): ?>
            <a href="<?php echo base_url('/admin/notifications_history/page/'.($current_page-1)).'/'.$filter_by_admin ?>"><< Prev</a>
          <?php endif ?>
        </div>
        <div class='pagination-next-page'>
          <?php if (count($notifications) == $items_per_page): ?>
            <a href="<?php echo base_url('/admin/notifications_history/page/'.($current_page+1)).'/'.$filter_by_admin ?>">Next >></a>
          <?php endif ?>
        </div>
      </div>
         
    </div>
  </div>