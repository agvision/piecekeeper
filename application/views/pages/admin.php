<div class="admin content-bottom">
    <?php $this->load->view('pages/admin_menu') ?>
    <div class="column-right">
        <div class="top-points-board">
            <div class="top-cell">
                <div class="cell-text">
                    <?php echo $statistics_general[ 'total_ambassadors']; ?>
                </div>
                <div class="cell-sub-text">TOTAL AMBASSADORS</div>
            </div>
            <div class="top-cell">
                <div class="cell-text">100</div>
                <div class="cell-sub-text">TOTAL SALES</div>
            </div>
            <div class="top-cell">
                <div class="cell-text">
                    <?php echo $statistics_general[ 'total_sales']; ?>
                </div>
                <div class="cell-sub-text">TOTAL SALES</div>
            </div>
            <div class="top-green-cell">
                <div class="cell-text">
                    <?php echo $this->currencies->format($statistics_general['total_earnings']); ?></div>
                <div class="cell-sub-text">TOTAL EARNINGS</div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="tabs" class="admin-tabs">
            <ul>
                <li><a href="#tabs-1">Notifications</a>
                </li>
                <li><a href="#tabs-2">Pending Ambasadors</a>
                </li>
                <li><a href="#tabs-3">General Settings</a>
                </li>
            </ul>
            <div id="tabs-1" href='google.ro'>
                <?php foreach ($notifications as $notification) { $details=$notification->getDetails(); if($details['correct']) { ?> <a href="<?=$details['url']?>"><div class='alert-box no-image <?=$details['class']?>'><?=$details['text']?></div></a>
                <?php } } ?> </div>
            <div id="tabs-2" class="admin-tab-2">
                <table>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                    <?php foreach ($pending_ambasadors as $user) { ?>
                    <tr>
                        <td>
                            <?=$user->id?></td>
                        <td>
                            <?=$user->first_name . " " . $user->last_name?></td>
                        <td> <a href='#'>Aprove</a> | <a href='#'>Edit</a> | <a href='#'>Delete</a> </td>
                    </tr>
                    <?php } ?>
                    <tr> </tr>
                </table>
            </div>
            <div id="tabs-3"></div>
        </div>
        <div class="header-stats"> Brand Ambassadors </div> Search:
        <br/>
        <br/>
        <?php echo form_open( '/',array( 'method'=> 'GET')); echo form_input(array('name' => 'search', 'value' => $this->input->get('search'), 'style' => 'width:150px;')); echo form_close(); ?>
        <table class="admin-table">
            <thead>
                <th>Name</th>
                <th>Email</th>
                <th>Level</th>
                <th>&nbsp;</th>
            </thead>
            <tbody>
                <?php $level[ '0']='Brand Ambassador' ; $level[ '1']='Master Brand Ambassador' ; $level[ '2']='Administrator' ; foreach ($users as $user) { echo '<tr>'; echo '<td>' . $user[ 'first_name'] . ' ' . $user[ 'last_name'] . '</td>'; echo '<td>' . $user[ 'email'] . '</td>'; echo '<td>' . $level[$user[ 'level']] . '</td>'; echo '<td>' . '<a href="' . base_url( '/admin/user_edit/' . $user[ 'id']) . '">' . '<img src="' . base_url( 'images/edit.png') . '" /></a>'. '<a href="' . base_url( '/admin/user_delete/' . $user[ 'id']) . '">' . '<img src="' . base_url( 'images/delete.png') . '" /></a>' . '</td>'; echo '</tr>'; } if (!sizeof($users)) echo '<tr><td>There are no Brand Ambassadors matching this criteria !</td><td></td><td></td></tr>'; ?> </tbody>
        </table>
    </div>
</div>
