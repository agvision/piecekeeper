<script type="text/javascript">
  $(document).ready(function(){
      var ranking = new Array();
      var labels  = new Array();

      ranking[0] = "<?php echo $global_ranking ?>";
      ranking[1] = <?php echo $country_ranking ?>;
      ranking[2] = <?php echo $city_ranking ?>;

      labels[0] = "Global Ranking";
      labels[1] = "Country Ranking";
      labels[2] = "City Ranking";

      var count = 1;

      setInterval(function(){
        $('#ranking-position').html(ranking[count]);
        $('#ranking-label').html(labels[count]);

        count++;
        if (count > 2) {
          count = 0;
        }
      }, 3000);
  });
</script>

    <div class="content">
      <div class="content-bottom">
        <div class="column-left">
              <div class="left-points-board" style='height: auto;'>
                  <div class="left-points-board-cells">
                    <div class="cell black">
                        <a href="<?php echo base_url('/levels') ?>" class='tooltip-left-box tipsy' title='We have 3 levels in our PieceKeeper program. Reaching a new level gets you free stuff and more points.'></a>
                        <div class="cell-text">
                            <?php $lvl = $user['level'] + 3; if($lvl > 3) {$lvl = 3;} ?>
                            <?php echo $lvl ?>
                        </div>
                        <div class="cell-sub-text">YOUR LEVEL</div>
                    </div>
                    <div class="cell" >
                      <a href="<?php echo base_url('/profile') ?>" class='tooltip-left-box tipsy' title='Total amount of social followers.'></a>
                      <div class="cell-text"><?=$countFollowers?></div>
                      <div class="cell-sub-text">TOTAL FOLLOWERS</div>
                    </div>
                    <div class="cell" >
                      <a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-left-box tipsy' title='Total points are your sales points and bonus points combined. The rankings are based on total points.'></a>
                      <div class="cell-text"><?=intval($totals->total_points)?></div>
                      <div class="cell-sub-text">TOTAL POINTS</div>
                    </div>
                    <div class="cell">
                      <a href="<?php echo base_url('/ranking') ?>" class='tooltip-left-box tipsy' title='This shows what ranking you have earned globally amongst all the PieceKeepers'></a>
                      <div class="cell-text"><?php echo $global_ranking ?></div>
                      <div class="cell-sub-text">GLOBAL RANK</div>
                    </div>
                  </div>
                </div>
          </div>
          <div class="column-right">
              <div class="header-stats"> <?php echo $goals_title ?> </div>

              <?php echo $goals_content ?>

              <div class='goals-wrapper'>
              <?php foreach ($goals as $g): ?>

                  <div class='goal-wrapper'>
                      <div class='goal-top'>
                          <div class='goal-title'>
                              <?php echo $g->title ?>
                          </div>
                      </div>
                      <div class='goal-middle'>
                          <div class='rank-display'>
                              <div class='rank-top'>
                                  <div class='rank-value'><?php echo $g->rank ?></div>
                                  <div class='rank-label'><?php echo $g->rank_label ?></div>
                              </div>
                              <div class="<?php echo $g->rank_status_class ?>">
                                  <div class='rank-status'><?php echo $g->rank_status ?></div>
                              </div>
                          </div>
                          <div class='goal-details'>
                              <div class='detail-label'>Points required:</div>   <div class='detail-value'><?php echo $g->points ?> (<?php echo $g->invited_label ?>)</div>
                              <div class='detail-label'>Who will be invited:</div>   <div class='detail-value'>Top <?php echo $g->invited ?> PieceKeepers (<?php echo $g->invited_label ?>)</div>
                              <div class='detail-label'>Deadline:</div>   <div class='detail-value'><?php echo date('M jS, Y', strtotime($g->deadline)) ?></div>
                          </div>
                          <div class='progress-bar'>
                              <div class='bar-title'>Goal Progress Bar</div>
                              <div class='bar-wrapper'>
                                  <?php if ($g->earned_points != 0): ?>
                                      <div class='progress-wrapper' style='width:<?php echo $g->progress_width ?>px;'>
                                          <?php if ($g->completed): ?>
                                              <div class='progress-label completed'>IT'S HAPPENING - GOAL REACHED!</div>
                                          <?php else: ?>
                                              <div class='progress-label'><?php echo $g->earned_points ?></div>
                                          <?php endif ?>
                                      </div>
                                  <?php else: ?>
                                      <div class='progress-wrapper' style='width: 30px; background-color: transparent;'>
                                          <div class='progress-label'>0</div>
                                      </div>
                                  <?php endif ?>

                                  <?php if ($g->completed): ?>
                                      <div class='total-wrapper completed'>
                                          <div class='total-value'><?php echo $g->points ?></div>
                                          <div class='total-label'>Points</div>
                                      </div>
                                  <?php else: ?>
                                      <div class='total-wrapper'>
                                          <div class='total-value'><?php echo $g->points ?></div>
                                          <div class='total-label'>Points</div>
                                      </div>
                                  <?php endif ?>
                              </div>
                          </div>
                      </div>
                      <div class='goal-bottom'>
                          <?php echo $g->description ?>
                      </div>
                  </div>

              <?php endforeach ?>
              </div>
          </div>
            <div class="clear"></div>
      </div>
    </div>
