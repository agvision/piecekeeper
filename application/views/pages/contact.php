<?php if ($this->session->flashdata('sent')): ?>
  <div class='alert-box success'>
      <?php echo $this->session->flashdata('sent');  ?>
  </div>
<?php endif ?>

<div class='container'>
  <div style='font-size: 18px; line-height: 25px; text-align: center;'>
      <?php echo $contact_message ?>
  </div>
  <br>
    <form action="<?=base_url('contact')?>" method="post" accept-charset="utf-8">
      <div class="row collapse" style='margin-left: 375px;'>
        <div class="large-4 column">&nbsp;</div>
        <div class="large-3">
          <input type="email" name="email" placeholder="email" />
        </div>
        <div class="large-4 column">&nbsp;</div>
        <div class="large-3">
          <input type="text" name="name" placeholder="name" />
        </div>
        <div class="large-4 column">&nbsp;</div>
        <div class="large-3">
          <input type="text" name="subject" placeholder="subject" />
        </div>
        <div class="large-4 column">&nbsp;</div>
        <div class="large-3">
          <textarea name='message' placeholder='message'></textarea>
        </div>
        <div class="large-3 column"></div>
      </div>
      <div class="row collapse">
        <div class="large-5 column">&nbsp;</div>
        <input type="submit" class="btn-black" value="Send" style='width: 77px; margin-left: 45px; margin-top: 15px;' />
      </div>
      <input type='hidden' name='trigger' value='send' />
    </form>

</div>

