<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script type="text/javascript">
		$(document).ready(function() {
				CKEDITOR.replace('entry_content');
		});
</script>
<div class='blog-post-add content-padding content-bottom'>
		<div class="header-stats"> Add Post </div>
		<br>
		<?php if ($this->session->flashdata('success') != ''): ?>
		<div class='form-alert-wrapper no-mb'>
				<div class='alert-box success form-alert'>
						<?php echo $this->session->flashdata('success'); ?> </div>
		</div>
		<br>
		<?php endif ?>
		<form method='post' action='<?=base_url(' blog/add ')?>' enctype='multipart/form-data'>
				<div class="large-4">
						<input type="text" name="name" placeholder="Entry name" value="" /> </div>
				<div class="large-4">
						<input type="text" name="short_description" placeholder="Entry short description" /> </div>
				<div class="large-4">
						<input type="file" name="image" />
				</div>
				<textarea name="body" id="entry_content"></textarea>
				<br>
				<input type="submit" name="save" class="btn-black" value="Add Post" />
				<input type='hidden' name='trigger' value='add-blog'> </form>
		<div class="dotted-line dotted-line-mt"></div>
		<br>
		<br> </div>
