<script type="text/javascript">
	$(document).ready(function(){
		var front_image = "<?php echo $front_image ?>";
		$('body').css('background', "#000000 url('<?php echo base_url('images/front/"+front_image+"') ?>')");
		$('body').css('background-size', 'cover');
		$('body').css('background-position', 'center');
	});
</script>

<?php if ($this->session->flashdata('social_not_unique')): ?>
	<br><br><br>
	<div class="front-alerts register-alerts opacity-bg">
		<div class="form-alert-wrapper no-mb">
			<div class="alert-box error form-alert">This social account has already been added in the system.</div>
		</div>
	</div>
<?php endif ?>

<div class='social front-content social-front-content'>
	<div class='social-title opacity-bg'>
		LINK SOCIAL MEDIA
	</div>
	<div class='social-content opacity-bg' id='link-networks'>
		Please link up your social media profile below. We <b>ONLY</b> pull your following count
		so we can determine your social reach. The more accounts you enter the bigger chance of being accepted.
		<br/>
		<b>You have to enter at least one social account.</b>

		<div class="followers social-followers">

				<div id="fb-accounts">
                  <div class="title">Choose a Facebook page</div>
                  <?php if(!empty($fb_pages)): ?>
                    <?php foreach ($fb_pages as $p): ?>
                      <a href="<?php echo base_url('social/facebook_page/'.$p['id']) ?>" class="account"><?php echo $p['name'] ?></a>
                    <?php endforeach ?>
                    <!-- <div class="front-btn">
                      <a href="/social/facebook_update">UPDATE LIST</a>
                    </div> -->
                  <?php else: ?>
                      <div>You have no facebook pages to display.</div>
                  <?php endif ?>
                </div>
				<div class="followers-box social-followers-box <?php if($facebook_linked) echo 'choose-facebook-registration'; ?>" >
					<a href="<?php if($facebook_linked) { echo '#'; } else { echo base_url('profile/facebook?next=social'); }?>">
						<img src="<?=base_url('images/facebook_followers.jpg')?>" />
						<?php if (isset($fb_friends)): ?>
								<div class='social-friends social-followers-friends'><?php echo $fb_friends ?></div>
						<?php elseif($facebook_linked): ?>
								<div class='social-friends social-followers-friends-small'>SELECT</div>
						<?php else: ?>
								<div class='social-friends social-followers-friends-small'>LINK UP</div>
						<?php endif ?>
					</a>
				</div>
				<div class="followers-box social-followers-box">
						<a href="<?php echo base_url('profile/twitter?next=social') ?>"><img src="<?=base_url('images/twitter_followers.jpg')?>" />
						<?php if (isset($tw_followers)): ?>
								<div class='social-friends social-followers-friends'><?php echo $tw_followers ?></div>
						<?php else: ?>
								<div class='social-friends social-followers-friends-small' >LINK UP</div>
						<?php endif ?>
					</a>
				</div>
				<div class="followers-box social-followers-box">
						<a href="<?php echo base_url('profile/instagram?next=social') ?>"><img src="<?=base_url('images/instagram_followers.jpg')?>" />
						<?php if (isset($inst_followers)): ?>
								<div class='social-friends social-followers-friends' ><?php echo $inst_followers ?></div>
						<?php else: ?>
								<div class='social-friends social-followers-friends-small' >LINK UP</div>
						<?php endif ?>
					</a>
				</div>
				<div class="followers-box social-followers-box">
						<a href="<?php echo base_url('profile/tumblr?next=social') ?>"><img src="<?=base_url('images/tumblr_followers.jpg')?>" />
						<?php if (isset($tub_followers)): ?>
								<div class='social-friends social-followers-friends'><?php echo $tub_followers ?></div>
						<?php else: ?>
								<div class='social-friends social-followers-friends-small'>LINK UP</div>
						<?php endif ?>
					</a>
				</div>
				<div class="followers-box social-followers-box">
						<a href="<?php echo base_url('profile/pinterest?next=social') ?>"><img src="<?=base_url('images/pinterest_followers.jpg')?>" />
						<?php if (isset($pin_followers)): ?>
								<div class='social-friends social-followers-friends'><?php echo $pin_followers ?></div>
						<?php else: ?>
								<div class='social-friends social-followers-friends-small'>LINK UP</div>
						<?php endif ?>
					</a>
				</div>
				<!-- <div class="followers-box social-followers-box">
						<a href="<?php echo base_url('profile/linkedin?next=social') ?>"><img src="<?=base_url('images/in_followers.jpg')?>" />
						<?php if (isset($ln_followers)): ?>
								<div class='social-friends social-followers-friends'><?php echo $ln_followers ?></div>
						<?php else: ?>
								<div class='social-friends social-followers-friends-small'>LINK UP</div>
						<?php endif ?>
					</a>
				</div> -->
					<div class="followers-box social-followers-box">
							<a href="<?php echo base_url('profile/youtube?next=social') ?>"><img src="<?=base_url('images/yb_followers.jpg')?>" />
							<?php if (isset($yb_followers)): ?>
									<div class='social-friends social-followers-friends'><?php echo $yb_followers ?></div>
							<?php else: ?>
									<div class='social-friends social-followers-friends-small'>LINK UP</div>
							<?php endif ?>
						</a>
					</div>
					<div class="followers-box social-followers-box">
							<a href="<?php echo base_url('profile/vine?next=social') ?>"><img src="<?=base_url('images/vn_followers.jpg')?>" />
							<?php if (isset($vn_followers)): ?>
									<div class='social-friends social-followers-friends'><?php echo $vn_followers ?></div>
							<?php else: ?>
									<div class='social-friends social-followers-friends-small'>LINK UP</div>
							<?php endif ?>
						</a>
					</div>
				<div class="clear"></div>


		</div>

		<?php
			if($facebook_linked && !isset($fb_friends))
			{
				?>
					<div class="facebook_pages">
						<h4>Please select one of your facebook pages:</h4>
						<?php
							if(!empty($fb_pages))
							{
								foreach ($fb_pages as $p)
								{ ?>
					                <a href="<?php echo base_url('social/facebook_page/'.$p['id']) ?>" class="account"><?php echo $p['name'] ?></a>
				                <?php
				                }
				                ?>
				                <!-- <div class="front-btn">
			                      <a href="/social/facebook_update">UPDATE LIST</a>
			                    </div > -->
			                    <?php
							}
							else
							{
								?>
								<div>You have no facebook pages to display.</div>
								<?php
							}
						?>
					</div>
				<?php
			}
		?>



		<?php
			if(intval($linked) > 0)
			{
				?>
				<div class='social-aditional-message'>
					Thank you for linking at least one social account. Your account will be reviewed by our team.
				</div>

				<br/>
				<?php
			}
		?>

		<?php if ($total_followers > 0): ?>
			<div class="total"><?php echo number_format($total_followers) ?> Total Following</div>
			<?php if (isset($ipad) && $ipad): ?>
				<div class="total"><?php echo $social_credit ?> Social Credit</div>
			<?php endif ?>
		<?php endif ?>

		<?php if (intval($linked) == 0): ?>
			<div class='front-btn social-followers-btn-done' >DONE</div>

		<?php elseif(intval($linked) == 1 && ( (!isset($ipad) || (isset($ipad) && !$ipad))) && (!isset($pku) || (isset($pku) && !$pku)) ): ?>
			<div id='ask-for-more-networks' class='front-btn social-followers-btn-done-2' >DONE</div>

		<?php else: ?>
			<?php if (isset($ipad) && $ipad): ?>
				<a href="<?php echo base_url('auth/finish_register_ipad/'.$user['hash']) ?>">
					<div class='front-btn social-followers-btn-done-2' >DONE</div>
				</a>
			<?php elseif (isset($pku) && $pku): ?>
				<a href="<?php echo base_url('auth/finish_register_pku/'.$user['hash']) ?>">
					<div class='front-btn social-followers-btn-done-2' >DONE</div>
				</a>
			<?php else: ?>
				<a href="<?php echo base_url('auth/finish_register/'.$user['hash']) ?>">
					<div class='front-btn social-followers-btn-done-2' >DONE</div>
				</a>
			<?php endif ?>
		<?php endif ?>
	</div>

	<div id='ask-for-more-networks-wrapper' class='social-content opacity-bg display-none'>
		If you link up more social media accounts, it is a <br>
		higher chance of your application being approved. <br><br>

		DO YOU WANT TO LINK UP MORE ACCOUNTS? <br><br>

		<div class='front-btn social-followers-btn-more' id='link-more'>YES</div>
		<a href="<?php echo base_url('auth/finish_register/'.$user['hash']) ?>">
			<div class='front-btn social-followers-btn-no'>NO</div>
		</a>
	</div>

</div>
