<div class='content content-padding blog-post'>
		<h3><a href='<?= base_url() . "events/view/" . $post->id ?>'><?php echo $post->name ?></a></h3>
		<p>Date: <?=$post->date?> |
		<?php if ($post->first_name): ?>
			Posted by <?php echo $post->first_name." ".$post->last_name ?></p>
		<?php else: ?>
			Posted by OnePiece</p>
		<?php endif ?>
		<?php if($post->image) { ?>
		<p><a href='<?= base_url() . "events/view/" . $post->id ?>'><img src="<?=base_url() . "images/event/big/" . $post->image?>" class="events-view-image" /></a></p>
		<?php } ?>
		<p>
			<?php echo $post->body ?>
		</p>
</div>
