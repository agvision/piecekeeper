
    <div class="content">
      <div class="content-bottom">
        <div class="column-left">
              <div class="left-points-board height-auto" >
                  <div class="left-points-board-cells">
                    <div class="cell black">
                        <a href="<?php echo base_url('/levels') ?>" class='tooltip-left-box tipsy' title='We have 3 levels in our PieceKeeper program. Reaching a new level gets you free stuff and more points.'></a>
                        <div class="cell-text">
                            <?php $lvl = $user['level'] + 3; if($lvl > 3) {$lvl = 3;} ?>
                            <?php echo $lvl ?>
                        </div>
                        <div class="cell-sub-text">YOUR LEVEL</div>
                    </div>
                    <div class="cell" >
                      <a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-left-box tipsy' title='Total points are your sales points and bonus points combined. The rankings are based on total points.'></a>
                      <div class="cell-text"><?=intval($totals->total_points)?></div>
                      <div class="cell-sub-text">TOTAL POINTS</div>
                    </div>
                    <div class="cell">
                      <a href="<?php echo base_url('/ranking') ?>" class='tooltip-left-box tipsy' title='This shows what ranking you have earned globally amongst all the PieceKeepers'></a>
                      <div class="cell-text"><?php echo $global_ranking ?></div>
                      <div class="cell-sub-text">GLOBAL RANK</div>
                    </div>
                  </div>
                </div>
          </div>
          <div class="column-right">
              <div class="header-stats"> <?php echo $title ?> </div>

              <form action='' method='post'>
                <?php foreach ($products as $p): ?>
                  <input type='radio' name='product' value="<?php echo $p ?>" ><?php echo $p ?><br>
                <?php endforeach ?>
                  <br>
                  <input type='submit' value='Save' class="btn-success save-profile-black" />

                  <input type='hidden' name='trigger' value='save' />
              </form>

          </div>
            <div class="clear"></div>

      </div>
    </div>
