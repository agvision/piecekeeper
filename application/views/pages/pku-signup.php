<script type="text/javascript">


    $(document).ready(function(){
        var front_image = "<?php echo $front_image ?>";
        $('body').css('background', "#000000 url('<?php echo base_url('images/front/"+front_image+"') ?>')");
        $('body').css('background-size', 'cover');
        $('body').css('background-position', 'center');

        <?php if ((isset($post['check_school']) && $post['check_school'] != 'Yes') || !isset($post['check_school'])): ?>
            $('#school-wrapper').hide();
        <?php endif ?>

        // force USA states
        <?php if (!isset($post['country'])): ?>
            checkUSA(236);
            changeCitiesByCountry(236, 'New York');
            $("#state").val("New York");
        <?php endif ?>

        // force loading fb state
        <?php if (isset($post['state'])): ?>
            checkUSA(236);
            $("#state").val("<?php echo $post['state'] ?>");
            $("#city").val("<?php echo $post['city'] ?>");
        <?php endif ?>
    });

    function getImage(input, image) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById(input).files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById(image).src = oFREvent.target.result;
        };
    }

    function checkUSA(id_country){
        if (id_country == 236) {
          var input = "<select id='state' name='state'>";

          <?php foreach ($states as $s): ?>
              input += "<option value='<?php echo $s->state ?>'><?php echo $s->state ?></option>"
          <?php endforeach ?>

          input += "</select>";
        } else {
          var input = "";
        }

        $('#states-wrapper').html(input);
    }

    function changeCitiesByCountry(id_country, state){
        $.ajax({
            type: 'POST',
            url:  "<?php echo base_url('auth/get_cities_by_country') ?>",
            data: 'id_country='+id_country+"&state="+state,
            dataType: 'json',
            success: function(data) {
                var input = "<select id='city' name='city' class='large-input'>\n";
                input += "<option value=''>Select City</option>\n";
                input += "<option value='other'>OTHER</option>\n";
                $.each(data, function(index, value){
                    input += "<option value='"+value+"'>"+value+"</option>\n";
                })
                input += "</select>";

                $('#city-wrapper').html(input);
                if(state == "New York"){
                  $("#city").val("New York");
                }
            }
        });
    }

    function changeSchoolsByCountry(id_country){
        $.ajax({
            type: 'POST',
            url:  "<?php echo base_url('auth/get_schools_by_country') ?>",
            data: 'id_country='+id_country,
            dataType: 'json',
            success: function(data) {
                var input = "<select id='school' name='school' class='large-input'>\n";
                input += "<option value=''>Select School</option>\n";
                input += "<option value='other'>OTHER</option>\n";
                $.each(data, function(index, value){
                    input += "<option value='"+value+"'>"+value+"</option>\n";
                })
                input += "</select>";

                $('#school-wrapper').html(input);
            }
        });
    }

    function changeSchoolsByCity(city){
        $.ajax({
            type: 'POST',
            url:  "<?php echo base_url('auth/get_schools_by_city') ?>",
            data: 'city='+city,
            dataType: 'json',
            success: function(data) {
                var input = "<select id='school' name='school' class='large-input'>\n";
                input += "<option value=''>Select School</option>\n";
                input += "<option value='other'>OTHER</option>\n";
                $.each(data, function(index, value){
                    input += "<option value='"+value+"'>"+value+"</option>\n";
                })
                input += "</select>";

                $('#school-wrapper').html(input);
            }
        });
    }

    function changeCountryPrefix(country) {
        $.ajax({
            type: 'POST',
            url:  "<?php echo base_url('auth/get_country_prefix') ?>",
            data: 'country='+country,
            success: function(data) {
                $('#dial_prefix').val(data);
            }
        });
    }

</script>

<br><br><br>
<?php if (validation_errors()): ?>
    <div class='front-alerts register-alerts opacity-bg'>
      <div class='form-alert-wrapper no-mb'>
        <?php echo validation_errors("<div class='alert-box error form-alert'>", "</div>"); ?>
      </div>
    </div>
<?php endif ?>

<?php if ($this->session->flashdata('success') != ''): ?>
  <div class='front-alerts register-alerts opacity-bg'>
    <div class='form-alert-wrapper no-mb'>
        <div class='alert-box success form-alert'>
            <?php echo $this->session->flashdata('success');  ?>
        </div>
    </div>
  </div>

<?php else: ?>

  <div class='ipad front-content ipad-front-content'>

      <div class='register-title opacity-bg'>
        BRING ONEPIECE TO YOUR CAMPUS
      </div>

      <div class='register-content opacity-bg ipad'>

        <div class='ipad-please-fill'>
          Please fill out the application form below.
          You will receive an email <br> once we have reviewed it.
        </div>

        <?php if ($show_fb_register): ?>
            <a href="<?php echo base_url('auth/fb_register/pku') ?>" class="fb-btn">Register with <b>Facebook</b></a>
        <?php endif ?>

        <form class='form-horizontal ipad' enctype="multipart/form-data" id="register" action="<?=base_url('auth/pku')?>" method="post" accept-charset="utf-8">
          <div class='row collapse'>

              <div class='large-input'>
                <input type="email" name="email" value="<?php echo isset($post['email']) ? $post['email'] : '' ?>" placeholder="Email" />
              </div>

              <div class='large-input'>
                <input type="password" name="password" placeholder="Password" />
              </div>

              <div class='large-input'>
                <input type="text" name="first_name" placeholder="First Name" value="<?php echo isset($post['first_name']) ? $post['first_name'] : '' ?>" />
              </div>

              <div class='large-input'>
                <input type="text" name="last_name" placeholder="Last Name" value="<?php echo isset($post['last_name']) ? $post['last_name'] : '' ?>" />
              </div>

              <p class="ipad-label-birthday">Birthday</p>

              <div class='large-input'>

                <select name='day' class="ipad-select-birthday" >
                    <option value=''>Day</option>
                    <?php for ($i=1; $i <= 31 ; $i++) {
                        if (isset($post['day']) && $post['day'] == $i) {
                            echo "<option value='".$i."' selected='selected'>".$i."</option>";
                        } else {
                            echo "<option value='".$i."'>".$i."</option>";
                        }
                    } ?>
                </select>

                <select name='month'  class="ipad-select-birthday <?php if( $user_country == "US" ) { echo " ipad-select-birthday-us "; } ?>" >
                    <option value=''>Month</option>
                    <?php for ($i=1; $i <= 12 ; $i++) {
                        if (isset($post['month']) && $post['month'] == $i) {
                            echo "<option value='".$i."' selected='selected'>".$months[$i-1]."</option>";
                        } else {
                            echo "<option value='".$i."'>".$months[$i-1]."</option>";
                        }
                    } ?>
                </select>

                 <select name='year'  class="ipad-select-birthday" >
                    <option value=''>Year</option>
                    <?php for ($i=0; $i <= 50 ; $i++) {
                        if (isset($post['year']) && $post['year'] == ($i+1960)) {
                            echo "<option value='".($i+1960)."' selected='selected'>".($i+1960)."</option>";
                        } else {
                            echo "<option value='".($i+1960)."'>".($i+1960)."</option>";
                        }
                    } ?>
                </select>
              </div>

              <div class='large-input'>
                <input type="text" name="address1" placeholder="Address 1" value="<?php echo isset($post['address1']) ? $post['address1'] : '' ?>" />
              </div>

              <div class='large-input'>
                <input type="text" name="address2" placeholder="Address 2 (Optional)" value="<?php echo isset($post['address2']) ? $post['address2'] : '' ?>" />
              </div>

              <div class='large-input'>
                <input type="text" name="postcode" placeholder="Zip/Postal Code" value="<?php echo isset($post['postcode']) ? $post['postcode'] : '' ?>"/>
              </div>

              <select id='country' name='country' class='large-input'>
                <option value=''>Select Country</option>
                <?php foreach ($countries as $c): ?>
                    <option value="<?php echo $c->country_id.'_'.$c->short_name ?>" <?php if( (isset($post['country']) && $post['country'] == $c->country_id.'_'.$c->short_name) || (!isset($post['country']) && $c->country_id == 236)) echo "selected='selected'" ?> ><?php echo $c->short_name ?></option>
                <?php endforeach ?>
              </select>

              <div id='states-wrapper' class='large-input'></div>

              <div class='large-input' id='city-wrapper'>
                <input type="text" name="city" placeholder="City" value="<?php echo isset($post['city']) ? $post['city'] : '' ?>" />
              </div>

              <div class='large-input'>
                <input disabled='disabled' id='dial_prefix' value="+1" class="ipad-input-dial-prefix" type='text' name='dial_prefix' placeholder="Prefix" value="<?php echo isset($post['dial_prefix']) ? $post['dial_prefix'] : '' ?>" />
                <input class="ipad-input-phone" type="text" name="phone" placeholder="Mobile number" value="<?php echo isset($post['phone']) ? $post['phone'] : '' ?>"/>
              </div>

              <div class='large-input'>
                <input type="text" name="school" placeholder="School or University You're Enrolled In" value="<?php echo isset($post['school']) ? $post['school'] : '' ?>"/>
              </div>

              <div class='large-input'>
                <input type="text" name="graduation_year" placeholder="Graduation Year" value="<?php echo isset($post['graduation_year']) ? $post['graduation_year'] : '' ?>" />
              </div> 

              <div class='large-input'>
                <textarea name="pku_campus_involvement" placeholder="Campus Involvement"><?php echo isset($post['pku_campus_involvement']) ? $post['pku_campus_involvement'] : '' ?></textarea>
              </div>

              <div class='large-input'>
                <select name='gender'>
                  <option value='1' <?php if(isset($post['gender']) && $post['gender'] == '1') echo "selected='selected'" ?>>Male</option>
                  <option value='0' <?php if(isset($post['gender']) && $post['gender'] == '0') echo "selected='selected'" ?>>Female</option>
                </select>
              </div>

              <div class='large-input'>
                <label>
                  <input type="checkbox" checked="checked" name="terms" class="ipad-checkbox-terms" /> <span class="ipad-accept">I accept <a href='<?php echo base_url('/terms') ?>' class="ipad-link-terms" target='_blank'>Terms and Conditions</a></span>
                </labe>
              </div>


            <div class="row collapse">
              <input type="submit" class="front-btn ipad-submit" value="NEXT" />
            </div>

            <?php if ($this->session->userdata('i')): ?>
              <input type='hidden' name='invitation' value="<?php echo $this->session->userdata('i') ?>" />
            <?php endif ?>

            <input type="hidden" name="trigger" value="pku" />

          </div>
        </form>
      </div>
  </div>

<?php endif ?>



<br><br><br>
