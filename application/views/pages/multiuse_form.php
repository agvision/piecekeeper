
<?php  if ( !($user['level'] < $settings['multiple_discount_level'])): ?>
	<h5>Create multiuse coupon</h5>
<?php endif ?>


<div id="multiple-discount-wrapper">
	<div class="coupons-form-wrapper">
		<form action="<?=base_url('coupons/create_coupon')?>" method="post" accept-charset="utf-8">
			<div class="row collapse">
				<input type="text" name="code" placeholder="Create Your Code" />
				<input type='text' name='link' placeholder='Link to where you will post code' />

				<input type='hidden' name='trigger' value='48_hours' />
				<input type="submit" class="btn-success save-profile-black coupons-create-submit" value="Create Multiuse Code" />
			</div>
		</form>
	</div>
	<div class='dynamic-text coupons-message'>
		<?php echo $coupons_multiple ?>
	</div>

	<table>
		<thead>
		<tr>
			<th style="width:200px;">Code</th>
			<th style="width:150px;">Discount</th>
			<th style="width:200px;">Expires</th>
			<th style="width:110px;">Status</th>
			</tr>
		</thead>
		<tbody>
		<?php $i = 0; ?>
		<?php foreach ($coupons as $c): ?>
			<?php if ($c->type == 2 && (strtotime("-30 days") < strtotime($c->created))): ?>
				<?php $i++; ?>
				<tr>
				<td>
					<b><?php echo $c->code ?></b>
				</td>
				<td>
					<?php if ($c->amount && $c->amount != '0.00'): ?>
					<?php echo $c->amount." ".$c->currency ?>
					<?php else: ?>
					<?php echo $c->percent." %" ?>
					<?php endif ?>
				</td>
				<td>
					<?php if ($c->approved): ?>
						<?php echo $this->users->formatDate($user['id'], strtotime($c->to),true); ?>
					<?php endif ?>
				</td>
				<td>
					<?php if ($c->approved == 1): ?>
						<?php if (time() > strtotime($c->to)): ?>
							<div style='color: #999999;'>Expired</div>
						<?php else: ?>
							<b>Active</b>
						<?php endif ?>
					<?php elseif ($c->deleted == 1): ?>
						<div style='text-decoration: underline; cursor: pointer;' class='show-reason' value="<?php echo $c->id ?>">Rejected</div>
					<?php else: ?>
						Pending
					<?php endif ?>
				</td>
				</tr>
				<tr id="<?php echo 'reason-'.$c->id ?>" class="display-none">
					<td>"<?php echo $c->reason ?>"</td>
				</tr>
			<?php endif ?>
		<?php endforeach ?>

		<?php if ($i==0): ?>
			<tr>
			<td colspan='4' >No Active Discount Codes.</td>
			</tr>
		<?php endif ?>

		</tbody>
	</table>
</div>
