<script type="text/javascript">

    $(document).ready(function(){
        var front_image = "<?php echo $front_image ?>";
        $('body').css('background', "#000000 url('<?php echo base_url('images/front/"+front_image+"') ?>')");
        $('body').css('background-size', 'cover');
        $('body').css('background-position', 'center');
    });

</script>

<br><br><br>
<?php if (validation_errors()): ?>
    <div class='front-alerts register-alerts opacity-bg'>
      <div class='form-alert-wrapper no-mb'>
        <?php echo validation_errors("<div class='alert-box error form-alert'>", "</div>"); ?>
      </div>
    </div>
<?php endif ?>

<?php if ($this->session->flashdata('success') != ''): ?>
  <div class='front-alerts register-alerts opacity-bg'>
    <div class='form-alert-wrapper no-mb'>
        <div class='alert-box success form-alert'>
            <?php echo $this->session->flashdata('success');  ?>
        </div>
    </div>
  </div>
<?php endif ?>

<div class='ipad front-content ipad-front-content'>

    <div class='register-title opacity-bg'>
      BRING ONEPIECE TO YOUR CAMPUS
    </div>

    <div class='register-content opacity-bg ipad'>

      <div class='ipad-please-fill'>
        Enter your email address here:
      </div>

      <form class='form-horizontal ipad' enctype="multipart/form-data" id="register" action="<?=base_url('auth/pku_email')?>" method="post" accept-charset="utf-8">
        <div class='row collapse'>

            <div class='large-input'>
              <input type="email" name="email" value="" placeholder="Email" />
            </div>

            
          <div class="row collapse">
            <input type="submit" class="front-btn ipad-submit" value="SUBMIT" />
          </div>

          <input type="hidden" name="trigger" value="pku_email" />

        </div>
      </form>
    </div>
</div>

<br><br><br>
