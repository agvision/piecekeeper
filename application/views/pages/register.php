<script type="text/javascript">
    $(document).ready(function() {
        var front_image = "<?php echo $front_image ?>";
        $('body').css('background', "#000000 url('<?php echo base_url('images/front/" + front_image + "') ?>')");
        $('body').css('background-size', 'cover');
        $('body').css('background-position', 'center');
        <?php
        if((isset($post['check_school']) && $post['check_school'] != 'Yes') || !isset($post['check_school'])): ?> $('#school-wrapper').hide(); <?php endif ?> $('input[name=check_school]').change(function() {
            var value = this.value;
            if(value == "Yes") {
                $('#school-wrapper').slideDown('slow');
            } else {
                $('#school-wrapper').slideUp('slow');
            }
        });
        setInterval(function() {
            var content = $('#tw-description').val();
            if(content.length > 140) {
                var replace = content.substr(0, 140);
                console.log(replace);
                $('#tw-description').val(replace);
            }
        }, 100);
    });

    function getImage(input, image) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById(input).files[0]);
        oFReader.onload = function(oFREvent) {
            document.getElementById(image).src = oFREvent.target.result;
        };
    }

    function checkUSA(id_country) {
        if(id_country == 236) {
            var input = "<select id='state' name='state'>";
            <?php foreach($states as $s): ?>
              input += "<option value='<?php echo $s->state ?>'><?php echo $s->state ?></option>";
               <?php endforeach ?>
              input += "</select>";
        } else {
            var input = "";
        }
        $('#states-wrapper').html(input);
    }

    function changeCitiesByCountry(id_country, state) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('auth/get_cities_by_country') ?>",
            data: 'id_country=' + id_country + "&state=" + state,
            dataType: 'json',
            success: function(data) {
                var input = "<select id='city' name='city' class='medium-input'>\n";
                input += "<option value=' '>Select City</option>\n";
                input += "<option value='other'>OTHER</option>\n";
                $.each(data, function(index, value) {
                    input += "<option value='" + value + "'>" + value + "</option>\n";
                })
                input += "</select>";
                $('#city-wrapper').html(input);
            }
        });
    }

    function changeSchoolsByCountry(id_country) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('auth/get_schools_by_country') ?>",
            data: 'id_country=' + id_country,
            dataType: 'json',
            success: function(data) {
                var input = "<select id='school' name='school' class='medium-input'>\n";
                input += "<option value=''>Select School</option>\n";
                input += "<option value='other'>OTHER</option>\n";
                $.each(data, function(index, value) {
                    input += "<option value='" + value + "'>" + value + "</option>\n";
                })
                input += "</select>";
                $('#school-wrapper').html(input);
            }
        });
    }

    function changeSchoolsByCity(city) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('auth/get_schools_by_city') ?>",
            data: 'city=' + city,
            dataType: 'json',
            success: function(data) {
                var input = "<select id='school' name='school' class='medium-input'>\n";
                input += "<option value=''>Select School</option>\n";
                input += "<option value='other'>OTHER</option>\n";
                $.each(data, function(index, value) {
                    input += "<option value='" + value + "'>" + value + "</option>\n";
                })
                input += "</select>";
                $('#school-wrapper').html(input);
            }
        });
    }

    function changeCountryPrefix(country) {
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('auth/get_country_prefix') ?>",
            data: 'country=' + country,
            success: function(data) {
                $('#dial_prefix').val(data);
            }
        });
    }
</script>
<div class="register">
<br>
<br>
<br>
<?php if (validation_errors()): ?>
<div class="front-alerts register-alerts opacity-bg">
    <div class="form-alert-wrapper no-mb">
        <?php echo validation_errors( "<div class='alert-box error form-alert'>", "</div>"); ?> </div>
</div>
<?php endif ?>
<div class="front-content register-front-content">
    <div class="register-title opacity-bg"> THE APPLICATION FORM </div>
    <?php if( isset($inviterProfile) ) { ?>
    <div class="register-inviter opacity-bg">
        <div class="pic"> <img src="<?=($inviterProfile["profile_pic"] ? base_url('images/profiles/'.$inviterProfile["profile_pic"]) : base_url('images/profile_picture2.jpg'))?>" /> </div>
        <div class="content"> You were invited to the exclusive PieceKeeper program by:
            <p class="name">
                <?=strtoupper($inviterProfile["first_name"]). " ".strtoupper($inviterProfile["last_name"])?>
            </p>
        </div>
    </div>
    <?php } ?>
    <div class="register-content opacity-bg">
        <div class="register-please-fill"> Please fill out the application form below. You will receive an email
            <br> once we have reviewed it. </div>

        <?php if ($show_fb_register): ?>
            <a href="<?php echo base_url('auth/fb_register') ?>" class="fb-btn">Register with <b>Facebook</b></a>
        <?php endif ?>

        <form class="form-horizontal" enctype="multipart/form-data" id="register" action="<?=base_url('auth/register')?>" method="post" accept-charset="utf-8">
            <div class="row collapse">
                <div class="medium-input">
                    <input type="email" name="email" value="<?php echo isset($post["email"]) ? $post["email"] : '' ?>" placeholder="Email" /> </div>
                <div class="medium-input">
                    <input type="password" name="password" placeholder="Password" /> </div>
                <div class="medium-input">
                    <input type="text" name="first_name" placeholder="First Name" value="<?php echo isset($post["first_name"]) ? $post["first_name"] : '' ?>" /> </div>
                <div class="medium-input">
                    <input type="text" name="last_name" placeholder="Last Name" value="<?php echo isset($post["last_name"]) ? $post["last_name"] : '' ?>" /> </div>
                <p class="register-label-birthday">Please upload 2 profile images (max. 2MB)</p>
                <div class="medium-input" id="profile-wrapper-1">
                    <div id="clickable-1"> <img class="register-pic" id="file1-image" src="<?php echo base_url('/images/register_ambassador.jpg') ?>">
                        <div class="choose-file-btn" id="choose-file-btn-1">Choose file</div>
                    </div>
                    <input type="file" name="profile_pic" id="file1" /> </div>
                <div class="medium-input" id="profile-wrapper-2">
                    <div id="clickable-2"> <img class="register-pic" id="file2-image" src="<?php echo base_url('/images/register_ambassador.jpg') ?>">
                        <div class="choose-file-btn" id="choose-file-btn-2">Choose file</div>
                    </div>
                    <input type="file" name="profile_pic_2" id="file2" /> </div>
                <p class="register-label-birthday">Birthday</p>
                <div class="medium-input">
                    <select name="day" class="register-select-birthday">
                        <option value=''>Day</option>
                        <?php for ($i=1; $i <=31 ; $i++) { if (isset($post["day"]) && $post["day"]==$i) { echo "<option value='".$i. "' selected='selected'>".$i. "</option>"; } else { echo "<option value='".$i. "'>".$i. "</option>"; } } ?> </select>
                    <select name="month" class="register-select-birthday" style='<?php if( $user_country == "US" ) { echo "float: left;margin-right:4px;"; } ?>'>
                        <option value=''>Month</option>
                        <?php for ($i=1; $i <=12 ; $i++) { if (isset($post["month"]) && $post["month"]==$i) { echo "<option value='".$i. "' selected='selected'>".$months[$i-1]. "</option>"; } else { echo "<option value='".$i. "'>".$months[$i-1]. "</option>"; } } ?> </select>
                    <select name="year" class="register-select-birthday">
                        <option value=''>Year</option>
                        <?php for ($i=0; $i <=50 ; $i++) { if (isset($post["year"]) && $post["year"]==( $i+1960)) { echo "<option value='".($i+1960). "' selected='selected'>".($i+1960). "</option>"; } else { echo "<option value='".($i+1960). "'>".($i+1960). "</option>"; } } ?> </select>
                </div>
                <div class="medium-input">
                    <input type="text" name="address1" placeholder="Address 1" value="<?php echo isset($post["address1"]) ? $post["address1"] : '' ?>" /> </div>
                <div class="medium-input">
                    <input type="text" name="address2" placeholder="Address 2 (Optional)" value="<?php echo isset($post["address2"]) ? $post["address2"] : '' ?>" /> </div>
                <div class="medium-input">
                    <input type="text" name="postcode" placeholder="Zip/Postal Code" value="<?php echo isset($post["postcode"]) ? $post["postcode"] : '' ?>" /> </div>
                <select id="country" name="country" class="medium-input">
                    <option value=''>Select Country</option>
                    <?php foreach ($countries as $c): ?>
                    <option value="<?php echo $c->country_id.'_'.$c->short_name ?>" <?php if(isset($post["country"]) && $post["country"]==$c->country_id.'_'.$c->short_name) echo "selected='selected'" ?> >
                        <?php echo $c->short_name ?></option>
                    <?php endforeach ?> </select>
                <div id="states-wrapper" class="medium-input"></div>
                <div class="medium-input" id="city-wrapper">
                    <input type="text" name="city" placeholder="City" value="<?php echo isset($post["city"]) ? $post["city"] : '' ?>" /> </div>
                <div class="medium-input">
                    <input disabled="disabled" id="dial_prefix" class="register-dial-prefix" type="text" name="dial_prefix" placeholder="Prefix" value="<?php echo isset($post["dial_prefix"]) ? $post["dial_prefix"] : '' ?>" />
                    <input class="register-input-phone" type="text" name="phone" placeholder="Mobile number" value="<?php echo isset($post["phone"]) ? $post["phone"] : '' ?>" /> </div>
                <div class="medium-input">
                    <div class="register-school">Do you go to School or University?</div>
                    <div class="register-school-input-radio-wrapper">
                        <input type="radio" name="check_school" value="Yes" <?php if(isset($post["check_school"]) && $post["check_school"]=="Yes" ) echo "checked='checked'" ?> /> Yes </div>
                    <div class="register-school-input-radio-wrapper">
                        <input type="radio" name="check_school" value="No" <?php if(isset($post["check_school"]) && $post["check_school"]=="No" ) echo "checked='checked'" ?> /> No </div>
                </div>
                <div class="medium-input" id="school-wrapper">
                    <input type="text" name="school" placeholder="School" value="<?php echo isset($post["school"]) ? $post["school"] : '' ?>" /> </div>
                <div class="medium-input">
                    <input type="text" name="graduation_year" placeholder="Graduation Year" value="<?php echo isset($post["graduation_year"]) ? $post["graduation_year"] : '' ?>" /> </div>
                <div class="medium-input">
                    <input type="text" name="website" placeholder="Blog / Personal Website" value="<?php echo isset($post["website"]) ? $post["website"] : '' ?>" /> </div>
                <div class="medium-input">
                    <textarea class="register-twitter-description" name="tw_description" id="tw-description" placeholder="Describe Yourself in a Tweet"><?php echo isset($post["tw_description"]) ? $post["tw_description"] : '' ?></textarea>
                </div>
                <div class="medium-input">
                    <textarea class="register-find-source" name="find_source" placeholder="Where did you hear about the PieceKeepers?"><?php echo isset($post["find_source"]) ? $post[ 'find_source'] : '' ?></textarea>
                </div>
                <div class="medium-input">
                    <select name="gender">
                        <option value="1" <?php if(isset($post["gender"]) && $post["gender"]=="1" ) echo "selected='selected'" ?>>Male</option>
                        <option value="0" <?php if(isset($post["gender"]) && $post["gender"]=="0" ) echo "selected='selected'" ?>>Female</option>
                    </select>
                </div>
                <?php $j=0 ; foreach ($questions as $q) { ?>
                <div class="medium-input">
                    <div class="register-questions">
                        <?php echo $q->question;?></div>
                    <input type="hidden" name="question-ref-<?=$j?>" value="<?=$q->id?>" />
                    <input type="text" name="question-<?=$q->id?>" placeholder="Your answer" value="<?php echo isset($post["question-".$q->id]) ? $post["question-".$q->id] : '' ?>" /> </div>
                <?php $j++; } ?>
                <div class="medium-input">
                    <input type="checkbox" name="terms" /> I accept <a href='<?php echo base_url('/terms ') ?>' class="register-link-terms" target="_blank">Terms and Conditions</a> </div>
                <div class="row collapse">
                    <input type="submit" class="front-btn register-submit" value="NEXT" /> </div>
                <?php if ($this->session->userdata('i')): ?>
                <input type="hidden" name="invitation" value="<?php echo $this->session->userdata('i') ?>" />
                <?php endif ?> </div>
        </form>
    </div>
</div>
<br>
<br>
<br>
</div>
