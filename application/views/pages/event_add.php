<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        CKEDITOR.replace('event_content');
    });
</script>


<div class='event-add content-padding content-bottom'>

    <div class="header-stats">
      Add Event
    </div>

    <br>

    <?php if ($this->session->flashdata('success') != ''): ?>
        <div class='form-alert-wrapper no-mb'>
            <div class='alert-box success form-alert'>
                <?php echo $this->session->flashdata('success');  ?>
            </div>
        </div>
        <br>
    <?php endif ?>

    <p>Add an Event</p>
    <form method='post' action='<?=base_url('events/add')?>'  enctype='multipart/form-data'>
      <div class="large-4">
        <input type="text" name="name" placeholder="Event name" value="" />
      </div>
      <div class="large-4">
        <input type="text" class='date' name="date" placeholder="Choose a date" value="" />
      </div>
       <div class="large-4">
        <input type="text" name="short_description" placeholder="Entry short description" />
      </div>
      <div class="large-4"> <input type="file" name="image" /></div>
      <textarea name="body" id="event_content" ></textarea>
      <br>
      <input type="submit" name="save" class="btn-black" value="Add Event" />

      <input type='hidden' name='trigger' value='add-event'>
    </form>
    <div class="dotted-line dotted-line-mt"></div>
    <br><br>

</div>
