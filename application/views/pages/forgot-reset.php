<br><br><br>

<?php if (validation_errors()): ?>
    <div class='form-alert-wrapper'>
      <?php echo validation_errors("<div class='alert-box error form-alert'>", "</div>"); ?>
    </div>
<?php endif ?>

<?php if ($this->session->flashdata('success') != ''): ?>
  <div class='form-alert-wrapper'>
      <div class='alert-box success form-alert'>
          <?php echo $this->session->flashdata('success');  ?>
      </div>
  </div>
<?php endif ?>

    <form id="login" action="<?=base_url('auth/forgot_password/'.$hash)?>" method="post" accept-charset="utf-8">
      <div class="row collapse">
        <div class="medium-input">
          <input type="password" name="password" placeholder="new password" />
          <input type='hidden' name='trigger' value='reset' />
        </div>
      </div>
      <div class="row collapse">
        <input type="submit" class="btn-black forgot-reset-btn-submit" value="Reset" />
      </div>
    </form>

<br><br><br>
