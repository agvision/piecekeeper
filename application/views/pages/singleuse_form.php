<h5>Create singleuse coupon</h5>

<?php if ($this->session->flashdata('error') != ''): ?>
	<div class='form-alert-wrapper no-mb'>
		<?php echo $this->session->flashdata('error');  ?>
	</div>
	<br>
<?php endif ?>

<?php if ($this->session->flashdata('success') != ''): ?>
	<div class='form-alert-wrapper coupons-alert-success'>
		<div class='alert-box success form-alert'>
			<?php echo $this->session->flashdata('success');  ?>
		</div>
	</div>
<?php endif ?>


<?php //if ($user['level'] < $settings['multiple_discount_level']): ?>

	<?php if ($this->session->flashdata('success_single_use') != ''): ?>
	<div class='form-alert-wrapper coupons-alert-success-single-use'>
		<div class='alert-box success form-alert'>
			<?php echo $this->session->flashdata('success_single_use');  ?>
		</div>
	</div>
	<?php endif ?>

	<?php if ($this->session->flashdata('error_single_use') != ''): ?>
	<div class='form-alert-wrapper coupons-alert-error-single-use'>
		<?php echo $this->session->flashdata('error_single_use');  ?>
	</div>
	<br>
	<?php endif ?>

	<div class="coupons-form-wrapper">
	<form action="<?=base_url('coupons/create_coupon')?>" method="post" accept-charset="utf-8">
		<div class="row collapse">
			<input type="text" name="code" placeholder="Create Your Code" />
			<input type='hidden' name='trigger' value='discount_20' />
			<input type="submit" class="btn-success save-profile-black coupons-create-submit" value="Create Singleuse Coupon" />
		</div>
	</form>
	</div>

	<div class='dynamic-text coupons-message'>
		<?php echo $coupons_single ?>
	</div>

	<table>
	<thead>
		<tr>
		<th style="width:200px;">Code</th>
		<th style="width:150px;">Discount</th>
		<th style="width:200px;">Expires</th>
		<th style="width:110px;">Status</th>
		</tr>
	</thead>
	<tbody>
		<?php $i = 0; ?>
		<?php foreach ($coupons as $c): ?>
		<?php if ($c->type == 1 && $c->approved == 1 && time() < strtotime($c->to)): ?>
			<?php $i++; ?>
			<tr>
			<td><b><?php echo $c->code ?></b></td>
			<td>
				<?php if ($c->amount && $c->amount != '0.00'): ?>
				<?php echo $c->amount." ".$c->currency ?>
				<?php else: ?>
				<?php echo $c->percent." %" ?>
				<?php endif ?>
			</td>
			<td>
				<?php echo $this->users->formatDate($user['id'], strtotime($c->to),true); ?>
			</td>
			<td>
				<?php if ($c->approved == 1): ?>
					<?php if (time() > strtotime($c->to)): ?>
						<div style='color: #999999;'>Expired</div>
					<?php else: ?>
						<b>Active</b>
					<?php endif ?>
				<?php else: ?>
					Pending
				<?php endif ?>
			</td>
			</tr>
		<?php endif ?>
		<?php endforeach ?>

		<?php if ($i==0): ?>
			<tr>
			<td colspan='4' >No Active Discount Codes.</td>
			</tr>
		<?php endif ?>

	</tbody>
	</table>

	<?php if ($user['level'] < $settings['multiple_discount_level']): ?>
	<div id="multiple-discount-hide">
		<img class="layout-lock" src="<?php echo base_url("/images/lock.png") ?>">
		<p>MULTIPLE CODES WILL BE UNLOCKED WHEN YOU REACH LEVEL 2.</p>
	</div>

	<?php endif ?>
<?php //endif ?>
