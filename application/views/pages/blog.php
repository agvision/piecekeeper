<div class='blog-page-wrapper'>
	<div class='blog-left-column'>
		<div class='content-padding blog-post blog-wrapper'>
			<?php foreach ($posts as $post): ?>
				<?php if($post->image) { ?>
					<p><a href='<?= base_url() . "blog/view/" . $post->id ?>'><img src="<?=base_url() . "images/blog/" . $post->image?>" class="blog-image" alt="Post Image" /></a></p>
				<?php } ?>
				<h3><a href='<?= base_url() . "blog/view/" . $post->id ?>'><?php echo $post->name ?></a></h3>
				<?php if ($post->first_name): ?>
					<div class='blog-post-postedby'>Posted by <?php echo $post->first_name." ".$post->last_name ?></div>
				<?php else: ?>
					<div class='blog-post-postedby'>Posted by OnePiece</div>
				<?php endif ?>

				<div class='blog-post-description'>
					<?php echo $post->short_description ?>
				</div>
				<div class="dotted-line"></div>
				<br/>
				<br/>
			<?php endforeach ?>
		</div>
	</div>


	<div class='blog-right-column'>
		<?php if ($user['level'] >= 1): ?>
				<a href="<?php echo base_url('blog/add') ?>" class='btn-black btn-blog-add'> Create Blog Post</a>
		<?php endif ?>

		<div class="blog-instagram-wrapper">
			<br>
			<div>Instagram #onepiecenorway</div>
			<?php foreach ($tags['onepiecenorway'] as $t): ?>
				<a href="<?php echo $t['url'] ?>" target='_blank'><img src="<?php echo $t['img'] ?>" class="blog-instagram-image" alt="Instagram onepiecenorway Photo"></a>
			<?php endforeach ?>

			<br><br><br>

			<div>Instagram #forgettherules</div>
			<?php foreach ($tags['forgettherules'] as $t): ?>
				<a href="<?php echo $t['url'] ?>" target='_blank'><img src="<?php echo $t['img'] ?>" class="blog-instagram-image" alt="Instagram forgettherules Photo"></a>
			<?php endforeach ?>

			<br><br><br>

			<div>Instagram #slackerlife</div>
			<?php foreach ($tags['slackerlife'] as $t): ?>
				<a href="<?php echo $t['url'] ?>" target='_blank'><img src="<?php echo $t['img'] ?>" class="blog-instagram-image" alt="Instagram slackerlife Photo"></a>
			<?php endforeach ?>
		</div>
	</div>

</div>
