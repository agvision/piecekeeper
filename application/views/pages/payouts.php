<script type="text/javascript">
    $(document).ready(function(){
        <?php if ($user['payment_name'] && $user['payment_number']): ?>
            $('#change-paypal-form').hide();
        <?php endif ?>

        var bonus = <?php echo $settings['giftcard_extra_commission'] ?>;

        $('#gift-input').keyup(function(){
            var amount = (parseFloat(bonus)+100)/100*parseFloat(this.value);
            var gift  = toFixed(parseFloat(amount), 2);
            if (isNaN(gift) || gift == 0) {
                var input  = "";
            } else {
                var input  = gift+" $ as ";
            }
            $('#gift-bonus').html(input);
            var paypal       = toFixed(this.value, 2);
            var paypal_email = "<?php echo $user['payment_number'] ?>";

            if (isNaN(amount) || amount == 0) {
                var input = "Pay to Paypal account";
                var info  = "";
            } else {
                var input = paypal+" $ to PayPal account ("+paypal_email+")";
                var info  = "Please allow up to two weeks for funds to reach your PayPal account."
            }

            $('#paypal-pay').html(input);
            $('#paypal-info').html(info);
        });
    });

    function toFixed(value, precision) {
        var precision = precision || 0,
        neg = value < 0,
        power = Math.pow(10, precision),
        value = Math.round(value * power),
        integral = String((neg ? Math.ceil : Math.floor)(value / power)),
        fraction = String((neg ? -value : value) % power),
        padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');

        return precision ? integral + '.' +  padding + fraction : integral;
    }
</script>

<div class="content payouts">
    <div class="content-bottom">
        <div class="column-left">
            <div class="left-points-board height-auto">
                <div class="left-points-board-cells">
                    <div class="cell black">
                        <a href="<?php echo base_url('/levels') ?>" class='tooltip-left-box tipsy' title='We have 3 levels in our PieceKeeper program. Reaching a new level gets you free stuff and more points.'></a>
                        <div class="cell-text">
                            <?php $lvl = $user['level'] + 3; if($lvl > 3) {$lvl = 3;} ?>
                            <?php echo $lvl ?>
                        </div>
                        <div class="cell-sub-text">YOUR LEVEL</div>
                    </div>
                    <div class="cell" >
                      <a href="<?php echo base_url('/profile') ?>" class='tooltip-left-box tipsy' title='Total amount of social followers.'></a>
                      <div class="cell-text"><?=$countFollowers?></div>
                      <div class="cell-sub-text">TOTAL FOLLOWERS</div>
                    </div>
                    <div class="cell" >
                        <a href="<?php echo base_url('/stats') ?>" class='tooltip-left-box tipsy' title='Total points are your sales points and bonus points combined. The rankings are based on total points.'></a>
                        <div class="cell-text"><?=intval($totals->total_points)?></div>
                        <div class="cell-sub-text">TOTAL POINTS</div>
                    </div>
                    <div class="cell">
                        <a href="<?php echo base_url('/ranking') ?>" class='tooltip-left-box tipsy' title='This shows what ranking you have earned globally amongst all the PieceKeepers'></a>
                        <div class="cell-text"><?php echo $global_ranking ?></div>
                        <div class="cell-sub-text">GLOBAL RANK</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column-right">

            <div class="header-stats">
              Payouts
            </div>

            <?php if ($this->session->flashdata('paypal_error') != ''): ?>
                <div class='form-alert-wrapper'>
                    <?php echo $this->session->flashdata('paypal_error');  ?>
                </div>
                <br>
            <?php endif ?>

            <div id='change-paypal-form'>
                <img class="float-right" src="<?=base_url('images/paypal-logo.png')?>" alt="PayPal Logo">
                <p>Please enter your PayPal details below:</p>
                <p class="payouts-paypal-account">Do you not have a PayPal account? <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_registration-run" target='_blank'>Click here</a> to Sign Up.</p>

                <form action="<?=base_url('payouts/payment_details')?>" method="post" accept-charset="utf-8">
                  <div class="row collapse">
                      <input class="payouts-input-text" type="text" name="payment_name" placeholder="Name" value="<?php echo $user['payment_name'] ?>" />
                      <input class="payouts-input-email" type="email" name="payment_number" placeholder="Paypal Email" value="<?php echo $user['payment_number'] ?>" />
                      <input type="submit" class="btn-success payouts-paypal-save-submit" value="Update" />
                  </div>
                </form>

                <div class="dotted-line payouts-dotted-line"></div>
            </div>

            <?php if ($user['payment_name'] && $user['payment_number']): ?>
                <div class="payouts-paypal-email">
                    PayPal E-Mail: <?php echo $user['payment_number'] ?><br>
                    <div class="payouts-paypal-email-change" id='change-paypal'><b>Change</b></div>
                </div>
            <?php endif ?>

            <p>Your Commission: <b>$<?php echo number_format($commission, 2, '.', '') ?></b></p>

            <div class="dotted-line payouts-dotted-line"></div>

            <?php if ($this->session->flashdata('invalid_amount') != ''): ?>
                <div class='form-alert-wrapper no-mb'>
                    <div class='alert-box error form-alert'>
                        <?php echo $this->session->flashdata('invalid_amount');  ?>
                    </div>
                </div>
                <br>
            <?php endif ?>

            <?php if ($this->session->flashdata('error') != ''): ?>
              <div class='form-alert-wrapper'>
                  <?php echo $this->session->flashdata('error');  ?>
              </div>
              <br>
            <?php endif ?>

            <?php if ($this->session->flashdata('success_request') != ''): ?>
                <div class='form-alert-wrapper no-mb'>
                    <div class='alert-box success form-alert'>
                        <?php echo $this->session->flashdata('success_request');  ?>
                    </div>
                </div>
                <br>
            <?php endif ?>

            <p>Request Payment Or Giftcard</p>

            <?php if ($this->session->flashdata('request_error') != ''): ?>
                <div class='form-alert-wrapper'>
                    <?php echo $this->session->flashdata('request_error');  ?>
                </div>
                <br>
            <?php endif ?>

            <div class="dynamic-text" class="payouts-request">
                <?php echo $payment_request_content ?>
            </div>

            <form id="payment-form" action="<?=base_url('payouts/request_payment')?>" method="post" accept-charset="utf-8" class="payouts-form-request-payment">
              <div class="row collapse">
                <div class="large-9">
                  <input type="text" name="amount" placeholder="Amount" value="<?php echo number_format($commission, 2, '.', '') ?>" id='gift-input' class="payouts-input-amount"/>
                  <div >
                      <input type='radio' id="giftcard-checkbox" name='payment_method' value='giftcard' checked="checked" /> <div id="gift-bonus"  class="display-inline"><?php echo number_format($commission*(100+$settings['giftcard_extra_commission'])/100, 2, '.', '') ?> $ as&nbsp</div>Giftcard <b>(Get <?php echo $settings['giftcard_extra_commission'] ?>% Extra)</b>
                  </div>
                  <div>
                      <input type='radio' id="paypal-checkbox" name='payment_method' value='paypal' /> <div id='paypal-pay' class="display-inline"><?php echo number_format($commission, 2, '.', '') ?> $ to PayPal account</div>
                  </div>
                  <div id="paypal-info">Please allow up to two weeks for funds to reach your PayPal account.</div>
                  <input id="get-paid" type="submit" class="btn-success save-profile-black payouts-request-payment-submit" value="REQUEST PAYMENT" />
                </div>
              </div>
            </form>

            <div class="dotted-line payouts-dotted-line"></div>
            <img class='float-right' src="<?=base_url('images/paypal-logo.png')?>" alt="PayPal Logo">
            <p>Payments:</p>

            <table>
              <thead>
                <tr>
                  <th style="width:200px;">Amount</th>
                  <th style="width:250px;">Status</th>
                  <th style="width:200px;">Date</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=0; ?>
                <?php foreach ($payments as $p): ?>
                  <?php if ($p->giftcard == 0): ?>
                      <?php $i++; ?>
                      <tr>
                        <td><b><?php echo $p->amount ?>$</b></td>
                        <td>
                          <?php if ($p->aproved == 1): ?>
                            Approved
                          <?php elseif($p->aproved == 0): ?>
                            Pending
                          <?php else: ?>
                             <div class='show-reason payouts-rejected' value="<?php echo $p->id ?>">Rejected</div>
                          <?php endif ?>
                        </td>
                        <td><?php echo $this->users->formatDate($user['id'], strtotime($p->created)) ?></td>
                      </tr>

                      <tr id="<?php echo 'payout-reason-'.$p->id ?>" class='display-none'>
                          <td>"<?php echo $p->reason ?>"</td>
                      </tr>
                  <?php endif ?>
                <?php endforeach ?>

                <?php if ($i==0): ?>
                  <tr>
                    <td colspan='3' >No payments yet.</td>
                  </tr>
                <?php endif ?>
              </tbody>
            </table>


            <div class="dotted-line" style='margin-top: 70px; margin-bottom: 25px;'></div>
            <p>Giftcards:</p>

            <table>
              <thead>
                <tr>
                  <th style="width:200px;">Code</th>
                  <th style="width:250px;">Discount</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php $i=0; ?>
                <?php foreach ($giftcards as $g): ?>
                    <?php $i++; ?>
                    <tr>
                      <td><b><?php echo $g->code ?></b></td>
                      <td><?php echo $g->gift_amount ?>$</td>
                    </tr>
                <?php endforeach ?>

                <?php if ($i==0): ?>
                  <tr>
                    <td colspan='3' >No giftcards yet.</td>
                  </tr>
                <?php endif ?>
              </tbody>
            </table>


        </div>
        <div class="clear"></div>

    </div>
</div>
