<html>
<head>
	<title>Vine Followers</title>
	<link type="text/css" rel="stylesheet" href="<?=base_url('css/style.css')?>" />
</head>
<body>
	<div id="vine-login">
		<div class="title">
			<img src="<?=base_url('images/vine.png')?>" />
		</div>
		<div class="social-error">
		<?php
			if(!empty(form_error('user_id'))) {
				echo form_error('user_id');
			}
			else {
				if(!empty($error)) {
					echo $error;
				}
			}

			$redirect = '/profile';

			// Set where to redirect if the user press cancel button
			if ($this->session->userdata('next') && $this->session->userdata('next') == 'social')
			{
                $this->session->unset_userdata('next');
                $redirect = '/social';
            }
		?>
		</div>
		<form action="<?php echo base_url('/profile/vine') ?>" method='post'>
			<input type='text' name='user_id' placeholder='Vine User ID' /><br>
			<!-- <input type='password' name='password' placeholder='Vine Password' /><br> -->
			<input type='submit' value='Send' />
			<a href="<?php echo base_url($redirect) ?>" class="cancel">Cancel</a>
		</form>
	</div>
</body>
</html>
