<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=161689280703003";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class='content content-padding blog-post'>
    <?php if($post->image) { ?>
      <p><a href='<?= base_url() . "blog/view/" . $post->id ?>'><img src="<?=base_url() . "images/blog/" . $post->image?>" class="blog-post-image" /></a></p>
    <?php } ?>
    <h3><a href='<?= base_url() . "blog/view/" . $post->id ?>'><?php echo $post->name ?></a></h3>
    <?php if ($post->first_name): ?>
      <div class='blog-post-postedby'>Posted by <?php echo $post->first_name." ".$post->last_name ?></div>
    <?php else: ?>
      <div class='blog-post-postedby'>Posted by OnePiece</div>
    <?php endif ?>
    <br>
    <div class='blog-post-description'>
      <?php echo $post->body ?>
    </div>
    <br><br>
    <div class="fb-comments" data-href="<?php echo base_url('/blog/view/'.$post->id) ?>" data-colorscheme="light" data-numposts="5" data-width="The pixel width of the plugin"></div>
</div>
