<div class="content checkout">
  <div class="content-bottom">
    <form action="/products/order" method="post" >
      <div class="column-left">
        <div class="left-points-board height-auto">
          <div class="left-points-board-cells">
            <?php
              if(isset($in_cart_p_id)) {
            ?>
            <div class="cell selected-product" >
              <div class="cell-sub-text">Your selection</div>
              <div class="cell-text">OnePiece <?=$in_cart_p_name?></div>
              <div class="cell-text">Size <?=$in_cart_p_size?></div>
            </div>
            <?php
              }
            ?>
            <div class="cell black">
                <a href="<?php echo base_url('/levels') ?>" class='tooltip-left-box tipsy' title='We have 3 levels in our PieceKeeper program. Reaching a new level gets you free stuff and more points.'></a>
                <div class="cell-text">
                    <?php $lvl = $user['level'] + 3; if($lvl > 3) {$lvl = 3;} ?>
                    <?php echo $lvl ?>
                </div>
                <div class="cell-sub-text">YOUR LEVEL</div>
            </div>
            <div class="cell" >
              <a href="<?php echo base_url('/profile') ?>" class='tooltip-left-box tipsy' title='Total amount of social followers.'></a>
              <div class="cell-text"><?=$countFollowers?></div>
              <div class="cell-sub-text">TOTAL FOLLOWERS</div>
            </div>
            <div class="cell" >
              <a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-left-box tipsy' title='Total points are your sales points and bonus points combined. The rankings are based on total points.'></a>
              <div class="cell-text"><?=intval($totals->total_points)?></div>
              <div class="cell-sub-text">TOTAL POINTS</div>
            </div>
            <div class="cell">
              <a href="<?php echo base_url('/ranking') ?>" class='tooltip-left-box tipsy' title='This shows what ranking you have earned globally amongst all the PieceKeepers'></a>
              <div class="cell-text"><?php echo $global_ranking ?></div>
              <div class="cell-sub-text">GLOBAL RANK</div>
            </div>
          </div>
        </div>
      </div>
      <div class="column-right">
        <div class="large-12 columns standard-address ">
          <div class="row">
              <h2 class="large-12 columns checkout-standard-address-title" >Personal information</h2>
          </div>

          <div class="row">
            <div class="large-6 columns required">
              <label for="email">E-mail address</label>
              <input type="email" id="email" name="email" required value="<?=$user['email']?>" >
            </div>

            <div class="large-6 columns required">
              <label for="phone">Phone number</label>
              <div class="row collapse">
                <div class="small-10 columns required ">
                  <input name="phone" type="tel" value="<?=$user['phone']?>" placeholder="" required="" data-invalid="">
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <h3 class="large-12 columns checkout-shipping-address-title">Shipping address</h3>
          </div>

          <div class="row">
              <div class="large-6 columns required ">
                  <label for="shipping_first_name">First name</label>
                  <input type="text" id="shipping_first_name" name="shipping_first_name" value="<?=$user['first_name']?>" required="" data-invalid="">
              </div>
              <div class="large-6 columns required ">
                  <label for="shipping_last_name">Last name</label>
                  <input type="text" id="shipping_last_name" name="shipping_last_name" value="<?=$user['last_name']?>" required="">
              </div>
          </div>
          <div class="row">
              <div class="large-12 columns required ">
                  <label for="shipping_address_1">Address line 1</label>
                  <input type="text" id="shipping_address_1" name="shipping_address_1" value="<?=$user['address1']?>" required="">
              </div>
          </div>


          <div class="row">
            <div class="large-12 columns">
              <label for="shipping_address_2">Address line 2</label>
              <input type="text" id="shipping_address_2" name="shipping_address_2" value="<?=$user['address2']?>" >
            </div>
          </div>

          <div class="row">
            <div class="large-3 columns required">
              <label for="shipping_zip">Postcode / Zip</label>
              <input type="text" id="shipping_zip" name="shipping_zip" required="required" value="<?=$user['postcode']?>"  >
            </div>
            <div class="large-5 columns required ">
              <label for="shipping_city">City</label>
              <input type="text" id="shipping_city" name="shipping_city" required="" value="<?=$user['city']?>" >
            </div>
            <div class="large-4 columns sac">
              <label for="shipping_access_code">Building Access Code (if needed)</label>
              <input type="text" id="shipping_access_code" name="shipping_access_code" title="For buildings or gated communities">
            </div>
          </div>

          <div class="row">
              <div class="large-12 columns ">
                <label for="shipping_country">Country</label>
                <select id="shipping_country" name="shipping_country" required class="hidden-field" data-id="1421576238305-A4LOH">
                  <option value="" >Select Country</option>
                  <?php

                    foreach ($countries as $country) {

                      ?>

                        <option value="<?=$country->short_name?>" <?php if($country->short_name == $user['country']) echo "selected"; ?> ><?=$country->short_name?></option>

                      <?php

                    }

                  ?>
                </select>
            </div>
          </div>

          <div class="row state-wrapper">
            <div class="large-12 columns ">
              <label for="shipping_state">State/Region</label>
              <select id="shipping_state" name="shipping_state"   class="hidden-field" data-id="1421576238305-A4LOH">
                <option value="" selected>Select state</option>
                <?php

                    foreach ($states as $state) {

                      ?>

                        <option value="<?=$state->state?>"  <?php if($state->state == $user['state']) echo "selected"; ?>  ><?=$state->state?></option>

                      <?php

                    }

                  ?>
              </select>
          </div>
        </div>

        <div class="row">
          <div class="large-12 columns required">
            <input type="submit" name="order" class="btn-success checkout-submit-order" value="ORDER">

            <input type="hidden" name="in_cart_p_id"    id="in_cart_p_id"   value="<?=$in_cart_p_id?>" />
            <input type="hidden" name="in_cart_p_sku"   id="in_cart_p_sku"  value="<?=$in_cart_p_sku?>" />
            <input type="hidden" name="in_cart_p_size"  id="in_cart_p_size" value="<?=$in_cart_p_size?>" />
            <input type="hidden" name="in_cart_p_name"  id="in_cart_p_name" value="<?=$in_cart_p_name?>" />
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
