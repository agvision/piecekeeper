<div class="content maintenance content-maintenance">
	<div class="login-title opacity-bg">
		Piecekeeper is now Brandbassador.com
	</div>
	<div class="login-content opacity-bg">
		We are pleased to announce that PieceKeepers is now live on
		<a href="https://www.brandbassador.com/login/onepiece" class="bb-link">Brandbassador.com!</a>
		It's the PieceKeeper site, just better. Access brand new features on your account
		<a href="https://www.brandbassador.com/login/onepiece" class="bb-link">here</a>.
		<br/>
		<br/>
		For security reasons, you will need to reset your password the first time you log in.
		Once you've reset your password, you'll be all set to explore the new and improved PieceKeepers on <a href="https://www.brandbassador.com/login/onepiece" class="bb-link">Brandbassador.com!</a>
	</div>
</div>
