<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script>
    $(document).ready(function(){
      var ranking = new Array();
      var labels  = new Array();

      ranking[0] = "<?php echo $global_ranking ?>";
      ranking[1] = <?php echo $country_ranking ?>;
      ranking[2] = <?php echo $city_ranking ?>;

      labels[0] = "Global Ranking";
      labels[1] = "Country Ranking";
      labels[2] = "City Ranking";

      var count = 1;

      setInterval(function(){
        $('#ranking-position').html(ranking[count]);
        $('#ranking-label').html(labels[count]);

        count++;
        if (count > 2) {
          count = 0;
        }
      }, 3000);

      jQuery('.date').fdatepicker({format: '<?php echo $this->users->getDatepickerFormat($user["id"]) ?>', weekStart: 1});
    });

  </script>
  <div class="profile">

        <div class="header-middle" id='user-header'>
          <div class="profile-picture">
            <img src="<?=($user['profile_pic'] ? base_url('images/profiles/'.$user['profile_pic']) : base_url('images/profile_picture2.jpg'))?>"  alt="Profile Picture"/>
          </div>
            <a href="<?php echo base_url('ranking') ?>">
              <div class="ranking-box">
                <div class="upper-ranking-box" id='ranking-position'> <?php echo $global_ranking ?> </div>
                  <div class="middle-ranking-box"> <hr /> </div>
                  <div class="bottom-ranking-box" id='ranking-label'> Global Ranking </div>
              </div>
            </a>
            <div class="notification-box">1</div>
            <div class="welcome-message">
              Welcome <br />
                <span class="name"><?=$user['first_name'] . ' ' . $user['last_name']?></span>
              <div class="edit-profile-button">
              <a class="small-btn small-btn-success" href="<?=base_url('/coupons')?>">CREATE DISCOUNT CODE</a>
              </div>
            </div>
            <div class="member-since">
            Member since <?=date('Y', strtotime($user['date_joined']))?>
            </div>
            <div class="clear"></div>
        </div>


        <div class="content">
          <div class="content-bottom">
            <div class="column-left">

              <div class="left-points-board height-auto" >
                <div class="left-points-board height-auto">
                  <div class="left-points-board-cells">
                    <div class="cell black">
                      <a href="<?php echo base_url('/levels') ?>" class='tooltip-left-box tipsy' title='We have 3 levels in our PieceKeeper program. Reaching a new level gets you free stuff and more points.'></a>
                      <div class="cell-text">
                        <?php $lvl = $user['level'] + 3; if($lvl > 3) {$lvl = 3;} ?>
                        <?php echo $lvl ?>
                      </div>
                      <div class="cell-sub-text">YOUR LEVEL</div>
                    </div>
                    <div class="cell" >
                      <a href="<?php echo base_url('/profile') ?>" class='tooltip-left-box tipsy' title='Total amount of social followers.'></a>
                      <div class="cell-text"><?=$countFollowers?></div>
                      <div class="cell-sub-text">TOTAL FOLLOWERS</div>
                    </div>
                    <div class="cell" >
                      <a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-left-box tipsy' title='Total points are your sales points and bonus points combined. The rankings are based on total points.'></a>
                      <div class="cell-text"><?=intval($totals->total_points)?></div>
                      <div class="cell-sub-text">TOTAL POINTS</div>
                    </div>
                    <div class="cell">
                      <a href="<?php echo base_url('/ranking') ?>" class='tooltip-left-box tipsy' title='This shows what ranking you have earned globally amongst all the PieceKeepers'></a>
                      <div class="cell-text"><?php echo $global_ranking ?></div>
                      <div class="cell-sub-text">GLOBAL RANK</div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="column-header visibility-hidden"> </div>

            </div>
            <div class="column-right">


              <div class="header-stats">
                      Personal details
                    </div>
                    <?php if ($this->session->flashdata('picture_upload_false')): ?>
                      <div class="form-alert-wrapper no-mb">
                        <div class="alert-box error form-alert">Please upload a valid image (size < 2MB and format: .gif, .jpg, .jpeg, .png).</div>
                      </div>
                    <?php endif ?>

                    <?php if ($this->session->flashdata('social_not_unique')): ?>
                      <div class="form-alert-wrapper no-mb">
                        <div class="alert-box error form-alert">The social account has already been added in the system.</div>
                      </div>
                    <?php endif ?>


                    <?php

                    $changed_password_state = $this->session->flashdata('changed_password');

                    if ($changed_password_state): ?>
                    <div class="form-alert-wrapper no-mb">
                      <div class="alert-box error form-alert"><?=$changed_password_state?></div>
                    </div>
                  <?php endif ?>

                  <div class="followers">
                    <div id="fb-accounts">
                      <div class="title">Choose a Facebook page</div>
                      <?php if(!empty($fb_pages)): ?>
                        <?php foreach ($fb_pages as $p): ?>
                          <a href="<?php echo base_url('profile/facebook_page/'.$p['id']) ?>" class="account"><?php echo $p['name'] ?></a>
                        <?php endforeach ?>
                       <!--  <div class="front-btn">
                          <a href="/profile/facebook_update">UPDATE LIST</a>
                        </div> -->
                      <?php else: ?>
                          <div>You have no facebook pages to display.</div>
                      <?php endif ?>
                    </div>

                    <div class="followers-box <?php if($facebook_linked) echo 'choose-facebook-registration'; ?>" >
    					<a href="<?php if($facebook_linked) { echo '#'; } else { echo base_url('profile/facebook'); } ?>">
    						<img id="fb-choose-profile" src="<?=base_url('images/facebook_followers.jpg')?>" alt="Facebook Followers"/>
    						<?php if (isset($fb_friends)): ?>
    								<div class='social-friends social-followers-friends'><?php echo $fb_friends ?></div>
    						<?php elseif($facebook_linked): ?>
    								<div class='social-friends social-followers-friends-small'>SELECT</div>
    						<?php else: ?>
    								<div class='social-friends social-followers-friends-small'></div>
    						<?php endif ?>
    					</a>
    				</div>

                    <div class="followers-box">
                      <a href="<?php echo base_url('profile/twitter') ?>"><img src="<?=base_url('images/twitter_followers.jpg')?>"  alt="Twitter Followers" /></a>
                      <?php if (isset($tw_followers)): ?>
                        <div class='social-friends'><?php echo $tw_followers ?></div>
                      <?php endif ?>
                    </div>
                    <div class="followers-box">
                      <a href="<?php echo base_url('profile/instagram') ?>"><img src="<?=base_url('images/instagram_followers.jpg')?>"  alt="Instagram Followers" /></a>
                      <?php if (isset($inst_followers)): ?>
                        <div class='social-friends'><?php echo $inst_followers ?></div>
                      <?php endif ?>
                    </div>
                    <div class="followers-box">
                      <a href="<?php echo base_url('profile/tumblr') ?>"><img src="<?=base_url('images/tumblr_followers.jpg')?>"  alt="Tumblr Followers" /></a>
                      <?php if (isset($tub_followers)): ?>
                        <div class='social-friends'><?php echo $tub_followers ?></div>
                      <?php endif ?>
                    </div>
                    <div class="followers-box">
                      <a href="<?php echo base_url('profile/pinterest') ?>"><img src="<?=base_url('images/pinterest_followers.jpg')?>"  alt="Pinterest Followers"  /></a>
                      <?php if (isset($pin_followers)): ?>
                        <div class='social-friends'><?php echo $pin_followers ?></div>
                      <?php endif ?>
                    </div>
                    <!-- <div class="followers-box">
                      <a href="<?php echo base_url('profile/linkedin') ?>"><img src="<?=base_url('images/in_followers.jpg')?>"  alt="In Followers"  /></a>
                      <?php if (isset($ln_followers)): ?>
                        <div class='social-friends'><?php echo $ln_followers ?></div>
                      <?php endif ?>
                    </div> -->
                    <div class="followers-box">
                      <a href="<?php echo base_url('profile/youtube') ?>"><img src="<?=base_url('images/yb_followers.jpg')?>"  alt="YB Followers"  /></a>
                      <?php if (isset($yb_followers)): ?>
                        <div class='social-friends'><?php echo $yb_followers ?></div>
                      <?php endif ?>
                    </div>
                    <div class="followers-box">
                      <a href="<?php echo base_url('profile/vine') ?>"><img src="<?=base_url('images/vn_followers.jpg')?>"  alt="VN Followers"  /></a>
                      <?php if (isset($vn_followers)): ?>
                        <div class='social-friends'><?php echo $vn_followers ?></div>
                      <?php endif ?>
                    </div>
                    <div class="clear"></div>
                  </div>


                  <?php if (intval($user['master_request']) == 1): ?>
                  <div class='form-label'>
                    Applied as Master PieceKeeper.
                  </div>
                <?php endif ?>
                <br />
                <?=form_open('profile/save/' . $user['id'], array('class' => 'custom', 'enctype' => 'multipart/form-data'))?>
                <div class="row collapse">
                  <div class="small-2 large-3 column">
                    <input class="name-input" name="first_name" type="text" placeholder="First name" value="<?=$user['first_name']?>"/>
                  </div>
                  <div class="small-2 large-3 next-input column">
                    <input class="name-input" name="last_name" type="text" placeholder="Last name" value="<?=$user['last_name']?>"/>
                  </div>
                  <div class="small-1 large-1 column"></div>
                </div>
                <div class="form-label">
                  PROFILE PICTURE
                </div>
                <div class='medium-input'>
                  <input type="file" name="profile_pic" />
                </div>
                <div class="clear"></div>
                <div class="dotted-line"></div>

                <div class='form-label'>
                  WEBSITE
                </div>
                <div class="row collapse">
                  <div class="large-8 column">
                    <input class="name-input" name="website" type="text" placeholder="Blog / Personal Website" value="<?=$user['website']?>"/>
                  </div>
                </div>

                <div class="form-label">
                  ADDRESS
                </div>
                <div class="row collapse">
                  <div class="large-8 column">
                    <input name="address1" type="text" placeholder="Address line 1" value="<?=$user['address1']?>"/>
                  </div>
                </div>
                <div class="row collapse">
                  <div class="large-8 column">
                    <input name="address2" type="text" placeholder="Address line 2" value="<?=$user['address2']?>"/>
                  </div>
                </div>
                <div class="row collapse">
                  <div class="large-4 column">
                    <input name="postcode" type="text" placeholder="Zip Code" value="<?=$user['postcode']?>"/>
                  </div>
                </div>
                <div class="row collapse">
                  <div class="small-2 column">
                    <input name="city" type="text" placeholder="City" value="<?=$user['city']?>"/>
                  </div>
                  <div class="large-2 column next-input">
                    <?php if ( count($countries) > 0 ) { ?>
                      <select name='country'>
                        <?php foreach ($countries as $c): ?>
                          <option value="<?php echo $c->country_id.'_'.$c->short_name ?>" <?php echo ($user['country'] == $c->short_name) ? "selected='selected'" : "" ?>><?php echo $c->short_name ?></option>
                        <?php endforeach ?>
                      </select>
                      <?php } ?>
                    </div>
                    <div class="large-1 column"></div>
                  </div>
                  <div class="dotted-line"></div>
                  <div class="form-label">
                    DATE OF BIRTH
                  </div>
                  <div class="row collapse">
                    <div class="large-2 column">
                      <input name="dob" type="text" placeholder="Birthdate" class='date' value="<?=$this->users->formatDate($user['id'], strtotime($user['dob'])) ?>" />
                    </div>
                    <div class="large-2 column next-input">
                      <select name="gender" >
                        <option value="-1" disabled>Gender</option>
                        <option value="0" <?=($user['gender'] == 0 ? 'SELECTED' : '')?> >Female</option>
                        <option value="1" <?=($user['gender'] == 1 ? 'SELECTED' : '')?> >Male</option>
                      </select>
                    </div>
                    <div class="large-1 column"></div>
                  </div>
                  <div class="dotted-line"></div>
                  <div class="form-label">
                    DID YOU GO TO SCHOOL
                  </div>
                  <div class="row collapse">
                    <div class="large-3 column">
                      <input name="school" type="text" placeholder="Choose school"
                      value="<?=$user['school']?>" />
                    </div>
                    <div class="large-3 column next-input">
                      <input name="graduation_year" type="text" placeholder="Year of graduation"
                      value="<?=$user['graduation_year']?>"/>
                    </div>
                    <div class="large-3 column"></div>
                  </div>
                  <div class="clear"></div>
                  <div class="dotted-line"></div>
                  <div class="form-label">
                    CONTACT INFORMATION
                  </div>
                  <div class="row collapse">
                    <div class="large-3 column">
                      <input name="email" type="text" placeholder="Email"
                      value="<?=$user['email']?>" />
                    </div>
                    <div class="large-3 column next-input">
                      <input name="phone" type="text" placeholder="Phone number"
                      value="<?=$user['phone']?>" />
                    </div>
                    <div class="large-3 column"></div>
                  </div>
                  <div class="dotted-line"></div>
                  <div class="clear"></div>
                  <div class="form-label">
                    FAVORITE ARTIST / CELEBRITY
                  </div>
                  <div class="large-3">
                    <input name="celebrity" type="text" placeholder="e.g.John Doe"
                    value="<?=$user['celebrity']?>"/>
                  </div>
                  <div class="clear"></div>
                  <div class="dotted-line"></div>
                  <div class="form-label">
                    FAVORITE VACANTION SPOT
                  </div>
                  <div class="large-3">
                    <input name="food" type="text" placeholder="e.g.Seafood"
                    value="<?=$user['vacantion']?>" />
                  </div>
                  <div class="clear"></div>
                  <div class="dotted-line"></div>
                  <div class="form-label">
                    FAVORITE ACTIVITY
                  </div>
                  <div class="large-3">
                    <input name="activity" type="text" placeholder="e.g.Couch potato"
                    value="<?=$user['activity']?>"/>
                  </div>
                  <div class="clear"></div>
                  <div class="dotted-line"></div>
                  <div class="form-label">
                    ITEM TO BRING TO A DESERTED ISLAND (YOU ARE ALREADY WEARING A ONEPIECE JUMPSUIT)
                  </div>
                  <div class="large-3">
                    <input name="island" type="text" placeholder="e.g.Old Radio"
                    value="<?=$user['island']?>"/>
                  </div>
                  <div class="clear"></div>
                  <div class="dotted-line"></div>
                  <div class="form-label">
                    CHANGE PASSWORD
                  </div>
                  <div class="row collapse">
                    <div class="small-3 large-3 columns">
                      <span class="prefix">
                        Old password
                      </span>
                    </div>
                    <div class="small-9 large-9 columns">
                      <input name="old-pass" type="password" value="" />
                    </div>
                  </div>
                  <div class="row collapse">
                    <div class="small-3 large-3 columns">
                      <span class="prefix">
                        New password
                      </span>
                    </div>
                    <div class="small-9 large-9 columns">
                      <input name="new-pass" type="password" value="" />
                    </div>
                  </div>
                  <div class="row collapse">
                    <div class="small-3 large-3 columns">
                      <span class="prefix">
                        Retype new password
                      </span>
                    </div>
                    <div class="small-9 large-9 columns">
                      <input name="new-re-pass" type="password" value="" />
                    </div>
                  </div>



                  <div class="dotted-line"></div>
                  <div class="form-label">
                    IF YOU HAD THE POSIBILLITY TO SAY ONE SENTENCE TO THE FOLLOWING
                  </div>
                  <div class="row collapse">
                    <div class="small-3 large-2 columns">
                      <span class="prefix">
                        Jesus
                      </span>
                    </div>
                    <div class="small-9 large-10 columns">
                      <input name="jesus" type="text"
                      value="<?=$user['jesus']?>" />
                    </div>
                  </div>
                  <div class="row collapse">
                    <div class="small-3 large-2 columns">
                      <span class="prefix">
                        Obama
                      </span>
                    </div>
                    <div class="small-9 large-10 columns">
                      <input name="obama" type="text"
                      value="<?=$user['obama']?>" />
                    </div>
                  </div>
                  <div class="row collapse">
                    <div class="small-3 large-2 columns">
                      <span class="prefix">
                        Kim Jong Un
                      </span>
                    </div>
                    <div class="small-9 large-10 columns">
                      <input name="jongun" type="text"
                      value="<?=$user['jongun']?>" />
                    </div>
                  </div>
                  <div class="row collapse">
                    <div class="small-3 large-2 columns">
                      <span class="prefix">
                        Michael Jackson
                      </span>
                    </div>
                    <div class="small-9 large-10 columns">
                      <input name="jackson" type="text"
                      value="<?=$user['jackson']?>" />
                    </div>
                  </div>
                            <?php
                            $i=0;
                            foreach ($questions as $q) {
                              ?>

                              <div class="dotted-line"></div>
                              <div class="form-label">
                                <?=$q->question?>
                              </div>
                              <div class="large-3">
                                <input name="question-ref-<?=$i?>" type="hidden"  value="<?=$q->id?>"/>
                                <input name="question-<?=$q->id?>" type="text" placeholder="Your Answer" value='<?php if($q->answer == "0"){ echo "" ;} else { echo $q->answer;}?>'/>
                              </div>

                              <?php
                              $i++;
                              }

                            foreach ($unansw_questions as $q) {
                              ?>

                              <div class="dotted-line"></div>
                              <div class="form-label">
                                <?=$q->question?>
                              </div>
                              <div class="large-3">
                                <input name="question-ref-<?=$i?>" type="hidden"  value="<?=$q->id?>"/>
                                <input name="question-<?=$q->id?>" type="text" placeholder="Your Answer" value=""/>
                              </div>

                              <?php
                              $i++;
                              }
                            ?>

                            <input type="submit" name="save" class="btn-success save-profile-black" value="UPDATE PROFILE" />
                            <div class="clear"></div>
                      </form>



          </div>
          <div class="clear"></div>
      </div>
    </div>
</div>
