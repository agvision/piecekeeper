</div>
<div>
<section class="content-box">

  <h1>
    <span class="heading">
      Order #<?=$order->order_id?>
    </span>

    <span class="buttons">
      <!-- <a href="http://www.onepiece.ch/en-ch/shop/order/view:54c7a445-5790-4b64-beee-4cc3d9542034" class="preview" target="_blank">
        View
      </a> -->

      <a href="javascript:history.go(-1)" class="back">
        Back
      </a>
    </span>

    <!-- <span class="buttons">
      <form action="./Order  View - OnePiece_files/Order  View - OnePiece.html" method="post" accept-charset="utf-8">

          <input type="submit" name="cancel" value="Cancel order" class="back">

                  </form>

      </span> -->

  </h1>


  <div id="order-view-progress">
    <h2>
      Progress
    </h2>

    <ol>
      <li class="complete">
        Order registered
        <span class="time">
        	<?=date("j M y G:i:s", $order->created)?>
        </span>
      </li>

      <?php if (isset($order->shipments) && count($order->shipments) && isset($order->shipments[0]->tracking_uri) && strlen($order->shipments[0]->tracking_uri)): ?>
        <li class="complete">
          Packing complete
        </li>
      <?php else: ?>
        <li class="incomplete">
          Packing incomplete
        </li>
      <?php endif ?>

      <!-- <li class="complete">
        Sent to packing
        <span class="time">
        	?
        </span>
      </li>
      <li class="incomplete">
        Packing complete
      </li>
      <li class="incomplete">
        Package shipped
      </li> -->
    </ol>

    <div class="tracking">
      Package tracking:
      <em>
        <?php if (isset($order->shipments) && count($order->shipments) && isset($order->shipments[0]->tracking_uri) && strlen($order->shipments[0]->tracking_uri)): ?>
          <a href="<?php echo $order->shipments[0]->tracking_uri ?>" target="_blank">Track</a>
        <?php else: ?>
          Unavailable
        <?php endif ?>
      </em>

    </div>


  </div>

  <div id="order-view-addresses">
    <div id="order-view-shipping-address">
      <h2>
        Shipping address
      </h2>

      <ul>
        <li>
          <?=$order->shipping_first_name." ".$order->shipping_last_name?>
        </li>
        <li>
          <?=$order->shipping_city?>
        </li>
        <li>

        </li>
        <li>
          <?=$order->shipping_address_1?>
        </li>
        <li>

        </li>
        <li>
          <?=$order->country->name."<br/> ".$order->shipping_state?>
        </li>
      </ul>
    </div>

    <div id="order-view-billing-address">
      <h2>
        Billing address
      </h2>

      <ul>
        <li>
          <?=$order->billing_first_name." ".$order->billing_last_name?>
        </li>
        <li>
          <?=$order->billing_city?>
        </li>
        <li>

        </li>
        <li>
          <?=$order->billing_address_1?>
        </li>
        <li>

        </li>
        <li>
          <?=$order->country->name."<br/>".$order->billing_state?>
        </li>
      </ul>
    </div>
  </div>

  <div id="order-view-contact">
    <h2>
      Contact info
    </h2>

    <ul>
      <li>
        <span id="order-email">
          E-mail:
          <a href="mailto:<?=$order->email?>" class="email">
            <?=$order->email?>
          </a>
        </span>
      </li>
      <li>
        Phone: <?=$order->phone?>
      </li>
    </ul>
  </div>




  <div id="order-view-products">
    <h2>
      Products
    </h2>

    <table>
      <thead>
        <tr>
          <th width="100">
          </th>
          <th width="400">
            Product
          </th>
          <th width="200">
            SKU
          </th>
          <th width="100">
            Price
          </th>
          <th width="100">
            Total
          </th>
        </tr>
      </thead>

      <tbody>

        <?php foreach ($order->orderlines as $ol): ?>
          <tr>
            <td><?php echo $ol->quantity ?> x</td>
            <td><?php echo $ol->name ?></td>
            <td><?php echo $ol->sku ?></td>
            <td><?php echo $ol->price." ".$order->currency ?></td>
            <td><?php echo $ol->quantity*$ol->price." ".$order->currency ?></td>
          </tr>
        <?php endforeach ?>

      </tbody>
      <tfoot>
        <tr class="total-row">
          <td colspan="4">
            Order total:
          </td>
          <td class="price-sum">
            <?=$order->total." ".$order->currency?>
          </td>
        </tr>
      </tfoot>
    </table>
  </div>

</section>
