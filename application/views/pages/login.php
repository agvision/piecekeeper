<script type="text/javascript">
	$(document).ready(function(){
		var front_image = "<?php echo $front_image ?>";
		$('body').css('background', "#000000 url('<?php echo base_url('images/front/"+front_image+"') ?>')");
		$('body').css('background-size', 'cover');
		$('body').css('background-position', 'center');
	});
</script>


	<?php if (validation_errors()): ?>
		<div class='front-alerts login-alerts opacity-bg'>
		    <div class='form-alert-wrapper' style='margin-bottom: 0;'>
		      <?php echo validation_errors("<div class='alert-box error form-alert'>", "</div>"); ?>
		    </div>
		</div>
	<?php endif ?>

	<?php if ($this->session->flashdata('error') != ''): ?>
		<div class='front-alerts login-alerts opacity-bg'>
		  <div class='form-alert-wrapper' style='margin-bottom: 0;'>
		      <div class='alert-box error form-alert'>
		          <?php echo $this->session->flashdata('error');  ?>
		      </div>
		  </div>
		</div>
	<?php endif ?>

	<?php if ($this->session->flashdata('success') != ''): ?>
		<div class='front-alerts login-alerts opacity-bg'>
		  <div class='form-alert-wrapper' style='margin-bottom: 0;'>
		      <div class='alert-box success form-alert'>
		          <?php echo $this->session->flashdata('success');  ?>
		      </div>
		  </div>
		</div>
	<?php endif ?>


<div class='front-content'>
	<div class='login-title opacity-bg'>
		BECOME A PIECEKEEPER
	</div>
	<div class='login-content opacity-bg'>
		We're looking for outgoing, social people from all over the globe <br>
		to join our exclusive PieceKeeper network. So if you think you <br>
		have what it takes, got a good network and wanna become a part <br>
		of the OnePiece family, fill out your application now. <br>

		<a href="<?php echo base_url('auth/register') ?>">
			<div class='front-btn' style='margin-top: 12px;'>APPLY NOW</div>
		</a>
	</div>
</div>
