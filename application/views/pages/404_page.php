<div class="content 404 content-404">
	<div class="login-title opacity-bg">
		Page Not Found!
	</div>
	<div class="login-content opacity-bg">
		The page you are looking for does not exist! <br>

        <a href="<?php echo base_url() ?>">
			<div class='front-btn' style='margin-top: 12px;'>piecekeeper.onepiece.com</div>
		</a>
	</div>
</div>
