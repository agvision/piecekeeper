<?php if (isset($errors) && $errors == "username"): ?>
	<p>This YouTube username is not valid.</p>
<?php endif ?>

<?php
	$redirect = '/profile';

	// Set where to redirect if the user press cancel button
	if ($this->session->userdata('next') && $this->session->userdata('next') == 'social')
	{
		$this->session->unset_userdata('next');
		$redirect = '/social';
	}
?>

<form action="<?php echo base_url('/profile/youtube') ?>" method='get'>
	<input type='text' class="youtube-channel" name='username' placeholder='Enter your YouTube Channel' />
	<input type='submit' value='Send' />
	<a href="<?php echo base_url($redirect) ?>">Cancel</a>
</form>
