<?php
	$redirect = '/profile';

	// Set where to redirect if the user press cancel button
	if ($this->session->userdata('next') && $this->session->userdata('next') == 'social')
	{
		$this->session->unset_userdata('next');
		$redirect = '/social';
	}
?>

<form action="<?php echo base_url('/profile/pinterest') ?>" method='get'>
	<input type='text' class="pinterest-request-input-username" name='username' placeholder='Enter your Pinterest Username' />
	<input type='submit' value='Send' />
	<a href="<?php echo base_url($redirect) ?>">Cancel</a>
</form>
