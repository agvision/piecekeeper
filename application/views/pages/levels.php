<script type="text/javascript">
  $(document).ready(function(){
      var ranking = new Array();
      var labels  = new Array();

      ranking[0] = "<?php echo $global_ranking ?>";
      ranking[1] = <?php echo $country_ranking ?>;
      ranking[2] = <?php echo $city_ranking ?>;

      labels[0] = "Global Ranking";
      labels[1] = "Country Ranking";
      labels[2] = "City Ranking";

      var count = 1;

      setInterval(function(){
        $('#ranking-position').html(ranking[count]);
        $('#ranking-label').html(labels[count]);

        count++;
        if (count > 2) {
          count = 0;
        }
      }, 3000);
  });
</script>


    </div>
    <div class="content">
      <div class="content-top">
        <div class="column-left">
              <div class="left-points-board" style='height: auto;'>
                  <div class="left-points-board-cells">
                    <div class="cell black">
                        <a href="<?php echo base_url('/levels') ?>" class='tooltip-left-box tipsy' title='We have 3 levels in our PieceKeeper program. Reaching a new level gets you free stuff and more points.'></a>
                        <div class="cell-text">
                            <?php $lvl = $user['level'] + 3; if($lvl > 3) {$lvl = 3;} ?>
                            <?php echo $lvl ?>
                        </div>
                        <div class="cell-sub-text">YOUR LEVEL</div>
                    </div>
                    <div class="cell" >
                      <a href="<?php echo base_url('/profile') ?>" class='tooltip-left-box tipsy' title='Total amount of social followers.'></a>
                      <div class="cell-text"><?=$countFollowers?></div>
                      <div class="cell-sub-text">TOTAL FOLLOWERS</div>
                    </div>
                    <div class="cell" >
                      <a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-left-box tipsy' title='Total points are your sales points and bonus points combined. The rankings are based on total points.'></a>
                      <div class="cell-text"><?=intval($totals->total_points)?></div>
                      <div class="cell-sub-text">TOTAL POINTS</div>
                    </div>
                    <div class="cell">
                      <a href="<?php echo base_url('/ranking') ?>" class='tooltip-left-box tipsy' title='This shows what ranking you have earned globally amongst all the PieceKeepers'></a>
                      <div class="cell-text"><?php echo $global_ranking ?></div>
                      <div class="cell-sub-text">GLOBAL RANK</div>
                    </div>
                  </div>
                </div>
          </div>
          <div class="column-right">
              <div class="top-points-board">
                <div class="top-cell">
                  <a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-top-box tipsy' title='In addition to cash commission you also get sale points. These points will improve your ranking.'></a>
                  <div class="cell-text"><?=intval($totals->sales_points)?></div>
                  <div class="cell-sub-text">SALE POINTS</div>
                </div>
                <div class="top-cell">
                  <a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-top-box tipsy' title='Take part in our missions and get extra bonus points by doing certain tasks we post within the time limit.'></a>
                  <div class="cell-text"><?=intval($totals->bonus)?></div>
                  <div class="cell-sub-text">BONUS POINTS</div>
                </div>
                <div class="top-cell">
                  <a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-top-box tipsy' title='This shows the number of sales that you have generated.'></a>
                  <div class="cell-text"><?=intval($totals->sales_no)?></div>
                  <div class="cell-sub-text">TOTAL SALES</div>
                </div>
                <div class="top-green-cell">
                  <a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-top-box-green tipsy' title='This is total commission that you have earned. You can either get paid to your PayPal account or get a giftcard.'></a>
                  <div class="cell-text"><?=$this->currencies->format($totals->total_earnings)?></div>
                  <div class="cell-sub-text">TOTAL EARNINGS</div>
                </div>
                <div class="clear"></div>
              </div>
              <div class="right-column-content"><?php echo $details ?></div>
          </div>
          <div class="clear"></div>
      </div>
