<script type="text/javascript">
    $(document).ready(function() {
      var ranking = new Array();
      var labels  = new Array();

      ranking[0] = "<?php echo $global_ranking ?>";
      ranking[1] = <?php echo $country_ranking ?>;
      ranking[2] = <?php echo $city_ranking ?>;

      labels[0] = "Global Ranking";
      labels[1] = "Country Ranking";
      labels[2] = "City Ranking";

      var count = 1;

      setInterval(function(){
        $('#ranking-position').html(ranking[count]);
        $('#ranking-label').html(labels[count]);

        count++;
        if (count > 2) {
          count = 0;
        }
      }, 3000);

      /* Facebook Stuff */
      $.ajaxSetup({ cache: true });
      $.getScript('//connect.facebook.net/en_UK/all.js', function(){
        FB.init({
          appId: '161689280703003',
          channelUrl: '//piecekeeper.onepiece.com/',
        });

        $('.fb-share:not(.done)').click(function(){

            var id   = $(this).attr('value');
            var link = $(this).attr('data-link');
            var desc = $(this).attr('data-description');

            FB.ui(
             {
               method: 'feed',
               link: link,
               description: desc
             },
             function(response) {
               if (response && response.post_id) {
                 logSocialShare(id, "fb");
               }
             }
           );
        });
      });


      /* Twitter Stuff */
      window.twttr = (function (d, s, id) {
          var t, js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return; js = d.createElement(s); js.id = id;
          js.src = "//platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs);
          return window.twttr || (t = { _e: [], ready: function (f) { t._e.push(f) } });
      }(document, "script", "twitter-wjs"));

      twttr.ready(function (twttr) {
        twttr.events.bind('tweet', function (event) {
            var id = $(event.target.parentNode).attr('value');
            logSocialShare(id, "tw");
        });
      });

    });

    function logSocialShare(id_mission, network) {
        $.ajax({
        type: 'POST',
        url:  "<?php echo base_url('/missions/log_social_share_ajax') ?>",
        data: 'id_mission='+id_mission+'&network='+network,
        success: function(data) {
            location.reload();
        }
      });
    }
</script>

    <div class="content">
      <div class="content-bottom">
        <div class="column-left">
              <div class="left-points-board height-auto">
                  <div class="left-points-board-cells">
                    <div class="cell black">
                        <a href="<?php echo base_url('/levels') ?>" class='tooltip-left-box tipsy' title='We have 3 levels in our PieceKeeper program. Reaching a new level gets you free stuff and more points.'></a>
                        <div class="cell-text">
                            <?php $lvl = $user['level'] + 3; if($lvl > 3) {$lvl = 3;} ?>
                            <?php echo $lvl ?>
                        </div>
                        <div class="cell-sub-text">YOUR LEVEL</div>
                    </div>
                    <div class="cell" >
                      <a href="<?php echo base_url('/profile') ?>" class='tooltip-left-box tipsy' title='Total amount of social followers.'></a>
                      <div class="cell-text"><?=$countFollowers?></div>
                      <div class="cell-sub-text">TOTAL FOLLOWERS</div>
                    </div>
                    <div class="cell" >
                      <a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-left-box tipsy' title='Total points are your sales points and bonus points combined. The rankings are based on total points.'></a>
                      <div class="cell-text"><?=intval($totals->total_points)?></div>
                      <div class="cell-sub-text">TOTAL POINTS</div>
                    </div>
                    <div class="cell">
                      <a href="<?php echo base_url('/ranking') ?>" class='tooltip-left-box tipsy' title='This shows what ranking you have earned globally amongst all the PieceKeepers'></a>
                      <div class="cell-text"><?php echo $global_ranking ?></div>
                      <div class="cell-sub-text">GLOBAL RANK</div>
                    </div>
                  </div>
                </div>
          </div>
          <div class="column-right">
              <div class="header-stats"> Missions </div>
			<?php if (!empty($this->session->flashdata('error'))): ?>
                <div class='form-alert-wrapper'>
                    <?php echo $this->session->flashdata('error');  ?>
                </div>
                <br>
            <?php endif ?>

              <div class="dynamic-text"><?php echo $missions_message ?></div>

              <br>

              <form class='missions-sort' method='get' action='#'>
                  <div class='left missions-sort-label'>
                      Show
                  </div>
                  <div class='left missions-sort-selector'>
                      <select name='sort'>
                          <option value=''>All Missions</option>
                          <option value='user' <?php if(isset($_GET['sort']) && $_GET['sort'] == 'user') echo "selected='selected'" ?> >My Missions</option>
                          <option value='completed' <?php if(isset($_GET['sort']) && $_GET['sort'] == 'completed') echo "selected='selected'" ?> >Completed Missions</option>
                          <option value='waiting' <?php if(isset($_GET['sort']) && $_GET['sort'] == 'waiting') echo "selected='selected'" ?> >Waiting Missions</option>
                          <option value='uncompleted' <?php if(isset($_GET['sort']) && $_GET['sort'] == 'uncompleted') echo "selected='selected'" ?> >Uncompleted Missions</option>
                      </select>
                  </div>
                  <button class='small-btn' type='submit'>GO</button>
              </form>

              <?php foreach ($missions as $m): ?>

                  <?php if((!empty($m->min_followers) && empty($m->max_followers) && $countFollowers >= $m->min_followers)
                      || (!empty($m->max_followers) && empty($m->min_followers) && $countFollowers <= $m->max_followers )
                      || (empty($m->min_followers) && empty($m->max_followers))
                      || (!empty($m->min_followers) && !empty($m->max_followers) && $countFollowers >= $m->min_followers && $countFollowers <= $m->max_followers)
                  ): ?>
                  <form action="<?php echo base_url('missions/send') ?>" method='post' enctype='multipart/form-data'>
                  <div class='mission-wrapper'>
                      <div class='status-wrapper'>
                          <div class='mission-status'>
                              <?php if ($m->submitted): ?>
                                  <?php if ($m->status == 0): ?>
                                      <div class='waiting'>WAITING</div>
                                  <?php elseif ($m->status == 1): ?>
                                      <div class='completed'>COMPLETED</div>
                                  <?php elseif ($m->status < 0): ?>
                                      <div class='incomplete'>INCOMPLETE</div>
                                  <?php endif ?>
                              <?php else: ?>
                                  <div class='incomplete'>INCOMPLETE</div>
                              <?php endif ?>
                          </div>
                          <div class='mission-bonus'>
                            <b>Bonus Points:</b> <?php echo $m->points ?>
                          </div>
                      </div>

                      <div class='body-wrapper'>
                          <div class='mission-title'>
                              <?php echo $m->name ?>
                          </div>

                          <?php if ($m->countdown): ?>
                              <div class='mission-expire'>
                                <div class='expire-title'>Expires in:</div>
                                <div class='expire-body'><?php echo $m->remaining['days'] ?> days, <?php echo $m->remaining['hours'] ?> hours, <?php echo $m->remaining['mins'] ?> min</div>
                              </div>
                          <?php endif ?>

                          <div class='mission-description'>
                              <?= $m->description ?>
                          </div>

                          <?php if ($m->video): ?>
                              <div class='mission-video'>
                                  <?php echo $m->video ?>
                              </div>
                          <?php endif ?>

                          <?php if ($m->image): ?>
                              <br>
                              <a href="<?php echo base_url('/images/missions/'.$m->image) ?>" target='_black'>
                                  <img src="<?php echo base_url('/images/missions/'.$m->image) ?>" width="320" alt="Mission Image">
                              </a>
                          <?php endif ?>

                          <?php if ($m->submitted && $m->status == -1): ?>
                              <div class='mission-rejected'>
                                  "<?php echo $m->details ?>"
                              </div>
                          <?php endif ?>

                          <?php if ($m->fb_share && !$m->fb_injected): ?>
                              <?php if ($m->fb_share && !$m->fb_shared): ?>
                                  <div class="fb-link display-none"><?php echo $m->fb_link ?></div>
                                  <div class="fb-text display-none">"<?php echo $m->fb_text ?>"</div>
                                  <div class='fb-share' value="<?php echo $m->id ?>" data-link="<?php echo $m->fb_link ?>" data-description="<?php echo $m->fb_text ?>">
                                      Share on <b>Facebook</b>
                                  </div>
                              <?php endif ?>
                              <?php if($m->fb_share && $m->fb_shared): ?>
                                <div class='fb-share done' style="width: 200px; opacity: 0.6; margin: 0 0 10px 0; float: left;">
                                      You shared on <b>Facebook!</b>
                                </div>
                                <img style="display:inline; margin:5px 0 0 5px;" src="'.base_url('images/check_20.png').'" /><br /><br />
                              <?php endif; ?>
                          <?php endif; ?>

                          <?php if ($m->tw_share && !$m->tw_injected): ?>
                              <?php if ($m->tw_share && !$m->tw_shared): ?>
                                  <div class="fb-link display-none"><?php echo $m->tw_link ?></div>
                                  <div class="fb-text display-none">"<?php echo $m->tw_text ?>"</div>
                                  <div class='tw-share' value="<?php echo $m->id ?>">
                                      <a href="https://twitter.com/intent/tweet?text=<?php echo trim(urlencode($m->tw_text)); ?>&amp;tw_p=tweetbutton&amp;url=<?php echo $m->tw_link ?>" target="_blank">
                                          Share on <b>Twitter</b>
                                      </a>
                                  </div>
                              <?php endif ?>
                              <?php if ($m->tw_share && $m->tw_shared): ?>
                                  <div class='tw-share' style="width: 180px; opacity: 0.6; margin: 0 0 10px 0; float: left;" onclick="return false;">
                                      <a href="#">You shared on <b>Twitter!</b></a>
                                  </div>
                                  <img style="display:inline; margin:5px 0 0 5px;" src="'.base_url('images/check_20.png').'" /><br /><br />
                              <?php endif; ?>
                          <?php endif; ?>

                          <?php if ($m->pin_share && !$m->pin_injected): ?>

                              <?php if ($m->pin_share && !$m->pin_shared): ?>
                                  <div class="fb-link display-none"><?php echo $m->pin_link ?></div>
                                  <div class="fb-text display-none">"<?php echo $m->pin_text ?>"</div>
                                  <div class='pin-share' value="<?php echo $m->id ?>">
                                      <a href="//www.pinterest.com/pin/create/button/?url=<?php echo $m->pin_link ?>&media=<?php echo $m->pin_image ?>&description=<?php echo $m->pin_text ?>" data-pin-do="buttonPin" data-pin-config="none" target='_blank'>
                                          Share on <b>Pinterest</b>
                                      </a>
                                  </div>
                              <?php endif ?>
                              <?php if ($m->pin_share && $m->pin_shared): ?>
                                  <div class='pin-share' style="width: 193px; opacity: 0.6; margin: 0 0 10px 0; float: left;" onclick="return false;">
                                          <a href="#">You shared on <b>Pinterest!</b></a>
                                  </div>
                                  <img style="display:inline; margin:5px 0 0 5px;" src="'.base_url('images/check_20.png').'" /><br /><br />
                              <?php endif; ?>
                          <?php endif; ?>

                          <?php if (!$m->submitted || ($m->submitted && $m->status < 0)): ?>
                              <?php //if (!$m->sales_goals && !$m->fb_share && !$m->tw_share && !$m->pin_share): ?>
                              <div class='send-wrapper' >

                                    <?php if ($m->allow_text && !$m->text_injected): ?>
                                      <?php if ($m->allow_text): ?>
                                          <textarea class='link' name='text' placeholder="<?php echo $m->text_details ?>"></textarea>
                                      <?php endif ?>
                                    <?php endif; ?>

                                    <?php if ($m->allow_link && !$m->link_injected): ?>
                                      <?php if ($m->allow_link): ?>
                                          <input class='link' type='text' name='link' placeholder="<?php echo $m->link_details ?>" />
                                      <?php endif ?>
                                    <?php endif; ?>

                                      <?php if ($m->allow_image): ?>
                                          <input type='file' name='image' />
                                      <?php endif ?>

                                      <?php if ($m->is_post_mission || (!$m->is_sharing_mission && !$m->is_sale_mission)): ?>
                                          <input type='submit' class='btn-black' value='SUBMIT MISSION' />
                                      <?php endif ?>

                                      <input type='hidden' name='mission' value="<?php echo $m->id ?>" />
                                      <input type='hidden' name='trigger' value='send' />

                              </div>
                              <?php //endif ?>
                          <?php endif ?>

                      </div>

                  </div>
                  </form>
                  <?php endif; ?>
              <?php endforeach ?>

          </div>
            <div class="clear"></div>
      </div>
    </div>
