<script src="/js/jquery.clipboard.js"></script>

<div class="content sales-team">
    <div class="content-bottom">
        <div class="column-left">
            <div class="left-points-board height-auto" >
                <div class="left-points-board-cells">
                    <div class="cell black">
                        <a href="<?php echo base_url('/levels') ?>" class='tooltip-left-box tipsy' title='We have 3 levels in our PieceKeeper program. Reaching a new level gets you free stuff and more points.'></a>
                        <div class="cell-text">
                            <?php $lvl = $user['level'] + 3; if($lvl > 3) {$lvl = 3;} ?>
                            <?php echo $lvl ?>
                        </div>
                        <div class="cell-sub-text">YOUR LEVEL</div>
                    </div>
                    <div class="cell" >
                      <a href="<?php echo base_url('/profile') ?>" class='tooltip-left-box tipsy' title='Total amount of social followers.'></a>
                      <div class="cell-text"><?=$countFollowers?></div>
                      <div class="cell-sub-text">TOTAL FOLLOWERS</div>
                    </div>
                    <div class="cell" >
                        <a href="<?php echo base_url('/stats') ?>" class='tooltip-left-box tipsy' title='Total points are your sales points and bonus points combined. The rankings are based on total points.'></a>
                        <div class="cell-text"><?=intval($totals->total_points)?></div>
                        <div class="cell-sub-text">TOTAL POINTS</div>
                    </div>
                    <div class="cell">
                        <a href="<?php echo base_url('/ranking') ?>" class='tooltip-left-box tipsy' title='This shows what ranking you have earned globally amongst all the PieceKeepers'></a>
                        <div class="cell-text"><?php echo $global_ranking ?></div>
                        <div class="cell-sub-text">GLOBAL RANK</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column-right">

            <div class="header-stats">
              <?php echo $underkeepers_title ?>
            </div>

            <?php if ($this->session->flashdata('success') != ''): ?>
            <div class='form-alert-wrapper'>
                <div class='alert-box success form-alert'>
                    <?php echo $this->session->flashdata('success');  ?>
                </div>
            </div>
            <br>
          <?php endif ?>

          <?php if ($this->session->flashdata('error') != ''): ?>
            <div class='form-alert-wrapper'>
                <div class='alert-box error form-alert'>
                    <?php echo $this->session->flashdata('error');  ?>
                </div>
            </div>
            <br>
          <?php endif ?>

          <?php if ($this->session->flashdata('invalid_slave_request') != ''): ?>
            <div class='form-alert-wrapper'>
                <div class='alert-box error form-alert'>
                    <?php echo $this->session->flashdata('invalid_slave_request');  ?>
                </div>
            </div>
            <br>
          <?php endif ?>

            <div class='dynamic-text'>
                <?php echo $underkeepers_content ?>
            </div>

            <?php if ($user['level'] < 1): ?>
                <div id="sales-team-hide">
                    <img class="layout-lock" src="<?php echo base_url("/images/lock.png") ?>">
                    <p>THIS OPTION WILL BE UNLOCKED WHEN YOU REACH LEVEL 3.</p>
                </div>
            <?php endif ?>

            <div id='sales-team-wrapper'>
                  <?php if ($user['slave_invitations_sent'] >= $user['slaves']): ?>

                      <?php if (isset($last_slaves_request->aproved) && $last_slaves_request->aproved == '-1'): ?>
                          <div class='alert-box error no-image'>
                              Your request has been rejected: "<?php echo $last_slaves_request->reason ?>"
                          </div>
                      <?php endif ?>

                      <br>

                      <p>Request more UnderKeepers:</p>

                      <form action="<?=base_url('sales_team/slaves')?>" method="post" accept-charset="utf-8">
                        <div class="row collapse">
                          <div class="large-3">
                            <input type="text" name="slaves" placeholder="number of UnderKeepers" />
                            <input type="submit" class="btn-success save-profile-black sales-team-slaves-requests-submit" value="Request" />
                          </div>
                          <div class="large-3 column"></div>
                        </div>
                      </form>

                      <div class="dotted-line sales-team-dotted-line"></div>
                  <?php endif ?>

                  <p>Invite UnderKeepers:</p>

                  <form action="<?=base_url('sales_team/invite_slave')?>" method="post" accept-charset="utf-8">
                    <div class="row collapse">
                      <div class="large-4">
                        <input type="email" name="email" placeholder="email" />
                        <input type="submit" class="btn-success save-profile-black sales-team-slaves-invite-submit" value="Invite" />
                      </div>
                      <div class="large-4 column"></div>
                    </div>
                  </form>

                  <div class="dotted-line sales-team-dotted-line"></div>
                
                  <?php if (count($invitations)): ?>



                      <p>Pending UnderKeepers</p>

                      <table>
                          <thead>
                            <th width='250'>Email</th>
                            <th width='200'>Register URL</th>
                          </thead>
                      <?php $i=0; ?>
                      <?php foreach ($invitations as $inv): ?>
                          <?php $i++; ?>
                          <tr>
                             <td><?php echo $inv->email ?></td>
                             <td>
                              <a id="reg-url-<?=$i?>" data-inv="<?=$inv->hash?>" href="#">Copy Link</a>
                             </td>
                          </tr>
                      <?php endforeach ?>

                      <?php if ($i==0): ?>
                        <tr>
                          <td colspan='3' >No invitations.</td>
                        </tr>
                      <?php endif ?>

                      </table>

                      <script type="text/javascript">
                        $(document).ready(function(){

                          var len = <?=$i?>;

                          for( var i = 0; i < len ; i++ ) {

                            $('a#reg-url-'+i).clipboard({
                                    path: '/js/jquery.clipboard.swf',
                                    copy: function() {
                                        return "http://piecekeeper.onepiece.com/auth/register?i="+$(this).data("inv");
                                    }
                            });
                          }

                        });
                      </script>

                  <?php endif ?>

                <p>Active UnderKeepers</p>

                <table>
                    <thead>
                    <th width='250'>Name</th>
                    <th width='200'>Email</th>
                    <th width='200'>Join Date</th>
                    </thead>
                    <?php $i=0; ?>
                    <?php foreach ($active_slaves as  $slave): ?>
                        <tr>
                            <td><?php echo $slave->first_name . " " . $slave->last_name ?></td>
                            <td><?php echo $slave->email ?></td>
                            <td><?php echo date('d M y', strtotime($slave->date_joined)) ?></td>
                        </tr>
                    <?php endforeach ?>

                    <?php if (empty($active_slaves)): ?>
                        <tr>
                            <td colspan='3' >You have no active UnderKeepers.</td>
                        </tr>
                    <?php endif ?>

                </table>

            </div>

        </div>
        <div class="clear"></div>
    </div>
</div>
