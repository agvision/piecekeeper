<div class="coupons content">
    <div class="content-bottom">
        <div class="column-left">
            <div class="left-points-board height-auto">
                <div class="left-points-board-cells">
                    <div class="cell black">
                        <a href="<?php echo base_url('/levels') ?>" class='tooltip-left-box tipsy' title='We have 3 levels in our PieceKeeper program. Reaching a new level gets you free stuff and more points.'></a>
                        <div class="cell-text">
                            <?php $lvl = $user['level'] + 3; if($lvl > 3) {$lvl = 3;} ?>
                            <?php echo $lvl ?>
                        </div>
                        <div class="cell-sub-text">YOUR LEVEL</div>
                    </div>
                    <div class="cell" >
                      <a href="<?php echo base_url('/profile') ?>" class='tooltip-left-box tipsy' title='Total amount of social followers.'></a>
                      <div class="cell-text"><?=$countFollowers?></div>
                      <div class="cell-sub-text">TOTAL FOLLOWERS</div>
                    </div>
                    <div class="cell" >
                        <a href="<?php echo base_url('/stats') ?>" class='tooltip-left-box tipsy' title='Total points are your sales points and bonus points combined. The rankings are based on total points.'></a>
                        <div class="cell-text"><?=intval($totals->total_points)?></div>
                        <div class="cell-sub-text">TOTAL POINTS</div>
                    </div>
                    <div class="cell">
                        <a href="<?php echo base_url('/ranking') ?>" class='tooltip-left-box tipsy' title='This shows what ranking you have earned globally amongst all the PieceKeepers'></a>
                        <div class="cell-text"><?php echo $global_ranking ?></div>
                        <div class="cell-sub-text">GLOBAL RANK</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column-right">

                <div class="header-stats">
                    <?php echo $coupons_title ?>
                    <div id="read-more">read more...</div>
                    <div id="read-more-content">
                        <?php echo $coupons_description ?>
                    </div>
                </div>
                <br>

            <!-- Level 1: single use, Level2,3 : multiuse, singleuse -->
            <?php if ($user['level'] < $settings['multiple_discount_level']): ?>
                 <?php $this->load->view('pages/singleuse_form'); ?>
                 <?php $this->load->view('pages/multiuse_form'); ?>
            <?php else: ?>
                <?php $this->load->view('pages/multiuse_form'); ?>
                <br/>
                <?php $this->load->view('pages/singleuse_form'); ?>
            <?php endif ?>




        </div>
        <div class="clear"></div>

    </div>
</div>
