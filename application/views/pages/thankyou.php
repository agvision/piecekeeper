<script type="text/javascript">
	$(document).ready(function(){
		var front_image = "<?php echo $front_image ?>";
		$('body').css('background', "#000000 url('<?php echo base_url('images/front/"+front_image+"') ?>')");
		$('body').css('background-size', 'cover');
		$('body').css('background-position', 'center');
	});
</script>

<div class='front-content thankyou-front-content' >
	<div class='thankyou-title opacity-bg'>
		REGISTER COMPLETE - THANK YOU!
	</div>
	<div class='thankyou-content opacity-bg' id='link-networks'>
		<img src="<?php echo base_url('images/op.png') ?>">
		<span>SOCIAL MEDIA FOLLOWING: <b><?php echo number_format($followers) ?></b></span>
		<span>SOCIAL CREDIT: <b><?php echo $social_credit ?></b></span>
		<span>PIECEKEEPER CODE: <b><?php echo $ipad_coupon ?></b></span>
	</div>

</div>
