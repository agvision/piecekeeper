<script type="text/javascript">
  $(document).ready(function(){
      var ranking = new Array();
      var labels  = new Array();

      ranking[0] = "<?php echo $global_ranking ?>";
      ranking[1] = <?php echo $country_ranking ?>;
      ranking[2] = <?php echo $city_ranking ?>;

      labels[0] = "Global Ranking";
      labels[1] = "Country Ranking";
      labels[2] = "City Ranking";

      var count = 1;

      setInterval(function(){
        $('#ranking-position').html(ranking[count]);
        $('#ranking-label').html(labels[count]);

        count++;
        if (count > 2) {
          count = 0;
        }
      }, 3000);
  });
</script>

    <div class="content">
      <div class="content-bottom">
        <div class="column-left">
              <div class="left-points-board height-auto" >
                  <div class="left-points-board-cells">
                    <div class="cell black">
                        <a href="<?php echo base_url('/levels') ?>" class='tooltip-left-box tipsy' title='We have 3 levels in our PieceKeeper program. Reaching a new level gets you free stuff and more points.'></a>
                        <div class="cell-text">
                            <?php $lvl = $user['level'] + 3; if($lvl > 3) {$lvl = 3;} ?>
                            <?php echo $lvl ?>
                        </div>
                        <div class="cell-sub-text">YOUR LEVEL</div>
                    </div>
                    <div class="cell" >
                      <a href="<?php echo base_url('/profile') ?>" class='tooltip-left-box tipsy' title='Total amount of social followers.'></a>
                      <div class="cell-text"><?=$countFollowers?></div>
                      <div class="cell-sub-text">TOTAL FOLLOWERS</div>
                    </div>
                    <div class="cell" >
                      <a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-left-box tipsy' title='Total points are your sales points and bonus points combined. The rankings are based on total points.'></a>
                      <div class="cell-text"><?=intval($totals->total_points)?></div>
                      <div class="cell-sub-text">TOTAL POINTS</div>
                    </div>
                    <div class="cell">
                      <a href="<?php echo base_url('/ranking') ?>" class='tooltip-left-box tipsy' title='This shows what ranking you have earned globally amongst all the PieceKeepers'></a>
                      <div class="cell-text"><?php echo $global_ranking ?></div>
                      <div class="cell-sub-text">GLOBAL RANK</div>
                    </div>
                  </div>
                </div>
          </div>
          <div class="column-right">
              <div class="header-stats"> <?php echo $ranking_title ?> </div>

              <?php echo $ranking_content ?>

              <div class='ranking-wrapper'>
                <div class='ranking-item'>
                    <div class='ranking-top'>
                        <div class='ranking-value'><?php echo $global_ranking ?></div>
                        <div class='ranking-label'>GLOBAL RANK</div>
                    </div>
                    <?php if ($one_month): ?>
                      <div class="ranking-bottom <?php echo $global_ranking_progress>=0 ? 'ranking-success' : 'ranking-danger' ?>">
                          <div class='ranking-progress'>
                            <?php if ($global_ranking_progress > 0): ?>
                                Up <?php echo $global_ranking_progress ?> last month
                            <?php elseif($global_ranking_progress == 0): ?>
                                Same as last month
                            <?php else: ?>
                                Down <?php echo (-1)*intval($global_ranking_progress) ?> last month
                            <?php endif ?>
                          </div>
                      </div>
                    <?php endif ?>
                </div>
                <div class='ranking-item'>
                    <div class='ranking-top'>
                        <div class='ranking-value'><?php echo $country_ranking ?></div>
                        <div class='ranking-label'>COUNTRY RANK</div>
                    </div>
                    <?php if ($one_month): ?>
                    <div class="ranking-bottom <?php echo $country_ranking_progress>=0 ? 'ranking-success' : 'ranking-danger' ?>">
                        <div class='ranking-progress'>
                          <?php if ($country_ranking_progress > 0): ?>
                              Up <?php echo $country_ranking_progress ?> last month
                          <?php elseif($country_ranking_progress == 0): ?>
                              Same as last month
                          <?php else: ?>
                              Down <?php echo (-1)*intval($country_ranking_progress) ?> last month
                          <?php endif ?>
                        </div>
                    </div>
                    <?php endif ?>
                </div>
                <div class='ranking-item'>
                    <div class='ranking-top'>
                        <div class='ranking-value'><?php echo $city_ranking ?></div>
                        <div class='ranking-label'>CITY RANK</div>
                    </div>
                    <?php if ($one_month): ?>
                      <div class="ranking-bottom <?php echo $city_ranking_progress>=0 ? 'ranking-success' : 'ranking-danger' ?>">
                          <div class='ranking-progress'>
                            <?php if ($city_ranking_progress > 0): ?>
                                Up <?php echo $city_ranking_progress ?> last month
                            <?php elseif($city_ranking_progress == 0): ?>
                                Same as last month
                            <?php else: ?>
                                Down <?php echo (-1)*intval($city_ranking_progress) ?> last month
                            <?php endif ?>
                          </div>
                      </div>
                     <?php endif ?>
                </div>
              </div>

              <p>Top 20 Global PieceKeepers</p>

              <table>
                <thead>
                    <tr>
                      <th>PieceKeeper</th>
                      <th>Country</th>
                      <th>Total Points</th>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach ($top as $t): ?>
                        <tr>
                            <td><?php echo $t->first_name.' '.$t->last_name ?></td>
                            <td><?php echo $t->country ?></td>
                            <td><?php echo $t->total ?></td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
              </table>

          </div>
            <div class="clear"></div>

      </div>
    </div>
