<script type="text/javascript">

	function setInactiveNotification(id_notification, callback) {
			$.ajax({
					type: 'POST',
					url:  "<?php echo base_url('welcome/setInactiveNotification') ?>",
					data: 'id_notification='+id_notification,
					success: function(data) {
							callback();
					}
			});
	}
</script>

<div class="content notifications-history">
		<div class="content-bottom">
				<div class="column-left">
					<div class="left-points-board height-auto">
										<div class="left-points-board-cells">
												<div class="cell black">
														<a href="<?php echo base_url('/levels') ?>" class='tooltip-left-box tipsy' title='We have 3 levels in our PieceKeeper program. Reaching a new level gets you free stuff and more points.'></a>
														<div class="cell-text">
																		<?php $lvl = $user['level'] + 3; if($lvl > 3) {$lvl = 3;} ?>
																		<?php echo $lvl ?>
														</div>
														<div class="cell-sub-text">YOUR LEVEL</div>
												</div>
												<div class="cell" >
														<a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-left-box tipsy' title='Total points are your sales points and bonus points combined. The rankings are based on total points.'></a>
														<div class="cell-text"><?=intval($totals->total_points)?></div>
														<div class="cell-sub-text">TOTAL POINTS</div>
												</div>
												<div class="cell">
														<a href="<?php echo base_url('/ranking') ?>" class='tooltip-left-box tipsy' title='This shows what ranking you have earned globally amongst all the PieceKeepers'></a>
														<div class="cell-text"><?php echo $global_ranking ?></div>
														<div class="cell-sub-text">GLOBAL RANK</div>
												</div>
										</div>
								</div>
						</div>
						<div class="column-right">
								<div class="header-stats"> <?php echo $title ?> </div>

								<?php if (count($notifications)): ?>
										<div id='not-wrapper'>
												<?php foreach ($notifications as $notification): ?>
														<?php $details = $notification->getDetails(); ?>
														<?php if (isset($details['label']) && isset($details['url']) && isset($details['text'])): ?>
																<div class="alert-box no-image user-notification-wrapper <?php if($notification->active) echo 'success'; ?>">
																		<div class='user-notification-message'>
																				<div class='wrapper-label'>
																						<div class='notification-label'><?=$details['label']?></div>
																				</div>
																				<a href="<?=$details['url']?>" class='read-notification' value="<?php echo $notification->id ?>"><?=$details['text']?></a>
																		</div>
																		<div class='user-notification-date'>
																				<?php echo date('d M Y', strtotime($notification->created)) ?> <br> <?php echo date('H:i:s', strtotime($notification->created)) ?>
																		</div>
																</div>
														<?php endif ?>
												<?php endforeach ?>
										</div>

										<div class='pagination-wrapper'>
											<div class='pagination-prev-page'>
												<?php if ($current_page > 1): ?>
													<a href="<?php echo base_url('/notifications/history/'.($current_page-1)) ?>"><< Prev</a>
												<?php endif ?>
											</div>
											<div class='pagination-next-page'>
												<?php if ($current_page < $total_pages): ?>
													<a href="<?php echo base_url('/notifications/history/'.($current_page+1)) ?>">Next >></a>
												<?php endif ?>
											</div>
										</div>
								<?php else: ?>
										<br>
								<?php endif ?>
						</div>
						<div class="clear"></div>

		</div>
</div>
