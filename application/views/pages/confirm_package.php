

    <div class="content checkout">
      <div class="content-bottom">
        <div class="column-left">
              <div class="left-points-board" style='height: auto;'>
                  <div class="left-points-board-cells">
                    <div class="cell black">
                        <a href="<?php echo base_url('/levels') ?>" class='tooltip-left-box tipsy' title='We have 3 levels in our PieceKeeper program. Reaching a new level gets you free stuff and more points.'></a>
                        <div class="cell-text">
                            <?php $lvl = $user['level'] + 3; if($lvl > 3) {$lvl = 3;} ?>
                            <?php echo $lvl ?>
                        </div>
                        <div class="cell-sub-text">YOUR LEVEL</div>
                    </div>
                    <div class="cell" >
                      <a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-left-box tipsy' title='Total points are your sales points and bonus points combined. The rankings are based on total points.'></a>
                      <div class="cell-text"><?=intval($totals->total_points)?></div>
                      <div class="cell-sub-text">TOTAL POINTS</div>
                    </div>
                    <div class="cell">
                      <a href="<?php echo base_url('/ranking') ?>" class='tooltip-left-box tipsy' title='This shows what ranking you have earned globally amongst all the PieceKeepers'></a>
                      <div class="cell-text"><?php echo $global_ranking ?></div>
                      <div class="cell-sub-text">GLOBAL RANK</div>
                    </div>
                  </div>
                </div>
          </div>
          <div class="column-right">
              <div class="header-stats" style='padding-bottom: 0px;'> <?php echo $page_message ?> </div>
                <form action="<?= base_url('packages/gift')?>" method="post" name="checkout_form">
                    <div class="large-12 columns standard-address ">
                      <div class="row">
                          <h2 class="large-12 columns checkout-standard-address-title" >Personal information</h2>
                      </div>

                      <div class="row">
                          <?php if ($this->session->flashdata('package_error')): ?>
                              <div class='form-alert-wrapper'>
                                  <div class='alert-box error form-alert'>
                                      <?=count($this->session->flashdata('package_error')) == 1 ? 'An error has occurred: ' : "Some errors occurred: "?>
                                      <br />
                                          <?php foreach($this->session->flashdata('package_error') as $err): ?>
                                             &nbsp; - &nbsp;&nbsp;&nbsp;<?=$err?><br />
                                          <?php endforeach; ?>

                                  </div>
                              </div>
                              <br>
                          <?php endif ?>

                        <div class="large-6 columns required">
                          <label for="email">E-mail address</label>
                          <input type="email" id="email" name="email" required value="<?=$user['email']?>" >
                        </div>

                        <div class="large-6 columns required">
                          <label for="phone">Phone number</label>
                          <div class="row collapse">
                            <div class="small-10 columns required ">
                              <input name="phone" type="tel" value="<?=$user['phone']?>" placeholder="" required="" data-invalid="">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <h3 class="large-12 columns checkout-shipping-address-title">Shipping address</h3>
                      </div>

                      <div class="row">
                          <div class="large-6 columns required ">
                              <label for="shipping_first_name">First name</label>
                              <input type="text" id="shipping_first_name" name="shipping_first_name" value="<?=$user['first_name']?>" required="" data-invalid="">
                          </div>
                          <div class="large-6 columns required ">
                              <label for="shipping_last_name">Last name</label>
                              <input type="text" id="shipping_last_name" name="shipping_last_name" value="<?=$user['last_name']?>" required="">
                          </div>
                      </div>
                      <div class="row">
                          <div class="large-12 columns required ">
                              <label for="shipping_address_1">Address line 1</label>
                              <input type="text" id="shipping_address_1" name="shipping_address_1" value="<?=$user['address1']?>" required="">
                          </div>
                      </div>


                      <div class="row">
                        <div class="large-12 columns">
                          <label for="shipping_address_2">Address line 2</label>
                          <input type="text" id="shipping_address_2" name="shipping_address_2" value="<?=$user['address2']?>" >
                        </div>
                      </div>

                      <div class="row">
                        <div class="large-3 columns required">
                          <label for="shipping_zip">Postcode / Zip</label>
                          <input type="text" id="shipping_zip" name="shipping_zip" required="required" value="<?=$user['postcode']?>"  >
                        </div>
                        <div class="large-5 columns required ">
                          <label for="shipping_city">City</label>
                          <input type="text" id="shipping_city" name="shipping_city" required="" value="<?=$user['city']?>" >
                        </div>
                        <div class="large-4 columns sac">
                          <label for="shipping_access_code">Building Access Code (if needed)</label>
                          <input type="text" id="shipping_access_code" name="shipping_access_code" title="For buildings or gated communities">
                        </div>
                      </div>

                      <div class="row">
                          <div class="large-12 columns ">
                            <label for="shipping_country">Country</label>
                            <select id="shipping_country" name="shipping_country" required class="hidden-field" data-id="1421576238305-A4LOH">
                              <option value="" >Select Country</option>
                              <?php

                                foreach ($countries as $country) {

                                  ?>

                                    <option value="<?=$country->short_name?>" <?php if($country->short_name == $user['country']) echo "selected"; ?> ><?=$country->short_name?></option>

                                  <?php

                                }

                              ?>
                            </select>
                        </div>
                      </div>

                      <div class="row state-wrapper">
                        <div class="large-12 columns ">
                          <label for="shipping_state">State/Region</label>
                          <select id="shipping_state" name="shipping_state"   class="hidden-field" data-id="1421576238305-A4LOH">
                            <option value="" selected>Select state</option>
                            <?php

                                foreach ($states as $state) {

                                  ?>

                                    <option value="<?=$state->state?>"  <?php if($state->state == $user['state']) echo "selected"; ?>  ><?=$state->state?></option>

                                  <?php

                                }

                              ?>
                          </select>
                      </div>
                    </div>

                    <div class="row">
                      <div class="large-12 columns required">
                        <input type="submit" name="confirm" class="btn-success checkout-submit-order" value="CONFIRM">
                      </div>
                    </div>
                  </div>
                </form>


          </div>
            <div class="clear"></div>

      </div>
    </div>
