<script type="text/javascript">
  $(document).ready(function(){

      var count_notifications = <?php echo count($notifications); ?>;

      if (count_notifications == 1){
          $('#not-wrapper').css('margin-bottom', '10px');
          $('#not-wrapper').css('height', '38px');
      }
      else if (count_notifications == 2){
          $('#not-wrapper').css('margin-bottom', '10px');
          $('#not-wrapper').css('height', '76px');
      }
      else if (count_notifications == 3){
          $('#not-wrapper').css('margin-bottom', '10px');
      }

      // clear all notifications
      var ids_notifications = new Array();

      <?php $i=0; ?>
      <?php foreach ($notifications as $n): ?>
          ids_notifications[<?php echo $i ?>] = <?php echo $n->id ?>;
          <?php $i++; ?>
      <?php endforeach ?>

      $('#clear-all-notifications').click(function(){

          $(this).css('cursor', 'progress');

          clearAllNotifications();
      });

      // display total and giftcard earnings
      var total_earnings_value    = "<?php echo $total_earnings ?>";
      var giftcard_earnings_value = "<?php echo $giftcard_earnings ?>";
      var total_earnings_label    = "EARNINGS (CASH)";
      var giftcard_earnings_label = "EARNINGS (GIFT CARD)";

      var i = 0;
      setInterval(function(){
        if(i % 2 == 0){
          $("#total-earnings-value").html(giftcard_earnings_value);
          $("#total-earnings-label").html(giftcard_earnings_label);
        } else {
          $("#total-earnings-value").html(total_earnings_value);
          $("#total-earnings-label").html(total_earnings_label);
        }
        i++;
      }, 2000);

      // display ranking
      var ranking = new Array();
      var labels  = new Array();

      ranking[0] = "<?php echo $global_ranking ?>";
      ranking[1] = <?php echo $country_ranking ?>;
      ranking[2] = <?php echo $city_ranking ?>;

      labels[0] = "Global Ranking";
      labels[1] = "Country Ranking";
      labels[2] = "City Ranking";

      var count = 1;
      setInterval(function(){
        $('#ranking-position').html(ranking[count]);
        $('#ranking-label').html(labels[count]);

        count++;
        if (count > 2) {
          count = 0;
        }
      }, 3000);



      var display = 0;
      var text = "SHOW MORE (<?php echo count($notifications)-3 ?>)";

      $('#user-notifications-handler').click(function(){
          var handler = this;
          if(display == 0){
              var full_height = 38 * <?php echo count($notifications) ?>;
              $('#not-wrapper').animate({height : full_height+"px"}, 500, function(){
                display = 1;
                $(handler).html('HIDE');
                $('#clear-all-notifications').show();
              });
          } else {
              $('#not-wrapper').animate({height : "112px"}, 500, function(){
                display = 0;
                $(handler).html(text);
                $('#clear-all-notifications').hide();
              });
          }
      });
  });

  function setInactiveNotification(id_notification, callback) {
      $.ajax({
          type: 'POST',
          url:  "<?php echo base_url('welcome/setInactiveNotification') ?>",
          data: 'id_notification='+id_notification,
          success: function(data) {
              callback();
          }
      });
  }

  function clearAllNotifications() {
      $.ajax({
          type: 'POST',
          url:  "<?php echo base_url('welcome/clearAllNotifications') ?>",
          data: '',
          success: function(data) {
              window.location.reload();
          }
      });
  }

</script>
<div class="main">
        <div class="header-middle">
          <div class="profile-picture">
          <img src="<?=($user['profile_pic'] ? base_url('images/profiles/'.$user['profile_pic']) : base_url('images/profile_picture2.jpg'))?>" alt="User Profile Picture" />
            </div>
            <a href="<?php echo base_url('ranking') ?>">
              <div class="ranking-box">
                <div class="upper-ranking-box" id='ranking-position'> <?php echo $global_ranking ?> </div>
                  <div class="middle-ranking-box"> <hr /> </div>
                  <div class="bottom-ranking-box" id='ranking-label'> Global Ranking </div>
              </div>
            </a>
            <div class="notification-box">1</div>
            <div class="welcome-message">
              Welcome <br />
              <span class="name"><?=$user['first_name'] . ' '. $user['last_name']?></span><br>
              <a class="small-btn small-btn-success" href="<?=base_url('/coupons')?>">CREATE DISCOUNT CODE</a>
            </div>

            <?php if ($user['level'] == -2 || $user['level'] == -1): ?>
                <div class='level-progress'>
                    <div class='level-title'>TO REACH LEVEL <?php echo $user['level'] + 4 ?></div>
                    <div class='bar-wrapper'>
                        <div class='bar'>
                          <?php if ($user_sales > 0): ?>
                              <div class='progression' style='width:<?php echo $sales_width ?>px;'><?php echo $user_sales ?></div>
                          <?php else: ?>
                              <div class='progression main-no-level-progression'></div>
                          <?php endif ?>
                          <div class='bar-goals'><?php echo $sales_goal ?></div>
                        </div>
                        <div class='bar-details'>SALES</div>
                    </div>
                    <div class='bar-wrapper'>
                        <div class='bar'>
                          <?php if ($user_points > 0): ?>
                              <div class='progression' style='width:<?php echo $points_width ?>px;'><?php echo $user_points ?></div>
                          <?php else: ?>
                              <div class='progression main-no-level-progression'></div>
                          <?php endif ?>
                          <div class='bar-goals'><?php echo $points_goal ?></div>
                        </div>
                        <div class='bar-details'>TOTAL POINTS</div>
                    </div>
                </div>
            <?php elseif($user['level'] == 0 && ($user['master_request'] == 0 || $user['master_request'] == 1)): ?>
                  <div class="level-progress">
                      <div class="level-title master">CONGRATULATIONS!</div>
                      <div class="message">You've reached the top level and you can get a sales team under you where you earn 20% of all their commission!</div>
                      <?php if ($user['master_request'] == 0): ?>
                          <a href="<?php echo base_url('/profile/master_apply') ?>" class="btn btn-success">APPLY</a>
                      <?php else: ?>
                          <div class="btn btn-pending">PENDING</div>
                      <?php endif ?>
                  </div>
            <?php endif ?>

            <div class="clear"></div>
        </div>
    </div>
    <div class="content">
      <div class="content-top">
        <div class="column-left">
              <div class="left-points-board height-auto">
                  <div class="left-points-board-cells">
                    <div class="cell black">
                        <a href="<?php echo base_url('/levels') ?>" class='tooltip-left-box tipsy' title='We have 3 levels in our PieceKeeper program. Reaching a new level gets you free stuff and more points.'></a>
                        <div class="cell-text">
                            <?php $lvl = $user['level'] + 3; if($lvl > 3) {$lvl = 3;} ?>
                            <?php echo $lvl ?>
                        </div>
                        <div class="cell-sub-text">YOUR LEVEL</div>
                    </div>
                    <div class="cell" >
                      <a href="<?php echo base_url('/profile') ?>" class='tooltip-left-box tipsy' title='Total amount of social followers.'></a>
                      <div class="cell-text"><?=$countFollowers?></div>
                      <div class="cell-sub-text">TOTAL FOLLOWERS</div>
                    </div>
                    <div class="cell" >
                      <a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-left-box tipsy' title='Total points are your sales points and bonus points combined. The rankings are based on total points.'></a>
                      <div class="cell-text"><?=intval($totals->total_points)?></div>
                      <div class="cell-sub-text">TOTAL POINTS</div>
                    </div>
                    <div class="cell">
                      <a href="<?php echo base_url('/ranking') ?>" class='tooltip-left-box tipsy' title='This shows what ranking you have earned globally amongst all the PieceKeepers'></a>
                      <div class="cell-text"><?php echo $global_ranking ?></div>
                      <div class="cell-sub-text">GLOBAL RANK</div>
                    </div>
                  </div>
                </div>

          </div>
          <div class="column-right">
              <div class="top-points-board">
                  <div class="top-cell">
                    <a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-top-box tipsy' title='In addition to cash commission you also get sale points. These points will improve your ranking.'></a>
                    <div class="cell-text"><?=intval($totals->sales_points)?></div>
                    <div class="cell-sub-text">SALE POINTS</div>
                  </div>
                  <div class="top-cell">
                    <a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-top-box tipsy' title='Take part in our missions and get extra bonus points by doing certain tasks we post within the time limit.'></a>
                    <div class="cell-text"><?=intval($totals->bonus)?></div>
                    <div class="cell-sub-text">BONUS POINTS</div>
                  </div>
                  <div class="top-cell">
                    <a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-top-box tipsy' title='This shows the number of sales that you have generated.'></a>
                    <div class="cell-text"><?=intval($totals->sales_no)?></div>
                    <div class="cell-sub-text">TOTAL SALES</div>
                  </div>
                  <div class="top-green-cell">
                    <a href="<?php echo base_url('/payouts') ?>" class='tooltip-top-box-green tipsy' title='This is total commission that you have earned. You can either get paid to your PayPal account or get a giftcard.'></a>
                    <div class="cell-text" id="total-earnings-value"><?php echo $total_earnings ?></div>
                    <div class="cell-sub-text" id="total-earnings-label">EARNINGS (CASH)</div>
                  </div>
                  <div class="clear"></div>
                </div>

                <!-- Package notification -->
                <?php if (count($notifications)): ?>
                    <div>
                      <?php foreach ($notifications as $notification): ?>
                          <?php if($notification->type == 15 || $notification->type == 16): ?>
                            <?php  $details = $notification->getDetails(); ?>
                              <div class="package-notification">
                                  <div class="fa-gift"></div>
                                  <a href="<?=$details['url']?>" class='read-notification' value="<?php echo $notification->id ?>">
                                    <div><?=$details['text']?></div>
                                  </a>
                              </div>
                          <?php endif ?>
                      <?php endforeach ?>
                    </div>
                <?php endif ?>



                <div class="main-notification-info">
                    <?php if (count($notifications)): ?>
                        Notifications
                        <div class="main-notification-info__count">(<?php echo count($notifications) ?> new)</div>
                    <?php else: ?>
                        No new notifications right now.
                    <?php endif ?>
                    <div class="main-notification-info__view-all">
                        <a href="<?php echo base_url('/notifications/history/') ?>">View All</a>
                    </div>
                </div>

                <?php if (count($notifications)): ?>
                    <div class="column-picture" id='not-wrapper'>
                      <?php foreach ($notifications as $notification): ?>
                            <?php  $details = $notification->getDetails(); ?>

                              <div class="alert-box no-image <?php if($notification->active) echo 'success'; ?>">
                                <div class='wrapper-label'>
                                  <div class='notification-label'><?=$details['label']?></div>
                                </div>
                                    <a href="<?=$details['url']?>" class='read-notification' value="<?php echo $notification->id ?>"><?=$details['text']?></a>
                                    <div class='delete-notification' value="<?php echo $notification->id ?>">x</div>
                              </div>

                      <?php endforeach ?>
                    </div>
                <?php else: ?>
                  <br>
                <?php endif ?>

                <?php if (count($notifications) > 3): ?>
                    <div id='user-notifications-handler' class='btn-black'>SHOW MORE (<?php echo count($notifications)-3 ?>)</div>
                <?php endif ?>

                <?php if (count($notifications) > 3): ?>
                    <div id="clear-all-notifications" class='btn-black'>CLEAR ALL</div>
                <?php endif ?>

                <div class="right-column-content">

                    <div class='featured-blog'>
                      <?php if ($featured_blog): ?>
                          <?php if ($featured_blog->youtube_embed): ?>
                              <iframe width="770" height="480" src="<?php echo str_replace("watch?v=", "embed/", $featured_blog->youtube_embed) ?>" frameborder="0" allowfullscreen></iframe>
                          <?php else: ?>
                              <a href="<?php echo base_url('blog/view/'.$featured_blog->id) ?>">
                                <img src="<?php echo base_url('/images/blog/'.$featured_blog->image) ?>" width='770' alt="Featured Blog Image">
                                <div class='blog-title'><?php echo $featured_blog->name ?></div>
                              </a>
                          <?php endif ?>
                          <div class='blog-desc'><?php echo $featured_blog->short_description ?></div>
                      <?php endif ?>
                    </div>
                    <div class='bottom-left-blog'>
                      <?php if ($bottom_left_blog): ?>
                          <?php if ($bottom_left_blog->youtube_embed): ?>
                              <iframe width="375" height="250" src="<?php echo str_replace("watch?v=", "embed/", $bottom_left_blog->youtube_embed) ?>" frameborder="0" allowfullscreen></iframe>
                          <?php else: ?>
                              <a href="<?php echo base_url('/blog/view/'.$bottom_left_blog->id) ?>">
                                <img src="<?php echo base_url('/images/blog/'.$bottom_left_blog->image) ?>" width='375' alt="Blog Image">
                                <div class='blog-title'><?php echo $bottom_left_blog->name ?></div>
                              </a>
                          <?php endif ?>
                          <div class='blog-desc'><?php echo $bottom_left_blog->short_description ?></div>
                      <?php endif ?>
                    </div>
                    <div class='bottom-right-blog'>
                      <?php if ($bottom_right_blog): ?>
                          <?php if ($bottom_right_blog->youtube_embed): ?>
                              <iframe width="375" height="250" src="<?php echo str_replace("watch?v=", "embed/", $bottom_right_blog->youtube_embed) ?>" frameborder="0" allowfullscreen></iframe>
                          <?php else: ?>
                              <a href="<?php echo base_url('/blog/view/'.$bottom_right_blog->id) ?>">
                                <img src="<?php echo base_url('/images/blog/'.$bottom_right_blog->image) ?>" width='375' alt="Blog Image">
                                <div class='blog-title'><?php echo $bottom_right_blog->name ?></div>
                              </a>
                          <?php endif ?>
                          <div class='blog-desc'><?php echo $bottom_right_blog->short_description ?></div>
                      <?php endif ?>
                    </div>

                </div>
          </div>
            <div class="clear"></div>
      </div>
</div>
