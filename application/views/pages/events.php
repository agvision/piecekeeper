<?php if ($user['level'] >= 1): ?>
    <a href="<?php echo base_url('events/add') ?>" class='btn-black events-btn-add'> Add Event</a>
<?php endif ?>

<div class='content content-padding blog-post'>
    <?php foreach ($events as $post): ?>
        <h3><a href='<?= base_url() . "events/view/" . $post->id ?>'><?php echo $post->name ?></a></h3>
        <p>Date: <?=$post->date?> |
        <?php if ($post->first_name): ?>
            Posted by <?php echo $post->first_name." ".$post->last_name ?></p>
        <?php else: ?>
            Posted by OnePiece</p>
        <?php endif ?>

        <?php if($post->image) { ?>
            <p><a href='<?= base_url() . "events/view/" . $post->id ?>'><img src="<?=base_url() . "images/event/" . $post->image?>" class="events-image" /></a></p>
        <?php } ?>
        <p>
            <?php echo $post->short_description ?>
        </p>
        <br>
    <?php endforeach ?>
</div>
