<div class="content maintenance content-maintenance">
	<div class="login-title opacity-bg">
		Currently down for maintanace
	</div>
	<div class="login-content opacity-bg">
        We are sorry for the inconvenience but we&rsquo;re performing maintenance at the moment.<br/>

        <a href="<?=base_url()?>">
            <div class='front-btn' style='margin-top: 12px;'>piecekeeper.onepiece.com</div>
        </a>
	</div>
</div>
