<div class="content">
  <div class="content-bottom">
    <div class="column-left">
      <div class="left-points-board height-auto">
        <div class="left-points-board-cells">
          <?php
            $this->load->model('Api_model', 'Api');
            if(!isset($in_cart_p_id)) {

              $in_cart_p_name_text = "Select a product!";
              $in_cart_p_size_text = "";
              $order_btn = "";

            } else {

              $in_cart_p_name_text = "OnePiece ".$in_cart_p_name;
              $in_cart_p_size_text = "Size ".$in_cart_p_size;
              $order_btn = "<a class=\"small-btn small-btn-success\" href=\"".base_url()."products/checkout\" class=\"full-opacity\">ORDER</a>";

            }

          ?>

          <div class="cell selected-product" >
            <div class="cell-sub-text">Your selection</div>
            <div class="cell-text"><?=$in_cart_p_name_text?></div>
            <div class="cell-text"><?=$in_cart_p_size_text?></div>
            <?=$order_btn?>
          </div>

          <div class="cell black">
              <a href="<?php echo base_url('/levels') ?>" class='tooltip-left-box tipsy' title='We have 3 levels in our PieceKeeper program. Reaching a new level gets you free stuff and more points.'></a>
              <div class="cell-text">
                  <?php $lvl = $user['level'] + 3; if($lvl > 3) {$lvl = 3;} ?>
                  <?php echo $lvl ?>
              </div>
              <div class="cell-sub-text">YOUR LEVEL</div>
          </div>
          <div class="cell" >
            <a href="<?php echo base_url('/profile') ?>" class='tooltip-left-box tipsy' title='Total amount of social followers.'></a>
            <div class="cell-text"><?=$countFollowers?></div>
            <div class="cell-sub-text">TOTAL FOLLOWERS</div>
          </div>
          <div class="cell" >
            <a href="<?php echo base_url('/profile#tabs-6') ?>" class='tooltip-left-box tipsy' title='Total points are your sales points and bonus points combined. The rankings are based on total points.'></a>
            <div class="cell-text"><?=intval($totals->total_points)?></div>
            <div class="cell-sub-text">TOTAL POINTS</div>
          </div>
          <div class="cell">
            <a href="<?php echo base_url('/ranking') ?>" class='tooltip-left-box tipsy' title='This shows what ranking you have earned globally amongst all the PieceKeepers'></a>
            <div class="cell-text"><?php echo $global_ranking ?></div>
            <div class="cell-sub-text">GLOBAL RANK</div>
          </div>
        </div>
      </div>
    </div>
    <div class="column-right">

      <p><?php echo $products_info ?></p>

      <?php

      $babies_products = array();
      $kids_products = array();

      if(!empty($products))
      {
        foreach ($products as $product) {

          if( strpos(strtolower($product->name), "baby") !== false ) {
            $babies_products[] = $product;
            continue;
          }

          if( strpos(strtolower($product->name), "kid") !== false ) {
            $kids_products[] = $product;
            continue;
          }

          $in_cart = false;

          if(isset($in_cart_p_id) && $product->id == $in_cart_p_id ) {
            $in_cart = true;
          }

          ?>

          <div class="product">

            <a target="blank" href="<?=$product->producturl?>" >

              <img src="<?=$product->image?>" />

            </a>
            <div class="product-mini-btns-wrapper" >
              <div  class="product-mini-btns"
                    data-id="<?=$product->id?>"

                    <?php

                      foreach ($product->options as $size => $size_value) {

                        $valid_size = false;

                        foreach ($size_value as $key => $value) {

                          if($this->Api->isValidSize($value->name)) {

                            $valid_size = true;

                          }

                        }

                        $valid_size = true;
                        if($valid_size) {

                          ?>

                          data-sku<?=strtolower(str_replace("/", "", $size))?>="<?=$product->{'sku_'.$size}?>"

                          <?php

                        }
                      }

                    ?>
                    data-price="<?=$product->price?>"
                    data-name="<?=$product->name?>"><?php

                      foreach ($product->options as $size => $size_value) {

                        $valid_size = false;

                        foreach ($size_value as $key => $value) {

                          if($this->Api->isValidSize($value->name)) {

                            $valid_size = true;

                          }

                        }

                        if( $valid_size && $product->stock[$size] >= 10 ) {

                          $sel_class = "";

                          if($in_cart && $in_cart_p_size == $size ) {

                            $sel_class = "selected";

                          }

                          ?><div class="product-mini-btn <?=$sel_class?>"><?=$size?></div><?php

                        }
                      }

                    ?>

              </div>
            </div>

            <p class="product-title" ><?=$product->name?></p>

          </div>

          <?php

        }
      }
      else
      {
          ?>
          <div>
            No available products.
          </div>
          <?php
      }

      foreach ($kids_products as $product) {

        $in_cart = false;

        if(isset($in_cart_p_id) && $product->id == $in_cart_p_id ) {

          $in_cart = true;

        }

        ?>

        <div class="product">

          <a target="blank" href="<?=$product->producturl?>" >

            <img src="<?=$product->image?>" />

          </a>
          <div class="product-mini-btns-wrapper" >
            <div  class="product-mini-btns"
                  data-id="<?=$product->id?>"

                  <?php

                    foreach ($product->options as $size => $size_value) {

                      $valid_size = false;

                      foreach ($size_value as $key => $value) {

                        if($this->Api->isValidSize($value->name)) {

                          $valid_size = true;

                        }

                      }


                      if($valid_size) {

                        ?>

                        data-sku<?=strtolower(str_replace("/", "", $size))?>="<?=$product->{'sku_'.$size}?>"

                        <?php

                      }
                    }

                  ?>
                  data-price="<?=$product->price?>"
                  data-name="<?=$product->name?>"><?php

                    foreach ($product->options as $size => $size_value) {

                      $valid_size = false;

                      foreach ($size_value as $key => $value) {

                        if($this->Api->isValidSize($value->name)) {

                          $valid_size = true;

                        }

                      }

                      if( $valid_size && $product->stock[$size] >= 10 ) {

                        $sel_class = "";

                        if($in_cart && $in_cart_p_size == $size ) {

                          $sel_class = "selected";

                        }

                        ?><div class="product-mini-btn <?=$sel_class?>"><?=$size?></div><?php

                      }
                    }

                  ?>

            </div>
          </div>

          <p class="product-title" ><?=$product->name?></p>

        </div>

        <?php

      }

      foreach ($babies_products as $product) {

        $in_cart = false;

        if(isset($in_cart_p_id) && $product->id == $in_cart_p_id ) {

          $in_cart = true;

        }

        ?>

        <div class="product">

          <a target="blank" href="<?=$product->producturl?>" >

            <img src="<?=$product->image?>" />

          </a>
          <div class="product-mini-btns-wrapper" >
            <div  class="product-mini-btns"
                  data-id="<?=$product->id?>"

                  <?php

                    foreach ($product->options as $size => $size_value) {

                      $valid_size = false;

                      foreach ($size_value as $key => $value) {

                        if($this->Api->isValidSize($value->name)) {

                          $valid_size = true;

                        }

                      }


                      if($valid_size) {

                        ?>

                        data-sku<?=strtolower(str_replace("/", "", $size))?>="<?=$product->{'sku_'.$size}?>"

                        <?php

                      }
                    }

                  ?>
                  data-price="<?=$product->price?>"
                  data-name="<?=$product->name?>"><?php

                    foreach ($product->options as $size => $size_value) {

                      $valid_size = false;

                      foreach ($size_value as $key => $value) {

                        if($this->Api->isValidSize($value->name)) {

                          $valid_size = true;

                        }

                      }

                      if( $valid_size && $product->stock[$size] >= 10 ) {

                        $sel_class = "";

                        if($in_cart && $in_cart_p_size == $size ) {

                          $sel_class = "selected";

                        }

                        ?><div class="product-mini-btn <?=$sel_class?>"><?=$size?></div><?php

                      }
                    }

                  ?>

            </div>
          </div>

          <p class="product-title" ><?=$product->name?></p>

        </div>

        <?php

      }
      ?>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){

    $(".product-mini-btn").click(function(){
      $(".product-mini-btn").removeClass("selected");
      $(this).addClass("selected");

      var p = $(this).parent();
      var psize = $(this).html();
      var psku = "";

      <?php

        foreach ($size_arr as $size ) {

          ?>

          if( psize == "<?=$size?>" ) {

            psku = p.data('sku<?=strtolower(str_replace("/", "", $size))?>');

          }

          <?php

        }

      ?>

      var pid = p.data('id');
      var pprice = p.data('price');
      var pname = p.data('name');

      $.post( "products/buy_product",
              { id: pid, qty: 1, price: pprice, name: pname, size: psize, sku: psku }
              ).done(function( data ) {
                    if( data.trim() == "Success" ) {

                      window.location.href = window.location.protocol + "//"+window.location.hostname+"/products?change=" + pid;

                    }
                  });

    });

  });
</script>
