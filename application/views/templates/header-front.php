<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?=$title?> | OnePiece&#8482; The PieceKeepers</title>

    <meta charset="UTF-8">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="<?=base_url('css/normalize.css')?>" />
    <link type="text/css" rel="stylesheet" href="<?=base_url('css/foundation.min.css')?>" />
    <link type="text/css" rel="stylesheet" href="<?=base_url('css/calendar.css')?>" />
    <link type="text/css" rel="stylesheet" href="<?=base_url('css/style.css')?>" />
    <link type="text/css" rel="stylesheet" href="<?=base_url('css/lightbox.css') ?>" />
    <link type="text/css" rel="stylesheet" href="<?=base_url('css/smoothness/jquery-ui-1.10.3.custom.min.css')?>">
    <link type="image/vnd.microsoft.icon" rel="shortcut icon" href="<?=base_url('images/favicon.ico')?>">
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="<?=base_url('js/custom.js')?>"></script>
</head>
<body>
<div class="top front-header">
    <div class="inner">
        <div class="header-top clearfix">
            <div class="header-top-left">
                <a href="<?php echo base_url('welcome') ?>">
                    <div class="logo"><img src="<?=base_url('images/onepiece-logo-white.png')?>" width="115" alt="Logo White" /> </div>
                    <div class="text-logo"> | &nbsp;THE &nbsp;PIECEKEEPERS
                        <!--<div class="beta">&nbsp;BETA!</div>-->
                    </div>
                </a>
            </div>
        </div>

        <div class="login-form">
            <form action="<?=base_url('auth/login')?>" method="post" accept-charset="utf-8">
                <div class="form-label">
                    Email :
                </div>
                <div class="login-input">
                    <input type="email" name="email"  />
                </div>
                <div class="form-label">
                    Password :
                </div>
                <div class="login-input">
                    <input type="password" name="password"  />
                </div>
                <div class="login-btn">
                    <input type='submit' value='LOG IN' />
                </div>
            </form>
            <div class="forgot-password"><a class = "forgot-password-link" href = "<?=base_url('auth/forgot_password')?>">Forgot Password?</a></div>
        </div>
    </div>

</div>
<div class="main">
    <div class="header">
