<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?=$title?> | OnePiece&#8482; The PieceKeepers</title>

    <meta charset="UTF-8">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="<?=base_url('css/normalize.css')?>" />
    <link type="text/css" rel="stylesheet" href="<?=base_url('css/foundation.min.css')?>" />
    <link type="text/css" rel="stylesheet" href="<?=base_url('css/calendar.css')?>" />
    <link type="text/css" rel="stylesheet" href="<?=base_url('css/style.css')?>" />
    <link type="text/css" rel="stylesheet" href="<?=base_url('css/lightbox.css') ?>" />
    <link type="text/css" rel="stylesheet" href="<?=base_url('css/smoothness/jquery-ui-1.10.3.custom.min.css')?>">
    <link type="image/vnd.microsoft.icon" rel="shortcut icon" href="<?=base_url('images/favicon.ico')?>">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="<?=base_url('js/custom.js')?>"></script>
</head>
<body class="page page-maintenance">

<div class="main">
    <div class="header">
