</div>
</div>
    <div class="footer">
        <div class="line">
          <div class="content">
             <div class="title"><a href="<?=base_url('missions')?>">Earn Bonus Points!</a></div>
             <div class="title"><a href="<?=base_url('coupons')?>">Earn Commission!</a></div>
             <div class="title"><a href="<?=base_url('goals')?>">Take part in our Goals!</a></div>
          </div>
        </div>
        <div class="content">
            <div class="footer-column" id="first">
                <p>The missions are tasks we ask you to
    do and as a reward you earn bonus points. The points determine your ranking which decides if you can take part in our prizes when we reach our Goals.</p>
                <p><a href="<?=base_url('missions')?>">Go to Missions now</a></p>
            </div>
            <div class="footer-column">
            	<p>Go to our coupons page and create a
    discount code you can share with friends.
    You earn 5% commission on all sales that
    use your code. At level 2 and 3 you earn
    6% and 7% commission.</p>
                <p><a href="<?=base_url('coupons')?>">Go to Coupons now</a></p>
            </div>
            <div class="footer-column" id="last">
                <p>If all the PieceKeepers combined reach
    a certain number of total points we will
    celebrate reaching our goal with inviting
    the top ranking PieceKeepers to amazing
    getaways and hapenings!</p>
                <p><a href="<?=base_url('goals')?>">Go to Goals now</a></p>
            </div>
        </div>
        <div class="clear"></div>

      <div class="privacy-policy">
       &copy; 2015 OnePiece.com L.P. All rights reserved. PieceKeeper Program <a href="<?php echo base_url('/terms') ?>" target="_blank">Terms & Conditions</a>
      </div>
    </div>

<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58443388-1', 'auto');
  ga('send', 'pageview');

var fb_param = {};
fb_param.pixel_id = '6007619156976';
fb_param.value = '0.00';
(function(){
  var fpw = document.createElement('script');
  fpw.async = true;
  fpw.src = '//connect.facebook.net/en_US/fp.js';
  var ref = document.getElementsByTagName('script')[0];
  ref.parentNode.insertBefore(fpw, ref);
})();
</script>
</body>
