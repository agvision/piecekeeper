<?php

    $header_user = $this->users->get($this->session->userdata('id'));

    // check if user accepted terms
    if($this->session->userdata('id') && !$header_user['terms_accepted'] && $this->router->fetch_class() != 'accept_terms' && $this->router->fetch_class() != 'terms'){
        redirect('/accept_terms');
    }

 ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title><?=$title?> | OnePiece&#8482; The PieceKeepers</title>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta name="viewport" content="width=device-width, user-scalable=no">
 <link href="https://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
 <link type="text/css" rel="stylesheet" href="<?=base_url('css/normalize.css')?>" />
 <link type="text/css" rel="stylesheet" href="<?=base_url('css/foundation.css')?>" />
 <link type="text/css" rel="stylesheet" href="<?=base_url('css/calendar.css')?>" />
 <link type="text/css" rel="stylesheet" href="<?=base_url('css/style.css')?>" />
 <link type="text/css" rel="stylesheet" href="<?=base_url('css/lightbox.css')?>" />
 <link href="<?=base_url('images/favicon.ico')?>" rel="shortcut icon" type="image/vnd.microsoft.icon">
 <link href="<?=base_url('css/smoothness/jquery-ui-1.10.3.custom.min.css')?>" rel="stylesheet">

 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
 <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
 <script src="<?=base_url('js/custom.js')?>"></script>
 <script src="<?=base_url('js/imgpreview.js')?>"></script>
 <script src="<?=base_url('js/highcharts-4.0.4.min.js')?>"></script>

 <script src="<?=base_url('js/foundation-datepicker.js');?>"></script>
 <link rel="stylesheet" href="<?=base_url('css/foundation-datepicker.css')?>">

 <script src="<?=base_url('js/jquery.tipsy.js')?>"></script>
 <link rel="stylesheet" type="text/css" href="<?=base_url('css/tipsy.css')?>" >

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.btn, .save-profile-black, .save-profile-white, .btn-black, .btn-white, .small-btn').mouseenter(function(e){
            jQuery(this).fadeTo('fast', 0.7);
        }).mouseleave(function(e){
            jQuery(this).fadeTo('fast', 1);
        });
    });
</script>

<script src="<?=base_url('js/tablesorter_latest.js')?>"></script>
<script type="text/javascript">
    $(document).ready(function(){

    	$.tablesorter.formatInt = function (s) {
            var i = parseInt(s);
            return (isNaN(i)) ? null : i;
        };

        $.tablesorter.addParser({
            id: "date",
            is: function (s) {
                return false;
            },
            format: function (s, table) {
                var date = (s + '').match(/(\d{1,2}\s+\w{3}\s+\d{4}),(\s+\d{1,2}:\d{1,2}:\d{1,2}\s+[AP]M)/);
                return date ? new Date(date[1] + date[2]).getTime() || s : s;
            },
            type: "numeric"
        });

        $.tablesorter.formatFloat = function (s) {
            var i = parseFloat(s);
            return (isNaN(i)) ? null : i;
        };
        $("table:not(#piecekeepers-list,#missions-list)").tablesorter({
        	emptyTo: 'bottom'
        });
        $('.tipsy').tipsy({gravity: 's'});
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#menu-trigger').hover(function(){
            $('.vertical-menu').show();
        }, function(){
            $('.vertical-menu').hide();
        });
    });
</script>

<!-- Delete confirmation -->
<script type="text/javascript">
    $(document).ready(function(){
        $('.delete-item').click(function(e){
            e.preventDefault();
            var href = $(this).attr('href');
            var confirmed = confirm('Are you sure you want to delete this item?');
            if (confirmed) {
                window.location = href;
            }
        });
        $('.enable-item').click(function(e){
            e.preventDefault();
            var href = $(this).attr('href');
            var confirmed = confirm('Are you sure you want to enable this item?');
            if (confirmed) {
                window.location = href;
            }
        });
        $('.disable-item').click(function(e){
            e.preventDefault();
            var href = $(this).attr('href');
            var confirmed = confirm('Are you sure you want to disable this item?');
            if (confirmed) {
                window.location = href;
            }
        });
        $('.remove-admin-rights').click(function(e){
            e.preventDefault();
            var href = $(this).attr('href');
            var confirmed = confirm('Are you sure you want to remove this admin\'s rights?');
            if (confirmed) {
                window.location = href;
            }
        });
        $('.followers .delete-box .delete-network a').click(function(e){
            e.preventDefault();
            var href = $(this).attr('href');
            var confirmed = confirm('Are you sure you want to remove this social media connection from this user?');
            if (confirmed) {
                window.location = href;
            }
        });

        $('.approve-package').click(function(e){
            e.preventDefault();
            var href = $(this).attr('href');
            var confirmed = confirm('Are you sure you want to ship this package to the PieceKeeper? This action cannot be canceled.');
            if (confirmed) {
                window.location = href;
            }
        });
    });
</script>

<!-- Fix fdatepicker empty value bug -->
<script type="text/javascript">
    $(document).ready(function(){
        $("input.date").each(function(i){
            if($(this).val() == "01 Jan 1970"){
                $(this).val("");
            }
        });
        $( "#date_for_goals" ).fdatepicker();
        $( "#date_start" ).fdatepicker();
        $( "#date_end" ).fdatepicker();
        $(".icon-chevron-right").html(">");
        $(".icon-chevron-left").html("<");
    });
</script>

</head>

<body>
<div class="fade-background"></div>
<div class="top">
    <div class="inner">
        <div class="header-top clearfix">
            <div class="header-top-left">
                <a href="<?php echo base_url('welcome') ?>">
                    <div class="logo"><img src="<?=base_url('images/widelogo.jpg')?>"  class="header_logo_img" alt="Logo"/></div>
                    <div class="text-logo-black"> <div class="display-inline-block vertical-align-top">|</div> <div class="logo-margin">THE PIECEKEEPERS</div>
                        <!--<div class="beta">BETA!</div>-->
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="menucont">
        <div class="inner">
            <ul class="header-menu clearfix">

                <?php if ($this->router->fetch_class() == 'auth' || $this->router->fetch_class() == 'social' || ($this->router->fetch_class() == 'contact' && !isset($logged))): ?>
                    <li class="menu-button <?php if($this->router->fetch_method() == 'login') echo 'current' ?>"><a href="<?=base_url('auth/login')?>">LOGIN</a></li>
                    <li class="menu-button <?php if($this->router->fetch_method() == 'register') echo 'current' ?>"><a href="<?=base_url('auth/register')?>">REGISTER</a></li>

                <?php else: ?>
                    <li class="<?php if($this->router->fetch_class() == 'welcome' && $this->uri->segment(1) != "admin" ) echo 'current' ?>"><a href="<?php echo base_url('/welcome') ?>">HOME</a></li>
                    <li id="menu-profile" class="<?php if($this->router->fetch_class() == 'profile' && $this->uri->segment(1) != "admin" ) echo 'current' ?>"><a href="<?php echo base_url('/profile') ?>">PROFILE</a></li>

                    <li id="menu-coupons" class="<?php if($this->router->fetch_class() == 'coupons' && $this->uri->segment(1) != "admin" ) echo 'current' ?>"><a href="<?php echo base_url('/coupons') ?>">DISCOUNT CODE</a></li>
                    <li class="<?php if($this->router->fetch_class() == 'missions' && $this->uri->segment(1) != "admin" ) echo 'current' ?>"><a href="<?=base_url('missions')?>">MISSIONS</a></li>
                    <li id="menu-stats" class="<?php if($this->router->fetch_class() == 'profile') echo 'stats' ?><?php if($this->router->fetch_class() == 'stats' && $this->uri->segment(1) != "admin" ) echo 'current' ?>"><a href="<?php echo base_url('/stats') ?>">STATS</a></li>
                    <li id="menu-payouts" class="<?php if($this->router->fetch_class() == 'profile') echo 'payouts' ?><?php if($this->router->fetch_class() == 'payouts' && $this->uri->segment(1) != "admin" ) echo 'current' ?>"><a href="<?php echo base_url('/payouts') ?>">PAYOUTS</a></li>
                    <li class="<?php if($this->router->fetch_class() == 'ranking' && $this->uri->segment(1) != "admin" ) echo 'current' ?>"><a href="<?=base_url('ranking')?>">RANKING</a></li>
                    <li class="<?php if($this->router->fetch_class() == 'goals' && $this->uri->segment(1) != "admin" ) echo 'current' ?>"><a href="<?=base_url('goals')?>">GOALS</a></li>

                    <?php $user = $this->users->get($this->session->userdata('id')); ?>
                    <li id="menu-team" class="<?php if($this->router->fetch_class() == 'sales_team') echo 'current' ?>"><a href="<?php echo base_url('/sales_team') ?>">SALES TEAM</a></li>

                    <li class="<?php if($this->router->fetch_class() == 'levels' && $this->uri->segment(1) != "admin" ) echo 'current' ?>"><a href="<?=base_url('levels')?>">LEVELS</a></li>
                    <li class="<?php if($this->router->fetch_class() == 'blog' && $this->uri->segment(1) != "admin" ) echo 'current' ?>"><a href="<?=base_url('blog')?>">BLOG</a></li>
                    <li class="<?php if($this->router->fetch_class() == 'contact' && $this->uri->segment(1) != "admin" ) echo 'current' ?>"><a href="<?=base_url('contact')?>">CONTACT</a></li>

                    <li class="menu-button" style="float: right;"><a href="<?=base_url('auth/logout')?>">LOG OUT</a></li>

                    <?php if (isset($header_user['level']) && $header_user['level'] >= 2): ?>
                        <li class="menu-button <?php if($this->router->fetch_class() == 'index') echo 'current' ?>" style="float: right;"><a href="<?=base_url('admin')?>">ADMIN</a></li>
                    <?php endif ?>
                    <?php if ($this->session->userdata('admin_hash')): ?>
                        <li class="menu-button <?php if($this->router->fetch_class() == 'index') echo 'current' ?>" style="float: right;"><a href="<?=base_url('auth/surf/'.$this->session->userdata('admin_hash').'?back=true')?>">ADMIN</a></li>
                    <?php endif ?>
                <?php endif ?>

            </ul>
        </div>
    </div>
</div>
<div class="main">
    <div class="header">
