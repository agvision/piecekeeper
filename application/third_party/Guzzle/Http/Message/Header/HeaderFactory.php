<?php

namespace Guzzle\Http\Message\Header;

use Guzzle\Http\Message\Header;

require_once APPPATH."third_party/Guzzle/Http/Message/Header/HeaderFactoryInterface.php";
require_once APPPATH."third_party/Guzzle/Http/Message/Header.php";

/**
 * Default header factory implementation
 */
class HeaderFactory implements HeaderFactoryInterface
{
    /** @var array */
    protected $mapping = array(
        'cache-control' => 'Guzzle\Http\Message\Header\CacheControl',
        'link'          => 'Guzzle\Http\Message\Header\Link',
    );

    public function createHeader($header, $value = null)
    {
        $lowercase = strtolower($header);

        return isset($this->mapping[$lowercase])
            ? new $this->mapping[$lowercase]($header, $value)
            : new Header($header, $value);
    }
}
