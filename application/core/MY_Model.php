<?php 

class MY_Model extends CI_Model
{
  public $table;

  function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  function get_all_entries()
  {
    $query = $this->db->get($this->table);
    return $query->result_array();
  }

  function get($id)
  {
    $this->db->select('*');
    $this->db->from($this->table);
    $this->db->where('id', $id);
    $query = $this->db->get()->result_array();

    return $query[0];
  }

  function add_new_entry($fields)
  {
    $this->db->insert($this->table, $fields);
  }

  function update_entry($id, $fields)
  {
    $this->db->where('id', $id);
    $this->db->update($this->table, $fields);
  }

  function delete_entry($id)
  {
    $this->db->where('id', $id);
    $this->db->delete($this->table);
  }
}

