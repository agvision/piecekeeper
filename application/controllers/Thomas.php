<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Thomas extends CI_Controller {

    public function __construct()
    {
       parent::__construct();
    }

    public function index()
    {
        $this->session->set_userdata('test_system', true);
    }

}
