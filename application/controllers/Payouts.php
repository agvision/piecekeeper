<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payouts extends CI_Controller
{
    public function __construct() {

        parent::__construct();
        $this->load->model('admin_notification', 'AdminNotification');
        $this->load->model('email_model', 'Email');
        $this->load->model('points_model', 'Points');
        $this->load->model('payment_request', 'PaymentRequest');
        $this->load->model('coupon_model', 'Coupon');
        $this->load->model("site_config");
        $this->load->model('Social_model', 'social');

        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('url');

        if (!$this->authentication->isLogged()) {
                $this->authentication->saveTheLastUrl();
                redirect('/auth/login');
        }
    }


    public function index() {
        $id_user = $this->session->userdata['id'];
        $data['title'] = 'Payouts';
        $data['user']  = $this->users->get_profile($this->session->userdata('id'));

        $time     = date('Y-m-d G:i:s', strtotime("-1 day", time()));
        $settings = $this->site_config->getGeneralSettings();
        $data['settings'] = $settings;

        $data['payments']   = $this->PaymentRequest->getUserItems($id_user);
        $data['giftcards'] = $this->db->query("SELECT * FROM payment_requests AS pr INNER JOIN coupons AS c ON c.id=pr.id_coupon WHERE id_user={$id_user} AND giftcard=1 ORDER BY pr.created DESC")->result();
        $data['payment_request_content'] = $this->db->query("SELECT * FROM content WHERE id=14")->row()->body;
        $data['commission'] = $this->users->getCommission($this->session->userdata('id'));

        $data['totals'] = $this->db->query("SELECT COUNT(*) AS sales_no, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id_user}")->row();
        $points = $this->Points->getFromUser($id_user);
        $data['totals']->total_points = $points['total'];
        $data['totals']->bonus = $points['bonus'];
        $data['global_ranking'] = $this->users->getGlobalRanking($id_user);

        $data['countFollowers'] = $this->social->countFollowers($this->session->userdata('id'));

    	$this->load->view('templates/header', $data);
    	$this->load->view('pages/payouts');
    	$this->load->view('templates/footer');
    }


    public function payment_details(){
        $id_user = $this->session->userdata['id'];

        $this->form_validation->set_rules('payment_number', 'Email', 'required|valid_email');

        if ($this->form_validation->run() == true)
        {

            $data = array(
                'payment_name' => ($this->input->post('payment_name') == NULL) ? FALSE : $this->input->post('payment_name'),
                'payment_number' => ($this->input->post('payment_number') == NULL) ? FALSE : $this->input->post('payment_number')
            );
            $this->db->where('id', $id_user);
            $this->db->update('auth_user', $data);
        }
        else
        {
            $this->session->set_flashdata(
                'paypal_error',
                "<div class='alert-box error form-alert'>Please insert a valid e-mail address.</div>"
            );

        }

        redirect('/payouts');
    }


    public function request_payment(){
        $id_user = $this->session->userdata['id'];
        $user    = $this->users->get($id_user);
        if($this->input->post('payment_method') == "paypal")
        {
            if(empty($user['payment_number']))
            {
                $this->session->set_flashdata(
                    'request_error',
                    "<div class='alert-box error form-alert'>Please update your PayPal e-mail address.</div>"
                );
                redirect('/payouts');
            }
        }

            $amount = floatval($this->input->post('amount'));
            $time   = date('Y-m-d G:i:s', strtotime("-1 month", time()));

            $settings   = $this->site_config->getGeneralSettings();
            $commission = $this->users->getCommission($id_user);

            $requests_last_month = $this->db->query("SELECT COUNT(*) AS total FROM payment_requests WHERE id_user={$id_user} AND created>='{$time}'")->row();

            $limit_per_month = $settings['payout_requests_per_month'];

            if (intval($requests_last_month->total) >= intval($limit_per_month)) {
                $this->session->set_flashdata(
                    'error',
                    "<div class='alert-box error form-alert'>You have reached the limit of requests for this month.</div>"
                );

                redirect('/payouts');
            }

            if ($commission < $settings['minimum_commission_payout']) {
                $this->session->set_flashdata(
                    'invalid_amount',
                    'You need at least $' . $settings['minimum_commission_payout'] . ' before you can ask for payout.'
                );

                redirect('/payouts');
            }

            if ($amount < $settings['minimum_commission_payout']) {
                $this->session->set_flashdata(
                    'invalid_amount',
                    'You need to request at least $' . $settings['minimum_commission_payout'] . '.'
                );

                redirect('/payouts');
            }

            if ($amount > $commission) {
                $this->session->set_flashdata(
                    'invalid_amount',
                    'Not enough credit.'
                );

                redirect('/payouts');
            }

            $data = array(
                'amount'  => $amount,
                'id_user' => $id_user
            );


            if (isset($_POST['paypal'])) {
                $data['paypal'] = 1;
            }

            // automatically create and send giftcard to user email
            if (isset($_POST['payment_method']) && $_POST['payment_method'] == 'giftcard') {
                $data['giftcard'] = 1;
                $data['aproved']  = 1;

                $country             = $this->db->query("SELECT * FROM country_t WHERE country_id={$user['id_country']}")->row();
                $code                = $this->Coupon->randomString(6);
                $gift_amount         = (float)$amount + ((float)$settings['giftcard_extra_commission'] * (float)$amount) / 100;
                $gift_amount         = number_format($gift_amount, 2, ".", "");
                $data['gift_amount'] = $gift_amount;

                // if($_SERVER['REMOTE_ADDR'] == "213.233.85.84") {
                //     var_dump($gift_amount); die();
                // }

                $data['id_coupon'] = $this->Coupon->createGiftcard($id_user, $code, $gift_amount, $country->country_id);
                $this->Email->giftcardCreated($code, $user);

                $this->session->set_flashdata(
                    'success_request',
                    'Your giftcard code will be sent to your email.'
                );

            }
            else
            {


                $this->AdminNotification->type  = 4;
                $this->AdminNotification->param = $this->db->insert_id();
                $this->AdminNotification->insert();

                $this->session->set_flashdata(
                    'success_request',
                    'Please wait for payment approval.'
                );
            }

            if(!empty($this->session->flashdata('success_request')))
            {
                $this->db->set('created', 'NOW()', FALSE);
                $this->db->insert('payment_requests', $data);
            }


        redirect('/payouts');
    }

}
