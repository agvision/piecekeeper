<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Coupons extends CI_Controller
{
    public function __construct() {

        parent::__construct();
        $this->load->model('admin_notification', 'AdminNotification');
        $this->load->model('email_model', 'Email');
        $this->load->model('points_model', 'Points');
        $this->load->model('coupon_model', 'Coupon');
        $this->load->model("site_config");
        $this->load->model('Social_model', 'social');

        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('url');

        if (!$this->authentication->isLogged()) {
                $this->authentication->saveTheLastUrl();
                redirect('/auth/login');
        }
    }


    public function index() {
        $id_user = $this->session->userdata['id'];
        $data['title'] = 'Create Discount Code';
        $data['user']  = $this->users->get_profile($this->session->userdata('id'));

        $time     = date('Y-m-d G:i:s', strtotime("-1 day", time()));
        $settings = $this->site_config->getGeneralSettings();
        $data['settings'] = $settings;

        $data['giftcards'] = $this->db->query("SELECT * FROM payment_requests AS pr INNER JOIN coupons AS c ON c.id=pr.id_coupon WHERE id_user={$id_user} AND giftcard=1 ORDER BY pr.created DESC")->result();
        $data['coupons_24h'] = $this->db->query("SELECT COUNT(*) AS total FROM coupons WHERE type=1 AND id_creator={$id_user} AND created>='{$time}'")->row()->total;
        $data['limit_24h']   = $settings['24h_discounts_per_user'];
        $data['coupons']    = $this->Coupon->getUserItems($id_user);

        // get content
        $data['coupons_title'] = $this->db->query("SELECT * FROM content WHERE id=4")->row()->body;

        $data['coupons_description'] = $this->db->query("SELECT * FROM content WHERE id=20")->row()->body;
        $data['coupons_description'] = str_replace('{DISCOUNT_PERCENTAGE}', $data['settings']['default_discount_percentage'], $data['coupons_description']);
        $data['coupons_description'] = str_replace('{COUPONS_PER_DAY}', $data['limit_24h'], $data['coupons_description']);
        $data['coupons_description'] = str_replace('{COUPONS_LEFT}', intval($data['limit_24h'] - $data['coupons_24h']), $data['coupons_description']);

        $data['coupons_single'] = $this->db->query("SELECT * FROM content WHERE id=5")->row()->body;
        $data['coupons_single'] = str_replace('{DISCOUNT_PERCENTAGE}', $data['settings']['default_discount_percentage'], $data['coupons_single']);
        $data['coupons_single'] = str_replace('{COUPONS_PER_DAY}', $data['limit_24h'], $data['coupons_single']);
        $data['coupons_single'] = str_replace('{COUPONS_LEFT}', intval($data['limit_24h'] - $data['coupons_24h']), $data['coupons_single']);
        $data['coupons_single'] = str_replace('{PK_COMMISSION}', $this->User->getCommissionPercentage($id_user), $data['coupons_single']);

        $data['coupons_multiple'] = $this->db->query("SELECT * FROM content WHERE id=6")->row()->body;
        $data['coupons_multiple'] = str_replace('{DISCOUNT_PERCENTAGE}', $data['settings']['default_discount_percentage'], $data['coupons_multiple']);
        $data['coupons_multiple'] = str_replace('{PK_COMMISSION}', $this->User->getCommissionPercentage($id_user), $data['coupons_multiple']);

        $data['totals'] = $this->db->query("SELECT COUNT(*) AS sales_no, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id_user}")->row();
        $points = $this->Points->getFromUser($id_user);
        $data['totals']->total_points = $points['total'];
        $data['totals']->bonus = $points['bonus'];
        $data['global_ranking'] = $this->users->getGlobalRanking($id_user);

        $data['countFollowers'] = $this->social->countFollowers($this->session->userdata('id'));

    	$this->load->view('templates/header', $data);
    	$this->load->view('pages/coupons');
    	$this->load->view('templates/footer');
    }


    public function create_coupon() {
        $id_user   = $this->session->userdata('id');
        $user      = $this->users->get($id_user);
        $time      = date('Y-m-d G:i:s', strtotime("-1 day", time()));
        $last_week = date('Y-m-d G:i:s', strtotime("-1 week", time()));
        $settings  = $this->site_config->getGeneralSettings();

        // Single use
        if (isset($_POST['trigger']) && $_POST['trigger'] == 'discount_20') {

            // check 24h limitations
            $coupons_24h = $this->db->query("SELECT COUNT(*) AS total FROM coupons WHERE type=1 AND id_creator={$id_user} AND created>='{$time}'")->row();
            $limit_24h = $settings['24h_discounts_per_user'];

            if (intval($coupons_24h->total) >= intval($limit_24h)) {

                $this->session->set_flashdata(
                    'error_single_use',
                    "<div class='alert-box error form-alert no-image'>You have reached the 24 hours limit.</div>"
                );

                redirect('/coupons');
            }

            $this->form_validation->set_rules('code', 'Coupon Code', 'required|max_length[30]|is_unique[coupons.code]|alpha_numeric');

            if($this->form_validation->run()) {
                $code      = strtoupper($this->input->post('code'));
                $id_coupon = $this->Coupon->createSingleUse($this->session->userdata('id'), $code, '');

                $this->session->set_flashdata(
                    'success_single_use',
                    'Code: <b>'.$code.'</b> - Code is valid for 2 weeks. You have created '.intval($coupons_24h->total+1).' of '.$limit_24h.' discount codes today.'
                );
            }
        }

        // Multi use
        if (isset($_POST['trigger']) && $_POST['trigger'] == '48_hours') {

            // check active code limitation
            $active_code = $this->Coupon->getActiveMultiuse($id_user);
            if($active_code) {
                $this->session->set_flashdata(
                    'error',
                    "<div class='alert-box error form-alert no-image'>Code <b>".$active_code."</b> is still valid. It expires 36 hours after the time it was created.</div>"
                );
                redirect('/coupons');
            }

            // check weekly codes limitation
            $coupons_this_week = $this->db->query("SELECT COUNT(*) AS total FROM coupons WHERE type=2 AND id_creator={$id_user} AND created>='{$last_week}'")->row();
            $limit_week = $settings['multiuse_codes_per_week'];

            if (intval($coupons_this_week->total) >= intval($limit_week)) {
                $this->session->set_flashdata(
                    'error',
                    "<div class='alert-box error form-alert no-image'>You have reached the multiuse codes limit for the past 7 days.</div>"
                );

                redirect('/coupons');
            }

            // check level limitation
            if ($user['level'] < $settings['multiple_discount_level']) {
                redirect('/coupons');
            }

            $this->form_validation->set_rules('code', 'Coupon Code', 'required|max_length[30]|is_unique[coupons.code]|alpha_numeric');
            // $this->form_validation->set_rules('valability', 'Valability', 'required');
            $this->form_validation->set_rules('link', 'Link', 'required');

            if ($this->form_validation->run()) {
                $code         = strtoupper($this->input->post('code'));
                $percent      = $settings['default_discount_percentage'];//$this->input->post('percent');
                $amount       = ($this->input->post('amount') == NULL) ? FALSE : $this->input->post('amount');
                $currency     = ($this->input->post('currency') == NULL) ? FALSE : $this->input->post('currency');
                $u_limit      = ($this->input->post('u_limit') == NULL) ? FALSE : $this->input->post('u_limit');
                $p_limit      = ($this->input->post('p_limit') == NULL) ? FALSE : $this->input->post('p_limit');
                $freeshipping = ($this->input->post('freeshipping') == NULL) ? FALSE : $this->input->post('p_limit');
                $link         = ($this->input->post('link') == NULL) ? FALSE : $this->input->post('link');

                $id_coupon = $this->Coupon->createMultipleUse($this->session->userdata('id'), $code, $percent, $amount, $currency, '', $link, $u_limit, $p_limit, $freeshipping, true, false);

                $this->AdminNotification->type  = 6;
                $this->AdminNotification->param = $id_coupon;
                $this->AdminNotification->insert();

                $this->session->set_flashdata(
                    'success',
                    'Code <b>'.$code.'</b> has been created.'
                );
            }
        }

        if (validation_errors()) {
            $errors = validation_errors("<div class='alert-box error form-alert no-image'>", "</div>");
            $errors = str_replace("The Coupon Code field must contain a unique value.", "Sorry this Discount Code has already been created.", $errors);
            $this->session->set_flashdata(
                'error',
                $errors
            );
        }


        redirect('/coupons');
    }

}
