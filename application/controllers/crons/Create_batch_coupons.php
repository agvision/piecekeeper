<?php

class Create_batch_coupons extends CI_Controller {
    
  public function __construct()
  {
    parent::__construct();
	 
    $this->load->model('site_config', 'Config');
    $this->load->model('scratch_coupon_model', 'ScratchCoupon');
    $this->load->model('coupon_model', 'Coupon');
  }
  
 public function index() {
    $config  = $this->Config->getGeneralSettings();
    $percent = $config['scratch_code_discount'];

    $batches = $this->ScratchCoupon->getPendingBatches();
    foreach ($batches as $b) {
        $coupons = $this->ScratchCoupon->getPendingCoupons($b->batch);
        foreach ($coupons as $c) {
            $this->Coupon->createBatchCoupon($c->coupon, $percent);
            $this->ScratchCoupon->setCreatedCoupon($c->coupon);
        }
        $this->ScratchCoupon->setCreatedBatch($b->batch);
    }

  }
  
}