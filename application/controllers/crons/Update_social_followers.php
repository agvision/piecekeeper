<?php

class Update_social_followers extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("social_model", "Social");
        $this->load->model("Users", "User");
        $this->load->model("Site_config", "settings");

        $CI = & get_instance();
        $CI->config->load("facebook",TRUE);
        $config = $CI->config->item('facebook');
        $this->load->library('Facebook', $config);

    }

    public function index() {
        $followersDecreasePercentage = $this->settings->getGeneralSettings()['followers_decrease_warning'];
        $this->Social->deleteUnexistingAccounts();
        $accounts = $this->Social->getNotUpdatedAccounts();

        foreach ($accounts as $ac) {
          switch ($ac->network) {
                    case '1':
                        $network = "facebook";
                        $followers = $this->Social->getAPIFacebookFollowers($ac->id);
                        $this->Social->createFollowersDecreaseNotification($ac->network, $ac->id_user, $followers, $followersDecreasePercentage);
                        $this->Social->updateNetwork($network, $ac->id_user, $followers);
                        break;

                    case '2':
                        $network = "twitter";
                        $followers = $this->Social->getAPITwitterFollowers($ac->id);
                        $this->Social->createFollowersDecreaseNotification($ac->network, $ac->id_user, $followers, $followersDecreasePercentage);
                        $this->Social->updateNetwork($network, $ac->id_user, $followers);
                        break;

                    case '3':
                        $network = "instagram";
                        $followers = $this->Social->getInstagramFollowers($ac->id);
                        $this->Social->createFollowersDecreaseNotification($ac->network, $ac->id_user, $followers, $followersDecreasePercentage);
                        $this->Social->updateNetwork($network, $ac->id_user, $followers);
                        break;

                    case '5':
                        $network = "pinterest";
                        $followers = $this->Social->getAPIPinterestFollowers($ac->id);
                        $this->Social->createFollowersDecreaseNotification($ac->network, $ac->id_user, $followers, $followersDecreasePercentage);
                        $this->Social->updateNetwork($network, $ac->id_user, $followers);
                        break;

//                    case '7':
//                        $network = "youtube";
//                        $followers = $this->Social->getAPIYoutubeFollowers($ac->id);
//                        $this->Social->createFollowersDecreaseNotification($ac->network, $ac->id_user, $followers, $followersDecreasePercentage);
//                        $this->Social->updateNetwork($network, $ac->id_user, $followers);
//                        break;

                    case '8':
                        $network = "vine";
                        $followers = $this->Social->getAPIVineFollowers($ac->id);
                        $this->Social->createFollowersDecreaseNotification($ac->network, $ac->id_user, $followers, $followersDecreasePercentage);
                        $this->Social->updateNetwork($network, $ac->id_user, $followers);
                        break;

                    default:
                        $followers = true;
                        break;
            }



            if($followers !== false)
            {
                if($this->User->getCompletedStatus($ac->id_user) != 1)
                {
                    $this->User->setCompleted($ac->id_user);
                }

                $this->Social->setUpdated($ac->id);
            }
        }
    }

    public function reset_updated_flag(){
        $this->db->set("updated", "0");
        $this->db->update("social_accounts");
    }
}
