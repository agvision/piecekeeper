<?php

class Daily extends CI_Controller {
    
  public function __construct()
  {
    parent::__construct();
	 
    $this->load->model('email_model', 'Email');
    $this->load->model('mission_model', 'Mission');
    $this->load->model('users', 'User');

    if(!defined('DATE_DB')) {
      define('DATE_DB', 'Y-m-d H:i:s');
    }

  }
  
  public function index() {

    $this->checkInactive45Days();
    $this->checkForMissionCompleted();
    $this->checkForLevelUpgrade();
  }


  private function checkInactive45Days() {
    $users = $this->User->get_all();

    $time  = strtotime("- 45 days");

    foreach ($users as $user) {
        if ($user['last_login'] == '0000-00-00 00:00:00') {
            $last_login = strtotime($user['date_joined']);
        } else {
            $last_login = strtotime($user['last_login']);
        }

        if ($last_login < $time && $user['completed'] == 1 && $user['deleted'] == 0 && $user['is_active'] == 1) {
            if($user['inactive_email_sent'] == 0){
                $this->Email->inactive45Days($user);

                $this->db->set('inactive_email_sent', 1, false);
                $this->db->where('id', $user['id']);
                $this->db->update('auth_user');
            }
        }
    }
  }

  private function checkForMissionCompleted() {
      $missions = $this->Mission->getSalesMissions();
      $users    = $this->User->getAll();

      foreach ($missions as $m) {
          $from = $m->sales_from;
          $to   = $m->sales_to;

          foreach ($users as $u) {
              $sales = $this->User->countSalesBetween($u->id, $from, $to);

              if ($sales >= $m->sales) {
                  if (!$this->Mission->approved($m->id, $u->id)) {
                      $this->Mission->approve($m->id, $u->id);
                  }
              }
          }
      }
  }

  private function checkForLevelUpgrade(){
      $users = $this->users->getAll();

      foreach ($users as $u) {
          if($u->is_active == 1 && $u->completed == 1 && $u->deleted == 0){
              $this->users->checkForLevelUpgrade($u->id);
          }
      }
  }
  
}