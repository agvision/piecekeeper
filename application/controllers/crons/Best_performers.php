<?php

class Best_performers extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('users', 'User');
    $this->load->model('site_config', 'Config');
    $this->load->model('best_performers_model', 'BestPerformers');

    if(!defined("DATE_DB")) define('DATE_DB', 'Y-m-d H:i:s');
  }

  public function index() {
    $settings = $this->Config->getGeneralSettings();
    $from     = date(DATE_DB, strtotime('-'.$settings['best_performers_days_interval'].' days'));
    $to       = date(DATE_DB, time());

    $users = $this->User->getGlobalClassament($from, $to, $settings['best_performers_list_size']);

    $ids_users = array();
    foreach ($users as $u) {
        $ids_users[] = $u->id;
    }

    $list = implode(',', $ids_users);
    $this->BestPerformers->insert($list, $from, $to);
  }

}
