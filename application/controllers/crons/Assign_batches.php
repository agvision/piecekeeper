<?php

class Assign_batches extends CI_Controller {

  	public function __construct()
  	{
    	parent::__construct();

        $this->load->model('scratch_coupon_model', 'ScratchCoupon');
    	$this->load->model('api_log_model', 'ApiLog');

    	if(!defined("AUTH_CODE")) define('AUTH_CODE', '0XgXBZ5Jyf');
    	if(!defined("DATE_DB")) define('DATE_DB', 'Y-m-d H:i:s');
  	}

  	public function index() {
        $this->db->where('batch_assigned', '0');
    	// $this->db->where('type', '1');
    	$packages = $this->db->get('packages')->result();

    	foreach ($packages as $p) {
    		if(intval($p->order_id)){
    			$json     = $this->curl->simple_get("http://onepiece.com/en-us/shop/order/get?auth=".AUTH_CODE."&orderid=".$p->order_id);
    			$this->ApiLog->insert("http://onepiece.com/en-us/shop/order/get?auth=".AUTH_CODE."&orderid=".$p->order_id, $json, "assign_batches");
                $response = json_decode($json);
    			$assigned = 0;

          if(!empty($response->orderlines))
          {
            foreach ($response->orderlines as $ol) {
      				if(strpos($ol->name, "100 batch Discount scratch card") !== false){
      					if(count($ol->serialnumbers)){
      						foreach ($ol->serialnumbers as $sn) {
      							$batch = $this->ScratchCoupon->getBatch($sn);
      							if($batch && !$batch->active){
      								$this->ScratchCoupon->assignBatch($p->id_user, $batch->batch);
      								$assigned = 1;
      							}
      						}
      					}
      				}
      			}
          }

    			$this->db->where('id', $p->id);
    			$this->db->set('order_status', $response->status);
    			if($assigned){
    				$this->db->set('batch_assigned', 1);
    			}
    			$this->db->update('packages');
    		}
    	}
  	}
}
