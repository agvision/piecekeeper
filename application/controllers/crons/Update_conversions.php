<?php

class Update_conversions extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('currencies_conversion_model', 'Conversion');
  }

  public function index() {

    $results = $this->Conversion->getAll();
    foreach ($results as $r) {
        $value = $this->curl->simple_get("http://download.finance.yahoo.com/d/quotes.csv?s=".$r->conversion."=X&f=l1");
        if($value){
          $this->Conversion->updateValue($r->conversion, $value);
        }
    }

  }

}
