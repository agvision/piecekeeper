<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Api_get_orders extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('commission_model', 'Commission');
		$this->load->model('users', 'User');
		$this->load->model('site_config', 'Config');
		$this->load->model('points_model', 'Points');
		$this->load->model('email_model', 'Email');
		$this->load->model('api_log_model', 'ApiLog');
		$this->load->model('social_model', 'social');
		$this->load->model('Coupon_model', 'Coupons');


		if(!defined("AUTH_CODE"))
		{
			define('AUTH_CODE', 'LpSTuC9o6S');
		}
		if(!defined("DATE_DB"))
		{
			define('DATE_DB', 'Y-m-d H:i:s');
		}
	}

	public function index()
	{
		$coupons_list = $this->Coupons->getCouponsForCron();

		$check_since = strtotime('-24hours'); // Every coupon is checked once at every 24hours.

		$fetched_orders = []; // prepare an array for storing every order that needs to be checked.

		$max_coupons = 250; // execute this for a maximum number of coupons.
		echo json_encode($coupons_list)."<br /><br/>Orders:";
		foreach($coupons_list as $c)
		{
			//$c->last_check = empty($c->last_check) ? 0 : $c->last_check;
			if(strtotime($c->last_check) > $check_since)
			{
				continue;
			}

			if($max_coupons <= 0)
			{
				continue;
			}

			$url  = 'https://www.onepiece.com/en-us/shop/discount/getorders?auth=' . AUTH_CODE . '&code=' . $c->code;
			$json = $this->curl->simple_get($url);
			$this->ApiLog->insert($url, $json, "orders_list_by_coupon");

			if(empty($json) || $json == '0')
			{
				continue;
			}

			$json = json_decode($json);

			if(!isset($json->status) || $json->status != 1)
			{
				continue;
			}

			$max_coupons--;

			if(!empty($json->orders))
			{

				foreach($json->orders as $o)
				{
					$fetched_orders[] = $o;
				}
			}

		}

		echo json_encode($fetched_orders);
			if(empty($fetched_orders))
			{
				return;
			}

			$temp = $this->db->select('order_id')->from('orders_coupons')->where_in('order_id', $fetched_orders)->get()->result();

			$existing_orders = [];
			foreach($temp as $o)
			{
				$existing_orders[] = $o->order_id;
			}

			// foreach order
			foreach($fetched_orders as $order)
			{
				if(in_array($order, $existing_orders))
				{
					continue;
				}

				$url = "http://onepiece.com/en-us/shop/order/get?auth=" . AUTH_CODE . "&orderid=" . $order;

				if($this->Social->check40X($url))
				{
					continue;
				}

				// $json     = $this->curl->simple_get($url);
				$json = $this->curl->simple_get($url);

				$this->ApiLog->insert($url, $json, "order_item");
				$response = json_decode($json);

				if(empty($response))
				{
					log_message('error', 'Empty response for: ' . $url);
				}

				if(property_exists($response, 'error'))
				{
					// echo "<p> Error on order ID: ".$order." => ".$response->error."</p>";
					continue;
				}

				if(intval($response->product_total) < 0)
				{
					continue;
				}

				$transaction_arr = $response->transaction;
				$transaction     = $transaction_arr[0];

				$country = $response->country;

				$data = array(
					'order_id'             => $response->order_id,
					'created'              => date(DATE_DB, $response->created),
					'modified'             => date(DATE_DB, $response->modified),
					'email'                => $response->email,
					'phone'                => $response->phone,

					'shipping_first_name'  => $response->shipping_first_name,
					'shipping_last_name'   => $response->shipping_last_name,
					'shipping_address_1'   => $response->shipping_address_1,
					'shipping_address_2'   => $response->shipping_address_2,
					'shipping_house_no'    => $response->shipping_house_no,
					'shipping_house_ext'   => $response->shipping_house_ext,
					'shipping_zip'         => $response->shipping_zip,
					'shipping_city'        => $response->shipping_city,
					'shipping_state'       => $response->shipping_state,
					'shipping_access_code' => $response->shipping_access_code,

					'billing_first_name'   => $response->billing_first_name,
					'billing_last_name'    => $response->billing_last_name,
					'billing_address_1'    => $response->billing_address_1,
					'billing_address_2'    => $response->billing_address_2,
					'billing_house_no'     => $response->billing_house_no,
					'billing_house_ext'    => $response->billing_house_ext,
					'billing_zip'          => $response->billing_zip,
					'billing_city'         => $response->billing_city,
					'billing_state'        => $response->billing_state,

					'country_id'           => $country->id,
					'country_name'         => $country->name,

					'currency'             => $response->currency,
					'total'                => $response->total,
					'status'               => $response->status,
					'shipping_method'      => $response->shipping_method,

					'transaction_type'     => $transaction->type,
					'transaction_amount'   => $transaction->amount,
					'transaction_details'  => $transaction->details
				);


				if(!empty($response))
				{
					if(property_exists($response, 'shipping_country_id'))
					{
						$data['shipping_country_id'] = $response->shipping_country_id;
					}
				}

				// save order in db only if not already exists
				$query = $this->db->get_where('orders_coupons', array('order_id' => $response->order_id));
				if(count($query->result()) == 0)
				{

					if(!empty($response->orderlines))
					{
						// get orderlines
						foreach($response->orderlines as $ol)
						{
							$data = array(
								'order_id'     => $response->order_id,
								'orderline_id' => $ol->orderlineid,
								'sku'          => $ol->sku,
								'name'         => $ol->name,
								'quantity'     => $ol->quantity,
								'price'        => $ol->price,
							);

							// get Discount Codes
							if(preg_match("/^Discount code (.*) \(.*\)$/", $data['name'], $matches))
							{
								$valid = false;

								// check if Discount Code belongs to an Ambassador
								$qry    = $this->db->query("SELECT * FROM coupons WHERE code='{$matches[1]}' AND approved=1 AND deleted=0");
								$result = $qry->result();

								if(count($result) > 0)
								{
									$id_user = $result[0]->id_creator;
									$valid   = true;
								}

								// check if Discount Code is a Scratch Coupon
								$qry    = $this->db->query("SELECT * FROM scratch_coupons WHERE coupon='{$matches[1]}' AND used=0");
								$result = $qry->result();

								if(count($result) > 0)
								{
									$id_scratch_coupon = $result[0]->id;

									$qry    = $this->db->query("SELECT * FROM scratch_batches WHERE batch='{$result[0]->batch}'");
									$result = $qry->row();
									if(count($result) && $result->active)
									{
										$id_user = $result->id_user;

										// mark scratch code as used
										$this->db->where('id', $id_scratch_coupon);
										$this->db->set('used', 1);
										$this->db->update('scratch_coupons');

										$valid = true;
									}
								}

								if($valid)
								{
									// insert used coupon
									$data2 = array(
										'order_id' => $data['order_id'],
										'coupon'   => $matches[1],
										'id_user'  => $id_user,
										'created'  => date(DATE_DB, $response->created)
									);
									$this->db->insert("orders_coupons", $data2);
								}
							}
						}

					}

				}

			}





	}

}
