<?php

class Refresh_cache extends CI_Controller {
    
  public function __construct()
  {
    parent::__construct();
	 
    $this->load->model('users', 'User');
  }
  
  public function index() {

    $this->User->getGroupGlobalRanking(null, false); // current global rankings
    $this->User->getGroupGlobalRanking(null, true);  // last month global rankings

    $this->User->getGroupCountryRanking(null, false);  // current country rankings
    $this->User->getGroupCountryRanking(null, true);  // last month country rankings
    
    $this->User->getGroupCityRanking(null, false);  // current city rankings
    $this->User->getGroupCityRanking(null, true);  // last month city rankings
  
  }
  
}