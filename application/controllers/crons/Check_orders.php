<?php

class Check_orders extends CI_Controller {
    
  public function __construct()
  {
    parent::__construct();
	 
    $this->load->model('commission_model', 'Commission');
    $this->load->model('users', 'User');
    $this->load->model('site_config', 'Config');
    $this->load->model('points_model', 'Points');
    $this->load->model('email_model', 'Email');
    $this->load->model('notification', 'Notification');
    $this->load->model('api_log_model', 'ApiLog');

    $this->load->library('currency');

    if(!defined("AUTH_CODE")) define('AUTH_CODE', 'LpSTuC9o6S');
    if(!defined("DATE_DB")) define('DATE_DB', 'Y-m-d H:i:s');
  }
  
 public function index() {
  // $this->Email->sendSuccessSale('mihaigeorge_c@yahoo.com');
  
    $config = $this->Config->getGeneralSettings();

    $orders = $this->db->query("SELECT * FROM orders_coupons WHERE checked=0")->result();

    foreach ($orders as $o) {
        $pending = intval($config['orders_pending_days']);
        $check   = strtotime("-".$pending." days");

        if (!$o->checked && strtotime($o->created) < $check) {
            $json     = $this->curl->simple_get("http://onepiece.com/en-us/shop/order/get?auth=".AUTH_CODE."&orderid=".$o->order_id);
            $this->ApiLog->insert("http://onepiece.com/en-us/shop/order/get?auth=".AUTH_CODE."&orderid=".$o->order_id, $json, 'check_orders');

            $response = json_decode($json);
            $data = array(
               'order_id' => $response->order_id ,
               'created' => date(DATE_DB, $response->created) ,
               'modified' => date(DATE_DB, $response->modified) ,
               'email' => $response->email ,
               'phone' => $response->phone ,

               'shipping_first_name' => $response->shipping_first_name ,
               'shipping_last_name' => $response->shipping_last_name ,
               'shipping_address_1' => $response->shipping_address_1 ,
               'shipping_address_2' => $response->shipping_address_2 ,
               'shipping_house_no' => $response->shipping_house_no ,
               'shipping_house_ext' => $response->shipping_house_ext ,
               'shipping_zip' => $response->shipping_zip ,
               'shipping_city' => $response->shipping_city ,
               'shipping_state' => $response->shipping_state ,
               'shipping_access_code' => $response->shipping_access_code ,

               'billing_first_name' => $response->billing_first_name ,
               'billing_last_name' => $response->billing_last_name ,
               'billing_address_1' => $response->billing_address_1 ,
               'billing_address_2' => $response->billing_address_2 ,
               'billing_house_no' => $response->billing_house_no ,
               'billing_house_ext' => $response->billing_house_ext ,
               'billing_zip' => $response->billing_zip ,
               'billing_city' => $response->billing_city ,
               'billing_state' => $response->billing_state ,

               'country_id' => $response->country->id ,
               'country_name' => $response->country->name ,

               'currency' => $response->currency ,
               'total' => $this->currency->convertToUSD($response->product_total, $response->currency) ,
               'original_total' => $response->product_total ,
               'status' => $response->status ,
               'shipping_method' => $response->shipping_method ,

               'transaction_type' => $response->transaction[0]->type ,
               'transaction_amount' => $this->currency->convertToUSD($response->product_total, $response->currency) ,
               'original_transaction_amount' => $response->transaction[0]->amount ,
               'transaction_details' => $response->transaction[0]->details
            );

            if(property_exists($response,'shipping_country_id')) {
              $data['shipping_country_id'] = $response->shipping_country_id;
            }

            // check if order is active
            if ($response->status != 'cancelled') {
                // insert order
                $this->db->insert('orders', $data); 

                $id_user = $o->id_user;
                $user    = $this->User->get($id_user);
                $coupon  = $o->coupon;

                // get PieceKeeper commission
                $commission    = $this->Commission->getValue($data['total'], $id_user);
                $pk_commission = $commission;
                $this->Commission->insert($id_user, $commission, $data['order_id'], $data['created'], $coupon, 1); // 1 - direct sale

                if (intval($user['id_master']) != 0 && !$this->User->is_master($id_user)) {
                    // get Master commission
                    $commission = $this->Commission->getValue($data['total'], $id_user, true);
                    $this->Commission->insert($user['id_master'], $commission, $data['order_id'], $data['created'], $coupon, 2); // 2 - slave sale
                }

                // get PieceKeeper Sales Points
                $points = $this->Points->getValue($data['total'], $id_user);
                $this->Points->insert($id_user, $points, 1, false, $data['order_id']); // 1 - sales points type (2 is for bonus points)

                // check if level of user must be upgraded
                $this->User->checkForLevelUpgrade($id_user);

                $this->db->set('checked', 1, false);
                $this->db->where('id', $o->id);
                $this->db->update('orders_coupons');

                $this->Notification->id_user = $id_user;
                $this->Notification->type = 3;
                $this->Notification->param = $pk_commission.",".$points;
                $this->Notification->insert();

                $this->Email->sendSuccessSale($user['email']);

            } else {
                $this->db->set('checked', -1, false);
                $this->db->where('id', $o->id);
                $this->db->update('orders_coupons');
            }   
        }
        
    }

  }
  
}