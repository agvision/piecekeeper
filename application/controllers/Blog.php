<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('users');
    $this->load->model('blog_entry', 'BlogEntry');
    $this->load->model('admin_notification', 'AdminNotification');

    /* Instagram Stuff */
    $CI = & get_instance();
    $CI->config->load("instagram", TRUE);
    $config = $CI->config->item('instagram');
    $this->load->library('instagram_api', $config);

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }

  }

  public function index()
  {
    $data['user'] = $this->users->get($this->session->userdata('id'));
    $data['title'] = 'Blog';
    $data['posts']  = $this->BlogEntry->getRecentPosts();

    $tags = array();
    $tags['onepiecenorway'] = $this->getPicturesByTag('onepiecenorway', 9);
    $tags['forgettherules'] = $this->getPicturesByTag('forgettherules', 9);
    $tags['slackerlife'] = $this->getPicturesByTag('slackerlife', 9);

    $data['tags'] = $tags;

		$this->load->view('templates/header', $data);
		$this->load->view('pages/blog', $data);
		$this->load->view('templates/footer');
  }

  public function add() {
    $id_user = $this->session->userdata['id'];

    if (!$this->users->is_master($id_user)) {
        redirect('/blog');
    }

    if (isset($_POST['trigger']) && $_POST['trigger'] == 'add-blog') {

      $short_description = ($this->input->post('short_description') == NULL) ? FALSE : $this->input->post('short_description');

      $this->load->model('blog_entry', 'blog');

      $img = $this->blog->uploadImage($_FILES, 'image');

      $data=array(
          'name' => ($this->input->post('name') == NULL) ? FALSE : $this->input->post('name'),
          'body' => ($this->input->post('body') == NULL) ? FALSE : $this->input->post('body'),
          'id_user' => $id_user,
          'short_description' => $short_description
      );

      if($img) {
        $data['image'] = $img;
      }

      $this->db->set('creation_date', 'NOW()', FALSE);
      $this->db->insert('blog_entry', $data);

      $this->AdminNotification->type  = 2;
      $this->AdminNotification->param = $this->db->insert_id();
      $this->AdminNotification->insert();

      $this->session->set_flashdata(
        'success',
        'Blog Post added. Wait for Admin approval.'
      );

        redirect('/blog/add');

    }


    $data['title'] = 'Add Blog Post';

    $this->load->view('templates/header', $data);
    $this->load->view('pages/blog_post_add', $data);
    $this->load->view('templates/footer');
  }

  public function view($id) {

    $data['post']  = $this->BlogEntry->getById($id);

    if(!$data['post']) {
      header("Location: " . base_url());
    }

    $data['title'] = $data['post']->name;

    $this->load->view('templates/header', $data);
    $this->load->view('pages/blog-post', $data);
    $this->load->view('templates/footer');

  }

  private function getPicturesByTag($tag, $limit = false) {
      $pictures = $this->instagram_api->tagsRecent($tag);

      $results = array();

      $i = 0;

      if(!empty($pictures))
      {
          foreach ($pictures->data as $p) {
              $results[$i]['img'] = $p->images->thumbnail->url;
              $results[$i]['url'] = $p->link;
              if ($limit && $i >= $limit-1) {
                  break;
              }
              $i++;
          }
      }

      return $results;
  }

}
