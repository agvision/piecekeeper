<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Missions extends CI_Controller
{
  public function __construct() {

    parent::__construct();
    $this->load->model('admin_notification', 'AdminNotification');
    $this->load->model('mission_model', 'Mission');
    $this->load->model('email_model', 'Email');
    $this->load->helper('url');
    $this->load->model('Social_model', 'social');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }
  }


  public function index() {

    $id_user = $this->session->userdata('id');
    $data['user']     = $this->users->get($id_user);
    $data['title']    = 'Missions';

    $sort = $this->input->get('sort');

    switch ($sort) {
      case 'user':
        $missions = $this->Mission->getAll($id_user, 'user');
        break;

      case 'completed':
        $missions = $this->Mission->getAll($id_user, 'completed');
        break;

      case 'waiting':
        $missions = $this->Mission->getAll($id_user, 'waiting');
        break;

      case 'uncompleted':
        $missions = $this->Mission->getAll($id_user, 'uncompleted');
        break;

      case '':
      default:
        $missions = $this->Mission->getAll($id_user);
        break;
    }

    $data['countFollowers'] = $this->social->countFollowers($id_user);
    $countFollowers = $data['countFollowers'];
    $data['missions'] = array_map(function ($m) use ($countFollowers)
    {

        $m->fb_injected  = false;
        $m->tw_injected  = false;
        $m->pin_injected = false;
        $m->text_injected = false;
        $m->link_injected = false;



        $m->max_followers = intval($m->max_followers); /* just in case :) */
        $m->min_followers = intval($m->min_followers);
        preg_match_all('/{(.*?)}/', $m->description, $matches);
        //print_r($matches[1]);
        foreach($matches[1] as $match) {
            $rules   = array();
            $evaled  = '';
            $content = '';
            if ($match == "facebook_button") {
                if ($m->fb_share && !$m->fb_shared) {
                    $content = '<div class="fb-link display-none">' . $m->fb_link . '</div>
                              <div class="fb-text display-none">"' . $m->fb_text . '"</div>
                              <div class="fb-share" value="' . $m->id . '" data-link="' . $m->fb_link . '" data-description="' . $m->fb_text . '">
                                  Share on <b>Facebook</b>
                              </div>';
                }
                if ($m->fb_share && $m->fb_shared) {
                    $content = '<div class="fb-share done" style="width: 200px; opacity: 0.6; margin: 0 0 10px 0; float: left;">You shared on <b>Facebook!</b></div><img style="display:inline; margin:5px 0 0 5px;" src="' . base_url('images/check_20.png') . '" /><br /><br />';
                }
                $m->fb_injected = true;
            } elseif ($match == "twitter_button") {
                if ($m->tw_share && !$m->tw_shared) {
                    $content = '<div class="fb-link display-none">' . $m->tw_link . '</div>
                                  <div class="fb-text display-none">"' . $m->tw_text . '"</div>
                                  <div class="tw-share" value="' . $m->id . '">
                                      <a href="https://twitter.com/intent/tweet?text=' . urlencode(trim($m->tw_text)) . '&amp;tw_p=tweetbutton&amp;url=' . $m->tw_link . '" target="_blank">
                                          Share on <b>Twitter</b>
                                      </a>
                                  </div>';
                }
                if ($m->tw_share && $m->tw_shared) {
                    $content = '<div class="tw-share" style="width: 180px; opacity: 0.6; margin: 0 0 10px 0; float: left;" onclick="return false;"><a href="#">You shared on <b>Twitter!</b></a></div><img style="display:inline; margin:5px 0 0 5px;" src="' . base_url('images/check_20.png') . '" /><br /><br />';
                }

                $m->tw_injected = true;
            } elseif ($match == "pinterest_button") {
                if ($m->pin_share && !$m->pin_shared) {
                    $content = '<div class="fb-link display-none">' . $m->pin_link . '</div>
                                  <div class="fb-text display-none">"' . $m->pin_text . '"</div>
                                  <div class="pin-share" value="' . $m->id . '">
                                      <a href="//www.pinterest.com/pin/create/button/?url=' . $m->pin_link . '&media=' . $m->pin_image . '&description=' . $m->pin_text . '" data-pin-do="buttonPin" data-pin-config="none" target="_blank">
                                          Share on <b>Pinterest</b>
                                      </a>
                                  </div>';
                }
                if ($m->pin_share && $m->pin_shared) {
                    $content = '<div class="pin-share" style="width: 193px; opacity: 0.6; margin: 0 0 10px 0; float: left;" onclick="return false;"><a href="#">You shared on <b>Pinterest!</b></a></div><img style="display:inline; margin:5px 0 0 5px;" src="' . base_url('images/check_20.png') . '" /><br /><br />';
                }

                $m->pin_injected = true;
            } elseif ($match == "link_box") {
                if (!$m->submitted || ($m->submitted && $m->status < 0)) {
                    $content       = '<input class="link" type="text" name="link" placeholder="' . $m->link_details . '">';
                    $m->link_injected = true;
                }
            } elseif ($match == "text_box") {
                if (!$m->submitted || ($m->submitted && $m->status < 0)) {
                    $content       = '<textarea class="link" name="text" placeholder="' . $m->text_details . '"></textarea>';
                    $m->text_injected = true;
                }
            } else {
                $pieces = explode('|', $match);
                if (count($pieces) > 1) {
                    foreach ($pieces as $p) {
                        if (strpos($p, ':') !== false) {
                            $rule                  = explode(':', $p);
                            $rules[trim($rule[0])] = trim($rule[1]);
                        } else {
                            $final_value = $p;
                        }
                    }
                } else {
                    $final_value = $pieces[0];
                }
                $new_match = str_replace('followers', '$countFollowers', $final_value);
                $evaled    = eval('return ' . $new_match . ';');
            }

            if (!empty($evaled) && !empty($rules)) {
                foreach ($rules as $rule_name => $rule_value) {
                    switch ($rule_name) {
                        case 'max':
                            if ($evaled > $rule_value) {
                                $evaled = $rule_value;
                            }
                            break;
                        case 'min':
                            if ($evaled < $rule_value) {
                                $evaled = $rule_value;
                            }
                            break;
                    }
                }
            }
            $m->description = str_replace("{".$match."}", !empty($evaled) ? number_format($evaled, 2, '.', ',') : $content , $m->description);

        }


        return $m;
    }, $missions);

    $data['totals'] = $this->db->query("SELECT COUNT(*) AS sales_no, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id_user}")->row();

    $points = $this->Points->getFromUser($id_user);
    $data['totals']->total_points = $points['total'];
    $data['totals']->bonus = $points['bonus'];

    $data['global_ranking'] = $this->users->getGlobalRanking($id_user);
    $data['country_ranking'] = $this->users->getCountryRanking($id_user);
    $data['city_ranking'] = $this->users->getCityRanking($id_user);

    $data['missions_message'] = $this->db->query("SELECT * FROM content WHERE id=16")->row()->body;



    //$data['followersOver500'] = $data['countFollowers']/500 > 160 ? 160 : $data['countFollowers']/500;

		$this->load->view('templates/header', $data);
		$this->load->view('pages/missions');
		$this->load->view('templates/footer');
  }


  public function send()
  {
      if (isset($_POST['trigger']) && $_POST['trigger'] == 'send') {
          $id_user    = ($this->session->userdata('id') == NULL) ? FALSE : $this->session->userdata('id');
          $id_mission = ($this->input->post('mission') == NULL) ? FALSE : $this->input->post('mission');

          $user = $this->users->get($id_user);

          $mission = $this->db->query("SELECT COUNT(*) AS submitted FROM missions_users WHERE id_user={$id_user} AND id_mission={$id_mission}")->row();

          $image = $this->Mission->uploadImage($_FILES, 'image');
          
          $mission_entry = $this->Mission->getById($id_mission);
          
          if($mission_entry->allow_link && empty($this->input->post('link')))
          {
             $this->session->set_flashdata(
                    'error',
                    "<div class='alert-box error form-alert'>The Link field is required.</div>"
                );
              redirect('/missions');
          }
          else
          {
              $link  = ($this->input->post('link') == NULL) ? FALSE : $this->input->post('link');
          }
          
          if($mission_entry->allow_text && empty($this->input->post('text')))
          {
              $this->session->set_flashdata(
                    'error',
                    "<div class='alert-box error form-alert'>The Text field is required.</div>"
            );
             
              redirect('/missions');
          }
          else
          {
              $text  = ($this->input->post('text') == NULL) ? FALSE : $this->input->post('text');
          }
          

          $data = array(
              'id_user' => $id_user,
              'id_mission' => $id_mission,
              'text' => $text,
              'link' => $link,
              'image' => $image,
              'status' => 0
            );

          $this->db->set('submitted', 'NOW()', false);

          if ($mission->submitted) {
              $this->db->where(array('id_user' => $id_user, 'id_mission' => $id_mission));
              $this->db->update('missions_users', $data);
          } else {
              $this->db->insert('missions_users', $data);
          }

          $this->AdminNotification->type  = 8;
          $this->AdminNotification->param = $id_mission.','.$id_user;
          $this->AdminNotification->insert();

          $this->Email->missionWaiting($user['email']);

          redirect('/missions');
      }
  }


  public function log_social_share_ajax() {
      $id_mission = ($this->input->post('id_mission') == NULL) ? FALSE : $this->input->post('id_mission');
      $network    = ($this->input->post('network') == NULL) ? FALSE : $this->input->post('network');
      $id_user    = ($this->session->userdata('id') == NULL) ? FALSE : $this->session->userdata('id');

      if($this->Mission->isShareMission($id_mission) && $this->Mission->isPostMission($id_mission)){
          $this->Mission->logSocialShare($id_mission, $id_user, $network);
      }
      else if ($this->Mission->isShareMission($id_mission)) {
          $this->Mission->logSocialShare($id_mission, $id_user, $network);
          if ($this->Mission->isShareCompleted($id_mission, $id_user)) {
              $this->Mission->approve($id_mission, $id_user);
          }
      }

      exit();
  }

}
