<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('social_model', 'Social');
    $this->load->model('stats_model', 'Stats');
    $this->load->model('Questions_model', 'questions');


    /* Facebook Stuff */
    parse_str( $_SERVER['QUERY_STRING'], $_REQUEST );
    $CI = & get_instance();
    $CI->config->load("facebook",TRUE);
    $config = $CI->config->item('facebook');
    $this->load->library('Facebook', $config);

    /* Twitter Stuff */
    $CI->config->load("twitter", TRUE);
    $config = $CI->config->item('twitter');
    $this->load->library('twconnect', $config);

    /* Instagram Stuff */
    $CI->config->load("instagram", TRUE);
    $config = $CI->config->item('instagram');
    $this->load->library('instagram_api', $config);

    /* Tumblr Stuff */
    $this->load->library('Tumblr');

    /* Linkedin Stuff */
    $CI->config->load("linkedin", TRUE);
    $config = $CI->config->item('linkedin');
    $this->load->library('linkedin', $config);

    /* YouTube Stuff */
    $this->load->library('youtube_lib');

    /* Vine Stuff */
    $this->load->library('vine_api');

    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->load->helper('url');

    $this->load->model('users');
    $this->load->model('admin_notification', 'AdminNotification');
    $this->load->model('payment_request', 'PaymentRequest');
    $this->load->model('coupon_model', 'Coupon');
    $this->load->model('email_model', 'Email');
    $this->load->model('points_model', 'Points');
    $this->load->model("site_config");


    $allowed_methods = array('facebook', 'twitter', 'twitter_callback', 'clear_twitter', 'instagram', 'instagram_callback', 'tumblr', 'tumblr_callback', 'pinterest', 'linkedin', 'linkedin_callback', 'youtube', 'vine');

    if (!in_array($this->router->fetch_method(), $allowed_methods)) {
      if (!$this->authentication->isLogged()) {
              $this->authentication->saveTheLastUrl();
              redirect('/auth/login');
      }
    }

  }

  public function facebook() {
      $id_user = $this->session->userdata('registered_user_id') ? $this->session->userdata('registered_user_id') : $this->session->userdata('id');
      $fb_user_id = $this->facebook->getUser();

      if(isset($_GET['next']) && $_GET['next'] == 'social') {
        $this->session->set_userdata('next', 'social');
      }

      if($fb_user_id == 0){
          $fb_url = $this->facebook->getLoginUrl(array('scope'=>'email,manage_pages,user_birthday'));
          redirect($fb_url);
      } else {
          $me = $this->facebook->api('/me');
          $fb_profile = $me['link'];
          $fb_friends = $this->facebook->api('/me/friends');
          $this->Social->updateNetwork('facebook', $id_user, count($fb_friends['data']), $fb_profile);
      }

      if ($this->session->userdata('next') && $this->session->userdata('next') == 'social') {
          $this->session->unset_userdata('next');
          redirect('/social');
      } else {
          redirect('/profile');
      }

  }

    public function facebook_profile($profile_id = false){
        if(!$profile_id){
            redirect('/profile');
        }
        $id_user = $this->session->userdata('id');
        $fb_user = $this->facebook->api('/'.$profile_id);
        $fb_profile = $fb_user['link'];
        $fb_friends = $this->facebook->api('/'.$profile_id.'/friends');
        $this->Social->updateNetwork('facebook', $id_user, count($fb_friends['data']), $fb_profile);
        redirect('/profile');
    }

    public function facebook_page($page_id = false){
        if(!$page_id){
            redirect('/profile');
        }
        $id_user = $this->session->userdata('id');
        $fb_page = $this->facebook->api('/'.$page_id);
        $fb_profile = $fb_page['link'];
        $fb_likes   = isset($fb_page['likes']) ? $fb_page['likes'] : 0;
        $this->Social->updateNetwork('facebook', $id_user, $fb_likes, $fb_profile);
        redirect('/profile');
    }

  	public function facebook_update(){
  		$this->facebook->destroySession();
  		redirect('/profile/facebook_update_cb');
  	}

  	public function facebook_update_cb(){
  		$fb_user_id = $this->facebook->getUser();
  		if($fb_user_id == 0){
  		    $fb_url = $this->facebook->getLoginUrl(array('scope'=>'email,manage_pages'));
  		    redirect($fb_url);
  		} else {
  			redirect('/profile');
  		}
  	}

  public function twitter() {
    $this->session->unset_userdata('tw_access_token');
    $this->session->unset_userdata('tw_status');

    if(isset($_GET['next']) && $_GET['next'] == 'social') {
        $this->session->set_userdata('next', 'social');
      }

    $success = $this->twconnect->twredirect();

    if (!$success) {
      echo 'Could not connect to Twitter. <br> <a href="'.base_url('/profile/clear_twitter').'">Retry</a>';
    }
  }

  public function twitter_callback() {
      $this->twconnect->twprocess_callback();

      $id_user = $this->session->userdata('registered_user_id') ? $this->session->userdata('registered_user_id') : $this->session->userdata('id');
      $tw_user = $this->twconnect->twaccount_verify_credentials();
      if (!$tw_user) {
          redirect('/profile/twitter');
      } else {
          $tw_profile = 'http://twitter.com/'.$tw_user->screen_name;
          $tw_followers = $tw_user->followers_count;
          $this->Social->updateNetwork('twitter', $id_user, $tw_followers, $tw_profile);
      }

      if ($this->session->userdata('next') && $this->session->userdata('next') == 'social') {
          $this->session->unset_userdata('next');
          redirect('/social');
      } else {
          redirect('/profile');
      }
  }

  public function clear_twitter() {
    $this->session->unset_userdata('tw_access_token');
    $this->session->unset_userdata('tw_status');

    redirect('/profile/twitter');
  }

  public function instagram() {
      $auth_url = $this->instagram_api->instagramLogin();

      if(isset($_GET['next']) && $_GET['next'] == 'social') {
        $this->session->set_userdata('next', 'social');
      }

      redirect($auth_url);
  }

  public function instagram_callback() {
      if(isset($_GET['code']) && $_GET['code'] != '') {
          $id_user = $this->session->userdata('registered_user_id') ? $this->session->userdata('registered_user_id') : $this->session->userdata('id');
          $auth_response = $this->instagram_api->authorize($_GET['code']);
          $inst_profile = 'http://instagram.com/'.$auth_response->user->username;

          $inst_user = $auth_response->user->id;

      	  $user_request_url = "https://api.instagram.com/v1/users/{$inst_user}/?access_token={$auth_response->access_token}";

          if ($this->Social->check40X($user_request_url, false)) {
              $inst_followers = 0;
          } else {
              $user = $this->instagram_api->getUser($inst_user);
              $inst_followers = $user->data->counts->followed_by;
          }

          $this->Social->updateNetwork('instagram', $id_user, $inst_followers, $inst_profile);

      }

      if ($this->session->userdata('next') && $this->session->userdata('next') == 'social') {
          $this->session->unset_userdata('next');
          redirect('/social');
      } else {
          redirect('/profile');
      }
  }

  public function tumblr() {

  $this->tumblr->reset_session();

	if(isset($_GET['next']) && $_GET['next'] == 'social') {
        $this->session->set_userdata('next', 'social');
    }

    $this->tumblr->handle_auth();
  }

  public function tumblr_callback() {
      $id_user = $this->session->userdata('registered_user_id') ? $this->session->userdata('registered_user_id') : $this->session->userdata('id');

      $access_token = $this->tumblr->handle_callback();
      $this->tumblr->create($this->tumblr->tumblr_consumer_key, $this->tumblr->tumblr_secret_key, $access_token['oauth_token'], $access_token['oauth_token_secret']);

      $data = $this->tumblr->get('http://api.tumblr.com/v2/user/info');
      $tub_followers = 0;
      $tub_profile = false;

      foreach ($data->response->user->blogs as $blog) {
          if (!$tub_profile) {
              $tub_profile = $blog->url;
          }
          $tub_followers += $blog->followers;
      }

      $this->Social->updateNetwork('tumblr', $id_user, $tub_followers, $tub_profile);

      if ($this->session->userdata('next') && ($this->session->userdata('next') == 'social')) {
          $this->session->unset_userdata('next');
          redirect('/social');
      } else {
          redirect('/profile');
      }
  }

  public function pinterest() {
      $id_user = $this->session->userdata('registered_user_id') ? $this->session->userdata('registered_user_id') : $this->session->userdata('id');

      if(isset($_GET['next']) && $_GET['next'] == 'social') {
        $this->session->set_userdata('next', 'social');
      }

      if (isset($_GET['username'])) {
          $username = $_GET['username'];
          $pin_profile = 'http://pinterest.com/'.$username.'/';

          if(!($this->Social->check40X($pin_profile))) {
            $metas = get_meta_tags($pin_profile);
            $pin_followers = $metas['pinterestapp:followers'];
          } else {
            $pin_followers = 0;
          }

          $this->Social->updateNetwork('pinterest', $id_user, $pin_followers, $pin_profile);

          if (($this->session->userdata('next') && $this->session->userdata('next') == 'social') || ($this->session->userdata('registered_user_id'))) {
              $this->session->unset_userdata('next');
              redirect('/social');
          } else {
              redirect('/profile');
          }
      } else {
          $data['countFollowers'] = $this->Social->countFollowers($this->session->userdata('id'));
          $this->load->view('pages/pinterest-request');
      }
  }

  public function linkedin() {

      if(isset($_GET['next']) && $_GET['next'] == 'social') {
        $this->session->set_userdata('next', 'social');
      }

      $this->linkedin->getRequestToken();
      $_SESSION['requestToken'] = serialize($this->linkedin->request_token);
      $auth_url = $this->linkedin->generateAuthorizeUrl();
      redirect($auth_url);
  }

  public function linkedin_callback() {
      $id_user = $this->session->userdata('registered_user_id') ? $this->session->userdata('registered_user_id') : $this->session->userdata('id');

      if (isset($_REQUEST['oauth_verifier'])){
            $_SESSION['oauth_verifier']     = $_REQUEST['oauth_verifier'];

            $this->linkedin->request_token    =   unserialize($_SESSION['requestToken']);
            $this->linkedin->oauth_verifier   =   $_SESSION['oauth_verifier'];
            $this->linkedin->getAccessToken($_REQUEST['oauth_verifier']);

            $_SESSION['oauth_access_token'] = serialize($this->linkedin->access_token);
            redirect('/profile/linkedin_callback');
       } else {
            $this->linkedin->request_token    =   unserialize($_SESSION['requestToken']);
            $this->linkedin->oauth_verifier   =   $_SESSION['oauth_verifier'];
            $this->linkedin->access_token     =   unserialize($_SESSION['oauth_access_token']);
       }

       $xml_response = $this->linkedin->getProfile("~:(public-profile-url)");
       $user = simplexml_load_string($xml_response);
       $user = get_object_vars($user);

       if (isset($user['public-profile-url'])) {
          $ln_profile = $user['public-profile-url'];
       } else {
          $ln_profile = '';
       }

       $xml_response = $this->linkedin->getProfile("~/connections");
       $connections = simplexml_load_string($xml_response);

       $ln_followers = count($connections->person);

       $this->Social->updateNetwork('linkedin', $id_user, $ln_followers, $ln_profile);

       if ($this->session->userdata('next') && $this->session->userdata('next') == 'social') {
           $this->session->unset_userdata('next');
           redirect('/social');
       } else {
           redirect('/profile');
       }
  }

  public function youtube() {
      $id_user = $this->session->userdata('registered_user_id') ? $this->session->userdata('registered_user_id') : $this->session->userdata('id');

      if(isset($_GET['next']) && $_GET['next'] == 'social') {
        $this->session->set_userdata(['next' => 'social']);
      }

      if ($this->input->get('username')) {
          $username = $this->input->get('username');

          $yb_followers = $this->youtube_lib->get_subs($username);

          if($yb_followers == -1){
              $data['errors'] = "username";
              $data['countFollowers'] = $this->Social->countFollowers($this->session->userdata('id'));
              $this->load->view('pages/youtube-request', $data);
          } else {
            $yb_profile   = 'https://www.youtube.com/user/'.$username;

            $this->Social->updateNetwork('youtube', $id_user, $yb_followers, $yb_profile);

            if (($this->session->userdata('next') && $this->session->userdata('next') == 'social') || ($this->session->userdata('registered_user_id'))) {
                $this->session->unset_userdata('next');
                redirect('/social');
            } else {
                redirect('/profile');
            }
          }
      } else {
          $data['countFollowers'] = $this->Social->countFollowers($this->session->userdata('id'));
          $this->load->view('pages/youtube-request');
      }
  }

  public function vine(){

      $id_user = $this->session->userdata('registered_user_id') ? $this->session->userdata('registered_user_id') : $this->session->userdata('id');

      if(isset($_GET['next']) && $_GET['next'] == 'social') {
        $this->session->set_userdata('next', 'social');
      }

      $this->form_validation->set_rules('user_id', 'User ID', 'required|numeric');

      if ($this->form_validation->run() && $this->input->post('user_id')) {
          $user_id = $this->input->post('user_id');

          $vine_user = $this->vine_api->getUser($this->input->post('user_id'));

          if($vine_user){

            $vn_followers = $vine_user->followerCount;
            $vn_profile   = 'https://vine.co/u/'.$user_id;

            $this->Social->updateNetwork('vine', $id_user, $vn_followers, $vn_profile);

            if (($this->session->userdata('next') && $this->session->userdata('next') == 'social') || ($this->session->userdata('registered_user_id'))) {
                $this->session->unset_userdata('next');
                redirect('/social');
            } else {
                redirect('/profile');
            }

        } else {

            $error = array('error' => 'This Vine User ID is not valid.');
            $this->load->view('pages/vine-request', $error);
        }

      } else {

          $data['countFollowers'] = $this->Social->countFollowers($this->session->userdata('id'));

          $this->load->view('pages/vine-request');
      }
  }

  public function index()
  {

    $id_user = $this->session->userdata['id'];
    $data['title'] = 'Profile';
    $data['user']  = $this->users->get_profile($this->session->userdata('id'));

    /* Facebook data */
    $fb_friends = $this->Social->checkNetwork('facebook', $id_user);
    if ($fb_friends || $fb_friends === "0") {
        $data['fb_friends'] = $fb_friends;
    }

    // Test if the user has linked a facebook page
    $this->db->select("*");
    $this->db->from('social_accounts');
    $this->db->where('id_user', $id_user);
    $this->db->where('network', 1);

    $result = $this->db->get()->result();
    $data['facebook_linked'] = empty($result) ? false : true;
    
    /* Twitter data */
    $tw_followers = $this->Social->checkNetwork('twitter', $id_user);
    if ($tw_followers || $tw_followers === "0") {
        $data['tw_followers'] = $tw_followers;
    }

    /* Instagram data */
    $inst_followers = $this->Social->checkNetwork('instagram', $id_user);
    if ($inst_followers || $inst_followers === "0") {
        $data['inst_followers'] = $inst_followers;
    }

    /* Tumblr Data */
    $tub_followers = $this->Social->checkNetwork('tumblr', $id_user);
    if ($tub_followers || $tub_followers === "0") {
        $data['tub_followers'] = $tub_followers;
    }

    /* Pinterest Data */
    $pin_followers = $this->Social->checkNetwork('pinterest', $id_user);
    if ($pin_followers || $pin_followers === "0") {
        $data['pin_followers'] = $pin_followers;
    }

    /* Linkedin Data */
    $ln_followers = $this->Social->checkNetwork('linkedin', $id_user);
    if ($ln_followers || $ln_followers === "0") {
        $data['ln_followers'] = $ln_followers;
    }

    /* YouTube Data */
    $yb_followers = $this->Social->checkNetwork('youtube', $id_user);
    if ($yb_followers || $yb_followers === "0" || $yb_followers == "-1") {
        $data['yb_followers'] = $yb_followers;
    }

    /* Vine Data */
    $vn_followers = $this->Social->checkNetwork('vine', $id_user);
    if ($vn_followers || $vn_followers === "0") {
        $data['vn_followers'] = $vn_followers;
    }

    $fb_pages = array();
    if($this->facebook->getUser()){

        $pages = $this->facebook->api('/me/accounts');
        if(isset($pages['data']) && count($pages['data'])){
            foreach ($pages['data'] as $k => $p) {
                $fb_pages[$k]["name"] = $p['name'].' - '.$p['category'];
                $fb_pages[$k]["id"] = $p['id'];
            }
        }

    }
    $data["fb_pages"] = $fb_pages;


    // $data['payments']   = $this->PaymentRequest->getUserItems($id_user);
    // $data['coupons']    = $this->Coupon->getUserItems($id_user);
    // $data['commission'] = $this->users->getCommission($this->session->userdata('id'));

    // $data['countries'] = $this->db->query("SELECT iso2 as code, short_name as name from country_t")->result();
    // $data['currencies'] = $this->db->query("SELECT code FROM currencies")->result();

    // $data['slaves']   = $this->db->get_where('auth_user', array('id_master' => $this->session->userdata('id')))->result();
    // $data['orders'] = $this->users->getSalesStats($this->session->userdata('id'), strtotime($this->input->get('begin')), strtotime($this->input->get('end')));

    $data['settings'] = $this->site_config->getGeneralSettings();

    // $data['invitations'] = $this->db->query("SELECT * FROM invitations WHERE id_user={$id_user}")->result();

    // $data['totals'] = $this->db->query("SELECT COUNT(*) as sales_no, SUM(points) as total_points, SUM(bonus) as bonus, SUM(total) as total_earnings FROM sales WHERE id_user=" . $this->session->userdata('id'))->row();
    $data['totals'] = $this->db->query("SELECT COUNT(*) AS sales_no, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id_user}")->row();

    $points = $this->Points->getFromUser($id_user);
    $data['totals']->total_points = $points['total'];
    $data['totals']->bonus = $points['bonus'];

    // $query = $this->db->query("SELECT * FROM country_t");
    // $data['countries'] = $query->result();
    $data['countries'] = array();
    // $data['giftcards'] = $this->db->query("SELECT * FROM payment_requests AS pr INNER JOIN coupons AS c ON c.id=pr.id_coupon WHERE id_user={$id_user} AND giftcard=1 ORDER BY pr.created DESC")->result();

    // $time     = date('Y-m-d G:i:s', strtotime("-1 day", time()));
    // $settings = $this->site_config->getGeneralSettings();

    // $data['coupons_24h'] = $this->db->query("SELECT COUNT(*) AS total FROM coupons WHERE type=1 AND id_creator={$id_user} AND created>='{$time}'")->row()->total;
    // $data['limit_24h']   = $settings['24h_discounts_per_user'];

    // $data['total_commission'] = $this->users->getTotalCommission($id_user);

    // $data['chart_sales_stats'] = $this->Stats->getCommissionsStatsByUser($id_user, strtotime($this->input->get('begin')), strtotime($this->input->get('end')));
    // $data['chart_sales_stats'] = $this->Stats->getSalesStatsByUser($id_user, strtotime($this->input->get('begin')), strtotime($this->input->get('end')));

    // if ($data['user']['level'] >= 1) {
    //     // $data['chart_sales_underkeepers'] = $this->Stats->getUnderkeepersSalesStats($id_user, strtotime($this->input->get('begin')), strtotime($this->input->get('end')));
    //     // $data['underkeepers_stats']   = $this->users->getUnderkeepersStats($id_user, strtotime($this->input->get('begin')), strtotime($this->input->get('end')));

    //   $sales_underkeepers = $this->Stats->getUnderkeepersCommissionStats($id_user, strtotime($this->input->get('begin')), strtotime($this->input->get('end')));

    //   // bild array for charts
    //   $data['chart_sales_underkeepers']['total'] = $sales_underkeepers['total'];
    //   $data['chart_sales_underkeepers']['graph_title'] = $sales_underkeepers['graph_title'];
    //   $data['chart_sales_underkeepers']['y_axis_label'] = $sales_underkeepers['y_axis_label'];

    //   if (isset($sales_underkeepers['keys'])) {
    //     foreach ($sales_underkeepers['keys'] as $key => $value) {
    //       $data['chart_sales_underkeepers']['keys'][$key] = $sales_underkeepers['keys'][$key];
    //       $data['chart_sales_underkeepers']['values'][$key] = $sales_underkeepers['values'][$key];
    //       if (count($data['chart_sales_underkeepers']['keys']) == 12) {
    //         break;
    //       }
    //     }
    //   }

    //   $data['sales_underkeepers'] = $sales_underkeepers;
    // }

    $data['global_ranking'] = $this->users->getGlobalRanking($id_user);
    $data['country_ranking'] = $this->users->getCountryRanking($id_user);
    $data['city_ranking'] = $this->users->getCityRanking($id_user);


    // get content
    // $data['coupons_title'] = $this->db->query("SELECT * FROM content WHERE id=4")->row()->body;

    // $data['coupons_single'] = $this->db->query("SELECT * FROM content WHERE id=5")->row()->body;
    // $data['coupons_single'] = str_replace('{DISCOUNT_PERCENTAGE}', $data['settings']['default_discount_percentage'], $data['coupons_single']);
    // $data['coupons_single'] = str_replace('{DISCOUNT_TIME}', $data['settings']['default_coupon_active_time'], $data['coupons_single']);
    // $data['coupons_single'] = str_replace('{COUPONS_PER_DAY}', $data['limit_24h'], $data['coupons_single']);
    // $data['coupons_single'] = str_replace('{COUPONS_LEFT}', intval($data['limit_24h'] - $data['coupons_24h']), $data['coupons_single']);

    // $data['coupons_multiple'] = $this->db->query("SELECT * FROM content WHERE id=6")->row()->body;

    // $data['underkeepers_title'] = $this->db->query("SELECT * FROM content WHERE id=7")->row()->body;

    // $data['underkeepers_content'] = $this->db->query("SELECT * FROM content WHERE id=8")->row()->body;
    // $data['underkeepers_content'] = str_replace('{UNDERKEEPER_TOTAL}', $data['user']['slaves'], $data['underkeepers_content']);
    // $data['underkeepers_content'] = str_replace('{INVITATIONS_LEFT}', intval($data['user']['slaves'] - $data['user']['slave_invitations_sent']), $data['underkeepers_content']);

    // $data['payment_request_content'] = $this->db->query("SELECT * FROM content WHERE id=14")->row()->body;

    // $data['last_slaves_request'] = $this->db->query("SELECT * FROM slaves_requests WHERE id_user={$id_user} ORDER BY created DESC")->row();

    $data['countFollowers'] = $this->Social->countFollowers($this->session->userdata('id'));

    $data['questions'] = $this->questions->getAllByUser( $id_user );

    $answered_q_ids = array();
    foreach ($data['questions'] as $answered_q) {
      $answered_q_ids[] = $answered_q->id;
    }

    $all_q = $this->questions->getAll( $id_user );

    $data['unansw_questions'] = array();

    foreach ($all_q as $q) {

      if(!in_array($q->id, $answered_q_ids)) {
        $data['unansw_questions'][] = $q;
      }

    }

		$this->load->view('templates/header', $data);
		$this->load->view('pages/profile');
		$this->load->view('templates/footer');
  }

  public function save($id)
  {


    $j = 0;

    while($this->input->post('question-ref-'.$j)) {

      $q_id = $this->input->post('question-ref-'.$j);
      $q_value = $this->input->post('question-'.$q_id);

      unset($_POST['question-ref-'.$j]);

      unset($_POST['question-'.$q_id]);

      $this->questions->save_answer($id,$q_id,$q_value);

      $j++;
    }

    $this->users->profile_update($id);

    $pic1 = $this->users->uploadImage($_FILES, 'profile_pic');

    if ($pic1) {
        $this->db->where('id', $id);
        $this->db->set('profile_pic', "'".$pic1."'", false);
        $this->db->update('auth_user');
    }
    else
    {
        $this->session->set_flashdata('picture_upload_false',true);
    }



    if ($this->users->is_admin($this->session->userdata('id')))
    {
      redirect('/admin/user/view/'.$id);
    }
    else
    {
      redirect('/profile');
    }
  }


  public function master_apply(){
      $id_user = $this->session->userdata('id');
      $user    = $this->users->getByID($id_user);

      if($user->level == "0"){
          $this->users->applyMaster($id_user);
      }

      redirect('/welcome');
  }

}
