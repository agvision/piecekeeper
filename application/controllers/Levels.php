<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Levels extends CI_Controller
{
  public function __construct() {

    parent::__construct();
    $this->load->model('points_model', 'Points');
    $this->load->helper('url');
    $this->load->model('Social_model', 'social');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }
  }


  public function index() {

    $id_user = $this->session->userdata('id');
    $data['user']     = $this->users->get($id_user);
    $data['title']    = 'Levels';

    $data['totals'] = $this->db->query("SELECT COUNT(*) AS sales_no, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id_user}")->row();

    $data['details'] = $this->db->query("SELECT * FROM content WHERE id=22")->row()->body;;

    $points = $this->Points->getFromUser($id_user);
    $data['totals']->total_points = $points['total'];
    $data['totals']->sales_points = $points['sales'];
    $data['totals']->bonus = $points['bonus'];
    $data['totals']->total_earnings = $this->users->getCommission($id_user);

    $data['global_ranking'] = $this->users->getGlobalRanking($id_user);
    $data['country_ranking'] = $this->users->getCountryRanking($id_user);
    $data['city_ranking'] = $this->users->getCityRanking($id_user);

    $this->load->model("site_config");
    $data['settings'] = $this->site_config->getGeneralSettings();

    $data['countFollowers'] = $this->social->countFollowers($this->session->userdata('id'));

	$this->load->view('templates/header', $data);
	$this->load->view('pages/levels');
	$this->load->view('templates/footer');
  }

}
