<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sms extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('twilio_sms_model', 'SMS');

    if (! $this->session->userdata('id') ) {
      redirect('/auth/login');
    }

  }

  public function index()
  {

    $this->SMS->from('+16602173052');
    $this->SMS->to('+40722483401');
    $this->SMS->message('Buna');

    $this->SMS->send();

    exit();
  }


}
