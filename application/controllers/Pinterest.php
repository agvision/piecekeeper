<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pinterest extends CI_Controller
{
  public function __construct() {

    parent::__construct();      
    $this->load->model('mission_model', 'Mission');
    $this->load->library('email'); 
  }


  public function check_share() {
      $user_agent = $_SERVER['HTTP_USER_AGENT'];

      $data = $this->input->get('s');
      $data = explode('**', $data);

      $m = $data['0'];
      $u = $data['1'];
      $s = $data['2'];

      if (preg_match("/^Pinterest(.*)$/", $user_agent, $matches)) {
          $id_mission = $m;
          $id_user    = $u;
          if($this->Mission->isShareMission($id_mission) && $this->Mission->isPostMission($id_mission)){
              $this->Mission->logSocialShare($id_mission, $id_user, "pin");
          }
          else if ($this->Mission->isShareMission($id_mission)) {
              $this->Mission->logSocialShare($id_mission, $id_user, "pin");
              if ($this->Mission->isShareCompleted($id_mission, $id_user)) {
                  $this->Mission->approve($id_mission, $id_user);
              }
          }
      }

      $image = $s;
      redirect($image);

  }

}
