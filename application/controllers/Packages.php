<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Packages extends CI_Controller
{

  public function __construct() {

    parent::__construct();
    $this->load->model('package_model', 'Package');
    $this->load->model("site_config");
    $this->load->model('points_model', 'Points');
    $this->load->model('ordered_products', 'OrderedProduct');
    $this->load->model('country');
    $this->load->model('email_model', 'Email');
    $this->load->model('Validation');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }
  }


  public function index() {
    redirect('/');
  }


  public function details($id_package){
      $id_user = $this->session->userdata('id');
      $user    = $this->users->get($id_user);

      $package = $this->Package->getById($id_package);

      $data['totals'] = $this->db->query("SELECT COUNT(*) AS sales_no, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id_user}")->row();
      $points = $this->Points->getFromUser($id_user);
      $data['totals']->total_points = $points['total'];
      $data['global_ranking'] = $this->users->getGlobalRanking($id_user);

      $data['package'] = $package;
      $data['user']    = $user;
      $data['title']   = 'Package Details';

      $this->load->view('templates/header', $data);
      $this->load->view('pages/package_details');
      $this->load->view('templates/footer');
  }

  // function that manage level 2 gift
  public function gift()
  {
        // get user general informations
        $id_user = $this->session->userdata('id');

        $user    = $this->users->get_profile($id_user);
        $settings = $this->site_config->getGeneralSettings();
        $countries = $this->db->query("SELECT * FROM country_t")->result();
        $data['states'] = $this->db->query("SELECT * FROM states")->result();

        // redirect user if he is not level 2
        if( intval($user['level']) != 0 && intval($user['level']) != -1 ) {
          redirect('/');
        }

        // redirect the user if his package is aproved
        if($this->Package->isApproved($id_user, '1')) {
          redirect('/');
        }

        // verify if the user have a level 2 package if he donsen't have create it
        if (!($this->users->getLevel2Package($id_user))) {
             $this->Package->createLevel2Package($id_user);
         }

         //test if the form is sent from view
         if(!empty($this->input->post('email')))
         {
             $email                    = ($this->input->post('email') == NULL) ? FALSE : $this->input->post('email');
             $phone                    = ($this->input->post('phone') == NULL) ? FALSE : $this->input->post('phone');

             if(!$this->Validation->validate('phone', $phone))
             {
                 $this->session->set_flashdata('package_error', array('Your phone number is not valid.'));
                 $this->session->keep_flashdata('package_error');
                 redirect('/packages/gift');
             }


             $shipping_first_name      = ($this->input->post('shipping_first_name') == NULL) ? FALSE : $this->input->post('shipping_first_name');
             $shipping_last_name       = ($this->input->post('shipping_last_name') == NULL) ? FALSE : $this->input->post('shipping_last_name');
             $shipping_address_1       = ($this->input->post('shipping_address_1') == NULL) ? FALSE : $this->input->post('shipping_address_1');
             $shipping_address_2       = ($this->input->post('shipping_address_2') == NULL) ? FALSE : $this->input->post('shipping_address_2');
             $shipping_zip             = ($this->input->post('shipping_zip') == NULL) ? FALSE : $this->input->post('shipping_zip');
             $shipping_city            = ($this->input->post('shipping_city') == NULL) ? FALSE : $this->input->post('shipping_city');
             $shipping_access_code     = ($this->input->post('shipping_access_code') == NULL) ? FALSE : $this->input->post('shipping_access_code');
             $shipping_state           = ($this->input->post('shipping_state') == NULL) ? FALSE : $this->input->post('shipping_state');
             $shipping_country         = ($this->input->post('shipping_country') == NULL) ? FALSE : $this->input->post('shipping_country');


             $country_information = $this->country->getByShortName($shipping_country);
             $currency = $country_information->currency;
             if(empty($currency)) $currency = "EUR";
             $country = $country_information->iso2;

             $order = array(
                 'currency'            => $currency,
                 'email'               => $email,
                 'phone'               => $phone ,
                 'gender'              => $user['gender'],
                 'total'               => 0,
                 'shipping_first_name' => $shipping_first_name,
                 'shipping_last_name'  => $shipping_last_name,
                 'shipping_address_1'  => $shipping_address_1,
                 'shipping_address_2'  => $shipping_address_2,
                 'shipping_house_no'   => '',
                 'shipping_house_ext'  => '',
                 'shipping_zip'        => $shipping_zip,
                 'shipping_city'       => $shipping_city,
                 'shipping_state'      => $shipping_state,
                 'shipping_country'    => $shipping_country,
                 'billing_first_name'  => $shipping_first_name,
                 'billing_last_name'   => $shipping_last_name,
                 'billing_address_1'   => $shipping_address_1,
                 'billing_address_2'   => $shipping_address_2,
                 'billing_house_no'    => '',
                 'billing_house_ext'   => '',
                 'billing_zip'         => $shipping_zip,
                 'billing_city'        => $shipping_city,
                 'billing_state'       => $shipping_state, // US Only
                 'country'             => $country
             );

             // save the order details in ordered_products table
             $this->OrderedProduct->insertNewOrderedProduct( $order );
             $this->users->setLevel2Product($id_user, $this->db->insert_id());

             redirect('/');
           }



           $page_message = $this->db->get_where('content', array('id' => 17))->row()->body;

           $data['totals'] = $this->db->query("SELECT COUNT(*) AS sales_no, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id_user}")->row();

           $points = $this->Points->getFromUser($id_user);
           $data['totals']->total_points = $points['total'];
           $data['global_ranking'] = $this->users->getGlobalRanking($id_user);

           $data['page_message']     = $page_message;
           $data['user']             = $user;
           $data['title']            = "Confirm Package";
           $data['user']             = $user;
           $data['countries']        = $countries;

           // render the view
           $this->load->view('templates/header', $data);
           $this->load->view('pages/confirm_package');
           $this->load->view('templates/footer');
  }


  public function free_product(){

    $id_user = $this->session->userdata('id');
    $user    = $this->users->get($id_user);

    if( (intval($user['level']) < 0)){
      redirect('/');
    }

    if (!($this->users->getLevel3Package($id_user))) {
      $this->Package->createLevel3Package($id_user);
    }

    redirect("/products");
  }

  public function send_package_reminder($id_user, $level)
  {
    if(empty($level) || empty($id_user))
    {
      redirect('/admin/user');
    }

    $user = $this->users->get_profile($id_user);

    switch($level)
    {
      case 2:
          $this->Email->sendLevel2Package($user['email']);
        break;
      case 3:
          $this->Email->sendLevel3Package($user['email']);
        break;

    }

    redirect('/admin/user/view/' . $id_user);
  }


}
