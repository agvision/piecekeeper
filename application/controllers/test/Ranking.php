<?php

class Ranking extends CI_Controller {
    
    public function __construct()
    {
    	parent::__construct();
        $this->load->model('users', 'User');
    }

    public function get($id_user, $last_month = false) {
     //    $this->load->driver('cache');
     //    $user = $this->users->get($id_user);

     //    $cached = false;
    
    	// // get from cache
     //    if ($this->cache->memcached->is_supported()) {
      
     //        if ($last_month) {
     //          	$ranking = $this->cache->memcached->get('last_month_global_ranking_'.$id_user);
     //        } else {
     //        	$ranking = $this->cache->memcached->get('current_global_ranking_'.$id_user);
     //        }

     //        if ($ranking) {
     //        	$cached = true;
     //        }
     //    }

     //    if ($cached) {
     //    	return $ranking;
     //    } 

     //    // construct queries
     //    if ($last_month) {
     //      	$last_month = date('Y-m-d H:i:s', strtotime('-1 month'));
     //        $points = $this->db->query("SELECT SUM(value) AS total FROM points WHERE id_user={$id_user} AND created<='{$last_month}'")->row();
              
     //     	if (!$points->total) {
     //        	$points->total = 0;
     //      	}

     //      	$sql = "SELECT u.id, SUM(p.value) AS total, u.date_joined FROM points AS p RIGHT JOIN auth_user AS u ON p.id_user=u.id WHERE p.created<='{$last_month}' GROUP BY u.id HAVING total>'{$points->total}' OR ((total='{$points->total}' OR '{$points->total}'='0') AND u.date_joined<'".$user['date_joined']."')";

     //    } else {
     //      	$points  = $this->db->query("SELECT SUM(value) AS total FROM points WHERE id_user={$id_user}")->row();
     //      	if (!$points->total) {
     //        	$points->total = 0;
     //      	}

     //      	$sql = "SELECT u.id, SUM(p.value) AS total, u.date_joined FROM points AS p RIGHT JOIN auth_user AS u ON p.id_user=u.id GROUP BY u.id HAVING total>'{$points->total}' OR ((total='{$points->total}' OR '{$points->total}'='0') AND u.date_joined<'".$user['date_joined']."')";
     //    }

     //    // compute if not cached
     //    $ranking = $this->db->query($sql)->result();

     //    // save to cache
     //    if ($this->cache->memcached->is_supported()) {
     //    	if ($last_month) {
     //    		$this->cache->memcached->save('last_month_global_ranking_'.$id_user, count($ranking)+1, 86400);
     //    	} else {
     //    		$this->cache->memcached->save('current_global_ranking_'.$id_user, count($ranking)+1, 86400);
     //    	}
     //    }

     //    return count($ranking)+1;
    }

    public function get_group($ids_users = null, $last_month = false){
  //   	$this->load->driver('cache');

  //   	// construct query
  //       $this->db->select("u.id, u.id_country, u.first_name, u.last_name, SUM(p.value) AS total_points");
  //       $this->db->from("auth_user AS u");
  //       $this->db->join('points AS p', 'p.id_user = u.id', 'left');
  //       $this->db->group_by("u.id");
  //       $this->db->order_by("total_points DESC, u.date_joined ASC");

  //       if ($last_month) {
  //       	$last_month = date("Y-m-d H:i:s", strtotime("-1 month"));
  //       	$this->db->where("p.created <= '{$last_month}' OR p.created IS NULL");
  //       }

  //       // get users list ordered by ranking
  //       $result = $this->db->get()->result();

  //       $rankings = array();
  //       $place = 1;
		
		// // compute place        
  //       foreach ($result as $r) {
  //           if(!$ids_users || isset($ids_users['user_'.$r->id])){
  //               $r->place = $place;
  //               $rankings['user_'.$r->id] = $r;

  //               if ($this->cache->memcached->is_supported()) {
  //               	if ($last_month) {
  //               		$this->cache->memcached->save('last_month_global_ranking_'.$r->id, $place, 86400);
  //               	} else {
  //               		$this->cache->memcached->save('current_global_ranking_'.$r->id, $place, 86400);
  //               	}
  //               }

  //           }
  //           $place++;
  //       }

  //       return $rankings;
    }

}