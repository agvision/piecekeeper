<?php

class RepairArchive extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Social_model', 'Social');
    }

    public function index()
    {
        $count = 0;
        $good = 0;
        $bad = [];
        $stats = [];
        $this->db->select("id_user, network, profile");
        $this->db->from("social_accounts_archive");
        $archive = $this->db->get()->result_array();
        foreach ($archive as $row)
        {
            //$query = $this->db->query("SELECT id_user, network, profile FROM social_accounts WHERE profile = '0' AND id_user = {$row['id_user']}");
            $this->db->select("id_user, network, profile");
            //$this->db->where("profile", "0"); // Nope
            $this->db->where("id_user", $row['id_user']);
            $this->db->where("network", $row['network']);
            $this->db->from("social_accounts");
            $exists = $this->db->get()->row_array();
           if(!empty($exists))
           {
               if(empty($exists['profile'])) {
                   $this->db->where("id_user", $row['id_user']);
                   $this->db->where("network", $row['network']);
                   $this->db->delete("social_accounts");
                   $data = [];
                   $data['id_user'] = $row['id_user'];
                   $data['network'] = $row['network'];
                   $data['profile'] = $row['profile'];
                   $data['updated'] = 0;
                   if (isset($stats[$row['network']])) $stats[$row['network']]++; else $stats[$row['network']] = 0;
                   $this->db->insert("social_accounts", $data);
                   $count++;
               }
               else
               {
                   if($exists['profile'] == $row['profile'])
                   {
                        $good++;
                   }
                   else
                   {
                       array_push($bad, $row['id_user']);
                   }
               }
           } else
           {
               $data = [];
               $data['id_user'] = $row['id_user'];
               $data['network'] = $row['network'];
               $data['profile'] = $row['profile'];
               $data['updated'] = 0;
               if(isset($stats[$row['network']])) $stats[$row['network']]++; else $stats[$row['network']] = 0;
               $this->db->insert("social_accounts", $data);
               $count++;
           }







        }
        echo "System has made <b>".$count."</b> restorations..";
        foreach($stats as $k => $v)
        {
            echo "<br /> On network ".$k.": ".$v." restorations.";
        }
        echo "<br /> Before this restoration, ".$good." profiles already existed in social_accounts.";
        if(count($bad))
        {
            echo "<br /> Profiles are mismatching for the following users: ".implode(", ", $bad);
        }
    }

}