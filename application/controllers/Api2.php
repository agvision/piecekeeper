<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api2 extends CI_Controller {


    public function __construct() {
        parent::__construct();

        $this->load->model('Api_keys_model', 'Api_key');
        $this->load->model('api_model', 'API');
        $this->load->model('Users', 'Users');
        $this->load->model('Email_model', 'Email');
        $this->load->model('auth_logs_model', 'auth_logs');
        $this->load->model('Coupon_model', 'Coupons');
        $this->load->model('mission_model', 'Mission');
        $this->load->model("site_config");
        $this->load->model("State");
        $this->load->model("School");
        $this->load->library('currency');
        $this->load->model('admin_notification', 'AdminNotification');
        $this->load->model('payment_request', 'PaymentRequest');
        $this->load->model('blog_entry', 'BlogEntry');
        $this->load->model('Social_model', 'Social');

        $this->load->library('form_validation');
        $this->load->helper('form');

        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');

        if(!empty($this->input->get('key')))
        {
           $received_key = $this->input->get('key');
        }

        if(!empty($this->input->post('key')))
        {
           $received_key = $this->input->post('key');
        }

        //Key for developement
        if($received_key == '9ca6b938f38401bc21c3302e519e67c02ea7f753')
        {
            $data = array(
              'id'    => '4',
              'email' => 'mihaigeorge_c@yahoo.com',
              'level' => '3'
            );

            $this->session->set_userdata($data);
        }
        // Verify if the key exist in database
        else if( empty($received_key) || !$this->Api_key->VerifyKey($received_key) )
        {

           $this->output->set_status_header('403');
           exit();
        }

    }

    /* Main */
    public function index()
    {
        $this->output->set_status_header('403');
        exit();
    }

    /*
    * Check if user is logged in
    * @return bool result
    */
    public function isLogged()
    {
        // Create restult for return
        $result = array();

        // Check if the user is logged
        if($this->authentication->isLogged())
        {
            $result['logged'] = true;
        }
        else
        {
            $result['logged'] = false;
        }

        // Return the response
        echo json_encode($result);
        exit();

    }

    /*
    * Function for log out
    * @return bool result
    */
    public function logout()
    {
        // Create restult for return
        $result = array();

        // Test if the user is logged in
        if(!$this->authentication->isLogged())
        {
            $this->output->set_status_header('403');
            exit();
        }
        else
        {
            // Get the session user date
            $id_user = $this->session->userdata('id');

            $this->session->sess_destroy();

            $result['logout'] = true;
        }

        // Return the response
        echo json_encode($result);
        exit();
    }


    /*
    * login function
    * @param string email
    * @param string password
    * @return array result
    */
    public function login()
    {

        //create restult for return
        $result = array();

        //get the parameters sent from aplication
        $email      = $this->input->get('email');
        $password   = $this->input->get('password');

        //test if the data is not empty
        if( empty($email) || empty($password) )
        {
            $result['logged'] = false;
            echo json_encode($result);

            exit();
        }

        //try to login the user
        $user = $this->Users->login($email, $password);

        //get the client IP
        $client_ip = $_SERVER["REMOTE_ADDR"];


        if($this->auth_logs->blocked($client_ip))
        {
            //test if the user is blocked
            $this->output->set_status_header('403');
        }
        else if (!$user)
        {
            //add login attempt if login failed
            $this->auth_logs->add_attempt($client_ip, 0);

            $result['logged'] = false;
        }
        else
        {
            $this->db->where('id', $user['id']);
                $this->db->update('auth_user', array('last_login' => date('Y-m-d H:i:s', time())));

            $data = array(
              'id'    => $user['id'],
              'email' => $user['email'],
              'level' => $user['level']
            );

            $this->session->set_userdata($data);

            // refresh invalid email sent
            $this->db->set('inactive_email_sent', 0, false);
            $this->db->where('id', $user['id']);
            $this->db->update('auth_user');

            $this->auth_logs->add_attempt($client_ip, 1 );

            //login succes
            $result['logged'] = true;
            $result['id'] = $user['id'];
        }

        //return the response
        echo json_encode($result);
        exit();
    }

    /*
    * Discount Code function
    * @return array result
    */
    public function discountCode($action = null)
    {
        $settings = $this->site_config->getGeneralSettings();
        $time      = date('Y-m-d G:i:s', strtotime("-1 day", time()));
        $last_week = date('Y-m-d G:i:s', strtotime("-1 week", time()));

        // Test if the user is logged in
        if(!$this->authentication->isLogged())
        {
            $this->output->set_status_header('403');
            exit();
        }
        else
        {
            //get the session user date
            $id_user = $this->session->userdata('id');

            //create result variable to send to aplication
            $result = array();

            // Test if I want to create new coupon
            if($action == 'create')
            {
                $couponType = '';
                $couponCode = '';
                $couponLink = '';
                // Get all the post variable send form application
                $couponType = $this->input->post('type');
                $couponCode = $this->input->post('code');
                $couponLink = $this->input->post('link');

                // Flag for create multiuse coupon
                $multiuseCouponFlag = 0;

                // Test if type or code coupon is empty
                empty($couponType) ? $result['errors'][] = 'Type required' : '';
                empty($couponCode) ? $result['errors'][] = 'Code is required' : '';

                if(!empty($couponCode))
                {
                    $this->form_validation->set_rules('code', 'Coupon Code', 'max_length[30]|is_unique[coupons.code]|alpha_numeric');
                    if($this->form_validation->run() == FALSE)
                    {
                         $result['errors'][] = str_replace("\n","",strip_tags(validation_errors()));
                    }
                }

                // Test if is any request for multiuse coupon
                if( $couponType == 2 && !empty($couponCode) && !array_key_exists('errors', $result))
                {
                    (empty($couponLink)) ? $result['errors'][] = 'Link is required' : $multiuseCouponFlag = 1;
                }
                else if( $couponType == 2 && empty($couponCode) || $couponType == 2 && !empty($couponCode))
                {
                    (empty($couponLink)) ? $result['errors'][] = 'Link is required' : '';
                }

                // Test if a single use coupon code was created in the past 24 hours
                if( $couponType == 1)
                {

                    // check 24h limitations
                    $coupons_24h = $this->Coupon->getCoupon24($id_user,$time);
                    $limit_24h = $settings['24h_discounts_per_user'];

                    if (intval($coupons_24h->total) >= intval($limit_24h))
                    {
                      $result['errors'][] = 'You have reached the 24 hours limit';
                    }
                }

                // Test if a multiuse use coupon code was created in the past 1 week
                if( $couponType == 2)
                {
                    // check weekly codes limitation
                    $coupons_this_week = $this->Coupon->getCouponsForThisWeek($id_user, $last_week);
                    $limit_week = $settings['multiuse_codes_per_week'];

                    if (intval($coupons_this_week->total) >= intval($limit_week))
                    {
                        $result['errors'][] = 'You have reached the multiuse codes limit for the past 7 days.';
                    }
                }

                // Check if any error exist
                if(!array_key_exists('errors', $result))
                {
                    if($multiuseCouponFlag == 1)
                    {
                        $code         = strtoupper($couponCode);
                        $percent      = $settings['default_discount_percentage'];
                        $amount       = FALSE;
                        $currency     = FALSE;
                        $u_limit      = FALSE;
                        $p_limit      = FALSE;
                        $freeshipping = FALSE;
                        $link         = ($couponLink == NULL) ? FALSE : $couponLink;

                        $id_coupon = $this->Coupon->createMultipleUse($id_user, $code, $percent, $amount, $currency, '', $link, $u_limit, $p_limit, $freeshipping, true, false);
                    }
                    else
                    {
                        $code      = strtoupper($couponCode);
                        $id_coupon = $this->Coupon->createSingleUse($id_user, $code, '');

                    }

                    $result['created'] = true;
                }
                else if(array_key_exists('errors', $result))
                {
                  $result['created'] = false;
                }
            }
            else
            {

                //get all coupons asociated with the logged user
                $coupons = $this->Coupons->findAllActiveByIdUser($id_user);

                if(empty($coupons))
                {
                    $couponComponent = array();

                    $result['discount_codes'] = $couponComponent;
                }
                else
                {
                    $couponComponent = array();

                    foreach($coupons as $coupon)
                    {
                        $couponComponent[] = array(
                                'code' => $coupon->code,
                                'discount' => $coupon->percent,
                                'expires' => $this->Users->formatDate($id_user, strtotime($coupon->to), true)
                        );
                    }

                    $result['discount_codes'] = $couponComponent;
                }

            }

            echo json_encode($result);
        }
    }

    /*
    * Levels function
    * @return array result
    */
    public function levels()
    {

        // Test if the user is logged in
        if(!$this->authentication->isLogged())
        {
            $this->output->set_status_header('403');
            exit();
        }
        else
        {

            // Create result variable to send to aplication
            $result = array();

            // Get the level details
            $data['details'] = $this->db->query("SELECT * FROM content WHERE id=22")->row()->body;

            // Get the discount percent for every level
            $data['settings'] = $this->site_config->getGeneralSettings();

            // Array that will contain info for every level
            $level_one_info = array();
            $level_two_info = array();
            $level_three_info = array();

            // Get the content of div that contain the level 1 info
            preg_match_all("/<div class=\"column-text-line level-1-info\">([^`]*?)<\/div>/", $data['details'], $level_one_info);

            // Get the content of div that contain the level 2 info
            preg_match_all("/<div class=\"column-text-line level-2-info\">([^`]*?)<\/div>/", $data['details'], $level_two_info);

            // Get the content of div that contain the level 3 info
            preg_match_all("/<div class=\"column-text-line level-3-info\">([^`]*?)<\/div>/", $data['details'], $level_three_info);

            // Test of I have any info for level 1 and take it
            if(!empty($level_one_info[1][0]))
            {
                preg_match_all("/-([^`]*?)<br \/>/", $level_one_info[1][0], $level_one_info);
            }
            else
            {
                $level_one_info = array();
            }

            // Test of I have any info for level 2 and take it
            if(!empty($level_two_info[1][0]))
            {
                preg_match_all("/-([^`]*?)<br \/>/", $level_two_info[1][0], $level_two_info);
            }
            else
            {
                $level_two_info = array();
            }

            // Test of I have any info for level 3 and take it
            if(!empty($level_three_info[1][0]))
            {
                preg_match_all("/-([^`]*?)<br \/>/", $level_three_info[1][0], $level_three_info);
            }
            else
            {
                $level_three_info = array();
            }

            // Store details for level 1
            $result['level_1'] = array(
                'commission' => $data['settings']['level1_commission'],
                'details' => $level_one_info[1]
            );

            // Store details for level 2
            $result['level_2'] = array(
                'commission' => $data['settings']['level2_commission'],
                'details' => $level_two_info[1]
            );

            // Store details for level 3
            $result['level_3'] = array(
                'commission' => $data['settings']['level3_commission'],
                'details' => $level_three_info[1]
            );

            echo json_encode($result);
        }
    }

    /**
    * Missions function
    * @param string status
    * @return array result
    */
    public function missions($id_mission = null, $type = null)
    {

        // Test if the user is logged in
        if(!$this->authentication->isLogged())
        {
            $this->output->set_status_header('403');
            exit();
        }
        else
        {
            // Result array
            $result = array();

            // Get the session user date
            $id_user = $this->session->userdata('id');
            $user = $this->users->get($id_user);

            // Test if is sent an call for submit mission or get missions
            if(!empty($id_mission) && empty($type))
            {
                if(!empty($this->Mission->getById($id_mission)))
                {
                    $text  = $this->input->post('text');
                    $link  = $this->input->post('link');

                    if($this->Mission->isPostTextMission($id_mission))
                    {
                        // Test if the text is sent
                        if(empty($text))
                        {
                            // Retrieve error
                            $result = array(
                                'error' => 'Text is required',
                                'submitted' => false
                            );
                        }
                    }
                    else
                    {
                        $text = false;
                    }

                    if($this->Mission->isPostLinkMission($id_mission))
                    {
                        // Test if the link is sent
                        if(empty($link))
                        {
                            // Retrieve error
                            $result = array(
                                'error' => 'Link is required',
                                'submitted' => false
                            );
                        }
                    }
                    else
                    {
                        $link = false;
                    }

                    if(empty($result) || !array_key_exists('error', $result))
                    {
                        // Prepare data for insert into database
                        $data = array(
                            'id_user'    => $id_user,
                            'id_mission' => $id_mission,
                            'text'       => $text ? $text : false,
                            'link'       => $link ? $link : false,
                            'image'      => false,
                            'status'     => 0
                        );

                        $this->db->set('submitted', 'NOW()', false);

                        $mission = $this->db->query("SELECT COUNT(*) AS submitted FROM missions_users WHERE id_user={$id_user} AND id_mission={$id_mission}")->row();

                        if ($mission->submitted)
                        {
                            $this->db->where(array('id_user' => $id_user, 'id_mission' => $id_mission));
                            $this->db->update('missions_users', $data);
                        }
                        else
                        {
                            $this->db->insert('missions_users', $data);
                        }

                        // If the image is required, send the notification after it is uploaded.
                        if(!$this->Mission->isPostImageMission($id_mission))
                        {
                            $this->AdminNotification->type  = 8;
                            $this->AdminNotification->param = $id_mission.','.$id_user;
                            $this->AdminNotification->insert();

                            $this->Email->missionWaiting($user['email']);
                        }

                        $result = array(
                            'submitted' => true
                        );
                    }
                }
                else
                {
                    $result = array(
                        'error'     => 'Mission does not exist',
                        'submitted' => false
                    );
                }
            }
            else if (!empty($id_mission) && $type == 'image')
            {
                // Verify if the mission exists.
                if(empty($this->Mission->getById($id_mission)))
                {
                    $result = array(
                        'error'    => 'Mission does not exist',
                        'uploaded' => false
                    );
                }
                else
                {
                    // Verify if the mission allows images.
                    if(!$this->Mission->isPostImageMission($id_mission))
                    {
                        $result = array(
                            'error'    => 'Mission does not allow images.',
                            'uploaded' => false
                        );
                    }
                    else
                    {
                        // Verify if the image was sent.
                        if(empty($_FILES))
                        {
                            $result = array(
                                'error'    => 'Image is required.',
                                'uploaded' => false
                            );
                        }

                        if(empty($result) || !array_key_exists('error', $result))
                        {
                            $image = $this->Mission->uploadImage($_FILES, 'file');

                            // Verify if the image was uploaded.
                            if(!empty($image))
                            {
                                $data = array(
                                    'image' => $image
                                );

                                $mission = $this->db->query("SELECT COUNT(*) AS submitted FROM missions_users WHERE id_user={$id_user} AND id_mission={$id_mission}")->row();

                                if ($mission->submitted)
                                {
                                    $this->db->where(array('id_user' => $id_user, 'id_mission' => $id_mission));
                                    $this->db->update('missions_users', $data);
                                }

                                // Send notifications.
                                $this->AdminNotification->type  = 8;
                                $this->AdminNotification->param = $id_mission.','.$id_user;
                                $this->AdminNotification->insert();

                                $this->Email->missionWaiting($user['email']);

                                $result = array(
                                    'uploaded' => true
                                );
                            }
                            else
                            {
                                $result = array(
                                    'error'    => 'Upload failed.',
                                    'uploaded' => false
                                );
                            }
                        }
                    }
                }
            }
            else if(!empty($id_mission) && $type == 'share_completed')
            {

                $network = $this->input->post('network');


                if(empty($this->Mission->getById($id_mission)))
                {
                    $result = array(
                        'error'    => 'Mission does not exist',
                    );
                }
                else
                {
                    if(empty($network))
                    {
                        $result = array(
                            'updated'   => false,
                            'completed' => false
                        );
                    }
                    else if(!in_array($network, array("fb", "tw", "pin")))
                    {
                        $result = array(
                            'updated'   => false,
                            'completed' => false
                        );
                    }
                    else if($this->Mission->isShareMission($id_mission) && $this->Mission->isPostMission($id_mission))
                    {
                        $this->Mission->logSocialShare($id_mission, $id_user, $network);

                        $result = array(
                            'updated'   => true,
                            'completed' => false
                        );
                    }
                    else if ($this->Mission->isShareMission($id_mission))
                    {
                        $this->Mission->logSocialShare($id_mission, $id_user, $network);

                        $result = array(
                            'updated'   => true,
                            'completed' => false
                        );


                        if ($this->Mission->isShareCompleted($id_mission, $id_user))
                        {
                            $this->Mission->approve($id_mission, $id_user);

                            $result = array(
                                'updated'   => true,
                                'completed' => true
                            );
                        }
                    }
                }

            }
            else
            {
                // Get the status
                $status = $this->input->get('status');

                // Variable that will contain the error message
                $error = array();

                // Get the mission for asociated status sent
                switch ($status) {

                  case 'completed':
                    $missions = $this->Mission->getAll($id_user, 'completed');
                    break;

                  case 'waiting':
                    $missions = $this->Mission->getAll($id_user, 'waiting');
                    break;

                  case 'uncompleted':
                    $missions = $this->Mission->getAll($id_user, 'uncompleted');
                    break;

                  case '':
                    $missions = $this->Mission->getAll($id_user);
                    break;

                  default:
                    $error = array(
                        'error' => 'invalid status'
                    );
                    break;
                }

                $countFollowers = $this->Social->countFollowers($id_user);


                $missions = array_map(function ($m) use ($countFollowers)
                {

                    $m->fb_injected  = false;
                    $m->tw_injected  = false;
                    $m->pin_injected = false;
                    $m->text_injected = false;
                    $m->link_injected = false;



                    $m->max_followers = intval($m->max_followers); /* just in case :) */
                    $m->min_followers = intval($m->min_followers);
                    preg_match_all('/{(.*?)}/', $m->description, $matches);
                    //print_r($matches[1]);
                    foreach($matches[1] as $match) {
                        $rules   = array();
                        $evaled  = '';
                        $content = '';
                        if ($match == "facebook_button") {
                            if ($m->fb_share && !$m->fb_shared) {
                                $content = '';
                            }
                            if ($m->fb_share && $m->fb_shared) {
                                $content = '';
                            }
                            $m->fb_injected = true;
                        } elseif ($match == "twitter_button") {
                            if ($m->tw_share && !$m->tw_shared) {
                                $content = '';
                            }
                            if ($m->tw_share && $m->tw_shared) {
                                $content = '';
                            }

                            $m->tw_injected = true;
                        } elseif ($match == "pinterest_button") {
                            if ($m->pin_share && !$m->pin_shared) {
                                $content = '';
                            }
                            if ($m->pin_share && $m->pin_shared) {
                                $content = '';
                            }

                            $m->pin_injected = true;
                        } elseif ($match == "link_box") {
                            if (!$m->submitted || ($m->submitted && $m->status < 0)) {
                                $content       = '';
                                $m->link_injected = true;
                            }
                        } elseif ($match == "text_box") {
                            if (!$m->submitted || ($m->submitted && $m->status < 0)) {
                                $content       = '';
                                $m->text_injected = true;
                            }
                        } else {
                            $pieces = explode('|', $match);
                            if (count($pieces) > 1) {
                                foreach ($pieces as $p) {
                                    if (strpos($p, ':') !== false) {
                                        $rule                  = explode(':', $p);
                                        $rules[trim($rule[0])] = trim($rule[1]);
                                    } else {
                                        $final_value = $p;
                                    }
                                }
                            } else {
                                $final_value = $pieces[0];
                            }
                            $new_match = str_replace('followers', '$countFollowers', $final_value);
                            $evaled    = eval('return ' . $new_match . ';');
                        }

                        if (!empty($evaled) && !empty($rules)) {
                            foreach ($rules as $rule_name => $rule_value) {
                                switch ($rule_name) {
                                    case 'max':
                                        if ($evaled > $rule_value) {
                                            $evaled = $rule_value;
                                        }
                                        break;
                                    case 'min':
                                        if ($evaled < $rule_value) {
                                            $evaled = $rule_value;
                                        }
                                        break;
                                }
                            }
                        }
                        $m->description = str_replace("{".$match."}", !empty($evaled) ? number_format($evaled, 2, ',', '.') : $content , $m->description);

                    }


                    return $m;
                }, $missions);

                // Check if any error exist
                if(empty($error))
                {
                    // Create result variable to send to aplication
                    $result = array(
                        'missions' => $missions
                    );
                }
                else
                {
                    $result = $error;
                }

            }

            // Send response
            echo json_encode($result);
            exit();
        }
    }

    /*
    * Function for get states
    * @return array
    */
    public function states()
    {
        // Create restult for return
        $result = array();

        $result = array(
            'states' => $this->State->getStates()
        );

        echo json_encode($result);
    }

    /*
    * Function for get schools name
    * @return array
    */
    public function schools()
    {
        // Create restult for return
        $result = array();

        // Test If is sent a contry state or city
        if(empty($this->input->get('id_country')) && empty($this->input->get('state'))&& empty($this->input->get('city')))
        {
            $result = array(
                'schools' => $this->School->getAllSchools()
            );
        }
        else if(!empty($this->input->get('id_country')))
        {
            $result = array(
                'schools' => $this->School->getSchoolByCountryForApi($this->input->get('id_country'))
            );
        }
        else if(!empty($this->input->get('city')))
        {
            $result = array(
                'schools' => $this->School->getSchoolByCityForApi($this->input->get('city'))
            );
        }
        else if(!empty($this->input->get('state')))
        {
            $result = array(
                'schools' => $this->School->getAllSchoolsByState($this->input->get('state'))
            );
        }

        echo json_encode($result);
    }

    /*
    * Function for reset password
    * @return true or false
    */
    public function resetPassword()
    {
        if(empty($this->input->post('hash')))
        {
            $this->output->set_status_header('403');
            exit();
        }
        else
        {
            // Create restult for return
            $result = array();

            $hash = $this->input->post('hash');

            $user = $this->db->query("SELECT * FROM auth_user WHERE hash='{$hash}'")->row();

            if (count($user) == 0)
            {
                $this->output->set_status_header('403');
                exit();
            }
            else
            {
                if (!empty($this->input->post('password')))
                {
                    $password = $this->input->post('password');
                    $password = password_hash($password, PASSWORD_DEFAULT);

                    $this->db->set('password', $password);
                    $this->db->where('hash', $hash);
                    $this->db->update('auth_user');

                    // Delete their failed login attempts if they changed their password
                    $this->users->resetLoginAttempts();

                    $result['updated'] = true;
                }
                else
                {
                    $result['updated'] = false;
                    $result['errors']   = 'Password is required';
                }

                // Return the response
                echo json_encode($result);
                exit();
            }
        }
    }

    /*
    * Function for forget password
    * @return array
    */
    public function forgotPassword()
    {
        // Create restult for return
        $result = array();

        // Test If is sent the user email
        if(empty($this->input->post('email')))
        {
            $result = array(
                'sent' => false,
                'error' => 'Invalid email'
            );
        }
        else if(!empty($this->input->post('email')))
        {
            $email = $this->input->post('email');

            $user = $this->Users->getUserDataByEmail($email);

            if (!empty($user))
            {
                $link = base_url('auth/forgot_password/'.$user->hash);
                $this->Email->sendRecoveryLink($email, $link);

                $result = array(
                    'sent' => true,
                );
            }
            else
            {
              $result = array(
                  'sent' => false,
                  'error' => 'Invalid email'
              );
            }

        }

        echo json_encode($result);
    }

    /**
     * Payouts API endpoint
     * @param $action
     *        $action = 'totals' => returns the totals (Your Commission and Giftcard)
     * @return JSON-encoded response.
     */
    public function payouts($action = 'all')
    {
        $data = array();
        $id_user = $this->session->userdata['id'];

        switch($action)
        {
            case 'all':
                // Totals
                $settings = $this->site_config->getGeneralSettings();
                $extra_commission = $settings['giftcard_extra_commission'];
                $data['totals']['commission'] = number_format($this->users->getCommission($id_user), 2, ".", "");
                $data['totals']['giftcard'] = number_format((($extra_commission+100)/100 * $data['totals']['commission']), 2, ".", "");
                $data['totals']['extra_giftcard_percentage'] = $extra_commission;

                // Payments
                $data['payments']   = $this->PaymentRequest->getUserItems($id_user);
                $data['payments'] = array_filter($data['payments'], function($value)
                {
                    return ($value->giftcard == 0 ? true : false);
                });

                $data['payments'] = array_values($data['payments']);

                array_map(function($value)
                {
                    $value->handled = strtotime($value->handled);
                    $value->created = strtotime($value->created);
                    $value->approved = $value->aproved;
                    unset($value->aproved);
                    return $value;
                }, $data['payments']);

                // Giftcards
                $data['giftcards'] = $this->db->query("SELECT * FROM payment_requests AS pr INNER JOIN coupons AS c ON c.id=pr.id_coupon WHERE id_user={$id_user} AND giftcard=1 ORDER BY pr.created DESC")->result();
                array_map(function($value)
                {
                    $value->handled = strtotime($value->handled);
                    $value->created = strtotime($value->created);
                    $value->from = strtotime($value->from);
                    $value->to = strtotime($value->to);
                    return $value;
                }, $data['giftcards']);
                $data['payouts'] = $data;



                break;
            case "totals":
                $settings = $this->site_config->getGeneralSettings();
                $extra_commission = $settings['giftcard_extra_commission'];
                $data['totals']['commission'] = number_format($this->users->getCommission($id_user), 2, ".", "");
                $data['totals']['giftcard'] = number_format((($extra_commission+100)/100 * $data['totals']['commission']), 2, ".", "");
                $data['totals']['extra_giftcard_percentage'] = $extra_commission;
                break;
            case "payments":
                $data['payments']   = $this->PaymentRequest->getUserItems($id_user);
                $data['payments'] = array_filter($data['payments'], function($value)
                {
                    return ($value->giftcard == 0 ? true : false);
                });

                $data['payments'] = array_values($data['payments']);

                array_map(function($value)
                {
                    $value->handled = strtotime($value->handled);
                    $value->created = strtotime($value->created);
                    $value->approved = $value->aproved;
                    unset($value->aproved);
                    return $value;
                }, $data['payments']);
                break;
            case "giftcards":
                $data['giftcards'] = $this->db->query("SELECT * FROM payment_requests AS pr INNER JOIN coupons AS c ON c.id=pr.id_coupon WHERE id_user={$id_user} AND giftcard=1 ORDER BY pr.created DESC")->result();
                array_map(function($value)
                {
                    $value->handled = strtotime($value->handled);
                    $value->created = strtotime($value->created);
                    $value->from = strtotime($value->from);
                    $value->to = strtotime($value->to);
                    return $value;
                }, $data['giftcards']);
                break;
            case "requestPayment":
                $data['errors'] = array();
                $id_user = $this->session->userdata['id'];
                $user    = $this->users->get($id_user);

                $amount = floatval($this->input->post('amount'));
                $time   = date('Y-m-d G:i:s', strtotime("-1 month", time()));

                $settings = $this->site_config->getGeneralSettings();
                $commission = $this->users->getCommission($id_user);

                $requests_last_month = $this->db->select("COUNT(*) AS total")
                                                ->from("payment_requests")
                                                ->where(array(
                                                    "id_user" => $id_user,
                                                    "created >=" => $time
                                                ))->get()->row();
                $limit_per_month = $settings['payout_requests_per_month'];
                if (intval($requests_last_month->total) >= intval($limit_per_month)) {
                    $data['requested'] = false;
                    array_push($data['errors'], 'You have reached the limit of requests for this month.');

                    $this->output->set_status_header('400');
                    break;
                }

                if ($commission < $settings['minimum_commission_payout']) {
                    $data['requested'] = false;
                    array_push($data['errors'], 'You need at least $'.$settings['minimum_commission_payout'].' before you can ask for payout.');
                    $this->output->set_status_header('400');
                    break;
                }

                if ($amount < $settings['minimum_commission_payout']) {
                    $data['requested'] = false;
                    array_push($data['errors'], 'You need to request at least $'.$settings['minimum_commission_payout'].'.');
                    $this->output->set_status_header('400');
                    break;
                }

                if ($amount > $commission) {
                    $data['requested'] = false;
                    array_push($data['errors'], 'Not enough credit.');

                    $this->output->set_status_header('400');
                    break;
                }

                $insert = array(
                    'amount' => $amount,
                    'id_user' => $id_user
                );

                if (isset($_POST['paypal'])) {
                    $insert['paypal'] = 1;
                }

                // automatically create and send giftcard to user email
                if (isset($_POST['payment_method']) && $_POST['payment_method'] == 'giftcard') {
                    $insert['giftcard'] = 1;
                    $insert['aproved'] = 1;

                    $country     = $this->db->query("SELECT * FROM country_t WHERE country_id={$user['id_country']}")->row();
                    $code        = $this->Coupon->randomString(6);
                    $gift_amount = (float)$amount + ((float)$settings['giftcard_extra_commission'] * (float)$amount) / 100;
                    $gift_amount = number_format($gift_amount, 2, ".", "");
                    $insert['gift_amount'] = $gift_amount;

                    $insert['id_coupon'] = $this->Coupon->createGiftcard($id_user, $code, $gift_amount, $country->country_id);
                    $this->Email->giftcardCreated($code, $user);

                    $data['requested'] = true;

                } else {

                    $this->AdminNotification->type  = 4;
                    $this->AdminNotification->param = $this->db->insert_id();
                    $this->AdminNotification->insert();

                    $data['requested'] = true;
                }

                $this->db->set('created', 'NOW()', FALSE);
                $this->db->insert('payment_requests', $insert);
                break;
            default:

                $this->output->set_status_header('400');
                $data['status'] = array(
                    "code" => 400,
                    "message" => "Bad Request"
                );

                break;
        }
        $result = json_encode($data);
        echo $result;
    }
    public function blogs($epoch = 'latest', $limit = 10)
    {
        if($this->session->userdata('id'))
        {
            $result['sent'] = true;
            if($epoch == 'latest')
            {
                $result['blogs'] = $this->BlogEntry->getRecentPosts($limit);
                array_map(function($value) {
                    $value->image = base_url() . "images/blog/" . $value->image;
                }, $result['blogs']);
            }
        }
        else
        {
            $result['sent'] = false;
            $result['errors'] = array(
                "You are not logged-in."
            );
            $this->output->set_status_header('403');
        }

        echo json_encode($result);

    }
    public function terms()
    {
        $data = $this->db->select("body")
                         ->from("content")
                         ->where("id", 13)
                         ->get()
                         ->row();

        $data = array("terms" => $data->body);
        echo json_encode($data);
    }
    public function sales_team($action = 'get')
    {
        $data = array();
        if($action == 'get') {

            $id_user = $this->session->userdata['id'];

            if(empty($id_user))
            {
                $data['received'] = false;
                $data['errors'] = array(
                    "You are not logged-in."
                );
                $this->output->set_status_header('403');
                echo json_encode($data);
                exit;
            }

            $data['received'] = true;

            //$data['title'] = 'Coupons';
            $user = $this->users->get_profile($this->session->userdata('id'));

            $time = date('Y-m-d G:i:s', strtotime("-1 day", time()));
            $settings = $this->site_config->getGeneralSettings();

            $data['underkeepers_content'] = $this->db->query("SELECT * FROM content WHERE id=8")->row()->body;
            $data['underkeepers_content'] = str_replace('{UNDERKEEPER_TOTAL}', $user['slaves'], $data['underkeepers_content']);
            $data['underkeepers_content'] = str_replace('{INVITATIONS_LEFT}', intval($user['slaves'] - $user['slave_invitations_sent']), $data['underkeepers_content']);
            $data['underkeepers'] = intval($user['slaves']);
            $data['invitations_sent'] = intval($user['slave_invitations_sent']);
            $data['invitations_left'] = intval($user['slaves'] - $user['slave_invitations_sent']);
            $data['invitations'] = $this->db->query("SELECT * FROM invitations WHERE id_user={$id_user} ORDER BY created DESC")->result();

            $data['totals'] = $this->db->query("SELECT COUNT(*) AS sales_no, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id_user}")->row();
        }
        else
        {
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            if ($this->form_validation->run() == true)
            {
                $id_user = $this->session->userdata['id'];
                $email = $this->input->post('email');

                if(empty($id_user))
                {
                    $data['invited'] = false;
                    $data['errors'] = array(
                        "You are not logged-in."
                    );
                    $this->output->set_status_header('403');
                    echo json_encode($data);
                    exit;
                }

                $user  = $this->users->get($this->session->userdata['id']);

                if($user['level'] < 1)
                {
                    $data['invited'] = false;
                    $data['errors'] = array(
                        "You cannot send invitations."
                    );
                    $this->output->set_status_header('403');
                    echo json_encode($data);
                    exit;
                }

                if ($user['slaves'] <= $user['slave_invitations_sent']) {
                    $data['invited'] = false;
                    $data['errors'] = array(
                        "You have reached your limit for invitations."
                    );
                    $this->output->set_status_header('403');
                    echo json_encode($data);
                    exit;

                }

                $invitations_sent = $this->db->select("COUNT(*) as sent_invitations")
                    ->from("invitations")
                    ->where("id_user", $id_user)
                    ->get()->row_array();

                $this->db->set('slave_invitations_sent', $invitations_sent['sent_invitations']);
                $this->db->where('id', $id_user);
                $this->db->update('auth_user');

                // obtain hash
                do {
                    $hash = md5(time());
                    $check = $this->db->query("SELECT * FROM invitations WHERE hash='{$hash}'")->result();
                } while ( count($check) > 0);

                $insert = array(
                    'id_user' => $user['id'],
                    'email'   => $email,
                    'hash'    => $hash
                );

                $this->db->set('created', 'NOW()', false);
                $this->db->insert('invitations', $insert);

                $this->Email->sendSlaveInvitation($email, $user, $hash);

                $data['invited'] = true;

            }
            else
            {
                $data['invited'] = FALSE;
                $data['errors'] = $this->form_validation->error_array();
                $this->output->set_status_header('403');
            }

        }

        echo json_encode($data);

    }

    public function setInvitation()
    {
        if($this->input->post('invitation'))
        {
            $this->session->set_userdata('invitation', $this->input->post('invitation'));
            $data['set'] = true;
        }
        else
        {
            $data['set'] = FALSE;
            $data['errors'] = array(
                "Invalid request."
            );
            $this->output->set_status_header('403');
        }

        echo json_encode($data);
    }

    public function fblogin()
    {
        $access_token = $this->input->post('access_token');
        $response = @file_get_contents('https://graph.facebook.com/me/?access_token='.$access_token);

        $response = json_decode($response);
        if(!isset($response->error))
        {
            $user = $this->Users->getByEmail($response->email);
            if(!empty($user))
            {
                $data = array(
                    'id' => $user->id,
                    'email' => $user->email,
                    'level' => $user->level
                );

                $this->session->set_userdata($data);

                $data['logged'] = true;
            }
            else
            {
                $data['logged'] = false;
                $data['errors'] = array(
                    "No user could be associated with your Facebook account."
                );
                $this->output->set_status_header('403');
            }

        }
        else
        {
            $data['logged'] = false;
            $data['errors'] = array(
                "There was an error while trying to get your Facebook data."
            );
            $this->output->set_status_header('403');
        }

        echo json_encode($data);
    }

    public function linkNetwork()
    {
        $network_name = $this->input->post('network');
        $followers    = $this->input->post('followers');
        $profile      = $this->input->post('profile');

        if ($this->session->userdata('id'))
        {
            $id_user = $this->session->userdata('id');
        }
        else
        {
            $id_user = $this->session->userdata('registered_id');
        }

        if(empty($id_user))
        {
            $data['linked'] = false;
            $data['error_code'] = 1;
            $data['errors'] = array(
                'You are not logged-in.'
            );
            $this->output->set_status_header('403');
            echo json_encode($data);
            exit;
        }

        $this->Social->updateNetwork($network_name, $id_user, $followers, $profile);

        if ($this->session->flashdata('social_not_unique')) {
            $data['linked'] = false;
            $data['error_code'] = 2;
            $data['errors'] = array(
                'This social account has already been linked.'
            );
            $this->output->set_status_header('403');
            echo json_encode($data);
            exit;
        }

        $data['linked'] = true;
        echo json_encode($data);

    }

    public function temp()
    {
        $result = $this->db->query("SELECT * FROM auth_user")->result();

        echo json_encode($result);
    }


}
