<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Missions extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->library('form_validation');

    $this->load->helper('form');
    $this->load->helper('url');

    $this->load->model('users', 'User');
    $this->load->model('email_model', 'Email');
    $this->load->model('mission_model', 'Mission');
    $this->load->model('admin_notification', 'AdminNotification');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }
    
    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }

  }

  public function index($id_notification = false) {
    $data['admin'] = $this->users->get($this->session->userdata('id'));
    $id_user = $this->session->userdata('id');
    $user = $data['admin'];

    // limit results by admin countries
    if ($user['level'] == 2) {
      $countries = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}")->result();
    } elseif ($user['level'] == 3) {
      $countries = $this->db->query("SELECT country_id AS id_country FROM country_t")->result();
    }

    $ids_countries = array();
    foreach ($countries as $c) {
        $ids_countries[] = $c->id_country;
    }
    $ids_countries = implode(', ', $ids_countries);

    $missions = $this->db->query("SELECT * FROM missions AS m LEFT JOIN country_t AS c ON m.country=c.country_id WHERE (m.country IN ({$ids_countries}) OR m.country=0) AND deleted=0 ORDER BY created DESC")->result();


    $data['title']  = "Missions";
    $data['missions'] = $missions;

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/missions-list', $data);
    $this->load->view('templates/footer');

  }


  public function add() {

      $data['admin'] = $this->users->get($this->session->userdata('id'));
      $id_user = $this->session->userdata('id');
      $user = $data['admin'];

      if ($user['level'] == 2) {
          $countries = $this->db->query("SELECT c.* FROM admins_countries AS ac INNER JOIN country_t AS c ON c.country_id=ac.id_country WHERE ac.id_user={$id_user}")->result();
          $cities    = $this->db->query("SELECT u.city AS name FROM admins_countries AS ac INNER JOIN auth_user AS u ON u.id_country=ac.id_country WHERE ac.id_user={$id_user} GROUP BY u.city")->result();
      } elseif($user['level'] == 3) {
          $countries = $this->db->query("SELECT c.* FROM auth_user AS u INNER JOIN country_t AS c ON c.country_id=u.id_country GROUP BY c.country_id")->result();
          $cities    = $this->db->query("SELECT city AS name FROM auth_user GROUP BY city")->result();
      }

      if (isset($_POST['trigger']) && $_POST['trigger'] == 'add') {

          $this->form_validation->set_rules("name", "Mission Name", 'required');
          $this->form_validation->set_rules("from", "Mission Start Date", 'required');
          if($this->input->post('countdown')){
              $this->form_validation->set_rules("to", "Mission End Date", 'required');
          }
          $this->form_validation->set_rules("points", "Number of Points", 'required');
          if($this->input->post('sales_goals')){
              $this->form_validation->set_rules("sales", "Number of Sales", 'required');
              if (!$this->input->post('days_number')) {
                  $this->form_validation->set_rules("sales_from", "Sales Start Date", 'required');
                  $this->form_validation->set_rules("sales_to", "Sales End Date", 'required');
              }
          }

          if($this->form_validation->run()){

              if ($user['level'] == 2 && !$this->input->post('country')) {
                  redirect('admin/missions/add');
              }

              $image = $this->Mission->uploadImage($_FILES, 'image');

              if (!$this->input->post('from')) {
                  $from = date('Y-m-d', time());
              } else {
                  $from = date('Y-m-d', strtotime($this->input->post('from')));
              }

              if ($this->input->post('to')) {
                  $to = date('Y-m-d', strtotime($this->input->post('to')));
              }


              if ($this->input->post('days_number')) {
                  $days = intval($this->input->post('days_number'));
                  $sales_from = date('Y-m-d', time());
                  $sales_to = date('Y-m-d', strtotime('+'.$days.' days'));
              } else {
                  $sales_from = date('Y-m-d', strtotime($this->input->post('sales_from')));
                  $sales_to = date('Y-m-d', strtotime($this->input->post('sales_to')));
              }

              $levels = array();
              if ($this->input->post("pk_level_1")) {
                  array_push($levels, "-2");
              }
              if ($this->input->post("pk_level_2")) {
                  array_push($levels, "-1");
              }
              if ($this->input->post("pk_level_3")) {
                  array_push($levels, "0");
              }
              if ($this->input->post("pk_level_m")) {
                  array_push($levels, "1");
              }

              $levels = implode(",", $levels);

              $data = array(
                  'name' => $this->input->post('name'),
                  'description' => $this->input->post('description'),
                  'pk_levels' => $levels,
                  'from' => $from,
                  'to' => $to,
                  'country' => $this->input->post('country'),
                  'city' => $this->input->post('city'),
                  'points' => $this->input->post('points'),
                  'link_details' => $this->input->post('link_details'),
                  'text_details' => $this->input->post('text_details'),
                  'image' => $image,
                  'video' => $this->input->post('video'),
                  'sales' => $this->input->post('sales'),
                  'sales_from' => $sales_from,
                  'sales_to' => $sales_to,
                  'fb_link' => $this->input->post('fb_link'),
                  'fb_text' => $this->input->post('fb_text'),
                  'tw_link' => $this->input->post('tw_link'),
                  'tw_text' => $this->input->post('tw_text'),
                  'pin_link' => $this->input->post('pin_link'),
                  'pin_image' => $this->input->post('pin_image'),
                  'pin_text' => $this->input->post('pin_text')
                );

              if ($this->input->post('allow_text')) {
                  $data['allow_text'] = 1;
              }

              if ($this->input->post('allow_link')) {
                  $data['allow_link'] = 1;
              }

              if ($this->input->post('allow_image')) {
                  $data['allow_image'] = 1;
              }

              if ($this->input->post('sales_goals')) {
                  $data['sales_goals'] = 1;
              }

              if ($this->input->post('fb_share')) {
                  $data['fb_share'] = 1;
              }

              if ($this->input->post('tw_share')) {
                  $data['tw_share'] = 1;
              }

              if ($this->input->post('pin_share')) {
                  $data['pin_share'] = 1;
              }

              if ($this->input->post('countdown')) {
                  $data['countdown'] = 1;
              }

              if(!empty($this->input->post('min_followers')))
              {
                  $data['min_followers'] = $this->input->post('min_followers');
              }

              if(!empty($this->input->post('max_followers')))
              {
                  $data['max_followers'] = $this->input->post('max_followers');
              }

              $this->db->set('created', 'NOW()', false);
              $this->db->insert('missions', $data);

              redirect('/admin/missions/');

          }

      }

      $data['title'] = "Add Mission";
      $data['countries'] = $countries;
      $data['cities'] = $cities;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/mission-add', $data);
      $this->load->view('templates/footer');
  }


  public function edit($id) {
      $data['admin'] = $this->users->get($this->session->userdata('id'));
      $id_user = $this->session->userdata('id');
      $user = $data['admin'];

      if ($user['level'] == 2) {
          $countries = $this->db->query("SELECT c.* FROM admins_countries AS ac INNER JOIN country_t AS c ON c.country_id=ac.id_country WHERE ac.id_user={$id_user}")->result();
          $cities    = $this->db->query("SELECT u.city AS name FROM admins_countries AS ac INNER JOIN auth_user AS u ON u.id_country=ac.id_country WHERE ac.id_user={$id_user} GROUP BY u.city")->result();
      } elseif($user['level'] == 3) {
          $countries = $this->db->query("SELECT c.* FROM auth_user AS u INNER JOIN country_t AS c ON c.country_id=u.id_country GROUP BY c.country_id")->result();
          $cities    = $this->db->query("SELECT city AS name FROM auth_user GROUP BY city")->result();
      }

      $mission = $this->db->query("SELECT * FROM missions WHERE id={$id}")->row();

      $levels = explode(",", $mission->pk_levels);

      if (in_array("-2", $levels)) {
          $mission->pk_level_1 = true;
      } else {
          $mission->pk_level_1 = false;
      }

      if (in_array("-1", $levels)) {
          $mission->pk_level_2 = true;
      } else {
          $mission->pk_level_2 = false;
      }

      if (in_array("0", $levels)) {
          $mission->pk_level_3 = true;
      } else {
          $mission->pk_level_3 = false;
      }

      if (in_array("1", $levels)) {
          $mission->pk_level_m = true;
      } else {
          $mission->pk_level_m = false;
      }

      if ($user['level'] == 2 && !$mission->country) {
          redirect('admin/missions');
      }

      if (isset($_POST['trigger']) && $_POST['trigger'] == 'edit') {

          $this->form_validation->set_rules("name", "Mission Name", 'required');
          $this->form_validation->set_rules("from", "Mission Start Date", 'required');
          if($this->input->post('countdown')){
              $this->form_validation->set_rules("to", "Mission End Date", 'required');
          }
          $this->form_validation->set_rules("points", "Number of Points", 'required');
          if($this->input->post('sales_goals')){
              $this->form_validation->set_rules("sales", "Number of Sales", 'required');
              if (!$this->input->post('days_number')) {
                  $this->form_validation->set_rules("sales_from", "Sales Start Date", 'required');
                  $this->form_validation->set_rules("sales_to", "Sales End Date", 'required');
              }
          }

          if($this->form_validation->run()){
              $image = $this->Mission->uploadImage($_FILES, 'image');

              if (!$this->input->post('from')) {
                  $from = date('Y-m-d', time());
              } else {
                  $from = date('Y-m-d', strtotime($this->input->post('from')));
              }

              if ($this->input->post('to')) {
                  $to = date('Y-m-d', strtotime($this->input->post('to')));
              }


              if (!$this->input->post('sales_from')) {
                  $sales_from = date('Y-m-d', time());
              } else {
                  $sales_from = date('Y-m-d', strtotime($this->input->post('sales_from')));
              }

              if (!$this->input->post('sales_to')) {
                  $sales_to = date('Y-m-d', time());
              } else {
                  $sales_to = date('Y-m-d', strtotime($this->input->post('sales_to')));
              }


              $levels = array();
              if ($this->input->post("pk_level_1")) {
                  array_push($levels, "-2");
              }
              if ($this->input->post("pk_level_2")) {
                  array_push($levels, "-1");
              }
              if ($this->input->post("pk_level_3")) {
                  array_push($levels, "0");
              }
              if ($this->input->post("pk_level_m")) {
                  array_push($levels, "1");
              }

              $levels = implode(",", $levels);


              $data = array(
                  'name' => $this->input->post('name'),
                  'description' => $this->input->post('description'),
                  'pk_levels' => $levels,
                  'from' => $from,
                  'to' => $to,
                  'country' => $this->input->post('country'),
                  'city' => $this->input->post('city'),
                  'points' => $this->input->post('points'),
                  'link_details' => $this->input->post('link_details'),
                  'text_details' => $this->input->post('text_details'),
                  'video' => $this->input->post('video'),
                  'sales' => $this->input->post('sales'),
                  'sales_from' => $sales_from,
                  'sales_to' => $sales_to,
                  'fb_link' => $this->input->post('fb_link'),
                  'fb_text' => $this->input->post('fb_text'),
                  'tw_link' => $this->input->post('tw_link'),
                  'tw_text' => $this->input->post('tw_text'),
                  'pin_link' => $this->input->post('pin_link'),
                  'pin_image' => $this->input->post('pin_image'),
                  'pin_text' => $this->input->post('pin_text')
                );

              if ($image) {
                  $data['image'] = $image;
              }

              if ($this->input->post('allow_link')) {
                  $data['allow_link'] = 1;
              } else {
                  $data['allow_link'] = 0;
              }

              if ($this->input->post('allow_text')) {
                  $data['allow_text'] = 1;
              } else {
                  $data['allow_text'] = 0;
              }

              if ($this->input->post('allow_image')) {
                  $data['allow_image'] = 1;
              } else {
                  $data['allow_image'] = 0;
              }

              if ($this->input->post('sales_goals')) {
                  $data['sales_goals'] = 1;
              } else {
                  $data['sales_goals'] = 0;
              }

              if ($this->input->post('fb_share')) {
                  $data['fb_share'] = 1;
              } else {
                  $data['fb_share'] = 0;
              }

              if ($this->input->post('tw_share')) {
                  $data['tw_share'] = 1;
              } else {
                  $data['tw_share'] = 0;
              }

              if ($this->input->post('pin_share')) {
                  $data['pin_share'] = 1;
              } else {
                  $data['pin_share'] = 0;
              }

              if ($this->input->post('countdown')) {
                  $data['countdown'] = 1;
              } else {
                  $data['countdown'] = 0;
              }

              if(!empty($this->input->post('min_followers')))
              {
                  $data['min_followers'] = $this->input->post('min_followers');
              }

              if(!empty($this->input->post('max_followers')))
              {
                  $data['max_followers'] = $this->input->post('max_followers');
              }

              if($this->input->post('remove_image')){
                $data['image'] = "";
                unlink('./images/missions/'.$mission->image);
              }

              $this->db->where('id', $id);
              $this->db->update('missions', $data);

              redirect('/admin/missions/');
          }
      }


      $data['title'] = "Edit Mission";
      $data['countries'] = $countries;
      $data['cities'] = $cities;
      $data['mission'] = $mission;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/mission-edit', $data);
      $this->load->view('templates/footer');
  }


  public function view($id_mission, $id_user = false, $id_notification = false) {
      $data['admin'] = $this->users->get($this->session->userdata('id'));

      if ($id_notification) {
          $this->AdminNotification->setInactive($id_notification);
      }

      if ($id_user) {
          $participant = $this->users->get($id_user);
          $mission = $this->Mission->getById($id_mission);
          $response = $this->Mission->getResponse($id_mission, $id_user);

          $data['title'] = $mission->name;
          $data['mission'] = $mission;
          $data['participant'] = $participant;
          $data['response'] = $response;

          $this->load->view('templates/header', $data);
          $this->load->view('pages/admin/mission-response', $data);
          $this->load->view('templates/footer');

      } else {

          $export = false;

          if(isset($_POST['trigger']) && $_POST['trigger'] == 'export-xls') {
              $export = true;
          }

          $mission = $this->Mission->getById($id_mission);
          $participants = $this->Mission->getParticipants($id_mission);

          // get users ids
          $ids_users = array();
          foreach ($participants as $p) {
              $ids_users['user_'.$p->id] = $p->id;
          }

          if($export){
              // get rankings
              $global_ranks  = $this->users->getGroupGlobalRanking($ids_users);
              $country_ranks = $this->users->getGroupCountryRanking($ids_users);
              $city_ranks    = $this->users->getGroupCityRanking($ids_users);
              foreach ($participants as $key => $p) {
                  //$participants[$key]->followers    = $this->users->getAllFollowers($p->id);
                  //$participants[$key]->total_points = $this->users->getPoints($p->id, 'total');
                  //$participants[$key]->commission   = $this->users->getTotalCommission($p->id);
                  $participants[$key]->global_rank  = $global_ranks['user_'.$p->id]->place;
                  $participants[$key]->country_rank = $country_ranks['user_'.$p->id]->place;
                  $participants[$key]->city_rank    = $city_ranks['user_'.$p->id]->place;
              }
          }

          if ($export) {
            $this->load->library('excel');
            $format = $this->input->post('format');

            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('PieceKeepers');

            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('X')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('Z')->setWidth(20);

            $this->excel->getActiveSheet()->setCellValue('A1', 'First Name');
            $this->excel->getActiveSheet()->setCellValue('B1', 'Last Name');
            $this->excel->getActiveSheet()->setCellValue('C1', 'Country');
            $this->excel->getActiveSheet()->setCellValue('D1', 'City');
            $this->excel->getActiveSheet()->setCellValue('E1', 'Submitted');
            $this->excel->getActiveSheet()->setCellValue('F1', 'Status');
            $this->excel->getActiveSheet()->setCellValue('G1', 'Email');
            $this->excel->getActiveSheet()->setCellValue('H1', 'Phone');
            $this->excel->getActiveSheet()->setCellValue('I1', 'DoB');
            $this->excel->getActiveSheet()->setCellValue('J1', 'Gender');
            $this->excel->getActiveSheet()->setCellValue('K1', 'Level');
            $this->excel->getActiveSheet()->setCellValue('L1', 'Postcode');
            $this->excel->getActiveSheet()->setCellValue('M1', 'School');
            $this->excel->getActiveSheet()->setCellValue('N1', 'Graduation Year');
            $this->excel->getActiveSheet()->setCellValue('O1', 'Address1');
            $this->excel->getActiveSheet()->setCellValue('P1', 'Address2');
            //$this->excel->getActiveSheet()->setCellValue('Q1', 'Social Followers');
            //$this->excel->getActiveSheet()->setCellValue('R1', 'Total Points');
            //$this->excel->getActiveSheet()->setCellValue('S1', 'Earned Commission');
            $this->excel->getActiveSheet()->setCellValue('Q1', 'Global Rank');
            $this->excel->getActiveSheet()->setCellValue('R1', 'Country Rank');
            $this->excel->getActiveSheet()->setCellValue('S1', 'City Rank');
            $this->excel->getActiveSheet()->setCellValue('T1', 'Inactive for days');
            $this->excel->getActiveSheet()->setCellValue('U1', 'State');
            $this->excel->getActiveSheet()->setCellValue('V1', 'Date of Registration');

            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('N1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('O1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('P1')->getFont()->setBold(true);
            //$this->excel->getActiveSheet()->getStyle('Q1')->getFont()->setBold(true);
            //$this->excel->getActiveSheet()->getStyle('R1')->getFont()->setBold(true);
            //$this->excel->getActiveSheet()->getStyle('S1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('Q1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('R1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('S1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('T1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('U1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('V1')->getFont()->setBold(true);

            $i = 2;
            $currentTime = new DateTime(date('Y-m-d'));

            foreach ($participants as $p) {

              if ($p->status == -1) {
                  $participant_status = "Rejected";
              } elseif ($p->status == 0) {
                  $participant_status = "Waiting";
              } else {
                  $participant_status = "Completed";
              }

              $this->excel->getActiveSheet()->setCellValue('A'.$i, $p->first_name);
              $this->excel->getActiveSheet()->setCellValue('B'.$i, $p->last_name );
              $this->excel->getActiveSheet()->setCellValue('C'.$i, $p->country );
              $this->excel->getActiveSheet()->setCellValue('D'.$i, $p->city );
              $this->excel->getActiveSheet()->setCellValue('E'.$i, $p->submitted );
              $this->excel->getActiveSheet()->setCellValue('F'.$i, $participant_status );
              $this->excel->getActiveSheet()->setCellValue('G'.$i, $p->email );
              $this->excel->getActiveSheet()->setCellValue('H'.$i, $p->phone );
              $this->excel->getActiveSheet()->setCellValue('I'.$i, $p->dob );

              if ($p->gender == 0) {
                  $this->excel->getActiveSheet()->setCellValue('J'.$i, 'Female');
              } elseif ($p->gender == 1) {
                  $this->excel->getActiveSheet()->setCellValue('J'.$i, 'Male');
              }


              if ($p->level == -2) {
                  $this->excel->getActiveSheet()->setCellValue('K'.$i, 'PieceKeeper Level 1');
              } elseif ($p->level == -1) {
                  $this->excel->getActiveSheet()->setCellValue('K'.$i, 'PieceKeeper Level 2');
              } elseif ($p->level == 0) {
                  $this->excel->getActiveSheet()->setCellValue('K'.$i, 'PieceKeeper Level 3');
              } elseif ($p->level == 1) {
                  $this->excel->getActiveSheet()->setCellValue('K'.$i, 'Master PieceKeeper');
              }

              $this->excel->getActiveSheet()->setCellValue('L'.$i, $p->postcode);
              $this->excel->getActiveSheet()->setCellValue('M'.$i, $p->school);
              $this->excel->getActiveSheet()->setCellValue('N'.$i, $p->graduation_year);
              $this->excel->getActiveSheet()->setCellValue('O'.$i, $p->address1);
              $this->excel->getActiveSheet()->setCellValue('P'.$i, $p->address2);
              //$this->excel->getActiveSheet()->setCellValue('Q'.$i, $p->followers);
              //$this->excel->getActiveSheet()->setCellValue('R'.$i, $p->total_points);
              //$this->excel->getActiveSheet()->setCellValue('S'.$i, $p->commission);
              $this->excel->getActiveSheet()->setCellValue('Q'.$i, $p->global_rank);
              $this->excel->getActiveSheet()->setCellValue('R'.$i, $p->country_rank);
              $this->excel->getActiveSheet()->setCellValue('S'.$i, $p->city_rank);

              $last_login = new DateTime(substr($p->last_login,0,10));
              $diff = $currentTime->diff($last_login);

              if($diff->days > 700000){
                $loggedInDays = "NL";
              }else{
                $loggedInDays = $diff->days;
              }

              $this->excel->getActiveSheet()->setCellValue('T'.$i, $loggedInDays);
              $this->excel->getActiveSheet()->setCellValue('U'.$i, $p->state);
              $this->excel->getActiveSheet()->setCellValue('V'.$i, $p->date_joined);

              $i++;

            }


            switch ($format) {
              default:
              case 'xls':
                $filename = "$mission->name Piecekeepers.xls";
                header('Content-Type: application/vnd.ms-excel'); //mime type
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

                break;

              case 'xlsx':
                $filename = "$mission->name Piecekeepers.xlsx";
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //mime type
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

                break;
            }

            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache

            $objWriter->save('php://output');
            exit();
          }


          $data['title'] = $mission->name;
          $data['mission'] = $mission;
          $data['participants'] = $participants;

          $this->load->view('templates/header', $data);
          $this->load->view('pages/admin/mission-participants', $data);
          $this->load->view('templates/footer');
      }
  }


  public function approve($id_mission, $id_user) {
        $id_submission = $this->Mission->approve($id_mission, $id_user);

        $this->AdminNotification->logHandling($this->session->userdata('id'), $id_submission, 'missions_users');
        redirect('admin/missions/view/'.$id_mission);
  }


  public function reject() {
    if (isset($_POST['trigger']) && $_POST['trigger'] == 'reject') {
        $id_user    = $this->input->post('user');
        $id_mission = $this->input->post('mission');
        $details    = $this->input->post('details');

        $id_submission = $this->Mission->reject($id_mission, $id_user, $details);

        $this->AdminNotification->logHandling($this->session->userdata('id'), $id_submission, 'missions_users');
        redirect('admin/missions/view/'.$id_mission);
    }
  }


  public function delete($id) {
      $user = $this->users->get($this->session->userdata('id'));
      $mission = $this->db->query("SELECT * FROM missions WHERE id={$id}")->row();

      if ($user['level'] == 2 && !$mission->country) {
          redirect('admin/missions');
      }

      $this->db->set('deleted', 1);
      $this->db->where('id', $id);
      $this->db->update('missions');

      redirect('admin/missions/');
  }

}
