<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('admin_notification', 'AdminNotification');
    $this->load->model('notification', 'Notification');
    $this->load->model('coupon_model', 'Coupon');
    $this->load->model('points_model', 'Points');
    $this->load->model('Orders', 'Orders');
    $this->load->model('social_model', 'Social');
    $this->load->model('user_tag_model', 'UserTag');
    $this->load->model('commission_model', 'Commission');
    $this->load->model('scratch_coupon_model', 'ScratchCoupon');
    $this->load->model('admin_comment_model', 'AdminComment');
    $this->load->model('site_config');
    //$this->load->model('email_model', 'Email');
    $this->load->model('users', 'User');
    $this->load->model('questions_model', 'questions');
    $this->load->model('package_model', 'Package');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }

    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }
  }

  public function index() {
      $data['admin'] = $this->users->get($this->session->userdata('id'));
      $data['all_user_tags'] = $this->UserTag->getAll();

      $data['filter_countries'] = $this->db->query("SELECT * FROM auth_user AS u INNER jOIN country_t AS c ON c.country_id=u.id_country GROUP BY c.country_id")->result();
      $data['filter_states']    = $this->db->query("SELECT DISTINCT state AS name FROM auth_user ORDER BY state")->result();
      $data['filter_cities']    = $this->db->query("SELECT city AS name FROM auth_user GROUP BY city")->result();
      $data['filter_schools']   = $this->db->query("SELECT school AS name FROM auth_user GROUP BY school")->result();

      // check if is export
      $export = false;
      if(isset($_POST['trigger']) && $_POST['trigger'] == 'export-xls'){
          $export = true;
      }

      $only = ($this->input->get("only") == NULL) ? FALSE : $this->input->get("only");
      $search = ($this->input->get("name") == NULL) ? FALSE : $this->input->get("name");
      $level = ($this->input->get('level') == NULL) ? FALSE : $this->input->get('level');
      $type = ($this->input->get("type") == NULL) ? FALSE : $this->input->get("type");
      $gender = ($this->input->get("gender") == NULL) ? FALSE : $this->input->get("gender");
      $country = ($this->input->get("country") == NULL) ? FALSE : $this->input->get("country");
      $city = ($this->input->get("city") == NULL) ? FALSE : $this->input->get("city");
      $state = ($this->input->get("state") == NULL) ? FALSE : $this->input->get("state");
      $school = ($this->input->get("school") == NULL) ? FALSE : $this->input->get("school");
      $signup = ($this->input->get("signup") == NULL) ? FALSE : $this->input->get("signup");
      $low_age = ($this->input->get("low-age") == NULL) ? FALSE : $this->input->get("low-age");
      $high_age = ($this->input->get("high-age") == NULL) ? FALSE : $this->input->get("high-age");
      $days_inactive = ($this->input->get("days-inactive") == NULL) ? FALSE : $this->input->get("days-inactive");
      $limit_rows = ($this->input->get('limit-rows') == NULL) ? FALSE : $this->input->get('limit-rows');

      $order_by = ($this->input->get("table-order-by") == NULL) ? FALSE : $this->input->get("table-order-by");
      $order_how = ($this->input->get("table-order-how") == NULL) ? FALSE : $this->input->get("table-order-how");

      if ($type == 2) {
          $level = 1;
      }


      if( $order_by == "name" ) {
        $orderby = "order by first_name,last_name ".$order_how;
      } else if( $order_by == "email" ) {
        $orderby = "order by email ".$order_how;
      } else if( $order_by == "inact" ) {
        $orderby = "order by last_login ".$order_how;
      } else if( $order_by == "reg" ) {
        $orderby = "order by date_joined ".$order_how;
      } else if( $order_by == "points" ) {
        $orderby = "order by total_points ".$order_how;
      } else if( $order_by == "followers" ) {
        $orderby = "order by total_followers ".$order_how;
      } else if( $order_by == "level" ) {
        $orderby = "order by level ".$order_how;
      } else if( $order_by == "ipad" ) {
        $orderby = "order by ipad ".$order_how;
      } else {
        $orderby = "order by date_joined DESC";
      }

      $where = array();
      $wheresql = "";
      $limit = "";

      if(intval($days_inactive) >= 0 && $days_inactive !== false && $days_inactive!==''){
      	$dateObject = new DateTime( 'now' );
      	$days = 'P'.$days_inactive.'D';
      	$last_login_date =  $dateObject->sub( new DateInterval($days) )->format('Y-m-d');
      	$where[] = "DATE(u.last_login)='{$last_login_date}'";
      }

      if (intval($low_age) > 0) {
          $time = strtotime("-".$low_age." years");
          $date = date("Y-m-d", $time);
          $where[] = "u.dob<='{$date}'";
      }

      if (intval($high_age) > 0) {
          $time = strtotime("-".$high_age." years");
          $date = date("Y-m-d", $time);
          $where[] = "u.dob>='{$date}'";
      }

      if(intval($limit_rows) > 0) {
          $limit = " LIMIT {$limit_rows}";
      }

      // read user tags
      $user_tags = '';
      if($this->input->get('user-tags')){
        $tags = $this->input->get('user-tags');
        foreach ($tags as $tag) {
          $user_tags[] = "'".$tag."'";
        }
        $user_tags = implode(',', $user_tags);
        $user_tags = str_replace(",''", "", $user_tags);
        $user_tags = str_replace("'',", "", $user_tags);
        $user_tags = str_replace("''", "", $user_tags);
        $user_tags = str_replace(" ", "", $user_tags);
      }

      // for disapproved we are showing those uncompleted, too
      if($only===false || $only==="") {
          $where[] = "u.completed=1 ";
      } elseif($only==0) {
          $where[] = "u.completed=1 AND u.is_active=0 AND u.deleted=0 ";
      } elseif($only==1) {
          $where[] = "u.completed=1 AND u.is_active=1 ";
      } elseif($only==2) {
          $where[] = "u.is_active=-1 ";
      } elseif($only==3) {
          $where[] = "u.completed=1 AND u.deleted=1 ";
      }

      if ($level === false || $level === "") {
      } elseif($level == -2 || $level == -1 || $level == 0) {
          $where[] = "u.level={$level} ";
      } elseif($level == 1){
          $where[] = "u.level>={$level} ";
      }

      if ($signup === false || $signup === "") {
      } elseif($signup == "ipad") {
          $where[] = "u.ipad=1 ";
      } elseif($signup == "pku") {
          $where[] = "u.pku=1 ";
      } elseif($signup == "pc") {
          $where[] = "u.ipad=0 AND u.pku=0 ";
      }

      if ($type === false || $type === "") {
      } elseif($type == 1) {
          $where[] = "u.id_master!=0 ";
      }

      if ($gender === false || $gender === "") {
      } elseif($gender == "1" || $gender == "0") {
          $where[] = "u.gender={$gender} ";
      }

      if ($country === false || $country === "") {
      } else {
          $where[] = "u.id_country={$country} ";
      }

      if ($city === false || $city === "") {
      } else {
          $where[] = "u.city='{$city}' ";
      }

      if ($state === false || $state === "") {
      } else {
          $where[] = "u.state='{$state}' ";
      }

      if ($school === false || $school === "") {
      } else {
          $where[] = "u.school='{$school}' ";
      }

      if($search) {
          $where[] = "(u.first_name like '%{$search}%' or u.last_name like '%{$search}%')";
      }

      $inner_join_user_tags = '';
      $user_tags_rows = '';
      $groupby = '';
      if(strlen($user_tags)){
        $where[] = "ut.tag IN (".$user_tags.")";
        $inner_join_user_tags = "INNER JOIN user_tags AS ut ON ut.id_user=u.id";
        $user_tags_rows = ', ut.tag';
        $groupby = 'GROUP BY ut.id_user';
      }

      if(!empty($where)) {
          $wheresql = " WHERE ". implode(" AND " , $where);
      }


      $data['only'] = $only;
      $data['name'] = $search;
      $data['user_level'] = $level;
      $data['type'] = $type;
      $data['filter_gender'] = $gender;
      $data['filter_signup'] = $signup;
      $data['filter_country'] = $country;
      $data['filter_city'] = $city;
      $data['filter_state'] = $state;
      $data['filter_school'] = $school;
      $data['low_age'] = $low_age;
      $data['high_age'] = $high_age;
      $data['limit_rows'] = $limit_rows;
	    $data['days_inactive'] = $days_inactive;
	    // if is Admin get user by Assigned Countries
      $id_user = $this->session->userdata('id');
      $user    = $this->User->get($id_user);
      $query     = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}");
      $countries = $query->result();
      $ids_countries = array();
      foreach ($countries as $c) {
          $ids_countries[] = $c->id_country;
      }
      $ids_countries = implode(', ', $ids_countries);

      if ($order_by == "points" ) {

        if ($user['level'] == 2) {
            if (empty($where)) {
                $query_string = "SELECT u.*$user_tags_rows, concat(u.first_name,u.last_name) as username  , ".

                " (SELECT SUM(value) AS total FROM points WHERE ( type=1 or type=2) AND points.id_user=u.id )  as total_points  ".

                " FROM auth_user as u $inner_join_user_tags ".
                " WHERE u.id_country IN ($ids_countries) $groupby $orderby";
            } else {
                $query_string = "SELECT u.*$user_tags_rows, concat(u.first_name,u.last_name)  as username, ".

                " (SELECT SUM(value) AS total FROM points WHERE ( type=1 or type=2) AND points.id_user=u.id )  as total_points  ".

                " FROM auth_user as u $inner_join_user_tags $wheresql AND u.id_country IN ($ids_countries) $groupby $orderby";
            }
        } else {
            $query_string = "SELECT u.*$user_tags_rows, concat(u.first_name,u.last_name) as username, ".

                " (SELECT SUM(value) AS total FROM points WHERE ( type=1 or type=2) AND points.id_user=u.id )  as total_points  ".

                " FROM auth_user as u $inner_join_user_tags $wheresql $groupby $orderby";
        }
      } else if( $order_by == "followers" ){

        if ($user['level'] == 2) {
            if (empty($where)) {
                $query_string = "SELECT u.*$user_tags_rows, concat(u.first_name,u.last_name) as username  , ".

                " (SELECT SUM(value) AS total FROM social_followers WHERE social_followers.id_user=u.id )  as total_followers  ".

                " FROM auth_user as u $inner_join_user_tags ".
                " WHERE u.id_country IN ($ids_countries) $groupby $orderby";
            } else {
                $query_string = "SELECT u.*$user_tags_rows, concat(u.first_name,u.last_name)  as username, ".

                " (SELECT SUM(value) AS total FROM social_followers WHERE social_followers.id_user=u.id )  as total_followers  ".

                " FROM auth_user as u $inner_join_user_tags $wheresql AND u.id_country IN ($ids_countries) $groupby $orderby";
            }
        } else {
            $query_string = "SELECT u.*$user_tags_rows, concat(u.first_name,u.last_name) as username, ".

                " (SELECT SUM(value) AS total FROM social_followers WHERE social_followers.id_user=u.id )  as total_followers  ".

                " FROM auth_user as u $inner_join_user_tags $wheresql $groupby $orderby";
        }
      } else {
          if ($user['level'] == 2) {
            if (empty($where)) {
                $query_string = "SELECT u.*$user_tags_rows, concat(u.first_name,u.last_name) as username  FROM auth_user as u $inner_join_user_tags WHERE u.id_country IN ($ids_countries) $groupby $orderby";
            } else {
                $query_string = "SELECT u.*$user_tags_rows, concat(u.first_name,u.last_name)  as username  FROM auth_user as u $inner_join_user_tags $wheresql AND u.id_country IN ($ids_countries) $groupby $orderby";
            }
        } else {
            $query_string = "SELECT u.*$user_tags_rows, concat(u.first_name,u.last_name) as username FROM auth_user as u $inner_join_user_tags $wheresql $groupby $orderby";
        }
      }

      // Add the limit number of rows
      $query_string .= $limit;

      $users = $this->db->query($query_string)->result();

      $selected_tags = explode(',', $user_tags);
      if(strlen($user_tags)){
        $count_selected_tags = count($selected_tags);
      } else {
        $count_selected_tags = 0;
      }

      $users_result = array();
      foreach ($users as $key => $u) {
          $match_tags = true;
          $pk_tags = $this->UserTag->get($u->id);
          foreach ($selected_tags as $st) {
              if(!in_array(str_replace("'", "", $st), $pk_tags)){
                  $match_tags = false;
              }
          }

          if($count_selected_tags && !$match_tags){
            continue;
          }

          $users_result[$key] = $u;
      }

      // get all users ids
      $all_ids_users = array();
      foreach ($users_result as $u) {
          $all_ids_users['user_'.$u->id] = $u->id;
      }
      $data['all_ids_users'] = implode(",", $all_ids_users);

      // send filters to view
      $filters_string = array();
      foreach ($_GET as $key => $value) {
        if(is_array($value)){
            foreach ($value as $k => $v) {
              $filters_string[] = $key."%5B%5D=".$v;
            }
        } else {
            if($key != "page"){
                $filters_string[] = $key."=".$value;
            }
        }
      }
      $data['filters_string'] = "?".implode("&", $filters_string);

      // paginate array result
      if(!$export){
          $page = intval($this->input->get("page")) ? $this->input->get("page") : 1;
          $page = $page < 1 ? 1 : $page;
          $show_per_page = 20;

          $start = ($page - 1) * ($show_per_page);
          $offset = $show_per_page;

          // pagination configuration
          $config['base_url']   = base_url()."admin/user/".$data['filters_string'];
          $config['total_rows'] = count($users_result);
          $config['per_page']   = $show_per_page;
          $config['page_query_string'] = TRUE;
          $config['use_page_numbers'] = TRUE;
          $config['enable_query_strings'] = true;
          $config['query_string_segment'] = 'page';
          // $config['first_link'] = 'First';
          // $config['last_link']  = 'Last>>';

          $users_result = array_slice($users_result, $start, $offset);

          // init codeigniter pagination
          $this->load->library('pagination');
          $this->pagination->initialize($config);
      }


      // get users ids
      $ids_users = array();
      foreach ($users_result as $u) {
          $ids_users['user_'.$u->id] = $u->id;
      }
      $data['ids_users'] = implode(",", $ids_users);

      if($export){
          // get rankings
          $global_ranks  = $this->users->getGroupGlobalRanking($ids_users);
          $country_ranks = $this->users->getGroupCountryRanking($ids_users);
          $city_ranks    = $this->users->getGroupCityRanking($ids_users);
      }

      foreach ($users_result as $key => $u)
      {
          $s_commission = 0;
          $s_revenue    = 0;
          $slaves = $this->users->getSlaves($u->id);

          foreach ($slaves as $s)
          {
              $orders     = $this->users->getOrders($s->id);
              $revenue    = $orders[0]->revenue;
              $commission = $this->users->getTotalCommission($s->id);
              $s_commission += $commission;
              $s_revenue += $revenue;
          }

          $s_commission = number_format($s_commission, 2, '.', '');

          $time          = strtotime($u->dob);
          $dob           = date('Y-m-d',$time);
          $now           = date("Y-m-d");
          $age           = $now - $dob;

          $data['user_stats']      = $this->users->getOrders($u->id);
          $data['user_stats'][0]->total_earnings = $data['user_stats'][0]->revenue;
          $total_stats['earnings'] = $data['user_stats'][0]->total_earnings + $s_revenue;

          $facebook_followers  = $this->Social->checkNetwork('facebook', $u->id);
          $instagram_followers = $this->Social->checkNetwork('instagram', $u->id);
          $twitter_followers   = $this->Social->checkNetwork('twitter', $u->id);
          $tumblr_followers    = $this->Social->checkNetwork('tumblr', $u->id);
          $pinterest_followers = $this->Social->checkNetwork('pinterest', $u->id);
          $youtube_followers   = $this->Social->checkNetwork('youtube', $u->id);
          $vine_followers      = $this->Social->checkNetwork('vine', $u->id);

          $users_result[$key]->codes_sales              = number_format($total_stats['earnings'], 2);
          $users_result[$key]->revenue                  = number_format($data['user_stats'][0]->revenue, 2);
          $users_result[$key]->number_of_coupons        = count($this->Coupon->findAllByIdUser($u->id));
          $users_result[$key]->underkeepers_number      = count($slaves);
          $users_result[$key]->underkeepers_commission  = $s_commission;
          $users_result[$key]->pk_age                   = $age;
          $users_result[$key]->followers                = $this->users->getAllFollowers($u->id);
          $users_result[$key]->facebook_followers       = empty($facebook_followers) ? 0 : $facebook_followers;
          $users_result[$key]->twitter_followers        = empty($twitter_followers) ? 0 : $twitter_followers;
          $users_result[$key]->instagram_followers      = empty( $instagram_followers) ? 0 :  $instagram_followers;
          $users_result[$key]->tumblr_followers         = empty( $tumblr_followers) ? 0 :  $tumblr_followers;
          $users_result[$key]->pinterest_followers      = empty($pinterest_followers) ? 0 : $pinterest_followers;
          $users_result[$key]->youtube_followers        = empty($youtube_followers) ? 0 : $youtube_followers;
          $users_result[$key]->vine_followers           = empty($vine_followers ) ? 0 : $vine_followers ;
          $users_result[$key]->total_points             = $this->users->getPoints($u->id, 'total');
          $users_result[$key]->commission               = $this->users->getTotalCommission($u->id);
          if($export){
              $users_result[$key]->global_rank  = $global_ranks['user_'.$u->id]->place;
              $users_result[$key]->country_rank = $country_ranks['user_'.$u->id]->place;
              $users_result[$key]->city_rank    = $city_ranks['user_'.$u->id]->place;
          }
      }

      if(empty($users_result)){
          $coupon = $this->Coupon->getByCode($search);
          if($coupon){
              $user = $this->users->getById($coupon->id_creator);
              if($user){

                  $s_commission = 0;
                  $s_revenue    = 0;
                  $slaves = $this->users->getSlaves($user->id);
                  foreach ($slaves as $s)
                  {
                      $orders     = $this->users->getOrders($s->id);
                      $revenue    = $orders[0]->revenue;
                      $commission = $this->users->getTotalCommission($s->id);
                      $s_commission += $commission;
                      $s_revenue += $revenue;
                  }

                  $time          = strtotime($user->dob);
                  $dob           = date('Y-m-d',$time);
                  $now           = date("Y-m-d");
                  $age           = $now - $dob;

                  $data['user_stats'] = $this->users->getOrders($user->id);
                  $data['user_stats'][0]->total_earnings = $data['user_stats'][0]->revenue;
                  $total_stats['earnings'] = $data['user_stats'][0]->total_earnings + $s_revenue;

                  $facebook_followers  = $this->Social->checkNetwork('facebook', $user->id);
                  $instagram_followers = $this->Social->checkNetwork('instagram', $user->id);
                  $twitter_followers   = $this->Social->checkNetwork('twitter', $user->id);
                  $tumblr_followers    = $this->Social->checkNetwork('tumblr', $user->id);
                  $pinterest_followers = $this->Social->checkNetwork('pinterest', $user->id);
                  $youtube_followers   = $this->Social->checkNetwork('youtube', $user->id);
                  $vine_followers      = $this->Social->checkNetwork('vine', $user->id);

                  $users_result[0] = $user;
                  $users_result[0]->codes_sales             = number_format($total_stats['earnings'], 2);
                  $users_result[0]->revenue                 = number_format($data['user_stats'][0]->revenue, 2);
                  $users_result[0]->number_of_coupons       = count($this->Coupon->findAllByIdUser($user->id));
                  $users_result[0]->underkeepers_number     = count($slaves);
                  $users_result[0]->underkeepers_commission = $s_commission;
                  $users_result[0]->pk_age                  = $age;
                  $users_result[0]->followers               = $this->users->getAllFollowers($user->id);
                  $users_result[0]->facebook_followers      = empty($facebook_followers) ? 0 : $facebook_followers;
                  $users_result[0]->twitter_followers       = empty($twitter_followers) ? 0 : $twitter_followers;
                  $users_result[0]->instagram_followers     = empty( $instagram_followers) ? 0 :  $instagram_followers;
                  $users_result[0]->tumblr_followers        = empty( $tumblr_followers) ? 0 :  $tumblr_followers;
                  $users_result[0]->pinterest_followers     = empty($pinterest_followers) ? 0 : $pinterest_followers;
                  $users_result[0]->youtube_followers       = empty($youtube_followers) ? 0 : $youtube_followers;
                  $users_result[0]->vine_followers          = empty($vine_followers ) ? 0 : $vine_followers ;
                  $users_result[0]->total_points            = $this->users->getPoints($user->id, 'total');
                  $users_result[0]->commission              = $this->users->getTotalCommission($user->id);
                  $users_result[0]->global_rank             = $this->users->getGlobalRanking($user->id);
                  $users_result[0]->country_rank            = $this->users->getCountryRanking($user->id);
                  $users_result[0]->city_rank               = $this->users->getCityRanking($user->id);
              }
          }
      }

      $data['users'] = $users_result;
      $data['count_selected_tags'] = $count_selected_tags;
      $data['selected_tags'] = $selected_tags;

      if ($export) {
          $this->load->library('excel');
          $format = $this->input->post('format');

          // get automatically coupons
          $auto_coupons = $this->session->userdata("coupons");
          if(!$auto_coupons){
            $auto_coupons = array();
          }

          $this->excel->setActiveSheetIndex(0);
          $this->excel->getActiveSheet()->setTitle('PieceKeepers');

          $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
          $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
          $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
          $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
    	  $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
    	  $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
    	  $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
    	  $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
    	  $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
    	  $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('X')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('Z')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('AA')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('AB')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('AC')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('AD')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('AE')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('AF')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('AG')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('AH')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('AI')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('AJ')->setWidth(20);
          $this->excel->getActiveSheet()->getColumnDimension('AK')->setWidth(20);

          $this->excel->getActiveSheet()->setCellValue('A1', 'First Name');
          $this->excel->getActiveSheet()->setCellValue('B1', 'Last Name');
          $this->excel->getActiveSheet()->setCellValue('C1', 'Email');
		  $this->excel->getActiveSheet()->setCellValue('D1', 'Phone');
		  $this->excel->getActiveSheet()->setCellValue('E1', 'Dob');
		  $this->excel->getActiveSheet()->setCellValue('F1', 'Gender');
          $this->excel->getActiveSheet()->setCellValue('G1', 'Level');
		  $this->excel->getActiveSheet()->setCellValue('H1', 'Address1');
		  $this->excel->getActiveSheet()->setCellValue('I1', 'Address2');
		  $this->excel->getActiveSheet()->setCellValue('J1', 'Postcode');
          $this->excel->getActiveSheet()->setCellValue('K1', 'Country');
          $this->excel->getActiveSheet()->setCellValue('L1', 'City');
          $this->excel->getActiveSheet()->setCellValue('M1', 'School');
          $this->excel->getActiveSheet()->setCellValue('N1', 'Graduation Year');
          $this->excel->getActiveSheet()->setCellValue('O1', 'Social Followers');
          $this->excel->getActiveSheet()->setCellValue('P1', 'Facebook Followers');
          $this->excel->getActiveSheet()->setCellValue('Q1', 'Twitter Followers');
          $this->excel->getActiveSheet()->setCellValue('R1', 'Instagram Followers');
          $this->excel->getActiveSheet()->setCellValue('S1', 'Tumblr Followers');
          $this->excel->getActiveSheet()->setCellValue('T1', 'Pinterest Followers');
          $this->excel->getActiveSheet()->setCellValue('U1', 'Youtube Followers');
          $this->excel->getActiveSheet()->setCellValue('V1', 'Vine Followers');
          $this->excel->getActiveSheet()->setCellValue('W1', 'Total Points');
          $this->excel->getActiveSheet()->setCellValue('X1', 'Earned Commission');
          $this->excel->getActiveSheet()->setCellValue('Y1', 'Global Rank');
          $this->excel->getActiveSheet()->setCellValue('Z1', 'Country Rank');
          $this->excel->getActiveSheet()->setCellValue('AA1', 'City Rank');
          $this->excel->getActiveSheet()->setCellValue('AB1', 'Inactive for days');
          $this->excel->getActiveSheet()->setCellValue('AC1', 'Auto Coupon');
          $this->excel->getActiveSheet()->setCellValue('AD1', 'State');
          $this->excel->getActiveSheet()->setCellValue('AE1', 'Date of Registration');
          $this->excel->getActiveSheet()->setCellValue('AF1', 'Number Of Undekeepers');
          $this->excel->getActiveSheet()->setCellValue('AG1', 'Underkeepers Commission');
          $this->excel->getActiveSheet()->setCellValue('AH1', 'PieceKeeper Age');
          $this->excel->getActiveSheet()->setCellValue('AI1', 'Number Of Codes');
          $this->excel->getActiveSheet()->setCellValue('AJ1', 'Revenue');
          $this->excel->getActiveSheet()->setCellValue('AK1', 'Codes Sales');

          $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
          $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
          $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
          $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
          $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
          $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
          $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);

          $i = 2;

          $currentTime = new DateTime(date('Y-m-d'));

          foreach ($data['users'] as $u) {
              if ($u->level <= 1 ) {
                  $this->excel->getActiveSheet()->setCellValue('A'.$i, $u->first_name);
                  $this->excel->getActiveSheet()->setCellValue('B'.$i, $u->last_name);
                  $this->excel->getActiveSheet()->setCellValue('C'.$i, $u->email);
        		  $this->excel->getActiveSheet()->setCellValue('D'.$i, $u->phone);
        		  $this->excel->getActiveSheet()->setCellValue('E'.$i, $u->dob);
        		  $this->excel->getActiveSheet()->setCellValue('H'.$i, $u->address1);
        		  $this->excel->getActiveSheet()->setCellValue('I'.$i, $u->address2);
        		  $this->excel->getActiveSheet()->setCellValue('J'.$i, $u->postcode);
                  $this->excel->getActiveSheet()->setCellValue('N'.$i, $u->graduation_year);
                  $this->excel->getActiveSheet()->setCellValue('O'.$i, $u->followers);
                  $this->excel->getActiveSheet()->setCellValue('P'.$i, $u->facebook_followers);
                  $this->excel->getActiveSheet()->setCellValue('Q'.$i, $u->twitter_followers);
                  $this->excel->getActiveSheet()->setCellValue('R'.$i, $u->instagram_followers);
                  $this->excel->getActiveSheet()->setCellValue('S'.$i, $u->tumblr_followers);
                  $this->excel->getActiveSheet()->setCellValue('T'.$i, $u->pinterest_followers);
                  $this->excel->getActiveSheet()->setCellValue('U'.$i, $u->youtube_followers);
                  $this->excel->getActiveSheet()->setCellValue('V'.$i, $u->vine_followers);

                  $this->excel->getActiveSheet()->setCellValue('W'.$i, $u->total_points);
                  $this->excel->getActiveSheet()->setCellValue('X'.$i, $u->commission);
                  $this->excel->getActiveSheet()->setCellValue('Y'.$i, $u->global_rank);
                  $this->excel->getActiveSheet()->setCellValue('Z'.$i, $u->country_rank);
        		  $this->excel->getActiveSheet()->setCellValue('AA'.$i, $u->city_rank);

                  if ($u->level == -2) {
                      $this->excel->getActiveSheet()->setCellValue('G'.$i, 'PieceKeeper Level 1');
                  } elseif ($u->level == -1) {
                      $this->excel->getActiveSheet()->setCellValue('G'.$i, 'PieceKeeper Level 2');
                  } elseif ($u->level == 0) {
                      $this->excel->getActiveSheet()->setCellValue('G'.$i, 'PieceKeeper Level 3');
                  } elseif ($u->level == 1) {
                      $this->excel->getActiveSheet()->setCellValue('G'.$i, 'Master PieceKeeper');
                  }
				          if ($u->gender == 0) {
                      $this->excel->getActiveSheet()->setCellValue('F'.$i, 'Female');
                  } elseif ($u->gender == 1) {
                      $this->excel->getActiveSheet()->setCellValue('F'.$i, 'Male');
                  }
                  $this->excel->getActiveSheet()->setCellValue('K'.$i, $u->country);
                  $this->excel->getActiveSheet()->setCellValue('L'.$i, $u->city);
                  $this->excel->getActiveSheet()->setCellValue('M'.$i, $u->school);

                  $last_login = new DateTime(substr($u->last_login,0,10));
                  $diff = $currentTime->diff($last_login);

                  if($diff->days > 700000){
                  	$loggedInDays = "NL";
                  }else{
                  	$loggedInDays = $diff->days;
                  }

                  $this->excel->getActiveSheet()->setCellValue('AB'.$i, $loggedInDays);

                  if(isset($auto_coupons["_".$u->id])){
                    $this->excel->getActiveSheet()->setCellValue('AC'.$i, $auto_coupons["_".$u->id]);
                  }

                  $this->excel->getActiveSheet()->setCellValue('AD'.$i, $u->state);
                  $this->excel->getActiveSheet()->setCellValue('AE'.$i, $u->date_joined);
                  $this->excel->getActiveSheet()->setCellValue('AF'.$i, $u->underkeepers_number);
                  $this->excel->getActiveSheet()->setCellValue('AG'.$i, $u->underkeepers_commission);
                  $this->excel->getActiveSheet()->setCellValue('AH'.$i, $u->pk_age);
                  $this->excel->getActiveSheet()->setCellValue('AI'.$i, $u->number_of_coupons);
                  $this->excel->getActiveSheet()->setCellValue('AJ'.$i, $u->revenue);
                  $this->excel->getActiveSheet()->setCellValue('AK'.$i, $u->codes_sales);

                  $i++;
              }
          }

          switch ($format) {
            default:
            case 'xls':
                $filename = "piecekeepers.xls";
                header('Content-Type: application/vnd.ms-excel'); //mime type
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

            break;

            case 'xlsx':
                $filename = "piecekeepers.xlsx";
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //mime type
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

            break;

            case 'csv':
                $filename = "piecekeepers.csv";
                header('Content-type: text/csv'); //mime type
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');

            break;
          }

          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache

          $objWriter->save('php://output');
          exit();
      }


      $data['order_by'] = $this->input->get("table-order-by");
      $data['order_how'] = $this->input->get("table-order-how");
      $data['title'] = "PieceKeepers";

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/user-list', $data);
      $this->load->view('templates/footer');
  }


  public function edit($id, $notification_id = false) {
      $data['admin'] = $this->users->get($this->session->userdata('id'));
      $this->users->checkAdminUserRights($id, 'admin/user/');

      if($notification_id) {
          $this->AdminNotification->setInactive($notification_id);
      }
      $data['user'] = $this->users->get_profile($id);
      $data['title'] = "Edit ".$data['user']['first_name'] . " " . $data['user']['last_name'] . "'s Profile";

      // $data['user_stats'] = $this->users->get_stats($id);
      // $data['user_stats'] = $this->db->query("SELECT COUNT(*) as total_sales, SUM(points) as total_points, SUM(bonus) as bonus_points, SUM(total) as total_earnings FROM sales WHERE id_user=" . $this->session->userdata('id'))->row();
      $data['user_stats'] = $this->db->query("SELECT COUNT(*) AS total_sales, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id}")->row();

      $points = $this->Points->getFromUser($id);
      $data['user_stats']->total_points = $points['sales'];
      $data['user_stats']->bonus_points = $points['bonus'];

      $query = $this->db->query("SELECT * FROM country_t");
      $data['countries'] = $query->result();


      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/user-edit', $data);
      $this->load->view('templates/footer');
  }


public function view($id, $notification_id = false) {

      $settings = $this->site_config->getGeneralSettings();

      $data['fb_profile']   = $this->Social->getNetworkProfile('facebook', $id);
      $data['tw_profile']   = $this->Social->getNetworkProfile('twitter', $id);
      $data['inst_profile'] = $this->Social->getNetworkProfile('instagram', $id);
      $data['tub_profile']  = $this->Social->getNetworkProfile('tumblr', $id);
      $data['pin_profile']  = $this->Social->getNetworkProfile('pinterest', $id);
      $data['ln_profile']   = $this->Social->getNetworkProfile('linkedin', $id);
      $data['yb_profile']   = $this->Social->getNetworkProfile('youtube', $id);
      $data['vn_profile']   = $this->Social->getNetworkProfile('vine', $id);

      $data['fb_friends']     = $this->Social->checkNetwork('facebook', $id);
      $data['tw_followers']   = $this->Social->checkNetwork('twitter', $id);
      $data['inst_followers'] = $this->Social->checkNetwork('instagram', $id);
      $data['tub_followers']  = $this->Social->checkNetwork('tumblr', $id);
      $data['pin_followers']  = $this->Social->checkNetwork('pinterest', $id);
      $data['ln_followers']   = $this->Social->checkNetwork('linkedin', $id);
      $data['yb_followers']   = $this->Social->checkNetwork('youtube', $id);
      $data['vn_followers']   = $this->Social->checkNetwork('vine', $id);

      $id_user = $this->session->userdata('id');

      $data['admin'] = $this->users->get($this->session->userdata('id'));
      $this->users->checkAdminUserRights($id, 'admin/user/');

      // get admin comments
      $data['comments'] = $this->AdminComment->getByUser($id);

      // scratch coupons
      $scratch_batches = $this->users->getScratchBatches($id);
      foreach ($scratch_batches as $key => $batch) {
          $scratch_batches[$key]->used_coupons = $this->ScratchCoupon->countUsedCoupons($batch->batch);
          $scratch_batches[$key]->all_coupons  = $this->ScratchCoupon->countAllCoupons($batch->batch);
      }

      $data['scratch_batches']       = $scratch_batches;
      $data['scratch_code_discount'] = $settings['scratch_code_discount'];


      // get ranking
      $user = $this->users->get($id);
      if ($user['date_joined'] < date("Y-m-d H:i:s", strtotime("-1 month"))) {
          $one_month = true;
      } else {
          $one_month = false;
      }

      $global_ranking = $this->users->getGlobalRanking($id);
      $country_ranking = $this->users->getCountryRanking($id);
      $city_ranking = $this->users->getCityRanking($id);

      if ($one_month) {
          $global_ranking_last_month = $this->users->getGlobalRanking($id, true);
          $country_ranking_last_month = $this->users->getCountryRanking($id, true);
          $city_ranking_last_month = $this->users->getCityRanking($id, true);

          $global_ranking_progress = $global_ranking_last_month - $global_ranking;
          $country_ranking_progress = $country_ranking_last_month - $country_ranking;
          $city_ranking_progress = $city_ranking_last_month - $city_ranking;
      }

      $data['global_ranking']           = $global_ranking;
      $data['global_ranking_progress']  = isset($global_ranking_progress) ? $global_ranking_progress : null;
      $data['country_ranking']          = $country_ranking;
      $data['country_ranking_progress'] = isset($country_ranking_progress) ? $country_ranking_progress : null;
      $data['city_ranking']             = $city_ranking;
      $data['city_ranking_progress']    = isset($city_ranking_progress) ? $city_ranking_progress : null;
      $data['one_month']      = $one_month;

      if($notification_id) {
          $this->AdminNotification->setInactive($notification_id);
      }

      $data['user']               = $this->users->get_profile($id);
      $data['user_id']            = $id;
      $data['user_date_format']   = $this->User->getDateFormat($id);

      // get slaves info
      $s_sales      = 0;
      $s_commission = 0;
      $s_revenue    = 0;
      $s_coupons    = 0;
      $s_sales_points = 0;
      $s_bonus_points = 0;

      if ($this->users->is_master($id)) {

          $slaves = $this->users->getSlaves($id);
          foreach ($slaves as $s) {
              $coupons    = $this->Coupon->findAllByIdUser($s->id);
              $orders     = $this->users->getOrders($s->id);
              $commission = $this->users->getTotalCommission($s->id);
              $revenue    = $orders[0]->revenue;
              $points     = $this->users->getPoints($s->id);
              $sales_points = $points['sales'];
              $bonus_points = $points['bonus'];

              $s_sales += intval($orders[0]->total_sales);
              $s_commission += $commission;
              $s_revenue += $revenue;
              $s_coupons += count($coupons);
              $s_sales_points += $sales_points;
              $s_bonus_points += $bonus_points;
          }

          $s_sales = intval($s_sales);
          $s_revenue = number_format($s_revenue, 2, '.', '');
          $s_commission = number_format($s_commission, 2, '.', '');
          $s_coupons = intval($s_coupons);
          $s_sales_points = intval($s_sales_points);
          $s_bonus_points = intval($s_bonus_points);
      }

      $data['s_sales']      = $s_sales;
      $data['s_revenue']    = $s_revenue;
      $data['s_commission'] = $s_commission;
      $data['s_coupons']    = $s_coupons;


      if (intval($data['user']['id_master']) != 0) {
          $data['master'] = $this->users->get($data['user']['id_master']);
      }

      $data['title'] = $data['user']['first_name'] . " " . $data['user']['last_name'] . "'s Profile";

      $data['user_stats'] = $this->users->getOrders($id);
      $data['user_stats'] = $data['user_stats'][0];
      $data['user_stats']->total_earnings = $data['user_stats']->revenue;
      $data['user_stats']->total_points = 0;
      $data['user_stats']->bonus_points = 0;

      $total_stats['sales'] = $data['user_stats']->total_sales + $s_sales;
      $total_stats['earnings'] = $data['user_stats']->total_earnings + $s_revenue;

      $points = $this->Points->getFromUser($id);
      $total_stats['total_points'] = $points['sales'] + $s_sales_points;
      $total_stats['bonus_points'] = $points['bonus'] + $s_bonus_points;

      $data['total_stats'] = $total_stats;

      $this->session->set_flashdata('prev_url', uri_string());

      $data['payment_requests'] = $this->db->query("SELECT p.id, p.created, p.amount, p.giftcard, p.gift_amount, p.aproved, u.first_name, u.last_name, u.id as user_id, u.email , c.code ".
            " FROM payment_requests as p ".
            " INNER JOIN auth_user as u ON p.id_user=u.id ".
            " LEFT JOIN coupons as c ON p.id_coupon=c.id ".
            " WHERE p.id_user=$id ORDER BY p.created DESC ")->result();

      $data['slaves_requests'] = $this->db->query("SELECT p.id,p.created,p.slaves,p.aproved, u.first_name, u.last_name, u.id ".
              "as user_id, u.email FROM slaves_requests as p ".
              " INNER JOIN auth_user as u ON p.id_user=u.id WHERE p.id_user=$id ORDER BY p.created DESC ")->result();

      $coupons_data = $this->Coupon->findAllByIdUser($id);

      foreach($coupons_data as $c){
        $c->countOrders = $this->Coupon->countOrders($c->code);
      }

      $data['coupons'] = $coupons_data;
      $data['orders'] = $this->users->getSalesStats($data['user']['id'], strtotime($data['user']['date_joined']), time());
      $data['commission'] = $this->users->getTotalCommission($id);
      $data['unpaid_commission'] = $this->users->getCommission($id);

      $data['slaves'] = $this->users->getSlaves($id);

      $query = $this->db->query("SELECT * FROM country_t");
      $data['countries'] = $query->result();

      $dob  = DateTime::createFromFormat('Y-m-d', $data['user']['dob']);
      $diff = $dob->diff(new DateTime);
      $age  = $diff->y;

      // get starting level
      $data['starting_level'] = $this->users->getStartingLevel($id);

      $data['age'] = $age;
      $data['total_followers'] = $this->users->getTotalFollowers($id);
      $data['social_credit'] = $this->users->getSocialCredit($data['total_followers'], true);
      $data['user_tags']     = $this->UserTag->get($id);
      $data['all_user_tags'] = $this->UserTag->getAll();


      $data["questions"] = $this->questions->getAllByUser($id);

      //get country prefix
      $country = $data['user']['country'];

      $code    = $this->db->query("SELECT * FROM dial_codes WHERE country='{$country}'")->row();
      if (count($code)) {
      	$data['user']['prefix'] = $code->code;
      } else {
      	$data['user']['prefix'] = '';
      }


      // check if package lvl 2 is sent or not
      $data['package_lvl2'] = $this->Package->getByIdUserAndType($id, 1);
      $data['package_lvl3'] = $this->Package->getByIdUserAndType($id, 2);

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/user-view', $data);
      $this->load->view('templates/footer');

  }

  public function notifications($id, $page = false){
      $data['admin'] = $this->users->get($this->session->userdata('id'));
      $this->users->checkAdminUserRights($id, 'admin/user/');
      $user = $this->users->getById($id);

      $page = intval($page) > 0 ? $page : 1;
      $items_per_page = 20;

      $notifications = $this->Notification->getHistory($id, $page, $items_per_page);
      $total_items   = $this->Notification->countAll($id);
      $total_pages   = $this->Notification->countPages($id, $items_per_page);

      foreach ($notifications as $key => $n) {
          if($n->id_admin){
              $admin = $this->users->getById($n->id_admin);
              $notifications[$key]->admin_name = $admin->first_name.' '.$admin->last_name;
          }
      }

      $data['title'] = $user->first_name.' '.$user->last_name."'s Notifications";
      $data['notifications'] = $notifications;
      $data['total_items']   = $notifications;
      $data['total_pages']   = $total_pages;
      $data['current_page']  = $page;
      $data['user']          = $user;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/user-notifications', $data);
      $this->load->view('templates/footer');
  }

  public function change_status($id) {
      $this->users->checkAdminUserRights($id, 'admin/user/');

      $this->users->change_status($id);
      redirect('admin/user/view/'.$id);
  }

  public function change_level($user_id) {
      $this->users->checkAdminUserRights($user_id, 'admin/user/');

      $user = $this->users->get($user_id);

      if ($user['level'] < 1) {
          $level = 1;
      } else {
          $level = 0;
      }

      $this->db->set("level", $level, FALSE );
      $this->db->set("master_request", 0, FALSE );
      $this->db->where("id", $user_id);
      $this->db->update("auth_user");


      if($level == 1) {

        if (intval($user['slaves']) <= 5) {
            $this->db->set('slaves', 5, false);
            $this->db->where("id", $user_id);
            $this->db->update("auth_user");
        }

        $this->load->model("email_model", "Email");
        $this->Email->approvedMaster( $user['email'] );
      }

      redirect('admin/user/view/'.$user_id);

  }


  public function add_comment($id_user){
    $id_admin = $this->session->userdata('id');
    $comment  = ($this->input->post("comment") == NULL) ? FALSE : $this->input->post("comment");

    $sendEmail = ($this->input->post("comment-send-mail") == NULL) ? FALSE : $this->input->post("comment-send-mail");

    if($sendEmail) {

      $this->load->model("email_model", "Email");

      $user = $this->User->getById($id_user);

      $this->Email->sendComment($user->email,$comment);

      $sendEmail = 1;

    } else {

      $sendEmail = 0;

    }

    $this->AdminComment->insert($id_admin, $id_user, $comment, $sendEmail);

    redirect('admin/user/view/'.$id_user."#show-comments");
  }

  public function delete_comment($id){
    $id_admin = $this->session->userdata('id');
    $comment  = $this->AdminComment->getById($id);

    if($comment->id_admin == $id_admin){
      $this->AdminComment->delete($id);
    }

    redirect('admin/user/view/'.$comment->id_user."#show-comments");
  }

  public function update_package_sent(){
    $id_user = ($this->input->post("id_user") == NULL) ? FALSE : $this->input->post("id_user");
    $level   = ($this->input->post("level") == NULL) ? FALSE : $this->input->post("level");
    $action  = ($this->input->post("action") == NULL) ? FALSE : $this->input->post("action");

    $this->users->updatePackageSent($id_user, $level, $action);
  }

  public function create_discount_codes(){
    $this->load->helper('string');

    $type       = $this->input->post("type");
    $days       = (int) $this->input->post("days");
    $percentage = (int) $this->input->post("percentage");
    $ids_users  = explode(",", $this->input->post("users"));
    $filters_string = ($this->input->post("filters_string") == NULL) ? FALSE : $this->input->post("filters_string");

    $errors = array();

    if($type != 1 && $type != 2){
      array_push($errors, "Invalid type");
    }
    if($days < 1 || $days > 10){
      array_push($errors, "Invalid days number");
    }
    if($percentage < 1 || $percentage > 100){
      array_push($errors, "Invalid percentage number");
    }

    if(count($errors)){
      $response['status'] = "error";
      $response['errors'] = $errors;
    } else {
      $response['status']     = "success";
      $response['type']       = $type;
      $response['days']       = $days;
      $response['percentage'] = $percentage;
      //$response['coupon']     = $coupon;
      $response['count']      = count($ids_users);

      $coupons = array();
      foreach ($ids_users as $id_user) {
        $coupon = random_string("alnum", 6);
        $coupon = strtoupper($coupon);

        $coupons["_".$id_user] = $coupon;

        $this->Coupon->createAutoCoupon($id_user, $coupons["_".$id_user], $type, $percentage, $days);
      }

      $this->session->set_userdata("coupons", $coupons);
    }

    $this->session->set_flashdata("coupons_response", $response);
    redirect("/admin/user".$filters_string);
  }

  public function block($id)
  {
    $this->users->checkAdminUserRights($id, 'admin/user/');

    $this->users->block($id);
    redirect('admin/user/view/'.$id);
  }

  public function delete_pending($id)
  {
      $this->users->checkAdminUserRights($id, 'admin/user/');

      $this->users->block($id, true);
      redirect('admin/user/view/'.$id);
  }

  public function delete($id)
  {
    $this->users->checkAdminUserRights($id, 'admin/user/');

    $this->users->delete($id);
    redirect('admin/user/');
  }

  public function unblock($id)
  {
    $this->users->checkAdminUserRights($id, 'admin/user/');

    $this->users->unblock($id);
    redirect('admin/user/view/'.$id);
  }

  public function approve($id, $level = -2)
  {
    $this->users->checkAdminUserRights($id, 'admin/user/');
    $user = $this->users->get($id);

    $this->users->approve($id);
    $this->AdminNotification->logHandling($this->session->userdata('id'), $id, 'auth_user');

    if ($level == -1) {
        $this->users->changeLevel($id, "-1");
        $this->Email->sendUpgradeLevel2($user['email']);
    }
    else if ($level == 0) {
        $this->users->changeLevel($id, "0");
        $this->Email->sendUpgradeLevel3($user['email']);
    }
    redirect('admin/user/view/'.$id);
  }

  public function disapprove($id)
  {
    $this->users->checkAdminUserRights($id, 'admin/user/');
    $user = $this->users->get($id);

    $this->users->disapprove($id);
    $this->AdminNotification->logHandling($this->session->userdata('id'), $id, 'auth_user');
    $this->Email->sendPiecekeeperApplicationRejected($user['email']);

    redirect('admin/user/view/'.$id);
  }

  public function add_tag(){
    $tag     = ($this->input->post('tag') == NULL) ? FALSE : $this->input->post('tag');
    $id_user = ($this->input->post('id_user') == NULL) ? FALSE : $this->input->post('id_user');
    $this->UserTag->add($id_user, $tag);
  }

  public function remove_tag(){
    $tag     = ($this->input->post('tag') == NULL) ? FALSE : $this->input->post('tag');
    $id_user = ($this->input->post('id_user') == NULL) ? FALSE : $this->input->post('id_user');
    $this->UserTag->remove($id_user, $tag);
  }

  public function edit_sales_points(){

    $value     = ($this->input->post('value') == NULL) ? FALSE : $this->input->post('value');
    $old_value = ($this->input->post('old_value') == NULL) ? FALSE : $this->input->post('old_value');
    $id_user   = ($this->input->post('id_user') == NULL) ? FALSE : $this->input->post('id_user');

    $added_value = intval($value) - intval($old_value);

    // if($added_value <= 0){
    //   echo 'invalid value';
    //   exit();
    // }

    $this->Points->insert($id_user, $added_value, 1);
    $this->Notification->insertData($id_user, 17, "1_".$added_value);
    echo 'success';
  }

  public function edit_bonus_points(){
    $value     = ($this->input->post('value') == NULL) ? FALSE : $this->input->post('value');
    $old_value = ($this->input->post('old_value') == NULL) ? FALSE : $this->input->post('old_value');
    $id_user   = ($this->input->post('id_user') == NULL) ? FALSE : $this->input->post('id_user');

    $added_value = intval($value) - intval($old_value);

    // if($added_value <= 0){
    //   echo 'invalid value';
    //   exit();
    // }

    $this->Points->insert($id_user, $added_value, 2);
    $this->Notification->insertData($id_user, 17, "2_".$added_value);
    echo 'success';
  }

  public function edit_commission(){
    $value     = ($this->input->post('value') == NULL) ? FALSE : $this->input->post('value');
    $old_value = ($this->input->post('old_value') == NULL) ? FALSE : $this->input->post('old_value');
    $id_user   = ($this->input->post('id_user') == NULL) ? FALSE : $this->input->post('id_user');
    $password  = ($this->input->post('password') == NULL) ? FALSE : $this->input->post('password');
    $id_admin  = $this->session->userdata('id');

    $added_value = floatval($value) - floatval($old_value);
    $added_value = number_format($added_value, 2);

    if(!$this->users->checkPassword($id_admin, $password)){
      echo 'invalid password';
      exit();
    }

    if($added_value <= 0){
      echo 'invalid value';
      exit();
    }

    $this->Commission->insert($id_user, $added_value, '', date('Y-m-d H:i:s', time()), '', 1);
    $this->Notification->insertData($id_user, 17, "3_".$added_value);
    echo 'success';
  }

  public function edit_earnings(){
    $value     = ($this->input->post('value') == NULL) ? FALSE : $this->input->post('value');
    $old_value = ($this->input->post('old_value') == NULL) ? FALSE : $this->input->post('old_value');
    $id_user   = ($this->input->post('id_user') == NULL) ? FALSE : $this->input->post('id_user');
    $password  = ($this->input->post('password') == NULL) ? FALSE : $this->input->post('password');
    $id_admin  = $this->session->userdata('id');

    $added_value = floatval($value) - floatval($old_value);
    $added_value = number_format($added_value, 2);

    if(!$this->users->checkPassword($id_admin, $password)){
      echo 'invalid password';
      exit();
    }

    if($added_value <= 0){
      echo 'invalid value';
      exit();
    }

    // get the fake order ID
    $this->db->select_min('order_id');
    $order_id = $this->db->get('orders')->row()->order_id;
    if($order_id >= 0){
        $order_id = -1;
    } else {
        $order_id --;
    }

    // insert order with coupon "ADJUSTED_EARNINGS"
    $coupon = "ADJUSTED_EARNINGS_".$id_user;
    $this->db->where(array(
        'id_creator' => $id_user,
        'code' => $coupon
      ));
    $result = $this->db->get('coupons')->row();
    if(!count($result)){
        $data = array(
              'id_creator' => $id_user,
              'code' => $coupon,
              'approved' => 1,
              'deleted' => 0
          );
        $this->db->set('created', 'NOW()', false);
        $this->db->insert('coupons', $data);
    }

    // insert a fake order
    $data = array(
        'order_id' => $order_id,
        'currency' => 'USD',
        'total' => $added_value,
        'transaction_amount' => $added_value
      );
    $this->db->set('created', 'NOW()', false);
    $this->db->set('modified', 'NOW()', false);
    $this->db->insert('orders', $data);

    $data = array(
        'order_id' => $order_id,
        'id_user' => $id_user,
        'coupon' => $coupon,
        'checked' => 1,
      );
    $this->db->set('created', 'NOW()', false);
    $this->db->insert('orders_coupons', $data);

    $this->Notification->insertData($id_user, 17, "4_".$added_value);
    echo 'success';
  }


  public function link_to_master($id_user){
      $id_user = intval($id_user);
      if($id_user){
          $email = ($this->input->post("email") == NULL) ? FALSE : $this->input->post("email");
          $master = $this->users->getByEmail($email);

          if($master){
              $id_master = $master->id;
              $this->users->linkToMaster($id_user, $id_master);
          }
          redirect("admin/user/view/".$id_user);
      }

      redirect("/");
  }


  public function unlink_pk($id_user, $id_master){
      $user = $this->users->getById($id_user);
      if($user && $user->id_master == $id_master){
          $this->db->set("id_master", 0);
          $this->db->where("id", $id_user);
          $this->db->update("auth_user");

          redirect("/admin/user/view/".$id_user);
      }
      redirect('/');
  }

  public function delete_social_account($id_user, $network) {
      $this->users->checkAdminUserRights($id_user, 'admin/user/');

      $this->Social->deleteAccount($id_user, $network);
      redirect("/admin/user/view/".$id_user);
  }
}

?>
