<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admins extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->load->helper('url');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }
    
    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }

    $admin = $this->users->get($this->session->userdata('id'));
    if ($admin['level'] < 3) {
        redirect('/admin');
    }

  }

  public function index() {
    $data['admin'] = $this->users->get($this->session->userdata('id'));

    $query  = $this->db->query("SELECT * FROM auth_user WHERE level>=2");
    $admins = $query->result();

    $data['title']  = 'Admins';
    $data['admins'] = $admins;


    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/admins-list', $data);
    $this->load->view('templates/footer');
  }

  public function add(){
    $data['admin'] = $this->users->get($this->session->userdata('id'));



    $query     = $this->db->query("SELECT * FROM country_t");
    $countries = $query->result();

    if (isset($_POST['trigger']) && $_POST['trigger'] == 'add') {
        $this->form_validation->set_rules("first_name", "First Name", 'required');
        $this->form_validation->set_rules("last_name", "Last Name", 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == true) {
            $pk = $this->users->getByEmail($this->input->post('email'));

            if(!count($pk)){
                $this->users->register(false, true);
            }

            $query   = $this->db->query("SELECT id FROM auth_user WHERE email='{$this->input->post('email')}'");
            $user    = $query->result();
            $id_user = $user[0]->id;

            if($this->input->post('super_admin')){
                $this->db->set('level', '3');
            } else {
                $this->db->set('level', '2');
            }
            $this->db->where('id', $id_user);
            $this->db->update('auth_user');

            if($this->input->post('all-countries')) {

              foreach ($countries as $c) {

                  $query = $this->db->query("SELECT * FROM admins_countries WHERE id_user={$id_user} AND id_country={$c->country_id}");

                  if (!count($query->result())) {
                      $this->db->set('id_user', $id_user, false);
                      $this->db->set('id_country', $c->country_id, false);
                      $this->db->insert('admins_countries');
                  }
              }

            } else {

              foreach ($this->input->post('countries') as $id_country) {
                  if (intval($id_country)) {
                      $query = $this->db->query("SELECT * FROM admins_countries WHERE id_user={$id_user} AND id_country={$id_country}");
                      if (!count($query->result())) {
                        $this->db->set('id_user', $id_user, false);
                        $this->db->set('id_country', $id_country, false);
                        $this->db->insert('admins_countries');
                      }
                  }
              }

            }

            redirect('/admin/admins/');
        }
    }


    $data['title']     = 'Add Admin';
    $data['countries'] = $countries;

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/admins-add', $data);
    $this->load->view('templates/footer');
  }

  public function edit($id) {
    $data['admin'] = $this->users->get($this->session->userdata('id'));

    // get user
    $query  = $this->db->query("SELECT * FROM auth_user WHERE id={$id}");
    $result = $query->result();
    $admin  = $result[0];

    // get countries
    $query     = $this->db->query("SELECT * FROM admins_countries AS ac INNER JOIN country_t AS c ON c.country_id=ac.id_country WHERE ac.id_user={$id}");
    $admin_countries = $query->result();


    $query     = $this->db->query("SELECT * FROM country_t");
    $countries = $query->result();

    if (isset($_POST['trigger']) && $_POST['trigger'] == 'edit') {
        $this->form_validation->set_rules("first_name", "First Name", 'required');
        $this->form_validation->set_rules("last_name", "Last Name", 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        if ($this->form_validation->run() == true) {
            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email')
              );
            if($this->input->post('edit_super_admin')){
                if($this->input->post('super_admin')){
                    $data['level'] = 3;
                } else {
                    $data['level'] = 2;
                }
            }
            $this->db->where('id', $id);
            $this->db->update('auth_user', $data);

            if($this->input->post('all-countries')) {

              foreach ($countries as $c) {

                $query = $this->db->query("SELECT * FROM admins_countries WHERE id_user={$id} AND id_country={$c->country_id}");

                if (!count($query->result())) {

                  $this->db->set('id_user', $id, false);
                  $this->db->set('id_country', $c->country_id, false);
                  $this->db->insert('admins_countries');

                }
              }

            } else {

              foreach ($this->input->post('countries') as $id_country) {
                  if (intval($id_country)) {
                      $query = $this->db->query("SELECT * FROM admins_countries WHERE id_user={$id} AND id_country={$id_country}");
                      if (!count($query->result())) {
                        $this->db->set('id_user', $id, false);
                        $this->db->set('id_country', $id_country, false);
                        $this->db->insert('admins_countries');
                      }
                  }
              }
            }

            redirect('/admin/admins/');
        }
    }


    $data['all_countries_already_selected'] = false;
    if(count($admin_countries) == count($countries)) {
      $data['all_countries_already_selected'] = true;
    }

    $data['title']     = 'Edit Admin';
    $data['countries'] = $countries;
    $data['admin_countries'] = $admin_countries;
    $data['admin_user']     = $admin;

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/admins-edit', $data);
    $this->load->view('templates/footer');
  }

  public function block($id) {
      $this->db->set('is_active', 0, false);
      $this->db->where('id', $id);
      $this->db->update('auth_user');

      redirect('admin/admins/');
  }

  public function approve($id) {
      $this->db->set('is_active', 1, false);
      $this->db->where('id', $id);
      $this->db->update('auth_user');

      redirect('admin/admins/');
  }

  public function remove_country($id_user, $id_country) {

    $this->db->delete('admins_countries', array('id_user' => $id_user, 'id_country' => $id_country));

    redirect('admin/admins/edit/'.$id_user);
  }

  public function remove_countries($id_user) {

    $this->db->delete('admins_countries', array('id_user' => $id_user ));

    redirect('admin/admins/edit/'.$id_user);
  }

  public function remove_rights($id_user){
     $user = $this->users->get($id_user);
     if($user['level'] >= 2){
        $level = $this->users->computeLevel($id_user);
        $this->users->updateLevel($id_user, $level);
        $this->db->delete('admins_countries', array('id_user' => $id_user ));
     }
     redirect('admin/admins/');
  }
}
