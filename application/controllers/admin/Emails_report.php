<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emails_report extends CI_Controller {

    public function __construct()
  {
    parent::__construct();

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }
    
    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }

  }

  public function index() {
      $data['admin'] = $this->users->get($this->session->userdata('id'));
      $id_user = $this->session->userdata('id');
      $user = $data['admin'];

      // limit results by admin countries
      if ($user['level'] == 2) {
        $countries = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}")->result();
      } elseif ($user['level'] == 3) {
        $countries = $this->db->query("SELECT country_id AS id_country FROM country_t")->result();
      }

      $ids_countries = array();
      foreach ($countries as $c) {
          $ids_countries[] = $c->id_country;
      }
      $ids_countries = implode(', ', $ids_countries);

      $results = $this->db->query("SELECT count(*) AS receivers, r.id, r.id_sender, r.created, r.subject FROM emails_users AS eu INNER JOIN auth_user AS u ON u.id=eu.id_user INNER JOIN emails_report AS r ON r.id=eu.id_email WHERE u.id_country IN ({$ids_countries}) GROUP BY r.id ORDER BY r.created DESC")->result();

      $emails = array();

      foreach ($results as $r) {
          $email = false;
          $email = clone $r;
          $sender = $this->db->query("SELECT first_name, last_name FROM auth_user WHERE id={$r->id_sender}")->row();
          $email->sender = $sender->first_name." ".$sender->last_name;
          $emails[] = $email;
      }

      $data['title']  = "Emails Report";
      $data['emails'] = $emails;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/emails_report-list', $data);
      $this->load->view('templates/footer');
  }


  public function view($id) {
      $data['admin'] = $this->users->get($this->session->userdata('id'));
      $id_user = $this->session->userdata('id');
      $user = $data['admin'];

      // limit results by admin countries
      if ($user['level'] == 2) {
        $countries = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}")->result();
      } elseif ($user['level'] == 3) {
        $countries = $this->db->query("SELECT country_id AS id_country FROM country_t")->result();
      }

      $ids_countries = array();
      foreach ($countries as $c) {
          $ids_countries[] = $c->id_country;
      }
      $ids_countries = implode(', ', $ids_countries);


      $email = $this->db->query("SELECT r.*, u.first_name, u.last_name FROM emails_report AS r INNER JOIN auth_user AS u ON u.id=r.id_sender WHERE r.id={$id}")->row();
      $receivers = $this->db->query("SELECT * FROM emails_users AS eu INNER JOIN auth_user AS u ON u.id=eu.id_user WHERE eu.id_email={$id} AND u.id_country IN ({$ids_countries})")->result();


      $data['title']  = "Emails Report";
      $data['email'] = $email;
      $data['receivers'] = $receivers;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/emails_report-view', $data);
      $this->load->view('templates/footer');
  }

}
