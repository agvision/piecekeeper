<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BlogPosts extends CI_Controller {

    public function __construct()
  {
    parent::__construct();

    $this->load->model('admin_notification', 'AdminNotification');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }

    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }
  }

public function index()
  {
    $data['admin'] = $this->users->get($this->session->userdata('id'));
    $this->load->model('blog_entry', 'blog');

    $data['title'] = 'Blog Admin';
    $data['entries'] = $this->blog->getAll();

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/blog-list', $data);
    $this->load->view('templates/footer');
  }


  public function post($id='', $notification_id=false)
  {
      $data['admin'] = $this->users->get($this->session->userdata('id'));
      if($notification_id) {
          $this->AdminNotification->setInactive($notification_id);
      }

    $this->load->model('blog_entry', 'blog');

    $user    = $data['admin'];
    $id_user = $this->session->userdata('id');

    // limit results by admin countries
    if ($user['level'] == 2) {
      $countries = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}")->result();
    } elseif ($user['level'] == 3) {
      $countries = $this->db->query("SELECT country_id AS id_country FROM country_t")->result();
    }

    $ids_countries = array();
    foreach ($countries as $c) {
        $ids_countries[] = $c->id_country;
    }
    $ids_countries = implode(', ', $ids_countries);

    $data['countries'] = $this->db->query("SELECT * FROM country_t WHERE country_id IN ({$ids_countries})")->result();

    if ($id != '') {
      $data['entry'] = $this->blog->get($id);
      $data['entry']['youtube_embed'] = urlencode($data['entry']['youtube_embed']);
      $data['user'] = $this->users->get($data['entry']['id_user']);
      $data['title'] = 'Edit Blog Entry';
    } else {
      $data['entry'] = array(
        'id' => '',
        'name' => '',
        'body' => '',
        'country' => '',
        'short_description' => '',
        'image' => '',
        'display' => '',
        'level_1' => '',
        'level_2' => '',
        'level_3' => '',
        'youtube_embed' => '',
      );
      $data['title'] = 'Add Blog Entry';
    }

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/blog-entry', $data);
    $this->load->view('templates/footer');
  }

  public function save()
  {
    $data['admin'] = $this->users->get($this->session->userdata('id'));
    $this->load->model('blog_entry', 'blog');

    $id      = $this->input->post('id');
    $name    = $this->input->post('name');
    $body    = $this->input->post('body');
    $country = $this->input->post('country');
    $short_description = $this->input->post('short_description');
    $youtube_embed = $this->input->post('youtube_embed');

    $img = $this->blog->uploadImage($_FILES, 'image');
    if(!$img && isset($_FILES['image']) && strlen($_FILES['image']['name']) > 0){
        $this->session->set_userdata("blog_errors", "The image has to be png, jpg or gif with a maximum size of 4MB.");
    } else {
        $this->session->set_userdata("blog_errors", "");
    }

    $fields = array(
      'name' => $name,
      'body' => $body,
      'country' => $country,
      'aproved' => 1,
      'short_description' => $short_description,
      'youtube_embed' => urldecode($youtube_embed)
    );

    $fields['display'] = 0;

    if ($this->input->post('display') == 1) {
        if ($this->input->post('level_1')) {
          $this->db->where(array('display' => 1, 'level_1' => 1));
          $this->db->update('blog_entry', array('level_1' => 0));
        }
        if ($this->input->post('level_2')) {
          $this->db->where(array('display' => 1, 'level_2' => 1));
          $this->db->update('blog_entry', array('level_2' => 0));
        }
        if ($this->input->post('level_3')) {
          $this->db->where(array('display' => 1, 'level_3' => 1));
          $this->db->update('blog_entry', array('level_3' => 0));
        }
        $fields['display'] = 1;
    }

    if ($this->input->post('display') == 2) {
        if ($this->input->post('level_1')) {
          $this->db->where(array('display' => 2, 'level_1' => 1));
          $this->db->update('blog_entry', array('level_1' => 0));
        }
        if ($this->input->post('level_2')) {
          $this->db->where(array('display' => 2, 'level_2' => 1));
          $this->db->update('blog_entry', array('level_2' => 0));
        }
        if ($this->input->post('level_3')) {
          $this->db->where(array('display' => 2, 'level_3' => 1));
          $this->db->update('blog_entry', array('level_3' => 0));
        }
        $fields['display'] = 2;
    }

    if ($this->input->post('display') == 3) {
        if ($this->input->post('level_1')) {
          $this->db->where(array('display' => 3, 'level_1' => 1));
          $this->db->update('blog_entry', array('level_1' => 0));
        }
        if ($this->input->post('level_2')) {
          $this->db->where(array('display' => 3, 'level_2' => 1));
          $this->db->update('blog_entry', array('level_2' => 0));
        }
        if ($this->input->post('level_3')) {
          $this->db->where(array('display' => 3, 'level_3' => 1));
          $this->db->update('blog_entry', array('level_3' => 0));
        }
        $fields['display'] = 3;
    }

    if ($this->input->post('level_1')) {
        $fields['level_1'] = 1;
    } else {
        $fields['level_1'] = 0;
    }

    if ($this->input->post('level_2')) {
        $fields['level_2'] = 1;
    } else {
        $fields['level_2'] = 0;
    }

    if ($this->input->post('level_3')) {
        $fields['level_3'] = 1;
    } else {
        $fields['level_3'] = 0;
    }


    if($img) {
      $fields['image'] = $img;
    }

    if ($id && $id != '') {
      $this->blog->update_entry($id, $fields);
    } else {
      $this->blog->add_new_entry($fields);
      $id = $this->db->insert_id();

      $this->load->model('notification');
      $user_notification = $this->notification;
      $user_notification->id_user = -1;
      $user_notification->type = 5;
      $user_notification->param = $id;
      $user_notification->insert();

    }

    redirect('admin/blogPosts/' . ($id ? "post/$id" : ""));
  }

  public function delete($id)
  {
    $this->load->model('blog_entry', 'blog');

    $this->blog->delete_entry($id);

    redirect('admin/blogPosts');
  }

  public function change_status($id) {
      $this->load->model('entry', 'BlogEntry');
      $this->BlogEntry->changeBlogEntryStatus($id);
      $this->AdminNotification->logHandling($this->session->userdata('id'), $id, 'blog_entry');
      redirect('admin/blogPosts' . ($id ? "/post/$id" : ""));
  }

 }
