<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Coupons extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->load->helper('url');

    $this->load->model('admin_notification', 'AdminNotification');
    $this->load->model('notification', 'Notification');
    $this->load->model('coupon_model', 'Coupon');
    $this->load->model('users', 'User');
    $this->load->model("Site_config");

    if (!$this->authentication->isLogged()) {
        $this->authentication->saveTheLastUrl();
        redirect('/auth/login');
    }

    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }
  }

  public function reset_batch($batch, $id_user = false)
  {

    $this->load->model('scratch_coupon_model', 'ScratchCoupon');

    if( $this->ScratchCoupon->countUsedCoupons( $batch ) == 0) {

        /* reset batch */

        $this->db->where(array(
                'batch'  => $batch
            ));

        $this->db->set('id_user', '0');
        $this->db->set('active', 0);
        $this->db->set('created', 0);
        $this->db->set('activated', "'0000-00-00 00:00:00'", false);

        $this->db->update('scratch_batches');
    }

    if ($id_user) {
        redirect('admin/user/view/'.$id_user.'#tabs-3');
    }

    redirect('admin/coupons');
  }

 public function index($id_notification = false) {

    $data['admin'] = $this->users->get($this->session->userdata('id'));

    // if is Admin get user by Assigned Countries
    $id_user = $this->session->userdata('id');
    $user    = $this->User->get($id_user);
    $query     = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}");
    $countries = $query->result();
    $ids_countries = array();
    foreach ($countries as $c) {
        $ids_countries[] = $c->id_country;
    }
    $ids_countries = implode(', ', $ids_countries);

    $data['title'] = 'Coupons';
    if ($user['level'] == 2) {
        $data['coupons'] = $this->Coupon->getAllCreated($ids_countries);
    } else {
        $data['coupons'] = $this->Coupon->getAllCreated();
    }

    if ($id_notification) {
        $this->AdminNotification->setInactive($id_notification);
    }

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/coupons_requests-list', $data);
    $this->load->view('templates/footer');
  }


  public function add($id_user) {

      if(empty($id_user))
      {
        redirect('/admin/coupons');
      }

      $data['admin'] = $this->users->get($this->session->userdata('id'));
      $settings = $this->Site_config->getGeneralSettings();

      $this->User->checkAdminUserRights($id_user, 'admin/coupons/');

      $user = $this->User->get($id_user);

      if (isset($_POST['trigger']) && $_POST['trigger'] == 'discount_20') {

        // $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('code_single', 'Coupon Code', 'required|max_length[30]|is_unique[coupons.code]|alpha_numeric');

        if ($this->form_validation->run()) {
            // $code = $this->Coupon->randomString(6);
            $code = $this->input->post('code_single');
            $country = $this->input->post('country');
            $id_coupon = $this->Coupon->createSingleUse($id_user, $code, $country, true);
        }

      }

      if (isset($_POST['trigger']) && $_POST['trigger'] == '48_hours') {

          $this->form_validation->set_rules('code', 'Coupon Code', 'required|max_length[30]|is_unique[coupons.code]|alpha_numeric');
          $this->form_validation->set_rules('valability', 'Valability', 'required|integer');
          if (isset($_POST['amount']) && strlen($_POST['amount']) > 0) {
              $this->form_validation->set_rules('amount', 'Amount', 'required');
              $this->form_validation->set_rules('currency', 'Currency', 'required');
          } else {
              $this->form_validation->set_rules('percent', 'Percent', 'required|integer');
          }

          // $this->form_validation->set_rules('country', 'Country', 'required');

          if ($this->form_validation->run()) {
              $code         = ($this->input->post('code') == NULL) ? FALSE : $this->input->post('code');
              $percent      = ($this->input->post('percent') == NULL) ? FALSE : $this->input->post('percent');
              $amount       = ($this->input->post('amount') == NULL) ? FALSE : $this->input->post('amount');
              $currency     = ($this->input->post('currency') == NULL) ? FALSE : $this->input->post('currency');
              $u_limit      = ($this->input->post('u_limit') == NULL) ? FALSE : $this->input->post('u_limit');
              $p_limit      = ($this->input->post('p_limit') == NULL) ? FALSE : $this->input->post('p_limit');
              $freeshipping = ($this->input->post('freeshipping') == NULL) ? FALSE : $this->input->post('freeshipping');
              $country      = ($this->input->post('country') == NULL) ? FALSE : $this->input->post('country');
              $link         = ($this->input->post('link') == NULL) ? FALSE : $this->input->post('link');
              $valability         = $this->input->post('valability');

              $id_coupon = $this->Coupon->createMultipleUse($id_user, $code, $percent, $amount, $currency, $country, $link, $u_limit, $p_limit, $freeshipping, true, $valability);
          }
      }

      if (validation_errors()) {
          $this->session->set_flashdata(
            'error',
            validation_errors("<div class='alert-box error form-alert'>", "</div>")
          );

          redirect('/admin/coupons/add/'.$id_user, $data);
      }

      if (isset($_POST['trigger'])) {
          redirect('/admin/user/view/'.$id_user.'#tabs-3', $data);
      }

      $data['settings'] = $settings;

      $data['title'] = 'Add Coupon';
      $data['user']  = $user;
      $data['countries'] = $this->db->query("SELECT iso2 as code, short_name as name from country_t")->result();
      $data['currencies'] = $this->db->query("SELECT code FROM currencies")->result();

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/coupon_add', $data);
      $this->load->view('templates/footer');

  }


  public function edit($id_coupon){
      $data['admin'] = $this->users->get($this->session->userdata('id'));

      $coupon = $this->db->query("SELECT * FROM coupons WHERE id={$id_coupon}")->row();
      $this->User->checkAdminUserRights($coupon->id_creator, 'admin/coupons/');

      if (isset($_POST['trigger']) && $_POST['trigger']=='save') {
          unset($_POST['trigger']);

          $this->form_validation->set_rules('code', 'Coupon Code', 'required');
          $this->form_validation->set_rules('valability', 'Valability', 'required');
          if (isset($_POST['amount']) && strlen($_POST['amount']) > 0) {
              $this->form_validation->set_rules('amount', 'Amount', 'required');
              $this->form_validation->set_rules('currency', 'Currency', 'required');
          } else {
              $this->form_validation->set_rules('percent', 'Percent', 'required');
          }

          // $this->form_validation->set_rules('country', 'Country', 'required');

          if ($this->form_validation->run()) {
              $post = $this->input->post();

              if (!isset($post['country'])) {
                  $post['country'] = '';
              }

              $this->db->where('id', $id_coupon);
              $this->db->update('coupons', $post);

              redirect('/admin/coupons/');
          }

      }

      if (validation_errors()) {
          $this->session->set_flashdata(
            'error',
            validation_errors("<div class='alert-box error form-alert'>", "</div>")
          );

          redirect('/admin/coupons/edit/'.$id_coupon);
      }

      $data['title']  = 'Edit Coupon';
      $data['coupon'] = $coupon;

      $data['currencies'] = $this->db->query("SELECT code FROM currencies")->result();
      $data['countries']  = $this->db->query("SELECT * FROM country_t")->result();

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/coupon_edit', $data);
      $this->load->view('templates/footer');
  }


  public function view($id) {
      $data['admin'] = $this->users->get($this->session->userdata('id'));
      $sql = "SELECT o.order_id, o.total, o.created, u.first_name, u.last_name, u.id FROM coupons AS c INNER JOIN orders_coupons AS oc ON oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id INNER JOIN auth_user AS u ON u.id=c.id_creator WHERE c.id={$id}";

      $qry    = $this->db->query($sql);
      $result = $qry->result();

      $coupon = $this->db->query("SELECT * FROM coupons WHERE id={$id}")->row();
      $this->User->checkAdminUserRights($coupon->id_creator, 'admin/coupons/');

      $data['orders'] = $result;
      $data['coupon'] = $coupon;
      $data['title']  = 'Orders';

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/coupon_orders', $data);
      $this->load->view('templates/footer');
  }


  public function approve($id, $id_user = false){
    $result = $this->db->query("SELECT id_creator FROM coupons WHERE id={$id}")->row();
    $this->User->checkAdminUserRights($result->id_creator, 'admin/coupons/');

    $this->Coupon->approve($id);

    $this->Notification->id_user = $result->id_creator;
    $this->Notification->type = 14;
    $this->Notification->insert();

    $this->AdminNotification->logHandling($this->session->userdata('id'), $id, 'coupons');

    if ($id_user) {
        redirect('admin/user/view/'.$id_user.'#tabs-3');
    }
    redirect('admin/coupons');
  }


  public function multipleApprove() {
      $requests = $this->input->post('requests');
      foreach ($requests as $r) {
          $this->Coupon->approve($r);
          $result = $this->db->query("SELECT id_creator FROM coupons WHERE id={$r}")->row();

          $this->Notification->id_user = $result->id_creator;
          $this->Notification->type = 14;
          $this->Notification->insert();

          $this->AdminNotification->logHandling($this->session->userdata('id'), $r, 'coupons');
      }

      redirect('admin/coupons');
  }


  public function delete($id, $id_user = false){
    $result = $this->db->query("SELECT id_creator FROM coupons WHERE id={$id}")->row();
    $this->User->checkAdminUserRights($result->id_creator, 'admin/coupons/');

    if ($this->input->post('reason')) {
        $reason = $this->input->post('reason');

        $this->db->where('id', $id);
        $this->db->set('reason', $reason);
        $this->db->update('coupons');
    }

    $this->Coupon->delete($id);
    $this->AdminNotification->logHandling($this->session->userdata('id'), $id, 'coupons');

    if ($id_user) {
        redirect('admin/user/view/'.$id_user.'#tabs-3');
    }
    redirect('admin/coupons');
  }


  public function disable($id, $id_user) {
      $result = $this->db->query("SELECT id_creator FROM coupons WHERE id={$id}")->row();
      $this->User->checkAdminUserRights($result->id_creator, 'admin/');

      $this->Coupon->disable($id);
      redirect('admin/user/view/'.$id_user.'#tabs-3');
  }


  public function enable($id, $id_user) {
      $result = $this->db->query("SELECT id_creator FROM coupons WHERE id={$id}")->row();
      $this->User->checkAdminUserRights($result->id_creator, 'admin/');

      $this->Coupon->enable($id);
      redirect('admin/user/view/'.$id_user.'#tabs-3');
  }

}
