<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('admin_notification', 'AdminNotification');
    $this->load->model('event_model', 'events');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }

    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }
  }

 public function index() {
    $data['admin'] = $this->users->get($this->session->userdata('id'));

    $data['title'] = 'Events Admin';
    $data['entries'] = $this->db->query("SELECT * FROM event_entry ORDER BY creation_date DESC")->result_array();

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/event-list', $data);
    $this->load->view('templates/footer');
  }

  public function edit($id='', $notification_id=false)
  {
      $data['admin'] = $this->users->get($this->session->userdata('id'));

     if($notification_id) {
          $this->AdminNotification->setInactive($notification_id);
      }

    if ($id != '') {
      $data['entry'] = $this->events->get($id);
      $data['user'] = $this->users->get($data['entry']['id_user']);
    } else {
      $data['entry'] = array(
        'id' => '',
        'name' => '',
        'body' => '',
        'date' => '',
        'short_description' => '',
        'image' => ''
      );
    }

    $data['title'] = 'Add / Edit Event';

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/event-entry', $data);
    $this->load->view('templates/footer');
  }

  public function save()
  {

    $id   = $this->input->post('id');
    $name = $this->input->post('name');
    $body = $this->input->post('body');
    $date = $this->input->post('date');
    $short_description = $this->input->post('date');

    $fields = array(
      'name' => $name,
      'body' => $body,
      'date' => $date,
      'aproved' => 1,
      'short_description' => $short_description
    );

    $img = $this->events->uploadImage($_FILES, 'image');
    if( $img ) {
      $fields['image'] = $img;
    }


    if ($id && $id != '') {
      $this->events->update_entry($id, $fields);
    } else {
      $this->events->add_new_entry($fields);
      $id = $this->db->insert_id();

      $this->load->model('notification');
      $user_notification = $this->notification;
      $user_notification->id_user = -1;
      $user_notification->type = 6;
      $user_notification->param = $id;
      $user_notification->insert();
    }

    redirect('admin/event');
  }

  public function delete($id)
  {

    $this->events->delete_entry($id);

    redirect('admin/event');
  }

  public function change_status($id) {


      $this->events->changeStatus($id);
      $this->AdminNotification->logHandling($this->session->userdata('id'), $id, 'event_entry');
      redirect("admin/event/edit/" . $id);
  }

}
