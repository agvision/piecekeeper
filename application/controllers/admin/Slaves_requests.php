<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slaves_requests extends CI_Controller {

    public function __construct()
  {
    parent::__construct();

    $this->load->model('admin_notification', 'AdminNotification');
    $this->load->model('users', 'User');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }

    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }

  }

  public function index() {
    $data['admin'] = $this->users->get($this->session->userdata('id'));

    // if is Admin get user by Assigned Countries
    $id_user = $this->session->userdata('id');
    $user    = $this->User->get($id_user);
    $query     = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}");
    $countries = $query->result();
    $ids_countries = array();
    foreach ($countries as $c) {
        $ids_countries[] = $c->id_country;
    }
    $ids_countries = implode(', ', $ids_countries);

    $only = ($this->input->get("only") == NULL) ? FALSE : $this->input->get("only");
    if($only===false) {
        $onlystr = "";
    } else {
        $onlystr = " WHERE p.aproved=$only";
        $data['only'] = $only;
    }

    if ($user['level'] == 2) {
        if ($only === false) {
            $data['requests'] = $this->db->query("SELECT p.id, p.reason, p.created,p.slaves,p.aproved, u.first_name, u.last_name, u.id as user_id FROM slaves_requests as p ".
                                " INNER JOIN auth_user as u ON p.id_user=u.id {$onlystr} WHERE u.id_country IN ($ids_countries) ORDER BY p.created DESC ")->result();
        } else {
            $data['requests'] = $this->db->query("SELECT p.id, p.reason, p.created,p.slaves,p.aproved, u.first_name, u.last_name, u.id as user_id FROM slaves_requests as p ".
                                " INNER JOIN auth_user as u ON p.id_user=u.id {$onlystr} AND u.id_country IN ($ids_countries) ORDER BY p.created DESC ")->result();
        }

    } else {
        $data['requests'] = $this->db->query("SELECT p.id, p.reason, p.created,p.slaves,p.aproved, u.first_name, u.last_name, u.id as user_id FROM slaves_requests as p ".
                                " INNER JOIN auth_user as u ON p.id_user=u.id {$onlystr} ORDER BY p.created DESC ")->result();
    }


    $data['title'] = "Slave Requests";

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/slaves_requests-list', $data);
    $this->load->view('templates/footer');

  }


    public function approve($id) {

        $request = $this->db->query("SELECT * FROM slaves_requests WHERE id={$id}")->row();
        if(!$request ) exit;

        $this->User->checkAdminUserRights($request->id_user, 'admin/slaves_requests/');

        $this->db->where("id", $id);
        $this->db->set("aproved", '1');
        $this->db->update("slaves_requests");

        $this->db->where("id", $request->id_user);
        $this->db->set("slaves", $request->slaves . "+slaves", FALSE);
        $this->db->update("auth_user");

        $this->load->model('notification');
        $user_notification = $this->notification;
        $user_notification->id_user = $request->id_user;
        $user_notification->type = 11;
        $user_notification->param = $id;
        $user_notification->insert();

        $this->AdminNotification->logHandling($this->session->userdata('id'), $id, 'slaves_requests');

        redirect("admin/slaves_requests");
    }


    public function multipleApprove() {
        $requests = $this->input->post('requests');
        foreach ($requests as $r) {
            $request = $this->db->query("SELECT * FROM slaves_requests WHERE id={$r}")->row();
            if (!$request) {
                break;
            }
            $this->db->where("id", $r);
            $this->db->set("aproved", '1');
            $this->db->update("slaves_requests");

            $this->db->where("id", $request->id_user);
            $this->db->set("slaves", $request->slaves . "+slaves", FALSE);
            $this->db->update("auth_user");

            $this->load->model('notification');
            $user_notification = $this->notification;
            $user_notification->id_user = $request->id_user;
            $user_notification->type = 11;
            $user_notification->param = $r;
            $user_notification->insert();

            $this->AdminNotification->logHandling($this->session->userdata('id'), $r, 'slaves_requests');
        }

        redirect("admin/slaves_requests");
    }


  public function view($id, $notification_id=false) {
    $data['admin'] = $this->users->get($this->session->userdata('id'));

    $request = $this->db->query("SELECT p.id, p.reason, p.created,p.slaves, p.aproved, p.id_user, u.email, u.first_name, u.last_name, u.id as user_id, u.slaves as user_slaves, u.slave_invitations_sent FROM slaves_requests as p ".
                                "LEFT JOIN auth_user as u ON p.id_user=u.id WHERE ".
                                "p.id={$id}")->row();

    $this->User->checkAdminUserRights($request->id_user, 'admin/slaves_requests/');

    $slaves = $this->db->query("SELECT COUNT(*) AS total FROM auth_user WHERE id_master={$request->id_user} AND is_active=1 AND deleted=0")->row();
    $data['slaves'] = $slaves->total;


    $data['title'] = "Slave request from " . $request->first_name . " " . $request->last_name;
    $data['request'] = $request;

    if($notification_id) {
        $this->load->model('admin_notification', 'AdminNotification');
        $this->AdminNotification->setInactive($notification_id);
      }

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/slaves_requests-view', $data);
    $this->load->view('templates/footer');
  }

  public function reject($id) {
    $reason = ($this->input->post('reason') == NULL) ? FALSE : $this->input->post('reason');

    $this->db->set('aproved', '-1');
    $this->db->set('reason', $reason);
    $this->db->where('id', $id);
    $this->db->update('slaves_requests');

    $this->AdminNotification->logHandling($this->session->userdata('id'), $id, 'slaves_requests');

    redirect('admin/slaves_requests');
  }

}
