<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content extends CI_Controller {

    public function __construct()
  {
    parent::__construct();

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }
    
    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }

    $user = $this->users->get($this->session->userdata('id'));
    if ($user['level'] < 3) {
        redirect('admin');
    }

  }

  public function index() {
      $data['admin'] = $this->users->get($this->session->userdata('id'));
      $query  = $this->db->get('content');
      $result = $query->result();

      // $automatic = array(13, 14, 19, 10, 15, 16, 7, 6, 9, 8, 18, 17, 11);

      // $results = array();
      // $i = 0;
      // foreach ($result as $r) {
      //     $results[$i]['id']      = $r->id;
      //     $results[$i]['name']    = $r->name;
      //     $results[$i]['content'] = $r->content;

      //     if (in_array($r->id, $automatic)) {
      //         $results[$i]['automatic'] = true;
      //     } else {
      //         $results[$i]['automatic'] = false;
      //     }
      //     $i++;
      // }

      $data['title'] = 'Content';
      $data['content'] = $result;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/content-list', $data);
      $this->load->view('templates/footer');
  }

  public function add() {
    $data['admin'] = $this->users->get($this->session->userdata('id'));
    if (isset($_POST['trigger']) && $_POST['trigger'] == 'add') {

        $type = $this->input->post('type');

        $data = array(
            'name' => $this->input->post('name'),
            'body' => $this->input->post('body'),
            'type' => $type
        );

        if($type == 1){
            $data['body'] = strip_tags($data['body']);
        }

        $this->db->insert('content', $data);

        redirect('admin/content');
    }

    $data['title'] = 'Add Content';

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/content-add');
    $this->load->view('templates/footer');
  }

  public function edit($id) {
      $data['admin'] = $this->users->get($this->session->userdata('id'));
      $query    = $this->db->get_where('content', array('id' => $id));
      $content  = $query->result();

      if (isset($_POST['trigger']) && $_POST['trigger'] == 'edit') {
          $data = array(
              'name' => $this->input->post('name'),
              'body' => $this->input->post('body')
            );

          if($content[0]->type == 1){
            $data['body'] = strip_tags($data['body']);
          }

          $this->db->where('id', $id);
          $this->db->update('content', $data);

          redirect('admin/content');
      }

      $data['title']    = 'Edit Content';
      $data['content'] = $content[0];

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/content-edit', $data);
      $this->load->view('templates/footer');
  }

  public function delete($id) {
      $this->db->where('id', $id);
      $this->db->delete('content');

      redirect('admin/content');
  }

}
