<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {

    public function __construct()
  {
    parent::__construct();

    $this->load->model('admin_notification', 'AdminNotification');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }
    
    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }
  }

  public function index()
  {
    $data['admin'] = $this->users->get($this->session->userdata('id'));
    $this->load->model('pages');
    $data['title'] = 'Admin Pages';
    $data['entries'] = $this->pages->get_all_entries();

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/page-list', $data);
    $this->load->view('templates/footer');
  }

  public function page_edit($id)
  {
    $data['admin'] = $this->users->get($this->session->userdata('id'));

    $this->load->model('page_model');
    $data['title'] = 'Admin';
    $data['page'] = $this->pages->get($id);

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/page-entry', $data);
    $this->load->view('templates/footer');
  }

  public function page_save()
  {
    $this->load->model('pages');

    $id   = $this->input->post('id');
    $body = $this->input->post('body');

    $fields = array(
      'body' => $body
    );

    $this->pages->update_entry($id, $fields);

    redirect('admin/page');
  }

}
