<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('admin_notification', 'AdminNotification');
    $this->load->model('users', 'User');
    $this->load->model('social_model', 'Social');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }

    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }
  }

  public function index()
  {

    $data['admin'] = $this->users->get($this->session->userdata('id'));

    // if is Admin get user by Assigned Countries
    $id_user = $this->session->userdata('id');
    $user    = $this->User->get($id_user);

    // limit results by admin countries
    if ($user['level'] == 2) {
      $countries = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}")->result();
    } elseif ($user['level'] == 3) {
      $countries = $this->db->query("SELECT country_id AS id_country FROM country_t")->result();
    }

    $ids_countries = array();
    foreach ($countries as $c) {
        $ids_countries[] = $c->id_country;
    }
    $ids_countries = implode(', ', $ids_countries);

    $data['title'] = 'Admin';
    $id = $this->session->userdata('id');
    $data['admin'] = $this->User->get($id);

    $this->load->model("Site_config");
    $data['statistics'] = $this->Site_config->getAdminStatistics();

    $data['pending_ambasadors'] = $this->db->query("SELECT * FROM auth_user WHERE is_active=0 AND deleted=0 AND (completed = 0 OR completed = 1)  AND (pku is null OR pku = 0) ORDER BY date_joined DESC")->result();
    $data['notifications'] = $this->AdminNotification->getAll($ids_countries);

    if ($user['level'] == 2) {
      $data['statistics'] = $this->Site_config->getAdminStatistics($ids_countries);
      $data['pending_ambasadors'] = $this->db->query("SELECT * FROM auth_user WHERE is_active=0 AND deleted=0 AND (completed = 0 OR completed = 1) AND (pku is null OR pku = 0) AND id_country IN ($ids_countries) ORDER BY date_joined DESC")->result();
    }

    $data['total_social'] = $this->Social->countAllFollowers();
    $data['total_social_last24h'] = $this->Social->countLast24h();

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/index', $data);
    $this->load->view('templates/footer');
  }

}

?>
