<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email_templates extends CI_Controller {

    public function __construct()
  {
    parent::__construct();

    $this->load->model('Country');
    $this->load->model('Email_model', 'Email');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }

    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }

    $user = $this->users->get($this->session->userdata('id'));
    if ($user['level'] < 3) {
        redirect('admin');
    }

  }

  public function index() {
      $data['admin'] = $this->users->get($this->session->userdata('id'));
      $query  = $this->db->get('email_templates');
      $result = $query->result();

      $automatic = array(13, 14, 19, 10, 15, 16, 7, 6, 9, 8, 18, 17, 11, 20, 21, 22, 23, 24, 46, 48, 49, 50, 53, 54);

      $results = array();
      $i = 0;
      foreach ($result as $r) {
          $results[$i]['id']         = $r->id;
          $results[$i]['name']       = $r->name;
          $results[$i]['content']    = $r->content;
          $results[$i]['parent_id']  = $r->parent_id;

          if (in_array($r->id, $automatic)) {
              $results[$i]['automatic'] = true;
          } else {
              $results[$i]['automatic'] = false;
          }
          $i++;
      }

      usort($results, function($a, $b){
        if($a['automatic'] && !$b['automatic']){
            return -1;
        }
        if(!$a['automatic'] && $b['automatic']){
            return 1;
        }
        return 0;
      });

      $data['title'] = 'Email Templates';
      $data['templates'] = $results;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/email_templates-list', $data);
      $this->load->view('templates/footer');
  }

  public function add() {
    $data['admin'] = $this->users->get($this->session->userdata('id'));
    if (isset($_POST['trigger']) && $_POST['trigger'] == 'add') {

        $name      = $this->input->post('name');
        $content   = $this->input->post('content');
        $parent_id = ($this->input->post('parent_id') == "null") ? null : $this->input->post('parent_id');
        $countries = $this->input->post('countries');

        $data = array(
            'name'       => $name,
            'content'    => $content,
            'parent_id'  => $parent_id
        );

        $this->db->insert('email_templates', $data);
        $email_id = $this->db->insert_id();

        if(!empty($countries) && !empty($parent_id))
        {
            foreach ($countries as $key => $country)
            {
                $data = array(
                    'email_id'   => $email_id,
                    'country_id' => $countries[$key]
                );

                $this->db->insert('email_country', $data);
            }
        }

        redirect('admin/email_templates');
    }

    $data['countries']     = $this->Country->getAllCountries();
    $data['parent_emails'] = $this->Email->getParentEmails();
    $data['title']         = 'Add Template';

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/email_templates-add');
    $this->load->view('templates/footer');
  }

  public function edit($id) {
      $data['admin'] = $this->users->get($this->session->userdata('id'));
      $query    = $this->db->get_where('email_templates', array('id' => $id));
      $template = $query->result();
      $email_countries = $this->Email->getCountriesForEmail($id);

      if (isset($_POST['trigger']) && $_POST['trigger'] == 'edit') {

          $parent_id = ($this->input->post('parent_id') == "null") ? null : $this->input->post('parent_id');;
          $countries = $this->input->post('countries');

          $data = array(
              'name'      => $this->input->post('name'),
              'content'   => $this->input->post('content'),
              'parent_id' => $parent_id
            );

        $this->db->where('id', $id);
        $this->db->update('email_templates', $data);

        $new_email_countries = $this->input->post('countries');
        /**
         * If there are selected other countries, delete all the countries for this email, and insert the new ones.
         */
        if($email_countries != $new_email_countries)
        {
            // Delete existing entries.
            $this->db->where('email_id', $id);
            $this->db->delete('email_country');

            // Add new entries.
            if(!empty($new_email_countries) && !empty($parent_id))
            {
                foreach ($new_email_countries as $key => $country)
                {
                    $data = array(
                        'email_id'   => $id,
                        'country_id' => $new_email_countries[$key]
                    );

                    $this->db->insert('email_country', $data);
                }
            }
            elseif (empty($new_email_countries))
            {
                $this->db->set('parent_id', NULL);
                $this->db->where('id', $id);
                $this->db->update('email_templates');
            }
        }

        redirect('admin/email_templates');
      }

      $data['title']           = 'Edit Template';
      $data['template']        = $template[0];
      $data['countries']       = $this->Country->getAllCountries();
      $data['parent_emails']   = $this->Email->getParentEmails();
      $data['email_countries'] = $email_countries;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/email_templates-edit', $data);
      $this->load->view('templates/footer');
  }

  public function delete($id) {
      $this->db->where('id', $id);
      $this->db->delete('email_templates');

      redirect('admin/email_templates');
  }

}
