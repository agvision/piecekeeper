<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stats extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('stats_model', 'Stats');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }

		if (!$this->users->is_admin($this->session->userdata('id'))) {
			redirect('/auth');
		}
	}

	public function index() {
		$data['admin'] = $this->users->get($this->session->userdata('id'));

		$id_user = $this->session->userdata('id');
		$user    = $this->users->get($id_user);

		$data['title'] = "Statistics";

		if ($user['level'] == 3) {
			$data['filter_countries'] = $this->db->query("SELECT * FROM auth_user AS u INNER jOIN country_t AS c ON c.country_id=u.id_country GROUP BY c.country_id")->result();
			$data['filter_cities']    = $this->db->query("SELECT city AS name FROM auth_user GROUP BY city")->result();
		} elseif ($user['level'] == 2) {
			$data['filter_countries'] = $this->db->query("SELECT * FROM auth_user AS u INNER JOIN admins_countries AS ac ON ac.id_country=u.id_country INNER jOIN country_t AS c ON c.country_id=u.id_country WHERE ac.id_user={$id_user} GROUP BY c.country_id")->result();
			$data['filter_cities']    = $this->db->query("SELECT city AS name FROM auth_user AS u INNER JOIN admins_countries AS ac ON ac.id_country=u.id_country WHERE ac.id_user={$id_user} GROUP BY city")->result();
		}



		// limit results by admin countries
		if ($user['level'] == 2) {
			$countries = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}")->result();
		} elseif ($user['level'] == 3) {
			$countries = $this->db->query("SELECT country_id AS id_country FROM country_t")->result();
		}

		$ids_countries = array();
		foreach ($countries as $c) {
		    $ids_countries[] = $c->id_country;
		}
		$ids_countries = implode(', ', $ids_countries);


		$sort  = $this->input->get('sort');

		$begin = strtotime($this->input->get('begin'));
		$end   = strtotime($this->input->get('end'));

		$country = $this->input->get('country');
		$city    = $this->input->get('city');

		$frequency = $this->input->get('frequency');
		if(!$frequency){
			$frequency = "month";
		}

		switch ($sort) {

			case 'coupon':
				$sales_stats = $this->Stats->getSalesStatsByCoupon($country, $city, $begin, $end, $ids_countries);
				$commissions_stats = $this->Stats->getCommissionsStatsByCoupon($country, $city, $begin, $end, $ids_countries);
				$roi_stats = $this->Stats->getROIStatsByCoupon($country, $city, $begin, $end, $ids_countries);
				$spc_stats = $this->Stats->getSPCStats($country, $city, $begin, $end, $ids_countries); // default spc
				$sales_points_stats = $this->Stats->getSalesPointsStatsByCoupon($country, $city, $begin, $end, $ids_countries);
				$bonus_points_stats = $this->Stats->getBonusPointsStats($country, $city, $begin, $end, $ids_countries);


				$pk_stats = $this->Stats->getPKStatsByRevenue($country, $city, $begin, $end, $ids_countries);
				break;

			case 'tags':
				$sales_stats = $this->Stats->getSalesStatsByTag($country, $city, $begin, $end, $ids_countries);
				$commissions_stats = $this->Stats->getCommissionsStatsByTag($country, $city, $begin, $end, $ids_countries);
				$roi_stats = $this->Stats->getROIStatsByTag($country, $city, $begin, $end, $ids_countries);
				$spc_stats = $this->Stats->getSPCStatsByTag($country, $city, $begin, $end, $ids_countries);
				$sales_points_stats = $this->Stats->getSalesPointsStatsByTag($country, $city, $begin, $end, $ids_countries);
				$bonus_points_stats = $this->Stats->getBonusPointsStatsByTag($country, $city, $begin, $end, $ids_countries);

				$pk_stats = $this->Stats->getPKStatsByRevenue($country, $city, $begin, $end, $ids_countries);
				break;

			case 'gender':
				$sales_stats = $this->Stats->getSalesStatsByGender($country, $city, $begin, $end, $ids_countries);
				$commissions_stats = $this->Stats->getCommissionsStatsByGender($country, $city, $begin, $end, $ids_countries);
				$roi_stats = $this->Stats->getROIStatsByGender($country, $city, $begin, $end, $ids_countries);
				$spc_stats = $this->Stats->getSPCStatsByGender($country, $city, $begin, $end, $ids_countries);
				$sales_points_stats = $this->Stats->getSalesPointsStatsByGender($country, $city, $begin, $end, $ids_countries);
				$bonus_points_stats = $this->Stats->getBonusPointsStatsByGender($country, $city, $begin, $end, $ids_countries);

				$pk_stats = $this->Stats->getPKStatsByRevenue($country, $city, $begin, $end, $ids_countries);
				break;

			case 'country':
				$sales_stats = $this->Stats->getSalesStatsByCountry($country, $city, $begin, $end, $ids_countries);
				$commissions_stats = $this->Stats->getCommissionsStatsByCountry($country, $city, $begin, $end, $ids_countries);
				$roi_stats = $this->Stats->getROIStatsByCountry($country, $city, $begin, $end, $ids_countries);
				$spc_stats = $this->Stats->getSPCStatsByCountry($country, $city, $begin, $end, $ids_countries);
				$sales_points_stats = $this->Stats->getSalesPointsStatsByCountry($country, $city, $begin, $end, $ids_countries);
				$bonus_points_stats = $this->Stats->getBonusPointsStatsByCountry($country, $city, $begin, $end, $ids_countries);

				$pk_stats = $this->Stats->getPKStatsByRevenue($country, $city, $begin, $end, $ids_countries);
				break;

			case 'city':
				$sales_stats = $this->Stats->getSalesStatsByCity($country, $city, $begin, $end, $ids_countries);
				$commissions_stats = $this->Stats->getCommissionsStatsByCity($country, $city, $begin, $end, $ids_countries);
				$roi_stats = $this->Stats->getROIStatsByCity($country, $city, $begin, $end, $ids_countries);
				$spc_stats = $this->Stats->getSPCStatsByCity($country, $city, $begin, $end, $ids_countries);
				$sales_points_stats = $this->Stats->getSalesPointsStatsByCity($country, $city, $begin, $end, $ids_countries);
				$bonus_points_stats = $this->Stats->getBonusPointsStatsByCity($country, $city, $begin, $end, $ids_countries);

				$pk_stats = $this->Stats->getPKStatsByRevenue($country, $city, $begin, $end, $ids_countries);
				break;

			case 'school':
				$sales_stats = $this->Stats->getSalesStatsBySchool($country, $city, $begin, $end, $ids_countries);
				$commissions_stats = $this->Stats->getCommissionsStatsBySchool($country, $city, $begin, $end, $ids_countries);
				$roi_stats = $this->Stats->getROIStatsBySchool($country, $city, $begin, $end, $ids_countries);
				$spc_stats = $this->Stats->getSPCStatsBySchool($country, $city, $begin, $end, $ids_countries);
				$sales_points_stats = $this->Stats->getSalesPointsStatsBySchool($country, $city, $begin, $end, $ids_countries);
				$bonus_points_stats = $this->Stats->getBonusPointsStatsBySchool($country, $city, $begin, $end, $ids_countries);

				$pk_stats = $this->Stats->getPKStatsByRevenue($country, $city, $begin, $end, $ids_countries);
				break;

			case 'pk_total_sales':
				$sales_stats = $this->Stats->getSalesStats($country, $city, $begin, $end, $ids_countries);
				$commissions_stats = $this->Stats->getCommissionsStats($country, $city, $begin, $end, $ids_countries);
				$roi_stats = $this->Stats->getROIStats($country, $city, $begin, $end, $ids_countries);
				$spc_stats = $this->Stats->getSPCStats($country, $city, $begin, $end, $ids_countries);
				$sales_points_stats = $this->Stats->getSalesPointsStats($country, $city, $begin, $end, $ids_countries);
				$bonus_points_stats = $this->Stats->getBonusPointsStats($country, $city, $begin, $end, $ids_countries);

				$pk_stats = $this->Stats->getPKStatsByTotalSales($country, $city, $begin, $end, $ids_countries);
				break;

			case 'pk_revenue':
				$sales_stats = $this->Stats->getSalesStats($country, $city, $begin, $end, $ids_countries);
				$commissions_stats = $this->Stats->getCommissionsStats($country, $city, $begin, $end, $ids_countries);
				$roi_stats = $this->Stats->getROIStats($country, $city, $begin, $end, $ids_countries);
				$spc_stats = $this->Stats->getSPCStats($country, $city, $begin, $end, $ids_countries);
				$sales_points_stats = $this->Stats->getSalesPointsStats($country, $city, $begin, $end, $ids_countries);
				$bonus_points_stats = $this->Stats->getBonusPointsStats($country, $city, $begin, $end, $ids_countries);

				$pk_stats = $this->Stats->getPKStatsByRevenue($country, $city, $begin, $end, $ids_countries);
				break;

			case 'pk_bonus_points':
				$sales_stats = $this->Stats->getSalesStats($country, $city, $begin, $end, $ids_countries);
				$commissions_stats = $this->Stats->getCommissionsStats($country, $city, $begin, $end, $ids_countries);
				$roi_stats = $this->Stats->getROIStats($country, $city, $begin, $end, $ids_countries);
				$spc_stats = $this->Stats->getSPCStats($country, $city, $begin, $end, $ids_countries);
				$sales_points_stats = $this->Stats->getSalesPointsStats($country, $city, $begin, $end, $ids_countries);
				$bonus_points_stats = $this->Stats->getBonusPointsStats($country, $city, $begin, $end, $ids_countries);

				$pk_stats = $this->Stats->getPKStatsByBonusPoints($country, $city, $begin, $end, $ids_countries);
				break;

			case 'pk_sales_points':
				$sales_stats = $this->Stats->getSalesStats($country, $city, $begin, $end, $ids_countries);
				$commissions_stats = $this->Stats->getCommissionsStats($country, $city, $begin, $end, $ids_countries);
				$roi_stats = $this->Stats->getROIStats($country, $city, $begin, $end, $ids_countries);
				$spc_stats = $this->Stats->getSPCStats($country, $city, $begin, $end, $ids_countries);
				$sales_points_stats = $this->Stats->getSalesPointsStats($country, $city, $begin, $end, $ids_countries);
				$bonus_points_stats = $this->Stats->getBonusPointsStats($country, $city, $begin, $end, $ids_countries);

				$pk_stats = $this->Stats->getPKStatsBySalesPoints($country, $city, $begin, $end, $ids_countries);
				break;

			case 'pk_total_points':
				$sales_stats = $this->Stats->getSalesStats($country, $city, $begin, $end, $ids_countries);
				$commissions_stats = $this->Stats->getCommissionsStats($country, $city, $begin, $end, $ids_countries);
				$roi_stats = $this->Stats->getROIStats($country, $city, $begin, $end, $ids_countries);
				$spc_stats = $this->Stats->getSPCStats($country, $city, $begin, $end, $ids_countries);
				$sales_points_stats = $this->Stats->getSalesPointsStats($country, $city, $begin, $end, $ids_countries);
				$bonus_points_stats = $this->Stats->getBonusPointsStats($country, $city, $begin, $end, $ids_countries);

				$pk_stats = $this->Stats->getPKStatsByTotalPoints($country, $city, $begin, $end, $ids_countries);
				break;

			case 'pk_commission':
				$sales_stats = $this->Stats->getSalesStats($country, $city, $begin, $end, $ids_countries);
				$commissions_stats = $this->Stats->getCommissionsStats($country, $city, $begin, $end, $ids_countries);
				$roi_stats = $this->Stats->getROIStats($country, $city, $begin, $end, $ids_countries);
				$spc_stats = $this->Stats->getSPCStats($country, $city, $begin, $end, $ids_countries);
				$sales_points_stats = $this->Stats->getSalesPointsStats($country, $city, $begin, $end, $ids_countries);
				$bonus_points_stats = $this->Stats->getBonusPointsStats($country, $city, $begin, $end, $ids_countries);

				$pk_stats = $this->Stats->getPKStatsByCommission($country, $city, $begin, $end, $ids_countries);
				break;

				case 'scratch':
				$sales_stats = $this->Stats->getSalesStats($country, $city, $begin, $end, $ids_countries);
				$commissions_stats = $this->Stats->getCommissionsStats($country, $city, $begin, $end, $ids_countries);
				$roi_stats = $this->Stats->getROIStats($country, $city, $begin, $end, $ids_countries);
				$spc_stats = $this->Stats->getSPCStats($country, $city, $begin, $end, $ids_countries);
				$sales_points_stats = $this->Stats->getSalesPointsStats($country, $city, $begin, $end, $ids_countries);
				$bonus_points_stats = $this->Stats->getBonusPointsStats($country, $city, $begin, $end, $ids_countries);

				$pk_stats = $this->Stats->getPKStatsByCommission($country, $city, $begin, $end, $ids_countries);



				break;
			case 'total':
			default:
				$sales_stats = $this->Stats->getSalesStats($country, $city, $begin, $end, $ids_countries, $frequency);
				$commissions_stats = $this->Stats->getCommissionsStats($country, $city, $begin, $end, $ids_countries, $frequency);
				$roi_stats = $this->Stats->getROIStats($country, $city, $begin, $end, $ids_countries, $frequency);
				$spc_stats = $this->Stats->getSPCStats($country, $city, $begin, $end, $ids_countries);
				$sales_points_stats = $this->Stats->getSalesPointsStats($country, $city, $begin, $end, $ids_countries, $frequency);
				$bonus_points_stats = $this->Stats->getBonusPointsStats($country, $city, $begin, $end, $ids_countries, $frequency);

				$pk_stats = $this->Stats->getPKStatsByRevenue($country, $city, $begin, $end, $ids_countries);
				break;
		}

		// Scratch coupons stats
		if(!isset($_GET['scratch_action']))
		{
			$scratch_action = "scratch_revenue";
			$_GET['scratch_action'] = "scratch_revenue";
		}
		else
		{
			$scratch_action = $this->input->get('scratch_action');
		}
		list($scratch_stats, $scratch_total) = $this->Stats->getScratchStats($country, $city, $begin, $end, $ids_countries, $scratch_action, $sales_stats['total']);
		$data['scratch'] = $scratch_stats;
		$data['scratch_total'] = $scratch_total;

		switch($scratch_action)
		{
			case "scratch_revenue":
			$data['chart_scratch_stats']['total'] = $scratch_total;
			$data['chart_scratch_stats']['graph_title'] = "Revenue for Scratch Coupons";
			$data['chart_scratch_stats']['y_axis_label'] = "Revenue ($)";
			$data['chart_scratch_stats']['title'] = "Revenue";
			$data['chart_scratch_stats']['y_data'] = "";
			$data['chart_scratch_stats']['x_data'] = "";
			$data['chart_scratch_stats']['sign'] = "$";
			arsort($scratch_stats);
			$max_show = 20;
			foreach($scratch_stats as $batch => $revenue)
			{
				$data['chart_scratch_stats']['y_data'] .= $revenue . ", ";
				$data['chart_scratch_stats']['x_data'] .= "'".$batch . "', ";
				$max_show--;
				if($max_show == 0) break;
			}
			$data['chart_scratch_stats']['y_data'] = rtrim($data['chart_scratch_stats']['y_data'], ", ");
			$data['chart_scratch_stats']['x_data'] = rtrim($data['chart_scratch_stats']['x_data'], ", ");

			break;
			case "scratch_sales_percentage":
			$data['chart_scratch_stats']['total'] = number_format($scratch_total, 4);
			$data['chart_scratch_stats']['graph_title'] = "Percentage of Scratch Coupons from Sales";
			$data['chart_scratch_stats']['y_axis_label'] = "Percentage (%)";
			$data['chart_scratch_stats']['title'] = "Percentage";
			$data['chart_scratch_stats']['y_data'] = "";
			$data['chart_scratch_stats']['x_data'] = "";
			$data['chart_scratch_stats']['sign'] = "%";
			$data['chart_scratch_stats']['decimals'] = 4;

			arsort($scratch_stats);
			$max_show = 20;
			foreach($scratch_stats as $batch => $percent)
			{

				$data['chart_scratch_stats']['y_data'] .= number_format($percent, 4) . ", ";
				$data['chart_scratch_stats']['x_data'] .= "'".$batch . "', ";
				$max_show--;
				if($max_show == 0) break;
			}
			$data['chart_scratch_stats']['y_data'] = rtrim($data['chart_scratch_stats']['y_data'], ", ");
			$data['chart_scratch_stats']['x_data'] = rtrim($data['chart_scratch_stats']['x_data'], ", ");

			break;
			case "scratch_piecekeepers":
			$data['chart_scratch_stats']['total'] = number_format($scratch_total, 4);
			$data['chart_scratch_stats']['graph_title'] = "Best Piecekeepers using Scratch Coupons";
			$data['chart_scratch_stats']['y_axis_label'] = "Revenue ($)";
			$data['chart_scratch_stats']['title'] = "Revenue";
			$data['chart_scratch_stats']['y_data'] = "";
			$data['chart_scratch_stats']['x_data'] = "";
			$data['chart_scratch_stats']['sign'] = "$";
			//$data['chart_scratch_stats']['decimals'] = 2;

			arsort($scratch_stats);
			$max_show = 20;
			foreach($scratch_stats as $user => $revenue)
			{

				$data['chart_scratch_stats']['y_data'] .= $revenue. ", ";
				$data['chart_scratch_stats']['x_data'] .= "'".$user . "', ";
				$max_show--;
				if($max_show == 0) break;
			}
			$data['chart_scratch_stats']['y_data'] = rtrim($data['chart_scratch_stats']['y_data'], ", ");
			$data['chart_scratch_stats']['x_data'] = rtrim($data['chart_scratch_stats']['x_data'], ", ");

			break;

		}

		$max_results = 31;


		$data['commissions_stats'] = $commissions_stats;
		$data['roi_stats']         = $roi_stats;
		$data['spc_stats']         = $spc_stats;

		$data['total_sales_stats'] = $sales_stats;
		$data['chart_sales_stats']    = array();
		$data['sort'] = $sort;

		$data['frequency'] = $frequency;

		$data['total_sales_points_stats'] = $sales_points_stats;
		$data['chart_sales_points_stats'] = array();

		$data['total_bonus_points_stats'] = $bonus_points_stats;
		$data['chart_bonus_points_stats'] = array();

		$data['pk_stats'] = $pk_stats;
		$data['chart_pk_stats'] = array();


		// bild array for charts
		$data['chart_pk_stats']['total'] = $pk_stats['total'];
		$data['chart_pk_stats']['graph_title'] = $pk_stats['graph_title'];
		$data['chart_pk_stats']['y_axis_label'] = $pk_stats['y_axis_label'];

		if (isset($pk_stats['keys'])) {
			foreach ($pk_stats['keys'] as $key => $value) {
				$data['chart_pk_stats']['keys'][$key] = $pk_stats['keys'][$key];
				$data['chart_pk_stats']['values'][$key] = $pk_stats['values'][$key];
			}
		}


		// bild array for charts
		$data['chart_bonus_points_stats']['total'] = $bonus_points_stats['total'];
		$data['chart_bonus_points_stats']['graph_title'] = $bonus_points_stats['graph_title'];
		$data['chart_bonus_points_stats']['y_axis_label'] = $bonus_points_stats['y_axis_label'];

		if (isset($bonus_points_stats['keys'])) {
			foreach ($bonus_points_stats['keys'] as $key => $value) {
				$data['chart_bonus_points_stats']['keys'][$key] = $bonus_points_stats['keys'][$key];
				$data['chart_bonus_points_stats']['values'][$key] = $bonus_points_stats['values'][$key];
				if (count($data['chart_bonus_points_stats']['keys']) == $max_results) {
					break;
				}
			}
		}


		// bild array for charts
		$data['chart_sales_points_stats']['total'] = $sales_points_stats['total'];
		$data['chart_sales_points_stats']['graph_title'] = $sales_points_stats['graph_title'];
		$data['chart_sales_points_stats']['y_axis_label'] = $sales_points_stats['y_axis_label'];

		if (isset($sales_points_stats['keys'])) {
			foreach ($sales_points_stats['keys'] as $key => $value) {
				$data['chart_sales_points_stats']['keys'][$key] = $sales_points_stats['keys'][$key];
				$data['chart_sales_points_stats']['values'][$key] = $sales_points_stats['values'][$key];
				if (count($data['chart_sales_points_stats']['keys']) == $max_results) {
					break;
				}
			}
		}


		// bild array for charts
		$data['chart_sales_stats']['total'] = $sales_stats['total'];
		$data['chart_sales_stats']['graph_title'] = $sales_stats['graph_title'];
		$data['chart_sales_stats']['y_axis_label'] = $sales_stats['y_axis_label'];

		if (isset($sales_stats['keys'])) {
			foreach ($sales_stats['keys'] as $key => $value) {
				$data['chart_sales_stats']['keys'][$key] = $sales_stats['keys'][$key];
				$data['chart_sales_stats']['values'][$key] = $sales_stats['values'][$key];
				if (count($data['chart_sales_stats']['keys']) == $max_results) {
					break;
				}
			}
		}


		// bild array for charts
		$data['chart_commissions_stats']['total'] = $commissions_stats['total'];
		$data['chart_commissions_stats']['graph_title'] = $commissions_stats['graph_title'];
		$data['chart_commissions_stats']['y_axis_label'] = $commissions_stats['y_axis_label'];

		if (isset($commissions_stats['keys'])) {
			foreach ($commissions_stats['keys'] as $key => $value) {
				$data['chart_commissions_stats']['keys'][$key] = $commissions_stats['keys'][$key];
				$data['chart_commissions_stats']['values'][$key] = $commissions_stats['values'][$key];
				if (count($data['chart_commissions_stats']['keys']) == $max_results) {
					break;
				}
			}
		}


		// bild array for charts
		$data['chart_roi_stats']['commission_total'] = $roi_stats['commission_total'];
		$data['chart_roi_stats']['sales_total'] = $roi_stats['sales_total'];
		$data['chart_roi_stats']['graph_title'] = $roi_stats['graph_title'];
		$data['chart_roi_stats']['y_axis_label'] = $roi_stats['y_axis_label'];

		if (isset($roi_stats['keys'])) {
			foreach ($roi_stats['keys'] as $key => $value) {
				if (!isset($roi_stats['keys'][$key])) {
					$roi_stats['keys'][$key] = "0.00";
				}
				if (!isset($roi_stats['commission_values'][$key])) {
					$roi_stats['commission_values'][$key] = "0.00";
				}
				if (!isset($roi_stats['sales_values'][$key])) {
					$roi_stats['sales_values'][$key] = "0.00";
				}

				$data['chart_roi_stats']['keys'][$key] = $roi_stats['keys'][$key];
				$data['chart_roi_stats']['commission_values'][$key] = $roi_stats['commission_values'][$key];
				$data['chart_roi_stats']['sales_values'][$key] = $roi_stats['sales_values'][$key];

				if (count($data['chart_roi_stats']['keys']) == $max_results) {
					break;
				}
			}
		}

		$this->load->view('templates/header', $data);
		$this->load->view('pages/admin/stats-index', $data);
		$this->load->view('templates/footer');
	}

}
