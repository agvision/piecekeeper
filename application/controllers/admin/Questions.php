<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Questions extends CI_Controller {

  public function __construct()  {

    parent::__construct();

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }
    
    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }

    $admin = $this->users->get($this->session->userdata('id'));
    if ($admin['level'] < 3) {
        redirect('/admin');
    }

    $this->load->model('Questions_model', 'questions');

  }

  public function add() {

    $question = $this->input->post('question');

    if( !empty( $question ) ) {

      $this->questions->add( $question );

      redirect("/admin/settings#questions");

    }


    $data['title']  = 'Add Question';
    $data['admin']  = $this->users->get($this->session->userdata('id'));
    $id_user        = $this->session->userdata('id');
    $user           = $data['admin'];

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/questions-add', $data);
    $this->load->view('templates/footer');

  }

  public function edit($id) {

    $question = $this->input->post('question');

    if( !empty( $question ) ) {

      $this->questions->update( $id, $question );

      redirect("/admin/settings#questions");

    }


    $data['admin'] = $this->users->get($this->session->userdata('id'));
    $id_user = $this->session->userdata('id');
    $user = $data['admin'];

    $q = $this->questions->get($id);

    $data['title']      = 'Edit Question';
    $data['question']     = $q->question;
    $data['question_id']  = $id;

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/questions-edit', $data);
    $this->load->view('templates/footer');

  }

  public function remove($id)
  {
      $this->questions->delete( $id );

      redirect("/admin/settings#questions");
  }

}
