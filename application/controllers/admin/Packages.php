<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Packages extends CI_Controller {

    public function __construct()
  {
    parent::__construct();

    $this->load->model('users', 'User');
    $this->load->model('notification', 'Notification');
    $this->load->model('admin_notification', 'AdminNotification');
    $this->load->model('package_model', 'Package');
    $this->load->model('site_config');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }

    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }
  }

   public function view($id) {
    $data['admin'] = $this->users->get($this->session->userdata('id'));

    $result = $this->db->query("SELECT * FROM packages WHERE id={$id}")->row();
    $this->User->checkAdminUserRights($result->id_user, 'admin/');

    $package = $this->db->query("SELECT p.*, u.first_name, u.last_name, u.address1, u.address2, u.city, u.country, u.state FROM packages AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE p.id={$id}")->row();
    $data['title'] = "Package for ".$package->first_name." ".$package->last_name;
    $data['package'] = $package;

    $data['package_order'] = $this->db->query("SELECT * FROM ordered_products WHERE id='".$package->id_order."'")->row();

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/package-view', $data);
    $this->load->view('templates/footer');

  }
  /*
   * Delete a rejected package.
  */
  public function delete($id)
  {
    $result = $this->db->query("SELECT * FROM packages WHERE id={$id}")->row();
    $this->User->checkAdminUserRights($result->id_user, 'admin/');
    $package = $this->db->query("SELECT p.*, u.first_name, u.last_name, u.address1, u.address2, u.city, u.country, u.state FROM packages AS p INNER JOIN auth_user AS u ON u.id=p.id_user WHERE p.id={$id}")->row();
    // if package is rejected
    if($package->status == -1)
    {
        $this->Package->delete($package->id, $package->id_order);
        redirect('admin/user/view/'.$package->id_user);
    }
    else {
      // package is not rejected.
      redirect('admin/packages/view/'.$id);
    }

  }

  public function approve($id) {
      $result = $this->db->query("SELECT * FROM packages WHERE id={$id}")->row();
      $this->User->checkAdminUserRights($result->id_user, 'admin/payments/');
      $settings = $this->site_config->getGeneralSettings();

      $size = '-OZ';
      if(strlen($result->package_product_size)){
          $size = '-'.strtoupper($result->package_product_size);
      }

      $products = array();

      if($result->type == '1'){
        $skus = $settings['level2_package'];
        $this->User->updatePackageSent($result->id_user, "2", "set");

        $skus = str_replace(' ', '', $skus);
        $skus = explode(',', $skus);

        foreach ($skus as $key => $sku) {
            if($key == 0){
                $products[] = array(
                    'sku' => 'P-'.$sku.$size,
                    'quantity' => 1,
                    'price' => 0
                );
            } else {
                $products[] = array(
                    'sku' => 'P-'.$sku,
                    'quantity' => 1,
                    'price' => 0
                );
            }
        }

        $details = $this->Package->getOrderDetails($result->id);
        $order_id = $this->Package->sendOrder($result->id_user, $products, $details);

        if (!$order_id) {
          $this->session->set_flashdata('response_error', $id);
          redirect('admin/packages/view/'.$id);
        } else {
          $this->Package->approve($id, $order_id);
          $this->AdminNotification->logHandling($this->session->userdata('id'), $id, 'packages');
        }
      }
      else if($result->type == '2'){
        $skus = $settings['level3_package'];
        $this->User->updatePackageSent($result->id_user, "3", "set");

        $skus = str_replace(' ', '', $skus);
        $skus = explode(',', $skus);

        $products[] = array(
            'sku' => $result->free_product,
            'quantity' => 1,
            'price' => 0
          );

        foreach ($skus as $key => $sku) {
            $products[] = array(
                'sku' => 'P-'.$sku,
                'quantity' => 1,
                'price' => 0
            );
        }

        $details = $this->Package->getOrderDetails($result->id);
        $order_id = $this->Package->sendOrder($result->id_user, $products, $details);

        if (!$order_id) {
            $this->session->set_flashdata('response_error', $id);
            redirect('admin/packages/view/'.$id);
        } else {
          $this->Package->approve($id, $order_id);
          $this->AdminNotification->logHandling($this->session->userdata('id'), $id, 'packages');
        }
      }

      redirect('admin/');
  }

  public function reject($id){
    $reason = $this->input->post('reason');
    $this->Package->reject($id, $reason);

    $package = $this->Package->getById($id);
    $this->Notification->insertData($package->id_user, 18, $id);

    $this->AdminNotification->logHandling($this->session->userdata('id'), $id, 'packages');
    redirect('admin/');
  }

}
