<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_requests extends CI_Controller {

    public function __construct()
  {
    parent::__construct();

    $this->load->model('admin_notification', 'AdminNotification');
    $this->load->model('notification', 'Notification');
    $this->load->model('users', 'User');
    $this->load->model('email_model', 'Email');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }
    
    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }

  }

  public function index($id_notification = false) {
    $data['admin'] = $this->users->get($this->session->userdata('id'));

    // if is Admin get user by Assigned Countries
    $id_user = $this->session->userdata('id');
    $user    = $this->User->get($id_user);
    $query     = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}");
    $countries = $query->result();
    $ids_countries = array();
    foreach ($countries as $c) {
        $ids_countries[] = $c->id_country;
    }
    $ids_countries = implode(', ', $ids_countries);

    if ($id_notification) {
        $this->AdminNotification->setInactive($id_notification);
    }

    if ($user['level'] == 2) {
        $data['requests'] = $this->db->query("SELECT first_name, last_name, id FROM auth_user WHERE master_request=1 AND id_country IN ($ids_countries)")->result();
    } else {
        $data['requests'] = $this->db->query("SELECT first_name, last_name, id FROM auth_user WHERE master_request=1")->result();
    }


    $data['title'] = "Master Requests";

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/master_requests-list', $data);
    $this->load->view('templates/footer');

  }


  public function multipleApprove() {
      $requests = $this->input->post('requests');

      foreach ($requests as $r) {
          $user = $this->users->get($r);

          $this->db->set('level', '1');
          $this->db->set('master_request', '0');
          $this->db->set('slaves', '5');
          $this->db->where('id', $r);
          $this->db->update('auth_user');

          $this->Notification->id_user = $r;
          $this->Notification->type = 7;
          $this->Notification->insert();

          $this->AdminNotification->logHandling($this->session->userdata('id'), $r, 'auth_user', 'id_admin_master', 'handled_master');

          $this->Email->approvedMaster($user['email']);
      }

      redirect('admin/master_requests');
  }


  public function reject($id){
      $user = $this->users->get($id);

      $this->db->set('level', '0');
      $this->db->set('master_request', '-1');
      $this->db->where('id', $id);
      $this->db->update('auth_user');

      $this->AdminNotification->logHandling($this->session->userdata('id'), $id, 'auth_user', 'id_admin_master', 'handled_master');

      $this->Email->sendMasterApplicationRejected($user['email']);
      redirect('/admin/master_requests');
  }

}
