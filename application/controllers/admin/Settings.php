<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('users', 'User');
    $this->load->model('Questions_model', 'questions');
    $this->load->model('front_image_model', 'FrontImage');
    $this->load->model("Site_config");

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }

    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }

    $admin = $this->users->get($this->session->userdata('id'));
    if ($admin['level'] != 3) {
      redirect('/admin');
    }
  }


  public function index() {
    $data['admin'] = $this->users->get($this->session->userdata('id'));

    $settings = $this->Site_config->getGeneralSettings();

    $countries_commissions = $this->db->query("SELECT * FROM countries_commissions INNER JOIN country_t ON country_t.country_id=countries_commissions.id_country")->result();
    $front_images = $this->FrontImage->getAll();

    // level upgrade networks
    $level_upgrade_networks = explode(",", $settings['level_upgrade_networks']);

    $data['settings'] = $settings;
    $data['level_upgrade_networks'] = $level_upgrade_networks;
    $data['countries_commissions'] = $countries_commissions;
    $data['front_images'] = $front_images;
    $data['title'] = 'Settings';
    $data['questions'] = $this->questions->getAll();

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/settings', $data);
    $this->load->view('templates/footer');

  }


  public function save(){
    // level upgrade networks
    $level_upgrade_networks = array();
    if($this->input->post("fb")){
        $level_upgrade_networks[] = "fb";
    }
    if($this->input->post("tw")){
        $level_upgrade_networks[] = "tw";
    }
    if($this->input->post("in")){
        $level_upgrade_networks[] = "in";
    }
    if($this->input->post("tl")){
        $level_upgrade_networks[] = "tl";
    }
    if($this->input->post("pi")){
        $level_upgrade_networks[] = "pi";
    }
    if($this->input->post("ln")){
        $level_upgrade_networks[] = "ln";
    }
    if($this->input->post("yb")){
        $level_upgrade_networks[] = "yb";
    }
    if($this->input->post("vi")){
        $level_upgrade_networks[] = "vi";
    }

    $_POST['level_upgrade_networks'] = implode(",", $level_upgrade_networks);

    foreach ($_POST as $name => $value) {
      $data = array(
        'value' => $value
      );

      $this->db->where('name', $name);
      $this->db->update('general_settings', $data);
    }

    if(count($_FILES) && $_FILES['front_images']['size'][0]){

        $images = $this->FrontImage->extractFiles($_FILES['front_images']);

        foreach ($images as $key => $value) {
            $image_name = $this->FrontImage->uploadImage($images, $key);
            $this->FrontImage->insert($image_name);
        }
    }

    redirect('/admin/settings/');
  }


  public function delete_front_image($id_img){
      $id_img = intval($id_img);

      if($id_img){
          $this->FrontImage->delete($id_img);
      }

      redirect('/admin/settings/');
  }


  public function add_commission() {
      $data['admin'] = $this->users->get($this->session->userdata('id'));

      $countries = $this->db->query("SELECT * FROM country_t")->result();

      if (isset($_POST['trigger']) && $_POST['trigger'] == 'add') {

          $post = array(
              'id_country' => ($this->input->post('country') == NULL) ? FALSE : $this->input->post('country'),
              'level' => ($this->input->post('level') == NULL) ? FALSE : $this->input->post('level'),
              'value' => ($this->input->post('value') == NULL) ? FALSE : $this->input->post('value'),
              'type' => 1
            );

          // check if settings already exists
          $setting = $this->db->query("SELECT * FROM countries_commissions WHERE id_country={$post['id_country']} AND level={$post['level']} AND type=1")->row();
          if (empty($setting)) {
              $this->db->insert('countries_commissions', $post);
          }
          redirect('admin/settings');

      }

      $data['title'] = 'Add Country Commission';
      $data['countries'] = $countries;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/settings_add_commission', $data);
      $this->load->view('templates/footer');
  }


  public function add_bonus() {
      $data['admin'] = $this->users->get($this->session->userdata('id'));

      $countries = $this->db->query("SELECT * FROM country_t")->result();

      if (isset($_POST['trigger']) && $_POST['trigger'] == 'add') {

          $post = array(
              'id_country' => ($this->input->post('country') == NULL) ? FALSE : $this->input->post('country'),
              'value' => ($this->input->post('value') == NULL) ? FALSE : $this->input->post('value'),
              'type' => 2
            );

          // check if settings already exists
          $setting = $this->db->query("SELECT * FROM countries_commissions WHERE id_country={$post['id_country']} AND type=2")->row();
          if (empty($setting)) {
              $this->db->insert('countries_commissions', $post);
          }
          redirect('admin/settings');

      }

      $data['title'] = 'Add Country Bonus per Slave';
      $data['countries'] = $countries;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/settings_add_bonus', $data);
      $this->load->view('templates/footer');
  }


  public function remove_country($type, $id_country) {

      $this->db->delete('countries_commissions', array('type' => $type, 'id_country' => $id_country));

      redirect('admin/settings');

  }


}
