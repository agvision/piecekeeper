<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Best_performers extends CI_Controller {

    public function __construct()
  {
    parent::__construct();

    $this->load->model('users', 'User');
    $this->load->model('notification', 'Notification');
    $this->load->model('admin_notification', 'AdminNotification');
    $this->load->model('best_performers_model', 'BestPerformers');
    $this->load->model('site_config');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }
    
    if (!$this->users->is_super_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }
  }

   public function view($id) {
      $data['admin'] = $this->users->get($this->session->userdata('id'));
      $settings = $this->site_config->getGeneralSettings();

      $list = $this->db->query("SELECT * FROM best_performers WHERE id={$id}")->row();
      $ids_users = explode(',', $list->list);

      $users = array();
      foreach ($ids_users as $key => $id_user) {
          $pk = $this->users->get($id_user);
          if(!$pk){
            continue;
          }
          $users[$key] = $pk;
          $users[$key]['total_sales']  = $this->users->countSalesBetween($id_user, $list->from, $list->to);
          $users[$key]['earnings']     = $this->users->getEarningsBetween($id_user, $list->from, $list->to);
          $users[$key]['bonus_points'] = $this->users->getPoints($id_user, 'bonus', $list->from, $list->to);
          $users[$key]['total_points'] = $this->users->getPoints($id_user, 'total', $list->from, $list->to);
      }

      $data['title']    = "Best Performers last ".$settings['best_performers_days_interval']." days";
      $data['users']    = $users;
      $data['list']     = $list;
      $data['settings'] = $settings;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/best-performers-view', $data);
      $this->load->view('templates/footer');
  }

  public function approve($id, $value, $approved_list) {
      $approved_list = explode("_", $approved_list);
      $value = intval($value);
      $this->BestPerformers->approve($id, $value, $approved_list);

      $this->AdminNotification->logHandling($this->session->userdata('id'), $id, 'best_performers');
      redirect('admin/');
  }

  public function reject($id){
    $reason = $this->input->post('reason');
    $this->BestPerformers->reject($id, $reason);

    $this->AdminNotification->logHandling($this->session->userdata('id'), $id, 'best_performers');
    redirect('admin/');
  }

}
