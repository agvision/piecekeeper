<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifications_history extends CI_Controller {

    public function __construct()
  {
    parent::__construct();

    $this->load->model('admin_notification', 'AdminNotification');
    $this->load->model('notification', 'Notification');
    $this->load->model('users', 'User');
    $this->load->model('email_model', 'Email');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }
    
    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }

  }

  public function index(){
     redirect('/admin/notifications_history/page/1');
  }

  public function page($page = false , $id_admin = false) {

  	$filter_admin = '';
  	$show_filters = false;
  	if( $this->input->post('filter_by_admin')){
  		$filter_admin = $this->input->post('filter_by_admin');
  	}else{
  		if($id_admin){
  			$filter_admin = $id_admin;
  		}
  	}



    $data['admin'] = $this->users->get($this->session->userdata('id'));
    $page = intval($page) > 0 ? intval($page) : 1;

    // if is Admin get user by Assigned Countries
    $id_user = $this->session->userdata('id');
    $user    = $this->User->get($id_user);

    if ($user['level'] == 2) {
      $countries = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}")->result();
    } elseif ($user['level'] == 3) {
      $countries = $this->db->query("SELECT country_id AS id_country FROM country_t")->result();
      $show_filters = true;
    }

    $ids_countries = array();
    foreach ($countries as $c) {
        $ids_countries[] = $c->id_country;
    }
    $ids_countries = implode(', ', $ids_countries);

    $items_per_page = 20;
    $current_page = $page;


    $filters = array('id_admin'=>$filter_admin);


    $notifications = $this->AdminNotification->getAll($ids_countries, true, $items_per_page, $page , $filters);


    foreach ($notifications as $key => $n) {
        $not_admin = $this->users->get($n['id_admin']);
        $notifications[$key]['admin_name'] = $not_admin['first_name'].' '.$not_admin['last_name'];
    }


    $query  = $this->db->query("SELECT id, first_name,last_name FROM auth_user WHERE level>=2");
    $admins = $query->result();

    $data['title']          = "Notifications History";
    $data['notifications']  = $notifications;
    $data['filter_by_admin'] = $filter_admin;
    $data['show_filters'] = $show_filters;
    $data['admins'] = $admins;
    $data['current_page']   = $current_page;
    $data['items_per_page'] = $items_per_page;

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/notifications_history', $data);
    $this->load->view('templates/footer');

  }



}
