<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Broadcast extends CI_Controller {

	public function __construct() {
		parent::__construct();

        $this->load->model('users', 'User');
        $this->load->model('email_model', 'Email');
		$this->load->model('user_tag_model', 'UserTag');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }

		if (!$this->users->is_admin($this->session->userdata('id'))) {
			redirect('/auth');
		}
	}

	public function index() {
        $data['admin'] = $this->users->get($this->session->userdata('id'));
        $data['all_user_tags'] = $this->UserTag->getAll();
        $id_user = $this->session->userdata('id');
        $user = $data['admin'];

       // krumo::dump($data); die();

        $user_tags = '';
        if($this->input->post('user-tags')){
          $tags = $this->input->post('user-tags');
          foreach ($tags as $tag) {
            $user_tags[] = "'".$tag."'";
          }
          $user_tags = implode(',', $user_tags);
          $user_tags = str_replace(",''", "", $user_tags);
          $user_tags = str_replace("'',", "", $user_tags);
          $user_tags = str_replace("''", "", $user_tags);
          $user_tags = str_replace(" ", "", $user_tags);
        }
        $selected_tags = explode(',', $user_tags);
        if(strlen($user_tags)){
          $count_selected_tags = count($selected_tags);
        } else {
          $count_selected_tags = 0;
        }

        if ($user['level'] == 2) {
            $data['filter_countries'] = $this->db->query("SELECT c.* FROM admins_countries AS ac INNER JOIN country_t AS c ON c.country_id=ac.id_country WHERE ac.id_user={$id_user}")->result();
            $data['filter_cities']    = $this->db->query("SELECT u.city AS name FROM admins_countries AS ac INNER JOIN auth_user AS u ON u.id_country=ac.id_country WHERE ac.id_user={$id_user} GROUP BY u.city")->result();
            $data['filter_schools']   = $this->db->query("SELECT u.school AS name FROM admins_countries AS ac INNER JOIN auth_user AS u ON u.id_country=ac.id_country WHERE ac.id_user={$id_user} GROUP BY u.school")->result();
            $data['filter_states']    = $this->db->query("SELECT u.state AS name FROM admins_countries AS ac INNER JOIN auth_user AS u ON u.id_country=ac.id_country WHERE ac.id_user={$id_user} GROUP BY u.state")->result();
        } elseif($user['level'] == 3) {
            $data['filter_countries'] = $this->db->query("SELECT c.* FROM auth_user AS u INNER JOIN country_t AS c ON c.country_id=u.id_country GROUP BY c.country_id")->result();
            $data['filter_cities']    = $this->db->query("SELECT city AS name FROM auth_user GROUP BY city")->result();
            $data['filter_schools']   = $this->db->query("SELECT school AS name FROM auth_user GROUP BY school")->result();
            $data['filter_states']     = $this->db->query("SELECT state AS name FROM auth_user GROUP BY state")->result();
        }

        $filters = array("age_min", "age_max", "gender", "min_sales", "min_sales_start", "min_sales_end", "activity_days",
            "no_activity_days", "score_type", "score", "min_slaves", "level", "slaves", "filter_country", "filter_city", "filter_school",
            "global_rank", "country_rank", "city_rank", "min_followers", "max_followers", "user-tags", "filter_state_arr");

        $data['filters'] = array();

        foreach($filters as $f) {
            $data[$f] = $this->input->post($f);
            if($data[$f] || $data[$f] === "0") {
            	$data['filters'][$f] = $data[$f];
            }
        }

        // if is Admin get user by Assigned Countries
        $id_user = $this->session->userdata('id');
        $user    = $this->User->get($id_user);
        $query     = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}");
        $countries = $query->result();
        $ids_countries = array();
        foreach ($countries as $c) {
            $ids_countries[] = $c->id_country;
        }
        $ids_countries = implode(', ', $ids_countries);

        $data['templates'] = $this->db->query("SELECT * FROM email_templates")->result();

        if ($user['level'] == 2) {
        	$data['results'] =  $this->users->getList($data, false, $ids_countries);
            $data['results_no'] = count($data['results']);
        } else {
        	$data['results'] =  $this->users->getList($data, false);
            $data['results_no'] = count($data['results']);
        }

        // check if is export
        $export = false;
        if(isset($_POST['exp']) && $_POST['exp'] == 'true'){
          $export = true;
        }

        if ($export) {
          $this->load->library('excel');
          $format = $this->input->post('exp-format');

          // get automatically coupons
          $auto_coupons = $this->session->userdata("coupons");
          if(!$auto_coupons){
            $auto_coupons = array();
          }

          $this->excel->setActiveSheetIndex(0);
          $this->excel->getActiveSheet()->setTitle('PieceKeepers');

          $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
          $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
          $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);

          $this->excel->getActiveSheet()->setCellValue('A1', 'First Name');
          $this->excel->getActiveSheet()->setCellValue('B1', 'Last Name');
          $this->excel->getActiveSheet()->setCellValue('C1', 'Email');

          $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
          $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
          $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);

          $i = 2;

          $currentTime = new DateTime(date('Y-m-d'));

          foreach ($data['results'] as $u) {
                  $this->excel->getActiveSheet()->setCellValue('A'.$i, $u->first_name);
                  $this->excel->getActiveSheet()->setCellValue('B'.$i, $u->last_name);
                  $this->excel->getActiveSheet()->setCellValue('C'.$i, $u->email);


                  $i++;

          }

          switch ($format) {
			default:
			case 'xls':
				$filename = "piecekeepers.xls";
				header('Content-Type: application/vnd.ms-excel'); //mime type
				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

			break;

			case 'xlsx':
				$filename = "piecekeepers.xlsx";
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //mime type
				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

			break;

			case 'csv':
				$filename = "piecekeepers.csv";
				header('Content-type: text/csv'); //mime type
				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');

			break;
          }

          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache

          $objWriter->save('php://output');
          exit();
      }


        $data['title'] = "Broadcast";
        $data['selected_tags'] = $selected_tags;
        $data['count_selected_tags'] = $count_selected_tags;

		$this->load->view('templates/header', $data);
		$this->load->view('pages/admin/broadcast-index', $data);
		$this->load->view('templates/footer');

	}

	public function send_email() {
        $data['admin'] = $this->users->get($this->session->userdata('id'));

		$filters = array("age_min", "age_max", "gender", "min_sales", "min_sales_start", "min_sales_end", "activity_days",
            "no_activity_days", "score_type", "score", "min_slaves", "level", "slaves", "filter_country", "filter_city", "filter_school",
            "global_rank", "country_rank", "city_rank", "min_followers", "max_followers", "user-tags", "filter_state_arr");

       	$options= array();

        foreach($filters as $f) {
            $options[$f] = $this->input->post($f);
        }

        // if is Admin get user by Assigned Countries
        $id_user = $this->session->userdata('id');
        $user    = $this->User->get($id_user);
        $query     = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}");
        $countries = $query->result();
        $ids_countries = array();
        foreach ($countries as $c) {
            $ids_countries[] = $c->id_country;
        }
        $ids_countries = implode(', ', $ids_countries);

        if ($user['level'] == 2) {
        	$users = $this->users->getList($options, false, $ids_countries);
        } else {
        	$users = $this->users->getList($options);
        }

        $data = array(
                'subject' => $this->input->post('subject'),
                'body' => $this->input->post('mail_body'),
                'id_sender' => $id_user,
                'created' => date('Y-m-d H:i:s', time())
            );

        $this->db->insert('emails_report', $data);
        $id_email = $this->db->insert_id();

        // prepare ids to get group stats
        $ids_users = array();
        foreach ($users as $u) {
            $ids_users['user_'.$u->id] = $u->id;
        }
        $global_ranks  = $this->users->getGroupGlobalRanking($ids_users);
        $country_ranks = $this->users->getGroupCountryRanking($ids_users);
        $city_ranks    = $this->users->getGroupCityRanking($ids_users);

        foreach ($users as $user) {
        	$stats = array();
        	$stats['name']         = $user->first_name.' '.$user->last_name;
        	$stats['followers']    = $this->users->getAllFollowers($user->id);
        	$stats['total_points'] = $this->users->getPoints($user->id, 'total');
        	$stats['commission']   = $this->users->getTotalCommission($user->id);
        	$stats['global_rank']  = $global_ranks['user_'.$user->id]->place;
        	$stats['country_rank'] = $country_ranks['user_'.$user->id]->place;
        	$stats['city_rank']    = $city_ranks['user_'.$user->id]->place;

            $this->Email->sendBroadcast($user->email, $_POST['subject'], $_POST['mail_body'], $stats);

            $data = array(
                    'id_email' => $id_email,
                    'id_user' => $user->id
                );
            $this->db->insert('emails_users', $data);
        }

       	redirect('/admin/broadcast');
	}

	public function get_template($template_id) {

		$template = $this->db->query("SELECT * FROM email_templates WHERE id={$template_id}")->row();
		if($template) {
			echo json_encode($template);
		}

	}

}
