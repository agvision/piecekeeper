<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Points extends CI_Controller {

    public function __construct()
  {
    parent::__construct();

    $this->load->model('points_model', 'Points');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }
    
    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }
  }

  public function index(){
      redirect('/admin/points/view');
  }

  public function view($type = false){
      $data['admin'] = $this->users->get($this->session->userdata('id'));
      if(!$type){
         $type = '0';
      }

      $id_user = $this->session->userdata('id');
      $user    = $this->users->get($id_user);

      // limit result by admin countries
      if ($user['level'] == 2) {
        $countries = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}")->result();
      } elseif ($user['level'] == 3) {
        $countries = $this->db->query("SELECT country_id AS id_country FROM country_t")->result();
      }

      $ids_countries = array();
      foreach ($countries as $c) {
          $ids_countries[] = $c->id_country;
      }
      $ids_countries = implode(', ', $ids_countries);

      if($type == 1){
          $points = $this->Points->getAll($ids_countries, 'sales');
          $data['title'] = 'Sales Points';
      }
      else if($type == 2){
          $points = $this->Points->getAll($ids_countries, 'bonus');
          $data['title'] = 'Bonus Points';
      }
      else {
          $points = $this->Points->getAll($ids_countries);
          $data['title'] = 'Points';
      }

      foreach ($points as $p) {
          $p->link = false;

          if($p->type == 1){
            $p->label = $p->order_id;
          }
          else if($p->type == 2 && $p->bonus_type){
              $result = explode('_', $p->bonus_type);
              $bonus_type = $result[0];
              $bonus_details = $result[1];
              if($bonus_type == 1){
                  $p->label = 'Mission';
                  $p->link  = base_url('/admin/missions/view/'.$bonus_details);
              }
              else if($bonus_type == 2){
                  $p->label = 'Level '.$bonus_details;
              }
              else if($bonus_type == 3){
                  $p->label = 'Best Performer';
                  $p->link  = base_url('/admin/best_performers/view/'.$bonus_details);
              }
          }
          else {
              $p->label = 'Other';
          }
      }

      $data['points'] = $points;
      $data['type']   = $type;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/points_view', $data);
      $this->load->view('templates/footer');
  }


}
