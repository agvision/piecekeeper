<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scratch_codes extends CI_Controller {

    public function __construct()
  {
    parent::__construct();

    $this->load->model('scratch_coupon_model', 'ScratchCoupon');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }
    
    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }

  }


  public function view($batch){
      $data['admin'] = $this->users->get($this->session->userdata('id'));

      $coupons = $this->ScratchCoupon->getAllCoupons($batch);

      $data['title']   = $batch;
      $data['coupons'] = $coupons;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/scratch_codes_view', $data);
      $this->load->view('templates/footer');
  }


  public function add($id_user){
      $data['admin'] = $this->users->get($this->session->userdata('id'));

      $id_user = intval($id_user);
      if(!$id_user){
        redirect('/admin');
      }

      $user   = $this->users->get($id_user);
      $errors = array();

      if(isset($_POST['trigger']) && $_POST['trigger'] == 'add'){
          $batch = $this->input->post('batch');

          $this->db->where('batch', $batch);
          $batch = $this->db->get('scratch_batches')->row();
          if(!count($batch)){
             $errors[] = "This Batch Code is not in the PieceKeeper System.";
          }
          else if($batch->active){
              $owner = $this->users->getById($batch->id_user);
              $errors[] = "This Batch Code has already been added to <b>".$owner->first_name." ".$owner->last_name."</b>.";
          }
          if(!count($errors)){
            $this->db->where('id', $batch->id);
            $this->db->set('id_user', $id_user);
            $this->db->set('active', '1');
            $this->db->set('activated', 'NOW()', false);
            $this->db->update('scratch_batches');

            redirect('/admin/user/view/'.$id_user);
          }
      }

      $data['title']   = 'Add Batch for '.$user['first_name'].' '.$user['last_name'];
      $data['user']    = $user;
      $data['errors']  = $errors;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/scratch_codes_add', $data);
      $this->load->view('templates/footer');
  }


}
