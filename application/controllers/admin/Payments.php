<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payments extends CI_Controller {

    public function __construct()
  {
    parent::__construct();

    $this->load->model('users', 'User');
    $this->load->model('coupon_model', 'Coupon');
    $this->load->model('email_model', 'Email');
    $this->load->model('payment_request', 'Payment');
    $this->load->model('notification', 'Notification');
    $this->load->model('admin_notification', 'AdminNotification');
    $this->load->model('site_config');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }

    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }
  }

  public function index() {
    $data['admin'] = $this->users->get($this->session->userdata('id'));

    // if is Admin get user by Assigned Countries
    $id_user = $this->session->userdata('id');
    $user    = $this->User->get($id_user);
    $query     = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}");
    $countries = $query->result();
    $ids_countries = array();
    foreach ($countries as $c) {
        $ids_countries[] = $c->id_country;
    }
    $ids_countries = implode(', ', $ids_countries);

    $data['title'] = "Payment requests from Ambasadors";
    $only = $this->input->get("only");
    if($only===false || empty($only)) {
        $onlystr = "";
    } else {
        $onlystr = " WHERE p.aproved=$only";
        $data['only'] = $only;
    }

    if ($user['level'] == 2) {
        if ($only === false) {
            $data['requests'] = $this->db->query("SELECT p.id, p.created, p.amount, p.gift_amount, p.aproved, p.giftcard, u.first_name, u.last_name, u.id as user_id FROM payment_requests as p ".
                                "INNER JOIN auth_user as u ON p.id_user=u.id $onlystr WHERE u.id_country IN ($ids_countries) ORDER BY p.created DESC ")->result();
        } else {
            $data['requests'] = $this->db->query("SELECT p.id, p.created, p.amount, p.gift_amount, p.aproved, p.giftcard, u.first_name, u.last_name, u.id as user_id FROM payment_requests as p ".
                                "INNER JOIN auth_user as u ON p.id_user=u.id $onlystr AND u.id_country IN ($ids_countries) ORDER BY p.created DESC ")->result();
        }

    } else {
        $data['requests'] = $this->db->query("SELECT p.id, p.created, p.amount, p.gift_amount, p.aproved, p.giftcard, u.first_name, u.last_name, u.id as user_id FROM payment_requests as p ".
                                "INNER JOIN auth_user as u ON p.id_user=u.id $onlystr ORDER BY p.created DESC ")->result();
    }


    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/payment_requests-list', $data);
    $this->load->view('templates/footer');

  }

   public function view($id, $notification_id=false) {
    $data['admin'] = $this->users->get($this->session->userdata('id'));

    $result = $this->db->query("SELECT id_user, giftcard, amount FROM payment_requests WHERE id={$id}")->row();
    $this->User->checkAdminUserRights($result->id_user, 'admin/payments/');

    $payment = $this->db->query("SELECT c.code, p.id, p.created, p.amount, p.reason, p.gift_amount, p.giftcard, p.paypal, p.aproved, u.first_name, u.last_name, u.payment_name, u.payment_number, u.email, u.id as user_id FROM payment_requests as p ".
                                "LEFT JOIN auth_user as u ON p.id_user=u.id LEFT JOIN coupons AS c ON p.id_coupon=c.id WHERE ".
                                "p.id={$id}")->row();
    $data['title'] = "Payment request from " . $payment->first_name . " " . $payment->last_name;
    $data['payment'] = $payment;

    if($notification_id) {
        $this->load->model('admin_notification', 'AdminNotification');
         $this->AdminNotification->setInactive($notification_id);
      }

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/payment_requests-view', $data);
    $this->load->view('templates/footer');

  }

  public function approve($id) {
      $result = $this->db->query("SELECT id_user, giftcard, amount FROM payment_requests WHERE id={$id}")->row();
      $this->User->checkAdminUserRights($result->id_user, 'admin/payments/');

      $settings = $this->site_config->getGeneralSettings();

      $this->load->model("payment_request", 'Request');
      $this->Request->aprove($id);

      $this->AdminNotification->logHandling($this->session->userdata('id'), $id, 'payment_requests');
      redirect('admin/payments/view/' . $id);
  }


  public function multipleApprove() {
      $requests = $this->input->post('requests');
      $this->load->model("payment_request", 'Request');

      foreach ($requests as $r) {
          $this->Request->aprove($r);
          $this->AdminNotification->logHandling($this->session->userdata('id'), $r, 'payment_requests');
      }

      redirect('admin/payments');
  }

  public function reject($id){
    $reason = $this->input->post('reason');

    $this->db->set('aproved', '-1');
    $this->db->set('reason', $reason);
    $this->db->where('id', $id);
    $this->db->update('payment_requests');

    $this->AdminNotification->logHandling($this->session->userdata('id'), $id, 'payment_requests');

    redirect('admin/payments');
  }

}
