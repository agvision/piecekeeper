<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Goals extends CI_Controller {

    public function __construct()
  {
    parent::__construct();

    $this->load->model('users', 'User');
    // $this->load->model('email_model', 'Email');
    $this->load->model('goal_model', 'Goal');
    // $this->load->model('admin_notification', 'AdminNotification');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }
    
    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }

  }

  public function index($id_notification = false) {
    $data['admin'] = $this->users->get($this->session->userdata('id'));
    $id_user = $this->session->userdata('id');
    $user = $data['admin'];

    // limit results by admin countries
    if ($user['level'] == 2) {
      $countries = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}")->result();
    } elseif ($user['level'] == 3) {
      $countries = $this->db->query("SELECT country_id AS id_country FROM country_t")->result();
    }

    $ids_countries = array();
    foreach ($countries as $c) {
        $ids_countries[] = $c->id_country;
    }
    $ids_countries = implode(', ', $ids_countries);

    $goals = $this->db->query("SELECT * FROM goals AS g LEFT JOIN country_t AS c ON g.country=c.country_id WHERE (g.country IN ({$ids_countries}) OR g.country=0) AND g.deleted=0 ORDER BY g.created DESC")->result();


    $data['title']  = "Goals";
    $data['goals']  = $goals;

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/goals-list', $data);
    $this->load->view('templates/footer');

  }


  public function add() {
      $data['admin'] = $this->users->get($this->session->userdata('id'));
      $id_user = $this->session->userdata('id');
      $user = $data['admin'];

      if ($user['level'] == 2) {
          $countries = $this->db->query("SELECT c.* FROM admins_countries AS ac INNER JOIN country_t AS c ON c.country_id=ac.id_country WHERE ac.id_user={$id_user}")->result();
          $cities    = $this->db->query("SELECT u.city AS name FROM admins_countries AS ac INNER JOIN auth_user AS u ON u.id_country=ac.id_country WHERE ac.id_user={$id_user} GROUP BY u.city")->result();
      } elseif($user['level'] == 3) {
          $countries = $this->db->query("SELECT c.* FROM auth_user AS u INNER JOIN country_t AS c ON c.country_id=u.id_country GROUP BY c.country_id")->result();
          $cities    = $this->db->query("SELECT city AS name FROM auth_user GROUP BY city")->result();
      }

      if (isset($_POST['trigger']) && $_POST['trigger'] == 'add') {

          if ($user['level'] == 2 && !$this->input->post('country')) {
              redirect('admin/goals/add');
          }

          if (!$this->input->post('deadline')) {
              $deadline = date('Y-m-d', time());
          } else {
              $deadline = $this->input->post('deadline');
          }

          $deadline = date('Y-m-d H:i:s', strtotime($deadline));

          $data = array(
              'title' => $this->input->post('title'),
              'deadline' => $deadline,
              'invited' => $this->input->post('invited'),
              'points' => $this->input->post('points'),
              'country' => $this->input->post('country'),
              'city' => $this->input->post('city'),
              'description' => $this->input->post('description'),
            );

          $this->db->set('created', 'NOW()', false);
          $this->db->insert('goals', $data);

          redirect('/admin/goals/');
      }

      $data['title'] = "Add Goal";
      $data['countries'] = $countries;
      $data['cities'] = $cities;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/goal-add', $data);
      $this->load->view('templates/footer');
  }


  public function edit($id) {
      $data['admin'] = $this->users->get($this->session->userdata('id'));
      $id_user = $this->session->userdata('id');
      $user = $data['admin'];

      if ($user['level'] == 2) {
          $countries = $this->db->query("SELECT c.* FROM admins_countries AS ac INNER JOIN country_t AS c ON c.country_id=ac.id_country WHERE ac.id_user={$id_user}")->result();
          $cities    = $this->db->query("SELECT u.city AS name FROM admins_countries AS ac INNER JOIN auth_user AS u ON u.id_country=ac.id_country WHERE ac.id_user={$id_user} GROUP BY u.city")->result();
      } elseif($user['level'] == 3) {
          $countries = $this->db->query("SELECT c.* FROM auth_user AS u INNER JOIN country_t AS c ON c.country_id=u.id_country GROUP BY c.country_id")->result();
          $cities    = $this->db->query("SELECT city AS name FROM auth_user GROUP BY city")->result();
      }

      $goal = $this->db->query("SELECT * FROM goals WHERE id={$id}")->row();



      if ($user['level'] == 2 && !$goal->country) {
          redirect('admin/goals');
      }

      if (isset($_POST['trigger']) && $_POST['trigger'] == 'edit') {

          if (!$this->input->post('deadline')) {
              $deadline = date('Y-m-d', time());
          } else {
              $deadline = $this->input->post('deadline');
          }

          $deadline = date('Y-m-d H:i:s', strtotime($deadline));

          $data = array(
              'title' => $this->input->post('title'),
              'deadline' => $deadline,
              'invited' => $this->input->post('invited'),
              'points' => $this->input->post('points'),
              'country' => $this->input->post('country'),
              'city' => $this->input->post('city'),
              'description' => $this->input->post('description'),
          	  'required_age'=>$this->input->post('required_age'),
          	  'required_date'=>$this->input->post('required_date'),
            );

          $this->db->where('id', $id);
          $this->db->update('goals', $data);

          redirect('/admin/goals/');
      }

      $data['title'] = "Edit Goal";
      $data['countries'] = $countries;
      $data['cities'] = $cities;
      $data['goal']  = $goal;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/goal-edit', $data);
      $this->load->view('templates/footer');
  }


  public function view($id_goal) {
      $data['admin'] = $this->users->get($this->session->userdata('id'));


      $goal_restrictions = $this->db->query("SELECT required_age , required_date  FROM goals where id = {$id_goal}")->row();

      $data['winners'] = $this->Goal->getWinners($id_goal,$goal_restrictions->required_age, $goal_restrictions->required_date);
      $data['title']   = "Winners";

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/goal-view', $data);
      $this->load->view('templates/footer');
  }


  public function delete($id) {
      $user = $this->users->get($this->session->userdata('id'));
      $goal = $this->db->query("SELECT * FROM goals WHERE id={$id}")->row();

      if ($user['level'] == 2 && !$goal->country) {
          redirect('admin/goals');
      }

      $this->db->set('deleted', 1);
      $this->db->where('id', $id);
      $this->db->update('goals');

      redirect('admin/goals/');
  }

}
