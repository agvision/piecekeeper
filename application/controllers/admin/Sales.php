<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales extends CI_Controller {

    public function __construct()
  {
    parent::__construct();

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }
    
    if (!$this->users->is_admin($this->session->userdata('id'))) {
      redirect('/auth');
    }
  }


  public function view($interval = false) {
    $data['admin'] = $this->users->get($this->session->userdata('id'));

    if (!$interval) {
        redirect('/admin/stats');
    }

    $id_user = $this->session->userdata('id');
    $user    = $this->users->get($id_user);

    $months_days = array("Jan" => 31, "Feb" => 28, "Mar" => 31, "Apr" => 30, "May" => 31, "Jun" => 30, "Jul" => 31, "Aug" => 31, "Sep" => 30, "Oct" => 31, "Nov" => 30, "Dec" => 31);

    $explode  = explode("_", $interval);

    $list_all = false;
    if(count($explode) <= 1){
        $list_all = true;
    } else {
        if (intval($explode[0])) {
          $start = date('Y-m-d H:i:s', strtotime($explode[0].' '.$explode[1].' '.$explode[2]));
          $end   = date('Y-m-d H:i:s', strtotime($explode[4].' '.$explode[5].' '.$explode[6])+86399);
        } else {
          $start = date('Y-m-d H:i:s', strtotime('1 '.$explode[0].' '.$explode[1]));
          $end   = date('Y-m-d H:i:s', strtotime($months_days[$explode[0]].' '.$explode[0].' '.$explode[1])+86399);
        }
    }

    // limit result by admin countries
    if ($user['level'] == 2) {
      $countries = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}")->result();
    } elseif ($user['level'] == 3) {
      $countries = $this->db->query("SELECT country_id AS id_country FROM country_t")->result();
    }

    $ids_countries = array();
    foreach ($countries as $c) {
        $ids_countries[] = $c->id_country;
    }
    $ids_countries = implode(', ', $ids_countries);

    $this->db->select('o.*, oc.coupon, oc.id_user, u.first_name, u.last_name');
    $this->db->from('orders AS o');
    $this->db->join('orders_coupons AS oc', 'oc.order_id=o.order_id');
    $this->db->join('auth_user AS u', 'u.id=oc.id_user');
    $this->db->where("u.id_country IN ({$ids_countries})");
    if($list_all){
        $this->db->where("o.order_id > 0");
    } else {
        $this->db->where("o.order_id > 0 AND o.created BETWEEN '{$start}' AND '{$end}'");
    }
    $this->db->order_by('o.created', 'desc');

    $sales = $this->db->get()->result();

    $data['title'] = 'Sales';
    $data['sales'] = $sales;

    $this->load->view('templates/header', $data);
    $this->load->view('pages/admin/sales', $data);
    $this->load->view('templates/footer');

  }


  public function history($items_per_page = 100, $page = 1){
      $data['admin'] = $this->users->get($this->session->userdata('id'));

      $id_user = $this->session->userdata('id');
      $user    = $this->users->get($id_user);

      if(intval($items_per_page) == 0){
        $items_per_page = 100;
      }
      if(intval($page) == 0){
        $page = 1;
      }

      // limit result by admin countries
      if ($user['level'] == 2) {
        $countries = $this->db->query("SELECT id_country FROM admins_countries WHERE id_user={$id_user}")->result();
      } elseif ($user['level'] == 3) {
        $countries = $this->db->query("SELECT country_id AS id_country FROM country_t")->result();
      }

      $ids_countries = array();
      foreach ($countries as $c) {
          $ids_countries[] = $c->id_country;
      }
      $ids_countries = implode(', ', $ids_countries);

      // $this->db->select('*');
      $this->db->from('orders AS o');
      $this->db->join('orders_coupons AS oc', 'oc.order_id=o.order_id');
      $this->db->join('auth_user AS u', 'u.id=oc.id_user');
      $this->db->where("u.id_country IN ({$ids_countries})");
      $total_sales = $this->db->count_all_results();

      if(intval($items_per_page) == 1){
          $items_per_page = $total_sales;
      }

      $total_pages = ceil($total_sales / $items_per_page);

      $this->db->select('o.total, o.order_id, o.created, c.value AS commission, p.value AS points, oc.id_user, u.first_name, u.last_name');
      $this->db->from('orders AS o');
      $this->db->join('orders_coupons AS oc', 'oc.order_id=o.order_id');
      $this->db->join('auth_user AS u', 'u.id=oc.id_user');
      $this->db->join('commissions AS c', 'c.order_id=o.order_id');
      $this->db->join('points AS p', 'p.order_id=o.order_id', 'left');
      $this->db->where("u.id_country IN ({$ids_countries})");
      $this->db->order_by('o.created', 'desc');
      $this->db->limit($items_per_page, ($page-1)*$items_per_page);
      $sales = $this->db->get()->result();

      $data['title'] = 'Sales';
      $data['sales'] = $sales;
      $data['current_page'] = $page;
      $data['items_per_page'] = $items_per_page;
      $data['total_pages'] = $total_pages;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/admin/sales_history', $data);
      $this->load->view('templates/footer');
  }


}
