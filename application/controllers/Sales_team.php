<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales_team extends CI_Controller
{
    public function __construct() {

        parent::__construct();
        $this->load->model('admin_notification', 'AdminNotification');
        $this->load->model('email_model', 'Email');
        $this->load->model('points_model', 'Points');
        $this->load->model('coupon_model', 'Coupon');
        $this->load->model("site_config");
        $this->load->model('Social_model', 'social');

        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('url');

        if (!$this->authentication->isLogged()) {
                $this->authentication->saveTheLastUrl();
                redirect('/auth/login');
        }    
    }


    public function index() {
        $id_user = $this->session->userdata['id'];
        $data['title'] = 'Coupons';
        $data['user']  = $this->users->get_profile($this->session->userdata('id'));

        $time     = date('Y-m-d G:i:s', strtotime("-1 day", time()));
        $settings = $this->site_config->getGeneralSettings();
        $data['settings'] = $settings;

        $data['slaves']   = $this->db->get_where('auth_user', array('id_master' => $this->session->userdata('id')))->result();
        $data['underkeepers_content'] = $this->db->query("SELECT * FROM content WHERE id=8")->row()->body;
        $data['underkeepers_content'] = str_replace('{UNDERKEEPER_TOTAL}', $data['user']['slaves'], $data['underkeepers_content']);
        $data['underkeepers_content'] = str_replace('{INVITATIONS_LEFT}', intval($data['user']['slaves'] - $data['user']['slave_invitations_sent']), $data['underkeepers_content']);
        $data['last_slaves_request'] = $this->db->query("SELECT * FROM slaves_requests WHERE id_user={$id_user} ORDER BY created DESC")->row();
        $data['invitations'] = $this->db->query("SELECT * FROM invitations WHERE id_user={$id_user} AND approved = 0")->result();

        $data['underkeepers_title'] = $this->db->query("SELECT * FROM content WHERE id=7")->row()->body;

        $data['totals'] = $this->db->query("SELECT COUNT(*) AS sales_no, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id_user}")->row();
        $points = $this->Points->getFromUser($id_user);
        $data['totals']->total_points = $points['total'];
        $data['totals']->bonus = $points['bonus'];
        $data['global_ranking'] = $this->users->getGlobalRanking($id_user);

        $data['active_slaves'] = $this->users->getSlaves($id_user);

        $data['countFollowers'] = $this->social->countFollowers($this->session->userdata('id'));

    	$this->load->view('templates/header', $data);
    	$this->load->view('pages/sales_team');
    	$this->load->view('templates/footer');
    }


    public function slaves() {
        $slaves  = intval($this->input->post('slaves'));
        $id_user = $this->session->userdata['id'];

        $user = $this->users->get($id_user);

        if (intval($user['slave_invitations_sent']) < 5 && $user['slaves'] != 0) {

            $this->session->set_flashdata(
                'invalid_slave_request',
                'You can apply for more slaves when you have used your 5 invitations.'
            );
            redirect('/sales_team');
        }

        if(!$slaves){
            $this->session->set_flashdata(
                'invalid_slave_request',
                'You have to specify the amount of UnderKeepers.'
            );
            redirect('/sales_team');
        }

        $data = array(
            'slaves' => $slaves,
            'id_user' => $this->session->userdata['id']
        );
        $this->db->set('created', 'NOW()', FALSE);
        $this->db->insert('slaves_requests', $data);

        $this->AdminNotification->type  = 5;
        $this->AdminNotification->param = $this->db->insert_id();
        $this->AdminNotification->insert();

        redirect('/sales_team');
    }


    public function invite_slave() {
        $id_user = $this->session->userdata['id'];
        $email = $this->input->post('email');
        $user  = $this->users->get($this->session->userdata['id']);

        if($user['level'] < 1){
            redirect('/sales_team');
        }

        if ($user['slaves'] <= $user['slave_invitations_sent']) {
            $this->session->set_flashdata(
                'error',
                "You have reached your limit for invitations."
              );

            redirect('/sales_team');
        }

        $invitations_sent = $this->db->select("COUNT(*) as sent_invitations")
            ->from("invitations")
            ->where("id_user", $id_user)
            ->get()->row_array();

        $this->db->set('slave_invitations_sent', $invitations_sent['sent_invitations']);
        $this->db->where('id', $id_user);
        $this->db->update('auth_user');

        // obtain hash
        do {
            $hash = md5(time());
            $check = $this->db->query("SELECT * FROM invitations WHERE hash='{$hash}'")->result();
        } while ( count($check) > 0);

        $data = array(
          'id_user' => $user['id'],
          'email'   => $email,
          'hash'    => $hash
        );

        $this->db->set('created', 'NOW()', false);
        $this->db->insert('invitations', $data);

        $this->Email->sendSlaveInvitation($email, $user, $hash);

        $this->session->set_flashdata(
            'success',
            "An invitation has been sent to ".$email
          );

        redirect('/sales_team');
    }

}
