<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maintenance extends CI_Controller
{
  public function __construct() {

    parent::__construct();

    // Until all piecekeepers are migrated.
    redirect('migrated');

  }


  public function index() {

        $data['title'] = "Maintenance";

		$this->load->view('templates/header-maintenance', $data);
		$this->load->view('pages/maintenance');

  }

}
