<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Goals extends CI_Controller
{
  public function __construct() {

    parent::__construct();
    $this->load->model('goal_model', 'Goal');
    $this->load->model('points_model', 'Points');
    $this->load->helper('url');
    $this->load->model('Social_model', 'social');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }

  }


  public function index() {

    $id_user = $this->session->userdata('id');
    $data['user']     = $this->users->get($id_user);
    $data['title']    = 'Goals';

    $goals = $this->Goal->getAllForUser($id_user);

    $data['goals'] = $goals;

    $data['totals'] = $this->db->query("SELECT COUNT(*) AS sales_no, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id_user}")->row();

    $points = $this->Points->getFromUser($id_user);
    $data['totals']->total_points = $points['total'];
    $data['totals']->bonus = $points['bonus'];

    $data['global_ranking'] = $this->users->getGlobalRanking($id_user);
    $data['country_ranking'] = $this->users->getCountryRanking($id_user);
    $data['city_ranking'] = $this->users->getCityRanking($id_user);

    $data['goals_title']   = $this->db->query("SELECT * FROM content WHERE id=9")->row()->body;
    $data['goals_content'] = $this->db->query("SELECT * FROM content WHERE id=10")->row()->body;

    $data['countFollowers'] = $this->social->countFollowers($this->session->userdata('id'));

	$this->load->view('templates/header', $data);
	$this->load->view('pages/goals');
	$this->load->view('templates/footer');
  }


}
