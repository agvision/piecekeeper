<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->model('users');
    $this->load->model('points_model', 'Points');
    $this->load->model('coupon_model', 'Coupon');
    $this->load->model('notification', 'Notification');
    $this->load->model('Social_model', 'social');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }

  }

  public function index()
  {


        $data['title'] = 'Main';
        $data['user']  = $this->users->get($this->session->userdata('id'));

        $data['notifications'] = $this->Notification->getRecent($this->session->userdata('id'), 20);
        // $data['package_notifications'] = $this->Notification->getPackageNotifications($this->session->userdata('id'), 20);
        // $data['new_notifications'] = $this->Notification->countNew($this->session->userdata('id'));

        $months = array("","Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

        $time = strtotime("-1 year", time());
        $date = date('Y-m-d G:i:s', $time);

        $this->load->model("site_config");
        $data['settings'] = $this->site_config->getGeneralSettings();
        $id_user = $this->session->userdata('id');

        // $data['totals'] = $this->db->query("SELECT COUNT(*) as sales_no, SUM(points) as total_points, SUM(bonus) as bonus, SUM(total) as total_earnings FROM sales WHERE id_user=" . )->row();
        $data['totals'] = $this->users->getOrders($id_user);
        $data['totals'] = $data['totals'][0];
        $data['totals']->total_earnings = $data['totals']->revenue;
        $data['totals']->sales_no = $data['totals']->total_sales;

        $points = $this->Points->getFromUser($id_user);
        $data['totals']->total_points = $points['total'];
        $data['totals']->sales_points = $points['sales'];
        $data['totals']->bonus = $points['bonus'];
        $data['totals']->total_earnings = $this->users->getCommission($id_user);

        // total and giftcard earnings
        $data['total_earnings']    = $this->currencies->format($data['totals']->total_earnings);
        $data['giftcard_earnings'] = $this->currencies->format(((100 + intval($data['settings']['giftcard_extra_commission'])) * $data['totals']->total_earnings) / 100);


        // get data for level progress
        $min_width = 30;
        $max_width = 125;
        $sales_width = 0;
        $points_width = 0;
        $sales_goal = 0;
        $points_goal = 0;

        // $user_sales  = $this->users->countSalesBetween($data['user']['id'], $data['user']['date_joined'], date('Y-m-d', time()));
        $user_sales  = $data['totals']->sales_no;
        $points      = $this->users->getPoints($data['user']['id']);
        $user_points = $points['total'];

        if ($data['user']['level'] == -2) {
            $sales_goal  = $data['settings']['sales_level_2'];
            $points_goal = $data['settings']['total_points_level_2'];

            $sales_percent = intval(100*$user_sales/$sales_goal);
            $points_percent = intval(100*$user_points/$points_goal);

            $sales_width  = intval($sales_percent*125/100);
            $points_width = intval($points_percent*125/100);

        } else if ($data['user']['level'] == -1) {
            $sales_goal  = $data['settings']['sales_level_3'];
            $points_goal = $data['settings']['total_points_level_3'];

            $sales_percent = intval(100*$user_sales/$sales_goal);
            $points_percent = intval(100*$user_points/$points_goal);

            $sales_width  = intval($sales_percent*125/100);
            $points_width = intval($points_percent*125/100);
        }

        if ($sales_width < $min_width) {
            $sales_width = $min_width;
        }

        if ($sales_width > $max_width) {
            $sales_width = $max_width;
        }

        if ($points_width < $min_width) {
            $points_width = $min_width;
        }

        if ($points_width > $max_width) {
            $points_width = $max_width;
        }

        $data['global_ranking'] = $this->users->getGlobalRanking($id_user);
        $data['country_ranking'] = $this->users->getCountryRanking($id_user);
        $data['city_ranking'] = $this->users->getCityRanking($id_user);

        $data['sales_goal'] = $sales_goal;
        $data['points_goal'] = $points_goal;
        $data['sales_width'] = $sales_width;
        $data['points_width'] = $points_width;
        $data['user_sales'] = $user_sales;
        $data['user_points'] = $user_points;

        // get blog posts
        $level = $data['user']['level'];
        if ($level == -2) {
            $level = 1;
        }
        elseif ($level == -1) {
            $level = 2;
        }
        elseif ($level >= 0) {
            $level = 3;
        }
        $data['featured_blog'] = $this->db->query("SELECT * FROM blog_entry WHERE display=1 AND level_".$level."=1 ORDER BY creation_date DESC LIMIT 1")->row();
        $data['bottom_left_blog'] = $this->db->query("SELECT * FROM blog_entry WHERE display=2 AND level_".$level."=1 ORDER BY creation_date DESC LIMIT 1")->row();
        $data['bottom_right_blog'] = $this->db->query("SELECT * FROM blog_entry WHERE display=3 AND level_".$level."=1 ORDER BY creation_date DESC LIMIT 1")->row();


        $data['countFollowers'] = $this->social->countFollowers($this->session->userdata('id'));

        $this->load->view('templates/header', $data);
        $this->load->view('pages/main', $data);
        $this->load->view('templates/footer');

  }

  public function ipad(){
        $id_user = $this->session->userdata('id');
        $data['user']     = $this->users->get($id_user);
        $data['title']    = 'Ranking';

        $global_ranking = $this->users->getGlobalRanking($id_user);

        $data['global_ranking'] = $global_ranking;
        $data['totals']         = $this->db->query("SELECT COUNT(*) AS sales_no, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id_user}")->row();

        $points = $this->Points->getFromUser($id_user);
        $data['totals']->total_points = $points['total'];
        $data['totals']->bonus = $points['bonus'];

        $followers     = $this->users->getAllFollowers($id_user);
        $social_credit = $this->users->getSocialCredit($followers);
        $ipad_coupon   = $this->Coupon->getIpadCode($id_user);

        $data['ipad_content'] = $this->db->query("SELECT * FROM content WHERE id=19")->row()->body;
        $data['ipad_content']  = str_replace("{IPAD_COUPON}", $ipad_coupon, $data['ipad_content']);
        $data['ipad_content']  = str_replace("{FOLLOWERS}", $followers, $data['ipad_content']);
        $data['ipad_content']  = str_replace("{SOCIAL_CREDIT}", $social_credit, $data['ipad_content']);

        $data['countFollowers'] = $this->social->countFollowers($this->session->userdata('id'));

        $this->load->view('templates/header', $data);
        $this->load->view('pages/welcome_ipad');
        $this->load->view('templates/footer');
  }

  public function setInactiveNotification(){
        $id_notification = ($this->input->post('id_notification') == NULL) ? FALSE : $this->input->post('id_notification');

        $this->db->set('active', 0, false);
        $this->db->where('id', $id_notification);
        $this->db->update('notifications');

        exit();
  }

  public function clearAllNotifications(){
    $id_user = ($this->session->userdata('id') == NULL) ? FALSE : $this->session->userdata('id');

    $this->db->set('active', 0, false);
    $this->db->where('id_user', $id_user);
    $this->db->update('notifications');
  }

}
