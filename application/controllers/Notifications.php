<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifications extends CI_Controller
{
    public function __construct() {

        parent::__construct();
        $this->load->model('points_model', 'Points');
        $this->load->model('notification', 'Notification');

        if (!$this->authentication->isLogged()) {
                $this->authentication->saveTheLastUrl();
                redirect('/auth/login');
        }
    }

    public function index(){
        redirect('/notifications/history');
    }

    public function history($page = 1) {
        $page = intval($page);
        $page = ($page == 0) ? 1 : $page;
        $id_user = $this->session->userdata('id');
        $user    = $this->users->get($id_user);

        $items_per_page = 20;

        $notifications = $this->Notification->getHistory($id_user, $page, $items_per_page);
        $total_items   = $this->Notification->countAll($id_user);
        $total_pages   = $this->Notification->countPages($id_user, $items_per_page);

        $data['totals'] = $this->db->query("SELECT COUNT(*) AS sales_no, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id_user}")->row();
        $points = $this->Points->getFromUser($id_user);
        $data['totals']->total_points = $points['total'];
        $data['global_ranking'] = $this->users->getGlobalRanking($id_user);

        $data['notifications'] = $notifications;
        $data['total_items']   = $total_items;
        $data['total_pages']   = $total_pages;
        $data['current_page']  = $page;
        $data['user']          = $user;
        $data['title']         = 'Notifications History';

        $this->load->view('templates/header', $data);
        $this->load->view('pages/notifications_history');
        $this->load->view('templates/footer');
    }

}
