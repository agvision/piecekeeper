<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migrated extends CI_Controller
{
  public function __construct() {

    parent::__construct();

    // Until all piecekeepers are migrated.
    //redirect('/');

  }


  public function index() {

        $data['title'] = "Piecekeeper";

		$this->load->view('templates/header-maintenance', $data);
		$this->load->view('pages/migrated');

  }

}
