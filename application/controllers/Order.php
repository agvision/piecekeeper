<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller
{
    public function __construct() {

        parent::__construct();
        $this->load->model('admin_notification', 'AdminNotification');
        $this->load->model('email_model', 'Email');
        $this->load->model('Orders', 'Orders');
        $this->load->model('Webshop_model', 'WebShop');

        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('url');

        if (!$this->authentication->isLogged()) {
                $this->authentication->saveTheLastUrl();
                redirect('/auth/login');
        }
    }


    public function index() {
    }

    public function view($id_order) {

        $id_user = $this->session->userdata['id'];

        $data['order'] = $this->WebShop->getOrderById($id_order);

        if(!$data['order']) {
            redirect("/");
        }

        $data['title'] = 'Order';
        $data['user']  = $this->users->get_profile($this->session->userdata('id'));

        $this->load->view('templates/header', $data);
        $this->load->view('pages/order_view');
        $this->load->view('templates/footer');
    }
}
