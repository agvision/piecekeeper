<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stats extends CI_Controller
{
    public function __construct() {

        parent::__construct();
        $this->load->model('admin_notification', 'AdminNotification');
        $this->load->model('email_model', 'Email');
        $this->load->model('points_model', 'Points');
        $this->load->model('stats_model', 'Stats');
        $this->load->model("site_config");
        $this->load->model('Social_model', 'social');

        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('url');

        if (!$this->authentication->isLogged()) {
                $this->authentication->saveTheLastUrl();
                redirect('/auth/login');
        }     
    }


    public function index() {
        $id_user = $this->session->userdata['id'];
        $data['title'] = 'Stats';
        $data['user']  = $this->users->get_profile($this->session->userdata('id'));

        $time     = date('Y-m-d G:i:s', strtotime("-1 day", time()));
        $settings = $this->site_config->getGeneralSettings();
        $data['settings'] = $settings;

        $data['slaves']   = $this->db->get_where('auth_user', array('id_master' => $this->session->userdata('id')))->result();
        $data['orders'] = $this->users->getSalesStats($this->session->userdata('id'), strtotime($this->input->get('begin')), strtotime($this->input->get('end')));
        $data['total_commission'] = $this->users->getTotalCommission($id_user);
        $data['chart_sales_stats'] = $this->Stats->getCommissionsStatsByUser($id_user, strtotime($this->input->get('begin')), strtotime($this->input->get('end')));

        if ($data['user']['level'] >= 1) {
            $sales_underkeepers = $this->Stats->getUnderkeepersCommissionStats($id_user, strtotime($this->input->get('begin')), strtotime($this->input->get('end')));

            // bild array for charts
            $data['chart_sales_underkeepers']['total'] = $sales_underkeepers['total'];
            $data['chart_sales_underkeepers']['graph_title'] = $sales_underkeepers['graph_title'];
            $data['chart_sales_underkeepers']['y_axis_label'] = $sales_underkeepers['y_axis_label'];

            if (isset($sales_underkeepers['keys'])) {
                foreach ($sales_underkeepers['keys'] as $key => $value) {
                    $data['chart_sales_underkeepers']['keys'][$key] = $sales_underkeepers['keys'][$key];
                    $data['chart_sales_underkeepers']['values'][$key] = $sales_underkeepers['values'][$key];
                    if (count($data['chart_sales_underkeepers']['keys']) == 12) {
                        break;
                    }
                }
            }

            $data['sales_underkeepers'] = $sales_underkeepers;
        }

        $data['totals'] = $this->db->query("SELECT COUNT(*) AS sales_no, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id_user}")->row();
        $points = $this->Points->getFromUser($id_user);
        $data['totals']->total_points = $points['total'];
        $data['totals']->bonus = $points['bonus'];

        $data['global_ranking'] = $this->users->getGlobalRanking($id_user);

        $data['countFollowers'] = $this->social->countFollowers($this->session->userdata('id'));

    	$this->load->view('templates/header', $data);
    	$this->load->view('pages/stats');
    	$this->load->view('templates/footer');
    }

}
