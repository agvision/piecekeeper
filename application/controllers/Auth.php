<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller
{
  function __construct()
  {
    parent::__construct();

    $this->authentication->isMaintenance();
    /* Facebook Stuff */
    parse_str( $_SERVER['QUERY_STRING'], $_REQUEST );
    $CI = & get_instance();
    $CI->config->load("facebook",TRUE);
    $config = $CI->config->item('facebook');
    $this->load->library('Facebook', $config);



    $this->load->library('form_validation');

    $this->load->helper('form');
    $this->load->helper('url');

    $this->load->model('admin_notification', 'AdminNotification');
    $this->load->model('email_model', 'Email');
    $this->load->model('coupon_model', 'Coupon');
    $this->load->model('auth_logs_model', 'auth_logs');
    $this->load->model('front_image_model', 'FrontImage');
    $this->load->model("Site_config");
    $this->load->model('users');
    $this->load->model('Questions_model','questions');
    $this->load->model('country','country');
    $this->load->model('School');
  }

  function index()
  {
    redirect('/welcome');
  }

  public function forgot_password($hash=false) {
      if ($hash) {

          $user = $this->db->query("SELECT * FROM auth_user WHERE hash='{$hash}'")->row();
          if (count($user) == 0) {
              redirect('/');
          }

          if (isset($_POST['trigger']) && $_POST['trigger'] == 'reset') {
              $this->form_validation->set_rules('password', 'Password', 'required');

              if ($this->form_validation->run() == true) {
                  $password = ($this->input->post('password') == NULL) ? FALSE : $this->input->post('password');
                  $password = password_hash($password, PASSWORD_DEFAULT);

                  $this->db->set('password', $password);
                  $this->db->where('hash', $hash);
                  $this->db->update('auth_user');

                  $this->session->set_flashdata(
                    'success',
                    'Now you can login with your new password.'
                  );

                  // Delete their failed login attempts if they changed their password
                  $this->users->resetLoginAttempts();

                  redirect('auth/login');
              }
          }


          $data['hash']  = $hash;
          $data['title'] = 'Forgot Password';
          $this->load->view('templates/header', $data);
          $this->load->view('pages/forgot-reset', $data);
          $this->load->view('templates/footer');
      } else {

          if (isset($_POST['trigger']) && $_POST['trigger'] == 'send') {
              $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

              if ($this->form_validation->run() == true) {
                  $email = $this->input->post('email');

                  $user = $this->db->query("SELECT * FROM auth_user WHERE email='{$email}'")->row();

                  if (count($user)) {
                      $link = base_url('auth/forgot_password/'.$user->hash);
                      $this->Email->sendRecoveryLink($email, $link);
                  }

                  $this->session->set_flashdata(
                    'success',
                    'A link for password recovery has been sent to your email.'
                  );

                  redirect('auth/forgot_password');
              }

          }

          $data['title'] = 'Forgot Password';
          $this->load->view('templates/header', $data);
          $this->load->view('pages/forgot-email', $data);
          $this->load->view('templates/footer');
      }

  }

  public function register(){

    $data['countries'] = $this->country->getAllCountries();
    $data['front_image'] = $this->FrontImage->getRandom();
    $data['agent'] = $this->agent;
    $data['states'] = $this->db->query("SELECT * FROM states")->result();
    $data['show_fb_register'] = true;

    $data['months'] = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

    $this->data['title'] = 'Register';

    $geo_data = unserialize($this->curl->simple_get('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));

    $data['user_country'] = $geo_data['geoplugin_countryCode'];

    if($this->input->get('i')){
        $this->session->set_userdata('i', $this->input->get('i'));
        $result_row = $this->db->query("SELECT * FROM invitations WHERE `hash` = '".$this->input->get('i')."' ")->result();

        if( isset( $result_row[0] ) ) {
          $inviter_row = $result_row[0];
          $data['inviterProfile'] = $this->users->get_profile($inviter_row->id_user);
        }

    }

    $data['questions'] = $this->questions->getAll();

    $this->form_validation->set_rules("first_name", "First Name", 'required');
    $this->form_validation->set_rules("last_name", "Last Name", 'required');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[auth_user.email]');
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('day', 'Day of Birthday', 'required');
    $this->form_validation->set_rules('address1', 'Address', 'required');
    $this->form_validation->set_rules('month', 'Month of Birthday', 'required');
    $this->form_validation->set_rules('year', 'Year oh Birthday', 'required');
    $this->form_validation->set_rules('country', 'Country', 'required');
    $this->form_validation->set_rules('city', 'City', 'required');
    $this->form_validation->set_rules('terms', 'Terms and Conditions', 'required');
    $this->form_validation->set_rules('phone', 'Phone', 'required');

    $this->form_validation->set_rules('profile_pic', 'Profile Image', 'file_required|file_size_max[2MB]');
    $this->form_validation->set_rules('profile_pic_2', 'Profile Image', 'file_size_max[2MB]');

    $email = $this->input->post('email');
    $account = $this->db->query("SELECT * FROM auth_user WHERE email='{$email}'")->row();
    if (count($account) && !$account->completed) {
        $this->db->where('email', $email);
        $this->db->delete('auth_user');
    }

    if ($this->form_validation->run() == true) {
        $this->users->register();

        $id_user = $this->db->insert_id();

        $pic1 = $this->users->uploadImage($_FILES, 'profile_pic');

        if ($pic1) {
          $this->db->where('id', $id_user);
          $this->db->set('profile_pic', "'".$pic1."'", false);
          $this->db->update('auth_user');
        }

        $pic2 = $this->users->uploadImage($_FILES, 'profile_pic_2');

        if ($pic2) {
          $this->db->where('id', $id_user);
          $this->db->set('profile_pic_2', "'".$pic2."'", false);
          $this->db->update('auth_user');
        }

        $this->AdminNotification->type  = 1;
        $this->AdminNotification->param = $id_user;
        $this->AdminNotification->insert();

        $this->session->set_flashdata(
          'success',
          'Wait for Admin approval.'
        );

        $j = 0;

        while($this->input->post('question-ref-'.$j)) {

          $q_id = $this->input->post('question-ref-'.$j);
          $q_value = $this->input->post('question-'.$q_id);

          $this->questions->save_answer($id_user,$q_id,$q_value);

          $j++;
        }

        $this->session->set_userdata('registered_user_id', $id_user);

        if ($this->session->userdata("fb_register")) {
          redirect('profile/facebook?next=social');
        } else {
          redirect('/social');
        }

    } else {


        if($this->session->userdata("fb_data"))
        {
            $data['post'] = $this->session->userdata("fb_data");
            $data['show_fb_register'] = false;
            $this->session->unset_userdata("fb_data");
        }
        else
        {
            $data['post']  = $this->input->post();
        }

        // if($this->input->post("trigger") == "register"){
        //   $data['post']  = $this->input->post();
        // } else {
        //   if($this->session->userdata("fb_data")){
        //     $data['post'] = $this->session->userdata("fb_data");
        //     $data['show_fb_register'] = false;
        //     $this->session->unset_userdata("fb_data");
        //   }
        // }

        $data['title'] = 'Register';
        $this->load->view('templates/header-front', $data);
        $this->load->view('pages/register', $data);
        $this->load->view('templates/footer-front');
    }

  }

  public function ipad(){
      $query = $this->db->query("SELECT * FROM country_t");

      $data['countries'] = $query->result();
      $data['front_image'] = $this->FrontImage->getRandom();
      $data['agent'] = $this->agent;
      $data['states'] = $this->db->query("SELECT * FROM states")->result();
      $data['show_fb_register'] = true;

      $data['months'] = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

      $this->data['title'] = 'Register';

      if($this->input->get('i')){
          $this->session->set_userdata('i', $this->input->get('i'));
      }

      $this->session->set_userdata('ipad', 'true');

      $this->form_validation->set_rules("first_name", "First Name", 'required');
      $this->form_validation->set_rules("last_name", "Last Name", 'required');
      $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[auth_user.email]');
      $this->form_validation->set_rules('password', 'Password', 'required');
      $this->form_validation->set_rules('day', 'Day of Birthday', 'required');
      $this->form_validation->set_rules('address1', 'Address', 'required');
      $this->form_validation->set_rules('month', 'Month of Birthday', 'required');
      $this->form_validation->set_rules('year', 'Year oh Birthday', 'required');
      $this->form_validation->set_rules('country', 'Country', 'required');
      $this->form_validation->set_rules('city', 'City', 'required');
      $this->form_validation->set_rules('terms', 'Terms and Conditions', 'required');
      $this->form_validation->set_rules('phone', 'Phone', 'required');

      // $this->form_validation->set_rules('profile_pic', 'Profile Image', 'file_required|file_size_max[2MB]');
      // $this->form_validation->set_rules('profile_pic_2', 'Profile Image', 'file_size_max[2MB]');

      $email = $this->input->post('email');
      $account = $this->db->query("SELECT * FROM auth_user WHERE email='{$email}'")->row();
      if (count($account) && !$account->completed) {
          $this->db->where('email', $email);
          $this->db->delete('auth_user');
      }

      if ($this->form_validation->run() == true) {
          $this->users->register();

          $id_user = $this->db->insert_id();

          // $pic1 = $this->users->uploadImage($_FILES, 'profile_pic');

          // if ($pic1) {
          //   $this->db->where('id', $id_user);
          //   $this->db->set('profile_pic', "'".$pic1."'", false);
          //   $this->db->update('auth_user');
          // }

          // $pic2 = $this->users->uploadImage($_FILES, 'profile_pic_2');

          // if ($pic2) {
          //   $this->db->where('id', $id_user);
          //   $this->db->set('profile_pic_2', "'".$pic2."'", false);
          //   $this->db->update('auth_user');
          // }

          $this->AdminNotification->type  = 1;
          $this->AdminNotification->param = $id_user;
          $this->AdminNotification->insert();

          $this->session->set_flashdata(
            'success',
            'Wait for Admin approval.'
          );

          $this->session->set_userdata('registered_user_id', $id_user);

          if ($this->session->userdata("fb_register")) {
            redirect('profile/facebook?next=social');
          } else {
            redirect('/social');
          }

      } else {

          if($this->input->post("trigger") == "ipad"){
            $data['post']  = $this->input->post();
          } else {
            if($this->session->userdata("fb_data")){
              $data['post'] = $this->session->userdata("fb_data");
              $data['show_fb_register'] = false;
              $this->session->unset_userdata("fb_data");
            }
          }


      $geo_data = unserialize($this->curl->simple_get('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));

      $data['user_country'] = $geo_data['geoplugin_countryCode'];

      $data['title'] = 'Register';
      $this->load->view('templates/header-front', $data);
      $this->load->view('pages/ipad', $data);
      $this->load->view('templates/footer-front');
    }
  }


  public function pku_email() {
      $data['front_image'] = $this->FrontImage->getRandom();
      $data['title'] = 'PKU Email';

      $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[auth_user.email]');

      if ($this->form_validation->run()) {

          $this->Email->sendPKUEmailPage($this->input->post("email"));

          $this->session->set_flashdata(
            'success',
            'Email sent.'
          );

          redirect("/pku");
      }

      $this->load->view('templates/header-front-nobar', $data);
      $this->load->view('pages/pku-email', $data);
      $this->load->view('templates/footer-front');
  }


  public function pku(){
      $query = $this->db->query("SELECT * FROM country_t");

      $data['countries'] = $query->result();
      $data['front_image'] = $this->FrontImage->getRandom();
      $data['agent'] = $this->agent;
      $data['states'] = $this->db->query("SELECT * FROM states")->result();
      $data['show_fb_register'] = true;

      $data['months'] = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

      $this->data['title'] = 'Register';

      if($this->input->get('i')){
          $this->session->set_userdata('i', $this->input->get('i'));
      }

      $this->session->set_userdata('pku', 'true');

      $this->form_validation->set_rules("first_name", "First Name", 'required');
      $this->form_validation->set_rules("last_name", "Last Name", 'required');
      $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[auth_user.email]');
      $this->form_validation->set_rules('password', 'Password', 'required');
      $this->form_validation->set_rules('day', 'Day of Birthday', 'required');
      $this->form_validation->set_rules('address1', 'Address', 'required');
      $this->form_validation->set_rules('month', 'Month of Birthday', 'required');
      $this->form_validation->set_rules('year', 'Year oh Birthday', 'required');
      $this->form_validation->set_rules('country', 'Country', 'required');
      $this->form_validation->set_rules('city', 'City', 'required');
      $this->form_validation->set_rules('terms', 'Terms and Conditions', 'required');
      $this->form_validation->set_rules('phone', 'Phone', 'required');
      $this->form_validation->set_rules('pku_campus_involvement', 'Campus Involvement', 'required');
      // $this->form_validation->set_rules('pku_resume', 'Resume', 'file_required');

      $email = $this->input->post('email');
      $account = $this->db->query("SELECT * FROM auth_user WHERE email='{$email}'")->row();
      if (count($account) && !$account->completed) {
          $this->db->where('email', $email);
          $this->db->delete('auth_user');
      }

      if ($this->form_validation->run() == true) {
          $this->users->register();

          $id_user = $this->db->insert_id();
          $this->db->where('id', $id_user);

          $this->db->set("pku_campus_involvement", $this->input->post("pku_campus_involvement"));
          $this->db->update('auth_user');

          $this->AdminNotification->type  = 1;
          $this->AdminNotification->param = $id_user;
          $this->AdminNotification->insert();

          $this->session->set_flashdata(
            'success',
            'Wait for Admin approval.'
          );

          $this->session->set_userdata('registered_user_id', $id_user);

          if ($this->session->userdata("fb_register")) {
            redirect('profile/facebook?next=social');
          } else {
            redirect('/social');
          }

      } else {

          if($this->input->post("trigger") == "pku"){
            $data['post']  = $this->input->post();
          } else {
            if($this->session->userdata("fb_data")){
              $data['post'] = $this->session->userdata("fb_data");
              $data['show_fb_register'] = false;
              $this->session->unset_userdata("fb_data");
            }
          }


      $geo_data = unserialize($this->curl->simple_get('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));

      $data['user_country'] = $geo_data['geoplugin_countryCode'];

      $data['title'] = 'Register';
      $this->load->view('templates/header-front-nobar', $data);
      $this->load->view('pages/pku-signup', $data);
      $this->load->view('templates/footer-front');
    }
  }


  public function fb_register($redirect = null){
      $fb_user_id = $this->facebook->getUser();
      $fb_data = array();

      if($fb_user_id == 0){
          $fb_url = $this->facebook->getLoginUrl(array('scope'=>'email,manage_pages,user_birthday'));
          redirect($fb_url);
      } else {
          $me = $this->facebook->api('/me');

          if (isset($me['birthday'])) {
            $date = DateTime::createFromFormat("m/d/Y", $me['birthday']);
            $fb_data['day']   = $date->format("j");
            $fb_data['month'] = $date->format("n");
            $fb_data['year']  = $date->format("Y");
          }

          if(isset($me['email'])){
            $fb_data['email'] = $me['email'];
          }
          if(isset($me['first_name'])){
            $fb_data['first_name'] = $me['first_name'];
          }
          if(isset($me['last_name'])){
            $fb_data['last_name'] = $me['last_name'];
          }

          if(isset($me['location']) && isset($me['location']['name'])){
            $data = explode(", ", $me['location']['name']);
            if(count($data) == 2){
              $fb_data['city'] = $data[0];
              if($this->users->isCountry($data[1])){
                $fb_data['country'] = $this->users->getCountryId($data[1])."_".$data[1];
              }
              else if($this->users->isState($data[1])){
                $fb_data['country'] = "236_United States";
                $fb_data['state'] = $data[1];
              }
            }
          }

          if(isset($me['education']) && count($me['education'])){
            $i = count($me['education']) - 1;
            if(isset($me['education'][$i]) && isset($me['education'][$i]['school']) && isset($me['education'][$i]['school']['name'])){
              $fb_data['school'] = $me['education'][$i]['school']['name'];
              $fb_data['check_school'] = "Yes";

              if (isset($me['education'][$i]['year']) && isset($me['education'][$i]['year']['name'])) {
                $fb_data['graduation_year'] = $me['education'][$i]['year']['name'];
              }
            }
          }
          if(isset($me['gender'])){
            if($me['gender'] == 'male'){
              $fb_data['gender'] = 1;
            } else {
              $fb_data['gender'] = 0;
            }
          }

          $this->session->set_userdata("fb_data", $fb_data);
          $this->session->set_userdata("fb_register", true);
          if ($redirect && $redirect == "ipad") {
              redirect("/ipad");
          }
          else if ($redirect && $redirect == "pku") {
              redirect("/pku-signup");
          }
          else {
              redirect("/auth/register");
          }
      }
  }

  public function thankyou($hash = false){
      if(!$hash){
        redirect("/");
      }

      $user    = $this->db->query("SELECT * FROM auth_user WHERE hash='{$hash}'")->row();
      $id_user = $user->id;

      $data['followers']     = $this->users->getAllFollowers($id_user);
      $data['social_credit'] = $this->users->getSocialCredit($data['followers']);
      $data['ipad_coupon']   = $this->Coupon->getIpadCode($id_user);

      $data['title'] = "Thank You";
      $data['front_image'] = $this->FrontImage->getRandom();

      $this->load->view('templates/header-front', $data);
      $this->load->view('pages/thankyou', $data);
      $this->load->view('templates/footer-front');
  }

  /*
   * Global login function to auth the user and redirect to proper area
   *
   */
  public function login()
  {

    // $pk_ids     = $this->curl->simple_get("https://www.brandbassador.com/MigratedUsers/getMigratedUsers");
    // $pk_ids     = json_decode($pk_ids);

    $this->data['title'] = 'Login';

    $data['agent'] = $this->agent;
    $data['front_image'] = $this->FrontImage->getRandom();

    $this->form_validation->set_rules('email', 'Email', 'required');
    $this->form_validation->set_rules('password', 'Password', 'required');

    if ($this->form_validation->run() == true) {

      $client_ip = $_SERVER["REMOTE_ADDR"];


      $id_user = $this->db->query("SELECT id FROM auth_user WHERE email = '".$this->input->post('email')."'")->row();

    //   if(in_array($id_user->id, $pk_ids))
    //   {
    //       $this->session->set_flashdata(
    //         'error',
    //         "Piecekeeper just got a lot better! Please access the new updated Piecekeeper system powered by Brandbassador through this link <a href='https://www.brandbassador.com/login' >Brandbassador</a>."
    //       );
      //
    //       redirect('auth/login');
    //   }

      if($this->auth_logs->blocked($client_ip))
      {
        // the ip is blocked

        $this->Email->sendBlockedAttempt($client_ip,$this->input->post('email'));

        $this->session->set_flashdata(
          'error',
          "You've reached the maximum number of login attempts!<br/> Please use the <a href='" . base_url('auth/forgot_password') . "' >Forgot Password </a> feature."
        );
        redirect('auth/login');
      }
      else
      {
         $remaining_login_attempts =  $this->auth_logs->lastLoginAttempt($client_ip);
      }

      if($this->users->isPendingUser($this->input->post('email')))
      {
          $this->session->set_flashdata(
            'error',
            "You are currently being reviewed for approval, we will notify you when you will be approved."
          );

          redirect('auth/login');
      }
      elseif ($user = $this->users->login($this->input->post('email'), $this->input->post('password')))
      {

          $this->db->where('id', $user['id']);
              $this->db->update('auth_user', array('last_login' => date('Y-m-d H:i:s', time())));

          $data = array(
            'id'    => $user['id'],
            'email' => $user['email'],
            'level' => $user['level']
          );
          $this->session->set_userdata($data);

          // refresh invalid email sent
          $this->db->set('inactive_email_sent', 0, false);
          $this->db->where('id', $user['id']);
          $this->db->update('auth_user');

          $this->auth_logs->add_attempt($client_ip, 1 );

          if ($user['terms_accepted']) {

              //if I have accesed a valid page before redirect to that page
              if($this->session->userdata('lastUrl'))
              {
                $urlForRedirect = $this->session->userdata('lastUrl');

                //unset the session
                $this->session->unset_userdata('lastUrl');

                redirect($urlForRedirect);
              }

              redirect('/');
          } else {
              redirect('/accept_terms');
          }

      } else {

         if($remaining_login_attempts == true)
         {
             // login was un-successfull
             $this->session->set_flashdata(
               'error',
               'Your login attempt failed. You have one login attempt left. <a href="'.base_url('auth/forgot_password').'">Forgot Password?</a>'
             );
         }
         else
         {
             // login was un-successfull
             $this->session->set_flashdata(
               'error',
               'Your login attempt failed. <a href="'.base_url('auth/forgot_password').'">Forgot Password?</a>'
             );
         }

        $this->auth_logs->add_attempt($client_ip, 0 );

        redirect('auth/login');
      }
    } else {
      // display the login page
      $data['title'] = 'Login';
      $this->load->view('templates/header-front', $data);
      $this->load->view('pages/login', $data);
      $this->load->view('templates/footer-front');
    }

  }


  public function finish_register($hash = false) {
      if(!$hash){
        redirect("/");
      }

      $user = $this->db->query("SELECT * FROM auth_user WHERE hash='{$hash}'")->row();

      if($user->completed)
      {

          $this->session->sess_destroy();

          $this->session->set_flashdata(
            'success',
            'Register completed. Wait for Admin approval.'
          );

          $this->session->set_userdata('i', '');
      }

      redirect('/auth/login');
  }


  public function finish_register_ipad($hash = false) {
      if(!$hash){
        redirect("/");
      }

      $user = $this->db->query("SELECT * FROM auth_user WHERE hash='{$hash}'")->row();

      if($user->completed){

          $this->session->sess_destroy();

          $this->session->set_userdata('i', '');
          $this->session->set_userdata('ipad', '');
      }

      redirect('/auth/thankyou/'.$hash);
  }


  public function finish_register_pku($hash = false) {
      if(!$hash){
        redirect("/");
      }

      $user = $this->db->query("SELECT * FROM auth_user WHERE hash='{$hash}'")->row();

      if($user->completed){

          $this->session->sess_destroy();

          $this->session->set_flashdata(
            'success',
            'Register completed. Wait for Admin approval.'
          );

          $this->session->set_userdata('i', '');
          $this->session->set_userdata('pku', '');
      }

      redirect('/pku-signup');
  }


  public function surf($hash) {
    $user = $this->db->query("SELECT * FROM auth_user WHERE hash='{$hash}'")->row();
    $admin = $this->users->get($this->session->userdata('id'));

    $back_to_admin = $this->input->get('back');

    if ($back_to_admin) {
        $this->session->unset_userdata('admin_hash', $admin['hash']);
    } else {
        $this->session->set_userdata('admin_hash', $admin['hash']);
    }

    if ($auth_user = $this->users->surf_login($user->email)) {
        $data = array(
          'id'    => $auth_user['id'],
          'email' => $auth_user['email'],
          'level' => $auth_user['level']
        );

        $this->session->set_userdata($data);
    }

    redirect('/');
  }


  public function logout()
  {

    $this->session->sess_destroy();
    redirect('/');
  }

  public function get_cities_by_country() {
    $id_country = $this->input->post('id_country');
    $state      = $this->input->post('state');
    $id_user = $this->session->userdata('id');
    $user    = $this->users->get($id_user);

    $data = $this->users->getCitiesByCountries($id_country, $state, $id_user, $user);

    $results = array();

    foreach ($data as $d) {
        $results[] = $d->city;
    }

    echo json_encode($results);
    exit();
  }

  public function get_schools_by_country() {
    $id_country = $this->input->post('id_country');
    $id_user = $this->session->userdata('id');
    $user    = $this->users->get($id_user);

    $results = $this->School->getSchoolByCountry($id_country, $id_user, $user);

    echo json_encode($results);
    exit();
  }

  public function get_schools_by_city() {
      $city = $this->input->post('city');
      $id_user = $this->session->userdata('id');
      $user    = $this->users->get($id_user);

      $results = $this->School->getSchoolByCity($city,$id_user,$user);

      echo json_encode($results);
      exit();
  }

  public function get_country_prefix() {
      $country = $this->input->post('country');
      $code    = $this->db->query("SELECT * FROM dial_codes WHERE country='{$country}'")->row();
      if (count($code)) {
          $prefix = "+".$code->code;
      } else {
          $prefix = '';
      }

      echo $prefix;
      exit();
  }

}

?>
