<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Social extends CI_Controller
{
  public function __construct() {

    parent::__construct();
    $this->load->model('admin_notification', 'AdminNotification');
    $this->load->model('social_model', 'Social');
    $this->load->model('front_image_model', 'FrontImage');
    $this->load->model('points_model', 'Points');
    $this->load->helper('url');

    /* Facebook Stuff */
    parse_str( $_SERVER['QUERY_STRING'], $_REQUEST );
    $CI = & get_instance();
    $CI->config->load("facebook",TRUE);
    $config = $CI->config->item('facebook');
    $this->load->library('Facebook', $config);

    if ($this->authentication->isLogged()) {
        redirect('/welcome');
    }
  }

  public function index() {

    if($this->session->userdata("ipad") && $this->session->userdata("ipad") == 'true'){
        $data['ipad'] = true;
    }

    if($this->session->userdata("pku") && $this->session->userdata("pku") == 'true'){
        $data['pku'] = true;
    }

    $total = 0;

    // If the hash parameter exists, display social accounts linking for that user
    $hash = $this->input->get('hash');

    if($hash)
    {
        // Get all the users who haven't been accepted yet
        $query = $this->db->query("SELECT id FROM auth_user WHERE hash='{$hash}' AND is_active = 0")->row();

        if($query)
        {
            $this->session->set_userdata(['registered_user_id' => $query->id]);
            $this->session->set_userdata(['next' => 'social']);
        }
        else
        {
            $this->session->sess_destroy();
        }
    }


    if ($this->session->userdata('registered_user_id')) {
        $data['agent'] = $this->agent;

        $id_user = $this->session->userdata('registered_user_id');

        $data['user']     = $this->users->get($id_user);
        $data['title']    = 'Social Networks';

        $linked = $this->Social->countFollowers($id_user);
        $data['linked'] = $linked;

        // Complete user registration if at least one social account was linked
        if($data['linked'] > 0 && $data['user']['completed'] == 0 && !empty($data['user']['hash']))
        {
            if (isset($data['ipad']) && $data['ipad'])
            {
                $this->users->completeRegistrationIpad($data['user']['hash']);
            }
            elseif (isset($data['pku']) && $data['pku'])
            {
                $this->users->completeRegistrationPKU($data['user']['hash']);
            }
            else
            {
                $this->users->completeRegistration($data['user']['hash']);
            }
        }

        /* Facebook data */
        $fb_friends = $this->Social->checkNetwork('facebook', $id_user);
        if ($fb_friends || $fb_friends === "0") {
            $data['fb_friends'] = $fb_friends;
            $total += $fb_friends;
        }

        $this->db->select("*");
        $this->db->from('social_accounts');
        $this->db->where('id_user', $id_user);
        $this->db->where('network', 1);

        $result = $this->db->get()->result();
        $data['facebook_linked'] = empty($result) ? false : true;

        /* Get facebook pages */
        $fb_pages = array();
        if($this->facebook->getUser()){

            $pages = $this->facebook->api('/me/accounts');
            if(isset($pages['data']) && count($pages['data'])){
                foreach ($pages['data'] as $k => $p) {
                    $fb_pages[$k]["name"] = $p['name'].' - '.$p['category'];
                    $fb_pages[$k]["id"] = $p['id'];
                }
            }

        }
        $data["fb_pages"] = $fb_pages;

        /* Twitter data */
        $tw_followers = $this->Social->checkNetwork('twitter', $id_user);
        if ($tw_followers || $tw_followers === "0") {
            $data['tw_followers'] = $tw_followers;
            $total += $tw_followers;
        }

        /* Instagram data */
        $inst_followers = $this->Social->checkNetwork('instagram', $id_user);
        if ($inst_followers || $inst_followers === "0") {
            $data['inst_followers'] = $inst_followers;
            $total += $inst_followers;
        }

        /* Tumblr Data */
        $tub_followers = $this->Social->checkNetwork('tumblr', $id_user);
        if ($tub_followers || $tub_followers === "0") {
            $data['tub_followers'] = $tub_followers;
            $total += $tub_followers;
        }

        /* Pinterest Data */
        $pin_followers = $this->Social->checkNetwork('pinterest', $id_user);
        if ($pin_followers || $pin_followers === "0") {
            $data['pin_followers'] = $pin_followers;
            $total += $pin_followers;
        }

        /* Linkedin Data */
        $ln_followers = $this->Social->checkNetwork('linkedin', $id_user);
        if ($ln_followers || $ln_followers === "0") {
            $data['ln_followers'] = $ln_followers;
            $total += $ln_followers;
        }

        /* YouTube Data */
        $yb_followers = $this->Social->checkNetwork('youtube', $id_user);
        if ($yb_followers || $yb_followers === "0") {
            $data['yb_followers'] = $yb_followers;
            $total += $yb_followers;
        }

        /* Vine Data */
        $vn_followers = $this->Social->checkNetwork('vine', $id_user);
        if ($vn_followers || $vn_followers === "0") {
            $data['vn_followers'] = $vn_followers;
            $total += $vn_followers;
        }

        if($total > 0)
        {
            $this->db->set('completed', 1);
            $this->db->where('id', $id_user);
            $this->db->update('auth_user');
        }

        $data['social_credit'] = $this->users->getSocialCredit($total);
        $data['front_image'] = $this->FrontImage->getRandom();
        $data['total_followers'] = $total;


        if ($this->session->userdata("pku") && $this->session->userdata("pku") == 'true') {
            $this->load->view('templates/header-front-nobar', $data);
        } else {
            $this->load->view('templates/header-front', $data);
        }

        $this->load->view('pages/social');
        $this->load->view('templates/footer-front');

    } else {
        redirect('/auth/login');
    }
  }

   public function facebook_page($page_id = false){
        if(!$page_id){
            redirect('/social');
        }
        $id_user = $this->session->userdata('registered_user_id');
        $fb_page = $this->facebook->api('/'.$page_id);
        $fb_profile = $fb_page['link'];
        $fb_likes   = isset($fb_page['likes']) ? $fb_page['likes'] : 0;
        $this->Social->updateNetwork('facebook', $id_user, $fb_likes, $fb_profile);
        redirect('/social');
    }

    public function facebook_update(){
        $this->facebook->destroySession();
        redirect('/social/facebook_update_cb');
    }

    public function facebook_update_cb(){
        $fb_user_id = $this->facebook->getUser();
        if($fb_user_id == 0){
            $fb_url = $this->facebook->getLoginUrl(array('scope'=>'email,manage_pages'));
            redirect($fb_url);
        } else {
            redirect('/social');
        }
    }


}
