<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->library('email');
    $this->load->model('users');
    $this->load->model("Site_config");

          $this->authentication->isMaintenance();
  }

  public function index()
  {
      $data['title'] = 'Contact';

      $settings = $this->Site_config->getGeneralSettings();

      if(isset($_POST['trigger']) && $_POST['trigger'] == 'send'){
          $this->email->from('piecekeepers@onepiece.com');
          $this->email->to($settings['contact_email']);
          $this->email->set_mailtype("html");

          $this->email->subject($this->input->post('subject'));
          $this->email->message("<b>From:</b> " . $this->input->post('name') . " (" . $this->input->post('email') . ") <br />" . $this->input->post('message'));

          $this->email->send();
          $this->session->set_flashdata(
              'sent',
              'Your message has been sent.'
            );
      }

      if ($this->session->userdata('id') ) {
          $data['logged'] = true;
      }

      $data['contact_message'] = $this->db->query("SELECT * FROM content WHERE id=15")->row()->body;

      $this->load->view('templates/header', $data);
      $this->load->view('pages/contact', $data);
      $this->load->view('templates/footer');

  }

}
