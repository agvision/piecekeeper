<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Events extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('event_model', 'Event');
    $this->load->model('admin_notification', 'AdminNotification');
    $this->load->helper('url');
  }

  function index()
  {
    $data['user'] = $this->users->get($this->session->userdata('id'));
    $data['title'] = 'Events';
    $data['events']  = $this->Event->getRecentEvents();

		$this->load->view('templates/header', $data);
		$this->load->view('pages/events');
		$this->load->view('templates/footer');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }

  }

  public function add() {
    $id_user = $this->session->userdata['id'];

    if (!$this->users->is_master($id_user)) {
        redirect('/blog');
    }

    if (isset($_POST['trigger']) && $_POST['trigger'] == 'add-event') {

      $data=array(
          'name' => ($this->input->post('name') == NULL) ? FALSE : $this->input->post('name'),
          'body' => ($this->input->post('body') == NULL) ? FALSE : $this->input->post('body'),
          'date' => ($this->input->post('date') == NULL) ? FALSE : $this->input->post('date'),
          'short_description' => ($this->input->post('short_description') == NULL) ? FALSE : $this->input->post('short_description'),
          'id_user' => $id_user
      );

      $this->load->model('event_model', 'event');
      $img = $this->event->uploadImage($_FILES, 'image');
      if( $img ) {
        $data['image'] = $img;
      }

      $this->db->set('creation_date', 'NOW()', FALSE);
      $this->db->insert('event_entry', $data);

      $this->AdminNotification->type  = 3;
      $this->AdminNotification->param = $this->db->insert_id();
      $this->AdminNotification->insert();

      $this->session->set_flashdata(
        'success',
        'Event added. Wait for Admin approval.'
      );

      redirect('/events/add');

    }


    $data['title'] = 'Add Event';

    $this->load->view('templates/header', $data);
    $this->load->view('pages/event_add', $data);
    $this->load->view('templates/footer');

  }

  public function view($id) {

    $data['post']  = $this->Event->getById($id);

    if(!$data['post']) {
      header("Location: " . base_url());
    }

    $data['title'] = $data['post']->name;

    $this->load->view('templates/header', $data);
    $this->load->view('pages/events-view', $data);
    $this->load->view('templates/footer');

  }

}
