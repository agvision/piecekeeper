<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('Api_model', 'Api');
    $this->load->model('Social_model', 'social');
    $this->load->model('admin_notification', 'AdminNotification');
    $this->load->model('mission_model', 'Mission');
    $this->load->model('email_model', 'Email');
    $this->load->model('Webshop_model', 'webshop');
    $this->load->helper('url');
    $this->load->model('country');

    $this->load->library('cart');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }

  }

  public function index()
  {

    $id_user = $this->session->userdata('id');
    $package_result =  $this->users->getLevel3Package($id_user);

    if( count($package_result) == 0 || $package_result->free_product != "" ){
        redirect("/");
    }

    $data['products_info'] = $this->db->query("SELECT * FROM content WHERE id=21")->row()->body;

    $data['user']     = $this->users->get($id_user);
    $data['countFollowers'] = $this->social->countFollowers($this->session->userdata('id'));

    $points = $this->Points->getFromUser($id_user);

    $data['totals'] = $this->db->query("SELECT COUNT(*) AS sales_no, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id_user}")->row();
    $data['totals']->total_points = $points['total'];
    $data['global_ranking'] = $this->users->getGlobalRanking($id_user);
    $data['title'] = "Products";

    $country = $this->country->getByShortName($data['user']['country']);
    $country = $country->iso2;

    $products = $this->Api->getProducts($country);

    $data['size_arr'] = array();

    if(!empty($products))
    {
        foreach ($products as $product) {
          $valid_product = false;

          foreach ($product->options as $size => $size_value) {
            if( !in_array( $size , $data['size_arr'] ) ) {
              $data['size_arr'][] = $size;
            }

            foreach ($size_value as $key => $value) {

              if($this->Api->isValidSize($value->name) && $key == $product->ident) {
                $product->{'sku_'.$size} = $value->sku;
                $product->stock[$size] = $value->stock;

                if( $value->stock >= 10 ) {
                  $valid_product = true;
                }
              }
            }
          }

          if ( !$valid_product )
          {
                continue;
          }

          $data['products'][] = $product;

        }
    }

    foreach ($this->cart->contents() as $items) {
      $data['in_cart_p_id'] = $items['id'];
      $data['in_cart_p_sku'] = $items['options']['sku'];
      $data['in_cart_p_size'] = $items['options']['size'];
      $data['in_cart_p_name'] = $items['name'];
    }

    $this->load->view('templates/header', $data);
    $this->load->view('pages/products');
    $this->load->view('templates/footer');
  }

  public function checkout() {

    $query = $this->db->query("SELECT * FROM country_t");

    $data['countries'] = $query->result();
    $data['states'] = $this->db->query("SELECT * FROM states")->result();

    $id_user = $this->session->userdata('id');

    $data['user']     = $this->users->get($id_user);
    $data['countFollowers'] = $this->social->countFollowers($this->session->userdata('id'));

    $points = $this->Points->getFromUser($id_user);

    $data['totals'] = $this->db->query("SELECT COUNT(*) AS sales_no, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id_user}")->row();
    $data['totals']->total_points = $points['total'];
    $data['title'] = "Checkout";
    $data['global_ranking'] = $this->users->getGlobalRanking($id_user);

    $cart_empty = true;

    foreach ($this->cart->contents() as $items) {
      $data['in_cart_p_id'] = $items['id'];
      $data['in_cart_p_sku'] = $items['options']['sku'];
      $data['in_cart_p_size'] = $items['options']['size'];
      $data['in_cart_p_name'] = $items['name'];

      $cart_empty = false;
    }

    if($cart_empty) {
      redirect("/products");
    }

    $this->load->view('templates/header', $data);
    $this->load->view('pages/checkout');
    $this->load->view('templates/footer');
  }

  public function order() {

    $id_user = ($this->session->userdata('id') == NULL) ? FALSE : $this->session->userdata('id');

    if( $this->input->post('order') ){

      $email                    = ($this->input->post('email') == NULL) ? FALSE : $this->input->post('email');
      $phone                    = ($this->input->post('phone') == NULL) ? FALSE : $this->input->post('phone');

      $shipping_first_name      = ($this->input->post('shipping_first_name') == NULL) ? FALSE : $this->input->post('shipping_first_name');
      $shipping_last_name       = ($this->input->post('shipping_last_name') == NULL) ? FALSE : $this->input->post('shipping_last_name');
      $shipping_address_1       = ($this->input->post('shipping_address_1') == NULL) ? FALSE : $this->input->post('shipping_address_1');
      $shipping_address_2       = ($this->input->post('shipping_address_2') == NULL) ? FALSE : $this->input->post('shipping_address_2');
      $shipping_zip             = ($this->input->post('shipping_zip') == NULL) ? FALSE : $this->input->post('shipping_zip');
      $shipping_city            = ($this->input->post('shipping_city') == NULL) ? FALSE : $this->input->post('shipping_city');
      $shipping_access_code     = ($this->input->post('shipping_access_code') == NULL) ? FALSE : $this->input->post('shipping_access_code');
      $shipping_state           = ($this->input->post('shipping_state') == NULL) ? FALSE : $this->input->post('shipping_state');
      $shipping_country         = ($this->input->post('shipping_country') == NULL) ? FALSE : $this->input->post('shipping_country');

      $in_cart_p_id             = ($this->input->post('in_cart_p_id') == NULL) ? FALSE : $this->input->post('in_cart_p_id');
      $in_cart_p_sku            = ($this->input->post('in_cart_p_sku') == NULL) ? FALSE : $this->input->post('in_cart_p_sku');
      $in_cart_p_size           = ($this->input->post('in_cart_p_size') == NULL) ? FALSE : $this->input->post('in_cart_p_size');
      $in_cart_p_name           = ($this->input->post('in_cart_p_name') == NULL) ? FALSE : $this->input->post('in_cart_p_name');

      $order_product = array(
                array(
                  'sku' => $in_cart_p_sku ,
                  'quantity' => 1 ,
                  'price' => 0
                  )
                );

      $user = $this->users->get($id_user);

      $country_information = $this->country->getByShortName($shipping_country);
      $currency = $country_information->currency;
      if(empty($currency)) $currency = "EUR";
      $country = $country_information->iso2;

      if($user['gender'] == '0') {
        $user['gender'] = 'f';
      } else {
        $user['gender'] = 'm';
      }

      $order = array(
          'currency'            => $currency,
          'email'               => $email,
          'phone'               => $phone ,
          'gender'              => $user['gender'],
          'total'               => 0,
          'shipping_first_name' => $shipping_first_name,
          'shipping_last_name'  => $shipping_last_name,
          'shipping_address_1'  => $shipping_address_1,
          'shipping_address_2'  => $shipping_address_2,
          'shipping_house_no'   => '',
          'shipping_house_ext'  => '',
          'shipping_zip'        => $shipping_zip,
          'shipping_city'       => $shipping_city,
          'shipping_state'      => $shipping_state,
          'shipping_country'    => $shipping_country,
          'billing_first_name'  => $shipping_first_name,
          'billing_last_name'   => $shipping_last_name,
          'billing_address_1'   => $shipping_address_1,
          'billing_address_2'   => $shipping_address_2,
          'billing_house_no'    => '',
          'billing_house_ext'   => '',
          'billing_zip'         => $shipping_zip,
          'billing_city'        => $shipping_city,
          'billing_state'       => $shipping_state, // US Only
          'country'             => $country
      );

      $this->db->insert( 'ordered_products', $order );
      $this->users->setLevel3FreeProduct($id_user,$in_cart_p_sku,$in_cart_p_size, $this->db->insert_id());

      $order['products'] = $order_product;
    }

    redirect("/products");
  }

  public function buy_product()
  {

    $this->cart->destroy();

    $id       = ($this->input->post('id') == NULL) ? FALSE : $this->input->post('id');
    $qty      = ($this->input->post('qty') == NULL) ? FALSE : $this->input->post('qty');
    $price    = ($this->input->post('price') == NULL) ? FALSE : $this->input->post('price');
    $name     = ($this->input->post('name') == NULL) ? FALSE : $this->input->post('name');
    $size     = ($this->input->post('size') == NULL) ? FALSE : $this->input->post('size');
    $sku      = ($this->input->post('sku') == NULL) ? FALSE : $this->input->post('sku');

    if( !$id ||  !$qty || !$price || !$name || !$size ) {
      echo "Fail";
    }

    $data = array(
               'id'      => $id,
               'qty'     => $qty,
               'price'   => $price,
               'name'    => $name,
               'options'    =>  array('size' =>  $size, 'sku' => $sku)
            );

    $row_id = $this->cart->insert($data);

    echo "Success";

  }


}
