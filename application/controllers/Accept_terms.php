<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accept_terms extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->library('email');
    $this->load->model('users');
    $this->load->model("Site_config");

  }

  public function index()
  {
      $data['title'] = 'Accept Terms and Conditions';

      $id_user = $this->session->userdata('id');
      $user    = $this->users->get($id_user);

      if(isset($_POST['trigger']) && $_POST['trigger'] == 'accept') {

          if(isset($_POST['accept'])){
              $this->db->set('terms_accepted', 1, false);
              $this->db->where('id', $id_user);
              $this->db->update('auth_user');

              redirect('/welcome');
          }
      }


      $this->load->view('templates/header', $data);
      $this->load->view('pages/accept-terms', $data);
      $this->load->view('templates/footer');

  }

}
