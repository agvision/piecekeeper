<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');

        /* Twitter Stuff */
        $CI = & get_instance();
        $CI->config->load("twitter", TRUE);
        $config = $CI->config->item('twitter');
        $this->load->library('twconnect', $config);

        /* Instagram Stuff */
        $CI->config->load("instagram", TRUE);
        $config = $CI->config->item('instagram');
        $this->load->library('instagram_api', $config);

        /* Tumblr Stuff */
        $this->load->library('Tumblr');

        /* Youtube Stuff */
        $this->load->library('youtube_api');

        /* Vine Stuff */
        $this->load->library('vine_api');

        $this->load->helper('form');
        $this->load->helper('url');

        $this->load->model('Api_keys_model', 'Api_key');
        $this->load->model('api_model', 'API');
        $this->load->model('Users', 'Users');
        $this->load->model('Coupon_model', 'Coupons');
        $this->load->model('Goal_model', 'Goals');
        $this->load->model('Social_model', 'social');
        $this->load->model('Mission_model', 'Mission');
        $this->load->model('admin_notification', 'AdminNotification');
        $this->load->model('country', 'country');
        $this->load->library('currency');

        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');

        if (!empty($this->input->get('key'))) {
            $received_key = $this->input->get('key');
        }

        if (!empty($this->input->post('key'))) {
            $received_key = $this->input->post('key');
        }

        //Key for developement
        if ($received_key == '9ca6b938f38401bc21c3302e519e67c02ea7f753') {
            $data = array(
                'id' => '4',
                'email' => 'mihaigeorge_c@yahoo.com',
                'level' => '3'
            );

            $this->session->set_userdata($data);
        } // Verify if the key exist in database
        else if (empty($received_key) || !$this->Api_key->VerifyKey($received_key)) {

            $this->output->set_status_header('403');
            exit();
        }
    }

    /* Main */
    public function index()
    {
        $this->output->set_status_header('403');
        exit;
    }

    /**
     *
     * Oauth API
     * call proper auth method  
     */
    public function auth($service, $step = null){
        // save oauth callback redirect path
        $redirectUrl = $this->input->get("redirectUrl");
        $this->session->set_userdata("oauth_redirect_url", $redirectUrl);

        switch ($service) {
            case 'twitter':
                $this->oauthTwitter($step);
                break;

            case 'instagram':
                $this->oauthInstagram($step);
                break;

            case 'tumblr':
                $this->oauthTumblr($step);
                break;

            case 'pinterest':
                $this->authPinterest($step);
                break;

            case 'youtube':
                $this->oauthYoutube($step);
                break;

            case 'vine':
                $this->authVine($step);

            default:
                break;
        }
    }

    public function oauthTwitter($step){
        $result = array();

        if ($step == "redirectUrl") {
            $this->session->unset_userdata('tw_access_token');
            $this->session->unset_userdata('tw_status');

            $url = $this->twconnect->twgetredirect();

            if ($url) {
                $result['url'] = $url;
            } else{
                $this->output->set_status_header('403');
                exit;
            }
        }

        else if ($step == "getUser") {
            $this->twconnect->twprocess_callback();
            $tw_user = $this->twconnect->twaccount_verify_credentials();

            if ($tw_user) {
                $result['user'] = $tw_user;
            } else {
                $this->output->set_status_header('403');
                exit;
            }
        }

        echo json_encode($result);
        exit;
    }

    public function oauthInstagram($step){
        $result = array();

        if ($step == "redirectUrl") {
            $url = $this->instagram_api->instagramLogin();

            if ($url) {
                $result['url'] = $url;
            } else{
                $this->output->set_status_header('403');
                exit;
            }
        }

        else if ($step == "getUser") {
            $auth_response = $this->instagram_api->authorize($_GET['code']);
            $inst_profile = 'http://instagram.com/'.$auth_response->user->username;

            $inst_user = $auth_response->user->id;

            $user_request_url = "https://api.instagram.com/v1/users/{$inst_user}/?access_token={$auth_response->access_token}";

            if ($this->Social->check40X($user_request_url, false)) {
                $inst_followers = 0;
            } else {
                $user = $this->instagram_api->getUser($inst_user);
                $inst_followers = $user->data->counts->followed_by;
            }

            $result['user']['profile'] = $inst_profile;
            $result['user']['followers'] = $inst_followers;
        }

        echo json_encode($result);
        exit;
    }

    public function oauthTumblr($step){
        $result = array();

        if ($step == "redirectUrl") {
            $this->tumblr->reset_session();

            $url = $this->tumblr->get_auth_url();

            if ($url) {
                $result['url'] = $url;
            } else{
                $this->output->set_status_header('403');
                exit;
            }
        }

        else if ($step == "getUser") {
            $access_token = $this->tumblr->handle_callback();
            $this->tumblr->create($this->tumblr->tumblr_consumer_key, $this->tumblr->tumblr_secret_key, $access_token['oauth_token'], $access_token['oauth_token_secret']);

            $data = $this->tumblr->get('http://api.tumblr.com/v2/user/info');
            $tub_followers = 0;
            $tub_profile = false;

            foreach ($data->response->user->blogs as $blog) {
                if (!$tub_profile) {
                    $tub_profile = $blog->url;
                }
                $tub_followers += $blog->followers;
            }

            $result['user']['profile'] = $tub_profile;
            $result['user']['followers'] = $tub_followers;
        }

        echo json_encode($result);
        exit;
    }

    public function authPinterest($step){
        $result = array();

        if ($step == "getUser") {
            $username = $this->input->get("username");
            
            if ($username) {
                $result['user']['profile'] = 'http://pinterest.com/'.$username.'/';

                if(!($this->Social->check40X($result['user']['profile']))) {
                  $metas = get_meta_tags($result['user']['profile']);
                  $result['user']['followers'] = $metas['pinterestapp:followers'];
                } else {
                  $data['linked'] = false;
                  $data['error_code'] = 2;
                  $data['errors'] = array(
                      'Invalid username.'
                  );
                  $this->output->set_status_header('403');
                  echo json_encode($data);
                  exit;
                }
            } else{
                $data['linked'] = false;
                $data['error_code'] = 1;
                $data['errors'] = array(
                    'Username not provided.'
                );
                $this->output->set_status_header('403');
                echo json_encode($data);
                exit;
            }
        }

        echo json_encode($result);
        exit;
    }

    public function oauthYoutube($step) {
        $result = array();
        $this->youtube_api->youtubeConnect();
        

        if ($step == "redirectUrl") {
            $url = $this->youtube_api->getAuthUrl();

            if ($url) {
                $result['url'] = $url;
            } else{
                $this->output->set_status_header('403');
                exit;
            }
        } 

        else if ($step == "getUser") {
            $followers = $this->youtube_api->getFollowers();
            $profile   = "https://youtube.com/channel/".$this->youtube_api->getChannelID();

            $result['user']['profile'] = $profile;
            $result['user']['followers'] = $followers;
        }

        echo json_encode($result);
        exit;
    }

    public function authVine($step){
        $result = array();

        if ($step == "getUser") {
            $userID = $this->input->get("userID");
            
            if ($userID) {
                $vine_user = $this->vine_api->getUser($userID);

                if ($vine_user) {
                    $vn_followers = $vine_user->followerCount;
                    $vn_profile   = 'https://vine.co/u/'.$userID;
                } else {
                    $data['linked'] = false;
                    $data['error_code'] = 2;
                    $data['errors'] = array(
                        'UserID not provided.'
                    );
                    $this->output->set_status_header('403');
                    echo json_encode($data);
                    exit;
                }

            } else {
                $data['linked'] = false;
                $data['error_code'] = 1;
                $data['errors'] = array(
                    'UserID not provided.'
                );
                $this->output->set_status_header('403');
                echo json_encode($data);
                exit;
            }
        }

        $result['user']['profile'] = $vn_profile;
        $result['user']['followers'] = $vn_followers;

        echo json_encode($result);
        exit;
    }

    /**
     * Profile API
     * @return user profile as JSON object
     */
    public function profile($action = 'view')
    {
        if (!$this->authentication->isLogged()) {
            // not logged in
            $this->output->set_status_header('403');
            exit;
        } else {
            $id_user = $this->session->userdata('id');

            $user_profile = $this->Users->get_profile($id_user);

            $result = array();

            if (!$user_profile) {
                $this->output->set_status_header('403');
                exit;
            }
        }
        if ($action == 'edit') {
            if(isset($_POST['paypal_email']))
            {
                $this->form_validation->set_rules('paypal_email', 'Paypal Email', 'required|valid_email');
                if ($this->form_validation->run()) {
                    $data = array("payment_number" => $this->input->post('paypal_email'));
                    $this->db->where("id", $id_user)->update("auth_user", $data);
                    $result['updated'] = true;
                }
                else
                {
                    $this->output->set_status_header('400');
                    $result['updated'] = false;
                    $result['errors'] =  $this->form_validation->error_array();
                }
                echo json_encode($result);
                exit;

            }


            $_POST['dob'] = $_POST['year']."-".$_POST['month']."-".$_POST['day'];
            $fields = array(
                'first_name',
                'last_name',
                'id_country',
                'country',
                'address1',
                'address2',
                'postcode',
                'city',
                'dob',
                'gender',
                'school',
                'graduation_year',
                'email',
                'phone'
            );
            $old_email = $this->db->select("email")
                                  ->from("auth_user")
                                  ->where("id", $id_user)
                                  ->get()->row_array()['email'];

            $this->form_validation->set_rules("first_name", "First Name", 'required');
            $this->form_validation->set_rules("last_name", "Last Name", 'required');
            if($old_email != $this->input->post('email'))
            {
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[auth_user.email]');
            }
            $this->form_validation->set_rules('day', 'Day of Birthday', 'required');
            $this->form_validation->set_rules('address1', 'Address', 'required');
            $this->form_validation->set_rules('month', 'Month of Birthday', 'required');
            $this->form_validation->set_rules('year', 'Year oh Birthday', 'required');
            $this->form_validation->set_rules('country', 'Country', 'required');
            $this->form_validation->set_rules('city', 'City', 'required');
            $this->form_validation->set_rules('phone', 'Phone', 'required');
            if ($this->form_validation->run()) {
                $data = array();
                $result['updated'] = true;
                foreach ($fields as $field)
                {
                        $data[$field] = $this->input->post($field);
                }

                if ($result['updated']) {
                    $this->db->where("id", $id_user)->update("auth_user", $data);
                }


            } else
            {
                $result['updated'] = FALSE;
                $result['errors'] = $this->form_validation->error_array();
                $this->output->set_status_header('400');
            }
            echo json_encode($result);
            exit;

        } else {
        // Profile information
        $result['first_name'] = $user_profile['first_name'];
            $result['last_name'] = $user_profile['last_name'];
            $result['country'] = array(
                "name" => $user_profile['country'],
                "id" => $user_profile['id_country']
            );



            $result['state'] = $user_profile['state'];
            $result['level']['value'] = intval($user_profile['level']);
            $result['address1'] = $user_profile['address1'];
            $result['address2'] = $user_profile['address2'];
            $result['postcode'] = $user_profile['postcode'];
            $result['city'] = $user_profile['city'];
            $result['day'] = date("j", strtotime($user_profile['dob']));
            $result['month'] = date("n", strtotime($user_profile['dob']));
            $result['year'] = date("Y", strtotime($user_profile['dob']));
            $result['gender'] = $user_profile['gender'];
            $result['school'] = array(
                'name' => $user_profile['school'],
                'graduation_year' => $user_profile['graduation_year']
            );
            $result['email'] = $user_profile['email'];
            $result['phone'] = $user_profile['phone'];
            $result['paypal'] = array(
                "email" => $user_profile['payment_number'],
                "name" => $user_profile['payment_name']
            );


            $data['settings'] = $this->site_config->getGeneralSettings();

        $data['totals'] = $this->users->getOrders($id_user);
        $data['totals'] = $data['totals'][0];
        $data['totals']->total_earnings = $data['totals']->revenue;

        // Sales
        $result['sales'] = intval($data['totals']->total_sales);

        $points = $this->Points->getFromUser($id_user);
        $data['totals']->total_earnings = $this->users->getCommission($id_user);

        // Total and giftcard earnings
        $result['earnings']['cash'] = number_format(($data['totals']->total_earnings), 2);
        $result['earnings']['giftcard'] = number_format(((100 + intval($data['settings']['giftcard_extra_commission'])) * $data['totals']->total_earnings) / 100, 2);

        // Points
        $result['points']['sales'] = number_format((intval($points['sales'])), 0);
        $result['points']['bonus'] = number_format((intval($points['bonus'])), 0);
        $result['points']['total'] = number_format((intval($points['total'])), 0);

        $result['ranking'] = [
            'global' => [],
            'country' => [],
            'city' => []
        ];

        // Ranking
        if ($user_profile['date_joined'] < date("Y-m-d H:i:s", strtotime("-1 month"))) {
            $one_month = true;
        } else {
            $one_month = false;
        }

        $global_ranking = $this->users->getGlobalRanking($id_user);
        $country_ranking = $this->users->getCountryRanking($id_user);
        $city_ranking = $this->users->getCityRanking($id_user);

        if ($one_month) {
            $global_ranking_last_month = $this->users->getGlobalRanking($id_user, true);
            $country_ranking_last_month = $this->users->getCountryRanking($id_user, true);
            $city_ranking_last_month = $this->users->getCityRanking($id_user, true);

            $global_ranking_progress = $global_ranking_last_month - $global_ranking;
            $country_ranking_progress = $country_ranking_last_month - $country_ranking;
            $city_ranking_progress = $city_ranking_last_month - $city_ranking;
        }

        $result['ranking']['global']['last_month'] = isset($global_ranking_progress) ? $global_ranking_progress : null;
        $result['ranking']['global']['rank'] = number_format(($global_ranking), 0);
        $result['ranking']['global']['percentage'] = 0; // by default
        $result['ranking']['country']['rank'] = number_format(($country_ranking), 0);
        $result['ranking']['country']['last_month'] = isset($country_ranking_progress) ? $country_ranking_progress : null;
        $result['ranking']['city']['rank'] = number_format(($city_ranking), 0);
        $result['ranking']['city']['last_month'] = isset($city_ranking_progress) ? $city_ranking_progress : null;


        // Get the percentage
        $user_sales = $data['totals']->total_sales;
        $points = $this->users->getPoints($id_user);
        $user_points = $points['total'];

        $sales_percent = 100;
        $points_percent = 100;

        if ($user_profile['level'] == -2) {
            $sales_goal = $data['settings']['sales_level_2'];
            $points_goal = $data['settings']['total_points_level_2'];

            $sales_percent = intval(100 * $user_sales / $sales_goal);
            $points_percent = intval(100 * $user_points / $points_goal);
        } else if ($user_profile['level'] == -1) {
            $sales_goal = $data['settings']['sales_level_3'];
            $points_goal = $data['settings']['total_points_level_3'];

            $sales_percent = intval(100 * $user_sales / $sales_goal);
            $points_percent = intval(100 * $user_points / $points_goal);
        }

        $sales_percent  = $sales_percent > 100 ? 100 : $sales_percent;
        $sales_percent  = $sales_percent < 0 ? 0 : $sales_percent;

        $points_percent = $points_percent > 100 ? 100 : $points_percent;
        $points_percent = $points_percent < 0 ? 0 : $points_percent;

        // For the next level is the mean of the sales and points
        $result['level']['percentage'] = ($sales_percent + $points_percent) / 2;

        $result['points']['percentage'] = $points_percent;

        $all_users = $this->users->getAll();

        // Get the percentage for ranking (0-last, 100-first)
        $ranking_global_percentage = intval(100 - (100 * intval($global_ranking - 1)) / count($all_users));

        $ranking_global_percentage = $ranking_global_percentage > 100 ? 100 : $ranking_global_percentage;
        $ranking_global_percentage = $ranking_global_percentage < 0 ? 0 : $ranking_global_percentage;

        $result['ranking']['global']['percentage'] = $ranking_global_percentage;

        echo json_encode($result);
        exit;
    }

    $this->output->set_status_header('403');
    exit;
}

/**
 * Ranking API
 * @return first 'length' users ordered descending by total points as JSON object
 */
public function ranking()
{
    $length = $this->input->get('length');

    if (!$this->authentication->isLogged()) {
        // Not logged in
        $this->output->set_status_header('403');
        exit;
    } elseif (empty($length) || !is_numeric($length)) {
        // Invalid length parameter
        $data = array();
        $data['error'] = 'Invalid length parameter';
    } else {
        $data['top'] = array();
        $users = $this->users->getTopUsers(intval($length));

        $result = array();

        $i = 0;

        foreach ($users as $user) {
            $result[$i]['total_points'] = number_format(($user->total), 2);
            $result[$i]['first_name'] = $user->first_name;
            $result[$i]['last_name'] = $user->last_name;
            $result[$i]['country'] = $user->iso2;

            $i++;
        }

        $data['top'] = $result;
    }

    echo json_encode($data);
}


/**
 * Goals API
 * @return list of goals and related details for current authenticated user.
 */
public function goals()
{

    if (!$this->authentication->isLogged()) {
        // not logged
        $this->output->set_status_header('403');
        exit;
    } else {
        $id_user = $this->session->userdata('id');

        $goals = $this->Goals->getAllForUser($id_user);


        foreach ($goals as $goal) {
            $goal->deadline = strtotime($goal->deadline);
        }


        echo json_encode($goals);
    }

}


/**
 * Register a new user API
 *
 * @return registered: bool and error message if error
 */
public function register()
{
    $this->form_validation->set_rules("first_name", "First Name", 'required');
    $this->form_validation->set_rules("last_name", "Last Name", 'required');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[auth_user.email]');
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('day', 'Day of Birthday', 'required');
    $this->form_validation->set_rules('month', 'Month of Birthday', 'required');
    $this->form_validation->set_rules('year', 'Year oh Birthday', 'required');
    $this->form_validation->set_rules('address1', 'Address', 'required');
    $this->form_validation->set_rules('country', 'Country', 'required');
    $this->form_validation->set_rules('city', 'City', 'required');
    $this->form_validation->set_rules('terms', 'Terms and Conditions', 'required');
    $this->form_validation->set_rules('phone', 'Phone', 'required');

    //  $this->form_validation->set_rules('profile_pic', 'Profile Image', 'file_required|file_size_max[2MB]');
    //  $this->form_validation->set_rules('profile_pic_2', 'Profile Image', 'file_size_max[2MB]');

    $email = $this->input->post('email');
    $account = $this->db->query("SELECT * FROM auth_user WHERE email='{$email}'")->row();

    if (count($account) && !$account->completed) {
        $this->db->where('email', $email);
        $this->db->delete('auth_user');
    }

    if($this->session->userdata("invitation"))
    {
        $_POST['invitation'] = $this->session->userdata("invitation");
    }

    if ($this->form_validation->run() == true) {
        $this->users->register();

        $id_user = $this->db->insert_id();

        $pic1 = $this->session->userdata("picture_1_loaded");

        if (!empty($pic1)) {
            $this->db->where('id', $id_user);
            $this->db->set('profile_pic', "'" . $pic1 . "'", false);
            $this->db->update('auth_user');
            $this->session->unset_userdata("picture_1_loaded");

        }

        $pic2 = $this->session->userdata("picture_2_loaded");

        if (!empty($pic2)) {
            $this->db->where('id', $id_user);
            $this->db->set('profile_pic_2', "'" . $pic2 . "'", false);
            $this->db->update('auth_user');
            $this->session->unset_userdata("picture_2_loaded");
        }

        $j = 0;

        while ($this->input->post('question-ref-' . $j)) {
            $q_id = $this->input->post('question-ref-' . $j);
            $q_value = $this->input->post('question-' . $q_id);

            $this->questions->save_answer($id_user, $q_id, $q_value);

            $j++;
        }

        $data['registered'] = TRUE;

        $this->session->set_userdata('registered_id', $id_user);

        echo json_encode($data);
    } else {
        $data['registered'] = FALSE;
        $data['errors'] = $this->form_validation->error_array();

        // Send the form validation errors
        echo json_encode($data);
    }

}

    public function completeRegistration()
    {
        $id_user = $this->session->userdata('registered_id');

        if(empty($id_user))
        {
            $data['registered'] = false;
            $data['error_code'] = 1;
            $data['errors'] = array(
                'You are not logged-in.'
            );
            $this->output->set_status_header('403');
            echo json_encode($data);
            exit;
        }

        // Social networks
        $networks_count = 0;

        /* Facebook data */
        $hasFb = $this->Social->isLinked('facebook', $id_user);
        if ($hasFb)
        {
            $networks_count++;
        }

        /* Twitter data */
        $hasTw = $this->Social->isLinked('twitter', $id_user);
        if ($hasTw)
        {
            $networks_count++;
        }

        /* Instagram data */
        $hasInst = $this->Social->isLinked('instagram', $id_user);
        if ($hasInst)
        {
            $networks_count++;
        }

        /* Tumblr Data */
        $hasTumblr = $this->Social->isLinked('tumblr', $id_user);
        if ($hasTumblr)
        {
            $networks_count++;
        }

        /* Pinterest Data */
        $hasPin = $this->Social->isLinked('pinterest', $id_user);
        if ($hasPin)
        {
            $networks_count++;
        }

        /* Linkedin Data */
        $hasLn = $this->Social->isLinked('linkedin', $id_user);
        if ($hasLn)
        {
            $networks_count++;
        }

        /* YouTube Data */
        $hasYt = $this->Social->isLinked('youtube', $id_user);
        if ($hasYt)
        {
            $networks_count++;
        }

        /* Vine Data */
        $hasVn = $this->Social->isLinked('vine', $id_user);
        if ($hasVn)
        {
            $networks_count++;
        }

        if($networks_count > 0)
        {
            // Make completed = 1
            $this->db->where('id', $id_user);
            $this->db->update('auth_user', ['completed' => 1]);

            // unset session registered id
            $this->session->unset_userdata('registered_id');

            // Await confirmation from admin.
            $this->AdminNotification->type = 1;
            $this->AdminNotification->param = $id_user;
            $this->AdminNotification->insert();
            $data['registered'] = true;
            echo json_encode($data);
            exit;


        }
        else
        {
            $data['registered'] = false;
            $data['errors'] = array(
                'You need to link at least one account.'
            );
            $data['error_code'] = 2;
            $this->output->set_status_header('403');
            echo json_encode($data);
            exit;

        }

    }


public function profile_image($index = "1")
{
    if(isset($_POST['imgUrl']))
    {
        $img_url = $_POST['imgUrl'];
    }
    else
    {
        $img_url = null;
    }
    $id_user = "";
    if ($this->authentication->isLogged()) {
        // Update profile.
        $id_user = $this->session->userdata('id');
    }

    // Verify if the $_FILES variable exists
    if (empty($_FILES) && empty($img_url)) {
        $response = array(
            'uploaded' => false,
            'error' => 'File does not exist.'
        );

        echo json_encode($response);
        exit;
    }

    $uploaded = false;
    if(empty($img_url))
    {
        $path_info = pathinfo($_FILES['file']["name"]);
        $ext = $path_info['extension'];
    }
    else
    {
        $ext = 'jpg';
    }
    switch ($index) {
        case "1":
            $pic1 = md5(time() . "asd" . rand(10, 999999)) . "." . $ext;

            if ($pic1 && $id_user) {
                $this->db->where('id', $id_user);
                $this->db->set('profile_pic', "'" . $pic1 . "'", false);
                $this->db->update('auth_user');
            } elseif ($pic1 && !$id_user) {
                $this->session->set_userdata("picture_1_loaded", $pic1);
            }
            if(empty($img_url))
            {
                $this->users->uploadImage($_FILES, 'file', $pic1);
            }
            else
            {
                $this->users->uploadURLImage($img_url, $pic1);
            }
            $uploaded = true;
            break;

        case "2":
            $pic2 = md5(time() . "asd" . rand(10, 999999)) . "." . $ext;

            if ($pic2 && $id_user) {
                $this->db->where('id', $id_user);
                $this->db->set_userdata('profile_pic_2', "'" . $pic2 . "'", false);
                $this->db->update('auth_user');

            } elseif ($pic2 && !$id_user) {
                $this->session->set_userdata("picture_2_loaded", $pic2);
            }
            $this->users->uploadImage($_FILES, 'file', $pic2);
            $uploaded = true;

            break;
    }

    if ($uploaded) {
        $response = array(
            'uploaded' => true
        );
    } else {
        $response = array(
            'uploaded' => false,
            'error' => 'Upload failed.'
        );
    }

    echo json_encode($response);
}


/**
 * Get all the countries API
 *
 * @return array with countries
 */
public function countries()
{
    $data['countries'] = $this->country->getALlCountries();

    echo json_encode($data);
}


/**
 * Get all the cities API
 * If the state and country parameters don't exist return all the exitent cities,
 * otherwise group them by country and state
 *
 * @return array with cities
 */
public function cities()
{
    $id_country = $this->input->get('country');
    $id_state = $this->input->get('state');

    $data = array();

    if ($id_country && is_numeric($id_country) && !$id_state) {
        $data['cities'] = $this->users->getCitiesByCountries($id_country, $id_state, null, null);
    } else if ($id_country && is_numeric($id_country) && $id_state) {
        $data['cities'] = $this->users->getCitiesByCountries($id_country, $id_state, null, null);;
    } else if (!$id_state && !$id_country) {
        $data['cities'] = $this->users->getCitiesByCountries($id_country, $id_state, null, null);
    }

    echo json_encode($data);
}

/**
 * Return all the files
 * @return [type] [description]
 */
public function testFiles()
{
    echo json_encode($_FILES);
}

// public function profile($filter = false) {
//
//     if (!$filter) {
//         $id_user    = $this->session->userdata('id');
//         $key        = $this->input->get('key');
//
//
//         if (empty($id_user) || is_numeric($id_user) || empty($key) || $this->key != $key) {
//             $this->output->set_status_header('403');
//
//             return;
//         }
//
//         $user_profile = $this->Users->get_profile($id_user);
//         $result = array();
//
//         if (!$user_profile) {
//             $result['profile'] = false;
//         } else {
//             $result['profile'] = $user_profile;
//         }
//
//         echo json_encode($result);
//     } elseif ($filter == "update") {
//
//         $id_user    = $this->session->userdata('id');
//         $key        = $this->input->get('key');
//
//
//         if (empty($id_user) || !is_numeric($id_user) || empty($key) || $this->key != $key) {
//             $this->output->set_status_header('403');
//             return;
//         }
//
//         $j = 0;
//
//         while($this->input->post('question-ref-'.$j)) {
//
//             $q_id = $this->input->post('question-ref-'.$j);
//             $q_value = $this->input->post('question-'.$q_id);
//             unset($_POST['question-ref-'.$j]);
//             unset($_POST['question-'.$q_id]);
//
//             $this->questions->save_answer($id,$q_id,$q_value);
//             $j++;
//         }
//
//         $this->users->profile_update($id_user);
//
//         $pic1 = $this->users->uploadImage($_FILES, 'profile_pic');
//
//         if ($pic1) {
//             $this->db->where('id', $id_user);
//             $this->db->set('profile_pic', "'".$pic1."'", false);
//             $this->db->update('auth_user');
//         }
//
//         return;
//
//     } elseif ($filter == "delete") {
//         $id_user    = $this->session->userdata('id');
//         $key        = $this->input->get('key');
//
//         if (empty($id_user) || !is_numeric($id_user) || empty($key) || $this->key != $key) {
//             $this->output->set_status_header('403');
//             return;
//         }
//
//         $this->users->delete($id_user);
//         return;
//
//     } else {
//         $this->output->set_status_header('403');
//         return;
//     }
// }

public function coupons($filter = false)
{

    if ($filter == 'all') {

        $id_user = $this->session->userdata('id');
        $filter = $this->input->get('filter');
        $key = $this->input->get('key');

        if ($this->key == $key) {

            $coupons = $this->Coupons->findAllByIdUser($id_user);
            echo json_encode($coupons);
            return;
        }
    } else if ($filter == 'add') {
        /* Creates only singleuse coupon */
        $id_user = $this->session->userdata('id');
        $code = strtoupper($this->input->post('code'));
        $key = $this->input->post('key');

        if (is_numeric($id_user) && $this->key == $key && !$this->Coupons->exists($code)) {

            $this->Coupons->createUnique20($id_user, $code, '');
            return;
        }
    } else if ($filter == 'delete') {
        /* Creates only singleuse coupon */
        $id_user = $this->session->userdata('id');
        $id_coupon = $this->input->post('id');
        $key = $this->input->post('key');

        if (is_numeric($id_user) && $this->key == $key) {

            $this->Coupons->disable($id_user, $code, '');
            return;
        }
    }

    $this->output->set_status_header('403');
    return;
}

public function missions($filter = false)
{

    $id_user = $this->session->userdata('id');

    if (!is_numeric($id_user)) {
        $this->output->set_status_header('403');
        return;
    }

    if ($filter === false) {
        $id_mission = $this->input->post('mission');
        $user = $this->users->get($id_user);
        $mission = $this->db->query("SELECT COUNT(*) AS submitted FROM missions_users WHERE id_user={$id_user} AND id_mission={$id_mission}")->row();
        $image = $this->Mission->uploadImage($_FILES, 'image');
        $link = $this->input->post('link');
        $text = $this->input->post('text');


        $data = array(
            'id_user' => $id_user,
            'id_mission' => $id_mission,
            'text' => $text,
            'link' => $link,
            'image' => $image,
            'status' => 0
        );

        $this->db->set('submitted', 'NOW()', false);

        if ($mission->submitted) {
            $this->db->where(array('id_user' => $id_user, 'id_mission' => $id_mission));
            $this->db->update('missions_users', $data);
        } else {
            $this->db->insert('missions_users', $data);
        }

        return;
    } elseif ($filter === "all") {
        $missions = $this->Mission->getAll($id_user);
    } else {
        $this->output->set_status_header('403');
        return;
    }

    echo json_encode($missions);
}

// public function goals($filter = false) {
//
//     $id_user = $this->session->userdata('id');
//
//     $key        = $this->input->get('key');
//
//     if ($this->key != $key) {
//         $this->output->set_status_header('403');
//         return;
//     }
//
//     if ($filter === false) {
//         $this->output->set_status_header('403');
//         return;
//     }
//
//     $goals = $this->Goals->getAllForUser($id_user);
//
//     echo json_encode($goals);
// }


// public function ranking() {
//     $id_user = $this->session->userdata('id');
//
//
//     $key        = $this->input->get('key');
//
//     if ($this->key != $key) {
//         $this->output->set_status_header('403');
//         return;
//     }
//
//     $user = $this->users->get($id_user);
//
//     if ($user['date_joined'] < date("Y-m-d H:i:s", strtotime("-1 month"))) {
//         $one_month = true;
//     } else {
//         $one_month = false;
//     }
//
//     $global_ranking = $this->users->getGlobalRanking($id_user);
//     $country_ranking = $this->users->getCountryRanking($id_user);
//     $city_ranking = $this->users->getCityRanking($id_user);
//
//     if ($one_month) {
//         $global_ranking_last_month = $this->users->getGlobalRanking($id_user, true);
//         $country_ranking_last_month = $this->users->getCountryRanking($id_user, true);
//         $city_ranking_last_month = $this->users->getCityRanking($id_user, true);
//
//         $global_ranking_progress = $global_ranking_last_month - $global_ranking;
//         $country_ranking_progress = $country_ranking_last_month - $country_ranking;
//         $city_ranking_progress = $city_ranking_last_month - $city_ranking;
//     }
//
//     $data['global_ranking']           = $global_ranking;
//     $data['global_ranking_progress']  = isset($global_ranking_progress) ? $global_ranking_progress : null;
//     $data['country_ranking']          = $country_ranking;
//     $data['country_ranking_progress'] = isset($country_ranking_progress) ? $country_ranking_progress : null;
//     $data['city_ranking']             = $city_ranking;
//     $data['city_ranking_progress']    = isset($city_ranking_progress) ? $city_ranking_progress : null;
//     $data['count_followers']          = $this->social->countFollowers($id_user);
//
//     $data['totals'] = $this->db->query("SELECT COUNT(*) AS sales_no, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id_user}")->row();
//
//     $points = $this->Points->getFromUser($id_user);
//     $data['totals']->total_points = $points['total'];
//     $data['totals']->bonus = $points['bonus'];
//
//     echo json_encode($data);
// }

/* Old controller function */
public function custom_total($code = false)
{
    if (!$code || ($code != 'SERIOUSREQUEST')) {
        echo "Code cannot be empty.";
        exit();
    }

    $result = $this->API->getSalesByCoupon($code);
    $total = $this->currency->convertToEUR($result[0]->original_total, 'USD');

    echo number_format($total, 2, ',', '.');
}

    public function socialNetworks()
    {
        if ($this->session->userdata('id'))
        {
            $id_user = $this->session->userdata('id');
        }
        else
        {
            $id_user = $this->session->userdata('registered_id');
        }

        if(empty($id_user))
        {
            $data['sent'] = false;
            $data['errors'] = array(
                'You are not logged-in.'
            );
            $this->output->set_status_header('403');
            echo json_encode($data);
            exit;
        }

        // Social networks
        $result['sent']     = true;
        $result['networks'] = array();
        /* Facebook data */
        $fb_friends = $this->Social->checkNetwork('facebook', $id_user);
        if ($fb_friends !== false) {
            $result['networks']['facebook'] = array(
                'url'       => $this->Social->getNetworkProfile('facebook', $id_user),
                'followers' => $fb_friends
            );
        }

        /* Twitter data */
        $tw_followers = $this->Social->checkNetwork('twitter', $id_user);
        if ($tw_followers !== false) {
            $result['networks']['twitter'] = array(
                'url'       => $this->Social->getNetworkProfile('twitter', $id_user),
                'followers' => $tw_followers
            );

        }

        /* Instagram data */
        $inst_followers = $this->Social->checkNetwork('instagram', $id_user);
        if ($inst_followers !== false) {
            $result['networks']['instagram'] = array(
                'url'       => $this->Social->getNetworkProfile('instagram', $id_user),
                'followers' => $inst_followers
            );
        }

        /* Tumblr Data */
        $tub_followers = $this->Social->checkNetwork('tumblr', $id_user);
        if ($tub_followers !== false) {
            $result['networks']['tumblr'] = array(
                'url'       => $this->Social->getNetworkProfile('tumblr', $id_user),
                'followers' => $tub_followers
            );
        }

        /* Pinterest Data */
        $pin_followers = $this->Social->checkNetwork('pinterest', $id_user);
        if ($pin_followers !== false) {
            $result['networks']['pinterest'] = array(
                'url'       => $this->Social->getNetworkProfile('pinterest', $id_user),
                'followers' => $pin_followers
            );
        }

        /* Linkedin Data */
        $ln_followers = $this->Social->checkNetwork('linkedin', $id_user);
        if ($ln_followers !== false) {
            $result['networks']['linkedin'] = array(
                'url'       => $this->Social->getNetworkProfile('linkedin', $id_user),
                'followers' => $ln_followers
            );
        }

        /* YouTube Data */
        $yb_followers = $this->Social->checkNetwork('youtube', $id_user);
        if ($yb_followers !== false || $yb_followers == "-1") {
            $result['networks']['youtube'] = array(
                'url'       => $this->Social->getNetworkProfile('youtube', $id_user),
                'followers' => $yb_followers
            );
        }

        /* Vine Data */
        $vn_followers = $this->Social->checkNetwork('vine', $id_user);
        if ($vn_followers !== false) {
            $result['networks']['vine'] = array(
                'url'       => $this->Social->getNetworkProfile('vine', $id_user),
                'followers' => $vn_followers
            );
        }


        echo json_encode($result);
        exit;

    }

    /**
     * Send profile data for brandbassador migration system
     * [int]      $id      [id of piecekeeper]
     *
     * @return [array] [return profile data]
     */
    public function getProfileDataForMigration($id)
    {
        $user_profile = $this->Users->get_profile($id);
        
        if(!empty($user_profile))
        {
            echo json_encode($user_profile);
            exit;
        }
        else
        {
            $error = array('no_data');
            echo json_encode($error);
            exit;
        }
    }

    /**
     * Send social data for brandbassador migration system
     * [int]      $id      [id of piecekeeper]
     *
     * @return [array] [return profile data]
     */
    public function getPiecekeeperSocialData($id_user)
    {
        // Social networks
        $result['sent']     = true;
        $result['networks'] = array();

        /* Facebook data */
        $fb_friends = $this->Social->checkNetwork('facebook', $id_user);
        if ($fb_friends !== false) 
        {
            $result['networks']['facebook'] = array(
                'url'               => $this->Social->getNetworkProfile('facebook', $id_user),
                'social_account_id' => 1,
                'followers'         => $fb_friends
            );
        }

        /* Twitter data */
        $tw_followers = $this->Social->checkNetwork('twitter', $id_user);
        if ($tw_followers !== false) 
        {
            $result['networks']['twitter'] = array(
                'url'       => $this->Social->getNetworkProfile('twitter', $id_user),
                'social_account_id' => 2,
                'followers' => $tw_followers
            );

        }

        /* Instagram data */
        $inst_followers = $this->Social->checkNetwork('instagram', $id_user);
        if ($inst_followers !== false) 
        {
            $result['networks']['instagram'] = array(
                'url'       => $this->Social->getNetworkProfile('instagram', $id_user),
                'social_account_id' => 3,
                'followers' => $inst_followers
            );
        }

        /* Tumblr Data */
        $tub_followers = $this->Social->checkNetwork('tumblr', $id_user);
        if ($tub_followers !== false) 
        {
            $result['networks']['tumblr'] = array(
                'url'       => $this->Social->getNetworkProfile('tumblr', $id_user),
                'social_account_id' => 4,
                'followers' => $tub_followers
            );
        }

        /* Pinterest Data */
        $pin_followers = $this->Social->checkNetwork('pinterest', $id_user);
        if ($pin_followers !== false) 
        {
            $result['networks']['pinterest'] = array(
                'url'       => $this->Social->getNetworkProfile('pinterest', $id_user),
                'social_account_id' => 5,
                'followers' => $pin_followers
            );
        }

        /* Linkedin Data */
        $ln_followers = $this->Social->checkNetwork('linkedin', $id_user);
        if ($ln_followers !== false) 
        {
            $result['networks']['linkedin'] = array(
                'url'       => $this->Social->getNetworkProfile('linkedin', $id_user),
                'social_account_id' => 6,
                'followers' => $ln_followers
            );
        }

        /* YouTube Data */
        $yb_followers = $this->Social->checkNetwork('youtube', $id_user);
        if ($yb_followers !== false || $yb_followers == "-1") 
        {
            $result['networks']['youtube'] = array(
                'url'       => $this->Social->getNetworkProfile('youtube', $id_user),
                'social_account_id' => 7,
                'followers' => $yb_followers
            );
        }

        /* Vine Data */
        $vn_followers = $this->Social->checkNetwork('vine', $id_user);
        if ($vn_followers !== false) 
        {
            $result['networks']['vine'] = array(
                'url'       => $this->Social->getNetworkProfile('vine', $id_user),
                'social_account_id' => 8,
                'followers' => $vn_followers
            );
        }


        echo json_encode($result);
        exit;
    }

    /**
     * Send scratch batches data for brandbassador migration system
     * [int]      $id      [id of piecekeeper]
     *
     * @return [array] [return profile data]
     */
    public function getPiecekeeperScratchBatchesData($id)
    {   
        $this->db->select('*');
        $this->db->from('scratch_batches');
        $this->db->where('scratch_batches.id_user',$id);
        $scratch_batches_only = $this->db->get()->result();

        $this->db->select('scratch_coupons.*');
        $this->db->from('scratch_batches');
        $this->db->join('scratch_coupons','scratch_batches.batch = scratch_coupons.batch','LEFT');
        $this->db->where('scratch_batches.id_user',$id);
        $scratch_batches = $this->db->get()->result();

        $data_array = array(
            'scratch_batches_only' => $scratch_batches_only,
            'scratch_coupons'      => $scratch_batches
        );

        if(!empty($scratch_batches))
        {
            echo json_encode($data_array);
            exit;
        }
        else
        {
            $error = array('no_data');
            echo json_encode($error);
            exit;
        }
    }

    /**
     * Send points data for brandbassador migration system
     * [int]      $id      [id of piecekeeper]
     *
     * @return [array] [return profile data]
     */
    public function getPiecekeeperPointsData($id)
    {   
        $this->db->select('*');
        $this->db->from('points');
        $this->db->where('id_user',$id);
        $points = $this->db->get()->result();

        if(!empty($points))
        {
            echo json_encode($points);
            exit;
        }
        else
        {
            $error = array('no_data');
            echo json_encode($error);
            exit;
        }
    }

     /**
     * Send payment requests data for brandbassador migration system
     * [int]      $id      [id of piecekeeper]
     *
     * @return [array] [return profile data]
     */
    public function getPiecekeeperPaymentRequestData($id)
    {
        $this->db->select('*');
        $this->db->from('payment_requests');
        $this->db->where('id_user',$id);
        $payment_requests = $this->db->get()->result();

        if(!empty($payment_requests))
        {
            echo json_encode($payment_requests);
            exit;
        }
        else
        {
            $error = array('no_data');
            echo json_encode($error);
            exit;
        }
    }

    /**
     * Send packages data for brandbassador migration system
     * [int]      $id      [id of piecekeeper]
     *
     * @return [array] [return profile data]
     */
    public function getPiecekeeperPackagesData($id)
    {
        $this->db->select('*');
        $this->db->from('packages');
        $this->db->where('id_user',$id);
        $packages = $this->db->get()->result();

        if(!empty($packages))
        {
            echo json_encode($packages);
            exit;
        }
        else
        {
            $error = array('no_data');
            echo json_encode($error);
            exit;
        }
    }

    /**
     * Send orders coupons data for brandbassador migration system
     * [int]      $id      [id of piecekeeper]
     *
     * @return [array] [return profile data]
     */
    public function getPiecekeeperOrdersCouponsAndOrders($id)
    {
        $this->db->select('orders_coupons.id, orders_coupons.order_id, orders_coupons.id_user, orders_coupons.coupon, orders_coupons.checked, orders_coupons.created');
        $this->db->from('orders_coupons');
        $this->db->where('id_user',$id);
        $packages = $this->db->get()->result();

        $this->db->select('orders_coupons.id, orders_coupons.order_id, orders_coupons.id_user, orders_coupons.coupon, orders_coupons.checked, orders_coupons.created, orders.*');
        $this->db->from('orders_coupons');
        $this->db->join('orders','orders_coupons.order_id =  orders.order_id','RIGHT');
        $this->db->where('id_user',$id);
        $orders = $this->db->get()->result();

        $data = array(
                'orders_coupons' => $packages,
                'orders'         => $orders
            );

        if(!empty($packages))
        {
            echo json_encode($data);
            exit;
        }
        else
        {
            $error = array('no_data');
            echo json_encode($error);
            exit;
        }
    }

    /**
     * Send notifications data for brandbassador migration system
     * [int]      $id      [id of piecekeeper]
     *
     * @return [array] [return profile data]
     */
    public function getPiecekeeperNotifications($id)
    {
        $this->db->select('*');
        $this->db->from('notifications');
        $this->db->where('id_user',$id);
        $notifications = $this->db->get()->result();

        if(!empty($notifications))
        {
            echo json_encode($notifications);
            exit;
        }
        else
        {
            $error = array('no_data');
            echo json_encode($error);
            exit;
        }
    }

    /**
     * Send notifications data for brandbassador migration system
     * [int]      $id      [id of piecekeeper]
     *
     * @return [array] [return profile data]
     */
    public function getPiecekeeperMissionUsers($id)
    {
        $this->db->select('*');
        $this->db->from('missions_users');
        $this->db->where('id_user',$id);
        $missions_users = $this->db->get()->result();

        if(!empty($missions_users))
        {
            echo json_encode($missions_users);
            exit;
        }
        else
        {
            $error = array('no_data');
            echo json_encode($error);
            exit;
        }
    }

    /**
     * Send coupons data for brandbassador migration system
     * [int]      $id      [id of piecekeeper]
     *
     * @return [array] [return profile data]
     */
    public function getPiecekeeperCoupons($id)
    {
        $this->db->select('*');
        $this->db->from('coupons');
        $this->db->where('id_creator',$id);
        $coupons = $this->db->get()->result();

        if(!empty($coupons))
        {
            echo json_encode($coupons);
            exit;
        }
        else
        {
            $error = array('no_data');
            echo json_encode($error);
            exit;
        }
    }

    /**
     * Send commissions data for brandbassador migration system
     * [int]      $id      [id of piecekeeper]
     *
     * @return [array] [return profile data]
     */
    public function getPiecekeeperCommission($id)
    {
        $this->db->select('*');
        $this->db->from('commissions');
        $this->db->where('id_user',$id);
        $commission = $this->db->get()->result();

        if(!empty($commission))
        {
            echo json_encode($commission);
            exit;
        }
        else
        {
            $error = array('no_data');
            echo json_encode($error);
            exit;
        }
    }

    /**
     * Send tags data for brandbassador migration system
     * [int]      $id      [id of piecekeeper]
     *
     * @return [array] [return profile data]
     */
    public function getPiecekeeperTags($id)
    {
        $this->db->select('*');
        $this->db->from('user_tags');
        $this->db->where('id_user',$id);
        $commission = $this->db->get()->result();

        if(!empty($commission))
        {
            echo json_encode($commission);
            exit;
        }
        else
        {
            $error = array('no_data');
            echo json_encode($error);
            exit;
        }
    }

     /**
     * Send tags data for brandbassador migration system
     * [int]      $id      [id of piecekeeper]
     *
     * @return [array] [return profile data]
     */
    public function getAllActivePiecekeepers()
    {
        $this->db->select('id');
        $this->db->from('auth_user');
        $this->db->where('is_active',1);
        $users = $this->db->get()->result();

        if(!empty($users))
        {
            echo json_encode($users);
            exit;
        }
        else
        {
            $error = array('no_data');
            echo json_encode($error);
            exit;
        }
    }

    /**
     * Send tags data for brandbassador migration system
     * [int]      $id      [id of piecekeeper]
     *
     * @return [array] [return profile data]
     */
    public function getAllPiecekeepers()
    {
        $this->db->select('id');
        $this->db->from('auth_user');
        $users = $this->db->get()->result();

        if(!empty($users))
        {
            echo json_encode($users);
            exit;
        }
        else
        {
            $error = array('no_data');
            echo json_encode($error);
            exit;
        }
    }
}
//12345678
