<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ranking extends CI_Controller
{
  public function __construct() {

    parent::__construct();

    $this->load->driver('cache');

    $this->load->model('points_model', 'Points');
    $this->load->model('social_model', 'social');
    $this->load->helper('url');

    if (!$this->authentication->isLogged()) {
            $this->authentication->saveTheLastUrl();
            redirect('/auth/login');
    }  
  }


  public function index() {

    $id_user = $this->session->userdata('id');
    $user    = $this->users->get($id_user);
    $data['user']     = $user;
    $data['title']    = 'Ranking';

    if ($user['date_joined'] < date("Y-m-d H:i:s", strtotime("-1 month"))) {
        $one_month = true;
    } else {
        $one_month = false;
    }

    $global_ranking = $this->users->getGlobalRanking($id_user);
    $country_ranking = $this->users->getCountryRanking($id_user);
    $city_ranking = $this->users->getCityRanking($id_user);

    if ($one_month) {
        $global_ranking_last_month = $this->users->getGlobalRanking($id_user, true);
        $country_ranking_last_month = $this->users->getCountryRanking($id_user, true);
        $city_ranking_last_month = $this->users->getCityRanking($id_user, true);

        $global_ranking_progress = $global_ranking_last_month - $global_ranking;
        $country_ranking_progress = $country_ranking_last_month - $country_ranking;
        $city_ranking_progress = $city_ranking_last_month - $city_ranking;
    }

    $top = $this->users->getTopUsers(20);

    $data['global_ranking']           = $global_ranking;
    $data['global_ranking_progress']  = isset($global_ranking_progress) ? $global_ranking_progress : null;
    $data['country_ranking']          = $country_ranking;
    $data['country_ranking_progress'] = isset($country_ranking_progress) ? $country_ranking_progress : null;
    $data['city_ranking']             = $city_ranking;
    $data['city_ranking_progress']    = isset($city_ranking_progress) ? $city_ranking_progress : null;
    $data['count_followers']          = $this->social->countFollowers($id_user);

    $data['top'] = $top;

    $data['totals'] = $this->db->query("SELECT COUNT(*) AS sales_no, SUM(o.transaction_amount) AS total_earnings FROM coupons AS c INNER JOIN orders_coupons AS oc on oc.coupon=c.code INNER JOIN orders AS o ON o.order_id=oc.order_id WHERE c.id_creator={$id_user}")->row();

    $points = $this->Points->getFromUser($id_user);
    $data['totals']->total_points = $points['total'];
    $data['totals']->bonus = $points['bonus'];


    $data['ranking_title']   = $this->db->query("SELECT * FROM content WHERE id=11")->row()->body;
    $data['ranking_content'] = $this->db->query("SELECT * FROM content WHERE id=12")->row()->body;

    $data['countFollowers'] = $this->social->countFollowers($this->session->userdata('id'));
    $data['one_month']      = $one_month;


	$this->load->view('templates/header', $data);
	$this->load->view('pages/ranking');
	$this->load->view('templates/footer');
  }


}
