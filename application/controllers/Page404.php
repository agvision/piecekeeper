<?php

class Page404 extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('authentication');
    }

    /*
     * Main function.
     */
    public function index()
    {
        /* check if the user is logged in */
        $front = $this->authentication->isLogged() ? "":"-front";

        $this->output->set_status_header('404');

        $data['title'] = 'Page Not Found';
        $data['logged'] = false;

        $this->load->view("templates/header-404", $data);
        $this->load->view('pages/404_page');
        $this->load->view("templates/footer{$front}");
    }
}

?>
