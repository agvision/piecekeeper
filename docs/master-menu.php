
<div class="large-2 columns menu">

    <h1>Api Description</h1>

    <a href="../index.php">Home</a>

    <h1>Endpoints</h1>

    <a href="login.php">Login</a>
    <a href="logout.php">Logout</a>
    <a href="forgotPassword.php">Forgot Password</a>
    <a href="resetPassword.php">Reset Password</a>
    <a href="isLogged.php">Is Logged</a>
    <a href="discountCode.php">Discount Code</a>
    <a href="profile.php">Profile</a>
    <a href="ranking.php">Ranking</a>
    <a href="goals.php">Goals</a>
    <a href="levels.php">Levels</a>
    <a href="missions.php">Missions</a>
    <a href="register.php">Register</a>
    <a href="profile_image.php">Upload Profile Image</a>
    <a href="countries.php">Countries</a>
    <a href="states.php">States</a>
    <a href="cities.php">Cities</a>
    <a href="schools.php">Schools</a>

</div>
