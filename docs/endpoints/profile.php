<?php
    $path = '../';
    require '../master-header.php';
    require '../master-menu.php';
?>

        <div class="large-8 columns content">
            <h1>Profile Endpoint</h1>

            <!-- User Profile API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">get</div>
                    /api/profile
                </div>
                <div class="description">
                    Get information about the user's profile
                </div>
                <div class="parameter">
                    <div class="name">key</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
                <div class="response">
                    <div class="title">Profile</div>
                    <div class="type">get</div>
                    <div class="url">/api/profile?key=1234</div>
                    <div class="btn">View Response</div>
                    <div class="content">
                        <pre>
If the user is not logged in the response will be:

    HTTP/1.1 403 Forbiden

If the key is not sent to the endpoint the response will be:

    HTTP/1.1 403 Forbiden

If the user is logged in the response will be:

    HTTP/1.1 200 OK

    {
        "first_name": "Casper Cornelius",
        "last_name": "Vasbotten",
        "country": "Norway",
        "city": "Oslo",
        "level": {
            "value": -1,
            "percentage": 12
        },
        "sales": 1,
        "earnings": {
            "cash": "5.84",
            "giftcard": "17.52"
        },
        "points": {
            "sales": 243,
            "bonus": 500,
            "total": 743,
            "percentage": 14
        },
        "ranking": {
            "global": {
                "last_month": 0,
                "rank": 190,
                "percentage": 84
            },
            "country": {
                "rank": 3,
                "last_month": 0
            },
            "city": {
                "rank": 1,
                "last_month": 0
            }
        }
    }

                        </pre>
                    </div>
                </div>
            </div><!-- End Profile API -->
        </div>
        <div class="large-1 columns">&nbsp;</div>

<?php
    $path = '../';
    require '../master-footer.php';
?>
