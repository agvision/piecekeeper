<?php
    $path = '../';
    require '../master-header.php';
    require '../master-menu.php';
?>
        <div class="large-8 columns content">
            <h1>Missions Endpoint</h1>

            <!-- Missions API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">get</div>
                    /api2/missions
                </div>
                <div class="description">
                    Returns a list of missions available for current user.
                </div>
                <div class="parameter">
                    <div class="name">key</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
                <div class="parameter">
                    <div class="name">Status</div>
                    <div class="details">
                        The status of mission
                    </div>
                </div>
                <div class="response">
                <div class="title">Missions</div>
                <div class="type">get</div>
                <div class="url">localhost/piecekeeper/api2/missions?key=1234&status=uncompleted</div>

                <div class="btn">View Response</div>
                <div class="content">
                    <pre>
    If the user is not logged in the response will be:

    HTTP/1.1 403 Forbiden

    If the key is not sent to the endpoint the response will be:

    HTTP/1.1 403 Forbiden

    If the user is logged in and the status is valid the response will be:

    HTTP/1.1 200 OK

    {
        "missions": [
            {
                "id": "123",
                "name": "Tell us what you're thinking!",
                "description": "<p>We &lt;3 our PieceKeepers, and your opinions are super important to us.&nbsp;So we want to know what you think about the PieceKeeper system! Fill just click the following&nbsp;link and fill out the survey. All of your answers are completely anonymous, so don&#39;t hold back!</p>\n\n<p><a href=\"https://www.surveymonkey.com/s/WHH2VVL\">https://www.surveymonkey.com/s/WHH2VVL</a></p>\n\n<p>When you finish, a page will pop up that says, &quot;Thank you for taking this survey&quot;. Just take a screen shot of this page and submit it here to complete the mission!</p>\n",
                "points": "100",
                "pk_levels": "",
                "image": "0",
                "video": "\t\t\t\t\t\t\t\t\t\t",
                "country": "0",
                "city": "",
                "from": "2015-02-26",
                "to": "2016-01-01",
                "allow_text": "0",
                "text_details": "",
                "allow_link": "0",
                "link_details": "",
                "allow_image": "1",
                "sales_goals": "0",
                "sales": "0",
                "sales_from": "1970-01-01",
                "sales_to": "1970-01-01",
                "fb_share": "0",
                "fb_link": "",
                "fb_text": "\t\t\t\t\t\t\t\t\t\t",
                "tw_share": "0",
                "tw_link": "",
                "tw_text": "\t\t\t\t\t\t\t\t\t\t",
                "pin_share": "0",
                "pin_link": "",
                "pin_text": "\t\t\t\t\t\t\t\t\t\t",
                "pin_image": "",
                "countdown": "0",
                "deleted": "0",
                "created": "2015-02-26 19:12:24",
                "is_sharing_mission": false,
                "is_post_mission": true,
                "is_sale_mission": false,
                "remaining": {
                    "days": 234,
                    "hours": 9,
                    "mins": 59
                },
                "submitted": false,
                "fb_shared": false,
                "tw_shared": false,
                "pin_shared": false
            }
        ]
    }

    If the user is logged in and the status is invalid the response will be:

    HTTP/1.1 200 OK

    {
        "error":"invalid status"
    }

                        </pre>
                    </div>
                </div>
            </div><!-- End Missions API -->


            <!-- Missions submit API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">post</div>
                    /api2/missions/123
                </div>
                <div class="description">
                    Endpoint for submitting missions for admin approval.
                </div>
                <div class="parameter">
                    <div class="name">key</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
                <div class="parameter">
                    <div class="name">link</div>
                    <div class="details">
                        Link for submit
                    </div>
                </div>
                <div class="parameter">
                    <div class="name">text</div>
                    <div class="details">
                        Text for submit
                    </div>
                </div>
                <div class="response">
                <div class="title">Missions</div>
                <div class="type">post</div>
                <div class="url">localhost/piecekeeper/api2/missions/123</div>

                <div class="btn">View Response</div>
                <div class="content">
                    <pre>
        If the user is not logged in the response will be:

        HTTP/1.1 403 Forbiden

        If the key is not sent to the endpoint the response will be:

        HTTP/1.1 403 Forbiden

        If the user is logged in and all the needed parameters depending on mission type were sent:

        HTTP/1.1 200 OK

        {
            "submitted": true
        }

        If the user is logged in and the parameters needed are not sent the response will be:

        HTTP/1.1 200 OK

        {
            "submitted": false
            "error":"Parameter x is required"
        }

                        </pre>
                    </div>
                </div>
            </div><!-- End Submit Missions API -->


            <!-- Upload Mission Image API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">post</div>
                    /api2/missions/123/type?key=1234
                </div>
                <div class="description">
                    Endpoint for uploading images for missions.
                </div>
                <div class="parameter">
                    <div class="name">key (GET)</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
                <div class="parameter">
                    <div class="name">file</div>
                    <div class="details">
                        Image file
                    </div>
                </div>
                <div class="response">
                <div class="title">Upload image for missions</div>
                <div class="type">post</div>
                <div class="url">localhost/piecekeeper/api2/missions/123/image?key=1234</div>

                <div class="btn">View Response</div>
                <div class="content">
                    <pre>
    If the user is not logged in the response will be:

    HTTP/1.1 403 Forbiden

    If the key is not sent to the endpoint the response will be:

    HTTP/1.1 403 Forbiden

    If the mission does not exist:

    HTTP/1.1 200 OK

    {
        "uploaded": false
        "error": "Mission does not exist."
    }

    If the mission does not allow images:

    HTTP/1.1 200 OK

    {
        "uploaded": false
        "error": "Mission does not allow images."
    }

    If the file is not sent or is empty:

    HTTP/1.1 200 OK

    {
        "uploaded": false
        "error": "Image is required."
    }

    If the file upload failed:

    HTTP/1.1 200 OK

    {
        "uploaded": false
        "error": "Upload failed."
    }

    If the image was uploaded:

    HTTP/1.1 200 OK

    {
        "uploaded": true
    }

                        </pre>
                    </div>
                </div>
            </div><!-- End Upload Mission Image API -->

            <!-- Social share mission API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">post</div>
                    /api2/missions/123/type?key=1234
                </div>
                <div class="description">
                    Endpoint for social share the missions.
                </div>
                <div class="parameter">
                    <div class="name">key (GET)</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
                <div class="parameter">
                    <div class="name">network</div>
                    <div class="details">
                        social network type(fb,tw,pin)
                    </div>
                </div>
                <div class="response">
                <div class="title">Post social account mission share</div>
                <div class="type">post</div>
                <div class="url">localhost/piecekeeper/api2/missions/123/share_completed?key=1234</div>

                <div class="btn">View Response</div>
                <div class="content">
                    <pre>
    If the user is not logged in the response will be:

    HTTP/1.1 403 Forbiden

    If the key is not sent to the endpoint the response will be:

    HTTP/1.1 403 Forbiden

    If the mission does not exist:

    HTTP/1.1 200 OK

    {
        "error": "Mission does not exist."
    }

    If type is empty or is not in this list fb, tw, pin the response will be:

    HTTP/1.1 200 OK

    {
        'updated'   => false,
        'completed' => false
    }

    If type is share_completed and post the response will be:

    HTTP/1.1 200 OK

    {
        'updated'   => true,
        'completed' => false
    }

    If type is share_completed and the mission is only is share only and all info ware shared the response will be:

    HTTP/1.1 200 OK

    {
        'updated'   => true,
        'completed' => true
    }

                    </pre>
                </div>
            </div>
        </div><!-- End Social share mission API -->

        </div>
        <div class="large-1 columns">&nbsp;</div>

<?php
    $path = '../';
    require '../master-footer.php';
?>
