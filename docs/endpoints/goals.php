<?php
    $path = '../';
    require '../master-header.php';
    require '../master-menu.php';
?>

        <div class="large-8 columns content">
            <h1>Goals Endpoint</h1>

            <!-- Goals API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">get</div>
                    /api/goals
                </div>
                <div class="description">
                    Returns a list of goals and related details for current authenticated user.
                </div>
                <div class="parameter">
                    <div class="name">key</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
                <div class="response">
                <div class="title">Goals</div>
                <div class="type">get</div>
                <div class="url">/api/goals?key=1234</div>

                <div class="btn">View Response</div>
                <div class="content">
                    <pre>
            If the user is not logged in the response will be:

            HTTP/1.1 403 Forbiden

            If the key is not sent to the endpoint the response will be:

            HTTP/1.1 403 Forbiden

            If the user is logged in, the response will be:

            HTTP/1.1 200 OK

            [
                {
                    "id": "18",
                    "title": "<b>GLOBAL GOAL:</b> 500000 total points within 2014-12-24",
                    "deadline": "2014-12-24 00:00:00",
                    "invited": "50",
                    "points": "500000",
                    "country": "0",
                    "city": "",
                    "description": "",
                    "required_age": "18",
                    "required_date": "01/29/2015",
                    "deleted": "0",
                    "created": "2014-04-15 17:44:08",
                    "short_name": null,
                    "expired": true,
                    "rank": 1410,
                    "rank_label": "YOUR GLOBAL RANK",
                    "invited_label": "Globally",
                    "earned_points": "2343226",
                    "rank_status": "EXPIRED",
                    "rank_status_class": "rank-bottom-danger",
                    "progress_width": 300,
                    "completed": true
                },
                {
                    "id": "16",
                    "title": "<b>GLOBAL GOAL:</b> 500000 total points within 2014-02-01",
                    "deadline": "2014-02-01 00:00:00",
                    "invited": "10",
                    "points": "500000",
                    "country": "0",
                    "city": "",
                    "description": "",
                    "required_age": "0",
                    "required_date": "",
                    "deleted": "0",
                    "created": "2013-12-09 15:05:14",
                    "short_name": null,
                    "expired": true,
                    "rank": 1410,
                    "rank_label": "YOUR GLOBAL RANK",
                    "invited_label": "Globally",
                    "earned_points": "179731",
                    "rank_status": "EXPIRED",
                    "rank_status_class": "rank-bottom-danger",
                    "progress_width": 105,
                    "completed": false
                }
            ]
                        </pre>
                    </div>
                </div>
            </div><!-- End Goals API -->

        </div>
        <div class="large-1 columns">&nbsp;</div>

<?php
    $path = '../';
    require '../master-footer.php';
?>
