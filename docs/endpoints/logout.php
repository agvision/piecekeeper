<?php
    $path = '../';
    require '../master-header.php';
    require '../master-menu.php';
?>

        <div class="large-8 columns content">
            <h1>Logout Endpoint</h1>

            <!-- Missions API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">get</div>
                    /api2/logout
                </div>
                <div class="description">
                    Logout the user.
                </div>
                <div class="parameter">
                    <div class="name">key</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
                <div class="response">
                <div class="title">Logout</div>
                <div class="type">get</div>
                <div class="url">localhost/piecekeeper/api2/logout?key=1234</div>

                <div class="btn">View Response</div>
                <div class="content">
                    <pre>
                If the user is not logged in the response will be:

                HTTP/1.1 403 Forbiden

                If the key is not sent to the endpoint the response will be:

                HTTP/1.1 403 Forbiden

                If the user is logged in response will be:

                HTTP/1.1 200 OK

                {
                    "logout": true
                }

                        </pre>
                    </div>
                </div>
            </div><!-- End Missions API -->

        </div>
        <div class="large-1 columns">&nbsp;</div>

<?php
    $path = '../';
    require '../master-footer.php';
?>
