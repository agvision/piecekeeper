<?php
    $path = '../';
    require '../master-header.php';
    require '../master-menu.php';
?>

        <div class="large-8 columns content">
            <h1>Levels Endpoint</h1>

            <!-- Levels API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">get</div>
                    /api2/levels
                </div>
                <div class="description">
                    Returns details about levels.
                </div>
                <div class="parameter">
                    <div class="name">key</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
                <div class="response">
                <div class="title">Levels</div>
                <div class="type">get</div>
                <div class="url">/api2/levels?key=1234</div>

                <div class="btn">View Response</div>
                <div class="content">
                    <pre>
            If the user is not logged in the response will be:

            HTTP/1.1 403 Forbiden

            If the key is not sent to the endpoint the response will be:

            HTTP/1.1 403 Forbiden

            If the user is logged in the response will be:

            HTTP/1.1 200 OK

            {
                "level_1": {
                    "commission": "5",
                    "details": [
                        " Create single use codes",
                        " Rack up points by doing missions",
                        " Generate sales by sharing codes to friends"
                    ]
                },
                "level_2": {
                    "commission": "6",
                    "details": [
                        " Create multi use codes to share on your social media",
                        " Earn 2,000 bonus points",
                        " Receive your Level 2 Package",
                        " Get your PieceKeeper Member Card for discounts in all concept stores"
                    ]
                },
                "level_3": {
                    "commission": "7",
                    "details": [
                        " Create your own sales team and earn 20% of the commission your team brings in",
                        " Earn 3,000 bonus points",
                        " Receive Level 3 Package including a free OnePiece",
                        " Get the chance to join us on amazing trips like the Yacht Trip if we reach our goals"
                    ]
                }
            }
                        </pre>
                    </div>
                </div>
            </div><!-- End Level API -->

        </div>
        <div class="large-1 columns">&nbsp;</div>
<?php
    $path = '../';
    require '../master-footer.php';
?>
