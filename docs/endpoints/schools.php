<?php
    $path = '../';
    require '../master-header.php';
    require '../master-menu.php';
?>

    <div class="large-8 columns content">
            <h1>Schools Endpoint</h1>

            <!-- Schools Code API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">get</div>
                    /api2/schools
                </div>
                <div class="description">Returns a list of schools.</div>
                <div class="parameter">
                    <div class="name">key</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
                <div class="parameter">
                    <div class="name">id_country</div>
                    <div class="details">
                        Id of country
                    </div>
                </div>
                <div class="parameter">
                    <div class="name">state</div>
                    <div class="details">
                        Name of state
                    </div>
                </div>
                <div class="parameter">
                    <div class="name">city</div>
                    <div class="details">
                        Name of city
                    </div>
                </div>
                <div class="response">
                    <div class="title">Schools</div>
                    <div class="type">get</div>
                    <div class="url">/api2/schools?key=1234&city=London</div>
                    <div class="btn">View Response</div>
                    <div class="content">
                        <pre>
If the key is not sent to the endpoint the response will be:

    HTTP/1.1 403 Forbiden

If the key is correct:
    HTTP/1.1 200 OK

{
    "schools": [
       "Arts Ed Drama School, London",
       "Birmingham City University",
       "Bournemouth University",
       "British Academy of New Music ",
       "Brunel Business University",
       "Brunel University London",
       "Buckinghamshire new uni ",
       "Buxton",
       "cassette business school",
       "Durham University",
       "Edinburgh University",
       "Haileybury College",
       "Highgate school",
       "Ibstock Place School",
       "Loughborough University",
       "mmu",
       "Mulberry school for girls",
       "Musika Korp",
       "Parmiters",
       "Queen Margaret University Edinburgh",
       "Queen Mary, University of London",
       "Rosebery School",
       "Selborne college, South Africa ",
       "Sylvia Young Theatre School",
       "Tech  music school",
       "The College of Naturopathic Medicine",
       "UCL",
       "University of Bath",
       "University of Gdansk ",
       "University of Greenwich ",
       "University of Leicester",
       "University Of Southampton ",
       "University of the Arts",
       "University. Of law Moorgate"
   ]
}


                        </pre>
                    </div>
                </div>
            </div><!-- End Schools Code API -->



        </div>
        <div class="large-1 columns">&nbsp;</div>

<?php
    $path = '../';
    require '../master-footer.php';
?>
