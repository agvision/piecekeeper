<?php
    $path = '../';
    require '../master-header.php';
    require '../master-menu.php';
?>

    <div class="large-8 columns content">
            <h1>Countries Endpoint</h1>

            <!-- Discount Code API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">get</div>
                    /api/countries
                </div>
                <div class="description">Returns a list of countries and ids.</div>
                <div class="parameter">
                    <div class="name">key</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
                <div class="response">
                    <div class="title">Countries</div>
                    <div class="type">get</div>
                    <div class="url">/api/countries?key=1234</div>
                    <div class="btn">View Response</div>
                    <div class="content">
                        <pre>
If the key is not sent to the endpoint the response will be:

    HTTP/1.1 403 Forbiden

If the key is correct:
    HTTP/1.1 200 OK

{
	countries: [
		{
			country_id: "1",
			iso2: "af",
			short_name: "Afghanistan",
			long_name: "Islamic Republic of Afghanistan",
			iso3: "AFG",
			numcode: "004",
			un_member: "yes",
			calling_code: "93",
			cctld: ".af",
			currency: ""
		},
		{
			country_id: "2",
			iso2: "ax",
			short_name: "Aland Islands",
			long_name: "&Aring;land Islands",
			iso3: "ALA",
			numcode: "248",
			un_member: "no",
			calling_code: "358",
			cctld: ".ax",
			currency: ""
		},
		...
	]
}


                        </pre>
                    </div>
                </div>
            </div><!-- End Discount Code API -->



        </div>
        <div class="large-1 columns">&nbsp;</div>

<?php
    $path = '../';
    require '../master-footer.php';
?>
