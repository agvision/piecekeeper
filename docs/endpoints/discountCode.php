<?php
    $path = '../';
    require '../master-header.php';
    require '../master-menu.php';
?>

    <div class="large-8 columns content">
            <h1>Discount Code Endpoint</h1>

            <!-- Discount Code API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">get</div>
                    /api2/discountCode
                </div>
                <div class="description">Return a list of active discount codes for the current logged in user.</div>
                <div class="parameter">
                    <div class="name">key</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
                <div class="response">
                    <div class="title">Discount Codes</div>
                    <div class="type">get</div>
                    <div class="url">/api2/discountCode?key=1234</div>
                    <div class="btn">View Response</div>
                    <div class="content">
                        <pre>
If the user is not logged in the response will be:

    HTTP/1.1 403 Forbiden

If the user dosen't have any coupons the response will be:

    HTTP/1.1 200 OK

    {
        "success": true,
        "discount_codes": [

        ]
    }

If the user have coupons the response will be:
    HTTP/1.1 200 OK

    {
        "success": true,
        "discount_codes": [
            {
                "code": "CodeCoupon1",
                "discount": "20",
                "expires": "21 May 2015 15:38"
            },
            {
                "code": "CodeCoupon1",
                "discount": "20",
                "expires": "25 May 2015 15:38"
            }
        ]
    }

If the key is not sent to the endpoint the response will be:

    HTTP/1.1 403 Forbiden
                        </pre>
                    </div>
                </div>
            </div><!-- End Discount Code API -->


            <!--Create Discount Code API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">post</div>
                    /api2/discountCode/create
                </div>
                <div class="description">Create discount code.</div>

                <div class="parameter">

                    <div class="name">key</div>
                    <div class="details">
                        Authentication for api
                    </div>

                </div>

                <div class="parameter">
                    <div class="name">Type</div>
                    <div class="details">
                        1 | 2 (singleuse | multiuse)
                    </div>
                </div>

                <div class="parameter">
                    <div class="name">code</div>
                    <div class="details">
                        Coupon Code
                    </div>
                </div>

                <div>
                    <div class="name">link</div>
                    <div class="details">
                        Link for multiuse coupon.
                    </div>
                </div>


                <div class="response">
                    <div class="title">Create Discount Codes</div>
                    <div class="type">post</div>
                    <div class="url">/api2/discountCode/create</div>
                    <div class="btn">View Response</div>
                    <div class="content">
                        <pre>
If the user is not logged in the response will be:

    HTTP/1.1 403 Forbiden

If a field is missing:

    HTTP/1.1 200 OK

    {
        "error": 'Field X is required',
        "created" : false;
    }

If the coupon was created:
    HTTP/1.1 200 OK

    {
        "created" : true;
    }

If the key is not sent to the endpoint the response will be:

    HTTP/1.1 403 Forbiden
                        </pre>
                    </div>
                </div>
            </div><!-- Create Discount Code API -->

        </div>
        <div class="large-1 columns">&nbsp;</div>

<?php
    $path = '../';
    require '../master-footer.php';
?>
