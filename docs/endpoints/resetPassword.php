<?php
    $path = '../';
    require '../master-header.php';
    require '../master-menu.php';
?>

    <div class="large-8 columns content">
            <h1>Reset Password Endpoint</h1>

            <!-- Discount Code API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">Post</div>
                    /api2/resetPassword
                </div>
                <div class="description">Reset user password.</div>
                <div class="parameter">
                    <div class="name">key</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
                <div class="parameter">
                    <div class="name">hash</div>
                    <div class="details">
                        Hash of user
                    </div>
                </div>
                <div class="parameter">
                    <div class="name">password</div>
                    <div class="details">
                        The new password
                    </div>
                </div>
                <div class="response">
                    <div class="title">Reset Password</div>
                    <div class="type">Post</div>
                    <div class="url">/api2/resetPassword</div>
                    <div class="btn">View Response</div>
                    <div class="content">
                        <pre>
If the key is not sent to the endpoint the response will be:

    HTTP/1.1 403 Forbiden


If the key is sent to the endpoint but the hash of user is not sent
the response will be:

    HTTP/1.1 403 Forbiden

If the key is sent to the endpoint and the hash of user is incorrect
the response will be:

        HTTP/1.1 403 Forbiden

If the key is correct and the password is sent and the hash is found in the DB
the response will be:

    HTTP/1.1 200 OK
    {
      updated: true
    }

If the key is correct, the password is not sent and the hash is found in the DB
the response will be :

    HTTP/1.1 200 OK

    {
      updated: false,
      errors: 'Password is required'
    }

                        </pre>
                    </div>
                </div>
            </div><!-- End Discount Code API -->



        </div>
        <div class="large-1 columns">&nbsp;</div>

<?php
    $path = '../';
    require '../master-footer.php';
?>
