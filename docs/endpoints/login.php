<?php
    $path = '../';
    require '../master-header.php';
    require '../master-menu.php';
?>

        <div class="large-8 columns content">
            <h1>Login Endpoint</h1>

            <!-- Login API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">get</div>
                    /api2/login
                </div>
                <div class="description">Login the user into site</div>
                <div class="parameter">
                    <div class="name">key</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
                <div class="parameter">
                    <div class="name">Email</div>
                    <div class="details">
                        Email of user needed for login
                    </div>
                </div>
                <div class="parameter">
                    <div class="name">Password</div>
                    <div class="details">
                        Password of user needed for login
                    </div>
                </div>
                <div class="response">
                    <div class="title">Login</div>
                    <div class="type">get</div>
                    <div class="url">/api2/login?key=1234&email=example@email&password=1234</div>
                    <div class="btn">View Response</div>
                    <div class="content">
                        <pre>
If the user successful login respone will be:

    HTTP/1.1 200 OK
    {
        "logged" : true
        "id" : # {user_id}
    }

If the user hasn't successful loged in the response will be:

    HTTP/1.1 200 OK
    {
        "logged" : false
    }

If the user has reached the maximum number of failed login attempts the respone will be:

    HTTP/1.1 403 Forbiden
                        </pre>
                    </div>
                </div>
            </div><!-- End Login API -->
        </div>
        <div class="large-1 columns">&nbsp;</div>

<?php
    $path = '../';
    require '../master-footer.php';
?>
