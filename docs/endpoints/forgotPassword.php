<?php
    $path = '../';
    require '../master-header.php';
    require '../master-menu.php';
?>

    <div class="large-8 columns content">
            <h1>Forgot Password Endpoint</h1>

            <!-- Discount Code API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">Post</div>
                    /api2/forgotPassword
                </div>
                <div class="description">Send forgot password email.</div>
                <div class="parameter">
                    <div class="name">key</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
                <div class="parameter">
                    <div class="name">email</div>
                    <div class="details">
                        Email of user that want to reset his password
                    </div>
                </div>
                <div class="response">
                    <div class="title">Forgot Password</div>
                    <div class="type">Post</div>
                    <div class="url">/api2/forgotPassword</div>
                    <div class="btn">View Response</div>
                    <div class="content">
                        <pre>
If the key is not sent to the endpoint the response will be:

    HTTP/1.1 403 Forbiden

If the key is correct and the email is sent and is found in the DB the response will be:

    HTTP/1.1 200 OK

    {
      sent: true
    }

If the key is correct and the email is sent and is not found in the DB
or the emails is not sent the response will be :

    HTTP/1.1 200 OK

    {
      sent: false,
      error: 'Invalid email'
    }

                        </pre>
                    </div>
                </div>
            </div><!-- End Discount Code API -->



        </div>
        <div class="large-1 columns">&nbsp;</div>

<?php
    $path = '../';
    require '../master-footer.php';
?>
