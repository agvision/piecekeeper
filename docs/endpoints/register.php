<?php
    $path = '../';
    require '../master-header.php';
    require '../master-menu.php';
?>

        <div class="large-8 columns content">
            <h1>Register Endpoint</h1>

            <!-- Discount Code API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">post</div>
                    /api/register
                </div>
                <div class="description">Return bool and validation errors.</div>
                <div class="parameter">
                    <div class="name">key</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">email</div>
                    <div class="details">
                        Email (required, unique, valid)
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">first_name</div>
                    <div class="details">
                        First name (required)
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">last_name</div>
                    <div class="details">
                        Last name (required)
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">password</div>
                    <div class="details">
                        Password (required)
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">day</div>
                    <div class="details">
                        Day of birth (required)
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">month</div>
                    <div class="details">
                        Month of birth (required)
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">year</div>
                    <div class="details">
                        Year of birth (required)
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">phone</div>
                    <div class="details">
						Phone number (required)
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">address1</div>
                    <div class="details">
                        Address 1 (required)
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">address2</div>
                    <div class="details">
                        Address 2
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">postcode</div>
                    <div class="details">
                        Zip / Postal Code
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">country</div>
                    <div class="details">
                        Country (must be X_Name format), (required)
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">state</div>
                    <div class="details">
                        State
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">city</div>
                    <div class="details">
                        City (required)
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">gender</div>
                    <div class="details">
                        Gender
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">school</div>
                    <div class="details">
                        School
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">gratuation_year</div>
                    <div class="details">
						Graduation year
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">terms</div>
                    <div class="details">
						Terms and Conditions
                    </div>
                </div>
                <div class="response">
                    <div class="title">Register</div>
                    <div class="type">post</div>
                    <div class="url">/api/register</div>
                    <div class="btn">View Response</div>
                    <div class="content">
                        <pre>
If the key is not sent to the endpoint the response will be:

    HTTP/1.1 403 Forbiden

If the user has registered successfully:

    HTTP/1.1 200 OK

    {
        "registered": true
    }

If there are validation errors:

    HTTP/1.1 200 OK

	{
	    "registered": false,
	    "errors": {
	        "column_name_1": "Column_1 error",
	        "column_name_2": "Column_2 error",
			...
	    }
	}
                        </pre>
                    </div>
                </div>
            </div><!-- End Discount Code API -->
        </div>
        <div class="large-1 columns">&nbsp;</div>

<?php
    $path = '../';
    require '../master-footer.php';
?>
