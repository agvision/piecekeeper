<?php
    $path = '../';
    require '../master-header.php';
    require '../master-menu.php';
?>

        <div class="large-8 columns content">
            <h1>Ranking Endpoint</h1>

            <!-- Ranking API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">get</div>
                    /api/ranking
                </div>
                <div class="description">
                    Return first {{ length }} users ordered descending by total points
                </div>
                <div class="parameter">
                    <div class="name">key</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
                <div class="parameter">
                    <div class="name">Length</div>
                    <div class="details">
                        The number of users that will be returned
                    </div>
                </div>
                <div class="response">
                    <div class="title">Ranking</div>
                    <div class="type">get</div>
                    <div class="url">/api/ranking?key=1234&length=3</div>
                    <div class="btn">View Response</div>
                    <div class="content">
                        <pre>
If the user is not logged in the response will be:

    HTTP/1.1 403 Forbiden

If the key is not sent to the endpoint the response will be:

    HTTP/1.1 403 Forbiden

If the length parameter is missing or is not a valid number the response will be:

    HTTP/1.1 200 OK

    {
        error: "Invalid length parameter"
    }

If the user is logged in and the length parameter exists and is a valid number, the response will be:

    HTTP/1.1 200 OK

    {
        top: [
            {
                total_points: "331772",
                first_name: "Sandra",
                last_name: "G",
                country: "lt"
            },
            {
                total_points: "257384",
                first_name: "Micaela Valderrama and",
                last_name: "Jordyn Mcanany",
                country: "us"
            },
            {
                total_points: "225635",
                first_name: "Timmy ",
                last_name: "Souk ",
                country: "us"
            }
        ]
    }
                        </pre>
                    </div>
                </div>
            </div><!-- End Ranking API -->

        </div>
        <div class="large-1 columns">&nbsp;</div>

<?php
    $path = '../';
    require '../master-footer.php';
?>
