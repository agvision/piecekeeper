<?php
    $path = '../';
    require '../master-header.php';
    require '../master-menu.php';
?>

    <div class="large-8 columns content">
            <h1>States Endpoint</h1>

            <!-- States Code API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">get</div>
                    /api2/states
                </div>
                <div class="description">Returns a list of USA States.</div>
                <div class="parameter">
                    <div class="name">key</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
                <div class="response">
                    <div class="title">States</div>
                    <div class="type">get</div>
                    <div class="url">/api2/states?key=1234</div>
                    <div class="btn">View Response</div>
                    <div class="content">
                        <pre>
If the key is not sent to the endpoint the response will be:

    HTTP/1.1 403 Forbiden

If the key is correct:
    HTTP/1.1 200 OK

{
    "states": [
       {
           "abbreviation": "AK",
           "state": "Alaska"
       },
       {
           "abbreviation": "AL",
           "state": "Alabama"
       },
   	...
	]
}


                        </pre>
                    </div>
                </div>
            </div><!-- End States Code API -->



        </div>
        <div class="large-1 columns">&nbsp;</div>

<?php
    $path = '../';
    require '../master-footer.php';
?>
