<?php
    $path = '../';
    require '../master-header.php';
    require '../master-menu.php';
?>

        <div class="large-8 columns content">
            <h1>Upload Profile Image Endpoint</h1>

            <!-- User Profile API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">POST</div>
                    /api/profile_image/{index}/key=1234
                </div>
                <div class="description">
                    Upload profile image
                </div>
                <div class="parameter">
                    <div class="name">index (GET)</div>
                    <div class="details">
                        1 | 2 - image 1 or image 2 (default 1)
                    </div>
                </div>
                <div class="parameter">
                    <div class="name">key (GET)</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
                <div class="parameter">
                    <div class="name">file</div>
                    <div class="details">
                        Uploaded image
                    </div>
                </div>
                <div class="response">
                    <div class="title">Profile Image</div>
                    <div class="type">post</div>
                    <div class="url">/api/profile_image/{index}?key=1234</div>
                    <div class="btn">View Response</div>
                    <div class="content">
                        <pre>
If the user is not logged in or or the user is not recently registered, the response will be:

    HTTP/1.1 403 Forbiden

If the key is not sent to the endpoint the response will be:

    HTTP/1.1 403 Forbiden

If the file is missing:

    HTTP/1.1 200 OK

    {
        uploaded: false,
        error: "File does not exist."
    }

If the upload failed:

    HTTP/1.1 200 OK

    {
        uploaded: false,
        error: "Upload failed."
    }

If the file was uploaded:

    HTTP/1.1 200 OK

    {
        "uploaded": true
    }

                        </pre>
                    </div>
                </div>
            </div><!-- End Profile Image API -->
        </div>
        <div class="large-1 columns">&nbsp;</div>

<?php
    $path = '../';
    require '../master-footer.php';
?>
