<?php
    $path = '../';
    require '../master-header.php';
    require '../master-menu.php';
?>

    <div class="large-8 columns content">
            <h1>Cities Endpoint</h1>

            <!-- Discount Code API -->
            <div class="endpoint">
                <div class="url">
                    <div class="type">get</div>
                    /api/cities
                </div>
                <div class="description">Returns a list of cities.</div>
                <div class="parameter">
                    <div class="name">key</div>
                    <div class="details">
                        Authentication for api
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">country</div>
                    <div class="details">
                        Country ID (optional)
                    </div>
                </div>
				<div class="parameter">
                    <div class="name">state</div>
                    <div class="details">
                        State Name (optional)
                    </div>
                </div>
                <div class="response">
                    <div class="title">Cities</div>
                    <div class="type">get</div>
                    <div class="url">/api/cities?key=1234&country=236&state=Michigan</div>
                    <div class="btn">View Response</div>
                    <div class="content">
					<ul>
						<li>If the country is given, group by country</li>
						<li>If the country and state are given, group by country and by state</li>
						<li>Otherwise, return all the cities</li>
					</ul>
                        <pre>
If the key is not sent to the endpoint the response will be:

    HTTP/1.1 403 Forbiden

If the key is correct:

    HTTP/1.1 200 OK

	{
	    "cities": [
	        {
	            "city": "Clawson"
	        },
	        {
	            "city": "Detroit"
	        },
	        {
	            "city": "Grand Rapids"
	        },
	        {
	            "city": "Madison"
	        }
	    ]
	}


                        </pre>
                    </div>
                </div>
            </div><!-- End Discount Code API -->



        </div>
        <div class="large-1 columns">&nbsp;</div>

<?php
    $path = '../';
    require '../master-footer.php';
?>
