<?php
    $path = '';
    require 'master-header.php';
?>

<div class="large-2 columns menu">

    <h1>Api Description</h1>

    <a href="index.php" class="active-menu-item">Home</a>

    <h1>Endpoints</h1>

    <a href="endpoints/login.php">Login</a>
    <a href="endpoints/logout.php">Logout</a>
    <a href="endpoints/forgotPassword.php">Forgot Password</a>
    <a href="endpoints/resetPassword.php">Reset Password</a>
    <a href="endpoints/isLogged.php">Is Logged</a>
    <a href="endpoints/discountCode.php">Discount Code</a>
    <a href="endpoints/profile.php">Profile</a>
    <a href="endpoints/ranking.php">Ranking</a>
    <a href="endpoints/goals.php">Goals</a>
    <a href="endpoints/levels.php">Levels</a>
    <a href="endpoints/missions.php">Missions</a>
    <a href="endpoints/register.php">Register</a>
    <a href="endpoints/profile_image.php">Upload Profile Image</a>
    <a href="endpoints/countries.php">Countries</a>
    <a href="endpoints/states.php">States</a>
    <a href="endpoints/cities.php">Cities</a>
    <a href="endpoints/schools.php">Schools</a>

</div>

<div class="large-8 columns content">
        <div class="section">
            <h1>API Documentation for OnePiece Mobile Aplication</h1>

            <p> This documentation will present the endpoints for mobile aplication. </p>

        </div>
    <div class="large-1 columns">&nbsp;</div>
</div>

<?php
    require 'master-footer.php';
?>
