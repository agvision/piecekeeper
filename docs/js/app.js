'use strict';

var app = (function(document, $) {
	var docElem = document.documentElement,
		_userAgentInit = function() {
			docElem.setAttribute('data-useragent', navigator.userAgent);
		},
		_init = function() {
			$(document).foundation();
            // needed to use joyride
            // doc: http://foundation.zurb.com/docs/components/joyride.html
            $(document).on('click', '#start-jr', function () {
                $(document).foundation('joyride', 'start');
            });
			_userAgentInit();

			$(".response .btn").click(function(){
				var endpoint = $(this).parents(".endpoint");
				$(endpoint).find(".content").slideToggle();
			});
		};
	return {
		init: _init
	};
})(document, jQuery);

(function() {
	app.init();
})();
