<!doctype html>
<html class="no-js" lang="en" >
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Docs|Piecekeeper</title>
    <!-- build:css css/libraries.min.css -->
    <link rel="stylesheet" href="<?php echo $path;?>bower_components/font-awesome/css/font-awesome.css">
    <!-- bower:css -->
    <!-- endbower -->
    <!-- endbuild -->
    <!-- build:css css/app.min.css -->
    <link rel="stylesheet" href="<?php echo $path;?>css/app.css">
    <link rel="stylesheet" href="<?php echo $path;?>css/app_override.css">
    <!-- endbuild -->
    <!-- build:js js/vendor/modernizr.min.js -->
    <script src="<?php echo $path;?>bower_components/modernizr/modernizr.js"></script>
    <!-- endbuild -->
  </head>
  <body>

    <div class="row header">
        <div class="large-12 columns text-center">
            <img src="<?php echo $path;?>images/onepiece-logo-white.png" alt="logo" title="logo" />
        </div>
    </div>
    <div class="row">
        <div class="large-1 columns">&nbsp;</div>
